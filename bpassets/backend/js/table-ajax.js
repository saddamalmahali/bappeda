// =========================================================================
// Global Function
// =========================================================================

// Grid Data
var gridTable = function(el, action=false, target='', limit='') {
    if ( ! el.length ) {
        return;
    }

    var url     = el.data('url');
    var grid    = new Datatable();
    var tgt     = ( target!="" ? target : [ -1, 0 ] );
    var lmt     = ( limit!="" ? limit : 10 );

    grid.init({
        src: el,
        onSuccess: function(grid) {
            $('.btn-tooltip').tooltip({html:true});
        },
        onError: function(grid) {},
        dataTable: {
            "aLengthMenu": [
                [10, 20, 50, 100, -1],
                [10, 20, 50, 100, "All"]                        // change per page values here
            ],
            "iDisplayLength": lmt,                               // default record count per page
            "bServerSide": true,                                // server side processing
            "sAjaxSource": url,       // ajax source
            "aoColumnDefs": [
              { 'bSortable': false, 'aTargets': tgt }
           ]
        }
    });

    grid.getTableWrapper().on( 'draw', function () {
        $('.btn-tooltip').tooltip({
            html:true
        });
        var _tooltip = $('[data-toggle="tooltip"]');
        if ( _tooltip.length ) {
            _tooltip.tooltip();
        }
        var _popover = $('[data-toggle="popover"]');
        if ( _popover.length ) {
            _popover.each(function() {
                ! function(e) {
                    e.data("color") && (a = "popover-" + e.data("color"));
                    var t = {
                        trigger: "focus",
                        template: '<div class="popover ' + a + '" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>'
                    };
                    _popover.popover(t)
              }($(this))
            });
        }
    });

    if( action ){
        gridExport( grid, '.table-export-excel', url );
    }
}

// Export Grid Data
var gridExport = function( dataTable, selectorBtn, sUrl, sAction, parameter = '' ) {
    // handle group actionsubmit button click
    dataTable.getTableWrapper().on('click', selectorBtn, function(e) {
        e.preventDefault();

        if ( typeof sAction == 'undefined' ){
            sAction = 'export_excel';
        }

        var base_url    = window.location.origin + '/';
        var params      = 'export='+sAction;
        var table       = $( selectorBtn ).closest( '.table-container' ).find( 'table' );

        // get all typeable inputs
        $( 'textarea.form-filter, select.form-filter, input.form-filter:not([type="radio"],[type="checkbox"])', table ).each( function() {
            params += '&' + $(this).attr("name") + '=' + $(this).val();
        });

        // get all checkable inputs
        $( 'input.form-filter[type="checkbox"]:checked, input.form-filter[type="radio"]:checked', table ).each( function() {
            params += '&' + $(this).attr("name") + '=' + $(this).val();
        });

        if (parameter) {
            params += '&' + parameter;
        }

        var link_export = sUrl + '?' + params;

        $("div#mask").fadeIn();
        document.location.href =(link_export);
        setTimeout(function () { 
            $("div#mask").fadeOut();
            URL.revokeObjectURL(link_export); 
        }, 100);
    });
};

// Grid DatePicker
var initPickers = function () {
    //init date pickers
    $('.date-picker').datepicker({
        // rtl: App.isRTL(),
        autoclose: true
    });

    $( '.date-picker-month' ).datepicker({
        // rtl: App.isRTL(),
        autoclose: true,
        viewMode: 'years',
        minViewMode: 'months'
    });
};

// =========================================================================
// Master List Function
// =========================================================================
var TableAjaxMasterList = function () {
    var handleRecordsUrusanList = function() {
        gridTable( $("#list_table_urusan"), false );
    };
    var handleRecordsSubUrusanList = function() {
        gridTable( $("#list_table_suburusan"), false );
    };
    var handleRecordsProgramList = function() {
        gridTable( $("#list_table_program"), false );
    };
    var handleRecordsKegiatanList = function() {
        gridTable( $("#list_table_kegiatan"), false );
    }; handleRecordsSubKegiatanList = function() {
        gridTable( $("#list_table_sub_kegiatan"), false );
    };

    return {
        //main function to initiate the module
        init: function () {
            initPickers();
            handleRecordsUrusanList();
            handleRecordsSubUrusanList();
            handleRecordsProgramList();
            handleRecordsKegiatanList();
            handleRecordsSubKegiatanList();
        }
    };
}();

// =========================================================================
// Satuan Kerja List Function
// =========================================================================
var TableAjaxSatuanKerjaList = function () {
    var handleRecordsBidangList = function() {
        gridTable( $("#list_table_bidang"), false );
    };

    var handleRecordsSkpdList = function(){
        gridTable($('#list_table_skpd'));
    }

    return {
        //main function to initiate the module
        init: function () {
            initPickers();
            handleRecordsBidangList();
        },
        initSkpd: function(){
            initPickers();
            handleRecordsSkpdList();
        }
    };
}();

// =========================================================================
// Member List Function
// =========================================================================
var TableAjaxMemberLoanList = function () {
    var handleRecordsMemberLoanList = function() {
        gridTable( $("#list_table_member_loan"), false );
    };

    var handleRecordsMemberDepositeLoanList = function() {
        gridTable( $("#list_table_member_deposite_loan"), false );
    };

    return {
        //main function to initiate the module
        init: function () {
            initPickers();
            handleRecordsMemberLoanList();
            handleRecordsMemberDepositeLoanList();
        }
    };
}();

// =========================================================================
// Board List List Function
// =========================================================================
var TableAjaxBoardList = function () {
    var handleRecordsMemberBoardList = function() {
        gridTable( $("#list_table_member_board"), false );
    };

    return {
        //main function to initiate the module
        init: function () {
            initPickers();
            handleRecordsMemberBoardList();
        }
    };
}();

// =========================================================================
// Find Agent List Function
// =========================================================================
var TableAjaxFindAgent = function () {
    var handleRecordsFindAgent = function() {
        var table       = $("#list_table_find_agent");
        var url         = table.data('url');
        var grid        = new Datatable();
        grid.addAjaxParam("search_province_id", $('.select_province').val());
        grid.addAjaxParam("search_district_id", $('.select_district').val());
        grid.addAjaxParam("search_subdistrict_id", $('.select_subdistrict').val());
        grid.init({
            src: table,
            onSuccess: function(grid) {},
            onError: function(grid) {},
            dataTable: {  // here you can define a typical datatable settings from http://datatables.net/usage/options 
                "aLengthMenu": [
                    [10, 20, 50, 100, -1],
                    [10, 20, 50, 100, "All"]                        // change per page values here
                ],
                "iDisplayLength": 10,                               // default record count per page
                "bServerSide": true,                                // server side processing
                "sAjaxSource": url,                                 // ajax source
                "aoColumnDefs": [
                  { 'bSortable': false, 'aTargets': [ -3, -2, -1, 0 ] }
               ]
            }
        });

        grid.getTableWrapper().on('click', '.filter-submit', function(e){
            e.preventDefault();
            grid.addAjaxParam("search_province_id", $('.select_province').val());
            grid.addAjaxParam("search_district_id", $('.select_district').val());
            grid.addAjaxParam("search_subdistrict_id", $('.select_subdistrict').val());

            // get all typeable inputs
            $('textarea.form-filter, select.form-filter, input.form-filter:not([type="radio"],[type="checkbox"])').each(function(){
                grid.addAjaxParam($(this).attr("name"), $(this).val());
            });

            grid.getDataTable().fnDraw();
            grid.clearAjaxParams();
        });

        grid.getTableWrapper().on('click', '.filter-clear', function(e){
            e.preventDefault();
            $('textarea.form-filter, select.form-filter, input.form-filter').each(function(){
                if ( $(this).attr("name") !== 'search_sponsor') {
                    $(this).val("");
                }
            });
            $('input.form-filter[type="checkbox"]').each(function(){
                $(this).attr("checked", false);
            });   

            grid.getDataTable().fnDraw();
            grid.clearAjaxParams();
        });

        $("body").delegate( ".form-control", "keypress", function( e ) {
            var key = e.which;
            if(key == 13){ 
                $('#btn-find-agent').trigger('click');
                return false; 
            }
        });

        $("body").delegate( "#btn-find-agent", "click", function( e ) {
            grid.getTableWrapper().find('.filter-submit').click();
        });
    };

    return {
        //main function to initiate the module
        init: function () {
            handleRecordsFindAgent();
        }
    };
}();

// =========================================================================
// Data PIN List Function
// =========================================================================
var TableAjaxPINList = function () {
    var handleRecordsPINMemberActiveList = function() {
        gridTable( $("#list_table_pin_member_active"), false );
    };
    var handleRecordsPINUsedList = function() {
        gridTable( $("#list_table_pin_used"), false );
    };
    var handleRecordsPINStatusList = function() {
        gridTable( $("#list_table_pin_status"), false );
    };
    var handleRecordsPINMemberList = function() {
        gridTable( $("#list_table_pin_member"), false );
    };

    return {
        //main function to initiate the module
        init: function () {
            initPickers();
            handleRecordsPINMemberActiveList();
            handleRecordsPINUsedList();
            handleRecordsPINStatusList();
            handleRecordsPINMemberList();
        }
    };
}();

// =========================================================================
// PIN Order List Function
// =========================================================================
var TableAjaxPINOrderList = function () {
    var handleRecordsPINOrderList = function() {
        gridTable( $("#list_table_pin_order"), false );
    };

    return {
        //main function to initiate the module
        init: function () {
            initPickers();
            handleRecordsPINOrderList();
        }
    };
}();

// =========================================================================
// Product Manage List Function
// =========================================================================
var TableAjaxProductManageList = function () {
    var handleRecordsProductManageList = function() {
        gridTable( $("#list_table_product"), false );
    };

    var handleRecordsProductCategoryList = function() {
        gridTable( $("#list_table_category"), false );
    };

    var handleRecordsProductPointList = function() {
        gridTable( $("#list_table_product_point"), false );
    };

    return {
        //main function to initiate the module
        init: function () {
            handleRecordsProductManageList();
            handleRecordsProductCategoryList();
            handleRecordsProductPointList();
        }
    };
}();

// =========================================================================
// Promo Code List Function
// =========================================================================
var TableAjaxPromoCodeList = function () {
    var handleRecordsPromoCodeList = function() {
        gridTable( $("#list_table_promo_code"), false );
    };

    return {
        //main function to initiate the module
        init: function () {
            initPickers();
            handleRecordsPromoCodeList();
        }
    };
}();

// =========================================================================
// Commission List Function
// =========================================================================
var TableAjaxCommissionList = function () {
    var handleRecordsTotalBonusList = function() {
        gridTable( $("#list_table_total_bonus"), false );
    };
    var handleRecordsHistoryBonusList = function() {
        gridTable( $("#list_table_history_bonus"), false );
    };

    return {
        //main function to initiate the module
        init: function () {
            initPickers();
            handleRecordsTotalBonusList();
            handleRecordsHistoryBonusList();
        }
    };
}();

// =========================================================================
// Deposite List Function
// =========================================================================
var TableAjaxDepositeList = function () {
    var handleRecordsDepositeList = function() {
        gridTable( $("#list_table_deposite"), false );
    };

    return {
        //main function to initiate the module
        init: function () {
            initPickers();
            handleRecordsDepositeList();
        }
    };
}();

// =========================================================================
// Commission List Function
// =========================================================================
var TableAjaxCommissionssList = function () {
    var handleRecordsTotalCommissionList = function() {
        var table       = $("#list_table_total_commission");
        if ( table.length ) {
            var url         = table.data('url');
            var grid        = new Datatable();
            grid.addAjaxParam("search_startdate", $('input[name=search_startdate]').val());
            grid.addAjaxParam("search_enddate", $('input[name=search_enddate]').val());
            grid.init({
                src: table,
                onSuccess: function(grid) {},
                onError: function(grid) {},
                dataTable: {  // here you can define a typical datatable settings from http://datatables.net/usage/options 
                    "aLengthMenu": [
                        [10, 20, 50, 100, -1],
                        [10, 20, 50, 100, "All"]                        // change per page values here
                    ],
                    "iDisplayLength": 10,                               // default record count per page
                    "bServerSide": true,                                // server side processing
                    "sAjaxSource": url,                                 // ajax source
                    "aoColumnDefs": [
                      { 'bSortable': false, 'aTargets': [ -1, 0 ] }
                   ]
                }
            });

            grid.getTableWrapper().on('click', '.filter-search', function(e){
                e.preventDefault();
                grid.addAjaxParam("search_startdate", $('input[name=search_startdate]').val());
                grid.addAjaxParam("search_enddate", $('input[name=search_enddate]').val());

                // get all typeable inputs
                $('textarea.form-filter, select.form-filter, input.form-filter:not([type="radio"],[type="checkbox"])', table).each(function(){
                    grid.addAjaxParam($(this).attr("name"), $(this).val());
                });

                grid.getDataTable().fnDraw();
                grid.clearAjaxParams();
            });

            grid.getTableWrapper().on('click', '.filter-clear', function(e){
                e.preventDefault();
                grid.addAjaxParam("search_startdate", $('input[name=search_startdate]').val());
                grid.addAjaxParam("search_enddate", $('input[name=search_enddate]').val());
                $('textarea.form-filter, select.form-filter, input.form-filter', table).each(function(){
                    $(this).val("");
                });

                grid.getDataTable().fnDraw();
                grid.clearAjaxParams();
            });

            $("body").delegate( "#btn-search-period-commission", "click", function( event ) {
                event.preventDefault();
                $('#btn_list_table_total_commission').trigger('click');
            });
        }

        $("body").delegate( "#btn-search-period-commission-detail", "click", function( event ) {
            event.preventDefault();
            var url         = $(this).data('url');
            var startdate   = $('input[name=search_startdate]').val();
            var enddate     = $('input[name=search_enddate]').val();
            var url_direct  = url +'?daterange='+ startdate + '|' + enddate;
            $(location).attr('href', url_direct);
        });
    };

    return {
        //main function to initiate the module
        init: function () {
            initPickers();
            handleRecordsTotalCommissionList();
        }
    };
}();

// =========================================================================
// Withdraw List Function
// =========================================================================
var TableAjaxWithdrawList = function () {
    var handleRecordsWithdrawList = function() {
        if ( $("#list_table_withdraw").length ) {
            gridTable( $("#list_table_withdraw"), false );
        }
        if ( $("#list_table_withdraw_total").length && $('#load_withdraw_total').length ) {
            var load_total = $('#load_withdraw_total').val();
            if ( load_total == 1 ) {
                gridTable( $("#list_table_withdraw_total"), false );
            } 
        }

        $('body').delegate('.btn_withdraw_status', 'click', function(){
            var status_order = $(this).data('status');
            if ( status_order == 'withdraw' ) {
                var btn_search = $('#btn_list_table_withdraw');
                if ( btn_search.length ) {
                    btn_search.trigger('click');
                }
            }
            if ( status_order == 'total_withdraw' ) {
                var btn_search = $('#btn_list_table_withdraw_total');
                var load_total = $('#load_withdraw_total');
                if ( btn_search.length && load_total.length ) {
                    var input_load = load_total.val();
                    if ( input_load == 1 ) {
                        btn_search.trigger('click');
                    } else {
                        if ( $("#list_table_withdraw_total").length ) {
                            gridTable( $("#list_table_withdraw_total"), false );
                        }
                        load_total.val(1)
                    }
                }
            }
        });
    };

    return {
        //main function to initiate the module
        init: function () {
            initPickers();
            handleRecordsWithdrawList();
        }
    };
}();

// =========================================================================
// Flip List Function
// =========================================================================
var TableAjaxFlipList = function () {
    var handleRecordsFliptrxList = function() {
        gridTable( $("#list_table_flip_trx"), false, [ -2, -1, 0 ] );
    };
    var handleRecordsFlipTopupList = function() {
        gridTable( $("#list_table_flip_topup"), false );
    };
    var handleRecordsFlipInquiryList = function() {
        gridTable( $("#list_table_flip_inquiry"), false );
    };

    return {
        //main function to initiate the module
        init: function () {
            initPickers();
            handleRecordsFliptrxList();
            handleRecordsFlipTopupList();
            handleRecordsFlipInquiryList();
        }
    };
}();

// =========================================================================
// Shop Order Product List Function
// =========================================================================
var TableAjaxShopOrderList = function () {
    
    var handleRecordsShopList = function() {
        if ( $("#list_table_shop_history").length ) {
            gridTable( $("#list_table_shop_history"), false );
        }
        if ( $("#list_table_shop_stockist").length ) {
            gridTable( $("#list_table_shop_stockist"), false );
        }
    };

    var handleRecordsShopOrderList = function() {
        // Table
        var table_pending   = $("#list_table_shop_pending");
        var table_confirm   = $("#list_table_shop_confirmed");
        var table_done      = $("#list_table_shop_done");
        var table_cancel    = $("#list_table_shop_cancelled");

        // Load
        var load_pending    = $('#load_shop_pending');
        var load_confirm    = $('#load_shop_confirmed');
        var load_done       = $('#load_shop_done');
        var load_cancel     = $('#load_shop_cancel');

        if ( table_pending.length && load_pending.length ) {
            const val_pending = load_pending.val();
            if ( val_pending == 1 ) {
                gridTable( table_pending, false );
            } 
        }

        if ( table_confirm.length && load_confirm.length ) {
            const val_confirm = load_confirm.val();
            if ( val_confirm == 1 ) {
                gridTable( table_confirm, false );
            } 
        }

        if ( table_done.length && load_done.length ) {
            const val_done = load_done.val();
            if ( val_done == 1 ) {
                gridTable( table_done, false );
            } 
        }

        if ( table_cancel.length && load_cancel.length ) {
            const val_cancel = load_cancel.val();
            if ( val_cancel == 1 ) {
                gridTable( table_cancel, false );
            } 
        }

        $('body').delegate('.btn_shop_order_status', 'click', function(){
            var status_order    = $(this).data('status');
            if ( status_order == 'pending' ) {
                if ( table_pending.length && load_pending.length ) {
                    const val_pending   = load_pending.val();
                    const btn_search    = table_pending.find('#btn_list_table_shop_pending');
                    if ( val_pending == 1 ) {
                        btn_search.trigger('click');
                    } else {
                        gridTable( table_pending, false );
                        load_pending.val(1);
                    }
                }
            }

            if ( status_order == 'confirmed' ) {
                if ( table_confirm.length && load_confirm.length ) {
                    const val_confirm   = load_confirm.val();
                    const btn_search    = table_confirm.find('#btn_list_table_shop_confirmed');
                    if ( val_confirm == 1 ) {
                        btn_search.trigger('click');
                    } else {
                        gridTable( table_confirm, false );
                        load_confirm.val(1);
                    }
                }
            }

            if ( status_order == 'done' ) {
                if ( table_done.length && load_done.length ) {
                    const val_done      = load_done.val();
                    const btn_search    = table_done.find('#btn_list_table_shop_done');
                    if ( val_done == 1 ) {
                        btn_search.trigger('click');
                    } else {
                        gridTable( table_done, false );
                        load_done.val(1);
                    }
                }
            }

            if ( status_order == 'cancelled' ) {
                if ( table_cancel.length && load_cancel.length ) {
                    const val_cancel    = load_cancel.val();
                    const btn_search    = table_cancel.find('#btn_list_table_shop_cancelled');
                    if ( val_cancel == 1 ) {
                        btn_search.trigger('click');
                    } else {
                        gridTable( table_cancel, false );
                        load_cancel.val(1);
                    }
                }
            }
        });
    };

    var handleRecordsShopCustomerList = function() {
        // Table
        var table_pending   = $("#list_table_shop_customer_pending");
        var table_confirm   = $("#list_table_shop_customer_confirmed");
        var table_done      = $("#list_table_shop_customer_done");
        var table_cancel    = $("#list_table_shop_customer_cancelled");

        // Load
        var load_pending    = $('#load_shop_customer_pending');
        var load_confirm    = $('#load_shop_customer_confirmed');
        var load_done       = $('#load_shop_customer_done');
        var load_cancel     = $('#load_shop_customer_cancel');

        if ( table_pending.length && load_pending.length ) {
            const val_pending = load_pending.val();
            if ( val_pending == 1 ) {
                gridTable( table_pending, false );
            } 
        }

        if ( table_confirm.length && load_confirm.length ) {
            const val_confirm = load_confirm.val();
            if ( val_confirm == 1 ) {
                gridTable( table_confirm, false );
            } 
        }

        if ( table_done.length && load_done.length ) {
            const val_done = load_done.val();
            if ( val_done == 1 ) {
                gridTable( table_done, false );
            } 
        }

        if ( table_cancel.length && load_cancel.length ) {
            const val_cancel = load_cancel.val();
            if ( val_cancel == 1 ) {
                gridTable( table_cancel, false );
            } 
        }

        $('body').delegate('.btn_shop_customer_status', 'click', function(){
            var status_order    = $(this).data('status');
            if ( status_order == 'pending' ) {
                if ( table_pending.length && load_pending.length ) {
                    const val_pending   = load_pending.val();
                    const btn_search    = table_pending.find('#btn_list_table_shop_customer_pending');
                    if ( val_pending == 1 ) {
                        btn_search.trigger('click');
                    } else {
                        gridTable( table_pending, false );
                        load_pending.val(1);
                    }
                }
            }

            if ( status_order == 'confirmed' ) {
                if ( table_confirm.length && load_confirm.length ) {
                    const val_confirm   = load_confirm.val();
                    const btn_search    = table_confirm.find('#btn_list_table_shop_customer_confirmed');
                    if ( val_confirm == 1 ) {
                        btn_search.trigger('click');
                    } else {
                        gridTable( table_confirm, false );
                        load_confirm.val(1);
                    }
                }
            }

            if ( status_order == 'done' ) {
                if ( table_done.length && load_done.length ) {
                    const val_done      = load_done.val();
                    const btn_search    = table_done.find('#btn_list_table_shop_customer_done');
                    if ( val_done == 1 ) {
                        btn_search.trigger('click');
                    } else {
                        gridTable( table_done, false );
                        load_done.val(1);
                    }
                }
            }

            if ( status_order == 'cancelled' ) {
                if ( table_cancel.length && load_cancel.length ) {
                    const val_cancel    = load_cancel.val();
                    const btn_search    = table_cancel.find('#btn_list_table_shop_customer_cancelled');
                    if ( val_cancel == 1 ) {
                        btn_search.trigger('click');
                    } else {
                        gridTable( table_cancel, false );
                        load_cancel.val(1);
                    }
                }
            }
        });
    };

    return {
        //main function to initiate the module
        init: function () {
            initPickers();
            handleRecordsShopList();
            handleRecordsShopOrderList();
            handleRecordsShopCustomerList();
        }
    };
}();

// =========================================================================
// Reward List Function
// =========================================================================
var TableAjaxOmzetList = function () {
    var handleRecordsOmzetDailyList = function() {
        gridTable( $("#list_table_omzet_daily"), false );
    };

    var handleRecordsOmzetMonthlyList = function() {
        gridTable( $("#list_table_omzet_monthly"), false );
    };

    return {
        //main function to initiate the module
        init: function () {
            initPickers();
            handleRecordsOmzetDailyList();
            handleRecordsOmzetMonthlyList();
        }
    };
}();

// =========================================================================
// Reward List Function
// =========================================================================
var TableAjaxRewardList = function () {
    var handleRecordsRewardList = function() {
        gridTable( $("#list_table_reward"), false );
    };

    return {
        //main function to initiate the module
        init: function () {
            initPickers();
            handleRecordsRewardList();
        }
    };
}();

// =========================================================================
// Rank Qualification List Function
// =========================================================================
var TableAjaxQualificationList = function () {
    var handleRecordsQualificationList = function() {
        gridTable( $("#list_table_rank_qualification"), false );
    };

    return {
        //main function to initiate the module
        init: function () {
            initPickers();
            handleRecordsQualificationList();
        }
    };
}();

// =========================================================================
// Rank Qualification List Function
// =========================================================================
var TableAjaxWaybillList = function () {
    var handleRecordsWaybillList = function() {
        gridTable( $("#list_table_waybill"), false );
    };

    return {
        //main function to initiate the module
        init: function () {
            initPickers();
            handleRecordsWaybillList();
        }
    };
}();

var TableAjaxPickupList = function () {
    var handleRecordsPickupList = function() {
        gridTable( $("#list_table_pickup"), false );
    };

    return {
        //main function to initiate the module
        init: function () {
            initPickers();
            handleRecordsPickupList();
        }
    };
}();

// =========================================================================
// Setting Staff List Function
// =========================================================================
var TableAjaxStaffList = function () {
    var handleRecordsStaffList = function() {
        gridTable( $("#list_table_staff") );
    };

    return {
        //main function to initiate the module
        init: function () {
            handleRecordsStaffList();
        }
    };
}();

// =========================================================================
// Setting Notification List Function
// =========================================================================
var TableAjaxNotifList = function () {
    var handleRecordsNotificationList = function() {
        gridTable( $("#notification_list") );
    };

    return {
        //main function to initiate the module
        init: function () {
            handleRecordsNotificationList();
        }
    };
}();

// =========================================================================
// Setting Stages List Function
// =========================================================================
var TableAjaxSettingStagesList = function () {
    var handleRecordsSettingStagesList = function() {
        gridTable( $("#list_table_setting_stages") );

        // Even Delete
        $('body').delegate('.btn-delete-stages','click', function(e){
            e.preventDefault();
            var url         = $(this).data('url');
            var year        = $(this).data('year');
            var stage       = $(this).data('stage');
            var substage    = $(this).data('substage');
            var msg_body    = `
                <h4 class="pt-4 pb-3 text-center">Apakah anda yakin akan menghapus data ini ?</h4>
                <div class="row">
                    <div class="col-3"><small class="heading-small text-muted font-weight-bold">Tahun</small></div>
                    <div class="col-9"><small class="heading-small font-weight-bold">: ${year} </small></div>
                </div>
                <div class="row">
                    <div class="col-3"><small class="heading-small text-muted font-weight-bold">Tahap</small></div>
                    <div class="col-9"><small class="heading-small font-weight-bold">: ${stage} </small></div>
                </div>
                <div class="row">
                    <div class="col-3"><small class="heading-small text-muted font-weight-bold">SubTahap</small></div>
                    <div class="col-9"><small class="heading-small font-weight-bold">: ${substage} </small></div>
                </div>
                `;

            if ( $('.alert').length ) {
                $('.alert').remove();
            }

            if ( url ) {
                bootbox.confirm(msg_body, function(result) {
                    if(result  === true){
                        $.ajax({
                            type:   "POST",
                            url:    url,
                            beforeSend: function (){
                                App.run_Loader('roundBounce');
                            },
                            success: function( response ){
                                App.close_Loader();
                                response = $.parseJSON(response);

                                if( response.token ){
                                    App.kdToken(response.token);
                                }
                                
                                if( response.status == 'access_denied' ){
                                    $(location).attr('href',response.url);
                                }else{
                                    if( response.status == 'success'){
                                        $('#btn_list_table_setting_stages').trigger('click');
                                        App.notify({
                                            icon: 'fa fa-check-circle', 
                                            message: response.message, 
                                            type: 'success',
                                        });
                                    }else{
                                        App.notify({
                                            icon: 'fa fa-exclamation-triangle', 
                                            message: response.message, 
                                            type: 'danger',
                                        });
                                    }
                                }
                            },
                            error: function( jqXHR, textStatus, errorThrown ) {
                                App.close_Loader();
                                bootbox.alert('Terjadi kesalahan sistem! Ulangi proses beberapa saat lagi.', function(){ 
                                    location.reload();
                                });
                            }
                        });
                    }
                });
            }
        });

        // Event Update
        $('body').delegate('.btn-status-setting-stages', 'click', function(e){
            e.preventDefault();
            var url         = $(this).data('url');
            var year        = $(this).data('year');
            var stage       = $(this).data('stage');
            var substage    = $(this).data('substage');
            var status      = $(this).data('status');
            var _modal      = $('#modal-status-stages');
            var _form       = $('#form-status-stages');

            if ( url && _modal.length && _form.length ) {
                _form.attr('action', url);
                $('#modal_year', _form).val(year);
                $('#modal_stage', _form).val(stage);
                $('#modal_substage', _form).val(substage);
                $('#modal_status', _form).val(status);
                _modal.modal('show');
                if ( $('.alert').length ) {
                    $('.alert').remove();
                }
            }
            return false;
        });

        // Form Status Submit
        $( "#form-status-stages" ).submit(function( e ) {
            e.preventDefault();
            var url     = $(this).attr('action');
            var data    = $(this).serialize();
            var _modal  = $('#modal-status-stages');

            _modal.modal('hide');
            if ( $('.alert').length ) {
                $('.alert').remove();
            }
            bootbox.confirm("Apakah anda yakin akan edit status data ini ?", function(result) {
                if( result == true ){
                    $.ajax({
                        type:   "POST",
                        url:    url,
                        data:   data,
                        beforeSend: function (){
                            App.run_Loader('roundBounce');
                        },
                        success: function( response ){
                            App.close_Loader();
                            response = $.parseJSON(response);

                            if( response.token ){
                                App.kdToken(response.token);
                            }
                            
                            if( response.status == 'access_denied' ){
                                $(location).attr('href',response.url);
                            }else{
                                if( response.status == 'success'){
                                    var _icon = "fa fa-check-circle";
                                    var _type = "success";
                                    $('#btn_list_table_setting_stages').trigger('click');
                                }else{
                                    var _icon = "fa fa-exclamation-triangle";
                                    var _type = "danger";
                                    _modal.modal('show');
                                }
                                App.notify({
                                    message: response.message, 
                                    icon: _icon, 
                                    type: _type,
                                });
                            }
                        },
                        error: function( jqXHR, textStatus, errorThrown ) {
                            App.close_Loader();
                            bootbox.alert('Terjadi kesalahan sistem! Ulangi proses beberapa saat lagi.', function(){ 
                                location.reload();
                            });
                        }
                    });
                }
            });
        });
    };

    return {
        //main function to initiate the module
        init: function () {
            handleRecordsSettingStagesList();
        }
    };
}();
