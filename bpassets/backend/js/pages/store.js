var _limit          = 12;
var _offset         = 0;
var _totalRecords   = 0;
var _formSearch     = $( ".form-search-store-product" );

var handleGeneralQtyProduct = function(){
    //------------- DETAIL ADD - MINUS COUNT ORDER -------------//
    $('.btn-number').click(function(e){
        e.preventDefault();
        fieldName = $(this).attr('data-field');
        type      = $(this).attr('data-type');
        var input = $("input[name='"+fieldName+"']");
        var currentVal = parseInt(input.val());
        if (!isNaN(currentVal)) {
            if(type == 'minus') {
                
                if(currentVal > input.attr('data-min')) {
                    input.val(currentVal - 1).change();
                } 
                if(parseInt(input.val()) == input.attr('data-min')) {
                    $(this).attr('disabled', true);
                }

            } else if(type == 'plus') {

                if(currentVal < input.attr('data-max')) {
                    input.val(currentVal + 1).change();
                }
                if(parseInt(input.val()) == input.attr('data-max')) {
                    $(this).attr('disabled', true);
                }

            }
        } else {
            input.val(0);
        }
    });

    $('.input-number').focusin(function(){
       $(this).data('oldValue', $(this).val());
    });

    $('.input-number').change(function() {
        minValue =  parseInt($(this).attr('data-min'));
        maxValue =  parseInt($(this).attr('data-max'));
        valueCurrent = parseInt($(this).val());
        
        name = $(this).attr('name');
        if(valueCurrent >= minValue) {
            $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
        } else {
            StoreApp.notify({
                icon: 'fa fa-exclamation-triangle', 
                message: 'Minimal Qty harus '+ minValue + ' item', 
                type: 'warning',
            });
            $(this).val($(this).data('oldValue'));
        }
        if(valueCurrent <= maxValue) {
            $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
        } else {
            StoreApp.notify({
                icon: 'fa fa-exclamation-triangle', 
                message: 'Maksimal Qty harus '+ maxValue + ' item', 
                type: 'warning',
            });
            $(this).val($(this).data('oldValue'));
        }
    });

    $(".input-number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    // Show / Hide Password
    // -----------------------------------------------
    $("body").delegate( ".btn-password", "click", function( event ) {
        event.preventDefault();
        var parent  = $(this).parent();
        var icon    = $(this).children();
        if ( ! parent.length ) { return; }
        if ( ! icon.length ) { return; }

        var input   = parent.children('input');
        if ( ! input.length ) { return; }
        var type    = input.attr('type');
        if (type === "password") {
            type = "text";
            icon.removeClass('fa-eye-slash');
            icon.addClass('fa-eye');
        } else {
            type = "password";
            icon.removeClass('fa-eye');
            icon.addClass('fa-eye-slash');
        }
        input.attr('type',type);
        return;
    });

    $("input.phonenumber").keyup(function () {
        if (this.value.substring(0, 1) == "0") {
            this.value = this.value.replace(/^0+/g, "");
        }
    });
};

var handleLoadDataStoreProduct = function(){
    var el          = $('#product-shop-list')
    var url         = el.data( 'url' );
    var product     = $( '.search_product').val();
    var category    = $( '.search_category').val();
    var sortby      = $( '.search_sortby').val();
    var btn_loader  = $(".see-more-loading"); 

    if ( el && url ) {
        var h = el.height(); 
        if ( h > 0 ) {
            h = h - 50;
        }
        $.ajax({
            type: "POST",
            url: url,
            data: {
                'limit': _limit,
                'offset': _offset,
                'search': product,
                'category': category,
                'sortby': sortby
            },
            beforeSend: function (){
                // StoreApp.run_Loader('timer');
                if ( btn_loader.length ) {
                    btn_loader.show();
                }
            },
            success: function( response ){
                // StoreApp.close_Loader();     
                response = $.parseJSON(response);

                if( response.status == 'access_denied' ){
                    $(location).attr('href',response.url);
                }

                if ( response.token ) {
                    // StoreApp.kdToken(response.token);
                }

                _limit          = response.displayLimit;
                _offset         = response.displayStart;
                _totalRecords   = response.totalRecords;
                if ( $('.shopping-see-more').length ) {
                    if ( response.totalRecords > response.totalDisplayRecords ) {
                        $('.shopping-see-more').show();
                    } else {
                        $('.shopping-see-more').hide();
                    }
                }
                if ( $('.product-found').length ) {
                    if ( _totalRecords > 0 ) {
                        $('.product-found').html('<b>'+ _totalRecords +'</b> Produk ditemukan');
                    } else {
                        $('.product-found').text('Produk tidak ditemukan');
                    }
                }

                el.append(response.displayHTML);
                if ( btn_loader.length ) {
                    btn_loader.hide();
                }
                // StoreApp.scrollTo(el, h);
            },
            error: function( jqXHR, textStatus, errorThrown ) {
                StoreApp.notify({
                    icon: 'fa fa-exclamation-triangle', 
                    message: 'Terjadi kesalahan sistem! Ulangi proses beberapa saat lagi.', 
                    type: 'danger',
                });
            }
        });
    }
    return false;
};

// ===========================================================
// Store General
// ===========================================================
var handleGeneralStoreProduct = function(){
    // Button See More
    // -----------------------------------------------
    $("body").delegate( ".btn-see-more", "click", function( e ) {
        e.preventDefault();
        handleLoadDataStoreProduct();
    });

    // Button Product Detail
    // -----------------------------------------------
    $("body").delegate( ".btn-product-detail", "click", function( e ) {
        e.preventDefault();
        var url             = $(this).data('url');
        var modal_detail    = $('#modal-store-product-detail');
        var el_detail       = $('.info-store-product-detail', modal_detail);

        $.ajax({
            type:   "POST",
            url:    url,
            beforeSend: function (){
                el_detail.empty();
                StoreApp.run_Loader();
            },
            success: function( response ){
                StoreApp.close_Loader();
                response = $.parseJSON(response);
                if( response.status == 'access_denied' ){
                    $(location).attr('href',response.url);
                }else{
                    if( response.status == 'success'){
                        el_detail.html(response.data);
                        modal_detail.modal('show');
                        handleGeneralQtyProduct();
                        $('.quickview-slider-active').owlCarousel({
                            items:1,
                            autoplay:true,
                            autoplayTimeout:5000,
                            smartSpeed: 400,
                            autoplayHoverPause:true,
                            nav:true,
                            loop:true,
                            merge:true,
                            dots:false,
                            navText: ['<i class=" ti-arrow-left"></i>', '<i class=" ti-arrow-right"></i>'],
                        });
                    }else{
                        StoreApp.notify({
                            icon: 'fa fa-exclamation-triangle', 
                            title: 'Failed', 
                            message: response.message, 
                            type: 'danger',
                        });
                    }
                }
            },
            error: function( jqXHR, textStatus, errorThrown ) {
                StoreApp.notify({
                    icon: 'fa fa-exclamation-triangle', 
                    title: 'Failed', 
                    message: 'Terjadi kesalahan sistem! Ulangi proses beberapa saat lagi.', 
                    type: 'danger',
                });
            }
        });
    });

    // Search Product
    // -----------------------------------------------
    $("body").delegate( ".search-product-top, .search-product-main", "blur", function( e ) {
        e.preventDefault();
        var search      = $(this).val();
        var product     = $(this).data('product');
        var s_product   = true;
        if ( search == product ) {
            s_product   = false;
        }

        $('.search_product').val(search);

        if ( s_product && _formSearch.length ) {
            _formSearch.submit();
        }
        return false;
    });

    $("body").delegate( ".search-product-top, .search-product-main", "keypress", function( e ) {
        // e.preventDefault();
        var search      = $(this).val();
        var product     = $(this).data('product');
        var s_product   = true;
        if ( search == product ) {
            s_product   = false;
        }

        if( e.which == 13 ) {
            $('.search_product').val(search);
            if ( s_product && _formSearch.length ) {
                _formSearch.submit();
            } else {
                return false;
            }
        }
        return true;
    });

    // Search Category
    // -----------------------------------------------
    $("body").delegate( ".search_category", "change", function( e ) {
        e.preventDefault();
        if ( _formSearch.length ) {
            _formSearch.submit();
        }
    });

    // Search Category
    // -----------------------------------------------
    $("body").delegate( ".search_sortby", "change", function( e ) {
        e.preventDefault();
        if ( _formSearch.length ) {
            _formSearch.submit();
        }
    });

    _formSearch.submit(function( event ) {
        event.preventDefault();
        var url         = $(this).data( 'url' );
        var product     = $( '.search_product').val();
        var category    = $( '.search_category').val();
        var sortby      = $( '.search_sortby').val();
        var data        = {
            'search': product,
            'category': category,
            'sortby': sortby
        };

        var page_url    = url +"?"+ $.param(data);
        $(location).attr('href', page_url);
    });

    // Add To Cart
    // -----------------------------------------------
    $("body").delegate( "a.add-to-cart", "click", function( e ) {
        e.preventDefault();
        var el          = $(this);
        var url         = $(this).attr('href');
        var product     = $(this).data('cart');
        var type        = $(this).data('type');  
        var el_qty      = $(this).data('qty');  
        var qty         = 1;  

        if ( type == 'cart' ) { 
            $(location).attr('href', url);
            return false;
        }

        if ( el_qty ) { 
            var input = $("input[name='"+el_qty+"']");
            if ( input.length ) {
                qty = input.val();
            }
        }

        if ( product && type == 'addcart' ) {
            $.ajax({
                url: url,
                type: "POST",
                data: { 'id': product, 'qty': qty },
                beforeSend: function (){
                    // StoreApp.run_Loader('timer');
                    el.addClass('btn-outline-default').addClass('disabled');
                    $('.shopping-cart-loading', el).show();
                },
                success: function( response ){
                    // StoreApp.close_Loader();     
                    response = $.parseJSON(response);
                    $('.shopping-cart-loading', el).hide();
                    el.removeClass('disabled');
                    el.removeClass('btn-outline-default');

                    if( response.status == 'access_denied' ){
                        $(location).attr('href',response.url);
                    }

                    if ( response.token ) {
                        StoreApp.kdToken(response.token);
                    }

                    if( response.status == 'success'){
                        var type = 'success';
                        var icon = 'fa fa-check';
                        el.text('Go to cart');
                        el.attr('href', response.url_cart);
                        el.removeData('cart');
                        el.data('type', 'cart');
                        if ( $('#cart-total-menu').length ) {
                            $('#cart-total-menu').text(response.total_qty);
                        }
                        if ( $('#cart-total-item').length ) {
                            $('#cart-total-item').text(response.total_item);
                        }
                        if ( $('.shopping-list', '.menu-cart').length ) {
                            $('.shopping-list', '.menu-cart').append(response.html_menu);
                        } else {
                            if ( $('.menu-cart').length ) {
                                $('.menu-cart').append(response.html_menu);
                            }
                        }
                        if ( $('.cart-total-paymnet').length ) {
                            $('.cart-total-paymnet').html(response.total_cart);
                        }

                        if ( $('.modal-detail-qty').length ) {
                             $('.modal-detail-qty').hide();
                        }
                    }else{
                        var type = 'danger';
                        var icon = 'fa fa-exclamation-triangle';
                    }
                    StoreApp.notify({
                        icon: icon, 
                        message: response.message, 
                        type: type,
                    });
                    // StoreApp.scrollTo(el, 0);
                },
                error: function( jqXHR, textStatus, errorThrown ) {
                    // StoreApp.close_Loader();
                    StoreApp.notify({
                        icon: 'fa fa-exclamation-triangle', 
                        message: 'Terjadi kesalahan sistem. Silahkan reload page!', 
                        type: 'danger',
                    });
                }
            });          
        }
        return false;
    });

    handleGeneralQtyProduct();
}();

// ===========================================================
// Store Product
// ===========================================================
var StoreProduct = function() {
    return {
        init: function() {
            handleLoadDataStoreProduct();
        }
    };
}();

// ===========================================================
// Store Cart
// ===========================================================
var StoreCart = function() {
    var handleGeneralShoppingCart = function(){
        // Change Minus Qty Product 
        // -----------------------------------------------
        $("body").delegate( ".btn-cart-minus-qty", "click", function( e ) {
            e.preventDefault();
            var step    = $(this).data('step');
            var count   = $(this).closest(".product-quantity").find('.cart-item-qty').val();
            var countEl = $(this).closest(".product-quantity").find('.cart-item-qty');

            if ( parseInt(count) > parseInt(step) ) {
                count = parseInt(count) - parseInt(step);
                countEl.val(count).change();
            }
        });

        // Change Plus Qty Product 
        // -----------------------------------------------
        $("body").delegate( ".btn-cart-plus-qty", "click", function( e ) {
            e.preventDefault();
            var step    = $(this).data('step');
            var count   = $(this).closest(".product-quantity").find('.cart-item-qty').val();
            var countEl = $(this).closest(".product-quantity").find('.cart-item-qty');

            count = parseInt(count) + parseInt(step);
            countEl.val(count).change();
        });

        // Change Qty Product 
        // -----------------------------------------------
        $("body").delegate( ".cart-item-qty", "change", function( e ) {
            e.preventDefault();
            var url         = $(this).data('url');
            var rowid       = $(this).data('rowid');
            var productid   = $(this).data('productid');
            var price       = $(this).data('price');
            var qty         = $(this).val();

            var priceCart   = $(this).closest(".cart_item").find('.cart-item-price');
            var totalCart   = $(this).closest(".cart_item").find('.cart-item-subtotal');

            if ( ! qty || qty == '' || qty == '0' || qty == 0 || qty == undefined ) {
                location.reload();
                return false
            }

            $.ajax({
                type: "POST",
                url: url,
                data: { 'rowid': rowid, 'productid': productid, 'qty': qty },
                beforeSend: function (){
                    // StoreApp.run_Loader();
                },
                success: function( response ){
                    // StoreApp.close_Loader();     
                    response = $.parseJSON(response);

                    if( response.status == 'access_denied' ){
                        $(location).attr('href',response.url);
                    }

                    if ( response.token ) {
                        StoreApp.kdToken(response.token);
                    }

                    if( response.status == 'success'){
                        if ( $('#qty_'+rowid).length ) {
                            $('#qty_'+rowid).text(qty);
                        }
                        if ( $('#cart-total-menu').length ) {
                            $('#cart-total-menu').text(response.total_qty);
                        }
                        priceCart.html(response.price_cart);
                        totalCart.html(response.subtotal_cart);
                        if ( $('.cart-total-paymnet').length ) {
                            $('.cart-total-paymnet').html(response.total_cart);
                        }
                    }else{
                        StoreApp.notify({
                            icon: 'fa fa-exclamation-triangle', 
                            message: response.message, 
                            type: 'danger',
                        });
                    }
                },
                error: function( jqXHR, textStatus, errorThrown ) {
                    // StoreApp.close_Loader();
                    StoreApp.notify({
                        icon: 'fa fa-exclamation-triangle', 
                        message: 'Terjadi kesalahan sistem! Ulangi proses beberapa saat lagi.',
                        type: 'danger',
                    });
                }
            });

            return false;
        });

        // Delete Product Cart 
        // -----------------------------------------------
        $("body").delegate( "a.btn-product-cart-delete", "click", function( e ) {
            e.preventDefault();
            var url         = $(this).attr('href');
            var rowid       = $(this).data("id");
            var tr_cart     = $(this).closest("tr.cart_item");

            if ( url ) {
                $.ajax({
                    type: "POST",
                    url: url,
                    beforeSend: function (){
                    },
                    success: function( response ){
                        response = $.parseJSON(response);

                        if( response.status == 'access_denied' ){
                            $(location).attr('href',response.url);
                        }

                        if ( response.token ) {
                            StoreApp.kdToken(response.token);
                        }

                        if( response.status == 'success'){
                            tr_cart.remove();
                            if ( $('.cart-total-paymnet').length ) {
                                $('.cart-total-paymnet').html(response.total_cart);
                            }
                            if ( $('#cart-total-menu').length ) {
                                $('#cart-total-menu').text(response.total_qty);
                            }
                            if ( $('#cart-total-item').length ) {
                                $('#cart-total-item').text('');
                                if ( response.total_item != 0 && response.total_item != '0' ) {
                                    $('#cart-total-item').text(response.total_item);
                                }
                            }
                            if( $('[data-id="'+rowid+'"]', '.shopping-list').length ) {
                                $('[data-id="'+rowid+'"]', '.shopping-list').remove();
                            }
                            if ( response.total_item == 0 && response.total_item == '0' ) {
                                location.reload();
                            }
                        }else{
                            StoreApp.notify({
                                icon: 'fa fa-exclamation-triangle', 
                                message: response.message, 
                                type: 'danger',
                            });
                        }
                    },
                    error: function( jqXHR, textStatus, errorThrown ) {
                        StoreApp.notify({
                            icon: 'fa fa-exclamation-triangle', 
                            message: 'Terjadi kesalahan sistem! Ulangi proses beberapa saat lagi.', 
                            type: 'danger',
                        });
                        setTimeout(function(){ location.reload(); }, 500);
                    }
                });
            }
            return false;
        });

        // Empty Cart 
        // -----------------------------------------------
        $("body").delegate( "a.btn-cart-empty", "click", function( e ) {
            e.preventDefault();
            var url         = $(this).attr('href');

            if ( url ) {
                $.ajax({
                    type: "POST",
                    url: url,
                    beforeSend: function (){
                        StoreApp.run_Loader('timer');
                    },
                    success: function( response ){
                        response = $.parseJSON(response);

                        if( response.status == 'access_denied' ){
                            $(location).attr('href',response.url);
                        }

                        if ( response.token ) {
                            StoreApp.kdToken(response.token);
                        }

                        if( response.status == 'success'){
                            location.reload();
                        }else{
                            StoreApp.close_Loader();     
                            StoreApp.notify({
                                icon: 'fa fa-exclamation-triangle', 
                                message: response.message, 
                                type: 'danger',
                            });
                        }
                    },
                    error: function( jqXHR, textStatus, errorThrown ) {
                        StoreApp.notify({
                            icon: 'fa fa-exclamation-triangle', 
                            message: 'Terjadi kesalahan sistem! Ulangi proses beberapa saat lagi.', 
                            type: 'danger',
                        });
                        setTimeout(function(){ location.reload(); }, 500);
                    }
                });
            }
            return false;
        });
    };
    
    return {
        init: function() {
            handleGeneralShoppingCart();
        }
    };
}();

// ===========================================================
// Store Checkout
// ===========================================================
var StoreCheckout = function() {

    var form            = $('#form-store-checkout');
    var wrapper         = $('#form-store-checkout');

    // ---------------------------------
    // Handle Validation
    // ---------------------------------
    var handleValidationStoreCheckout = function() {
        if ( ! form.length ) {
            return;
        }

        form.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'invalid-feedback', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                name: {
                    minlength: 2,
                    required: true,
                    lettersonly: true
                },
                phone: {
                    minlength: 8,
                    required: true
                },
                email: {
                    email: true,
                    required: true
                },
                province: {
                    required: true
                },
                district: {
                    required: true
                },
                subdistrict: {
                    required: true
                },
                village: {
                    required: true
                },
                address: {
                    required: true
                },
                courier: {
                    required: true
                },
                service: {
                    required: true
                },
            },
            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.parent(".input-group").length > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) { 
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.select-option').length > 0) { 
                    error.appendTo(element.parents('.select-option'));
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit              
                StoreApp.alert({
                    type: 'danger', 
                    icon: 'bell', 
                    message: 'Ada beberapa error, silahkan cek formulir di bawah!', 
                    container: wrapper, 
                    closeInSeconds: 5,
                    place: 'prepend'
                });
                StoreApp.scrollTo(wrapper, -100);
            },
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                $(element).closest('.form-group').find('#sponsor_info').empty();
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            submitHandler: function (form) {
                $('#modal-process-checkout').modal({backdrop: 'static', keyboard: false, show: true});
            }
        });
        
        $.validator.addMethod("pwcheck", function(value) {
            return /[a-z].*[0-9]|[0-9].*[a-z]/i.test(value); // consists of only these
        }, "Password harus terdiri dari huruf dan angka" );
        
        $.validator.addMethod("unamecheck", function(value) {
            return /^[A-Za-z0-9]{4,16}$/i.test(value);   // consists of only these
        }, "Username tidak memenuhi kriteria" );
        
        $.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
        }, "Silahkan inputkan Nama dengan huruf saja" );
    };

    // Handle Validation Register Member
    var setRuleValidateMember = function() {
        if( $("input[name='options_reg']").length ){
            if ( $("input[name='sponsor']").length ) {
                $("input[name='sponsor']").rules("remove");
            }
            if ( $("input[name='username']").length ) {
                $("input[name='username']").rules("remove");
            }
            if ( $("input[name='password']").length ) {
                $("input[name='password']").rules("remove");
            }
            if ( $("input[name='idcard']").length ) {
                $("input[name='idcard']").rules("remove");
            }

            if( $("input[name='options_reg']:checked").val() == "member" ){
                if ( $("input[name='sponsor']").length ) {
                    $("input[name='sponsor']").rules( "add", {
                        minlength: 5,
                        required: function(element) {
                            if( $("input[name='options_reg']:checked").val() == "member" ){
                                return true;
                            }else{
                                return false;
                            }
                        },
                        remote: {
                            url: $("#sponsor").data('url'),
                            type: "post",
                            data: {
                                [StoreApp.kdName()]: function() {
                                    return StoreApp.kdToken();
                                },
                                sponsor: function() {
                                    return $("#sponsor").prop( 'readonly' ) ? '' : $("#sponsor").val();
                                }
                            },
                            beforeSend: function (){ 
                                $("#sponsor").parent().append('<span class="spinner-border spinner-sponsor text-success"></span>');
                                if ( $("#sponsor_info").length ) {
                                    $("#sponsor_info").empty();
                                }
                            },
                            dataFilter: function(response) {
                                $('.spinner-sponsor').remove();
                                response = $.parseJSON(response);
                                if ( response.token ) {
                                    StoreApp.kdToken(response.token);
                                }
                                if ( response.info && $("#sponsor_info").length ) {
                                    $("#sponsor_info").html(response.info);
                                }
                                return response.status;
                            }
                        },
                        messages: {
                            remote: "Referral tidak ditemukan. Silahkan gunakan Referral lain"
                        }
                    });
                }
                if ( $("input[name='username']").length ) {
                    $("input[name='username']").rules( "add", {
                        minlength: 5,
                        required: function(element) {
                            if( $("input[name='options_reg']:checked").val() == "member" ){
                                return true;
                            }else{
                                return false;
                            }
                        },
                        unamecheck: function(element) {
                            if( $("input[name='options_reg']:checked").val() == "member" ){
                                return true;
                            }else{
                                return false;
                            }
                        },
                        remote: {
                            url: $("#username").data('url'),
                            type: "post",
                            data: {
                                [StoreApp.kdName()]: function() {
                                    return StoreApp.kdToken();
                                },
                                username: function() {
                                    return $("#username").prop( 'readonly' ) ? '' : $("#username").val();
                                }
                            },
                            beforeSend: function (){ 
                                $("#username").parent().append('<span class="spinner-border spinner-username text-success"></span>');
                            },
                            dataFilter: function(response) {
                                $('.spinner-username').remove();
                                response = $.parseJSON(response);
                                if ( response.token ) {
                                    StoreApp.kdToken(response.token);
                                }
                                return response.status;
                            }
                        },
                        messages: {
                            remote: "Username tidak dapat digunakan. Silahkan gunakan username lain"
                        }
                    });
                }
                if ( $("input[name='password']").length ) {
                    $("input[name='password']").rules( "add", {
                        minlength: 6,
                        required: true,
                        pwcheck: true,
                    });
                }
                if ( $("input[name='idcard']").length ) {
                    $("input[name='idcard']").rules( "add", {
                        minlength: 16,
                        maxlength: 16,
                        required: true,
                    });
                }
            }
        }
        return false;
    };

    var handleGeneralStoreCheckout = function(){
        // Button Checkout 
        // -----------------------------------------------
        $("body").delegate( "a.btn-store-checkout", "click", function( e ) {
            e.preventDefault();
            form.submit();
            return false;
        });

        // Province Change
        // -----------------------------------------------
        $("body").delegate( ".select_province", "change", function( e ) {
            e.preventDefault();
            var val         = $(this).val();
            var url         = $(this).data('url');
            var el_dist     = $('.select_district');
            var el_subdist  = $('.select_subdistrict');
            var el_courier  = $('.select_courier');
            var el_service  = $('.select_service');

            if ( url ) {
                if ( el_courier.length ) {
                    el_courier.val('');
                }

                if ( el_service.length ) {
                    el_service.empty();
                }

                $.ajax({
                    type: "POST",
                    data: { 'province' : val },
                    url: url,
                    beforeSend: function (){ 
                        if ( el_dist.length ) {
                            el_dist.empty();
                            el_dist.parent().append('<span class="spinner-border spinner-district text-success"></span>');
                        }
                        if ( el_subdist.length ) {
                            el_subdist.empty();
                        }
                        $('select').niceSelect('update'); 
                    },
                    success: function( response ){
                        $('.spinner-district').remove();
                        response = $.parseJSON(response);
                        if ( el_dist.length ) {
                            el_dist.html(response.data);
                            el_dist.parent().removeClass('has-error');
                            el_dist.parent().find('.invalid-feedback').empty().hide();
                        }
                        $('.select_province').parent().removeClass('has-error');
                        $('.select_province').parent().find('.invalid-feedback').empty().hide();
                        $('select').niceSelect('update');
                    },
                    error: function( jqXHR, textStatus, errorThrown ) {
                        StoreApp.notify({
                            icon: 'fa fa-exclamation-triangle', 
                            message: 'Terjadi kesalahan sistem! Ulangi proses beberapa saat lagi.', 
                            type: 'danger',
                        });
                    }
                });
            }
            return false;
        });

        // District Change
        // -----------------------------------------------
        $("body").delegate( ".select_district", "change", function( e ) {
            e.preventDefault();
            var val         = $(this).val();
            var url         = $(this).data('url');
            var el_subdist  = $('.select_subdistrict');
            var el_courier  = $('.select_courier');
            var el_service  = $('.select_service');

            if ( url ) {
                if ( el_courier.length ) {
                    el_courier.val('');
                }

                if ( el_service.length ) {
                    el_service.empty();
                }

                $.ajax({
                    type: "POST",
                    data: { 'district' : val },
                    url: url,
                    beforeSend: function (){ 
                        if ( el_subdist.length ) {
                            el_subdist.empty();
                            el_subdist.parent().append('<span class="spinner-border spinner-subdistrict text-success"></span>');
                        }
                        $('select').niceSelect('update'); 
                    },
                    success: function( response ){
                        $('.spinner-subdistrict').remove();
                        response = $.parseJSON(response);
                        if ( el_subdist.length ) {
                            el_subdist.parent().removeClass('has-error');
                            el_subdist.parent().find('.invalid-feedback').empty().hide();
                            el_subdist.html(response.data);
                        }
                        $('.select_district').parent().removeClass('has-error');
                        $('.select_district').parent().find('.invalid-feedback').empty().hide();
                        $('select').niceSelect('update');
                    },
                    error: function( jqXHR, textStatus, errorThrown ) {
                        StoreApp.notify({
                            icon: 'fa fa-exclamation-triangle', 
                            message: 'Terjadi kesalahan sistem! Ulangi proses beberapa saat lagi.', 
                            type: 'danger',
                        });
                    }
                });
            }
            return false;
        });

        // Select Courier 
        // -----------------------------------------------
        $("body").delegate( ".select_courier", "change", function( e ) {
            e.preventDefault();
            var url         = $(this).data('url');
            var courier     = $(this).val();
            var province    = $('.select_province').val();
            var district    = $('.select_district').val();
            var subdistrict = $('.select_subdistrict').val();
            var el_service  = $('.select_service');

            if ( courier != 'pickup' ) {
                if ( province == '' || province == 0 || province == undefined ) {
                    $(this).val('');
                    StoreApp.notify({
                        icon: 'fa fa-exclamation-triangle', 
                        message: 'Provinsi belum di pilih. Silahkan pilih provinsi terlebih dahulu !', 
                        type: 'danger',
                    });
                    $('select').niceSelect('update');
                    return false;
                }

                if ( district == '' || district == 0 || district == undefined ) {
                    $(this).val('');
                    StoreApp.notify({
                        icon: 'fa fa-exclamation-triangle', 
                        message: 'Kab/Kota belum di pilih. Silahkan pilih Kab/Kota terlebih dahulu !', 
                        type: 'danger',
                    });
                    $('select').niceSelect('update');
                    return false;
                }

                if ( subdistrict == '' || subdistrict == 0 || subdistrict == undefined ) {
                    $(this).val('');
                    StoreApp.notify({
                        icon: 'fa fa-exclamation-triangle', 
                        message: 'Kecamatan belum di pilih. Silahkan pilih Kecamatan terlebih dahulu !', 
                        type: 'danger',
                    });
                    $('select').niceSelect('update');
                    return false;
                }
            }

            var form_data = {
                courier: courier,
                province: province,
                district: district,
                subdistrict: subdistrict,
            };

            shipping_fee = 0;
            $('#courier_cost').val(shipping_fee);
            $('.checkout-shipping').text(StoreApp.formatCurrency(shipping_fee));

            if ( courier ) {
                $.ajax({
                    type:   "POST",
                    url:    url,
                    data:   form_data,
                    beforeSend: function (){
                        if ( el_service.length ) {
                            el_service.empty();
                            el_service.parent().append('<span class="spinner-border spinner-service text-success"></span>');
                        }
                    },
                    success: function( resp ){
                        $('.spinner-service').remove();
                        response = $.parseJSON(resp);
                        if(response.status == 'error'){
                            App.notify({
                                icon: 'fa fa-exclamation-triangle', 
                                message: response.message, 
                                type: 'danger',
                            });
                        }

                        if ( el_service.length ) {
                            el_service.html(response.data);
                            el_service.parent().removeClass('has-error');
                            el_service.parent().find('.invalid-feedback').empty().hide();
                        }
                        $('.select_courier').parent().removeClass('has-error');
                        $('.select_courier').parent().find('.invalid-feedback').empty().hide();
                        $('.select_courier').val(courier);
                        $('.select_service').trigger('change');
                        $('select').niceSelect('update');
                    },
                    error: function( jqXHR, textStatus, errorThrown ) {
                        App.close_Loader();
                        App.notify({
                            icon: 'fa fa-exclamation-triangle', 
                            message: 'Terjadi kesalahan sistem! Ulangi proses beberapa saat lagi.', 
                            type: 'danger',
                        });
                    }
                });
            }
            return false;
        });

        // Select Courier Service 
        // -----------------------------------------------
        $("body").delegate( ".select_service", "change", function( e ) {
            e.preventDefault();
            var cost        = $('select.select_service option:selected').data('cost');
            var payment     = form.data('subtotal');
            var total       = 0;
            if ( cost ) {
                shipping_fee = parseInt(cost);
            } else {
                shipping_fee = 0;
            }

            if ( payment ) {
                total       = parseInt(shipping_fee) + parseInt(payment);
            }

            $('#courier_cost').val(shipping_fee);
            $('.checkout-shipping').text(StoreApp.formatCurrency(shipping_fee));
            $('.checkout-total-payment').text(StoreApp.formatCurrency(total, true));
            return false;
        });

        // Options Checkout 
        // -----------------------------------------------
        $("body").delegate( "[name=options_reg]", "change", function( e ) {
            e.preventDefault();
            var val = $(this).val();
            $("#options_member_reg").slideUp('slow');
            if (val == 'member') {
                $("#options_member_reg").slideDown('slow');
            }
            setRuleValidateMember();
            return false;
        });

        // Button Process Checkout
        // -----------------------------------------------
        $("body").delegate( "#btn-process-checkout", "click", function( e ) {
            e.preventDefault();
            processCheckout();
        });
    };

    // ---------------------------------
    // Process Checkout
    // ---------------------------------
    var processCheckout = function() {
        if ( ! form.length ) {
            return;
        }

        if ( $(form).valid() ) {
            var url = $(form).attr('action');
            $.ajax({
                type:   "POST",
                url:    url,
                data:   $(form).serialize(),
                beforeSend: function (){
                    StoreApp.run_Loader('timer');
                    $('#modal-process-checkout').modal('hide');
                },
                success: function( response ){
                    response = $.parseJSON(response);
                    StoreApp.close_Loader();

                    if ( response.token ) {
                        StoreApp.kdToken(response.token);
                    }
                    
                    if( response.status == 'access_denied' ){
                        $(location).attr('href',response.url);
                    }else{
                        if( response.status == 'success'){
                            StoreApp.notify({
                                icon: 'fa fa-check', 
                                message: response.message, 
                                type: 'success',
                            });
                            setTimeout(function(){ 
                                if ( response.url ) {
                                    $(location).attr('href',response.url);
                                } else {
                                    location.reload();
                                }
                            }, 1000);
                        }else{
                            StoreApp.notify({
                                icon: 'fa fa-exclamation-triangle', 
                                message: response.message, 
                                type: 'danger',
                            });
                        }
                    }
                },
                error: function( jqXHR, textStatus, errorThrown ) {
                    StoreApp.close_Loader();
                    StoreApp.notify({
                        icon: 'fa fa-exclamation-triangle', 
                        message: 'Terjadi kesalahan sistem! Ulangi proses beberapa saat lagi.', 
                        type: 'danger',
                    });
                    setTimeout(function(){ location.reload(); }, 500);
                }
            });
        } else {
            $('#modal-process-checkout').modal('hide');
        }
    };
    
    return {
        init: function() {
            handleValidationStoreCheckout();
            handleGeneralStoreCheckout();
        }
    };
}();

// ===========================================================
// Store App
// ===========================================================
var StoreApp = function() {    
  return {

    // wrapper function to scroll(focus) to an element
    scrollTo: function (el, offeset) {
        var pos = (el && el.length ) ? el.offset().top : 0;

        if (el) {       
            pos = pos + (offeset ? offeset : -1 * el.height());
        }

        jQuery('html,body').animate({
            scrollTop: pos
        }, 'slow');
    },

    // function to scroll to the top
    scrollTop: function () {
        scrollTo();
    },
    
    getUniqueID: function(prefix) {
        return 'prefix_' + Math.floor(Math.random() * (new Date()).getTime());
    },

    alert: function(options) {

        options = $.extend(true, {
            container: "", // alerts parent container(by default placed after the page breadcrumbs)
            place: "append", // append or prepent in container 
            type: 'success',  // alert's type
            message: "",  // alert's message
            close: true, // make alert closable
            reset: true, // close all previouse alerts first
            focus: true, // auto scroll to the alert after shown
            closeInSeconds: 0, // auto close after defined seconds
            icon: "" // put icon before the message
        }, options);

        var id = StoreApp.getUniqueID("app_alert");

        var html = '<div id="'+id+'" class="app-alerts alert alert-'+options.type+' fade show">' + 
          (options.close ? '<button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button>' : '' ) + 
        (options.icon != "" ? '<i class="fa-lg fa fa-'+options.icon + ' mr-2"></i>  ' : '') + options.message+'</div>';

        if (options.reset) {
            $('.app-alerts').remove();
        }

        if (!options.container) {
            $('.page-breadcrumb').after(html);
        } else {
            if (options.place == "append") {
                $(options.container).append(html);
            } else {
                $(options.container).prepend(html);
            }
        }

        if (options.focus) {
            StoreApp.scrollTo($('#' + id));
        }

        if (options.closeInSeconds > 0) {
            setTimeout(function(){
                $('#' + id).remove();
            }, options.closeInSeconds * 1000);
        }
    },
    
    notify: function(options) {
        options = $.extend(true, {
            icon: "", // put icon before the message
            title: "", // title notify
            message: "",  // message notify
            place: "top", // place in container 
            align: "center", // align in container 
            type: 'success',  // type notify
            enter: 'animated fadeInDown', // animate in
            exit: 'animated fadeOutUp', // animate in out
            timer: 2500, // delay close after defined seconds
        }, options);

        $.notify({
            icon: options.icon,
            title: options.title,
            message: options.message,
            url: ""
        }, {
            element: "body",
            type: options.type,
            allow_dismiss: !0,
            placement: {
                from: options.place,
               align: options.align
            },
            offset: {
                x: 15,
                y: 15
            },
            spacing: 10,
            z_index: 1080,
            delay: 2500,
            timer: options.timer,
            url_target: "_blank",
            mouse_over: !1,
            animate: {
                enter: options.enter,
                exit: options.exit
            },
            template: `<div data-notify="container" class="alert alert-dismissible alert-{0} alert-notify" role="alert">
                          <span class="alert-icon" data-notify="icon"></span> 
                          <div class="alert-text"> 
                            <span class="alert-title" data-notify="title">{1}</span> 
                            <span data-notify="message">{2}</span>
                          </div>
                          <button type="button" class="close" data-notify="dismiss" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      </div>`
        });
    },

    run_Loader: function() {
        $('.preloader').show();
    },

    close_Loader: function() {
        $('.preloader').delay(500).fadeOut('slow');
    },

    kdName: function(update = '') {
        if ( update ) {
          $('.kd-content').data('name', update);
          return update;
        } else {
          return $('.kd-content').data('name');
        }
    },

    kdToken: function(update = '') {
        if ( update ) {
          $('.kd-content').data('token', update);
          return update;
        } else {
          return $('.kd-content').data('token');
        }
    },

    formatCurrency: function(currency, rp = false) {
        if (currency) {
            var number_string = currency.toString();
            sisa   = number_string.length % 3;
            rupiah = number_string.substr(0, sisa);
            ribuan = number_string.substr(sisa).match(/\d{3}/g);

            if (ribuan) {
                separator = sisa ? '.' : '';
                rupiah   += separator + ribuan.join('.');
            }
            return ( rp ? 'Rp ' : '' ) + rupiah;
        } else {
            return rp ? 'Rp 0 ' : '0';
        }
    },

    readURLmedia: function(input, img_id, video_id = '', file_info = '') {
        if (input[0].files && input[0].files[0]) {
            var typeFile    = input[0].files[0].type;
            var sizeFile    = input[0].files[0].size;
            var _size       = Math.round(sizeFile/1024);
            var _type       = 'image';
            if ( typeFile ) {
                _type = typeFile.substr(0, typeFile.indexOf('/')); 
            }

            if ( $('.information-'+file_info).length ) {
              $('.information-'+file_info).show();
            } else {
              $('.img-information').show();
            }

            var reader = new FileReader();
            reader.onload = function (e) {
                if ( _type == 'video' && video_id ) {
                    video_id.attr('src', e.target.result);
                    video_id.show();
                    img_id.hide();
                    img_id.attr('src', '');
                } else {
                    img_id.attr('src', e.target.result);
                    img_id.show();
                    if ( video_id ) {
                        video_id.hide();
                        video_id.attr('src', '');
                    }
                }

                var size_img = $('#size_img_thumbnail');
                if ( $('#size-'+file_info).length ) {
                    size_img = $('#size-'+file_info);
                }

                if ( size_img.length ) {
                    if ( _size > 1024 ) {
                        _size = Math.round(_size/1024);
                        _size = _size + ' MB';
                    } else {
                        _size = _size + ' KB';
                    }
                    size_img.text(_size);
                }
            }

            reader.readAsDataURL(input[0].files[0]);
        }
    },
  };
}();