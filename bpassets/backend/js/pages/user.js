var User = function() {
    var form            = $('#form-staff');
    var wrapper         = $('.wrapper-form-staff');
    var form_type       = form.data('type');
    var alert_msg       = $('#alert');

    // ---------------------------------
    // Handle Validation User Form
    // ---------------------------------
    var handleValidationUserForm = function() {
        if ( ! form.length ) {
            return;
        }

        form.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'invalid-feedback', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                user_username: {
                    minlength: 5,
                    required: true,
                    unamecheck: true,
                    remote: {
                        url: $("#user_username").data('url'),
                        type: "post",
                        data: {
                            [App.kdName()]: function() {
                                return App.kdToken();
                            },
                            username: function() {
                                return $("#user_username").prop( 'readonly' ) ? '' : $("#user_username").val();
                            }
                        },
                        dataFilter: function(response) {
                            response = $.parseJSON(response);
                            if ( response.token ) {
                                App.kdToken(response.token);
                            }
                            return response.status;
                        }
                    }
                },
                user_password: {
                    minlength: 6,
                    required: true,
                    pwcheck: true,
                },
                user_password_confirm: {
                    required: true,
                    equalTo: '#user_password'
                },
                user_name: {
                    minlength: 3,
                    required: true,
                    lettersonly: true,
                },
                user_phone: {
                    minlength: 8,
                },
                user_email: {
                    email: true,
                    required: true,
                    remote: {
                        url: $("#user_email").data('url'),
                        type: "post",
                        data: {
                            [App.kdName()]: function() {
                                return App.kdToken();
                            },
                            email: function() {
                                return $("#user_email").prop( 'readonly' ) ? '' : $("#user_email").val();
                            }
                        },
                        dataFilter: function(response) {
                            response = $.parseJSON(response);
                            if ( response.token ) {
                                App.kdToken(response.token);
                            }
                            return response.status;
                        }
                    }
                },
                user_access: {
                    required: true,
                },
                user_bidang: {
                    required: function(element) {
                        if( $('#user_access').val() == 3 ){
                            return true;
                        }else{
                            return false;
                        }
                    }
                },
                user_skpd: {
                    required: function(element) {
                        if( $('#user_access').val() == 4 ){
                            return true;
                        }else{
                            return false;
                        }
                    }
                },
            },
            messages: {
                user_username: {
                    remote: "Username sudah digunakan. Silahkan gunakan username lain",
                },
                user_password_confirm: {
                    equalTo: "Password konfirmasi tidak cocok dengan password yang di atas",
                },
                user_email: {
                    remote: "Email sudah digunakan. Silahkan gunakan email lain",
                }
            },
            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.parent(".input-group").length > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) { 
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').length > 0) { 
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').length > 0) { 
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').length > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').length > 0) { 
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit              
                App.alert({
                    type: 'danger', 
                    icon: 'bell', 
                    message: 'Ada beberapa error, silahkan cek formulir di bawah!', 
                    container: wrapper, 
                    place: 'prepend',
                    closeInSeconds: 5
                });
            },
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            submitHandler: function (form) {
                var url             = $(form).attr('action');
                bootbox.confirm("Apakah anda yakin akan simpan data pengguna ini ?", function(result) {
                    if( result == true ){
                        $.ajax({
                            type:   "POST",
                            url:    url,
                            data:   $(form).serialize(),
                            beforeSend: function (){
                                App.run_Loader('timer');
                            },
                            success: function( response ){
                                response = $.parseJSON(response);
                                App.close_Loader();

                                if ( response.token ) {
                                    App.kdToken(response.token);
                                }
                                
                                if( response.status == 'access_denied' ){
                                    $(location).attr('href',response.url);
                                }else{
                                    if( response.status == 'success'){
                                        $(form)[0].reset();
                                        bootbox.alert(response.message, function(){ 
                                            $(location).attr('href', response.url);
                                        });
                                    }else{
                                        App.alert({
                                            type: 'danger', 
                                            icon: 'warning', 
                                            message: response.message, 
                                            container: wrapper, 
                                            place: 'prepend',
                                            closeInSeconds: 5,
                                        });
                                    }
                                }
                                return false;
                            },
                            error: function( jqXHR, textStatus, errorThrown ) {
                                App.close_Loader();
                                bootbox.alert('Terjadi kesalahan sistem! Ulangi proses beberapa saat lagi.', function(){ 
                                    location.reload();
                                });
                            }
                        });
                    }
                });
                return false;
            }
        });

        $.validator.addMethod("pwcheck", function(value) {
            return /[a-z].*[0-9]|[0-9].*[a-z]/i.test(value); // consists of only these
        }, "Password harus terdiri dari huruf dan angka" );
        
        $.validator.addMethod("unamecheck", function(value) {
            return /^[A-Za-z0-9]{4,16}$/i.test(value);   // consists of only these
        }, "Username tidak memenuhi kriteria" );
        
        $.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
        }, "Silahkan inputkan Nama dengan huruf saja" );
    };

    // ---------------------------------
    // Handle General User Form
    // ---------------------------------
    var handleGeneralUserForm = function() {
        var _accessToggle = function( val ) {
            $( '.access-box' ).hide();
            $( '.access-box.access-box-' + val ).show( 'fast' );
            if ( form_type == 'create' ) {
                $("input:checkbox").prop('checked', false)
                $('.select-user-access').val('').trigger('change');
            }
        };
        var _fiturToggle = function( val ) {
            $( '.fitur-box' ).hide();
            $( '.fitur-box.fitur-box-' + val ).show( 'fast' );
            if ( form_type == 'create' ) {
                $("input:checkbox").prop('checked', false)
            }
        };

        // Select Access
        $('select[name=user_access]').change(function(e){
            e.preventDefault();
            var val     = $(this).val();
            var access  = $('select[name="user_access"] option:selected').data('access');
            _accessToggle( access );
        });
        
        // Select Fitur
        $('label.fitur-toggle').click( function(e) {
            e.preventDefault();
            var input   = $( this ).find( 'input[name=fitur_access]' );
            var val     = input.val();
            
            input.attr( 'checked', 'checked' );
            _fiturToggle( val );
        });
        
        _fiturToggle( $( 'input[name=fitur_access]:checked' ).val() );
    };

    return {
        init: function() {
            handleValidationUserForm();
            handleGeneralUserForm();
        }
    };
}();
    