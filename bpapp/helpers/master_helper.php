<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

// -------------------------------------------------------------------------
// Master functions helper
// -------------------------------------------------------------------------

if (!function_exists('bp_urusan')) {
    /**
     * Get Product Cart function.
     */
    function bp_urusan($id = '')
    {
        $CI = &get_instance();
        $urusan = $CI->Model_Master->get_urusan($id);
        return $urusan;
    }
}

if (!function_exists('bp_sub_urusan')) {
    /**
     * Get Product Cart function.
     */
    function bp_sub_urusan($id = '')
    {
        $CI = &get_instance();
        $urusan = $CI->Model_Master->get_sub_urusan($id);
        return $urusan;
    }
}

if (!function_exists('bp_program')) {
    /**
     * Get Product Cart function.
     */
    function bp_program($id = '')
    {
        $CI = &get_instance();
        $urusan = $CI->Model_Master->get_program($id);
        return $urusan;
    }
}

if (!function_exists('bp_kegiatan')) {
    /**
     * Get Product Cart function.
     */
    function bp_kegiatan($id = '')
    {
        $CI = &get_instance();
        $urusan = $CI->Model_Master->get_kegiatan($id);
        return $urusan;
    }
}

if (!function_exists('bp_sub_kegiatan')) {
    /**
     * Get Product Cart function.
     */
    function bp_sub_kegiatan($id = '')
    {
        $CI = &get_instance();
        $urusan = $CI->Model_Master->get_sub_kegiatan($id);
        return $urusan;
    }
}
