<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

// -------------------------------------------------------------------------
// Member functions helper
// -------------------------------------------------------------------------

if (!function_exists('bp_get_memberdata_by_id')) {
    /**
     * Get member data by id
     *
     * @param integer $id Member ID
     * @return (object) member data
     */
    function bp_get_memberdata_by_id($id)
    {
        $CI = &get_instance();
        return $CI->Model_Member->get_memberdata($id);
    }
}

if (!function_exists('bp_get_memberconfirm_by_downline')) {
    /**
     * Get Member Confirm data by id downline
     *
     * @param integer $id ID Downline
     * @return (object) member confirm data
     */
    function bp_get_memberconfirm_by_downline($id)
    {
        $CI = &get_instance();
        return $CI->Model_Member->get_member_confirm_by_downline($id);
    }
}

if (!function_exists('bp_get_memberboard_by')) {
    /**
     * Get Member Board data by condition
     *
     * @param integer $id ID Downline
     * @return (object) member confirm data
     */
    function bp_get_memberboard_by($field, $value='', $conditions='', $limit = 0, $count = false)
    {
        $CI = &get_instance();
        return $CI->Model_Member->get_member_board_by($field, $value, $conditions, $limit, $count);
    }
}

if (!function_exists('bp_get_boardtree_by')) {
    /**
     * Get Member Board data by condition
     *
     * @param integer $id ID Downline
     * @return (object) member confirm data
     */
    function bp_get_boardtree_by($field, $value='', $conditions='', $limit = 0, $count = false)
    {
        $CI = &get_instance();
        return $CI->Model_Member->get_board_tree_by($field, $value, $conditions, $limit, $count);
    }
}

if (!function_exists('as_active_member')) {
    /**
     *
     * Is current member is active member
     * @param Object $member
     * @return bool
     */
    function as_active_member($member)
    {
        if (!empty($member)) {
            return ($member->status == 1);
        }
        return false;
    }
}

if (!function_exists('as_superadmin')) {
    /**
     *
     * Is current member is SuperAdmin
     * @param Object $member
     * @return bool
     */
    function as_superadmin($member)
    {
        if (!$member) return false;

        $CI = &get_instance();
        $member = $CI->bp_member->member($member->id);

        return (($member->type == ADMINISTRATOR));
    }
}

if (!function_exists('as_administrator')) {
    /**
     *
     * Is current member is Administrator
     * @param Object $member
     * @return bool
     */
    function as_administrator($member)
    {
        if (!$member) return false;

        $CI = &get_instance();
        $member = $CI->bp_member->member($member->id);

        if ( $member->type == ADMINISTRATOR || $member->type == ADMIN ) {
            return true;
        }
        return false;
    }
}

if (!function_exists('as_admin')) {
    /**
     *
     * Is current member is Admin
     * @param Object $member
     * @return bool
     */
    function as_admin($member)
    {
        if (!$member) return false;

        $CI = &get_instance();
        $member = $CI->bp_member->member($member->id);

        return (($member->type == ADMIN));
    }
}

if (!function_exists('as_bidang')) {
    /**
     *
     * Is current member is Bidang
     * @param Object $member
     * @return bool
     */
    function as_bidang($member)
    {
        if (!$member) return false;

        $CI = &get_instance();
        $member = $CI->bp_member->member($member->id);

        return (($member->type == BIDANG));
    }
}

if (!function_exists('as_skpd')) {
    /**
     *
     * Is current member is skpd
     * @param Object $member
     * @return bool
     */
    function as_skpd($member)
    {
        if (!$member) return false;

        $CI = &get_instance();
        $member = $CI->bp_member->member($member->id);

        return (($member->type == SKPD));
    }
}

if (!function_exists('as_dewan')) {
    /**
     *
     * Is current member is dewan
     * @param Object $member
     * @return bool
     */
    function as_dewan($member)
    {
        if (!$member) return false;

        $CI = &get_instance();
        $member = $CI->bp_member->member($member->id);

        return (($member->type == DEWAN));
    }
}

if (!function_exists('as_unit')) {
    /**
     *
     * Is current member is unit
     * @param Object $member
     * @return bool
     */
    function as_unit($member)
    {
        if (!$member) return false;

        $CI = &get_instance();
        $member = $CI->bp_member->member($member->id);

        return (($member->type == UNIT));
    }
}

if (!function_exists('as_member')) {
    /**
     *
     * Is current user is member
     * @param Object $member
     * @return bool
     */
    function as_member($member)
    {
        if (!$member) return false;

        $CI = &get_instance();
        $member = $CI->bp_member->member($member->id);

        return (($member->type == MEMBER));
    }
}

if (!function_exists('bp_generate_password')) {
    /**
     * Generate password for member
     * @author  Yuda
     * @param   int     $length     Random String Length
     * @param   boolean $cap        Capital (Default FALSE)
     * @return  String
     */
    function bp_generate_password($length = 0, $cap = false)
    {
        $characters     = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        if ($cap) {
            $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        }

        $randomString   = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }
}

if (!function_exists('bp_is_username_blacklisted')) {
    /**
     * Is ID member blacklisted
     * @param  string $username ID Member
     * @return boolean           blacklisted
     */
    function bp_is_username_blacklisted($username)
    {
        if (!$blacklist = get_option('blacklist'))
            return false;

        return in_array($username, bp_isset($blacklist['usernames'], array()));
    }
}

if (!function_exists('bp_is_email_blacklisted')) {
    /**
     * Is email blacklisted
     * @param  string $email Email
     * @return boolean        blacklisted
     */
    function bp_is_email_blacklisted($email)
    {
        if (!$blacklist = get_option('blacklist'))
            return false;

        return in_array($email, bp_isset($blacklist['emails'], array()));
    }
}

if (!function_exists('bp_unset_current_member_human')) {
    /**
     * @since 1.0.0
     * @access public
     * @author Yuda
     */
    function bp_unset_current_member_human()
    {
        $CI = &get_instance();
        return $CI->session->unset_userdata('is_human');
    }
}
if (!function_exists('bp_unset_clone_member_data')) {
    /**
     * @since 1.0.0
     * @access public
     * @author Yuda
     * @param  Object $memberdata Object member data
     */
    function bp_unset_clone_member_data($member, $unset_id = false)
    {
        if (!$member) return false;

        $CI = &get_instance();

        if (is_array($member)) $member = (object)$member;

        $memberdata = new stdClass();
        $memberdata->name                   = $member->name;
        $memberdata->email                  = $member->email;
        $memberdata->phone                  = $member->phone;
        $memberdata->phone_home             = $member->phone_home;
        $memberdata->phone_office           = $member->phone_office;
        $memberdata->pob                    = $member->pob;
        $memberdata->dob                    = $member->dob;
        $memberdata->gender                 = $member->gender;
        $memberdata->marital                = $member->marital;
        $memberdata->idcard_type            = $member->idcard_type;
        $memberdata->idcard                 = $member->idcard;
        $memberdata->npwp                   = $member->npwp;
        $memberdata->country                = $member->country;
        $memberdata->province               = $member->province;
        $memberdata->district               = $member->district;
        $memberdata->subdistrict            = $member->subdistrict;
        $memberdata->village                = $member->village;
        $memberdata->address                = $member->address;
        $memberdata->bank                   = $member->bank;
        $memberdata->bill                   = $member->bill;
        $memberdata->bill_name              = $member->bill_name;
        $memberdata->emergency_name         = $member->emergency_name;
        $memberdata->emergency_relationship = $member->emergency_relationship;
        $memberdata->emergency_phone        = $member->emergency_phone;

        return $memberdata;
    }
}

if (!function_exists('bp_generate_tree')) {
    /**
     * Generate tree for member
     * @author  Yuda
     * @param   Int     $id_member  (Required)  Member ID
     * @param   int     $up_tree    (Required)  Upline Tree
     * @return  String
     */
    function bp_generate_tree($id_member, $up_tree)
    {
        if (!$up_tree) return false;

        if (!is_numeric($id_member)) return false;

        $id_member  = absint($id_member);
        if (!$id_member) return false;

        $tree = $up_tree . '-' . $id_member;

        return $tree;
    }
}

if (!function_exists('bp_generate_tree_sponsor')) {
    /**
     * Generate tree for member
     * @author  Yuda
     * @param   Int     $id_member  (Required)  Member ID
     * @param   int     $spon_tree  (Required)  Sponsor Tree
     * @return  String
     */
    function bp_generate_tree_sponsor($id_member, $spon_tree)
    {
        if (!$spon_tree) return false;

        if (!is_numeric($id_member)) return false;

        $id_member  = absint($id_member);
        if (!$id_member) return false;

        $tree = $spon_tree . '-' . $id_member;

        return $tree;
    }
}

if (!function_exists('bp_ancestry')) {
    /**
     * Get ancestry data of member
     * @author  Yuda
     * @param   Int     $id_member      (Required)  Member ID
     * @return  Object parent data
     */
    function bp_ancestry($id_member)
    {
        $CI = &get_instance();
        return $CI->Model_Member->get_ancestry($id_member);
    }
}

if (!function_exists('bp_ancestry_sponsor')) {
    /**
     * Get ancestry sponsor data of member
     * @author  Yuda
     * @param   Int     $id_member      (Required)  Member ID
     * @return  Object parent data
     */
    function bp_ancestry_sponsor($id_member)
    {
        $CI = &get_instance();
        return $CI->Model_Member->get_ancestry_sponsor($id_member);
    }
}

if (!function_exists('bp_upline_available')) {
    /**
     * Get upline available data of member
     * @author  Yuda
     * @param   Int     $id_member      (Optional)  Member ID
     * @return  Object parent data
     */
    function bp_upline_available($id_member = '', $firstrow = true, $date = '', $level_qualified = '', $level_equal = false)
    {
        $CI = &get_instance();
        return $CI->Model_Member->get_upline_available_position($id_member, $firstrow, $date, $level_qualified, $level_equal);
    }
}

if (!function_exists('bp_downline')) {
    /**
     * Get downline of member
     * @author  Yuda
     * @param   Int     $id_member  (Required)  Member ID (Parent)
     * @param   String  $position   (Optional)  Position of downline, value ('kiri' or 'kanan')
     * @param   String  $status     (Optional)  Status of Downline, value ('active' or 'pending')
     * @param   Boolean $count      (Optional)  Get Count of Downline
     * @return  Mixed,  Boolean if wrong data of id member, otherwise data or count of downline
     */
    function bp_downline($id_member, $position = '', $status = '', $count = false)
    {
        if(!is_numeric($id_member)) return false;

        $id_member = absint($id_member);
        if(!$id_member) return false;

        $CI =& get_instance();
        $member = $CI->Model_Member->get_downline($id_member, $position, $status, $count);

        return $member;
    }
}

if(!function_exists('bp_downline_board')) {
    /**
     * Get downline board of member
     * @author  Iqbal
     * @param   Int     $id_member  (Required)  Member ID (Parent)
     * @param   String  $position   (Optional)  Position of downline, value ('kiri' or 'kanan')
     * @param   String  $status     (Optional)  Status of Downline, value ('active' or 'pending')
     * @param   Boolean $count      (Optional)  Get Count of Downline
     * @return  Mixed,  Boolean if wrong data of id member, otherwise data or count of downline
     */
    function bp_downline_board($id_member_board, $level = 1, $position = 0, $status = 1, $count = false)
    {
        if(!is_numeric($id_member_board)) return false;
        if(!is_numeric($level)) return false;
        if(!is_numeric($position)) return false;

        $id_member_board = absint($id_member_board);
        if(!$id_member_board) return false;

        $level = absint($level);
        if(!$level) return false;

        $position = absint($position);
        if(!$position) return false;

        $CI =& get_instance();

        if ( ! $memberboard = bp_get_memberboard_by('id', $id_member_board) ){
            return false;
        }

        if ( $memberboard->status == 0 ) {
            return false;
        }

        $member     = false;
        $condition  = array(
            'id_member'     => $memberboard->id_member,
            'level'         => $level,
            'position'      => $position,
            'status'        => $status,
        );

        $boardtree = $CI->Model_Member->get_board_tree_by('id_board', $id_member_board, $condition, 1, $count);
        if ( $boardtree ) {
            if ( $count ) {
                return $boardtree;
            }

            $id_member  = isset($boardtree->id_downline) ? $boardtree->id_downline : 0;
            $member     = bp_get_memberdata_by_id($id_member);  
        }

        return $member;
    }
}

if (!function_exists('bp_my_gen_sponsor')) {
    /**
     * Get ancestry sponsor data of member
     * @author  Yuda
     * @param   Int     $id_member      (Required)  Member ID
     * @return  Object parent data
     */
    function bp_my_gen_sponsor($id_member)
    {
        $CI = &get_instance();
        $ancestry_sponsor = $CI->Model_Member->get_ancestry_sponsor($id_member);
        if (!$ancestry_sponsor) return 0;
        $ids_sponsor = explode(',', $ancestry_sponsor);
        return count($ids_sponsor);
    }
}

if (!function_exists('bp_position_sponsor')) {
    /**
     * Check your position of sponsor
     * @author  Yuda
     * @param   Int     $id_member      (Required)  Member ID
     * @return Mixed, Boolean false if invalid member id, otherwise of position sponsor 
     */
    function bp_position_sponsor($id_member)
    {
        $CI = &get_instance();
        return $CI->Model_Member->get_position_sponsor($id_member);
    }
}

if (!function_exists('bp_check_username')) {
    /**
     *
     * Check username available
     * @param   Int     $username      Username
     * @return Mixed, Boolean false if invalid username, otherwise array of phone available
     */
    function bp_check_username($username)
    {
        if (!$username) return false;
        $CI = &get_instance();

        $username_exist = false;
        $condition      = ' WHERE %username% LIKE "' . $username . '" ';
        $data           = $CI->Model_Auth->get_all_user_data(1, 0, $condition, '');
        if ($data) {
            $username_exist = $data[0];
        }

        if (!$username_exist) {
            $staff = $CI->Model_Staff->get_by('username', $username);
            if ($staff) {
                $username_exist = $staff;
            }
        }

        return $username_exist;
    }
}

if (!function_exists('bp_generate_username_unique')) {
    /**
     * Generate Username unique number
     * @author  Yuda
     * @return  String
     */
    function bp_generate_username_unique($username, $plus = 0, $add_char = '')
    {
        if (!$username) return false;
        $CI = &get_instance();

        $username_exist = false;
        $number         = 0;
        $condition      = ' WHERE %username% LIKE "' . $username . '%"';
        $data           = $CI->Model_Auth->get_all_user_data(1, 0, $condition, 'username DESC');
        if ($data) {
            $username_exist = isset($data[0]->username) ? $data[0]->username : false;
        }

        if ($username_exist) {
            $len        = strlen($username);
            $number     = substr($username_exist, 0, $len);
            $number     = is_numeric($number) ? $number : 0;
        }

        $number         = intval($number);
        $number         = $number + $plus;
        // $unique_number  = str_pad($number + 1, 4, '0', STR_PAD_LEFT);
        $unique_user    = strtolower($username);
        if ( $number ) {
            $unique_user .= $add_char . $number;
        }

        $check_exist    = bp_check_username($unique_user);
        if ($check_exist) {
            $next       = $plus + 1;
            return bp_generate_username_unique($username, $next, $add_char);
        } else {
            return $unique_user;
        }
    }
}

if ( !function_exists('kb_saved_member_board') )
{
    /**
     * Saved member board
     * @param  integer  $id_member      ID Member
     * @param  integer  $board          Number of Board
     * @param  integer  $datetime       Datetime
     * @param  string   &$message       Message of status upgrade (reference variable)
     * @return boolean
     */
    function kb_saved_member_board( $memberdata, $board = 1, $datetime = '', $status = 0, &$message = '' ) {
        $CI =& get_instance();

        $datetime = $datetime ? $datetime : date( 'Y-m-d H:i:s' );

        if ( !$memberdata ){
            $message = 'Data Member tidak boleh kosong!';
            return false;
        }

        if ( !$board ){
            $message = 'Board tidak boleh kosong!';
            return false;
        }

        if ( !is_numeric($board) ) {
            $message = 'Nilai Board harus numeric !';
            return false;
        }

        $board      = absint($board);
        if( !$board ) {
            $message = 'Nilai Board harus numeric !';
            return false;            
        }

        if ( $board > 3 ) {
            $message = 'Nilai Board tidak valid !';
            return false;
        }

        if ( $status ) {
            if ( !is_numeric($status) ) {
                $message = 'Nilai Status harus numeric !';
                return false;
            }

            if ( $status > 1 ) {
                $message = 'Nilai Status tidak valid !';
                return false;
            }
        }

        // -------------------------------------------------
        // Check Sponsor
        // -------------------------------------------------
        $sponsor_id         = $memberdata->sponsor;
        $sponsordata        = bp_get_memberdata_by_id($sponsor_id);
        if( !$sponsordata ){
            $message = 'Sponsor tidak ditemukan.';
            return false;
        }

        // Set Data Member Board
        $data_member_board  = array(
            'code'          => 'MB' . strtotime($datetime) . bp_generate_rand_string(4),
            'id_member'     => $memberdata->id,
            'username'      => $memberdata->username,
            'sponsor'       => $sponsor_id,
            'board'         => $board,
            'level'         => $memberdata->gen,
            'status'        => $status,
            'datecreated'   => $datetime,
            'datemodified'  => $datetime
        );

        if ( $status == 1 ) {
            $data_member_board['dateactived'] = $datetime;
        }

        // Save Data Member Board
        if ( ! $save_member_board = $CI->Model_Member->save_data_member_board($data_member_board) ) {
            $message = 'Simpan Data Member Board tidak berhasil. Terjadi kesalahan pada transaksi !';
            return false;
        }

        if ( $status == 1 ) {
            // Generate downline board tree
            kb_generate_downline_board_tree($save_member_board, $datetime);
        }

        return $save_member_board;
    }
}

if ( !function_exists('kb_actived_member_board') )
{
    /**
     * Actived member board
     * @param  integer  $id_member      ID Member
     * @param  integer  $datetime       Datetime
     * @param  string   &$message       Message of status upgrade (reference variable)
     * @return boolean
     */
    function kb_actived_member_board( $memberboard, $datetime = '', &$message = '' ) {
        $CI =& get_instance();

        $datetime = $datetime ? $datetime : date( 'Y-m-d H:i:s' );

        if ( !$memberboard ){
            $message = 'Data Member Board tidak boleh kosong!';
            return false;
        }

        // Update Data Member Board
        $data_member_board  = array('status' => 1, 'datemodified' => $datetime, 'dateactived' => $datetime);
        if ( ! $update_member_board = $CI->Model_Member->update_data_member_board($memberboard->id, $data_member_board) ) {
            $message = 'Update Data Member Board tidak berhasil. Terjadi kesalahan pada transaksi !';
            return false;
        }

        // Generate downline board tree
        kb_generate_downline_board_tree($memberboard->id, $datetime);

        return $memberboard->id;
    }
}

if ( !function_exists('kb_check_member_board') )
{
    /**
     * Actived member board
     * @param  integer  $id_member      ID Member
     * @param  integer  $board          Number of Board
     * @param  integer  $datetime       Datetime
     * @param  string   &$message       Message of status upgrade (reference variable)
     * @return boolean
     */
    function kb_check_member_board( $memberdata, $board = 1, $datetime = '', &$message = '' ) {
        $CI =& get_instance();

        $datetime = $datetime ? $datetime : date( 'Y-m-d H:i:s' );

        if ( !$memberdata ){
            $message = 'Data Member Board tidak boleh kosong!';
            return false;
        }

        if ( !$board ){
            $message = 'Board tidak boleh kosong!';
            return false;
        }

        if ( !is_numeric($board) ) {
            $message = 'Nilai Board harus numeric !';
            return false;
        }

        $board      = absint($board);
        if( !$board ) {
            $message = 'Nilai Board harus numeric !';
            return false;            
        }

        if ( $board > 3 ) {
            $message = 'Nilai Board tidak valid !';
            return false;
        }

        // Set Variable
        $id_member              = $memberdata->id;
        $count_qualified        = 0;
        $boardactived           = false;
        $board_qualified        = config_item('board_qualified');

        // -------------------------------------------------
        // Check Downline Sponsor
        // -------------------------------------------------
        $count_downline         = $CI->Model_Member->count_by_sponsor($id_member, true);
        if ( $count_downline ) {
            $count_qualified    = floor($count_downline / $board_qualified);
        }

        if ( $count_qualified ) {
            // -------------------------------------------------
            // Check Member Board
            // -------------------------------------------------
            $memberboard        = bp_get_memberboard_by('id_member', $id_member, array('board' => $board, 'status' => 0));
            if( $memberboard ){
                $boardactived   = kb_actived_member_board($memberboard, $datetime);
            } else {
                $memberboard    = bp_get_memberboard_by('id_member', $id_member, array('board' => $board, 'status' => 1));
                if ( $memberboard ) {
                    $boardactived = $memberboard->id;
                }
            }

            $count_condition    = ' AND board = '. $board;
            $count_board        = $CI->Model_Member->count_member_board($id_member, $count_condition);

            if ( $count_qualified > $count_board ) {
                $status_board   = $boardactived ? 0 : 1;
                $saveboard      = kb_saved_member_board($memberdata, $board, $datetime, $status_board);
                $boardactived   = $boardactived ? $boardactived : $saveboard;
            }

            if ( $boardactived ) {
                kb_generate_board_tree($boardactived, $datetime);
            }
        }

        return true;
    }
}

if ( !function_exists('kb_generate_board_tree') )
{
    /**
     * Generate member board tree
     * @param  integer  $id_member_board      ID Member
     * @param  integer  $datetime             Datetime
     * @param  string   &$message       Message of status upgrade (reference variable)
     * @return boolean
     */
    function kb_generate_board_tree( $id_member_board, $datetime = '', &$message = '' ) {
        $CI =& get_instance();

        $datetime = $datetime ? $datetime : date( 'Y-m-d H:i:s' );

        if ( !$id_member_board ){
            $message = 'ID Member Board tidak boleh kosong!';
            return false;
        }

        if ( ! $memberboard = bp_get_memberboard_by('id', $id_member_board) ){
            $message = 'Member Board tidak ditemukan!';
            return false;
        }

        if ( $memberboard->status != 1 ){
            $message = 'Status Member Board tidak aktif!';
            return false;
        }

        // -------------------------------------------------
        // Set Variable
        // -------------------------------------------------
        $id_member              = $memberboard->id_member;
        $board                  = $memberboard->board;
        $next_board             = $board + 1;
        $board_level            = config_item('board_level');
        $board_tree             = config_item('board_tree');
        $ids_sponsor            = array();

        // -------------------------------------------------
        // Check Ancestry Sponsor
        // -------------------------------------------------\
        $ancestry_sponsor       = bp_ancestry_sponsor($id_member);
        if ( $ancestry_sponsor ) {
            $ancestry           = explode(',', $ancestry_sponsor);
            $level              = 1;
            foreach ($ancestry as $id_spon) {
                if ( $id_spon == 1 ) { continue; }
                if ( $level > $board_level) { break; }

                $sponsorboard   = bp_get_memberboard_by('id_member', $id_spon, array('board' => $board, 'status' => 1));
                if ( !$sponsorboard ) {
                    $level++;
                    continue;
                }

                // Check Downline Board Tree Exist
                $condtreedown   = array(
                    'id_board'  => $sponsorboard->id,
                    'board'     => $board,
                    'id_downline' => $id_member
                );
                $boardtreedown  = bp_get_boardtree_by('id_member', $id_spon, $condtreedown, 1);
                if ( $boardtreedown ) {
                    $level++;
                    continue;
                }

                $position       = 1;
                $position_max   = pow($board_tree, $level);
                $condtree       = array(
                    'id_board'  => $sponsorboard->id,
                    'board'     => $board,
                    'level'     => $level
                );

                $boardtree      = bp_get_boardtree_by('id_member', $id_spon, $condtree, 1);
                if ( $boardtree ) {
                    $postree    = isset($boardtree->position) ? $boardtree->position : 1;
                    if ( $postree >= $position_max ) {
                        $level++;
                        continue;
                    }
                    $position   = $postree + 1;
                }

                // Check Position Board Tree
                $condtree['position'] = $position;
                $checkboardtree     = bp_get_boardtree_by('id_member', $id_spon, $condtree, 1);
                if ( $checkboardtree ) {
                    $level++;
                    continue;
                }

                // Set Data Board Tree
                $data_board_tree    = array(
                    'id_board'      => $sponsorboard->id,
                    'id_member'     => $sponsorboard->id_member,
                    'username'      => $sponsorboard->username,
                    'id_downline'   => $id_member,
                    'downline'      => $memberboard->username,
                    'board'         => $board,
                    'level'         => $level,
                    'position'      => $position,
                    'status'        => 1,
                    'datecreated'   => $datetime,
                    'datemodified'  => $datetime
                );

                // Save Data Board Tree
                if ( ! $save_board_tree = $CI->Model_Member->save_data_board_tree($data_board_tree) ) {
                    $message = 'Simpan Board Tree tidak berhasil. Terjadi kesalahan pada transaksi !';
                    return false;
                }

                $ids_sponsor[$sponsorboard->id_member] = array(
                    'id_board'      => $sponsorboard->id,         
                    'id_member'     => $sponsorboard->id_member,                 
                );

                $level++;

            }
        }

        // -------------------------------------------------
        // Check Qualified Board
        // -------------------------------------------------
        if ( $ids_sponsor ) {
            foreach ($ids_sponsor as $key => $row) {
                $board_qualified    = $board_level ? true : false;
                $id_board           = isset($row['id_board']) ? $row['id_board'] : 0;
                $id_sponsor         = isset($row['id_member']) ? $row['id_member'] : 0;
                if ( !$id_board || !$id_sponsor ) {
                    continue;
                }

                $sponsordata        = bp_get_memberdata_by_id($id_sponsor);
                if ( !$sponsordata ) {
                    continue;
                }

                for ($gen=1; $gen<=$board_level; $gen++) {
                    $downline_max   = pow($gen, $board_tree); 
                    $condtree       = array(
                        'id_board'  => $id_board,
                        'board'     => $board,
                        'level'     => $gen
                    );
                    $countboardtree = bp_get_boardtree_by('id_member', $id_sponsor, $condtree, 0, true);
                    $countboardtree = $countboardtree ? $countboardtree : 0;
                    if ( $downline_max >= $countboardtree ) {
                        $board_qualified = false;
                    }
                }

                if ( $board_qualified ) {
                    $data_member_board  = array('status' => 2, 'datemodified' => $datetime, 'datequalified' => $datetime);
                    if ( ! $update_member_board = $CI->Model_Member->update_data_member_board($id_board, $data_member_board) ) {
                        $message = 'Update Data Member Board tidak berhasil. Terjadi kesalahan pada transaksi !';
                        return false;
                    }

                    // Calculate Bonus Board
                    $bonus_board        = bp_calculate_bonus_board($id_board, $datetime);

                    // Check Status Board Pending
                    $pendingboard       = bp_get_memberboard_by('id_member', $id_sponsor, array('board' => $board, 'status' => 0));
                    if ( $pendingboard ) {
                        $boardactived   = kb_actived_member_board($pendingboard, $datetime);
                    }

                    if ( $next_board > 1 && $next_board < 3 ) {
                        $memberboard    = bp_get_memberboard_by('id_member', $id_sponsor, array('board' => $next_board, 'status' => 1));
                        if( ! $memberboard && $sponsordata->status == ACTIVE ){
                            $saveboard      = kb_saved_member_board($sponsordata, $next_board, $datetime, 1);
                            if ( $saveboard ) {
                                kb_generate_board_tree($saveboard, $datetime);
                            }
                        }
                    }
                }
            }
        }

        return true;
    }
}

if ( !function_exists('kb_generate_downline_board_tree') )
{
    /**
     * Generate downline board tree
     * @param  integer  $id_member_board      ID Member
     * @param  date     $datetime             Datetime
     * @param  string   &$message       Message of status upgrade (reference variable)
     * @return boolean
     */
    function kb_generate_downline_board_tree( $id_member_board, $datetime = '', &$message = '' ) {
        $CI =& get_instance();

        $datetime = $datetime ? $datetime : date( 'Y-m-d H:i:s' );

        if ( !$id_member_board ){
            $message = 'ID Member Board tidak boleh kosong!';
            return false;
        }

        if ( ! $memberboard = bp_get_memberboard_by('id', $id_member_board) ){
            $message = 'Member Board tidak ditemukan!';
            return false;
        }

        if ( $memberboard->status != 1 ){
            $message = 'Status Member Board tidak aktif!';
            return false;
        }

        // -------------------------------------------------
        // Set Variable
        // -------------------------------------------------
        $id_member              = $memberboard->id_member;
        $board                  = $memberboard->board;
        $next_board             = $board + 1;
        $board_level            = config_item('board_level');
        $board_tree             = config_item('board_tree');
        $ids_sponsor            = array();

        // Get Data Member
        if ( ! $memberdata = bp_get_memberdata_by_id($id_member) ){
            $message = 'Member tidak ditemukan!';
            return false;
        }

        // -------------------------------------------------
        // Check Downline Sponsor
        // -------------------------------------------------
        $memberlevel            = $memberdata->gen;
        $maxlevel               = $memberlevel + $board_level;
        $params                 = array($maxlevel, $memberdata->tree_sponsor);
        $condition              = ' AND %status% >= 0 AND %level% <= ? AND %tree% LIKE CONCAT(?, "-%")';
        $order_by               = ' %level% ASC, %dateactived% ASC';
        $downlinedata           = $CI->Model_Member->get_all_member_board_data(0, 0, $condition, $order_by, $params);

        if ( $downlinedata ) {
            $level              = 1;
            foreach ($downlinedata as $key => $row) {
                $id_downline    = isset($row->id_member) ? $row->id_member : 0;
                $level_downline = isset($row->level) ? $row->level : 0;
                $level_downline = ($level_downline > $memberlevel) ? ($level_downline - $memberlevel) : 0;

                if ( $id_downline == 1 ) { continue; }
                if ( !$level_downline ) { continue; }
                if ( $level > $board_level) { break; }
                if ( $level_downline > $board_level) { break; }

                // Check Status Downline Board
                $downlineboard  = bp_get_memberboard_by('id_member', $id_downline, array('board' => $board, 'status' => 1));
                if ( !$downlineboard ) {
                    continue;
                }

                // Check Downline Board Tree Existd
                $condtreedown   = array(
                    'board'     => $board,
                    'level'     => $level_downline,
                    'id_downline' => $id_downline
                );
                $boardtreedown  = bp_get_boardtree_by('id_member', $id_member, $condtreedown, 1);
                if ( $boardtreedown ) {
                    continue;
                }

                $position_max   = pow($board_tree, $level_downline);
                $position       = 1;
                $condtree       = array(
                    'id_board'  => $memberboard->id,
                    'board'     => $board,
                    'level'     => $level_downline
                );

                // Check Level Position Board Tree
                $boardtree      = bp_get_boardtree_by('id_member', $id_member, $condtree, 1);
                if ( $boardtree ) {
                    $postree    = isset($boardtree->position) ? $boardtree->position : 1;
                    if ( $postree >= $position_max ) {
                        continue;
                    }
                    $position   = $postree + 1;
                }

                // Check Position Board Tree
                $condtree['position'] = $position;
                $checkboardtree     = bp_get_boardtree_by('id_member', $id_member, $condtree, 1);
                if ( $checkboardtree ) {
                    continue;
                }

                // Set Data Board Tree
                $data_board_tree    = array(
                    'id_board'      => $memberboard->id,
                    'id_member'     => $memberboard->id_member,
                    'username'      => $memberboard->username,
                    'id_downline'   => $id_downline,
                    'downline'      => $downlineboard->username,
                    'board'         => $board,
                    'level'         => $level_downline,
                    'position'      => $position,
                    'status'        => 1,
                    'datecreated'   => $datetime,
                    'datemodified'  => $datetime
                );

                // Save Data Board Tree
                if ( ! $save_board_tree = $CI->Model_Member->save_data_board_tree($data_board_tree) ) {
                    $message = 'Simpan Board Tree tidak berhasil. Terjadi kesalahan pada transaksi !';
                    return false;
                }

                $ids_sponsor[$key]  = array(
                    'id_board'      => $memberboard->id,         
                    'id_member'     => $memberboard->id_member,                 
                );
            }
        }

        // -------------------------------------------------
        // Check Qualified Board
        // -------------------------------------------------
        if ( $ids_sponsor ) {
            foreach ($ids_sponsor as $key => $row) {
                $board_qualified    = $board_level ? true : false;
                $id_board           = isset($row['id_board']) ? $row['id_board'] : 0;
                $id_sponsor         = isset($row['id_member']) ? $row['id_member'] : 0;
                if ( !$id_board || !$id_sponsor ) {
                    continue;
                }

                $sponsordata        = bp_get_memberdata_by_id($id_sponsor);
                if ( !$sponsordata ) {
                    continue;
                }

                for ($gen=1; $gen<=$board_level; $gen++) {
                    $downline_max   = pow($gen, $board_tree); 
                    $condtree       = array(
                        'id_board'  => $id_board,
                        'board'     => $board,
                        'level'     => $gen
                    );
                    $countboardtree = bp_get_boardtree_by('id_member', $id_sponsor, $condtree, 0, true);
                    $countboardtree = $countboardtree ? $countboardtree : 0;
                    if ( $downline_max >= $countboardtree ) {
                        $board_qualified = false;
                    }
                }

                if ( $board_qualified ) {
                    $data_member_board  = array('status' => 2, 'datemodified' => $datetime, 'datequalified' => $datetime);
                    if ( ! $update_member_board = $CI->Model_Member->update_data_member_board($id_board, $data_member_board) ) {
                        $message = 'Update Data Member Board tidak berhasil. Terjadi kesalahan pada transaksi !';
                        return false;
                    }

                    // Calculate Bonus Board
                    $bonus_board        = bp_calculate_bonus_board($id_board, $datetime);

                    // Check Status Board Pending
                    $pendingboard       = bp_get_memberboard_by('id_member', $id_sponsor, array('board' => $board, 'status' => 0));
                    if ( $pendingboard ) {
                        $boardactived   = kb_actived_member_board($pendingboard, $datetime);
                    }

                    if ( $next_board > 1 && $next_board < 3 && $sponsordata->status == ACTIVE ) {
                        $memberboard    = bp_get_memberboard_by('id_member', $id_sponsor, array('board' => $next_board, 'status' => 1));
                        $status_board   = ( $memberboard ) ? 0 : 1;
                        $saveboard      = kb_saved_member_board($sponsordata, $next_board, $datetime, $status_board);
                        if ( $saveboard ) {
                            kb_generate_board_tree($saveboard, $datetime);
                        }
                    }
                }
            }
        }

        return true;
    }
}

if ( !function_exists('kb_activation_ro') )
{
    /**
     * Activation RO
     * @param  object   $registrant     Data of Registrant
     * @param  object   $memberdata     Data of Member
     * @param  date     $datetime       Datetime
     * @param  string   &$message       Message of status upgrade (reference variable)
     * @return boolean
     */
    function kb_activation_ro( $registrant, $memberdata = '', $pin_id = '', $datetime = '', &$message = '' ) {
        $CI =& get_instance();

        $datetime       = $datetime ? $datetime : date( 'Y-m-d H:i:s' );
        $registrant     = $registrant ? $registrant : bp_get_current_member();
        $pin_id         = $pin_id ? bp_decrypt($pin_id) : false;

        if ( !$registrant ){
            $message = 'Pendaftar tidak valid!';
            return false;
        }

        if ( !$memberdata ){
            $message = 'Member tidak valid!';
            return false;
        }

        if ( !$pin_id ){
            $message = 'Data PIN tidak valid!';
            return false;
        }

        if ( !is_object($registrant) ){
            $message = 'Pendaftar tidak valid!';
            return false;
        }

        if ( !is_object($memberdata) ){
            $message = 'Member tidak valid!';
            return false;
        }

        $pindata                = $CI->Model_Shop->get_pin_by('id', $pin_id);
        if ( ! $pindata ) {
            $message = 'Data PIN tidak valid! ';
            return false;
        }


        if ( $pindata->status != 1 ) {
            $message = 'Status PIN tidak aktif!';
            return false;
        }

        // -------------------------------------------------
        // Begin Transaction
        // -------------------------------------------------
        $CI->db->trans_begin();

        // Set Data RO
        $data_ro                = array(
            'id_activator'      => $registrant->id,
            'id_member'         => $memberdata->id,
            'omzet'             => $pindata->bv,
            'amount'            => $pindata->amount,
            'pins'              => $pindata->id,
            'desc'              => '',
            'date'              => date('Y-m-d', strtotime($datetime)),
            'datecreated'       => $datetime,
            'datemodified'      => $datetime
        );

        // Save Data RO
        if ( ! $save_ro_id = $CI->Model_Member->save_data_ro($data_ro) ) {
            // Rollback Transaction
            $CI->db->trans_rollback();
            $message = 'RO tidak berhasil. Terjadi kesalahan pada transaksi !';
            return false;
        }

        // Update Data PIN
        $data_update_pin = array(
            'id_member_register'    => $registrant->id,
            'id_member_registered'  => $memberdata->id,
            'status'                => 2,
            'used'                  => 'ro',
            'dateused'              => $datetime
        );

        // update PIN status to 2 => used
        if ( ! $update_pin = $CI->Model_Shop->update_pin( $pindata->id, $data_update_pin) ) {
            $CI->db->trans_rollback();
            $message = 'RO tidak berhasil. Terjadi kesalahan data update data PIN Produk !';
            return false;
        }

        // Set Data Omzet RO
        $total_price            = $pindata->amount;
        $total_omzet_bv         = $pindata->bv;

        $data_member_omzet      = array(
            'id_member'         => $memberdata->id,
            'bv'                => $total_omzet_bv,
            'omzet'             => $total_omzet_bv,
            'amount'            => $total_price,
            'status'            => 'ro',
            'desc'              => 'RO',
            'date'              => date('Y-m-d', strtotime($datetime)),
            'datecreated'       => $datetime,
            'datemodified'      => $datetime
        );

        // Save Data Omzet RO
        if ( ! $insert_member_omzet = $CI->Model_Member->save_data_member_omzet($data_member_omzet) ) {
            $CI->db->trans_rollback();
            $message = 'RO tidak berhasil. Terjadi kesalahan pada proses transaksi simpan data omzet ro !';
            return false;
        }

        // -------------------------------------------------
        // Clone Member
        // -------------------------------------------------
        // Set Sponsor Data
        $sponsordata            = $memberdata;
        $sponsor_id             = $memberdata->id;

        // Get Upline Data
        $uplinedata             = bp_upline_available($sponsor_id);
        if ( !$uplinedata ) {
            $CI->db->trans_rollback();
            $message = 'RO tidak berhasil. Terjadi kesalahan pada transaksi clone data member (data upline) !';
            return false;
        }

        $upline_id              = $uplinedata->id;
        $position_node          = bp_check_node($upline_id);
        if ( $position_node ) {
            $position           = ( count($position_node) > 1 ) ? POS_LEFT : $position_node[0];
        } else {
            $CI->db->trans_rollback();
            $message = 'RO tidak berhasil. Terjadi kesalahan pada transaksi clone data member (position downline) !';
            return false;
        }

        // Set General Data Member
        $username               = $sponsordata->username .'_ro';
        $generate_username      = bp_generate_username_unique($username, 1);
        $password               = bp_decrypt($sponsordata->password_pin);
        $m_status               = 1;

        // Data Member
        $data_member            = array(
            'username'              => $generate_username,
            'password'              => $sponsordata->password,
            'password_pin'          => $sponsordata->password_pin,
            'type'                  => MEMBER,
            'package'               => $sponsordata->package,
            'sponsor'               => $sponsor_id,
            'parent'                => $upline_id,
            'position'              => $position,
            'name'                  => strtoupper($sponsordata->name),
            'pob'                   => $sponsordata->pob,
            'dob'                   => $sponsordata->dob,
            'gender'                => $sponsordata->gender,
            'marital'               => $sponsordata->marital,
            'idcard_type'           => $sponsordata->idcard_type,
            'idcard'                => $sponsordata->idcard,
            'npwp'                  => $sponsordata->npwp,
            'country'               => $sponsordata->country,
            'province'              => $sponsordata->province,
            'district'              => $sponsordata->district,
            'subdistrict'           => $sponsordata->subdistrict,
            'village'               => $sponsordata->village,
            'address'               => $sponsordata->address,
            'email'                 => $sponsordata->email,
            'phone'                 => $sponsordata->phone,
            'phone_home'            => $sponsordata->phone_home,
            'phone_office'          => $sponsordata->phone_office,
            'bank'                  => $sponsordata->bank,
            'bill'                  => $sponsordata->bill,
            'bill_name'             => $sponsordata->bill_name,
            'emergency_name'        => $sponsordata->emergency_name,
            'emergency_relationship'=> $sponsordata->emergency_relationship,
            'emergency_phone'       => $sponsordata->emergency_phone,
            'status'                => $m_status,
            'total_omzet'           => $total_omzet_bv,
            'uniquecode'            => 0,
            'datecreated'           => $datetime,
        );

        // -------------------------------------------------
        // Save Member
        // -------------------------------------------------
        if ( !$member_save_id = $CI->Model_Member->save_data($data_member) ) {
            $CI->db->trans_rollback();
            $message = 'RO tidak berhasil. Terjadi kesalahan pada proses transaksi clone data member !';
            return false;
        }

        if ($m_status == 1) {
            // Update Member Tree
            // -------------------------------------------------
            $gen                = $sponsordata->gen + 1;
            $level              = $uplinedata->level + 1;
            $tree               = bp_generate_tree($member_save_id, $uplinedata->tree);
            $tree_sponsor       = bp_generate_tree_sponsor($member_save_id, $sponsordata->tree_sponsor);
            $data_tree          = array('gen' => $gen, 'level' => $level, 'tree' => $tree, 'tree_sponsor' => $tree_sponsor);
            if ( !$update_tree = $CI->Model_Member->update_data_member($member_save_id, $data_tree) ) {
                $CI->db->trans_rollback();
                $message = 'RO tidak berhasil. Terjadi kesalahan pada proses transaksi clone data member !';
                return false;
            }

            // -------------------------------------------------
            // Generate Key Member
            // -------------------------------------------------
            $generate_key = bp_generate_key();
            bp_generate_key_insert($generate_key, ['id_member' => $member_save_id, 'name' => $sponsordata->name]);
        }

        if ( !$downline = bp_get_memberdata_by_id($member_save_id) ) {
            // Rollback Transaction
            $CI->db->trans_rollback();
            $message = 'RO tidak berhasil. Terjadi kesalahan pada proses transaksi clone data member !';
            return false;
        }

        $data_member_confirm    = array(
            'id_member'         => $registrant->id,
            'member'            => $registrant->username,
            'id_sponsor'        => $sponsordata->id,
            'sponsor'           => $sponsordata->username,
            'id_downline'       => $downline->id,
            'downline'          => $downline->username,
            'package'           => $downline->package,
            'status'            => $m_status,
            'access'            => 'ro',
            'omzet'             => 0,
            'uniquecode'        => 0,
            'nominal'           => 0,
            'datecreated'       => $datetime,
            'datemodified'      => $datetime,
        );

        $insert_member_confirm  = $CI->Model_Member->save_data_confirm($data_member_confirm);
        if (!$insert_member_confirm) {
            // Rollback Transaction
            $CI->db->trans_rollback();
            $message = 'RO tidak berhasil. Terjadi kesalahan pada proses transaksi clone data member confirm !';
            return false;
        }

        if ( $total_omzet_bv ) {
            // -------------------------------------------------
            // calculate bonus sponsor and bonus pairing
            // -------------------------------------------------
            $bonus_sponsor      = bp_calculate_bonus_sponsor($downline->id, $datetime);
            $bonus_pairing      = bp_calculate_pairing_bonus($downline->id, $datetime, false, true);
        }

        $saved_member_board     = kb_saved_member_board($downline, 1, $datetime);
        $check_member_board     = kb_check_member_board($sponsordata, 1, $datetime);

        // -------------------------------------------------
        // Commit or Rollback Transaction
        // -------------------------------------------------
        if ($CI->db->trans_status() === FALSE) {
            // Rollback Transaction
            $CI->db->trans_rollback();
            $message = 'RO tidak berhasil. Terjadi kesalahan pada proses transaksi !';
            return false;
        }

        // Commit Transaction
        $CI->db->trans_commit();
        // Complete Transaction
        $CI->db->trans_complete();

        bp_log_action('MEMBER_RO', $sponsordata->username, $registrant->username, json_encode(array('cookie' => $_COOKIE, 'status' => 'SUCCESS')));
        bp_log_action('MEMBER_REG_RO', $username, $registrant->username, json_encode(array('cookie' => $_COOKIE, 'status' => 'SUCCESS', 'form' => 'RO')));

        // Send Notif Email
        // $CI->bp_email->send_email_new_member($downline, $sponsordata, $password);
        $CI->bp_email->send_email_sponsor($downline, $sponsordata);

        $sponsorname    = $sponsordata->username . ' / ' . $sponsordata->name;
        $message        = '
            <div class="row">
                <h5 class="heading-small">Data New Member :</h5>
                <div class="col-sm-3"><small class="text-capitalize text-muted">' . lang('username') . '</small></div>
                <div class="col-sm-9"><small class="text-lowecase font-weight-bold">' . $username . '</small></div>
            </div>
            <div class="row">
                <div class="col-sm-3"><small class="text-capitalize text-muted">' . lang('name') . '</small></div>
                <div class="col-sm-9"><small class="text-uppercase font-weight-bold">' . $sponsordata->name . '</small></div>
            </div>
            <hr class="mt-2 mb-2">
            <div class="row">
                <div class="col-sm-3"><small class="text-capitalize text-muted">Sponsor</small></div>
                <div class="col-sm-9"><small class="font-weight-bold">' . $sponsorname . '</small></div>
            </div>';

        return true;
    }
}

if (!function_exists('bp_check_node')) {
    /**
     *
     * Check your first node available
     * @param   Int $id_member Member ID
     * @param   String $position Position of Member
     * @return Mixed, Boolean false if invalid member id, otherwise array of node available
     */
    function bp_check_node($id_member, $position = '')
    {
        if(!is_numeric($id_member)) return false;

        $id_member = absint($id_member);
        if(!$id_member) return false;

        $CI =& get_instance();

        if(!empty($position)) {
            $nodedata = $CI->Model_Member->get_node_available($id_member, FALSE, $position);
            if(!empty($nodedata)) {
                return $nodedata[0];
            }
            return false;
        } else {
            $nodedata   = $CI->Model_Member->get_node_available($id_member);
            $rows       = count($nodedata);
            $node       = array();

            if($rows == 0) {
                $node   = array();
                $node[] = POS_LEFT;
                $node[] = POS_RIGHT;
            } elseif($rows == 1) {
                $node   = array();
                if($nodedata[0]->position == POS_LEFT) $node[] = POS_RIGHT;
                else $node[] = POS_LEFT;
            } else {
                $node   = '';
            }
            return $node;
        }
    }
}

if (!function_exists('bp_avatar')) {
    /**
     * Get node
     * @author  Yuda
     * @param   Int $id_member (Required)  Member ID
     * @param   Boolean $new (Optional)  New Member
     * @return  Mixed, Boolean if wrong data of id member, otherwise data or node
     */
    function bp_avatar($id_member, $photo_me = '', $id_sponsor = 0, $showct = TRUE)
    {
        $avatar         = '';
        if(!is_numeric($id_member)) return $avatar;
        $id_member      = absint($id_member);
        if(!$id_member) return $avatar;

        $member         = bp_get_memberdata_by_id($id_member);
        if(!$member || empty($member)) return $avatar;

        $is_admin       = as_administrator($member);

        $sponsordata    = $id_sponsor ? bp_get_memberdata_by_id($id_sponsor) : 0;

        // AVATAR IMAGE
        $avt_img        = 'user.jpg';
        if ( $member->status != 1 ) {
            $avt_img    = 'user_notactive.jpg';
        }
        $avt_img        = $is_admin ? 'user.jpg' : $avt_img;
        // END AVATAR IMAGE

        $avatar         = '<div class="photo-wrapper '.$photo_me.'">';
        $avatar         = $is_admin ? '<div class="photo-wrapper photo-me">' : $avatar;
        if ( $sponsordata ) {
            $sponsored  = 'SPONSOR : ' . $sponsordata->username . '<br/>' . strtoupper($sponsordata->name);
            $avatar     = '<div class="photo-wrapper" title="' . $sponsored . '<br/>Join : '.date('d-M-y', strtotime($member->datecreated)).'">';
        }
        $avatar        .= '<div class="photo-content">';
        $avatar        .= '<div class="photo-image">';
        $avatar        .= '<img src="' . BE_TREE_PATH . $avt_img . '" />';
        $avatar        .= '</div>';
        $avatar        .= '</div>';
        $avatar        .= $is_admin ? '<div class="photo-name admin">' : '<div class="photo-name ' . $member->package . '">';
        $avatar        .= $member->username;
        $avatar        .= '</div>';
        $avatar        .= '<div class="photo-name2"><span>' . $member->name . '</span></div>';

        $avatar        .= bp_node($member->id, false, $showct);
        $avatar        .= '</div>';

        return $avatar;
    }
}

if (!function_exists('bp_node')) {
    /**
     * Get node
     * @author  Yuda
     * @param   Int $id_member (Required)  Member ID
     * @param   Boolean $new (Optional)  New Member
     * @return  Mixed, Boolean if wrong data of id member, otherwise data or node
     */
    function bp_node($id_member, $new = false, $showct = false)
    {
        if(!is_numeric($id_member)) return false;

        $id_member  = absint($id_member);
        if(!$id_member) return false;

        $member     = bp_get_memberdata_by_id($id_member);
        if(!$member || empty($member)) return false;

        if($new == true) {
            $node = '
            <div class="phone-node row">
            <div class="col-6 node-one" style="padding:0px !important">-</div>
            <div class="col-6 node-two" style="padding:0px !important">-</div>
            </div>
            ';
        } else {

            if ( $showct && $member->type == MEMBER ) {
                $tree_left          = bp_count_childs($id_member, POS_LEFT, TRUE);                              // Count Result Tree Left 
                $count_left         = isset($tree_left['total_downline']) ? $tree_left['total_downline'] : 0;   // Count Downline Left 
                $pair_left          = isset($tree_left['total_pairing']) ? $tree_left['total_pairing'] : 0;     // Count Pairing Left 
                
                $tree_right         = bp_count_childs($id_member, POS_RIGHT, TRUE);                             // Count Result Tree Right
                $count_right        = isset($tree_right['total_downline']) ? $tree_right['total_downline'] : 0; // Count Downline Right
                $pair_right         = isset($tree_right['total_pairing']) ? $tree_right['total_pairing'] : 0;   // Count Pairing Right

                if ( $pair_qualified = bp_count_pairing_qualified($id_member) ) {
                    $pair_left      = $pair_left - $pair_qualified;
                    $pair_left      = ($pair_left < 0 ? 0 : $pair_left);
                    $pair_right     = $pair_right - $pair_qualified;
                    $pair_right     = ($pair_right < 0 ? 0 : $pair_right);
                }
                
                $node = '
                    <div class="phone-node row" style="padding:0px">
                        <div class="col-6 node-one"  style="padding:0px !important">
                            <b>L</b>: ' . $count_left . '<br />
                        </div>
                        <div class="col-6 node-two"  style="padding:0px !important">
                            <b>R</b>: ' . $count_right . '<br />
                        </div>
                    </div>
                    <div class="phone-node row" style="padding:0px">
                        <div class="col-6 node-one"  style="padding:0px !important">
                            <b>P</b> : ' . $pair_left . '<br />
                        </div>
                        <div class="col-6 node-two"  style="padding:0px !important">
                            <b>P</b> : ' . $pair_right . '<br />
                        </div>
                    </div>'; 

            } else {
                $child_left     = bp_count_childs($id_member, POS_LEFT, FALSE, 'childs');   
                $child_right    = bp_count_childs($id_member, POS_RIGHT, FALSE, 'childs');   

                $node = '
                    <div class="phone-node row">
                        <div class="col-6 node-one"  style="padding:0px !important">
                            L: ' . $child_left . '<br />
                        </div>
                        <div class="col-6 node-two"  style="padding:0px !important">
                            R: ' . $child_right . '<br />
                        </div>
                    </div>';
            }

        }

        return $node;
    }
}

if(!function_exists('bp_avatar_board')) {
    /**
     * Get node
     * @author  Yuda
     * @param   Int $id_member (Required)  Member ID
     * @param   Boolean $new (Optional)  New Member
     * @return  Mixed, Boolean if wrong data of id member, otherwise data or node
     */
    function bp_avatar_board($id_member, $board = 1, $photo_me = '', $id_sponsor = 0, $id_board = 0, $showct = TRUE)
    {
        $avatar         = '';
        if(!is_numeric($id_member)) return $avatar;
        if(!is_numeric($board)) return $avatar;
        
        $sponsor        = 0;
        $id_member      = absint($id_member);
        if(!$id_member) return $avatar;

        $board          = absint($board);
        if(!$board) return $avatar;

        $CI =& get_instance();

        if ( $id_board ) {
            $memberboard    = bp_get_memberboard_by('id_member', $id_member, array('id' => $id_board));
        } else {
            $memberboard    = bp_get_memberboard_by('id_member', $id_member, array('board' => $board, 'status' => 1));
            if ( ! $memberboard ) {
                $memberboard = bp_get_memberboard_by('id_member', $id_member, array('board' => $board, 'status' => 2));
            }
        }

        $username       = isset($memberboard->username) ? $memberboard->username : 0;
        $status_board   = isset($memberboard->status) ? $memberboard->status : 0;
        $sponsor_board  = isset($memberboard->sponsor) ? $memberboard->sponsor : 0;

        if ( $memberboard ) {
            $sponsor    = $id_sponsor ? bp_get_memberdata_by_id($memberboard->sponsor) : 0;
        }

        // AVATAR IMAGE
        $avt_img        = 'user.jpg';
        $photo_class    = 'photo-name basic';
        if ( $status_board == 0 ) {
            $avt_img    = 'user-lock.jpg';
            $username   = 'Not Available';
            $photo_class = 'photo-name-notavailable';
        }

        $avatar         = '<div class="photo-wrapper-board '.$photo_me.'">';
        if ( $sponsor ) {
            $sponsored  = 'Sponsor : ' . $sponsor->username;
            $avatar     = '<div class="photo-wrapper-board" title="' . $sponsored . '<br/>Tgl Aktif : '.date('Y-m-d', strtotime($memberboard->dateactived)).'">';
        }
        $avatar        .= '<div class="photo-content-board">';
        $avatar        .= '<div class="photo-image">';
        $avatar        .= '<img src="' . BE_TREE_PATH . $avt_img . '" />';
        $avatar        .= '</div>';
        $avatar        .= '</div>';
        $avatar        .= '<div class="'. $photo_class .'">';
        $avatar        .= $username;
        $avatar        .= '</div>';
        // $avatar        .= '<div class="photo-name2"><span>' . $member->name . '</span></div>';

        $new            = ( $status_board > 0 ) ? false : true;
        $avatar        .= bp_node_board($id_member, $board, $new);
        $avatar        .= '</div>';
        // END AVATAR IMAGE

        return $avatar;
    }
}

if(!function_exists('bp_node_board')) {
    /**
     * Get node
     * @author  Yuda
     * @param   Int $id_member (Required)  Member ID
     * @param   Boolean $new (Optional)  New Member
     * @return  Mixed, Boolean if wrong data of id member, otherwise data or node
     */
    function bp_node_board($id_member, $board = 1, $new = false)
    {
        if(!is_numeric($id_member)) return false;
        if(!is_numeric($board)) return false;

        $id_member  = absint($id_member);
        if(!$id_member) return false;

        $board  = absint($board);
        if(!$board) return false;

        $CI =& get_instance();

        return false;

        if($new == true) {
            $node = '
            <div class="phone-node row mt-2">
            <div class="col-4 node-one" style="padding:0px !important">-</div>
            <div class="col-4 node-two" style="padding:0px !important">-</div>
            <div class="col-4 node-two" style="padding:0px !important">-</div>
            </div>
            ';
        } else {
            // $child_left     = $CI->Model_Member->count_childs_board($id_member, $board, POS_LEFT);
            // $child_center   = $CI->Model_Member->count_childs_board($id_member, $board, POS_CENTER);
            // $child_right    = $CI->Model_Member->count_childs_board($id_member, $board, POS_RIGHT);
            $child_left     = 0;
            $child_center   = 0;
            $child_right    = 0;

            $node = '
                <div class="phone-node row mt-2">
                    <div class="col-4 node-one"  style="padding:0px !important">
                        L: ' . $child_left . '<br />
                    </div>
                    <div class="col-4 node-two"  style="padding:0px !important">
                        C: ' . $child_center . '<br />
                    </div>
                    <div class="col-4 node-two"  style="padding:0px !important">
                        R: ' . $child_right . '<br />
                    </div>
                </div>';

        }

        return $node;
    }
}

if(!function_exists('bp_count_childs')) {
    /**
     * Counts childs of member
     * @author  Yuda
     * @param   Int $id_member (Required)  Member ID
     * @param   String $position (Required)  Position Of Node, value ('kiri' or 'kanan')
     * @param   Boolean $tree (Optional)  Get Only Tree
     * @param   String $cfg (Required)  Point Of Node, value ('all' or 'childs' or 'pairing')
     * @param   Date $datecreated (Optional)  Date Join of member
     * @return  Int of child number
     */
    function bp_count_childs($id_member, $position = '', $tree = true, $cfg = 'all', $datecreated = '', $equaldate = false)
    {
        $CI =& get_instance();
        return $CI->Model_Member->count_childs($id_member, $position, $tree, $cfg, $datecreated, $equaldate);
    }
}

if(!function_exists('bp_count_pairing')) {
    /**
     * Counts Point Pairing of member
     * @author  Yuda
     * @param   Int $id_member (Required)  Member ID
     * @param   String $position (Required)  Position Of Node, value ('kiri' or 'kanan')
     * @param   Date $datecreated (Optional)  Date Join of member
     * @param   Boolean $equaldate (Optional)  Get Only One Day
     * @return  Int of child number
     */
    function bp_count_pairing($id_member, $position = POS_LEFT, $datecreated = '', $equaldate = false)
    {
        $CI =& get_instance();
        $pair_point     = bp_count_childs($id_member, $position, false, 'pairing', $datecreated, $equaldate);

        if ( $pair_qualified = bp_count_pairing_qualified($id_member) ) {
            $pair_point = $pair_point - $pair_qualified;
            $pair_point = ($pair_point < 0 ? 0 : $pair_point);
        }
        return $pair_point;
    }
}

if(!function_exists('bp_count_pairing_qualified')) {
    /**
     * Counts Pairing Qualified of member
     * @author  Yuda
     * @param   Int $id_member (Required)  Member ID
     * @return  Int of child number
     */
    function bp_count_pairing_qualified($id_member, $count_total = true, $datecreated = '', $equal = false)
    {
        $CI =& get_instance();
        return $CI->Model_Member->count_pairing_qualified($id_member, $count_total, $datecreated, $equal);
    }
}

if ( !function_exists('bp_save_pair_qualified') )
{
    /**
     * Save Point Pair of member
     * @author  Yuda
     * @param   Array   $data      (Required)  Data Pair Qualified Member
     * @return  Boolean False or True 
     */
    function bp_save_pair_qualified($data) {
        $CI =& get_instance();
        return $CI->Model_Member->save_pair_qualified($data);
    }
}

if (!function_exists('bp_calc_tax')) {
    /**
     * Calculate Pajak
     */
    function bp_calc_tax($nominal, $npwp = '')
    {
        if (!$nominal || !is_numeric($nominal)) return 0;

        $tax_npwp       = 0;
        $tax_non_npwp   = 0;

        if ($_tax_npwp = get_option('setting_withdraw_tax_npwp')) {
            $tax_npwp   = $_tax_npwp;
        }

        if ($_tax_non_npwp = get_option('setting_withdraw_tax')) {
            $tax_non_npwp   = $_tax_non_npwp;
        }

        if (!$tax_npwp && !$tax_non_npwp) {
            return 0;
        }

        if ($npwp == '__.___.___._-___.___') {
            $npwp = '';
        }

        $npwp   = trim($npwp);
        $tax    = $tax_non_npwp;

        if (!empty($npwp)) {
            $tax = $tax_npwp;
        }

        $calc_tax = ($nominal * $tax) / 100;
        return round($calc_tax);
    }
}

if (!function_exists('bp_member_pin')){
    /**
     *
     * Get member pin
     * @param   Int     $id_member  (Required)  Member ID
     * @param   String  $status     (Optional)  Status of Pin, default 'all'
     * @param   Boolean $count      (Optional)  Count PIN, default 'false'
     * @param   String  $product    (Optional)  Product of Pin, default ''
     * @return Mixed, Boolean false if invalid member id, otherwise array of member pin
     */
    function bp_member_pin($id_member, $status='all', $count=true, $product=''){
        if ( !is_numeric($id_member) ) return false;

        $id_member  = absint($id_member);
        if ( !$id_member ) return false;

        $CI =& get_instance();

        $pins    = $CI->Model_Shop->get_pins($id_member, $status, $count, $product);

        return $pins;
    }
}

if (!function_exists('bp_use_pin')){
    /**
     * Use PIN
     *
     * @since 1.0.0
     * @access public
     *
     * @param int       $id_member  ID Member of PIN owner
     * @param int       $qty        Quantity PIN used.
     * @param string    $product    Product ID
     * @return boolean
     */
    function bp_use_pin( $id_member, $qty, $product, $id_member_registered = 0, $datetime = '', $used_for = 'register' ) {
        if ( empty( $id_member ) || empty( $qty ) || empty( $product ) )
            return false;

        $CI =& get_instance();

        // get active pins
        // lock row of pin used so they are not used by another DB connection
        if ( ! $pin_active = $CI->Model_Shop->get_pins_with_lock( $id_member, $product, 1, $qty ) )
            return false;

        // check if active pin is sufficient
        if ( count( $pin_active ) < $qty )
            return false;

        $used_pin       = array_slice( $pin_active, 0, $qty );
        $used_pin_ids   = array();
        foreach( $used_pin as $pin ){
            $used_pin_ids[] = $pin->id;
        }

        $datetime       = $datetime ? $datetime : date('Y-m-d H:i:s');

        // update PIN status to 2 => used
        return $CI->Model_Shop->update_pin( $used_pin_ids, array(
            'id_member_register'    => $id_member,
            'id_member_registered'  => $id_member_registered,
            'status'                => 2,
            'used'                  => $used_for,
            'dateused'              => $datetime
        ));

        // trans complete
        $CI->db->trans_complete();
        return true;
    }
}


if (!function_exists('bp_notification_email_template')) {
    /**
     * Get notification template
     * 
     * @since 1.0.0
     * @access public
     * 
     * @param string $message
     * @return array 
     * 
     * @author Yuda
     */
    function bp_notification_email_template($message = "", $title = "")
    {
        $company_name       = get_option('company_name');
        $company_name       = $company_name ? $company_name : COMPANY_NAME;
        $template_open      = '
        <style>
            pre{ background-color: transparent; color: #FFFFFF; border:none; padding: 0px 10px 10px; }
        </style>
        <body class="clean-body" style="margin: 0; padding: 20px 0px; -webkit-text-size-adjust: 100%; background-color: #F5F5F5; font-family:Roboto,Arial,Helvetica,sans-serif;">
            <div style="background-color:transparent; margin: 0 auto; min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word;">';

        $template_header = '
            <div style="background-color:#FFFFFF; display: block;">
                <div style="width:100% !important;">
                    <div style="border:0px solid transparent; padding: 25px 10px;">
                        <div style="padding: 0px; text-align: center;">
                            <!--<img src="' . BE_IMG_PATH . 'logo.png" alt="' . $company_name . '" width="20%">-->
                            <h1 style="margin: 0px; margin-top: 10px; font-size:18px; font-weight:bold; color:#5e72e4">' . $company_name . '</h1>
                        </div>
                    </div>
                </div>
            </div>';

        $template_body = '
            <div style="background-color:#FFFFFF; display: block; padding: 0px; font-size: 14px;">
                <div style="background: linear-gradient(87deg,#17acc7 0,#1171ef 100%)!important; padding: 20px; color: #FFFFFF;">
                    ' . (empty($title) ? '' : '<div style="text-align: center;"><h3 style="font-size:18px; color:white">' . $title . '</h3><hr/></div>') . '
                    ' . (empty($message) ? '<div style="text-align: center;">Email Notifikasi ini tidak memiliki pesan</div>' : $message) . '
                </div>
            </div>';

        $template_footer = '
            <div style="background-color:#FFFFFF;">
                <div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent;">
                    <div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                        <div class="col num12" style="min-width: 320px; max-width: 650px; display: table-cell; vertical-align: top; width: 650px;">
                            <div style="width:100% !important;">
                                <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:20px; padding-bottom:30px; padding-right: 0px; padding-left: 0px;">
                                    <table border="0" cellpadding="0" cellspacing="0" class="divider" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;" valign="top" width="100%">
                                        <tbody>
                                            <tr style="vertical-align: top;" valign="top">
                                                <td class="divider_inner" style="word-break: break-word; vertical-align: top; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding: 10px;" valign="top">
                                                    <table align="center" border="0" cellpadding="0" cellspacing="0" class="divider_content" height="0" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 60%; border-top: 1px dotted #C4C4C4; height: 0px;" valign="top" width="60%">
                                                        <tbody>
                                                            <tr style="vertical-align: top;" valign="top">
                                                                <td height="0" style="word-break: break-word; vertical-align: top; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;" valign="top"><span></span></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div style="color:#5F5F5F; line-height:120%; padding: 10px;">
                                        <div style="font-size: 12px; line-height: 14px; color: #5F5F5F;">
                                            <p style="font-size: 12px; line-height: 16px; text-align: center; margin: 0;">
                                                <strong>' . COMPANY_NAME . ' &copy; 2021. All Right Reserved</strong>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';

        $template_close = '
            <div>
        </body>';

        $template           = $template_open . $template_header . $template_body . $template_footer . $template_close;
        return $template;
    }
}

if (!function_exists('bp_notification_shop_template')) {
    /**
     * Get notification template
     * 
     * @since 1.0.0
     * @access public
     * 
     * @param string $message
     * @return array 
     * 
     * @author Yuda
     */
    function bp_notification_shop_template($shop_order = "", $subject = "", $usertype = '', $member = null)
    {
        $CI = &get_instance();
        $CI->load->helper('shop_helper');
        $currency           = config_item('currency');
        $server_name        = isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : DOMAIN_NAME;
        $company_name       = get_option('company_name');
        $company_name       = !empty($company_name) ? $company_name : COMPANY_NAME;
        $dateorder          = !empty($shop_order->datecreated) ? date_indo($shop_order->datecreated, 'datetime') : '';

        $title              = '
            <div style="text-align: center;">
                <p style="font-size:18px;">' . (empty($subject) ? 'Informasi Pesanan Produk' : $subject) . '</p>
                <hr>
                <p style="margin: 10px 0px 0px;">
                    <span style="font-size: 16px; font-weight:600; color: #fff;">Invoice </span>
                    <span style="font-size: 16px; font-weight:600; color: #ff8d2b;">' . $shop_order->invoice . '</span>
                </p>
                <p style="font-size: 12px; margin-top: 0px; color:#ddd">' . $dateorder . '</p>
                <hr/><br>
            </div>';

        $message            = '';
        $notif              = '';

        if ($usertype == 'stockist') {
            $text_message   = '';
            if ($shop_order->status == 0) {
                $notif = $CI->Model_Option->get_notification_by('slug', 'notification-new-order-stockist', 'email');

                $text_message   = '<p style="margin: 3px 0px;">Selamat ada pesanan produk dari member <b>' . $company_name . '</b>. Berikut informasi data pesanan member :</p>';
            }
            if ($shop_order->status == 1) {
                $notif = $CI->Model_Option->get_notification_by('slug', 'notification-confirmation-order-stockist', 'email');
                $text_message   = '<p style="margin: 3px 0px;">Pesanan produk dari member telah di konfirmasi. Berikut informasi data pesanan member :</p>';
            }
            if ($shop_order->status == 4) {
                $notif = $CI->Model_Option->get_notification_by('slug', 'notification-cancelation-order-stockist', 'email');
                $text_message   = '<p style="margin: 3px 0px;">Pesanan member telah dibatalkan. Berikut informasi data pesanan member :</p>';
            }
            $message        = '<p style="line-height: 1.2;">Halo <b>' . $shop_order->name . '</b></p>' . $text_message;
        } else {
            if ($member) {
                $text_message   = '';
                if ($shop_order->status == 0) {
                    $notif = $CI->Model_Option->get_notification_by('slug', 'notification-new-order-member', 'email');
                    $text_message   = '<p style="margin: 3px 0px;">Terima kasih sudah berbelanja di <b>' . $company_name . '</b>. Sebagai konfirmasi, berikut informasi data Pesanan anda :</p>';
                }
                if ($shop_order->status == 1) {
                    $notif = $CI->Model_Option->get_notification_by('slug', 'notification-confirmation-order-member', 'email');
                    $text_message   = '<p style="margin: 3px 0px;">Selamat pesanan produk anda telah di konfirmasi. Berikut informasi data Pesanan anda :</p>';
                }
                if ($shop_order->status == 4) {
                    $notif = $CI->Model_Option->get_notification_by('slug', 'notification-cancelation-order-member', 'email');
                    $text_message   = '<p style="margin: 3px 0px;">Pesanan telah dibatalkan. Berikut informasi data pesanan anda :</p>';
                }
                $message        = '<p style="line-height: 1.2;">Halo <b>' . $member->name . ' (' . $member->username . ')</b></p>' . $text_message;
            }
        }

        $product_detail     = '
        <table class="table no-wrap table-responsive" style="margin-bottom: 30px;width: 100%;line-height: inherit;text-align: left;">
            <thead>
                <tr class="heading">
                    <th colspan="2" style="background: #eee;border-bottom: 1px solid #ddd;font-weight: bold;padding: 10px;">Produk</th>
                    <th style="width:30%;text-align: right;background: #eee;border-bottom: 1px solid #ddd;font-weight: bold;padding: 10px;">Total</th>
                </tr>
            </thead>
            <tbody>';

        if (is_serialized($shop_order->products)) {
            $unserialize_data = maybe_unserialize($shop_order->products);
            foreach ($unserialize_data as $row) {
                $idMaster       = $row['id'];
                $image          = '';
                if ($get_data_product = bp_products($idMaster)) {
                    $image      = $get_data_product->image;
                }
                $img_src        = bp_product_image($image);

                $product_name   = isset($row['name']) ? $row['name'] : 'Produk';
                $bv             = isset($row['bv']) ? $row['bv'] : 'Produk';
                $qty            = isset($row['qty']) ? $row['qty'] : 0;
                $price          = isset($row['price']) ? $row['price'] : 0;
                $price_cart     = isset($row['price_cart']) ? $row['price_cart'] : 0;
                $discount       = isset($row['discount']) ? $row['discount'] : 0;
                $subtotal       = $qty * $price_cart;

                if ($price > $price_cart) {
                    $price_prod = '( <s style="font-size: 11px">' . bp_accounting($price) . '</s> <span style="color:#fb6340;font-size: 11px">' . bp_accounting($price_cart, $currency) . '</span> )';
                } else {
                    $price_prod = bp_accounting($price_cart, $currency);
                }

                $product_detail     .= '
                    <tr>
                        <td style="width:70px; vertical-align:top; padding-top:5px">
                            <img src="' . $img_src . '" style="width: 70px;float: left;">
                        </td>
                        <td style="text-align: left;text-transform: capitalize;padding: 5px;vertical-align: top;border-bottom: 1px solid #eee;">
                            <span style="font-size: 12px;font-weight:600; margin-bottom: 3px;display: block;">' . $product_name . '</span>
                            <span style="font-size: 10px;display:block;margin-bottom:2px">
                                BV: ' . bp_accounting($bv) . '
                            </span>
                            <span style="font-size: 10px;display:block;margin-bottom:2px">
                                Harga: ' . $price_prod . '
                            </span>
                            <span style="font-size: 10px; font-weight:600;">Qty: ' . $qty . '</span>
                        </td>
                        <td class="text-center" style="text-align: right;padding: 5px;vertical-align: top;border-bottom: 1px solid #eee;white-space: nowrap;">
                            ' . bp_accounting($subtotal)  . '
                        </td>
                    </tr>
                ';
            }
        }

        $uniquecode         = str_pad($shop_order->unique, 3, '0', STR_PAD_LEFT);
        $cfg_pay_method     = config_item('payment_method');
        $payment_method     = isset($cfg_pay_method[$shop_order->payment_method]) ? $cfg_pay_method[$shop_order->payment_method] : $shop_order->payment_method;
        $product_detail     .= '
                <tr>
                    <td colspan="2" style="text-align:right;padding:5px;vertical-aligntop;white-space:nowrap;font-weight:500; color:#666; font-size:13px">Subtotal</td>
                    <td style="text-align:right;padding:5px;vertical-align:top;white-space:nowrap;font-weight:bold; color:#666; font-size:13px">
                        ' . bp_accounting($shop_order->subtotal) . '
                    </td>
                </tr>
                ' . (($shop_order->unique) ? '
                    <tr>
                        <td colspan="2" style="text-align:right;padding:5px;vertical-aligntop;white-space:nowrap;font-weight:500; color:#666; font-size:13px">Kode Unik</td>
                        <td style="text-align:right;padding:5px;vertical-align:top;white-space:nowrap;font-weight:bold; color:#666; font-size:13px">
                            ' . ($uniquecode) . '
                        </td>
                    </tr>' : '') . '
                ' . (($shop_order->shipping) ? '
                    <tr>
                        <td colspan="2" style="text-align:right;padding:5px;vertical-aligntop;white-space:nowrap;font-weight:500; color:#666; font-size:13px">Biaya Pengiriman</td>
                        <td style="text-align:right;padding:5px;vertical-align:top;white-space:nowrap;font-weight:bold; color:#666; font-size:13px">
                            ' . bp_accounting($shop_order->shipping) . '
                        </td>
                    </tr>' : '') . '
                ' . (($shop_order->discount) ? '
                    <tr>
                        <td colspan="2" style="text-align:right;padding:5px;vertical-aligntop;white-space:nowrap;font-weight:500; color:#666; font-size:13px">
                            ' . lang('discount') . ' ' . ($shop_order->voucher ? ' (<span style="font-size:10px; color:#5e72e4">' . $shop_order->voucher . '</span>)' : '') . '
                        </td>
                        <td style="text-align:right;padding:5px;vertical-align:top;white-space:nowrap;font-weight:bold; color:#666; font-size:13px">
                            ' . bp_accounting($shop_order->discount) . '
                        </td>
                    </tr>' : '') . '
                <tr>
                    <td colspan="2" style="text-align:right;padding:5px;vertical-aligntop;white-space:nowrap;font-weight:500; color:#666; font-size:13px">' . lang('payment_method') . '</td>
                    <td style="text-align:right;padding:5px;vertical-align:top;white-space:nowrap;font-weight:bold; color:#666; font-size:13px">
                        ' . $payment_method . '
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align:right;padding:5px;vertical-aligntop;white-space:nowrap;font-weight:bold; color:#666; font-size:15px">
                        ' . lang('total_payment') . '
                    </td>
                    <td style="text-align:right;padding:5px;vertical-align:top;white-space:nowrap;font-weight:bold; color:#fb6340; font-size:15px">
                        ' . bp_accounting($shop_order->total_payment, $currency) . '
                    </td>
                </tr>
            </tbody>
        </table>';

        // Information Shipping Address
        $address            = ucwords(strtolower($shop_order->address)) . ', ' . $shop_order->village . br();
        $address           .= 'Kec '. $shop_order->subdistrict . ' ' . $shop_order->district .br();
        $address           .= $shop_order->province;
        $address           .= ($shop_order->postcode) ? ' (' . $shop_order->postcode . ')' : '';

        // shipping method
        $shipping_title     = 'Alamat Pengiriman';
        $_shipping          = '';
        if ( $shop_order->shipping_method == 'ekspedisi' ) {
            $_shipping  = 'Jasa Ekspedisi / Pengiriman';
            if ( $shop_order->courier ) {
                $_shipping  = strtoupper($shop_order->courier);
                if ( $shop_order->service ) {
                    $_shipping  .= ' (' . strtoupper($shop_order->service) .')';
                }
            }
        }
        if ( $shop_order->shipping_method == 'pickup' ) {
            $shipping_title = 'Alamat Penagihan';
            $_shipping      = 'Pickup';
        }

        $username_member    = '';
        if ( $usertype == 'stockist' && $shop_order->id_stockist > 0 ) {
            if ( $memberdata = bp_get_memberdata_by_id($shop_order->id_member) ) {
                $username_member = '
                <tr class="item">
                    <td style="width: 20%;padding: 5px 10px;vertical-align: top;border-bottom: 1px solid #eee;">' . lang("username") . '</td>
                    <td style="width: 2%x;">:</td>
                    <td style="width: 78px;padding: 5px;vertical-align: top;border-bottom: 1px solid #eee;white-space: nowrap;">
                        ' . strtolower($memberdata->username) . '
                    </td>
                </tr>';
            }
        }

        $shipping_detail    = '
        <table class="table" style="margin-bottom: 20px;width: 100%;line-height: inherit;text-align: left;">
            <tr class="heading">
                <td colspan="3" style="background: #eee;border-bottom: 1px solid #ddd;padding: 10px;"><b>Metode Pengiriman </b> : '. $_shipping .'</td>
            </tr>
            <tr class="heading">
                <th colspan="3" style="width: 100%;background: #eee;border-bottom: 1px solid #ddd;font-weight: bold;padding: 10px;">' . $shipping_title . '</th>
            </tr>
            '.$username_member.'
            <tr class="item">
                <td style="width: 20%;padding: 5px 10px;vertical-align: top;border-bottom: 1px solid #eee;">' . lang("name") . '</td>
                <td style="width: 2%x;">:</td>
                <td style="width: 78px;padding: 5px;vertical-align: top;border-bottom: 1px solid #eee;white-space: nowrap;">
                    ' . ucwords(strtolower($shop_order->name)) . '
                </td>
            </tr>
            <tr class="item">
                <td style="width: 20%;padding: 5px 10px;vertical-align: top;border-bottom: 1px solid #eee;">' . lang("reg_no_hp") . '</td>
                <td style="width: 2%x;">:</td>
                <td style="width: 78px;padding: 5px;vertical-align: top;border-bottom: 1px solid #eee;white-space: nowrap;">
                    ' . $shop_order->phone . '
                </td>
            </tr>
            <tr class="item">
                <td style="width: 20%;padding: 5px 10px;vertical-align: top;border-bottom: 1px solid #eee;">' . lang("reg_email") . '</td>
                <td style="width: 2%x;">:</td>
                <td style="width: 78px;padding: 5px;vertical-align: top;border-bottom: 1px solid #eee;white-space: nowrap;">
                    ' . $shop_order->email . '
                </td>
            </tr>
            <tr class="item">
                <td style="width: 20%;padding: 5px 10px;vertical-align: top;border-bottom: 1px solid #eee;">' . lang("reg_alamat") . '</td>
                <td style="width: 2%x;">:</td>
                <td style="width: 78px;padding: 5px;vertical-align: top;border-bottom: 1px solid #eee;white-space: nowrap;">
                    ' . $address . '
                </td>
            </tr>
        </table>';

        $info_stockist      = '';
        $view_stockist      = ( $shop_order->type_order == 'member_order' ) ? true : false;
        if ( $usertype == 'member' && $view_stockist && $shop_order->id_stockist ) {
            if ( $stockistdata = bp_get_memberdata_by_id($shop_order->id_stockist) ) {
                $info_stockist = '
                <table class="table" style="margin-top: 30px;margin-bottom: 20px;width: 100%;line-height: inherit;text-align: left;">
                    <tr class="heading">
                        <th colspan="3" style="width: 100%;background: #eee;border-bottom: 1px solid #ddd;font-weight: bold;padding: 10px;">Information Stockist</th>
                    </tr>
                    <tr class="item">
                        <td style="width: 20%;padding: 5px 10px;vertical-align: top;border-bottom: 1px solid #eee;">' . lang("name") . '</td>
                        <td style="width: 2%x;">:</td>
                        <td style="width: 78px;padding: 5px;vertical-align: top;border-bottom: 1px solid #eee;white-space: nowrap;">
                            ' . ucwords(strtolower($stockistdata->name)) . '
                        </td>
                    </tr>
                    <tr class="item">
                        <td style="width: 20%;padding: 5px 10px;vertical-align: top;border-bottom: 1px solid #eee;">' . lang("reg_no_hp") . '</td>
                        <td style="width: 2%x;">:</td>
                        <td style="width: 78px;padding: 5px;vertical-align: top;border-bottom: 1px solid #eee;white-space: nowrap;">
                            ' . $stockistdata->phone . '
                        </td>
                    </tr>
                    <tr class="item">
                        <td style="width: 20%;padding: 5px 10px;vertical-align: top;border-bottom: 1px solid #eee;">' . lang("reg_email") . '</td>
                        <td style="width: 2%x;">:</td>
                        <td style="width: 78px;padding: 5px;vertical-align: top;border-bottom: 1px solid #eee;white-space: nowrap;">
                            ' . $stockistdata->email . '
                        </td>
                    </tr>
                </table>';
            }
        }

        // Information Billing Account
        $billing_detail     = '';
        if ( $shop_order->status == 0 && $shop_order->id_stockist == 0) {
            $bill_bank          = '';
            $bill_no            = get_option('company_bill');
            $bill_name          = get_option('company_bill_name');
            if ($company_bank = get_option('company_bank')) {
                if ($getBank = bp_banks($company_bank)) {
                    $bill_bank = $getBank->nama;
                }
            }

            if ($bill_no) {
                $bill_format = '';
                $arr_bill    = str_split($bill_no, 4);
                foreach ($arr_bill as $no) {
                    $bill_format .= $no . ' ';
                }
                $bill_no = $bill_format ? $bill_format : $bill_no;;
            }

            $billing_detail     = '
            <table class="table" style="margin-bottom: 20px;width: 100%;line-height: inherit;text-align: left;">
                <tr class="heading">
                    <th colspan="3" style="width: 100%;background: #eee;border-bottom: 1px solid #ddd;font-weight: bold;padding: 10px;">Informasi Rekening Perusahaan</th>
                </tr>

                <tr class="item">
                    <td style="width: 20%;padding: 5px 10px;vertical-align: top;border-bottom: 1px solid #eee;">Bank</td>
                    <td style="width: 2%px;">:</td>
                    <td style="width: 78%; padding: 5px;vertical-align: top;border-bottom: 1px solid #eee;white-space: nowrap;">
                        ' . strtoupper($bill_bank) . '
                    </td>
                </tr>
                <tr class="item">
                    <td style="width: 20%;padding: 5px 10px;vertical-align: top;border-bottom: 1px solid #eee;">No. Rekening</td>
                    <td style="width: 2%px;">:</td>
                    <td style="padding: 5px;vertical-align: top;border-bottom: 1px solid #eee;white-space: nowrap;">
                        ' . $bill_no . '
                    </td>
                </tr>
                <tr class="item">
                    <td style="width: 20%;padding: 5px 10px;vertical-align: top;border-bottom: 1px solid #eee;">Nama Rekening</td>
                    <td style="width: 2%px;">:</td>
                    <td style="padding: 5px;vertical-align: top;border-bottom: 1px solid #eee;white-space: nowrap;">
                        ' . ucwords(strtolower($bill_name)) . '
                    </td>
                </tr>
            </table>';

            if ($shop_order->status == 0) {
                $billing_detail    .= '
                <div class="info-box" style="padding: 20px;margin: auto;background: #2e6694;color: white;">
                    Silahkan Transfer <strong>Pembayaran sebasar ' . bp_accounting($shop_order->total_payment, $currency) . '</strong> Ke Rekening Perusahaan.
                </div>';
            }
        }

        $template_style     = '
        <style>
            * { font-size: 14px; }

            @media only screen and (max-width:480px) {
                table td.mobile-center {
                    width: 100% !important;
                    display: block !important;
                    text-align: left !important;
                }

                table td.title.mobile-center {
                    text-align: center !important
                }

                .mobile-hide {
                    display: none;
                }

                .mobile-text-left {
                    text-align: left !important;
                }
            }

            .no-padding {
                padding: unset !important;
            }

            .rtl {
                direction: rtl;
                font-family: Tahoma, "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
            }

            .rtl table {
                text-align: right;
            }

            .rtl table tr td:nth-child(2) {
                text-align: left;
            }

            table.no-wrap td {
                white-space: unset;
            }

            table.table tr.item td {
                font-size: 13px;
            }
            pre{ background-color: transparent; color: #FFFFFF; border:none; padding: 0px 10px 10px; }
        </style>';

        $template_open      = '
        <body class="clean-body" style="margin: 0; padding: 20px 0px; -webkit-text-size-adjust: 100%; background-color: #F5F5F5; font-family:Roboto,Arial,Helvetica,sans-serif;">
            <div style="background-color:transparent; margin: 0 auto; min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word;">';

        $template_header = '
            <div style="background-color:#FFFFFF; display: block;">
                <div style="width:100% !important;">
                    <div style="border:0px solid transparent; padding: 25px 10px 10px;">
                        <div style="padding: 0px; text-align: center;">
                            <!-- <img src="' . BE_IMG_PATH . 'logo.png" alt="' . $company_name . '" width="20%"> -->
                            <h1 style="margin: 0px; margin-top: 10px; font-size:18px; font-weight:bold; color:#5e72e4">' . $company_name . '</h1>
                        </div>
                    </div>
                </div>
            </div>';

        $template_body = '
            <div style="background-color:#FFFFFF; display: block; padding: 0px; font-size: 14px;">
                <div style="background: linear-gradient(87deg,#17acc7 0,#1171ef 100%)!important; padding: 20px 20px 0px; color: #FFFFFF;">
                    ' . $title . '
                </div>
                <div style="padding: 10px 20px; margin-top: 30px; color: #333;">
                    ' . $message . '
                </div>
                <div style="padding: 5px 20px; color: #333;">
                    ' . $product_detail . '
                </div>
                <div style="padding: 5px 20px; color: #333;">
                    ' . $shipping_detail . '
                    ' . $info_stockist . '
                </div>
                <div style="padding: 5px 20px; color: #333;">
                    ' . $billing_detail . '
                </div>
            </div>';

        $template_footer = '
            <div style="background-color:#FFFFFF;">
                <div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent;">
                    <div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                        <div class="col num12" style="min-width: 320px; max-width: 650px; display: table-cell; vertical-align: top; width: 650px;">
                            <div style="width:100% !important;">
                                <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:20px; padding-bottom:30px; padding-right: 0px; padding-left: 0px;">
                                    <table border="0" cellpadding="0" cellspacing="0" class="divider" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;" valign="top" width="100%">
                                        <tbody>
                                            <tr style="vertical-align: top;" valign="top">
                                                <td class="divider_inner" style="word-break: break-word; vertical-align: top; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding: 10px;" valign="top">
                                                    <table align="center" border="0" cellpadding="0" cellspacing="0" class="divider_content" height="0" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 60%; border-top: 1px dotted #C4C4C4; height: 0px;" valign="top" width="60%">
                                                        <tbody>
                                                            <tr style="vertical-align: top;" valign="top">
                                                                <td height="0" style="word-break: break-word; vertical-align: top; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;" valign="top"><span></span></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div style="color:#5F5F5F; line-height:120%; padding: 10px;">
                                        <div style="font-size: 12px; line-height: 14px; color: #5F5F5F;">
                                            <p style="font-size: 12px; line-height: 16px; text-align: center; margin: 0; font-weight:600;">
                                                ' . COMPANY_NAME . ' &copy; 2021. All Right Reserved
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';

        $template_close = '
            <div>
        </body>';

        $template           = $template_style . $template_open . $template_header . $template_body . $template_footer . $template_close;

        if ($notif) {
            $content = $notif->content;
            $content = str_replace("%name%",                    ($member ? $member->name : ''), $content);
            $content = str_replace("%memberuid%",               ($member ? $member->username : ''), $content);
            $content = str_replace('%customer_name%',           ($shop_order ? $shop_order->name : ''), $content);
            $content = str_replace("%order_detail%",            $product_detail, $content);
            $content = str_replace("%shipping_detail%",         $shipping_detail, $content);
            $content = str_replace("%stockist_information%",    $info_stockist, $content);

            if ( $shop_order->id_stockist == 0 ) {
                $content = str_replace("%billing_detail%",      $billing_detail, $content);
            } else {
                $content = str_replace("%billing_detail%",      $info_stockist, $content);
            }

            $total_transfer     = $shop_order->total_payment;
            $content = str_replace("%total_transfer%", ($total_transfer ? bp_accounting($total_transfer, $currency) : ''), $content);

            $title              = '
            <div style="text-align: center;">
                <p style="font-size:18px;">' . (empty($subject) ? 'Informasi Pesanan Produk' : $subject) . '</p>
                <hr>
                <p style="margin: 10px 0px 0px;">
                    <span style="font-size: 16px; font-weight:600; color: #fff;">Invoice </span>
                    <span style="font-size: 16px; font-weight:600; color: #ff8d2b;">' . $shop_order->invoice . '</span>
                </p>
                <p style="font-size: 12px; margin-top: 0px; color:#ddd">' . $dateorder . '</p>
                <hr/><br>
            </div>';

            $template_body = '
            <div style="background-color:#FFFFFF; display: block; padding: 0px; font-size: 14px;">
                <div style="background: linear-gradient(87deg,#17acc7 0,#1171ef 100%)!important; padding: 20px 20px 0px; color: #FFFFFF;">
                    ' . $title . '
                </div>
                <div style="padding: 10px 20px; margin-top: 30px; color: #333;">
                    ' . $content . '
                </div>
            </div>';

            $template           = $template_style . $template_open . $template_header . $template_body . $template_footer . $template_close;
        }
        return $template;
    }
}

if (!function_exists('bp_notification_shop_customer_template')) {
    /**
     * Get notification template
     * 
     * @since 1.0.0
     * @access public
     * 
     * @param string $message
     * @return array 
     * 
     * @author Yuda
     */
    function bp_notification_shop_customer_template($shop_order = "", $subject = "", $email = true)
    {
        if ( !$shop_order ) { return false; }

        $invoice            = isset($shop_order->invoice) ? $shop_order->invoice : false;
        if ( !$invoice ) { return false; }

        $CI = &get_instance();
        $CI->load->helper('shop_helper');

        $currency           = config_item('currency');
        $server_name        = isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : DOMAIN_NAME;
        $company_name       = get_option('company_name');
        $company_name       = !empty($company_name) ? $company_name : COMPANY_NAME;
        $dateorder          = !empty($shop_order->datecreated) ? date_indo($shop_order->datecreated, 'datetime') : '';

        $message            = '';
        if ( $shop_order ) {
            $text_message   = '';
            if ($shop_order->status == 0) {
                $subject        = 'Informasi Pemesanan Produk';
                $text_message   = '<p style="margin: 3px 0px;">Terima kasih sudah berbelanja di <b>' . $company_name . '</b>. Sebagai konfirmasi, berikut informasi data Pesanan anda :</p>';
            }
            if ($shop_order->status == 1) {
                $subject        = 'Informasi Konfirmasi Pesanan';
                $text_message   = '<p style="margin: 3px 0px;">Selamat pesanan produk anda telah dikonfirmasi. Berikut informasi data Pesanan anda :</p>';
            }
            if ($shop_order->status == 2) {
                $subject        = 'Informasi Pengiriman Pesanan';
                $text_message   = '<p style="margin: 3px 0px;">Selamat pesanan produk anda telah dikirim. Berikut informasi data Pesanan anda :</p>';
            }
            if ($shop_order->status == 4) {
                $subject        = 'Informasi Pembatalan Pesanan';
                $text_message   = '<p style="margin: 3px 0px;">Pesanan telah dibatalkan. Berikut informasi data pesanan anda :</p>';
            }
            if ( $email ) {
                $message        = '<p style="line-height: 1.2; margin-top: 20px">Halo <b>' . $shop_order->name . '</b></p>' . $text_message;
            }
        }

        $title              = '
            <div style="text-align: center;">
                <p style="font-size:18px;">' . (empty($subject) ? 'Informasi Pesanan Produk' : $subject) . '</p>
                <hr>
                <p style="margin: 10px 0px 0px;">
                    <span style="font-size: 16px; font-weight:500; color: #fff;">Invoice </span>
                    <span style="font-size: 16px; font-weight:600; color: #fff;">' . $invoice . '</span>
                </p>
                <p style="font-size: 12px; margin-top: 0px; color:#ddd">' . $dateorder . '</p>
                <hr/><br>
            </div>';

        $product_detail     = '
        <table class="table no-wrap table-responsive" style="margin-bottom: 30px;width: 100%;line-height: inherit;text-align: left;">
            <thead>
                <tr class="heading">
                    <th colspan="2" style="background: #eee;border-bottom: 1px solid #ddd;font-weight: bold;padding: 10px;">Produk</th>
                    <th style="width:30%;text-align: right;background: #eee;border-bottom: 1px solid #ddd;font-weight: bold;padding: 10px;">Total</th>
                </tr>
            </thead>
            <tbody>';

        if (is_serialized($shop_order->products)) {
            $unserialize_data = maybe_unserialize($shop_order->products);
            foreach ($unserialize_data as $row) {
                $idMaster       = $row['id'];
                $image          = '';
                if ($get_data_product = bp_products($idMaster)) {
                    $image      = $get_data_product->image;
                }
                $img_src        = bp_product_image($image);

                $product_name   = isset($row['name']) ? $row['name'] : 'Produk';
                $bv             = isset($row['bv']) ? $row['bv'] : 'Produk';
                $qty            = isset($row['qty']) ? $row['qty'] : 0;
                $price          = isset($row['price']) ? $row['price'] : 0;
                $price_cart     = isset($row['price_cart']) ? $row['price_cart'] : 0;
                $discount       = isset($row['discount']) ? $row['discount'] : 0;
                $subtotal       = $qty * $price_cart;

                if ($price > $price_cart) {
                    $price_prod = '( <s style="font-size: 11px">' . bp_accounting($price) . '</s> <span style="color:#fb6340;font-size: 11px">' . bp_accounting($price_cart, $currency) . '</span> )';
                } else {
                    $price_prod = bp_accounting($price_cart, $currency);
                }

                $product_detail     .= '
                    <tr>
                        <td style="width:70px; vertical-align:top; padding-top:5px">
                            <img src="' . $img_src . '" style="width: 70px;float: left;">
                        </td>
                        <td style="text-align: left;text-transform: capitalize;padding: 5px;vertical-align: top;border-bottom: 1px solid #eee;">
                            <span style="font-size: 12px;font-weight:600; margin-bottom: 3px;display: block;">' . $product_name . '</span>
                            <span style="font-size: 10px;display:block;margin-bottom:3px">
                                '. $qty .' <span style="font-size:9px">x</span> ' . $price_prod . '
                            </span>
                            <span style="font-size: 9px;display:block;margin-bottom:2px;color:#0b7346;">
                                BV: ' . bp_accounting($bv) . '
                            </span>
                        </td>
                        <td class="text-center" style="text-align: right;padding: 5px;vertical-align: top;border-bottom: 1px solid #eee;white-space: normal;">
                            ' . bp_accounting($subtotal)  . '
                        </td>
                    </tr>
                ';
            }
        }

        $uniquecode         = str_pad($shop_order->unique, 3, '0', STR_PAD_LEFT);
        $cfg_pay_method     = config_item('payment_method');
        $payment_method     = isset($cfg_pay_method[$shop_order->payment_method]) ? $cfg_pay_method[$shop_order->payment_method] : $shop_order->payment_method;
        $product_detail     .= '
                <tr>
                    <td colspan="2" style="text-align:right;padding:5px;vertical-aligntop;white-space:nowrap;font-weight:500; color:#666; font-size:13px">Subtotal</td>
                    <td style="text-align:right;padding:5px;vertical-align:top;white-space:nowrap;font-weight:bold; color:#666; font-size:13px">
                        ' . bp_accounting($shop_order->subtotal) . '
                    </td>
                </tr>
                ' . (($shop_order->shipping) ? '
                    <tr>
                        <td colspan="2" style="text-align:right;padding:5px;vertical-aligntop;white-space:nowrap;font-weight:500; color:#666; font-size:13px">Biaya Pengiriman</td>
                        <td style="text-align:right;padding:5px;vertical-align:top;white-space:nowrap;font-weight:bold; color:#666; font-size:13px">
                            ' . bp_accounting($shop_order->shipping) . '
                        </td>
                    </tr>' : '') . '
                ' . (($shop_order->unique) ? '
                    <tr>
                        <td colspan="2" style="text-align:right;padding:5px;vertical-aligntop;white-space:nowrap;font-weight:500; color:#666; font-size:13px">Kode Unik</td>
                        <td style="text-align:right;padding:5px;vertical-align:top;white-space:nowrap;font-weight:bold; color:#666; font-size:13px">
                            ' . ($uniquecode) . '
                        </td>
                    </tr>' : '') . '
                ' . (($shop_order->discount) ? '
                    <tr>
                        <td colspan="2" style="text-align:right;padding:5px;vertical-aligntop;white-space:nowrap;font-weight:500; color:#666; font-size:13px">
                            ' . lang('discount') . ' ' . ($shop_order->voucher ? ' (<span style="font-size:10px; color:#5e72e4">' . $shop_order->voucher . '</span>)' : '') . '
                        </td>
                        <td style="text-align:right;padding:5px;vertical-align:top;white-space:nowrap;font-weight:bold; color:#666; font-size:13px">
                            ' . bp_accounting($shop_order->discount) . '
                        </td>
                    </tr>' : '') . '
                <tr>
                    <td colspan="2" style="text-align:right;padding:5px;vertical-aligntop;white-space:nowrap;font-weight:bold; color:#666; font-size:15px">
                        ' . lang('total_payment') . '
                    </td>
                    <td style="text-align:right;padding:5px;vertical-align:top;white-space:nowrap;font-weight:bold; color:#fb6340; font-size:15px">
                        ' . bp_accounting($shop_order->total_payment, $currency) . '
                    </td>
                </tr>
            </tbody>
        </table>';

        // Information Shipping Address
        $address            = ucwords(strtolower($shop_order->address)) . ', ' . $shop_order->village . ' ';
        $address           .= 'Kec '. $shop_order->subdistrict . ' ' . $shop_order->district . ' ';
        $address           .= $shop_order->province;
        $address           .= ($shop_order->postcode) ? ' (' . $shop_order->postcode . ')' : '';

        // shipping method
        $shipping_title     = 'Alamat Pengiriman';
        $_shipping          = '';
        $_resi              = '';
        if ( $shop_order->shipping_method == 'ekspedisi' ) {
            $_shipping  = 'Jasa Ekspedisi / Pengiriman';
            if ( $shop_order->courier ) {
                $_shipping  = strtoupper($shop_order->courier);
                if ( $shop_order->service ) {
                    $_shipping  .= ' (' . strtoupper($shop_order->service) .')';
                }

                if ( $shop_order->status == 2 && $shop_order->resi ) {
                    $_resi = '
                        <tr class="heading">
                            <td colspan="3" style="background: #eee;border-bottom: 1px solid #ddd;padding: 10px;"><b>Nomor Resi </b> : '. $shop_order->resi .'</td>
                        </tr>
                    ';
                }
            }
        }
        if ( $shop_order->shipping_method == 'pickup' ) {
            $shipping_title = 'Alamat Penagihan';
            $_shipping      = 'Pickup';
        }


        $shipping_detail    = '
        <table class="table" style="margin-bottom: 20px;width: 100%;line-height: inherit;text-align: left;">
            <tr class="heading">
                <td colspan="3" style="background: #eee;border-bottom: 1px solid #ddd;padding: 10px;"><b>Metode Pengiriman </b> : '. $_shipping .'</td>
            </tr>
            '. $_resi  .'
            <tr class="heading">
                <th colspan="3" style="width: 100%;background: #eee;border-bottom: 1px solid #ddd;font-weight: bold;padding: 10px;">' . $shipping_title . '</th>
            </tr>
            <tr class="item">
                <td style="width: 20%;padding: 5px 10px;vertical-align: top;border-bottom: 1px solid #eee;">' . lang("name") . '</td>
                <td style="width: 2%;vertical-align:top">:</td>
                <td style="width: 78px;padding: 5px;vertical-align: top;border-bottom: 1px solid #eee;white-space: normal;">
                    ' . ucwords(strtolower($shop_order->name)) . '
                </td>
            </tr>
            <tr class="item">
                <td style="width: 20%;padding: 5px 10px;vertical-align: top;border-bottom: 1px solid #eee;">' . lang("reg_no_hp") . '</td>
                <td style="width: 2%;vertical-align:top">:</td>
                <td style="width: 78px;padding: 5px;vertical-align: top;border-bottom: 1px solid #eee;white-space: normal;">
                    ' . $shop_order->phone . '
                </td>
            </tr>
            <tr class="item">
                <td style="width: 20%;padding: 5px 10px;vertical-align: top;border-bottom: 1px solid #eee;">' . lang("reg_email") . '</td>
                <td style="width: 2%px;vertical-align:top">:</td>
                <td style="width: 78px;padding: 5px;vertical-align: top;border-bottom: 1px solid #eee;white-space: normal;">
                    ' . $shop_order->email . '
                </td>
            </tr>
            <tr class="item">
                <td style="width: 20%;padding: 5px 10px;vertical-align: top;border-bottom: 1px solid #eee;">' . lang("reg_alamat") . '</td>
                <td style="width: 2%; vertical-align:top">:</td>
                <td style="width: 78px;padding: 5px;vertical-align: top;border-bottom: 1px solid #eee;white-space: normal;">
                    ' . $address . '
                </td>
            </tr>
        </table>';

        // Information Billing Account
        $billing_detail     = '';
        if ( $shop_order->status == 0 ) {
            $bill_bank          = '';
            $bill_no            = get_option('company_bill');
            $bill_name          = get_option('company_bill_name');
            if ($company_bank = get_option('company_bank')) {
                if ($getBank = bp_banks($company_bank)) {
                    $bill_bank = $getBank->nama;
                }
            }

            if ($bill_no) {
                $bill_format = '';
                $arr_bill    = str_split($bill_no, 4);
                foreach ($arr_bill as $no) {
                    $bill_format .= $no . ' ';
                }
                $bill_no = $bill_format ? $bill_format : $bill_no;;
            }

            $billing_detail     = '
            <table class="table" style="margin-bottom: 20px;width: 100%;line-height: inherit;text-align: left;">
                <tr class="heading">
                    <th colspan="3" style="width: 100%;background: #eee;border-bottom: 1px solid #ddd;font-weight: bold;padding: 10px;">Informasi Rekening Perusahaan</th>
                </tr>

                <tr class="item">
                    <td style="width: 20%;padding: 5px 10px;vertical-align: top;border-bottom: 1px solid #eee;">Bank</td>
                    <td style="width: 2%;">:</td>
                    <td style="width: 78%; padding: 5px;vertical-align: top;border-bottom: 1px solid #eee;white-space: normal;">
                        ' . strtoupper($bill_bank) . '
                    </td>
                </tr>
                <tr class="item">
                    <td style="width: 20%;padding: 5px 10px;vertical-align: top;border-bottom: 1px solid #eee;">No. Rekening</td>
                    <td style="width: 2%;">:</td>
                    <td style="padding: 5px;vertical-align: top;border-bottom: 1px solid #eee;white-space: normal;">
                        ' . $bill_no . '
                    </td>
                </tr>
                <tr class="item">
                    <td style="width: 20%;padding: 5px 10px;vertical-align: top;border-bottom: 1px solid #eee;">Nama Rekening</td>
                    <td style="width: 2%;">:</td>
                    <td style="padding: 5px;vertical-align: top;border-bottom: 1px solid #eee;white-space: normal;">
                        ' . ucwords(strtolower($bill_name)) . '
                    </td>
                </tr>
            </table>';

            if ($shop_order->status == 0) {
                $billing_detail    .= '
                <div class="info-box" style="padding: 20px;margin: auto;background: #0b7346;color: #fff;">
                    Silahkan Transfer Pembayaran sebasar <strong>' . bp_accounting($shop_order->total_payment, $currency) . '</strong> Ke Rekening Perusahaan diatas.
                </div>';
            }
        }

        $template_style     = '
        <style>
            * { font-size: 14px; }

            @media only screen and (max-width:480px) {
                table td.mobile-center {
                    width: 100% !important;
                    display: block !important;
                    text-align: left !important;
                }

                table td.title.mobile-center {
                    text-align: center !important
                }

                .mobile-hide {
                    display: none;
                }

                .mobile-text-left {
                    text-align: left !important;
                }
            }

            .no-padding {
                padding: unset !important;
            }

            .rtl {
                direction: rtl;
                font-family: Tahoma, "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
            }

            .rtl table {
                text-align: right;
            }

            .rtl table tr td:nth-child(2) {
                text-align: left;
            }

            table.no-wrap td {
                white-space: unset;
            }

            table.table tr.item td {
                font-size: 13px;
            }
            pre{ background-color: transparent; color: #FFFFFF; border:none; padding: 0px 10px 10px; }
        </style>';

        $template_open      = '
        <body class="clean-body" style="margin: 0; padding: 20px 0px; -webkit-text-size-adjust: 100%; background-color: #F5F5F5; font-family:Roboto,Arial,Helvetica,sans-serif;">
            <div style="background-color:transparent; margin: 0 auto; min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word;">';

        $template_header = '
            <div style="background-color:#FFFFFF; display: block;">
                <div style="width:100% !important;">
                    <div style="border:0px solid transparent; padding: 20px 10px;">
                        <div style="padding: 0px; text-align: center;">
                            <img src="' . BE_IMG_PATH . 'logo.png" alt="' . $company_name . '" height="80px">
                            <h1 style="margin: 0px; margin-top: 10px; font-size:18px; font-weight:bold; color:#0b7346">' . $company_name . '</h1>
                        </div>
                    </div>
                </div>
            </div>';

        $template_body = '
            <div style="background-color:#FFFFFF; display: block; padding: 0px; font-size: 14px;">
                <div style="background: linear-gradient(87deg,#0fc677 0,#0b7346 100%)!important; padding: 20px 20px 0px; color: #FFFFFF;">
                    ' . $title . '
                </div>
                <div style="padding: 10px 20px; color: #333;">
                    ' . $message . '
                </div>
                <div style="padding: 5px 20px; color: #333;">
                    ' . $product_detail . '
                </div>
                <div style="padding: 5px 20px; color: #333;">
                    ' . $shipping_detail . '
                </div>
                <div style="padding: 5px 20px; color: #333;">
                    ' . $billing_detail . '
                </div>
            </div>';

        $template_footer = '
            <div style="background-color:#FFFFFF;">
                <div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent;">
                    <div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                        <div class="col num12" style="min-width: 320px; max-width: 650px; display: table-cell; vertical-align: top; width: 650px;">
                            <div style="width:100% !important;">
                                <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:20px; padding-bottom:30px; padding-right: 0px; padding-left: 0px;">
                                    <table border="0" cellpadding="0" cellspacing="0" class="divider" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;" valign="top" width="100%">
                                        <tbody>
                                            <tr style="vertical-align: top;" valign="top">
                                                <td class="divider_inner" style="word-break: break-word; vertical-align: top; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding: 10px;" valign="top">
                                                    <table align="center" border="0" cellpadding="0" cellspacing="0" class="divider_content" height="0" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 60%; border-top: 1px dotted #C4C4C4; height: 0px;" valign="top" width="60%">
                                                        <tbody>
                                                            <tr style="vertical-align: top;" valign="top">
                                                                <td height="0" style="word-break: break-word; vertical-align: top; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;" valign="top"><span></span></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div style="color:#5F5F5F; line-height:120%; padding: 10px;">
                                        <div style="font-size: 12px; line-height: 14px; color: #5F5F5F;">
                                            <p style="font-size: 12px; line-height: 16px; text-align: center; margin: 0; font-weight:600;">
                                                ' . COMPANY_NAME . ' &copy; 2021. All Right Reserved
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';

        $template_close = '
            <div>
        </body>';

        $template           = $template_style . $template_open . $template_header . $template_body . $template_footer . $template_close;

        return $template;
    }
}


// Function for Generate Fake Member
if (!function_exists('bp_generate_member')) {
    /**
     * Generate Fake Member
     * @author  Saddam
     * @param   Int     $sponsor_id         (Required)  ID Sponsor
     * @param   Int     $length             (Required)  Generate Amount
     * @return selft function 
     */
    function bp_generate_member($sponsor_id, $length = 1, $debug = false, $username = '')
    {
        include APPPATH . '/third_party/faker/autoload.php';
        $CI = &get_instance();

        $faker = Faker\Factory::create('id_ID');
        $faker->seed($length);

        foreach (range(1, $length, 1) as $number) {
            $data_address                           = array();
            $gender                                 = $faker->randomElement(array('M', 'F'));
            // -------------------------------------------------------
            // Get Country Data
            // -------------------------------------------------------
            $country                                = 'IDN';
            $data_address['country']                = $country;
            // -------------------------------------------------------
            // Get Province Data
            // -------------------------------------------------------
            $list_province                          = bp_provinces();
            $province_change                        = $faker->randomElement($list_province);
            $province_code                          = isset($province_change->province_code) ? $province_change->province_code : $province_change->id;
            $data_address['province']               = $province_change;

            // -------------------------------------------------------
            // Get District Data
            // ------------------------------------------------------- 
            $list_district                          = bp_districts_by_province($province_change->id, '');
            $district_change                        = $faker->randomElement($list_district);
            $district_name                          = $district_change->district_type . ' ' . $district_change->district_name;
            $district_code                          = isset($district_change->district_code) ? $district_change->district_code : $district_change->id;
            $data_address['district']               = $district_change;


            // -------------------------------------------------------
            // Get Sub District Data
            // ------------------------------------------------------- 
            $list_subdistrict                       = bp_subdistricts_by_district($district_change->id);
            $subdistrict_change                     = $faker->randomElement($list_subdistrict);
            $data_address['subdistrict']            = $subdistrict_change;

            // -------------------------------------------------------
            // Get Village Data
            // ------------------------------------------------------- 
            $village                                = strtoupper($subdistrict_change->subdistrict_name);
            $data_address['village']                = $village;

            // -------------------------------------------------------
            // Get Bank Data
            // ------------------------------------------------------- 
            $list_bank                              = bp_banks();
            $bank_change                            = $faker->randomElement($list_bank);
            $bank                                   = $bank_change->nama;

            // -------------------------------------------------------
            // Get Sponsor Data
            // ------------------------------------------------------- 
            $sponsordata                            = $CI->Model_Member->get_memberdata($sponsor_id);

            // -------------------------------------------------------
            // Get Upline Data
            // ------------------------------------------------------- 
            $uplinedata                             = bp_upline_available($sponsor_id);

            // -------------------------------------------------------
            // Get Position
            // ------------------------------------------------------- 
            $uplinedata                             = bp_upline_available($sponsor_id);
            if ( !$uplinedata ) {
                continue;
            }
            $upline_id                              = $uplinedata->id;
            $position_node                          = bp_check_node($upline_id);
            if ( $position_node ) {
                $position                           = ( count($position_node) > 1 ) ? POS_LEFT : $position_node[0];
            } else {
                continue;
            }

            // -------------------------------------------------------
            // Get General Data
            // ------------------------------------------------------- 
            $name                   = $faker->name($gender == 'M' ? 'male' : 'female');
            $pob                    = $faker->city;
            $dateofbirth            = date('Y-m-d', strtotime('1992-01-04'));
            $marital                = $faker->randomElement(array('married', 'single'));
            $idcard_type            = 'KTP';
            $idcard                 = $faker->nik();
            $npwp                   = null;
            $address                = $faker->address;
            $email                  = $faker->unique()->safeEmail;
            $phone                  = $faker->unique()->phoneNumber;
            $phone_home             = null;
            $phone_office           = null;
            $bill                   = $faker->randomNumber(5, false);
            $bill_name              = strtoupper($name);
            $datetime               = date('Y-m-d H:i:s');

            $emergency_name         = $faker->name($gender == 'M' ? 'male' : 'female');
            $emergency_phone        = $faker->unique()->phoneNumber;
            $emergency_relationship = $faker->randomElement(array('Kaka', 'Adik', 'Saudara Perempuan', 'Saurdara Laki-Laki', 'Ayah', 'Ibu'));

            $username               = $username ? $username : $sponsordata->username;
            $username               = $username;
            $generate_username      = bp_generate_username_unique($username, 0, '_');
            $package                = MEMBER_BASIC;
            $m_status               = 1;

            // Data Member
            $data_member            = array(
                'username'              => $generate_username,
                'password'              => bp_password_hash('123qwe'),
                'password_pin'          => bp_encrypt('123qwe'),
                'type'                  => MEMBER,
                'package'               => $package,
                'sponsor'               => $sponsor_id,
                'parent'                => $upline_id,
                'position'              => $position,
                'name'                  => $name,
                'pob'                   => $pob,
                'dob'                   => $dateofbirth,
                'gender'                => $gender,
                'marital'               => $marital,
                'idcard_type'           => $idcard_type,
                'idcard'                => $idcard,
                'npwp'                  => $npwp,
                'country'               => $country,
                'province'              => $province_change->id,
                'district'              => $district_change->id,
                'subdistrict'           => $subdistrict_change->id,
                'village'               => $subdistrict_change->subdistrict_name,
                'address'               => $address,
                'email'                 => $email,
                'phone'                 => $phone,
                'phone_home'            => $phone_home,
                'phone_office'          => $phone_office,
                'bank'                  => $bank_change->id,
                'bill'                  => $bill,
                'bill_name'             => $bill_name,
                'emergency_name'        => $emergency_name,
                'emergency_relationship'=> $emergency_relationship,
                'emergency_phone'       => $emergency_phone,
                'status'                => $m_status,
                'total_omzet'           => 0,
                'uniquecode'            => 0,
                'datecreated'           => $datetime,
            );

            // Data Member Confirm
            $data_member_confirm    = array(
                'id_member'         => $sponsordata->id,
                'member'            => $sponsordata->username,
                'id_sponsor'        => $sponsordata->id,
                'sponsor'           => $sponsordata->username,
                'status'            => $m_status,
                'access'            => 'admin',
                'package'           => $package,
                'omzet'             => 0,
                'uniquecode'        => 0,
                'nominal'           => 0,
                'datecreated'       => $datetime,
                'datemodified'      => $datetime,
            );

            // if not Debug, execute to database
            if ( !$debug ) {
                $member_save_id = $CI->Model_Member->save_data($data_member);
                if ( !$member_save_id ) {
                    continue;
                }

                // -------------------------------------------------
                // Update Member Tree
                // -------------------------------------------------
                $gen                = $sponsordata->gen + 1;
                $level              = $uplinedata->level + 1;
                $tree               = bp_generate_tree($member_save_id, $uplinedata->tree);
                $tree_sponsor       = bp_generate_tree_sponsor($member_save_id, $sponsordata->tree_sponsor);
                $data_tree          = array('gen' => $gen, 'level' => $level, 'tree' => $tree, 'tree_sponsor' => $tree_sponsor);

                // Update Data Member
                $update_tree        = $CI->Model_Member->update_data_member($member_save_id, $data_tree);

                // -------------------------------------------------
                // Save Member Confirm
                // -------------------------------------------------
                $data_member_confirm = array_merge($data_member_confirm, array(
                    'id_downline'       => $member_save_id,
                    'downline'          => $generate_username,
                ));
                $insert_member_confirm  = $CI->Model_Member->save_data_confirm($data_member_confirm);

                $downline               = bp_get_memberdata_by_id($member_save_id);
                if ( !$downline ) {
                    continue;
                }

                $bonus_sponsor          = bp_calculate_bonus_sponsor($downline->id, $datetime);
                $saved_member_board     = kb_saved_member_board($downline, 1, $datetime);
                $check_member_board     = kb_check_member_board($sponsordata, 1, $datetime);

            } else {
                echo '<pre style="color:#333">';
                $data_res = array(
                    'member'            => $data_member,
                    'member_confirm'    => $data_member_confirm
                );
                echo '----------------------------------------------------' . br();
                echo ' Sponsor ID     : ' . $sponsordata->username . br();
                echo ' Username       : ' . $sponsordata->username . br();
                echo '----------------------------------------------------' . br();
                echo ' Upline ID      : ' . $uplinedata->id . br();
                echo ' Username       : ' . $uplinedata->username . br();
                echo ' Position       : ' . $position . br();
                echo '----------------------------------------------------' . br(2);
                echo ' Execute Member : ' . $number . br();
                echo '----------------------------------------------------' . br(2);
                echo var_dump($data_res);
                echo "</pre>";
            }
        }
    }
}


/*
CHANGELOG
---------
Insert new changelog at the top of the list.
-----------------------------------------------
Version YYYY/MM/DD  Person Name     Description
-----------------------------------------------
1.0.0   2016/06/01  Yuda           - Create this changelog.
*/
