<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


/*
|--------------------------------------------------------------------------
| Get Check Waybill Exist
|--------------------------------------------------------------------------
*/
if (!function_exists('sicepat_check_waybill_exist')) {
    function sicepat_check_waybill_exist($waybill = '')
    {
        if (!$waybill) {
            return 0;
        }
        $CI = &get_instance();
        $condition = ' status = 1 ';
        $waybilldata = $CI->Model_Sicepat->get_waybill_by('receive_number', $waybill, $condition, 1);

        if ($waybilldata) {
            return true;
        }

        return false;
    }
}

/*
|--------------------------------------------------------------------------
| Get Waybill
|--------------------------------------------------------------------------
*/
if (!function_exists('sicepat_waybill_active')) {
    function sicepat_waybill_active()
    {

        $CI = &get_instance();
        $condition = ' AND %status% = 1 ';
        $orderby        = ' %id% ASC ';
        $waybilldata = $CI->Model_Sicepat->get_all_waybill_data(1, 0, $condition, $orderby);

        return $waybilldata->receive_number;
    }
}

/*
|--------------------------------------------------------------------------
| Get Stock Waybill Active
|--------------------------------------------------------------------------
*/
if (!function_exists('sicepat_waybill_stock')) {
    function sicepat_waybill_stock($status = 1)
    {
        $CI = &get_instance();
        $stock = $CI->Model_Sicepat->get_stock_waybill($status);
        return $stock;
    }
}

/*
|--------------------------------------------------------------------------
| Update Waybill Used
|--------------------------------------------------------------------------
*/
if (!function_exists('sicepat_waybill_used')) {
    function sicepat_waybill_used($waybill)
    {
        if (!$waybill) return false;
        $CI = &get_instance();
        if ($CI->Model_Sicepat->get_waybill_by('receive_number', $waybill, '', 1)) {
            $datetime               = date('Y-m-d 23:59:5');
            $data = array(
                'status'            => 2,
                'datemodified'      => $datetime,
                'dateused'          => $datetime
            );
            $updated_data = $CI->Model_Sicepat->update_data_waybill_by('receive_number', $waybill, $data);

            if ($updated_data) {
                return true;
            }
        }

        return false;
    }
}

/*
|--------------------------------------------------------------------------
| Get Destination Data
|--------------------------------------------------------------------------
*/
if (!function_exists('sicepat_destination_data')) {
    function sicepat_destination_data($province = '', $city = '', $subdisctrict = '')
    {
        if (!$province || !$city || !$subdisctrict) return false;
        $CI = &get_instance();

        $condition = " AND %subdistrict% LIKE '%$subdisctrict%' AND %city% LIKE '%$city%' AND %province% LIKE '%$province%'";
        $datadest = $CI->Model_Sicepat->get_all_destination_data(1, 0, $condition);

        if ($datadest) {
            return $datadest;
        }
        return false;
    }
}


/*
|--------------------------------------------------------------------------
| Request Pickup
|--------------------------------------------------------------------------
*/
if (!function_exists('sicepat_request_pickup')) {
    function sicepat_request_pickup($id_order = 0, $type = 'customer', $debug = FALSE)
    {
        $url                = 'http://pickup.sicepat.com:8087/api/partner/requestpickuppackage';
        $dataRes = array(
            'status'        => 400,
            'waybill'       => '',
            'message'       => '',
            'data'          => array()
        );

        if (!$id_order || !$type) {
            $dataRes['message']     = 'Data Order Customer Tidak ditemukan';

            return $dataRes;
        }

        $CI = &get_instance();
        $data_shop                  = '';
        if ($type == 'customer') {
            $data_shop                  = $CI->Model_Shop->get_shop_customer_order_by('id', $id_order, '', 1);
        } else if (($type == 'member') || ($type == 'stockist')) {
            $data_shop                  = $CI->Model_Shop->get_shop_order_by('id', $id_order, '', 1);
        }

        if (!$data_shop) {
            $dataRes['message']     = 'Data Order Customer Tidak ditemukan';
            return $dataRes;
        }

        $auth_key                   = get_option('sicepat_token');
        $sicepat_active             = get_option('sicepat_active');

        $merchant_name              = get_option('company_name');
        $merchant_name              = $merchant_name ? $merchant_name : '-';

        $company_address            = get_option('company_address');
        $company_address            = $company_address ? $company_address : '-';

        $company_city               = get_option('company_city');
        $company_city               = $company_city ? bp_districts($company_city)->district_name : '-';

        $company_province           = get_option('company_province');
        $company_province           = $company_province ? bp_provinces($company_province)->province_name : '-';

        $company_phone              = get_option('company_phone');
        $company_phone              = $company_phone ? $company_phone : '-';

        $company_email              = get_option('company_email');
        $company_email              = $company_email ? $company_email : '-';

        $origin_code                = get_option('sicepat_origin_code');
        $origin_code                = $origin_code ? $origin_code : '-';

        $city                       = str_replace('KAB.', '', strtoupper($data_shop->district));
        $city                       = str_replace('KOTA ', '', strtoupper($city));
        $city                       = str_replace('KABUPATEN ', '', strtoupper($city));
        $data_dest                  = sicepat_destination_data(strtoupper($data_shop->province), $city, strtoupper($data_shop->subdistrict));
        $waybill_active             = sicepat_waybill_active();
        $datetime                   = date('Y-m-d H:i');

        if (!$waybill_active) {
            $dataRes['message'] = 'Resi Tidak Ditemukan';

            return $dataRes;
        }

        if ($sicepat_active && $auth_key && $waybill_active) {
            $data = array(
                "auth_key"                  => $auth_key,
                "reference_number"          => $data_shop->invoice,
                "pickup_request_date"       => $datetime,
                "pickup_merchant_name"      => $merchant_name,
                "pickup_address"            => $company_address,
                "pickup_city"               => $company_city,
                "pickup_merchant_phone"     => $company_phone,
                "pickup_merchant_email"     => $company_email,
                "PackageList" => array(
                    array(
                        "receipt_number"        => $waybill_active,
                        "origin_code"           => $origin_code,
                        "delivery_type"         => $data_shop->service,
                        "parcel_category"       => "Kecantikan",
                        "parcel_content"        => "-",
                        "parcel_qty"            => $data_shop->total_qty,
                        "parcel_value"          => $data_shop->subtotal,
                        "total_weight"          => ceil(($data_shop->weight / 1000)),
                        "shipper_name"          => $merchant_name,
                        "shipper_address"       => $company_address,
                        "shipper_province"      => $company_province,
                        "shipper_city"          => $company_city,
                        "shipper_district"      => "Jakarta Pusat",
                        "shipper_zip"           => "-",
                        "shipper_phone"         => $company_phone,
                        "recipient_title"       => "-",
                        "recipient_name"        => $data_shop->name,
                        "recipient_address"     => $data_shop->address,
                        "recipient_province"    => $data_shop->province,
                        "recipient_city"        => $city,
                        "recipient_district"    => $data_shop->subdistrict,
                        "recipient_zip"         => "-",
                        "recipient_phone"       => $data_shop->phone,
                        "destination_code"      => $data_dest ? $data_dest->destination_code : '-'
                    )
                )
            );


            $pickup_method          = get_option('sicepat_pickup_method');
            $pickup_method          = isset($pickup_method) ? $pickup_method : 'DROP';
            $data['pickup_method']  = $pickup_method;

            foreach ($data['PackageList'] as $index => $val) {
                $data['PackageList'][$index]['shipper_longitude']           = "0";
                $data['PackageList'][$index]['shipper_latitude']            = "0";
                $data['PackageList'][$index]['recipient_longitude']         = "0";
                $data['PackageList'][$index]['recipient_latitude']          = "0";
                $data['PackageList'][$index]['parcel_uom']                  = 'pcs';
            }

            if ($debug) {
                echo '<pre>';
                $datetime               = date('Y-m-d 23:59:5');
                echo '-------------------------------------------------------' . br();
                echo "          Debug Sicepat Waybill Number                " . br();
                echo '-------------------------------------------------------' . br();
                echo ' Function     : ' . ($debug ? 'View' : 'Save') . br();
                echo ' Datetime     : ' . $datetime . br();
                echo '-------------------------------------------------------' . br(2);
                echo 'Informasi Pengiriman' . br();
                echo '-------------------------------------------------------' . br();
                echo 'Auth Key                      : ' . $data['auth_key'] . br();
                echo 'Referensi                     : ' . $data['reference_number'] . br();
                echo 'Pickup Request Date           : ' . $data['pickup_request_date'] . br();
                echo 'Pickup Method                 : ' . $data['pickup_method'] . br();
                echo 'Pickup Merchant Name          : ' . $data['pickup_merchant_name'] . br();
                echo 'Pickup Address                : ' . $data['pickup_address'] . br();
                echo 'Pickup City                   : ' . $data['pickup_city'] . br();
                echo 'Pickup Mechant Phone          : ' . $data['pickup_merchant_phone'] . br();
                echo 'Pickup Mechant Email          : ' . $data['pickup_merchant_email'] . br();
                echo '-------------------------------------------------------' . br(2);

                echo 'Data Package' . br();
                echo '-------------------------------------------------------' . br();
                if (is_array($data['PackageList']) && (count($data['PackageList']) > 0)) {
                    $i = 1;
                    foreach ($data['PackageList'] as $index => $val) {
                        echo $i . '.' . br();
                        echo '=======================================================' . br();
                        echo 'Receipt Number                : ' . $val['receipt_number'] . br();
                        echo 'Origin Code                   : ' . $val['origin_code'] . br();
                        echo 'Delivery Type                 : ' . $val['delivery_type'] . br();
                        echo 'Parcel Category               : ' . $val['parcel_category'] . br();
                        echo 'Parcel Content                : ' . $val['parcel_content'] . br();
                        echo 'Parcel Quantity               : ' . $val['parcel_qty'] . br();
                        echo 'Parcel UOM                    : ' . $val['parcel_uom'] . br();
                        echo 'Parcel Value                  : ' . $val['parcel_value'] . br();
                        echo 'Parcel Weight                 : ' . $val['total_weight'] . br();
                        echo '=======================================================' . br();
                        echo 'Shipper Name                  : ' . $val['shipper_name'] . br();
                        echo 'Shipper Address               : ' . $val['shipper_address'] . br();
                        echo 'Shipper Province              : ' . $val['shipper_province'] . br();
                        echo 'Shipper City                  : ' . $val['shipper_city'] . br();
                        echo 'Shipper District              : ' . $val['shipper_district'] . br();
                        echo 'Shipper ZIP                   : ' . $val['shipper_zip'] . br();
                        echo 'Shipper Phone                 : ' . $val['shipper_phone'] . br();
                        echo 'Shipper Latitude              : ' . $val['shipper_latitude'] . br();
                        echo 'Shipper Longitude             : ' . $val['shipper_longitude'] . br();
                        echo '=======================================================' . br();
                        echo 'Receipient Title              : ' . $val['recipient_title'] . br();
                        echo 'Receipient Name               : ' . $val['recipient_name'] . br();
                        echo 'Receipient Address            : ' . $val['recipient_address'] . br();
                        echo 'Receipient Province           : ' . $val['recipient_province'] . br();
                        echo 'Receipient City               : ' . $val['recipient_city'] . br();
                        echo 'Receipient District           : ' . $val['recipient_district'] . br();
                        echo 'Receipient ZIP                : ' . $val['recipient_zip'] . br();
                        echo 'Receipient Phone              : ' . $val['recipient_phone'] . br();
                        echo 'Destination Code              : ' . $val['destination_code'] . br();
                        echo 'Receipient Latitude           : ' . $val['recipient_latitude'] . br();
                        echo 'Receipient Longitude          : ' . $val['recipient_longitude'] . br();
                        echo '=======================================================' . br(2);
                        $i++;
                    }
                }
                echo '-------------------------------------------------------' . br();
            } else {
                $dataTrans              = $data;
                $datetime               = date('Y-m-d 23:59:5');
                $postdata = json_encode($data);

                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                $result = curl_exec($ch);
                curl_close($ch);
                $result = json_decode($result);

                if ($result->status == "200") {
                    $dataRes['status']      = 200;
                    $dataRes['waybill']     = $waybill_active;
                    $dataRes['data']        = $result->datas;
                    $dataRes['result']      = $result;
                    sicepat_waybill_used($waybill_active);
                    if (is_array($result->datas) && (count($result->datas) > 0)) {
                        $data_pickup_detail = $dataTrans['PackageList'];
                        $data_pickup = $dataTrans;
                        unset($data_pickup['PackageList']);
                        unset($data_pickup['auth_key']);
                        $data_pickup['datecreated']     = $datetime;
                        $data_pickup['datecreated']     = $datetime;
                        $data_pickup['request_number']  = $result->request_number;
                        if ($save_data_pickup = $CI->Model_Sicepat->save_pickup_data($data_pickup)) {
                            foreach ($data_pickup_detail as $detail) {
                                $detail['datecreated']  = $datetime;
                                $detail['datecreated']  = $datetime;
                                $detail['id_pickup']    = $save_data_pickup;

                                $id_detile = $CI->Model_Sicepat->save_pickup_detail_data($detail);
                                if (!$id_detile) {
                                    $dataRes['status']      = 400;
                                    $dataRes['message']     = 'Gagal Menyimpan Data Detail Pickup' . json_encode($CI->db->error());
                                    $dataRes['data']        = array();

                                    return $dataRes;
                                }
                            }
                        } else {
                            $dataRes['status']      = 400;
                            $dataRes['message']     = 'Gagal Menyimpan Data Pickup' . json_encode($CI->db->error());
                            $dataRes['data']        = array();
                        }
                    }
                } else {
                    $dataRes['status']      = 400;
                    $dataRes['message']     = $result->error_message;
                    $dataRes['data']        = array();
                }

                return $dataRes;
            }
        } else {
            $dataRes['message'] = 'Resi Tidak Ditemukan';

            return $dataRes;
        }
    }
}

/*
|--------------------------------------------------------------------------
| Get Pickup Detail Data
|--------------------------------------------------------------------------
*/
if (!function_exists('sicepat_pickup_detail_data')) {
    function sicepat_pickup_detail_data($id_pickup)
    {
        if (!$id_pickup) return false;
        $CI = &get_instance();

        $datadetail = $CI->Model_Sicepat->get_pickup_detail_by('id_pickup', $id_pickup, '', 1);

        if ($datadetail) {
            return $datadetail;
        }
        return false;
    }
}
