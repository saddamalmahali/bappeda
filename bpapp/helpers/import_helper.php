<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('bp_import_urusan')) {
    function bp_import_urusan($file = '', $unit = URUSAN)
    {
        $CI                             = &get_instance();
        $data                           = array();
        $dataRes                        = array(
            'status'    => 'error'
        );
        $dataInsert                     = 0;
        $dataUpdate                     = 0;
        if (empty($file)) return $dataRes;

        $cfg_data_master                = config_item('data_master');

        if (!isset($cfg_data_master[$unit])) return $dataRes;

        $table                          = $cfg_data_master[$unit];
        $reader                         = bp_initialize_reader();

        $spreadsheet                    = $reader->load($file);

        if ($spreadsheet) {
            foreach ($spreadsheet->getWorksheetIterator() as $worksheet) {
                $highestRow             = $worksheet->getHighestRow();
                $highestColumn          = $worksheet->getHighestColumn();
                $data['higgestRow']     = $highestRow;
                $data['highestColumn']  = $highestColumn;

                $get_code               = $worksheet->getCellByColumnAndRow(2, 1)->getValue();
                $get_code               = isset($get_code) ? $get_code : '';

                if (!$get_code) return $dataRes;
                $get_code               = bp_decrypt($get_code);

                $validate_code          = get_option('import_code');
                $validate_code          = isset($validate_code) ? $validate_code : '';

                if (!$validate_code) return $dataRes;
                $validate_code          = bp_decrypt($validate_code);

                if ($get_code != $validate_code) return null;


                $header_no              = $worksheet->getCellByColumnAndRow(1, 2)->getValue();
                $header_code            = $worksheet->getCellByColumnAndRow(2, 2)->getValue();
                $header_name            = $worksheet->getCellByColumnAndRow(3, 2)->getValue();

                if (($header_no == 'ID') && ($header_code == 'KODE') && ($header_name == 'NAMA')) {
                    for ($row = 3; $row <= $highestRow; $row++) {

                        $kode           = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                        $nama           = $worksheet->getCellByColumnAndRow(3, $row)->getValue();

                        $getData        = $CI->Model_Master->get_urusan_by('kode', $kode, '', 1);

                        if ($getData) {

                            $data_update = array(
                                'kode'  => $kode,
                                'nama'  => $nama
                            );

                            if (!$CI->Model_Master->update_data_urusan($getData->id, $data_update)) {
                                break;
                                return $dataRes;
                            }
                            $dataUpdate++;
                            continue;
                        }

                        // Check available Urusan Data
                        $data['data'][] = array(
                            'kode'              => $kode,
                            'nama'              => $nama
                        );
                        $dataInsert++;
                    }
                } else {
                    return $dataRes;
                }
            }

            if (isset($data['data']) && is_array($data['data']) && (count($data['data']) > 0)) {
                $CI->db->insert_batch($table, $data['data']);
            }

            if (($dataInsert > 0) || ($dataUpdate > 0)) {
                $dataRes = array(
                    'status'        => 'success',
                    'data_insert'   => $dataInsert,
                    'data_update'   => $dataUpdate
                );
            }

            if (is_array($dataRes) && ($dataRes['status'] == 'success')) {
                // Format Output Import Array ('status'=>'success', 'data_insert'=> 1, 'data_update'=> 2)
                bp_log_action('ACTION_MIGRATE_URUSAN', 'SUCCESS', 'MIGARE_URUSAN', json_encode(array('cookie' => $_COOKIE, 'status' => 'SUCCESS', 'data' => $data)));
            }
        }
        return $dataRes;
    }
}

if (!function_exists('bp_import_sub_urusan')) {
    function bp_import_sub_urusan($file = '', $unit = SUB_URUSAN)
    {
        $CI                             = &get_instance();
        $data                           = array();
        $dataRes                        = array(
            'status'    => 'error'
        );
        $dataInsert                     = 0;
        $dataUpdate                     = 0;

        if (empty($file)) return $dataRes;

        $cfg_data_master                = config_item('data_master');

        if (!isset($cfg_data_master[$unit])) return $dataRes;

        $table                          = $cfg_data_master[$unit];
        $reader                         = bp_initialize_reader();

        $spreadsheet                    = $reader->load($file);

        if ($spreadsheet) {
            foreach ($spreadsheet->getWorksheetIterator() as $worksheet) {
                $highestRow             = $worksheet->getHighestRow();
                $highestColumn          = $worksheet->getHighestColumn();
                $data['higgestRow']     = $highestRow;
                $data['highestColumn']  = $highestColumn;
                $get_code               = $worksheet->getCellByColumnAndRow(2, 1)->getValue();
                $get_code               = isset($get_code) ? $get_code : '';

                if (!$get_code) return $dataRes;
                $get_code               = bp_decrypt($get_code);

                $validate_code          = get_option('import_code');
                $validate_code          = isset($validate_code) ? $validate_code : '';

                if (!$validate_code) return $dataRes;
                $validate_code          = bp_decrypt($validate_code);

                if ($get_code != $validate_code) return $dataRes;

                $header_no              = $worksheet->getCellByColumnAndRow(1, 2)->getValue();
                $header_code            = $worksheet->getCellByColumnAndRow(2, 2)->getValue();
                $header_name            = $worksheet->getCellByColumnAndRow(3, 2)->getValue();
                $header_fk              = $worksheet->getCellByColumnAndRow(4, 2)->getValue();

                if (($header_no == 'ID') && ($header_code == 'KODE') && ($header_name == 'NAMA') && ($header_fk == 'ID_URUSAN')) {

                    for ($row = 3; $row <= $highestRow; $row++) {
                        $kode           = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                        $nama           = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                        $id_urusan      = $worksheet->getCellByColumnAndRow(4, $row)->getValue();

                        $urusan         = $CI->Model_Master->get_urusan_by('id', $id_urusan, '', 1);
                        $urusan         = isset($urusan) ? $urusan : '';

                        if (!$urusan) continue;

                        $getData        = $CI->Model_Master->get_sub_urusan_by('kode', $kode, '', 1);

                        if ($getData) {
                            $data_update = array(
                                'kode'  => $kode,
                                'nama'  => $nama
                            );
                            if (!$CI->Model_Master->update_data_sub_urusan($getData->id, $data_update)) {
                                break;
                                return $dataRes;
                            }
                            $dataUpdate++;
                            continue;
                        }

                        $data['data'][] = array(
                            'id_urusan'         => $urusan ? $urusan->id : $id_urusan,
                            'kode'              => $kode,
                            'nama'              => $nama
                        );
                        $dataInsert++;
                    }
                } else {
                    break;
                    return $dataRes;
                }
            }

            if (isset($data['data']) && is_array($data['data']) && (count($data['data']) > 0)) {
                $CI->db->insert_batch($table, $data['data']);
            }

            if (($dataInsert > 0) || ($dataUpdate > 0)) {
                $dataRes = array(
                    'status'        => 'success',
                    'data_insert'   => $dataInsert,
                    'data_update'   => $dataUpdate
                );
            }

            if (is_array($dataRes) && ($dataRes['status'] == 'success')) {
                // Format Output Import Array ('status'=>'success', 'data_insert'=> 1, 'data_update'=> 2)
                bp_log_action('ACTION_MIGRATE_SUB_URUSAN', 'SUCCESS', 'MIGARE_SUB_URUSAN', json_encode(array('cookie' => $_COOKIE, 'status' => 'SUCCESS', 'data' => $data)));
            }
        }
        return $dataRes;
    }
}

if (!function_exists('bp_import_program')) {
    function bp_import_program($file = '', $unit = PROGRAM)
    {
        $CI                             = &get_instance();
        $data                           = array();
        $dataRes                        = array(
            'status'    => 'error'
        );
        $dataInsert                     = 0;
        $dataUpdate                     = 0;

        if (empty($file)) return $dataRes;

        $cfg_data_master                = config_item('data_master');

        if (!isset($cfg_data_master[$unit])) return $dataRes;

        $table                          = $cfg_data_master[$unit];
        $reader                         = bp_initialize_reader();

        $spreadsheet                    = $reader->load($file);

        if ($spreadsheet) {
            foreach ($spreadsheet->getWorksheetIterator() as $worksheet) {
                $highestRow             = $worksheet->getHighestRow();
                $highestColumn          = $worksheet->getHighestColumn();
                $data['higgestRow']     = $highestRow;
                $data['highestColumn']  = $highestColumn;
                $get_code               = $worksheet->getCellByColumnAndRow(2, 1)->getValue();
                $get_code               = isset($get_code) ? $get_code : '';

                if (!$get_code) return $dataRes;
                $get_code               = bp_decrypt($get_code);

                $validate_code          = get_option('import_code');
                $validate_code          = isset($validate_code) ? $validate_code : '';

                if (!$validate_code) return $dataRes;
                $validate_code          = bp_decrypt($validate_code);

                if ($get_code != $validate_code) return $dataRes;

                $header_no              = $worksheet->getCellByColumnAndRow(1, 2)->getValue();
                $header_code            = $worksheet->getCellByColumnAndRow(2, 2)->getValue();
                $header_name            = $worksheet->getCellByColumnAndRow(3, 2)->getValue();
                $header_fk              = $worksheet->getCellByColumnAndRow(4, 2)->getValue();

                if (($header_no == 'ID') && ($header_code == 'KODE') && ($header_name == 'NAMA') && ($header_fk == 'ID_SUB_URUSAN')) {

                    for ($row = 3; $row <= $highestRow; $row++) {
                        $kode           = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                        $nama           = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                        $id_sub_urusan  = $worksheet->getCellByColumnAndRow(4, $row)->getValue();

                        $sub_urusan         = $CI->Model_Master->get_sub_urusan_by('id', $id_sub_urusan, '', 1);
                        $sub_urusan         = isset($sub_urusan) ? $sub_urusan : null;

                        if (!$sub_urusan) continue;

                        $getData        = $CI->Model_Master->get_program_by('kode', $kode, '', 1);

                        if ($getData) {
                            $data_update = array(
                                'id_sub_urusan'     => $id_sub_urusan,
                                'kode'              => $kode,
                                'nama'              => $nama
                            );
                            if (!$CI->Model_Master->update_data_program($getData->id, $data_update)) {
                                break;
                                return $dataRes;
                            }
                            $dataUpdate++;
                            continue;
                        }

                        $data['data'][] = array(
                            'id_sub_urusan'         => isset($sub_urusan) ? $sub_urusan->id : $id_sub_urusan,
                            'kode'                  => $kode,
                            'nama'                  => $nama
                        );
                        $dataInsert++;
                    }
                } else {
                    break;
                    return $dataRes;
                }
            }

            if (isset($data['data']) && is_array($data['data']) && (count($data['data']) > 0)) {
                $CI->db->insert_batch($table, $data['data']);
            }

            if (($dataInsert > 0) || ($dataUpdate > 0)) {
                $dataRes = array(
                    'status'        => 'success',
                    'data_insert'   => $dataInsert,
                    'data_update'   => $dataUpdate
                );
            }

            if (is_array($dataRes) && ($dataRes['status'] == 'success')) {
                // Format Output Import Array ('status'=>'success', 'data_insert'=> 1, 'data_update'=> 2)
                bp_log_action('ACTION_MIGRATE_PROGRAM', 'SUCCESS', 'MIGARE_PROGRAM', json_encode(array('cookie' => $_COOKIE, 'status' => 'SUCCESS', 'data' => $data)));
            }
        }
        return $dataRes;
    }
}

if (!function_exists('bp_import_kegiatan')) {
    function bp_import_kegiatan($file = '', $unit = KEGIATAN)
    {
        $CI                             = &get_instance();
        $data                           = array();
        $dataRes                        = array(
            'status'    => 'error'
        );
        $dataInsert                     = 0;
        $dataUpdate                     = 0;

        if (empty($file)) return $dataRes;

        $cfg_data_master                = config_item('data_master');

        if (!isset($cfg_data_master[$unit])) return $dataRes;

        $table                          = $cfg_data_master[$unit];
        $reader                         = bp_initialize_reader();

        $spreadsheet                    = $reader->load($file);

        if ($spreadsheet) {
            foreach ($spreadsheet->getWorksheetIterator() as $worksheet) {
                $highestRow             = $worksheet->getHighestRow();
                $highestColumn          = $worksheet->getHighestColumn();
                $data['higgestRow']     = $highestRow;
                $data['highestColumn']  = $highestColumn;
                $get_code               = $worksheet->getCellByColumnAndRow(2, 1)->getValue();
                $get_code               = isset($get_code) ? $get_code : '';

                if (!$get_code) return $dataRes;
                $get_code               = bp_decrypt($get_code);

                $validate_code          = get_option('import_code');
                $validate_code          = isset($validate_code) ? $validate_code : '';

                if (!$validate_code) return $dataRes;
                $validate_code          = bp_decrypt($validate_code);

                if ($get_code != $validate_code) return $dataRes;

                $header_no              = $worksheet->getCellByColumnAndRow(1, 2)->getValue();
                $header_code            = $worksheet->getCellByColumnAndRow(2, 2)->getValue();
                $header_name            = $worksheet->getCellByColumnAndRow(3, 2)->getValue();
                $header_fk              = $worksheet->getCellByColumnAndRow(4, 2)->getValue();

                if (($header_no == 'ID') && ($header_code == 'KODE') && ($header_name == 'NAMA') && ($header_fk == 'ID_PROGRAM')) {

                    for ($row = 3; $row <= $highestRow; $row++) {
                        $kode           = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                        $nama           = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                        $id_program     = $worksheet->getCellByColumnAndRow(4, $row)->getValue();

                        $program         = $CI->Model_Master->get_program_by('id', $id_program, '', 1);
                        $program         = isset($program) ? $program : null;

                        if (!$program) continue;

                        $getData        = $CI->Model_Master->get_kegiatan_by('kode', $kode, '', 1);

                        if ($getData) {
                            $data_update = array(
                                'id_program'        => $id_program,
                                'kode'              => $kode,
                                'nama'              => $nama
                            );
                            if (!$CI->Model_Master->update_data_kegiatan($getData->id, $data_update)) {
                                break;
                                return $dataRes;
                            }
                            $dataUpdate++;
                            continue;
                        }

                        $data['data'][] = array(
                            'id_program'            => isset($program) ? $program->id : $id_program,
                            'kode'                  => $kode,
                            'nama'                  => $nama
                        );
                        $dataInsert++;
                    }
                } else {
                    break;
                    return $dataRes;
                }
            }

            if (isset($data['data']) && is_array($data['data']) && (count($data['data']) > 0)) {
                $CI->db->insert_batch($table, $data['data']);
            }

            if (($dataInsert > 0) || ($dataUpdate > 0)) {
                $dataRes = array(
                    'status'        => 'success',
                    'data_insert'   => $dataInsert,
                    'data_update'   => $dataUpdate
                );
            }

            if (is_array($dataRes) && ($dataRes['status'] == 'success')) {
                // Format Output Import Array ('status'=>'success', 'data_insert'=> 1, 'data_update'=> 2)
                bp_log_action('ACTION_MIGRATE_KEGIATAN', 'SUCCESS', 'MIGARE_KEGIATAN', json_encode(array('cookie' => $_COOKIE, 'status' => 'SUCCESS', 'data' => $data)));
            }
        }
        return $dataRes;
    }
}

if (!function_exists('bp_import_sub_kegiatan')) {
    function bp_import_sub_kegiatan($file = '', $unit = SUB_KEGIATAN)
    {
        $CI                             = &get_instance();
        $data                           = array();
        $dataRes                        = array(
            'status'    => 'error'
        );
        $dataInsert                     = 0;
        $dataUpdate                     = 0;

        if (empty($file)) return $dataRes;

        $cfg_data_master                = config_item('data_master');

        if (!isset($cfg_data_master[$unit])) return $dataRes;

        $table                          = $cfg_data_master[$unit];
        $reader                         = bp_initialize_reader();

        $spreadsheet                    = $reader->load($file);

        if ($spreadsheet) {
            foreach ($spreadsheet->getWorksheetIterator() as $worksheet) {
                $highestRow             = $worksheet->getHighestRow();
                $highestColumn          = $worksheet->getHighestColumn();
                $data['higgestRow']     = $highestRow;
                $data['highestColumn']  = $highestColumn;
                $get_code               = $worksheet->getCellByColumnAndRow(2, 1)->getValue();
                $get_code               = isset($get_code) ? $get_code : '';

                if (!$get_code) return $dataRes;
                $get_code               = bp_decrypt($get_code);

                $validate_code          = get_option('import_code');
                $validate_code          = isset($validate_code) ? $validate_code : '';

                if (!$validate_code) return $dataRes;
                $validate_code          = bp_decrypt($validate_code);

                if ($get_code != $validate_code) return $dataRes;

                $header_no              = $worksheet->getCellByColumnAndRow(1, 2)->getValue();
                $header_code            = $worksheet->getCellByColumnAndRow(2, 2)->getValue();
                $header_name            = $worksheet->getCellByColumnAndRow(3, 2)->getValue();
                $header_fk              = $worksheet->getCellByColumnAndRow(4, 2)->getValue();

                if (($header_no == 'ID') && ($header_code == 'KODE') && ($header_name == 'NAMA') && ($header_fk == 'ID_KEGIATAN')) {

                    for ($row = 3; $row <= $highestRow; $row++) {
                        $kode           = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                        $nama           = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                        $id_kegiatan    = $worksheet->getCellByColumnAndRow(4, $row)->getValue();

                        $kegiatan       = $CI->Model_Master->get_kegiatan_by('id', $id_kegiatan, '', 1);
                        $kegiatan       = isset($kegiatan) ? $kegiatan : null;

                        if (!$kegiatan) continue;

                        $getData        = $CI->Model_Master->get_sub_kegiatan_by('kode', $kode, '', 1);

                        if ($getData) {
                            $data_update = array(
                                'id_kegiatan'       => $id_kegiatan,
                                'kode'              => $kode,
                                'nama'              => $nama
                            );
                            if (!$CI->Model_Master->update_data_sub_kegiatan($getData->id, $data_update)) {
                                break;
                                return $dataRes;
                            }
                            $dataUpdate++;
                            continue;
                        }

                        $data['data'][] = array(
                            'id_kegiatan'            => isset($kegiatan) ? $kegiatan->id : $id_kegiatan,
                            'kode'                  => $kode,
                            'nama'                  => $nama
                        );
                        $dataInsert++;
                    }
                } else {
                    break;
                    return $dataRes;
                }
            }

            if (isset($data['data']) && is_array($data['data']) && (count($data['data']) > 0)) {
                $CI->db->insert_batch($table, $data['data']);
            }

            if (($dataInsert > 0) || ($dataUpdate > 0)) {
                $dataRes = array(
                    'status'        => 'success',
                    'data_insert'   => $dataInsert,
                    'data_update'   => $dataUpdate
                );
            }

            if (is_array($dataRes) && ($dataRes['status'] == 'success')) {
                bp_log_action('ACTION_MIGRATE_SUB_KEGIATAN', 'SUCCESS', 'MIGARE_SUB_KEGIATAN', json_encode(array('cookie' => $_COOKIE, 'status' => 'SUCCESS', 'data' => $data)));
            }
        }
        return $dataRes;
    }
}

if (!function_exists('bp_import_skpd')) {
    function bp_import_skpd($file = '', $unit = SKPD, $debug = TRUE)
    {
        $CI                             = &get_instance();
        $data                           = array();

        $dataRes                        = array(
            'status'    => 'error',
            'message'   => 'Terjadi kesalahan dalam proses import',
        );

        $dataInsert                     = 0;
        $dataUpdate                     = 0;

        if (empty($file)) {
            $dataRes['message']         = 'File tidak ditemukan';
            return $dataRes;
        }

        $cfg_data_master                = config_item('data_master');

        if (!isset($cfg_data_master[$unit])) {
            $dataRes['message']         = 'Konfigurasi Tabel belum di set!';
            return $dataRes;
        }

        $table                          = $cfg_data_master[$unit];
        $reader                         = bp_initialize_reader();

        $spreadsheet                    = $reader->load($file);

        if ($spreadsheet) {
            foreach ($spreadsheet->getWorksheetIterator() as $worksheet) {
                $highestRow             = $worksheet->getHighestRow();
                $highestColumn          = $worksheet->getHighestColumn();
                $data['higgestRow']     = $highestRow;
                $data['highestColumn']  = $highestColumn;

                $get_code               = $worksheet->getCellByColumnAndRow(2, 1)->getValue();
                $get_code               = isset($get_code) ? $get_code : '';

                if (!$get_code) {
                    $dataRes['message'] = 'Validasi Kode di file belum di set.';
                    return $dataRes;
                }

                $get_code               = bp_decrypt($get_code);

                $validate_code          = get_option('import_code');
                $validate_code          = isset($validate_code) ? $validate_code : '';

                if (!$validate_code) {
                    $dataRes['message'] = 'Konfigurasi Validasi Kode Belum di set.';
                    return $dataRes;
                }

                $validate_code          = bp_decrypt($validate_code);

                if ($get_code != $validate_code) {
                    $dataRes['message'] = 'Validasi Kode tidak sesuai';
                    return $dataRes;
                }



                $header_id              = $worksheet->getCellByColumnAndRow(1, 2)->getValue();
                $header_name            = $worksheet->getCellByColumnAndRow(2, 2)->getValue();
                $header_u1              = $worksheet->getCellByColumnAndRow(3, 2)->getValue();
                $header_u2              = $worksheet->getCellByColumnAndRow(4, 2)->getValue();
                $header_u3              = $worksheet->getCellByColumnAndRow(5, 2)->getValue();
                $header_urut            = $worksheet->getCellByColumnAndRow(6, 2)->getValue();
                $header_unit            = $worksheet->getCellByColumnAndRow(7, 2)->getValue();
                $header_code            = $worksheet->getCellByColumnAndRow(8, 2)->getValue();
                $header_bidang          = $worksheet->getCellByColumnAndRow(9, 2)->getValue();

                if (
                    ($header_id == 'ID') && ($header_name == 'NAMA') && ($header_u1 == 'U1')
                    && ($header_u2 == 'U2') && ($header_u3 == 'U3') && ($header_urut == 'URUT')
                    && ($header_unit == 'UNIT') && ($header_code == 'KODE_SKPD') && ($header_bidang == 'ID_BIDANG')
                ) {
                    for ($row = 3; $row <= $highestRow; $row++) {

                        $nama           = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                        $u1             = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                        $u2             = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                        $u3             = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
                        $urut           = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
                        $unit           = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
                        $kode           = $worksheet->getCellByColumnAndRow(8, $row)->getValue();
                        $bidang         = $worksheet->getCellByColumnAndRow(9, $row)->getValue();

                        $getData        = $CI->Model_Master->get_bidang_by('id', $bidang, '', 1);
                        $getSkpd        = $CI->Model_Master->get_skpd_by('kode', $kode, '', 1);

                        if ($u1 && $u1 == '0.00') {
                            if (!$sub_urusan = $CI->Model_Master->get_sub_urusan_by('kode', $u1, '', 0)) {
                                $u1 = '0.00';
                            }
                        }

                        if ($u2 && $u2 == '0.00') {
                            if (!$sub_urusan = $CI->Model_Master->get_sub_urusan_by('kode', $u2, '', 0)) {
                                $u2 = '0.00';
                            }
                        }

                        if ($u3 && $u3 == '0.00') {
                            if (!$sub_urusan = $CI->Model_Master->get_sub_urusan_by('kode', $u3, '', 0)) {
                                $u3 = '0.00';
                            }
                        }

                        if ($getSkpd) {
                            $data_update = array(
                                'kode'              => $kode,
                                'nama'              => $nama,
                                'u1'                => $u1,
                                'u2'                => $u2,
                                'u3'                => $u3,
                                'urut'              => $urut,
                                'unit'              => $unit,
                                'id_bidang'         => $getData->id
                            );

                            if (!$CI->Model_Master->update_data_skpd($getSkpd->id, $data_update)) {
                                break;
                                return $dataRes;
                            }

                            $dataUpdate++;
                            continue;
                        }

                        // Check available Urusan Data
                        $data['data'][] = array(
                            'kode'              => $kode,
                            'nama'              => $nama,
                            'u1'                => $u1,
                            'u2'                => $u2,
                            'u3'                => $u3,
                            'urut'              => $urut,
                            'unit'              => $unit,
                            'id_bidang'         => $getData->id
                        );
                        $dataInsert++;
                    }
                } else {
                    return $dataRes;
                }
            }

            if (isset($data['data']) && is_array($data['data']) && (count($data['data']) > 0)) {
                $CI->db->insert_batch($table, $data['data']);
            }

            if (($dataInsert > 0) || ($dataUpdate > 0)) {
                $dataRes = array(
                    'status'        => 'success',
                    'data_insert'   => $dataInsert,
                    'data_update'   => $dataUpdate
                );
            }

            if (is_array($dataRes) && ($dataRes['status'] == 'success')) {
                // Format Output Import Array ('status'=>'success', 'data_insert'=> 1, 'data_update'=> 2)
                bp_log_action('ACTION_MIGRATE_URUSAN', 'SUCCESS', 'MIGARE_URUSAN', json_encode(array('cookie' => $_COOKIE, 'status' => 'SUCCESS', 'data' => $data)));
            }
        }
        return $dataRes;
    }
}

if (!function_exists('bp_import_transaction')) {
    function bp_import_transaction($file = '', $unit = URUSAN)
    {
        $CI                             = &get_instance();
        $data                           = array();
        $dataRes                        = array(
            'status'    => 'error'
        );
        $dataInsert                     = 0;
        $dataUpdate                     = 0;
        if (empty($file)) return $dataRes;

        // $cfg_data_master                = config_item('data_master');

        // if (!isset($cfg_data_master[$unit])) return $dataRes;

        // $table                          = $cfg_data_master[$unit];
        $reader                         = bp_initialize_reader();

        $spreadsheet                    = $reader->load($file);

        if ($spreadsheet) {
            foreach ($spreadsheet->getWorksheetIterator() as $worksheet) {
                $highestRow             = $worksheet->getHighestRow();
                $highestColumn          = $worksheet->getHighestColumn();
                $data['higgestRow']     = $highestRow;
                $data['highestColumn']  = $highestColumn;

                // $get_code               = $worksheet->getCellByColumnAndRow(2, 1)->getValue();
                // $get_code               = isset($get_code) ? $get_code : '';

                // if (!$get_code) return $dataRes;
                // $get_code               = bp_decrypt($get_code);

                // $validate_code          = get_option('import_code');
                // $validate_code          = isset($validate_code) ? $validate_code : '';

                // if (!$validate_code) return $dataRes;
                // $validate_code          = bp_decrypt($validate_code);

                // if ($get_code != $validate_code) return null;

                function forRow() {
                    $highestRow             = $worksheet->getHighestRow();
                    $highestColumn          = $worksheet->getHighestColumn();
                    for ($row = 64; $row <= $highestRow; $row++) {
                        yield $row;
                    }
                }

                foreach (forRow() as $row) {
                    $urusan         = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                    $sub_urusan     = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                    $program        = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                    $kegiatan       = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                    $sub_kegiatan   = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
                    $name           = $worksheet->getCellByColumnAndRow(6, $row)->getValue();

                    if ( ! trim($sub_kegiatan) ) {
                        continue;
                    }

                    $kode           = $urusan .'.'. $sub_urusan .'.'. $program .'.'. $kegiatan .'.'. $sub_kegiatan;

                    // Get Data Sub Kegiatan
                    $getData        = $CI->Model_Master->get_sub_kegiatan_by('kode', $kode, '', 1);

                    // if ($getData) {

                    //     $data_update = array(
                    //         'kode'  => $kode,
                    //         'nama'  => $nama
                    //     );

                    //     // if (!$CI->Model_Master->update_data_urusan($getData->id, $data_update)) {
                    //     //     break;
                    //     //     return $dataRes;
                    //     // }
                    //     // $dataUpdate++;
                    //     // continue;
                    // }


                    // Check available Urusan Data
                    $data_row = array(
                        'urusan'        => $urusan,
                        'sub_urusan'    => $sub_urusan,
                        'program'       => $program,
                        'kegiatan'      => $kegiatan,
                        'sub_kegiatan'  => $sub_kegiatan,
                        'kode'          => $kode,
                        'nama'          => $name,
                        'getData'       => $getData
                    );

                    
                    // Check available Urusan Data
                    $data['data'][$row] = $data_row;
                    // var_dump($data);
                    var_dump($data_row);
                    // if ( trim($sub_kegiatan) ) {
                    //     break;
                    // }
                    // $dataInsert++;
                }
                // break;
            }

            $dataRes = array(
                'status'        => 'success',
                'data'          => $data,
            );
        }
        return $dataRes;
    }
}

if (!function_exists('bp_initialize_reader')) {
    function bp_initialize_reader()
    {
        /** Create a new Xls Reader  **/
        $reader                         = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        return $reader;
    }
}
