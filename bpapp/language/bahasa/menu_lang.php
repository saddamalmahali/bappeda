<?php

$lang['menu_dashboard']                 = 'Home';
$lang['menu_profile']                   = 'Profil';
$lang['menu_users']                   	= 'Pengguna';

$lang['menu_master'] 					= 'Master';
$lang['menu_master_urusan'] 			= 'Urusan';
$lang['menu_master_suburusan']			= 'Sub-Urusan';
$lang['menu_master_program'] 			= 'Program';
$lang['menu_master_kegiatan'] 			= 'Kegiatan';
$lang['menu_master_subkegiatan'] 		= 'Sub-Kegiatan';

$lang['menu_satuan_kerja'] 				= 'Satuan Kerja';
$lang['menu_bidang'] 					= 'Bidang';
$lang['menu_skpd'] 						= 'SKPD';

$lang['menu_tagging'] 					= 'Tagging';
$lang['menu_tagging_category']			= 'Kategori';
$lang['menu_tagging_theme'] 			= 'Tema';

$lang['menu_rkpd']						= 'RKPD';
$lang['menu_rkpd_change']				= 'RKPD Perubahan';
$lang['menu_kua_ppas']					= 'KUA dan PPAS';
$lang['menu_initial_plan']				= 'Rancangan Awal';
$lang['menu_plan']						= 'Rancangan';
$lang['menu_final_plan']				= 'Rancangan Akhir';
$lang['menu_making_plan']				= 'Penyusunan';
$lang['menu_consummation_plan'] 		= 'Penyempurnaan';

$lang['menu_member']                    = 'Member';
$lang['menu_member_new']                = 'Tambah Member Baru';
$lang['menu_member_list']               = 'List Member';
$lang['menu_member_tree']               = 'Jaringan Binary';
$lang['menu_member_generation']         = 'Jaringan Generasi';
$lang['menu_member_upgrade']            = 'Upgrade Paket';
$lang['menu_member_ro']                 = 'Aktivasi RO';
$lang['menu_member_loan']				= 'Stockist Loan';

$lang['menu_product']                   = 'Produk Manage';
$lang['menu_product_new']               = 'Tambah Produk Baru';
$lang['menu_product_edit']              = 'Edit Produk';
$lang['menu_package_new']               = 'Tambah Paket Baru';
$lang['menu_package_edit']              = 'Edit Paket';
$lang['menu_product_category']          = 'Kategori Produk';
$lang['menu_product_list']              = 'Master Produk';
$lang['menu_package_list']              = 'Master Paket';
$lang['menu_product_point']             = 'Poin Produk';
$lang['menu_product_order']             = 'Order Produk';

$lang['menu_promo_code']                = 'Kode Promo';
$lang['menu_promo_global']              = 'Global';
$lang['menu_promo_spesific']            = 'Produk Pilihan';

$lang['menu_financial']                 = 'Komisi';
$lang['menu_financial_bonus']           = 'Detail Komisi';
$lang['menu_financial_deposite']        = 'Deposit';
$lang['menu_financial_commission']      = 'Statement Komisi';
$lang['menu_financial_withdraw']        = 'Withdraw';

$lang['menu_flip']                      = 'Flip';
$lang['menu_flip_trx']                  = 'Transaksi';
$lang['menu_flip_topup']                = 'Topup';
$lang['menu_flip_inquiry']              = 'Inquiry Rekening';

$lang['menu_sicepat']                   = 'Sicepat';
$lang['menu_sicepat_waybill']           = 'Resi';
$lang['menu_sicepat_pickup_history']    = 'History Pickup';

$lang['menu_report']                    = 'Laporan';
$lang['menu_report_register']           = 'Pendaftaran';
$lang['menu_report_upgrade']            = 'Upgrade Paket';
$lang['menu_report_ro']                 = 'History RO';
$lang['menu_report_order']              = 'Order';
$lang['menu_report_sales']              = 'Penjualan Produk';
$lang['menu_report_buy']                = 'Pembelian Produk';
$lang['menu_report_omzet']              = 'Omset';
$lang['menu_report_omzet_posting']      = 'Omset Posting';
$lang['menu_report_omzet_order']        = 'Omset Order Produk';
$lang['menu_report_reward']             = 'Reward';
$lang['menu_report_pairing']            = 'Pasangan';
$lang['menu_report_rank_qualification'] = 'Kualifikasi Peringkat';

$lang['menu_setting']                   = 'Pengaturan';
$lang['menu_setting_general']           = 'Umum';
$lang['menu_setting_year']           	= 'Tahun';
$lang['menu_setting_stages']           	= 'Tahapan';
$lang['menu_setting_staff']             = 'Staff';
$lang['menu_setting_package']           = 'Paket';
$lang['menu_setting_product']           = 'Produk';
$lang['menu_setting_reward']            = 'Reward';
$lang['menu_setting_notification']      = 'Notifikasi';
$lang['menu_setting_withdraw']          = 'Withdraw';
$lang['menu_setting_grade']             = 'Peringkat';
$lang['menu_setting_bonus']             = 'Bonus';

/* End of file menu_lang.php */