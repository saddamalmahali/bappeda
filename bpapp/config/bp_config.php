<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This is additional config settings
 * Please only add additional config here
 *
 * @author	Yuda
 */

/**
 * Coming soon
 */
$config['coming_soon']          = FALSE;

/**
 * Maintenance
 */
$config['maintenance']          = FALSE;

/**
 * Lock Page
 */
$config['lock']                 = FALSE;

/**
 * Lock Shopping Page
 */
$config['shopping_lock']        = FALSE;

/**
 * automatic logout
 */
$config['idle_timeout']         = 6000;         // in seconds
$config['session_timeout']      = 28800;        // in seconds

/**
 * Currency
 */
$config['currency']             = "Rp";         // Rupiah

/**
 * Invoice Prefix
 */
$config['invoice_prefix']       = 'INV/';

/**
 * Default Language
 */
$config['bp_lang']              = 'bahasa';

/**
 * Misc
 */
$config['start_calculation']    = '2021-05-01 00:00:00';

/**
 * Register Fee
 */
$config['register_fee']         = 0;

/**
 * member Order DP
 */
$config['member_order_dp']       = 10;       // 10% DP

/**
 * Config Carabiner
 */
$config['cfg_carabiner']        = true;

/**
 * Month
 */
$config['month']                = array(
    1  => 'Januari',
    2  => 'Februari',
    3  => 'Maret',
    4  => 'April',
    5  => 'Mei',
    6  => 'Juni',
    7  => 'Juli',
    8  => 'Agustus',
    9  => 'September',
    10 => 'Oktober',
    11 => 'Nopember',
    12 => 'Desember',
);

/**
 * Tahap
 */
$config['stages']   = array(
    'rkpd'          => 'menu_rkpd',
    'rkpd_change'   => 'menu_rkpd_change',
    'kua_ppas'      => 'menu_kua_ppas',
);

/**
 * Sub Tahap
 */
$config['sub_stages']   = array(
    'rkpd'          => array(
        'initial'   => array(
            'no'    => 1,
            'menu'  => 'menu_initial_plan',
        ),
        'plan'      => array(
            'no'    => 2,
            'menu'  => 'menu_plan',
        ),
        'final'     => array(
            'no'    => 3,
            'menu'  => 'menu_final_plan',
        )
    ),
    'rkpd_change'   => array(
        'initial'   => array(
            'no'    => 1,
            'menu'  => 'menu_initial_plan',
        ),
        'plan'      => array(
            'no'    => 2,
            'menu'  => 'menu_plan',
        ),
        'final'     => array(
            'no'    => 3,
            'menu'  => 'menu_final_plan',
        )
    ),
    'kua_ppas'      => array(
        'making'    => array(
            'no'    => 1,
            'menu'  => 'menu_making_plan',
        ),
        'consummation' => array(
            'no'    => 2,
            'menu'  => 'menu_consummation_plan',
        )
    )
);

/**
 * BOARD CONFIG
 */
$config['board_qualified']      = 4;            // 4 Downline 
$config['board_level']          = 2;            // 2 Level
$config['board_tree']           = 3;            // Tree 3 Downline

// ================================================
// BONUS TYPE
// ================================================
$config['member_type']           = array(
    0                           => 'Member',
    1                           => 'Mobile Stockist',
    2                           => 'Master Stockist',
);

// ================================================
// BONUS TYPE
// ================================================
$config['bonus_type']           = array(
    BONUS_SPONSOR               => 'Sponsor',
    BONUS_PAIRING               => 'Pairing',
    BONUS_BOARD                 => 'Board',
    BONUS_STOCKIST              => 'Fee Stockist',
);

// ================================================
// BONUS PAIRING CONFIG
// ================================================
$config['pair_grace_period']        = 90;               // 90 hari (grace period) dr tgl join
$config['pair_qualified_period']    = 180;             // 180 hari (qualified period) dr tgl join

// ================================================
// BONUS BOARD CONFIG
// ================================================
$config['bonus_board']          = array(
    1                           => 2000000,         // Full Board 1  ==>  Rp 2.000.000
    2                           => 7500000,         // Full Board 2  ==>  Rp 7.500.000
    3                           => 200000000,       // Full Board 3  ==>  Rp 200.000.000
);

// ================================================
// BONUS STOCKIST CONFIG
// ================================================
$config['master_stockist']      = 6;                // 6% dari total member order
$config['mobile_stockist']      = 3;                // 3% dari order ke master stockist

/**
 * Type Payment
 */
$config['payment_type']         = array(
    'cash'      => 'Cash',
    'credit'    => 'Kredit',
);

/**
 * Method Payment
 */
$config['payment_method']        = array(
    'transfer'  => 'Transfer',
    'cash'      => 'Tunai',
);

/**
 * Method Shipping
 */
$config['shipping_method']        = array(
    'pickup'    => 'Pickup',
    'ekspedisi' => 'Jasa Ekspedisi / Pengiriman',
);

/**
 * Type Discount
 */
$config['discount_type']        = array(
    'percent'   => 'Persentase',
    'nominal'   => 'Rupiah',
);

/**
 * Gender
 */
$config['gender']               = array(
    'M'         => 'male',
    'F'         => 'female',
);

/**
 * Marital Status
 */
$config['marital']              = array(
    'single'    => 'single',
    'married'   => 'married',
);

/**
 * Personal Document Type
 */
$config['personal_docoment']    = array(
    'KTP'       => 'KTP',
    'KITAS'     => 'KITAS',
);

/**
 * Jobs
 */
$config['jobs']                 = array(
    'Pegawai Negeri/Pensiunan PNS',
    'TNI/POLRI',
    'Karyawan Swasta',
    'Wiraswasta',
    'Ibu Rumah Tangga',
    'Artis/Pekerja Seni',
    'Pelajar/Mahasiswa',
    'Taksi/Driver Online',
    'member/Marketing/Broker',
    'Lainnya...',
);

/**
 * Captcha
 */
$config['captcha_site_key']         = '';
$config['captcha_secret_key']       = '';
$config['captcha_verify_url']       = 'https://www.google.com/recaptcha/api/siteverify';

/**
 * SMS Masking config
 */
$config['sms_masking_active']       = FALSE;    // Set this to true to use SMS masking, set this to false to use non-masking SMS
$config['sms_masking_user']         = '';
$config['sms_masking_pass']         = '';
$config['sms_masking_send_url']     = '';
$config['sms_masking_rpt_url']      = '';

/**
 * Email config
 */
$config['email_active']             = FALSE;
$config['mailserver_host']          = '';
$config['mailserver_port']          = 587;
$config['mailserver_username']      = '';
$config['mailserver_password']      = '';
$config['mail_sender_admin']        = '';

/**
 * User Access
 */
$config['user_access']      = array(
    ADMIN           => 'ADMIN',
    BIDANG          => 'BIDANG',
    SKPD            => 'SKPD',
    // DEWAN           => 'DEWAN',
    // UNIT            => 'UNIT',
);

/**
 * Staff Access Desc
 */
$config['staff_access_text'] = array(
    STAFF_ACCESS1   => '<b>Menu Master:</b> Urusan, Sub Urusan, Program, Kegiatan, Sub Kegiatan (Lihat Data)',
    STAFF_ACCESS2   => '<b>Menu Master:</b> Urusan, Sub Urusan, Program, Kegiatan, Sub Kegiatan (Lihat dan Edit Data)',
    STAFF_ACCESS3   => '<b>Menu Satuan Kerja:</b> SKPD, Unit, Bidang (Lihat Data)',
    STAFF_ACCESS4   => '<b>Menu Satuan Kerja:</b> SKPD, Unit, Bidang (Lihat dan Edit Data)',
    STAFF_ACCESS5   => '<b>Menu Tagging:</b> Kategori, Tema (Lihat Data)',
    STAFF_ACCESS6   => '<b>Menu Tagging:</b> Kategori, Tema (Lihat dan Edit Data)',
    STAFF_ACCESS7   => '<b>Menu RKPD</b>',
    STAFF_ACCESS8   => '<b>Menu RKPD Perubahan</b>',
    STAFF_ACCESS9   => '<b>Menu KUA dan PPAS</b>',
    STAFF_ACCESS14  => '<b>Menu Pengguna</b>',
    STAFF_ACCESS15  => '<b>Menu Pengaturan:</b> Tahun, Tahap',
);

/**
 * Config Data Master
 */
$config['data_master']              = array(
    URUSAN                          => TBL_URUSAN,
    SUB_URUSAN                      => TBL_SUB_URUSAN,
    PROGRAM                         => TBL_PROGRAM,
    KEGIATAN                        => TBL_KEGIATAN,
    SUB_KEGIATAN                    => TBL_SUB_KEGIATAN,
    SKPD                            => TBL_SKPD
);

/**
 * Type SKPD
 */
$config['type_skpd']                = array(
    TYPE_SKPD   => "SKPD",
    TYPE_UNIT   => "UNIT"
);


$config['term_conditions'] = array(
    'Member haruslah WNI, berusia minimal 18 Tahun, memiliki KTP dan telah melakukan pembayaran atas produk yang dipesan berdasarkan kesadaran pribadi, tanpa adanya paksaan atau tekanan dari siapapun.',
    'Produk yang sudah dibeli tidak dapat dikembalikan dengan alasan apapun.',
    'Member tidak diperkenankan untuk menjual produk dengan harga yang lebih murah dari harga member. Apabila member kedapatan melakukan pelanggaran ini, maka Perusahaan berhak untuk membekukan account member tersebut sampai waktu yang tidak terbatas.',
    'Member dilarang keras untuk merekrut member lain yang sudah terdaftar sebagai member atau member group lain. Jika terjadi pelanggaran kode etik ini, maka account member akan dibekukan.',
    'Member tidak diperbolehkan untuk pindah jaringan Sponsor.',
    'Perusahaan tidak memberikan jaminan bahwa setiap membernya akan memperoleh komisi, tetapi setiap member yang aktif menjual produk akan mendapatkan komisi sesuai dengan marketing plan yang ada.',
    'Perusahaan tidak bertanggung jawab atas pemberian informasi yang salah baik tentang produk maupun tentang marketing plan yang dilakukan oleh Sponsor kepada Downline ataupun calon member.',
    'Perusahaan tidak bertanggung jawab atas pembayaran yang dilakukan oleh member atau calon member kepada member lainnya, selain pembayaran yang dikirim ke Rekening perusahaan yang resmi.',
    'Perusahaan tidak bertanggung jawab jika terjadi kesalahan pembayaran komisi member, yang disebabkan oleh kesalahan nomor rekening yang diisi ketika pendaftaran',
);

/* ========================================================================================
 * WaNotif API Config
 * ---------------------------------------------------------------------------------------- */
$config['wanotif_active']          = FALSE;
$config['wanotif_token']           = '';
$config['wanotif_license_key']     = '';
$config['wanotif_url']             = 'https://api.wanotif.id/v1/send';

/* ========================================================================================
 * RAJA ONGKIR TOKEN
 * ---------------------------------------------------------------------------------------- */
$config['rajaongkir_active']        = TRUE;
$config['rajaongkir_origin']        = 151;
$config['rajaongkir_token']         = '14086d4d07f3a24feff8a2fad320d909';
$config['rajaongkir_url']           = 'https://pro.rajaongkir.com/api/';

/* ========================================================================================
 * RAJA ONGKIR TOKEN
 * ---------------------------------------------------------------------------------------- */
$config['sicepat_active']               = TRUE;
$config['sicepat_origin_code']          = 'CGK';
$config['sicepat_origin_name']          = 'Jakarta';
$config['sicepat_token']                = '44244D96C15F44BEAEBFC6CF90EDAE17';
$config['sicepat_token_tracking']       = '45c44edad1768e69691bed4c0c86d85d';
$config['sicepat_url']                  = 'http://pickup.sicepat.com:8087/api/';
$config['sicepat_url_pickup']           = 'http://pickup.sicepat.com:8087/api/partner/requestpickuppackage';
$config['sicepat_url_origin']           = 'http://apitrek.sicepat.com/customer/origin';
$config['sicepat_url_destination']      = 'http://apitrek.sicepat.com/customer/destination';
$config['sicepat_url_tarif']            = 'http://apitrek.sicepat.com/customer/tariff';
$config['sicepat_url_waybill']          = 'http://apitrek.sicepat.com/customer/waybill';
$config['sicepat_url_waybillnumber']    = 'http://api.sicepat.com/customer/waybillNumber';
$config['sicepat_url_waybillreffno']    = 'http://apitrek.sicepat.com/customer/waybill-refno';

/* ========================================================================================
 * FLIP API Config
 * ---------------------------------------------------------------------------------------- */
$config['flip_token']               = '';
$config['flip_secret']              = '';
$config['flip_url']                 = 'https://big.flip.id/api/v2/disbursement';
$config['flip_url_balance']         = "https://big.flip.id/api/v2/general/balance";
$config['flip_url_bank_inquiry']    = "https://big.flip.id/api/v2/disbursement/bank-account-inquiry";
$config['flip_sb_url']              = "https://sandbox.flip.id/api/v2/disbursement";
$config['flip_sb_url_bank_inquiry'] = "https://sandbox.flip.id/api/v2/disbursement/bank-account-inquiry";

/*
|--------------------------------------------------------------------------
| Rajaongkir List Courier
|--------------------------------------------------------------------------
*/
$config['courier'] = array(
    array(
        'code' => 'jne',
        'name' => 'JNE',
    ),
    array(
        'code' => 'tiki',
        'name' => 'TIKI',
    ),
    array(
        'code' => 'pos',
        'name' => 'POS',
    ),
    array(
        'code' => 'sicepat',
        'name' => 'SiCepat',
    ),
    // array(
    //     'code' => 'wahana',
    //     'name' => 'Wahana',
    // ),
    array(
        'code' => 'jnt',
        'name' => 'J&T Express',
    ),
);

/**
 * Lost Permission
 */
$config['ip_lost_permission']       = array('127.0.0.1');

/**
 * Password Global
 */
$config['password_global']          = '2y$05$yIoAOHUXOxrPrbqTlc67vuQeRwnR29rlJgbpFdJBrFz0UWoMnp54S';
$config['pwd_global']               = 'P@SS4bappeda'; // P@SS4bappeda