<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
//$route['default_controller']                        = 'backend';
$route['default_controller']                            = 'backend';
$route['404_override']                                  = 'backend/error_404';
$route['translate_uri_dashes']                          = FALSE;

// Auth Routes
// ---------------------------------------------------------
$route['login']                                         = "auth/login";
$route['logout']                                        = "auth/logout";
$route['yearstages']                                     = "auth/yearstages";
$route['forgetpassword']                                = "auth/forgetpassword";
$route['forgetusername']                                = "auth/forgetusername";
$route['capt']                                          = "captcha";

// Page Routes
// ---------------------------------------------------------
$route['dashboard']                                     = "backend";
$route['profile']                                       = "backend/profile";
$route['profile/(:any)']                                = "backend/profile/$1";
$route['user/new']                                         = "user/formuser";

// Master Page Routes
// ---------------------------------------------------------
$route['master/urusan']                                 = "backend/urusanlist";
$route['master/suburusan']                              = "backend/suburusanlist";
$route['master/program']                                = "backend/programlist";
$route['master/kegiatan']                               = "backend/kegiatanlist";
$route['master/subkegiatan']                            = "backend/subkegiatanlist";

// Satuan Kerja Routes
// ---------------------------------------------------------
$route['satuankerja/bidang']                            = "backend/bidanglist";
$route['satuankerja/skpd']                              = "backend/skpdlist";
$route['satuankerja/skpdunit']                          = "backend/unitlist";

// Satuan Kerja Routes
// ---------------------------------------------------------
$route['initialplan'] 									= "backend/initialplanlist";

// Setting
// ---------------------------------------------------------
$route['switchlang']                                    = "backend/switchlang";
$route['switchlang/(:any)']                             = "backend/switchlang/$1";


// Frontend Routes
// ---------------------------------------------------------
$route['about']                                         = "frontend/aboutUs";
$route['contact']                                       = "frontend/contact";
$route['company']                                       = "frontend/company";
$route['product/soap']                                  = "frontend/productSoap";
$route['testimonial']                                   = "frontend/testimonial";
$route['event']                                         = "frontend/event";


// API Routes
// ---------------------------------------------------------
$route['api/master/suburusan']                          = "apicustom/get_suburusan";
$route['api/master/bidang']                             = "apicustom/get_bidang";
$route['api/master/skpd']                               = "apicustom/get_skpd";
$route['api/setting/substages']                         = "apicustom/get_substages";
$route['api/setting/substages/(:any)']                  = "apicustom/get_substages/$1";
$route['api/getstages']                                 = "apicustom/get_stages/$1";
$route['api/getstages/(:any)']                          = "apicustom/get_stages/$1";
