<div class="row justify-content-center">
    <div class="col-md-6">
        <div class="card login-card kd-content" data-name="<?php echo $this->security->get_csrf_token_name(); ?>" data-code="<?php echo $this->security->get_csrf_hash(); ?>">
            <div class="card-body pt-4 pb-3">
                <div class="card-title text-center">
                    <p class="mb-2 mt-3">
                        <img src="<?= LOGO_IMG_LOGIN ?>" alt="" width="20px">
                        <small class="ml-1 text-muted font-weight-bold"><?= COMPANY_NAME ?></small>
                    </p>
                    <p class="login-card-description mb-4">Silahkan Pilih Tahun dan Tahap Perencanaan.</p>
                </div>
                <form class="validate-form year-stages-form" method="post" action="<?php echo base_url('auth/validateyearstages'); ?>">
                    <div class="alert alert-danger error-validate" role="alert" style="line-height: 20px; text-align: center; display: none;">
                        <i class="mdi mdi-information-outline"></i>
                        <span style="font-size:12px;"> Ada beberapa kesalahan, silahkan cek formulir di bawah !</span>
                    </div>

                    <div class="form-group mb-0">
                        <select name="year" id="year" class="form-control select_year" data-url="<?php echo base_url('api/getstages') ?>">
                            <option value="">-- <?php echo lang('select') .' '. lang('year'); ?> -- </option>
                            <?php 
                                $cfg_stage_year = bp_stages(0, array('year'), 'year', 'DESC');
                                if ( $cfg_stage_year ) {
                                    foreach ($cfg_stage_year as $key => $row) {
                                        echo '<option value="'. $row->year .'">'. $row->year .'</option>';
                                    }
                                } else {
                                    $next_year      = date('Y', strtotime('+1 year'));
                                    $before_year    = date('Y', strtotime('-1 year'));
                                    for ($i=$next_year; $i>=$before_year; $i--) { 
                                        echo '<option value="'. $i .'">'. $i .'</option>';
                                    }
                                }
                            ?>
                        </select>
                    </div>

                    <div class="form-group mb-0 mt-2">
                        <select name="stages" id="stages" class="form-control select_stage">
                            <option value="">-- <?php echo lang('select') .' '. lang('step'); ?> -- </option>
                            <?php
                                $cfg_stages_opt = config_item('stages');
                                $cfg_stages_by  = bp_stage_by('year', $member->select_year, '', 0, 'stage');
                                if ( $cfg_stages_by ) {
                                    foreach ($cfg_stages_by as $key => $row) {
                                        $stage_name = isset($cfg_stages_opt[$row->stage]) ? lang($cfg_stages_opt[$row->stage]) : $row->stage;
                                        echo '<option value="'. $row->stage .'">'. $stage_name .'</option>';
                                    }
                                }
                            ?>
                        </select>
                    </div>
                    <input type="submit" name="login" id="login" class="btn btn-block login-btn mb-4 mt-3" type="button" value="Submit">
                </form>
                <a href="<?php echo base_url('logout'); ?>" class="text-reset login-card-footer-text d-block mb-0">Ingin Logout? Klik disni</a>
                <nav class="login-card-footer-nav mt-4 text-center">
                    <a href="<?php echo base_url(); ?>"><?php echo COMPANY_NAME; ?> &copy; 2021</a>
                </nav>
            </div>
        </div>
    </div>
</div>