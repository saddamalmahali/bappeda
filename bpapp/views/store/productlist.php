<?php
$cart_contents  = $this->cart->contents();
$shopping_lock  = config_item('shopping_lock');
$currency       = config_item('currency');
$type_product   = '';
?>

<?php if ($data) : ?>
	<?php foreach ($data as $row) : ?>
		<?php
		$product_id     = bp_encrypt($row->id);
		$img_src        = bp_product_image($row->image, false);
		$price          = $row->price_customer;

		$in_cart        = false;
		if ($cart_contents) {
			foreach ($cart_contents as $item) {
				$cart_product_id = isset($item['id']) ? $item['id'] : 'none';
				if ($cart_product_id == $row->id) {
					$in_cart = true;
				}
			}
		}
		?>

		<div class="col-lg-3 col-md-4 col-sm-6 col-6">
			<div class="single-product">
				<div class="product-img">
					<a href="javascript:;" class="btn-product-detail" data-url="<?php echo base_url('store/getproductdetail/' . $product_id); ?>">
						<img class="default-img" src="<?php echo $img_src ?>" alt="<?php echo $row->name; ?>">
						<img class="hover-img" src="<?php echo $img_src ?>" alt="<?php echo $row->name; ?>">
					</a>
					<div class="button-head">
						<div class="product-action pr-3">
							<a href="javascript:;" title="Quick View" class="btn-product-detail" data-url="<?php echo base_url('store/getproductdetail/' . $product_id); ?>">
								<i class=" ti-eye"></i><span>Detail Produk</span>
							</a>
						</div>
						<div class="product-action-2 pl-3">
							<?php if (!$shopping_lock) { ?>
								<?php if ($in_cart) { ?>
									<a title="Lihat keranjang" href="<?php echo base_url('store/cart'); ?>" class="add-to-cart" data-type="cart">Go to cart</a>
								<?php } else { ?>
									<a title="Masukan ke keranjang" href="<?php echo base_url('store/addToCart'); ?>" class="add-to-cart" data-type="addcart" data-cart="<?php echo $product_id; ?>">Add to cart</a>
								<?php } ?>
							<?php } ?>
						</div>
					</div>
				</div>
				<div class="product-content">
					<div class="product-name">
						<h3><a href="#"><?php echo ucwords(strtolower($row->name)); ?></a></h3>
					</div>
					<div class="row">
						<div class="col-sm-8 col-12">
							<div class="product-price"><span style="color: #ff5722 !important;"><?php echo bp_accounting($price, $currency); ?></span></div>
						</div>
						<div class="col-sm-4 col-12 right">
							<div class="mt-1 text-right text-success"><small><?php echo bp_accounting($row->bv); ?> Bv</small></div>
						</div>
					</div>
				</div>
			</div>
		</div>

	<?php endforeach; ?>
<?php else : ?>
	<div class="col-12 text-center">
		<div class="px-2 py-5">
			<h3 class="heading mt-5 mb-5">Produk tidak ditemukan</h3>
		</div>
	</div>
<?php endif; ?>