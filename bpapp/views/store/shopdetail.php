<!-- Breadcrumbs -->
<div class="breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="bread-inner">
                    <ul class="bread-list">
                        <li><a href="<?php echo base_url('/'); ?>">Home<i class="ti-arrow-right"></i></a></li>
                        <li><a href="<?php echo base_url('store'); ?>">Produk<i class="ti-arrow-right"></i></a></li>
                        <li class="active"><a href="javascript:;">#<?php echo $shop_order->invoice; ?></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Breadcrumbs -->

<section class="shopping-cart section pt-4">
    <div class="container">
        <?php
        $shop_order_id    = bp_encrypt($shop_order->id);
        $total_payment     = $shop_order->total_payment;
        $code_unique     = str_pad($shop_order->unique, 3, '0', STR_PAD_LEFT);
        $shop_details     = $this->Model_Shop->get_shop_customer_detail_by(array('id_shop_order' => $shop_order->id));
        $shop_details     = '';
        if (is_serialized($shop_order->products)) {
            $shop_details = maybe_unserialize($shop_order->products);
        }
        ?>
        <div class="card">
            <div class="card-body py-2" style="background-color: #b2dfdb !important">
                <div class="row">
                    <div class="col-6">
                        <p class="font-weight-bold">Invoice : <span style="color: #ff5722 !important;"><?php echo $shop_order->invoice; ?></span></p>
                    </div>
                    <div class="col-6 text-right">
                        <a class="text-primary" href="<?php echo base_url('store/invoice/' . $shop_order_id) ?>" target="_blank" style="font-size: 12px">
                            <b><i class="fa fa-print"></i> Cetak</b>
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-body py-2" style="background-color: #f5f5f5 !important">
                <table class="">
                    <thead>
                        <tr>
                            <td width="35%" class="text-muted text-uppercase py-1" style="font-size: 12px">Tanggal Transaksi</td>
                            <td width="35%" class="text-muted text-uppercase py-1" style="font-size: 12px">Total Pembayaran</td>
                            <td width="30%" class="text-muted text-uppercase py-1" style="font-size: 12px">Status Pesanan</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <p class="font-weight-bold"><?php echo date('d M Y', strtotime($shop_order->datecreated)); ?></p>
                                <p>Pukul <?php echo date('H:i', strtotime($shop_order->datecreated)); ?> WIB</p>
                            </td>
                            <td>
                                <h5 class="font-weight-bold" style="color: #ff5722 !important;"><?php echo bp_accounting($total_payment, $currency); ?></h5>
                            </td>
                            <td class="font-weight-bold">
                                <?php
                                $payment_status = 'Menuggu Pembayaran';
                                if ($shop_order->status == 1) {
                                    $payment_status = 'Pesanan Diproses';
                                }
                                if ($shop_order->status == 2) {
                                    $payment_status = 'Pesanan Dikirim';
                                }
                                if ($shop_order->status == 4) {
                                    $payment_status = 'Pesanan Dibatalkan';
                                }
                                echo $payment_status;
                                ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="card-body p-0">
                <?php if ($shop_details) : ?>
                    <table class="table shopping-summery">
                        <tbody>
                            <?php foreach ($shop_details as $row) :
                                $product_id       = isset($row['id']) ? $row['id'] : 'product_id';
                                $product_name   = isset($row['name']) ? $row['name'] : 'Produk';
                                $bv             = isset($row['bv']) ? $row['bv'] : 0;
                                $qty            = isset($row['qty']) ? $row['qty'] : 0;
                                $price          = isset($row['price']) ? $row['price'] : 0;
                                $price_cart     = isset($row['price_cart']) ? $row['price_cart'] : 0;
                                $discount       = isset($row['discount']) ? $row['discount'] : 0;
                                $weight           = isset($row['weight']) ? $row['weight'] : 0;
                                $subtotal       = $qty * $price_cart;
                                $total_weight     = $qty * $weight;

                                $productdata     = bp_products($product_id);
                                $product_img     = isset($productdata->image) ? $productdata->image : '';
                                $img_src        = bp_product_image($product_img, false);
                            ?>
                                <tr class="cart_item">
                                    <td class="image text-center py-2 pr-2">
                                        <img src="<?php echo $img_src; ?>" alt="<?php echo $product_name; ?>">
                                    </td>
                                    <td class="product-des py-2 pl-2" data-title="Produk">
                                        <p class="product-name" style="width: auto;"><a href="#"><?php echo $product_name; ?></a></p>
                                        <p class="product-des">
                                            <?php echo bp_accounting($qty) . ' <small>x</small> ' . bp_accounting($price_cart, $currency); ?>
                                        </p>
                                        <small class="text-mutred">
                                            Berat : <?php echo $total_weight; ?> gram
                                        </small>
                                    </td>
                                    <td class="total-amount text-right py-2" data-title="Total Harga">
                                        <span><?php echo bp_accounting($subtotal, $currency); ?></span>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td class="py-2"></td>
                                <td class="text-right py-2">Subtotal</td>
                                <td class="total-amount text-right py-2">
                                    <span class="font-weight-bold text-muted"><?php echo bp_accounting($shop_order->subtotal, $currency); ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="py-2"></td>
                                <td class="text-right py-2">Ongkir</td>
                                <td class="total-amount text-right py-2">
                                    <span class="font-weight-bold text-muted"><?php echo bp_accounting($shop_order->shipping, $currency); ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="py-2"></td>
                                <td class="text-right py-2">Kode Unik</td>
                                <td class="total-amount text-right py-2">
                                    <span class="font-weight-bold text-muted"><?php echo $code_unique; ?></span>
                                </td>
                            </tr>
                            <?php if ($shop_order->discount) { ?>
                                <tr>
                                    <td class="py-2"></td>
                                    <td class="text-right py-2">Diskon</td>
                                    <td class="total-amount text-right py-2">
                                        <span class="font-weight-bold text-success"><?php echo bp_accounting($shop_order->discount, $currency); ?></span>
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td class="py-2"></td>
                                <td class="h6 font-weight-bold text-muted text-right py-2">Total Pembayaran</td>
                                <td class="total-amount text-right py-2">
                                    <span class="h6 font-weight-bold"><?php echo bp_accounting($total_payment, $currency); ?></span>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                <?php endif; ?>
            </div>
            <div class="card-body py-2" style="background-color: #e0f2f1 !important">
                <div class="row">
                    <div class="col-12">
                        <p class="font-weight-bold"><i class="fa fa-map mr-3"></i> Informasi Alamat Pengiriman</p>
                    </div>
                </div>
            </div>
            <div class="card-body pt-3 pb-4">
                <div class="row align-items-center">
                    <div class="col-md-1 col-12"></div>
                    <div class="col-md-10 col-12">
                        <div class="row">
                            <div class="col-lg-2 col-sm-3 col-12"><span class="text-capitalize text-muted">Nama</span></div>
                            <div class="col-lg-10 col-sm-9 col-12"><span><?php echo $shop_order->name ?></span></div>
                        </div>
                        <div class="row">
                            <div class="col-lg-2 col-sm-3 col-12"><span class="text-capitalize text-muted">No. HP/WA</span></div>
                            <div class="col-lg-10 col-sm-9 col-12"><span><?php echo $shop_order->phone ?></span></div>
                        </div>
                        <div class="row">
                            <div class="col-lg-2 col-sm-3 col-12"><span class="text-capitalize text-muted">Email</span></div>
                            <div class="col-lg-10 col-sm-9 col-12"><span class="text-lowecase"><?php echo $shop_order->email ?></span></div>
                        </div>
                        <div class="row">
                            <?php
                            $address            = ucwords(strtolower($shop_order->address)) . ', ' . $shop_order->village . ' ';
                            $address           .= 'Kec ' . $shop_order->subdistrict . ' ' . $shop_order->district . ' ';
                            $address           .= $shop_order->province;
                            $address           .= ($shop_order->postcode) ? ' (' . $shop_order->postcode . ')' : '';
                            ?>
                            <div class="col-lg-2 col-sm-3 col-12"><span class="text-capitalize text-muted">Alamat</span><br></div>
                            <div class="col-lg-10 col-sm-9 col-12"><span class="text-capitalize"><?php echo $address ?></span></div>
                        </div>
                        <hr class="mb-3">
                        <div class="row">
                            <?php
                            $_shipping          = '';
                            if ($shop_order->shipping_method == 'ekspedisi') {
                                $_shipping  = 'Jasa Ekspedisi / Pengiriman';
                                if ($shop_order->courier) {
                                    $_shipping  = strtoupper($shop_order->courier);
                                    if ($shop_order->service) {
                                        $_shipping  .= ' (' . strtoupper($shop_order->service) . ')';
                                    }
                                }
                            }
                            if ($shop_order->shipping_method == 'pickup') {
                                $_shipping      = 'Pickup';
                            }
                            ?>
                            <div class="col-lg-2 col-sm-3 col-12"><span class="text-capitalize text-muted">Kurir</span><br></div>
                            <div class="col-lg-10 col-sm-9 col-12"><span><?php echo $_shipping ?></span></div>
                        </div>
                        <div class="row">
                            <div class="col-lg-2 col-sm-3 col-12"><span class="text-capitalize text-muted">Resi</span><br></div>
                            <div class="col-lg-10 col-sm-9 col-12"><span><?php echo ($shop_order->resi ? $shop_order->resi : '-') ?></span></div>
                        </div>
                    </div>
                </div>
            </div>
            <?php if ($shop_order->status == 0) : ?>
                <div class="card-body py-2" style="background-color: #e0f2f1 !important">
                    <div class="row">
                        <div class="col-12">
                            <p class="font-weight-bold"><i class="fa fa-credit-card mr-3"></i> Informasi Pembayaran</p>
                        </div>
                    </div>
                </div>
                <div class="card-body pt-3 pb-5">
                    <?php
                    $bill_bank          = '';
                    $bill_no            = get_option('company_bill');
                    $bill_name          = get_option('company_bill_name');
                    if ($company_bank = get_option('company_bank')) {
                        if ($getBank = bp_banks($company_bank)) {
                            $bill_bank = $getBank->nama;
                        }
                    }

                    if ($bill_no) {
                        $bill_format = '';
                        $arr_bill    = str_split($bill_no, 4);
                        foreach ($arr_bill as $no) {
                            $bill_format .= $no . ' ';
                        }
                        $bill_no = $bill_format ? $bill_format : $bill_no;
                    }
                    ?>
                    <div class="row align-items-center">
                        <div class="col-md-1 col-12"></div>
                        <div class="col-md-10 col-12">
                            <p class="mb-2">Pembayaran dapat dilakukan melalui Rekening Bank berikut :</p>
                            <div class="row">
                                <div class="col-lg-2 col-sm-3 col-12"><span class="text-capitalize text-muted">Bank</span></div>
                                <div class="col-lg-10 col-sm-9 col-12"><span class="font-weight-bold"><?php echo $bill_bank ?></span></div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2 col-sm-3 col-12"><span class="text-capitalize text-muted">No. Rekening</span></div>
                                <div class="col-lg-10 col-sm-9 col-12"><span class="font-weight-bold"><?php echo $bill_no ?></span></div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2 col-sm-3 col-12"><span class="text-capitalize text-muted">Atas Nama</span></div>
                                <div class="col-lg-10 col-sm-9 col-12"><span class="text-capitalize font-weight-bold"><?php echo $bill_name ?></span></div>
                            </div>
                            <?php if ($shop_order->status == 0) { ?>
                                <div class="mt-4 py-3 text-center" style="background: #0b7346; color: white;">
                                    Silahkan Transfer Pembayaran sebasar <span class="h6 font-weight-bold px-1"><?php echo bp_accounting($shop_order->total_payment, $currency) ?></span> Ke Rekening di atas.
                                </div>
                            <?php } ?>
                        </div>
                        <!-- <div class="col-md-3 col-12 pt-3">
                		<p class="mb-1 text-uppercase">Total Pembayaran</p>
                		<p class="h4 font-weight-bold" style="color: #ff5722 !important;"><?php echo bp_accounting($total_payment, $currency); ?></p>
                	</div> -->
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>