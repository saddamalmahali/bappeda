<!-- Breadcrumbs -->
<div class="breadcrumbs">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="bread-inner">
					<ul class="bread-list">
						<li><a href="<?php echo base_url('/'); ?>">Home<i class="ti-arrow-right"></i></a></li>
						<li><a href="<?php echo base_url('store'); ?>">Produk<i class="ti-arrow-right"></i></a></li>
						<?php if ($shop_order) : ?>
							<li class="active"><a href="javascript:;">#<?php echo $invoice; ?></a></li>
						<?php else : ?>
							<li class="active"><a href="javascript:;">Telusuri Pesanan</a></li>
						<?php endif; ?>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- End Breadcrumbs -->

<?php if ($shop_order) : ?>
	<section class="shopping-cart section pt-4 mb-5">
		<div class="container">
			<?php
			$shop_order_id	= bp_encrypt($shop_order->id);
			$total_payment 	= $shop_order->total_payment;
			$shop_details 	= $this->Model_Shop->get_shop_customer_detail_by(array('id_shop_order' => $shop_order->id));
			$shop_details 	= '';
			if (is_serialized($shop_order->products)) {
				$shop_details = maybe_unserialize($shop_order->products);
			}
			?>
			<div class="card">
				<div class="card-body py-2" style="background-color: #b2dfdb !important">
					<div class="row no-margin">
						<div class="col-6">
							<p class="font-weight-bold">Invoice : <?php echo $shop_order->invoice; ?></p>
						</div>
						<div class="col-6 text-right">
							<a class="text-primary" href="<?php echo base_url('store/detailorder/' . $shop_order_id) ?>" style="font-size: 12px">
								<b>Lihat Detail Pesanan</b>
							</a>
						</div>
					</div>
				</div>
				<div class="card-body py-2" style="background-color: #e0f2f1 !important">
					<table class="">
						<thead>
							<tr>
								<td width="35%" class="text-muted text-uppercase py-1" style="font-size: 12px">Tanggal Transaksi</td>
								<td width="35%" class="text-muted text-uppercase py-1" style="font-size: 12px">Total Pembayaran</td>
								<td width="30%" class="text-muted text-uppercase py-1" style="font-size: 12px">Status Pesanan</td>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<p class="font-weight-bold"><?php echo date('d M Y', strtotime($shop_order->datecreated)); ?></p>
									<p>Pukul <?php echo date('H:i', strtotime($shop_order->datecreated)); ?> WIB</p>
								</td>
								<td>
									<h5 class="font-weight-bold" style="color: #ff5722 !important;"><?php echo bp_accounting($total_payment, $currency); ?></h5>
								</td>
								<td class="font-weight-bold">
									<?php
									$payment_status = 'Menuggu Pembayaran';
									if ($shop_order->status == 1) {
										$payment_status = 'Pesanan Diproses';
									}
									if ($shop_order->status == 2) {
										$payment_status = 'Pesanan Dikirim';
									}
									if ($shop_order->status == 4) {
										$payment_status = 'Pesanan Dibatalkan';
									}
									echo $payment_status;
									?>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="card-body p-0">
					<?php if ($shop_details) : ?>
						<table class="table shopping-summery">
							<tbody>
								<?php foreach ($shop_details as $row) :
									$product_id   	= isset($row['id']) ? $row['id'] : 'product_id';
									$product_name   = isset($row['name']) ? $row['name'] : 'Produk';
									$bv             = isset($row['bv']) ? $row['bv'] : 0;
									$qty            = isset($row['qty']) ? $row['qty'] : 0;
									$price          = isset($row['price']) ? $row['price'] : 0;
									$price_cart     = isset($row['price_cart']) ? $row['price_cart'] : 0;
									$discount       = isset($row['discount']) ? $row['discount'] : 0;
									$weight       	= isset($row['weight']) ? $row['weight'] : 0;
									$subtotal       = $qty * $price_cart;
									$total_weight 	= $qty * $weight;

									$productdata 	= bp_products($product_id);
									$product_img 	= isset($productdata->image) ? $productdata->image : '';
									$img_src        = bp_product_image($product_img, false);
								?>
									<tr class="cart_item">
										<td class="image text-center pr-4" data-title="No">
											<img src="<?php echo $img_src; ?>" alt="<?php echo $product_name; ?>">
										</td>
										<td class="product-des pl-2" data-title="Description">
											<p class="product-name" style="width: 500px"><a href="#"><?php echo $product_name; ?></a></p>
											<p class="product-des">
												<?php echo bp_accounting($qty) . ' <small>x</small> ' . bp_accounting($price_cart, $currency); ?>
											</p>
											<small class="text-mutred">
												Berat : <?php echo $total_weight; ?> gram
											</small>
										</td>
										<td class="total-amount text-right" data-title="Subtotal">
											<span class="heading-small font-weight-bold text-success"><?php echo bp_accounting($subtotal, $currency); ?></span>
										</td>
									</tr>
							<?php endforeach;
							endif;
							?>
							</tbody>
						</table>
				</div>
			</div>
		</div>
	</section>
<?php else : ?>
	<section class="shop-newsletter section">
		<div class="container">
			<div class="inner-top py-5">
				<div class="row">
					<div class="col-lg-8 offset-lg-2 col-12">
						<div class="inner">
							<h4>Cari Pesanan</h4>
							<p>Silahkan masukan <span>Nomor Invoice</span> untuk mengetahui pesanan anda.</p>
							<form action="" method="get" class="newsletter-inner">
								<input type="text" name="invoice" placeholder="Nomor Invoice" class="text-uppercase" value="<?php echo $invoice; ?>">
								<button class="btn">Cari</button>
							</form>
						</div>
					</div>
				</div>
				<div class="row py-4 mt-4 mb-4">
					<div class="col-12 text-center mb-5">
						<?php if ($invoice) : ?>
							<p class="text-muted mb-3">Nomor Invoice <b class="text-danger text-uppercase"><?php echo $invoice; ?></b> tidak ditemukan.</p>
						<?php endif; ?>
						<a href="<?php echo base_url('store'); ?>" class="btn btn-sm text-white py-2 mb-2">Belanja Sekarang</a>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>