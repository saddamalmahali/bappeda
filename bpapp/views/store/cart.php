<?php
$sort_by = array(
	'dateupdate'	=> 'Terbaru',
	'name_asc'		=> 'Nama A-Z',
	'name_desc'		=> 'Nama Z-A',
	'price_asc'		=> 'Harga Terendah',
	'price_desc'	=> 'Harga Tertinggi',
);
?>

<!-- Breadcrumbs -->
<div class="breadcrumbs">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="bread-inner">
					<ul class="bread-list">
						<li><a href="<?php echo base_url('/'); ?>">Home<i class="ti-arrow-right"></i></a></li>
						<li><a href="<?php echo base_url('store'); ?>">Produk<i class="ti-arrow-right"></i></a></li>
						<li class="active"><a href="javascript:;">Keranjang Belanja</a></li>
					</ul>
					<span style="float: right">
						<a href="<?php echo base_url('store/searchorder'); ?>" class="text-white">Telusuri Pesanan</a>
					</span>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- End Breadcrumbs -->

<div class="shopping-cart section pt-4 mb-5">
	<div class="container mb-5">
		<?php if ($cart_content) : $num = 1; ?>
			<div class="row">
				<div class="col-12">
					<table class="table shopping-summery">
						<thead>
							<tr class="main-hading">
								<th colspan="2">Produk</th>
								<th class="text-center">Harga</th>
								<th class="text-center">Qty</th>
								<th class="text-center">Total</th>
								<th class="text-center"></th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($cart_content['data'] as $key => $row) : ?>
								<?php
								$product_id     = bp_encrypt($row['id']);
								$cart_price     = bp_accounting($row['cart_price']);
								$cart_subtotal  = bp_accounting($row['cart_subtotal']);
								?>
								<tr class="cart_item">
									<td class="image text-center pr-4" data-title="">
										<img src="<?php echo $row['product_image']; ?>" alt="<?php echo $row['product_name']; ?>">
									</td>
									<td class="product-des pl-2" data-title="Produk">
										<p class="product-name"><a href="#"><?php echo $row['product_name']; ?></a></p>
										<p class="product-des text-success"><small><?php echo $row['product_bv']; ?> Bv</small></p>
										<p class="product-des">Berat : <?php echo $row['product_weight']; ?> gram</p>
									</td>
									<td class="price text-right" data-title="Harga">
										<span class="cart-item-price"><?php echo $cart_price; ?></span>
									</td>
									<td class="qty text-center" data-title="Qty">
										<!-- Input Order -->
										<div class="input-group">
											<div class="button minus">
												<button type="button" class="btn btn-primary btn-number" data-type="minus" data-field="products[<?php echo ($product_id) ?>][qty]" <?php echo ($row['qty'] == 1) ? 'disabled="disabled"' : ''; ?>>
													<i class="ti-minus"></i>
												</button>
											</div>
											<input type="text" name="products[<?php echo ($product_id) ?>][qty]" class="input-number cart-item-qty" data-min="1" data-max="100" data-rowid="<?php echo $row['rowid']; ?>" data-productid="<?php echo $product_id; ?>" data-num="<?php echo $num; ?>" data-price="<?php echo $row['cart_price']; ?>" data-weight="<?php echo $row['weight']; ?>" data-url="<?php echo base_url('store/updateQtyCart'); ?>" value="<?php echo $row['qty']; ?>">
											<div class="button plus">
												<button type="button" class="btn btn-primary btn-number" data-type="plus" data-field="products[<?php echo ($product_id) ?>][qty]">
													<i class="ti-plus"></i>
												</button>
											</div>
										</div>
										<!--/ End Input Order -->
									</td>
									<td class="total-amount text-right" data-title="Total">
										<span class="cart-item-subtotal"><?php echo $cart_subtotal; ?></span>
									</td>
									<td class="action" data-title="Hapus">
										<a href="<?php echo base_url('store/deleteCart/' . $row['rowid']); ?>" class="btn-product-cart-delete" data-id="<?php echo $row['rowid']; ?>">
											<i class="ti-trash remove-icon"></i>
										</a>
									</td>
								</tr>
							<?php $num++;
							endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<!-- Total Amount -->
					<div class="total-amount">
						<div class="row">
							<div class="col-lg-7 col-md-5 col-sm-6 col-12">
								<div class="left">
									<div class="coupon">
										<form action="#" target="_blank">
											<input type="text" class="voucher-code" name="Coupon" placeholder="Kode Voucher">
											<button class="btn">Apply</button>
										</form>
									</div>
								</div>
							</div>
							<div class="col-lg-5 col-md-7 col-sm-6 col-12">
								<?php
								$total_payment  = $this->cart->total();
								$cart_payment   = bp_accounting($total_payment, '', true);
								?>
								<div class="right">
									<ul>
										<li>Subtotal<span class="cart-total-paymnet"><?php echo $cart_payment; ?></span></li>
										<li>Diskon<span>0</span></li>
										<li class="last">Total Pembayaran<span class="cart-total-paymnet"><?php echo $cart_payment; ?></span></li>
									</ul>
									<div class="button5">
										<a href="<?php echo base_url('store/checkout'); ?>" class="btn" style="background-color: #0b7346;">Checkout</a>
										<a href="<?php echo base_url('store'); ?>" class="btn">Lihat Produk Lainnya</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!--/ End Total Amount -->
				</div>
			</div>
		<?php else : ?>
			<div class="row mb-5">
				<div class="col-12 text-center mt-4 mb-5">
					<table class="table shopping-summery mt-3" style="height: auto;">
						<thead>
							<tr class="main-hading">
								<th colspan="2">Produk</th>
								<th class="text-center">Harga</th>
								<th class="text-center">Qty</th>
								<th class="text-center">Total</th>
								<th class="text-center"></th>
							</tr>
						</thead>
					</table>
					<h4 class="text-muted mt-5 mb-5">Keranjang belanja anda masih kosong</h4>
					<a href="<?php echo base_url('store'); ?>" class="btn btn-success mb-5 text-white">Belanja Sekarang</a>
				</div>
			</div>
		<?php endif; ?>
	</div>
</div>