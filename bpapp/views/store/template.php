<!DOCTYPE HTML>
<!-- <?php echo get_option('company_name'); ?> Template v1.0 -->

<!--[if lt IE 7]><html dir="ltr" lang="en-US" class="lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html dir="ltr" lang="en-US" class="lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html dir="ltr" lang="en-US" class="lt-ie9"><![endif]-->
<!--[if IE 9]><html dir="ltr" lang="en-US"  class="lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!-->
<html dir="ltr" lang="en-US" class="gt-ie9 non-ie no-js">
<!--<![endif]-->

<!-- BEGIN TEMPLATE HEADER -->
<head>
    <!-- Meta Tags -->
	<meta charset="UTF-8" />
	<meta name="description" content="<?php echo COMPANY_NAME; ?>" />
	<meta name="author" content="<?php echo COMPANY_NAME; ?>" />
    <meta name="google" content="notranslate">
	
	<!-- Mobile -->
	<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
	
	<!-- Page Title =================================================== -->
	<title><?php echo $title; ?></title>

    <!-- Shortcut Icon ================================================ -->
    <link rel="shortcut icon" href="<?php echo LOGO_IMG ?>" type="image/x-icon">

    <!-- Fonts ================================================ -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet">

    <!-- Icons ======================================== -->
    <link rel="stylesheet" href="<?php echo ASSET_PATH; ?>store/css/font-awesome.css" type="text/css">
    <link rel="stylesheet" href="<?php echo ASSET_PATH; ?>store/css/themify-icons.css" type="text/css">

    <!-- Style ======================================== -->
    <link rel="stylesheet" href="<?php echo ASSET_PATH; ?>store/css/bootstrap.css" type="text/css">
    <link rel="stylesheet" href="<?php echo ASSET_PATH; ?>store/css/jquery-ui.css" type="text/css">
    <link rel="stylesheet" href="<?php echo ASSET_PATH; ?>store/css/jquery.fancybox.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo ASSET_PATH; ?>store/css/magnific-popup.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo ASSET_PATH; ?>store/css/niceselect.css" type="text/css">
    <link rel="stylesheet" href="<?php echo ASSET_PATH; ?>store/css/flex-slider.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo ASSET_PATH; ?>store/css/owl-carousel.css" type="text/css">
    <link rel="stylesheet" href="<?php echo ASSET_PATH; ?>store/css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo ASSET_PATH; ?>store/css/animate.css" type="text/css">

    <link rel="stylesheet" href="<?php echo ASSET_PATH; ?>store/css/reset.css?ver=<?php echo CSS_VER_STORE; ?>" type="text/css">
    <link rel="stylesheet" href="<?php echo ASSET_PATH; ?>store/css/style.css?ver=<?php echo CSS_VER_STORE; ?>" type="text/css">
    <link rel="stylesheet" href="<?php echo ASSET_PATH; ?>store/css/responsive.css?ver=<?php echo CSS_VER_STORE; ?>" type="text/css">
</head>
<!-- END TEMPLATE HEADER -->

<!-- BEGIN BODY -->
<body class="js">
	<!-- Preloader -->
	<div class="preloader">
		<div class="preloader-inner">
			<div class="preloader-icon">
				<span></span>
				<span></span>
			</div>
		</div>
	</div>
	<!-- End Preloader -->
		
	<!-- Header -->
    <?php $this->load->view(VIEW_STORE . 'header'); ?>
	<!--/ End Header -->

    <!-- BEGIN CONTENT -->
    <?php $this->load->view(VIEW_STORE . $main_content); ?>
    <!-- END CONTENT -->

	<!-- Start Footer Area -->
	<footer class="footer">
		<div class="copyright">
			<div class="container">
				<div class="inner">
					<div class="row">
						<div class="col-12">
							<div class="left">
								<p>Copyright © 2021 <a href="<?php echo base_url() ?>"></a><?php echo COMPANY_NAME; ?>  -  All Rights Reserved.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!-- /End Footer Area -->

    <!-- BEGIN TEMPLATE FOOTER -->
    <script src="<?php echo ASSET_PATH; ?>store/js/jquery.min.js"></script>
    <script src="<?php echo ASSET_PATH; ?>store/js/jquery-migrate-3.0.0.js"></script>
    <script src="<?php echo ASSET_PATH; ?>store/js/jquery-ui.min.js"></script>
    <script src="<?php echo ASSET_PATH; ?>store/js/popper.min.js"></script>
    <script src="<?php echo ASSET_PATH; ?>store/js/bootstrap.min.js"></script>
    <script src="<?php echo ASSET_PATH; ?>store/js/slicknav.min.js"></script>
    <script src="<?php echo ASSET_PATH; ?>store/js/owl-carousel.js"></script>
    <script src="<?php echo ASSET_PATH; ?>store/js/magnific-popup.js"></script>
    <script src="<?php echo ASSET_PATH; ?>store/js/facnybox.min.js"></script>
    <script src="<?php echo ASSET_PATH; ?>store/js/waypoints.min.js"></script>
    <script src="<?php echo ASSET_PATH; ?>store/js/finalcountdown.min.js"></script>
    <script src="<?php echo ASSET_PATH; ?>store/js/nicesellect.js"></script>
    <script src="<?php echo ASSET_PATH; ?>store/js/scrollup.js"></script>
    <script src="<?php echo ASSET_PATH; ?>store/js/onepage-nav.min.js"></script>
    <script src="<?php echo ASSET_PATH; ?>store/js/easing.js"></script>
    <script src="<?php echo ASSET_PATH; ?>store/js/active.js"></script>

    <!-- Script -->
	<?php 
		if ( $carabiner = config_item('cfg_carabiner') ) {
	        // Theme App JS
	        $scripts[] = array(BE_PLUGIN_PATH .'bootstrap-notify/bootstrap-notify.min.js?ver='.JS_VER_MAIN);
	        $scripts[] = array(BE_PLUGIN_PATH .'bootbox/bootbox.min.js?ver='.JS_VER_MAIN);
	        $scripts[] = array(BE_JS_PATH .'pages/store.js?ver='.JS_VER_STORE);
	        $this->carabiner->group('app_js', array('js' => $scripts));
	        echo $this->carabiner->display('app_js');
	    } else {
	        echo $scripts; 
	        // Theme App JS
	        echo '<script src="'. BE_PLUGIN_PATH .'bootstrap-notify/bootstrap-notify.min.js"></script>';
	        echo '<script src="'. BE_PLUGIN_PATH .'bootbox/bootbox.min.js"></script>';
	        echo '<script src="'. BE_JS_PATH .'pages/store.js?ver='.JS_VER_STORE.'"></script>';
	    }
	?>

	<!-- Customs Script ========================================================== -->
	<script type="text/javascript">
	    $(document).ready(function() {
	        $.ajaxPrefilter(function (options, originalOptions, jqXHR) {
	            if (originalOptions.type === 'POST' || options.type === 'POST') {
	                if (options.processData && options.contentType === 'application/x-www-form-urlencoded; charset=UTF-8') {
	                    var token_params = { [StoreApp.kdName()] : StoreApp.kdToken() };
	                    options.data = (options.data ? options.data + '&' : '') + $.param(token_params);
	                }
	            }
	        });

	    $( document ).ajaxSuccess(function (event, request, settings) {
	        if ( request.responseText ) {
	            if ( request.responseText ) {
	                var response = $.parseJSON(request.responseText);
	                if( typeof response =='object' ) { if ( response.token ) { StoreApp.kdToken(response.token); } }
	            }
	        }
	    });
	    });
	</script>
    <!-- END TEMPLATE FOOTER -->
    <?php echo $scripts_init; ?>
</body>
<!-- END BODY -->

</html>