<?php
$sort_by = array(
	'dateupdate'	=> 'Terbaru',
	'name_asc'		=> 'Nama A-Z',
	'name_desc'		=> 'Nama Z-A',
	'price_asc'		=> 'Harga Terendah',
	'price_desc'	=> 'Harga Tertinggi',
);
?>

<!-- Breadcrumbs -->
<div class="breadcrumbs">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="bread-inner">
					<ul class="bread-list">
						<li><a href="<?php echo base_url('/'); ?>">Home<i class="ti-arrow-right"></i></a></li>
						<li><a href="<?php echo base_url('store'); ?>">Produk<i class="ti-arrow-right"></i></a></li>
						<li class="active"><a href="javascript:;">Checkout</a></li>
					</ul>
					<span style="float: right">
						<a href="<?php echo base_url('store/searchorder'); ?>" class="text-white">Telusuri Pesanan</a>
					</span>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- End Breadcrumbs -->

<section class="shop checkout section">
	<div class="container">
		<form class="form" id="form-store-checkout" method="post" action="<?= base_url('store/processcheckout') ?>" data-subtotal="<?= $total_payment; ?>">
			<div class="row">
				<div class="col-lg-8 col-12">
					<div class="checkout-form">
						<h2>Informasi Pengiriman</h2>
						<p>Silahkan isi formulir untuk checkout belanjaan anda</p>
						<div class="row">
							<div class="col-12">
								<div class="form-group">
									<label>Nama<span>*</span></label>
									<input type="text" name="name">
								</div>
							</div>
							<div class="col-sm-6 col-12">
								<div class="form-group">
									<label>No. Hp/WA<span>*</span></label>
									<input type="number" name="phone">
								</div>
							</div>
							<div class="col-sm-6 col-12">
								<div class="form-group">
									<label>Email<span>*</span></label>
									<input type="email" name="email">
								</div>
							</div>
							<div class="col-sm-6 col-12 mb-4">
								<div class="form-group select-option">
									<label>Provinsi<span>*</span></label>
									<select name="province" id="province" class="select_province" data-url="<?php echo base_url('address/selectprovince'); ?>">
										<option value="" disabled="" selected="selected">-- Silahkan Pilih Provinsi --</option>
										<?php
										if ($provinces = bp_provinces()) {
											foreach ($provinces as $key => $row) {
												echo '<option value="' . $row->id . '">' . $row->province_name . '</option>';
											}
										}
										?>
									</select>
								</div>
							</div>
							<div class="col-sm-6 col-12 mb-4">
								<div class="form-group select-option ">
									<label>Kota/Kabupaten<span>*</span></label>
									<select name="district" id="district" class="select_district" data-url="<?php echo base_url('address/selectdistrict'); ?>">
										<option value=""></option>
									</select>
								</div>
							</div>
							<div class="col-sm-6 col-12 mb-4">
								<div class="form-group select-option">
									<label>Kecamatan<span>*</span></label>
									<select name="subdistrict" id="subdistrict" class="select_subdistrict">
										<option value=""></option>
									</select>
								</div>
							</div>
							<div class="col-sm-6 col-12">
								<div class="form-group">
									<label>Kelurahan/Desa<span>*</span></label>
									<input type="text" name="village" id="village">
								</div>
							</div>
							<div class="col-12">
								<div class="form-group">
									<label>Alamat Lengkap<span>*</span></label>
									<input type="text" name="address">
								</div>
							</div>
							<div class="col-sm-6 col-12">
								<div class="form-group select-option">
									<label>Kurir<span>*</span></label>
									<select name="courier" id="courier" class="select_courier" data-url="<?php echo base_url('address/selectcourier'); ?>">
										<option value="" selected="">-- <?php echo lang('select') . ' ' . lang('courier'); ?> --</option>
										<?php
										if ($get_couriers = config_item('courier')) {
											foreach ($get_couriers as $key => $row) {
												echo '<option value="' . $row['code'] . '" >' . $row['name'] . '</option>';
											}
										}
										?>
									</select>
								</div>
							</div>
							<div class="col-sm-6 col-12">
								<div class="form-group select-option">
									<label>Layanan Kurir<span>*</span></label>
									<select name="service" id="service" class="select_service">
										<option value=""></option>
									</select>
									<input type="hidden" name="courier_cost" id="courier_cost" value="0" class="d-none">
								</div>
							</div>
							<div class="col-12 mt-4">
								<div class="form-group create-account">
									<input id="cbox" type="checkbox">
									<label>Create an account?</label>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-12">
					<div class="order-details">
						<div class="single-widget px-3">
							<div class="row">
								<div class="col-12 mb-2">
									Apakah Anda ingin menjadi Member ?
								</div>
								<div class="col-12">
									<div class="btn-group btn-group-sm btn-group-toggle" data-toggle="buttons">
										<label id="opt-member" class="btn btn-sm btn-success px-4 py-1">
											<input value="member" id="opt_reg_member" type="radio" name="options_reg" autocomplete="off"> Ya
										</label>
										<label id="opt-customer" class="btn btn-sm btn-success px-3 py-1 active">
											<input value="customer" id="opt_reg_customer" type="radio" name="options_reg" autocomplete="off" checked> Tidak
										</label>
									</div>
								</div>
							</div>
							<div class="row mt-4" id="options_member_reg" style="display: none">
								<div class="col-12">
									<div class="form-group mb-2">
										<label>Referral<span>*</span></label>
										<input type="text" name="sponsor" id="sponsor" class="mb-0" data-url="<?php echo base_url('store/checksponsor') ?>">
										<div id="sponsor_info"></div>
									</div>
									<hr class="mt-3 mb-4">
								</div>
								<div class="col-12">
									<div class="form-group mb-2">
										<label>Username<span>*</span></label>
										<input type="text" name="username" id="username" class="mb-0" data-url="<?php echo base_url('store/checkusername') ?>">
									</div>
								</div>
								<div class="col-12">
									<div class="form-group mb-2">
										<label>Password<span>*</span></label>
										<input type="password" name="password" id="password" class="mb-0">
										<button class="btn btn-password" type="button"><i class="fa fa-eye-slash"></i></button>
									</div>
								</div>
								<div class="col-12">
									<hr class="mt-3 mb-4">
									<div class="form-group mb-2">
										<label>No. KTP<span>*</span></label>
										<input type="number" name="idcard" id="idcard" class="mb-0">
									</div>
								</div>
							</div>
						</div>
						<hr class="mt-2 mb-3">
						<div class="single-widget mb-5">
							<h2 class="px-3">Ringkasan Order</h2>
							<div class="content">
								<?php if ($cart_product) : $num = 1;
									$total_bv = 0; ?>
									<table class="table mb-0">
										<?php foreach ($cart_product as $key => $row) : ?>
											<?php
											$cart_price     = bp_accounting($row['cart_price']);
											$cart_subtotal  = bp_accounting($row['cart_subtotal']);
											$cart_subtotal  = bp_accounting($row['cart_subtotal']);
											$total_bv 	   += $row['product_bv'];
											?>
											<tr>
												<td class="text-capitalize pr-1 pt-3 pb-2">
													<p class="small" style="white-space: normal;line-height: 10px">
														<?php echo $row['product_name']; ?>
														<small class="text-success">(<?php echo bp_accounting($row['product_bv']); ?> Bv)</small>
													</p>
													<small><?php echo bp_accounting($row['qty']) . ' <small>x</small> ' . $cart_price; ?></small>
												</td>
												<td class="text-right pl-1">
													<?php echo $cart_subtotal; ?>
												</td>
											</tr>
										<?php endforeach; ?>
									</table>
									<hr class="my-0">
								<?php endif; ?>
								<ul class="mt-3">
									<li class="px-3 mb-1">Subtotal<span class="checkout-subtotal"><?= bp_accounting($total_payment)  ?></span></li>
									<li class="px-3 mb-1">Ongkir<span class="checkout-shipping">0</span></li>
									<li class="px-3 mb-1">Diskon<span class="checkout-discount">0</span></li>
									<li class="last px-3 font-weight-bold">Total<span class="checkout-total-payment text-success"><?= bp_accounting($total_payment, $currency)  ?></span></li>
								</ul>
							</div>
						</div>
						<div class="single-widget get-button">
							<div class="content">
								<div class="button">
									<a href="#" class="btn btn-store-checkout">Proses checkout</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</section>

<div class="modal fade" id="modal-process-checkout" tabindex="-1" role="dialog" aria-labelledby="modal-process-checkout-save" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 500px; margin: 1% auto 5%">
		<div class="modal-content">
			<div class="modal-header px-4 py-2" style="position: relative;">
				<h5 class="modal-title" style="font-size: 1rem"><i class="fa fa-check"></i> Konfirmasi Checkout</h5>
				<button type="button" style="font-size: 20px" class="close pr-0" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body px-4 py-4 mb-3" style="height: auto;">
				<p style="color: #333">Apakah anda yakin akan checkout pesanan produk ini ?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default py-2" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-success py-2" id="btn-process-checkout" style="background-color: #0b7346;">Proses Checkout</button>
			</div>
		</div>
	</div>
</div>