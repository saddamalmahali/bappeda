<?php
$sort_by = array(
	'dateupdate'	=> 'Terbaru',
	'name_asc'		=> 'Nama A-Z',
	'name_desc'		=> 'Nama Z-A',
	'price_asc'		=> 'Harga Terendah',
	'price_desc'	=> 'Harga Tertinggi',
);
?>

<!-- Breadcrumbs -->
<div class="breadcrumbs">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="bread-inner">
					<ul class="bread-list">
						<li><a href="<?php echo base_url('/'); ?>">Home<i class="ti-arrow-right"></i></a></li>
						<li class="active"><a href="javascript:;">Produk</a></li>
					</ul>
					<span style="float: right">
						<a href="<?php echo base_url('store/searchorder'); ?>" class="text-white">Telusuri Pesanan</a>
					</span>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- End Breadcrumbs -->

<section class="product-area shop section pt-4 mb-5">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="row">
					<div class="col-12">
						<!-- Shop Top -->
						<div class="shop-top pt-2 pb-1">
							<div class="row">
								<div class="col-sm-6 col-12">
									<label class="product-found mt-1 mb-0">List Produk</label>
								</div>
								<div class="col-sm-6 col-12">
									<div class="shop-shorter">
										<div class="single-shorter">
											<label>Sort By :</label>
											<select class="search_sortby">
												<?php
												foreach ($sort_by as $key => $value) {
													$selected = (isset($s_sortby) && $s_sortby == $key) ? 'selected="selected"' : '';
													echo '<option value="' . $key . '" ' . $selected . '>' . $value . '</option>';
												}
												?>
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--/ End Shop Top -->
					</div>
				</div>
				<div class="row" id="product-shop-list" data-url="<?php echo base_url('store/productlistdata/' . bp_encrypt('get')) ?>"></div>
				<div class="row shopping-see-more mt-4 mb-4">
					<div class="col-12 text-center">
						<button type="button" class="btn btn-see-more">
							<span class="see-more-loading mr-2"><img src="<?php echo BE_IMG_PATH; ?>loading-spinner-blue.gif"></span>
							Lihat lebih banyak
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Modal -->
<div class="modal fade" id="modal-store-product-detail" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="ti-close" aria-hidden="true"></span></button>
			</div>
			<div class="modal-body" style="height: auto;">
				<div class="row no-gutters info-store-product-detail"></div>
			</div>
		</div>
	</div>
</div>
<!-- Modal end -->