<!-- Header -->
<header class="header shop kd-content" data-name="<?php echo $this->security->get_csrf_token_name(); ?>" data-token="<?php echo $this->security->get_csrf_hash(); ?>">
	<!-- Topbar -->
	<!-- <div class="topbar">
		<div class="container">
			<div class="row">
				<div class="col-6">
					<div class="top-left">
						<ul class="list-main">
							<li><i class="ti-home"></i> <a href="#">Home</a></li>
						</ul>
					</div>
				</div>
				<div class="col-6">
					<div class="right-content">
						<ul class="list-main">
							<li class="text-lowercase"><i class="ti-email"></i> <?php echo COMPANY_EMAIL; ?></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div> -->
	<!-- End Topbar -->
	<div class="middle-inner">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-3 col-12 pr-0">
					<div class="logo">
						<a href="<?php echo base_url(); ?>">
							<img src="<?php echo LOGO_IMG; ?>" alt="logo" style="height: 45px">
							<span class="text-logo"><?php echo COMPANY_NAME; ?></span>
						</a>
					</div>
					<div class="search-top">
						<div class="top-search"><a href="#0"><i class="ti-search"></i></a></div>
						<div class="search-top">
							<form class="search-form form-search-store-product">
								<input class="search_product search-product-top" type="text" placeholder="Cari Produk....." name="search" value="<?php echo isset($s_product) ? $s_product : ''; ?>" data-product="<?php echo isset($s_product) ? trim($s_product) : ''; ?>">
								<button value="search" type="button" class="btn-search-product"><i class="ti-search"></i></button>
							</form>
						</div>
					</div>

					<div class="mobile-nav">
						<div class="slicknav_menu">
							<a href="<?php echo base_url('store/cart'); ?>" class="slicknav_btn"><i class="ti-bag"></i></a>
						</div>
					</div>
				</div>
				<div class="col-lg-7 col-md-7 col-12 p-0">
					<div class="search-bar-top">
						<div class="search-bar">
							<select class="search_category" name="category">
								<option value="all">All Category</option>
								<?php
								if ($product_category = bp_product_category(0, true)) {
									foreach ($product_category as $key => $row) {
										$selected   = (isset($s_category) && $s_category == $row->slug) ? 'selected="selected"' : '';
										echo '<option value="' . $row->slug . '" ' . $selected . '>' . ucwords($row->name) . '</option>';
									}
								}
								?>
							</select>
							<form class="form-search-store-product" data-url="<?php echo base_url('store') ?>">
								<input class="search_product search-product-main" name="search" placeholder="Cari Produk....." type="search" value="<?php echo isset($s_product) ? $s_product : ''; ?>" data-product="<?php echo isset($s_product) ? trim($s_product) : ''; ?>">
								<button class="btnn btn-search-product"><i class="ti-search"></i></button>
							</form>
						</div>
					</div>
				</div>
				<div class="col-lg-2 col-md-2 col-12">
					<div class="right-bar">
						<div class="sinlge-bar">
							<a href="<?php echo base_url('login'); ?>" class="single-icon"><i class="fa fa-user-circle-o" aria-hidden="true"></i></a>
						</div>
						<div class="sinlge-bar shopping menu-cart">
							<a href="<?php echo base_url('store/cart'); ?>" class="single-icon">
								<i class="ti-bag"></i> <span class="total-count" id="cart-total-menu"><?php echo $this->cart->total_items(); ?></span>
							</a>
							<?php if ($cart_content && isset($cart_content['data'])) : ?>
								<div class="shopping-item">
									<div class="dropdown-cart-header">
										<span>Produk</span>
										<a href="#"><span id="cart-total-item"><?php echo count($cart_content['data']); ?></span> Items</a>
									</div>
									<ul class="shopping-list">
										<?php foreach ($cart_content['data'] as $key => $row) : ?>
											<li data-id="<?php echo $row['rowid']; ?>" class="pb-3 mb-3">
												<a class="cart-img" href="javascript:;"><img src="<?php echo $row['product_image']; ?>" alt="#"></a>
												<h4><a href="javascript:;"><?php echo $row['product_name']; ?></a></h4>
												<p><span class="amount"><?php echo bp_accounting($row['cart_price']); ?></span></p>
												<p class="quantity pb-2">Qty : <span id="qty_<?php echo $row['rowid']; ?>"><?php echo $row['qty']; ?></span></p>
											</li>
										<?php endforeach; ?>
									</ul>
									<div class="bottom">
										<div class="total">
											<span>Total</span>
											<span class="total-amount cart-total-paymnet"><?php echo bp_accounting($this->cart->total()); ?></span>
										</div>
										<a href="<?php echo base_url('store/cart'); ?>" class="btn animate">Lihat Keranjang</a>
									</div>
								</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
<!--/ End Header -->