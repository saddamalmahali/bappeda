<?php
$the_member         = (!empty($member_other) && $member_other->type == MEMBER) ? $member_other : $member;
$as_admin           = as_administrator($the_member);
$as_member          = as_member($the_member);
$as_staff           = ($staff = bp_get_current_staff()) ? true : false;
$as_mine            = ($the_member->id == $member->id) ? true : false;

$p_member_name      = $the_member->name;
$p_member_user      = $the_member->username;
$p_status           = 'Member';
$p_class            = 'default';
if ($as_admin) {
    $p_status       = 'Admin';
    $p_class        = 'danger';
}
if ($the_member->type == 1 && $as_staff) {
    $p_member_name  = $staff->name;
    $p_member_user  = $staff->username;
    if ($staff->id > 1) {
        $p_status   = 'Staff';
        $p_class    = 'success';
    }
}



$folder_path = BE_IMG_PATH . 'icons/avatar.png';
$avatar = '';

if ($the_member->photo && (file_exists(PROFILE_IMG_PATH . $the_member->photo))) {
    $avatar = PROFILE_IMG . $the_member->photo;
} else {
    $avatar = $folder_path;
}



// $avatar = (empty($the_member->photo) ? 'avatar.png' : $the_member->photo);



$data_profile = array(
    'member_other'  => $member_other,
    'the_member'    => $the_member,
    'staff'         => $as_staff ? bp_get_current_staff() : false,
    'as_admin'      => $as_admin,
    'as_member'     => $as_member,
    'as_staff'      => $as_staff,
    'as_mine'       => $as_mine,
);
?>

<div class="header d-flex align-items-center" style="min-height: 350px; background-image: url(<?php echo BE_IMG_PATH . 'bg-profile.jpg'; ?>); background-size: cover; background-position: center top;">
    <!-- Mask -->
    <span class="mask bg-gradient-default opacity-8"></span>
    <!-- Header container -->
    <div class="container-fluid mt--9">
        <div class="row">
            <div class="col-lg-7 col-md-10">
                <h1 class="display-4 text-white mb-0"><?php echo ucwords(strtolower($p_member_name)); ?></h1>
                <p class="text-white mt-0 mb-2">This is your profile page.</p>
            </div>
        </div>
    </div>
</div>

<!-- Page content -->
<div class="container-fluid mt--9">
    <div class="row">
        <div class="col-lg-8">
            <div class="card">
                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col-12">
                            <h3 class="mb-0">Edit profile </h3>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <?php $this->load->view(VIEW_BACK . 'member/profiledetail/personal_info', $data_profile); ?>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card card-profile d-none d-lg-inline-block">
                <img src="<?php echo BE_IMG_PATH . 'bg-profile.jpg'; ?>" alt="Image placeholder" class="card-img-top">
                <div class="row justify-content-center">
                    <div class="col-lg-3 order-lg-2">
                        <div class="card-profile-image">
                            <a href="#">
                                <img src="<?php echo $avatar; ?>" class="rounded-circle profile-photo" style="width: 100px; height:100px;" id="profile-photo">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
                    <div class="d-flex justify-content-between">
                        <a href="#" class="btn btn-sm btn-info  mr-4 profile-photo">Photo</a>
                        <a href="#" class="btn btn-sm btn-<?php echo $p_class; ?> float-right"><?php echo $p_status; ?></a>
                    </div>
                </div>
                <div class="card-body pt-0">
                    <div class="text-center">
                        <h5 class="h3 mb-0"><?php echo ucwords(strtolower($p_member_name)); ?></h5>
                        <h5 class="h4 text-info"><?php echo $p_member_user; ?></h5>
                    </div>
                </div>
                <div class="card-footer text-center btn-profile-photo" style="display: none;">
                    <input type="file" accept="image/x-png,image/jpeg" name="profile_img" id="profile_img" class="d-none">
                    <button class="btn btn-primary" id="do_save_profile_photo" data-url="<?php echo base_url('member/changeprofilephoto/' . bp_encrypt($the_member->id)); ?>">Simpan Foto Profile</button>
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <h5 class="h3 mb-0">Ganti Password</h5>
                </div>
                <div class="card-body">
                    <div class="row justify-content-center">
                        <div class="col-lg-12">
                            <?php 
                                $url_password = ( $as_staff ) ? 'member/changepasswordstaff' : 'member/changepassword';
                            ?>
                            <?php echo form_open($url_password, array('id' => 'cpassword', 'role' => 'form')); ?>
                            <div class="form-group">
                                <label class="control-label">Password Lama</label>
                                <div class="input-group">
                                    <input type="password" class="form-control" name="cur_pass" id="cur_pass" />
                                    <span class="input-group-btn">
                                        <button class="btn btn-flat btn-white pass-show-hide" type="button"><i class="fa fa-eye-slash"></i></button>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Password Baru</label>
                                <div class="input-group">
                                    <input type="password" class="form-control" name="new_pass" id="new_pass" />
                                    <span class="input-group-btn">
                                        <button class="btn btn-flat btn-white pass-show-hide" type="button"><i class="fa fa-eye-slash"></i></button>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Konfirmasi Password Baru</label>
                                <div class="input-group">
                                    <input type="password" class="form-control" name="cnew_pass" id="cnew_pass" />
                                    <span class="input-group-btn">
                                        <button class="btn btn-flat btn-white pass-show-hide" type="button"><i class="fa fa-eye-slash"></i></button>
                                    </span>
                                </div>
                            </div>
                            <hr class="my-4" />
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary bg-gradient-default my-2">Ganti Password</button>
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="save_cpassword" tabindex="-1" role="basic" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title"><i class="fa fa-lock"></i> Ganti Password</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            Apakah Anda yakin akan mengubah password ?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-warning" data-dismiss="modal">Batal</button>
                            <button type="button" class="btn btn-default" id="do_save_cpassword" data-form="cpassword">Lanjut</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>