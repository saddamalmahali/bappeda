<!-- Personal Info -->
<?php if ($as_staff) : ?>

    <?php echo form_open('member/staffinfo', array('id' => 'personal', 'class' => 'form-horizontal mb-4', 'role' => 'form')); ?>
    <h6 class="heading-small text-muted mb-4">Informasi Akun</h6>
    <div class="pl-lg-4">
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <label class="form-control-label" for="member_username"><?php echo lang('username'); ?></label>
                    <input type="text" name="member_username" id="member_username" class="form-control text-lowercase" placeholder="Username" value="<?php echo $staff->username; ?>" disabled="disabled">
                </div>
                <div class="form-group">
                    <label class="form-control-label" for="member_name"><?php echo lang('name'); ?></label>
                    <input type="text" name="member_name" id="member_name" class="form-control text-uppercase" value="<?php echo $staff->name; ?>">
                </div>
                <div class="form-group">
                    <label class="form-control-label" for="member_email"><?php echo lang('reg_email'); ?></label>
                    <input type="email" name="member_email" id="member_email" class="form-control" value="<?php echo $staff->email; ?>">
                </div>
                <div class="form-group">
                    <label class="form-control-label" for="member_phone"><?php echo lang('reg_no_telp'); ?></label>
                    <div class="input-group input-group-merge">
                        <div class="input-group-prepend">
                            <span class="input-group-text">+62</span>
                        </div>
                        <input type="text" name="member_phone" id="member_phone" class="form-control numbermask phonenumber" value="<?php echo $staff->phone; ?>">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr class="my-4" />
    <div class="text-center">
        <button type="submit" class="btn btn-primary bg-gradient-default my-2"><?php echo lang('save'); ?> Profile</button>
    </div>
    <?php echo form_close(); ?>

<?php else : ?>

    <?php echo form_open('member/admininfo', array('id' => 'personal', 'class' => 'form-horizontal mb-4', 'role' => 'form')); ?>
    <h6 class="heading-small text-muted mb-4">Informasi Akun</h6>
    <div class="pl-lg-4">
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <label class="form-control-label" for="member_username"><?php echo lang('username'); ?></label>
                    <input type="text" name="member_username" id="member_username" class="form-control text-lowercase" placeholder="Username" value="<?php echo $member->username; ?>" disabled="disabled">
                </div>
                <div class="form-group">
                    <label class="form-control-label" for="member_name"><?php echo lang('name'); ?></label>
                    <input type="text" name="member_name" id="member_name" class="form-control text-uppercase" value="<?php echo $member->name; ?>">
                </div>
                <div class="form-group">
                    <label class="form-control-label" for="member_email"><?php echo lang('reg_email'); ?></label>
                    <input type="email" name="member_email" id="member_email" class="form-control" value="<?php echo $member->email; ?>">
                </div>
                <div class="form-group">
                    <label class="form-control-label" for="member_phone"><?php echo lang('reg_no_telp'); ?></label>
                    <div class="input-group input-group-merge">
                        <div class="input-group-prepend">
                            <span class="input-group-text">+62</span>
                        </div>
                        <input type="text" name="member_phone" id="member_phone" class="form-control numbermask phonenumber" value="<?php echo $member->phone; ?>">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr class="my-4" />
    <div class="text-center">
        <button type="submit" class="btn btn-primary bg-gradient-default my-2"><?php echo lang('save'); ?> Profile</button>
    </div>
    <?php echo form_close(); ?>

<?php endif ?>

<!-- BEGIN CONFIRMATION MODAL -->
<div class="modal fade" id="save_profile" tabindex="-1" role="dialog" aria-labelledby="modalsave_profile" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><i class="fa fa-edit"></i> Profil</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Apakah profil <?php echo (!empty($member_other) && $member_other->type == MEMBER ? 'anggota <strong>' . $member_other->username . '</strong>' : 'Anda'); ?> sudah benar ?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" id="do_save_profile">Lanjut</button>
            </div>
        </div>
    </div>
</div>
<!-- END CONFIRMATION MODAL -->