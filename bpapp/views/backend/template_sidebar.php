<nav class="sidenav navbar navbar-vertical  fixed-left navbar-expand-xs bg-sidebar" id="sidenav-main">
    <div class="scrollbar-inner">
        <!-- Brand -->
        <div class="sidenav-header d-flex align-items-center">
            <a class="navbar-brand" href="<?php echo base_url('dashboard') ?>">
                <!-- <span class=" font-weight-bold"><small><?php echo COMPANY_NAME; ?></small></span> -->
                <img src="<?php echo BE_IMG_PATH; ?>logo.png" class="navbar-brand-img" alt="<?php echo COMPANY_NAME; ?>">
            </a>
            <div class=" ml-auto ">
                <!-- Sidenav toggler -->
                <div class="sidenav-toggler sidenav-toggler-dark d-none d-xl-block" data-action="sidenav-unpin" data-target="#sidenav-main">
                    <div class="sidenav-toggler-inner">
                        <i class="sidenav-toggler-line"></i>
                        <i class="sidenav-toggler-line"></i>
                        <i class="sidenav-toggler-line"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="navbar-inner">
            <!-- Collapse -->
            <div class="collapse navbar-collapse" id="sidenav-collapse-main">
                <!-- Nav items -->
                <?php
                include APPPATH . 'views/backend/menu.php';
                if ($sidebar) :
                    echo '<ul class="navbar-nav"> ';
                    
                    $user_access    = '';
                    $user_roles     = array();
                    $rkpd_plan      = array('initialplan', 'plan', 'finalplan');
                    $kuappas_plan   = array('makingplan', 'consummationplan');

                    if ( $as_admin = as_admin($member) ) {
                        $user_access    = $member->access;
                        $user_roles     = $member->roles ? $member->roles : $user_roles;
                    }

                    if ($staff = bp_get_current_staff()) {
                        $user_access    = $staff->access;
                        $user_roles     = $staff->role ? $staff->role : $user_roles;
                    }

                    $user_roles         = maybe_unserialize($user_roles);
                    foreach ($sidebar as $nav) :
                        $nav_access     = isset($nav['access']) ? $nav['access'] : '';
                        $nav_access     = maybe_unserialize($nav_access);
                        if ( is_array($nav_access) ) {
                            if ( !in_array($member->type, $nav_access) ) {
                                continue;
                            }
                        }

                        if ( $member->select_stages == 'kua_ppas') {
                            if ( in_array($nav['nav'], $rkpd_plan) ) {
                                continue;
                            }
                        } else {
                            if ( in_array($nav['nav'], $kuappas_plan) ) {
                                continue;
                            }
                        }

                        if ( $user_access ) {
                            $nav_roles  = isset($nav['roles']) ? $nav['roles'] : '';
                            $nav_roles  = maybe_unserialize($nav_roles);

                            if ($user_access == 'partial' && is_array($nav_roles) ) {
                                $menu_roles = array_intersect($nav_roles, $user_roles);
                                if ( !$menu_roles ) {
                                    continue;
                                }
                            }
                            if ($user_access == 'all' && is_array($nav_roles)) {
                                $menu_roles = array_intersect($nav_roles, array(STAFF_ACCESS14, STAFF_ACCESS15));
                                if ( $menu_roles ) {
                                    if ($user_roles) {
                                        $nav_role_admin = array_intersect($nav_roles, $user_roles);
                                        if (!$nav_role_admin) {
                                            continue;
                                        }
                                    } else {
                                        continue;
                                    }
                                }
                            }
                        }

                        if ($nav['sub']) {
                            echo '<li class="nav-item">
                                    <a class="nav-link ' . ($active_page == $nav['nav'] ? 'text-gray-dark active' : 'text-sidebar') . '" href="#navbar-' . $nav['nav'] . '" data-toggle="collapse" role="button" aria-expanded="' . ($active_page == $nav['nav'] ? 'true' : 'false') . '" aria-controls="navbar-' . $nav['nav'] . '">
                                        <i class="' . $nav['icon'] . ' ' . ($active_page == $nav['nav'] ? 'text-gray-dark' : '') . '"></i>
                                        <span class="nav-link-text">' . $nav['title'] . '</span>
                                    </a>
                                    <div class="collapse ' . ($active_page == $nav['nav'] ? 'show' : '') . '" id="navbar-' . $nav['nav'] . '">
                                        <ul class="nav nav-sm flex-column">';
                            foreach ($nav['sub'] as $sub) :
                                if (!$sub) {
                                    continue;
                                }

                                if ( $user_access ) {
                                    $subnav_roles   = isset($sub['roles']) ? $sub['roles'] : '';
                                    $subnav_roles   = maybe_unserialize($subnav_roles);

                                    if ( $user_access == 'partial' && is_array($subnav_roles) ) {
                                        $menusub_role = array_intersect($subnav_roles, $user_roles);
                                        if ( !$menusub_role ) {
                                            continue;
                                        }
                                    }
                                    if ( $user_access == 'all' && is_array($subnav_roles) ) {
                                        $menusub_role = array_intersect($subnav_roles, array(STAFF_ACCESS14, STAFF_ACCESS15));
                                        if ( $menusub_role ) {
                                            if ($user_roles) {
                                                $subnav_role_admin = array_intersect($subnav_roles, $user_roles);
                                                if (!$subnav_role_admin) {
                                                    continue;
                                                }
                                            } else {
                                                continue;
                                            }
                                        }
                                    }
                                }

                                $sub_newtab = ((isset($sub['newtab']) && $sub['newtab']) ? 'target="_blank"' : '');
                                $sub_icon   = ((isset($sub['icon']) && !empty($sub['icon'])) ? '<i class="' . $sub['icon'] . '"></i>' : '');
                                $sub_icon   = (empty($sub_icon)) ? strtoupper(substr($sub['title'], 0, 1)) : $sub_icon;
                                echo '<li class="nav-item">
                                                    <a href="' . $sub['link'] . '" ' . $sub_newtab . ' class="nav-link  ' . ($active_sub == $sub['nav'] ? 'text-sidebar active' : 'text-sidebar') . '">
                                                        <span class="sidenav-mini-icon"> ' . $sub_icon . ' </span>
                                                        <span class="sidenav-normal">' . $sub['title'] . '</span>
                                                    </a>
                                                </li>';
                            endforeach;
                            echo '</ul>
                                    </div>';
                        } else {
                            $nav_newtab = ((isset($nav['newtab']) && $nav['newtab']) ? 'target="_blank"' : '');
                            echo '<li class="nav-item">
                                    <a class="nav-link ' . ($active_page == $nav['nav'] ? 'text-gray-dark active' : 'text-sidebar') . '" href="' . $nav['link'] . '" ' . $nav_newtab . '>
                                        <i class="ni ' . $nav['icon'] . ' ' . ($active_page == $nav['nav'] ? 'text-gray-dark' : '') . '"></i>
                                        <span class="nav-link-text">' . $nav['title'] . '</span>
                                    </a>
                                </li>';
                        }
                    endforeach;
                    echo "</ul>";
                endif;
                ?>

                <!-- Divider -->
                <hr class="my-3">
                <!-- Heading -->
                <!-- <h6 class="navbar-heading p-0 text-muted">
                    <span class="docs-normal">Documentation</span>
                </h6> -->
                <!-- Navigation -->
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url('logout') ?>">
                            <i class="ni ni-button-power text-warning"></i>
                            <span class="nav-link-text text-sidebar">Logout</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>