<?php 
    $sel_sub_stages = array();
    $cfg_stages     = config_item('stages');
    $cfg_sub_stages = config_item('sub_stages');
    if ( $cfg_sub_stages ) {
        foreach ($cfg_sub_stages as $key => $value) {
            $sub_stages = $value;
            if ( !$sub_stages || !is_array($sub_stages) ) {
                continue;
            } 
            foreach ($sub_stages as $skey => $subval) {
                if ( isset($sel_sub_stages[$skey]) ) {
                    continue;
                }
                $name_subval    = isset($subval['menu']) ? $subval['menu'] : '';
                $sel_sub_stages[$skey] = lang($name_subval);
            }
        }
    }

?>

<div class="header bg-sidebar pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item"><a href="<?php echo base_url('dashboard') ?>"><i class="fas fa-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="#"><?php echo lang('menu_setting') ?></a></li>
                            <li class="breadcrumb-item active" aria-current="page"><?php echo $title_page; ?></li>
                        </ol>
                    </nav>
                </div>
                <div class="col-lg-6 col-5 text-right">
                    <a href="<?php echo base_url('setting/stages/create'); ?>" class="btn btn-sm btn-neutral">
                        <i class="fa fa-plus mr-1"></i> <?php echo lang('add') .' '. $title_page; ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid mt--6">
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0"><?php echo $title_page; ?> </h3>
                        </div>
                    </div>
                </div>
                <div class="table-container">
                    <table class="table align-items-center table-flush" id="list_table_setting_stages" data-url="<?php echo base_url('setting/stageslistdata'); ?>">
                        <thead class="thead-light">
                            <tr role="row" class="heading">
                                <th class="width5 text-center">No</th>
                                <th class="width25 text-center"><?php echo lang('year') ?></th>
                                <th class="width25 text-center"><?php echo lang('step') ?></th>
                                <th class="width25 text-center"><?php echo lang('sub_step') ?></th>
                                <th class="width10 text-center"><?php echo lang('start_date') ?></th>
                                <th class="width10 text-center"><?php echo lang('end_date') ?></th>
                                <th class="width10 text-center"><?php echo lang('status') ?></th>
                                <th class="width10 text-center"><?php echo lang('actions') ?></th>
                            </tr>
                            <tr role="row" class="filter">
                                <td></td>
                                <td class="px-1"><input type="text" class="form-control form-control-sm form-filter numbermask phonenumber" maxlength="4" name="search_year" /></td>
                                <td class="px-1">
                                    <select name="search_stage" class="form-control form-control-sm form-filter" >
                                        <option value=""><?php echo lang('select'); ?></option>
                                        <?php
                                            if ( $cfg_stages ) {
                                                foreach ($cfg_stages as $key => $value) {
                                                    echo '<option value="' . $key . '">' . lang($value) . '</option>';
                                                }
                                            }
                                        ?>
                                    </select>
                                </td>
                                <td class="px-1">
                                    <select name="search_sub_stage" class="form-control form-control-sm form-filter" >
                                        <option value=""><?php echo lang('select'); ?></option>
                                        <?php
                                            if ( $sel_sub_stages ) {
                                                foreach ($sel_sub_stages as $key => $value) {
                                                    echo '<option value="' . $key . '">' . $value . '</option>';
                                                }
                                            }
                                        ?>
                                    </select>
                                </td>
                                <td>
                                    <div class="input-group input-group-sm date date-picker mb-1" data-date-format="yyyy-mm-dd">
                                        <input type="text" class="form-control form-control-sm form-filter" readonly name="search_startdate_min" placeholder="From" />
                                        <span class="input-group-btn">
                                            <button class="btn btn-sm btn-white btn-flat" type="button"><i class="ni ni-calendar-grid-58 text-primary"></i></button>
                                        </span>
                                    </div>
                                    <div class="input-group input-group-sm date date-picker" data-date-format="yyyy-mm-dd">
                                        <input type="text" class="form-control form-control-sm form-filter" readonly name="search_startdate_max" placeholder="To" />
                                        <span class="input-group-btn">
                                            <button class="btn btn-sm btn-white btn-flat" type="button"><i class="ni ni-calendar-grid-58 text-primary"></i></button>
                                        </span>
                                    </div>
                                </td>
                                <td>
                                    <div class="input-group input-group-sm date date-picker mb-1" data-date-format="yyyy-mm-dd">
                                        <input type="text" class="form-control form-control-sm form-filter" readonly name="search_enddate_min" placeholder="From" />
                                        <span class="input-group-btn">
                                            <button class="btn btn-sm btn-white btn-flat" type="button"><i class="ni ni-calendar-grid-58 text-primary"></i></button>
                                        </span>
                                    </div>
                                    <div class="input-group input-group-sm date date-picker" data-date-format="yyyy-mm-dd">
                                        <input type="text" class="form-control form-control-sm form-filter" readonly name="search_enddate_max" placeholder="To" />
                                        <span class="input-group-btn">
                                            <button class="btn btn-sm btn-white btn-flat" type="button"><i class="ni ni-calendar-grid-58 text-primary"></i></button>
                                        </span>
                                    </div>
                                </td>
                                <td class="px-1">
                                    <select name="search_status" class="form-control form-control-sm form-filter">
                                        <option value=""><?php echo lang('select'); ?>...</option>
                                        <option value="active">AKTIIF</option>
                                        <option value="notactive">TIDAK AKTIF</option>
                                        <option value="done">SELESAI</option>
                                    </select>
                                </td>
                                <td class="text-center">
                                    <button class="btn btn-sm btn-outline-default filter-submit" id="btn_list_table_setting_stages"><i class="fa fa-search"></i></button>
                                    <button class="btn btn-sm btn-outline-warning filter-cancel"><i class="fa fa-times"></i></button>
                                </td>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Status Stage -->
<div class="modal fade" id="modal-status-stages" tabindex="-1" role="dialog" aria-labelledby="modal-status-stages" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header pt-3 pb-1">
                <h5 class="modal-title text-default"><i class="ni ni-book-bookmark mr-1"></i> <span class="modal-title-text font-weight-bold"> Edit Status</span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php echo form_open('setting/stagestatus', array('id' => 'form-status-stages', 'role' => 'form', 'class' => 'form-horizontal')); ?>
            <div class="modal-body wrapper-modal px-4 py-3" style="background-color: #f8f9fe">
                <div class="form-group row mb-2">
                    <label class="col-md-3 col-form-label form-control-label"><?php echo lang('year'); ?></label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" id="modal_year" disabled="" />
                    </div>
                </div>
                <div class="form-group row mb-2">
                    <label class="col-md-3 col-form-label form-control-label"><?php echo lang('step'); ?></label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" id="modal_stage" disabled="" />
                    </div>
                </div>
                <div class="form-group row mb-2">
                    <label class="col-md-3 col-form-label form-control-label"><?php echo lang('sub_step'); ?></label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" id="modal_substage" disabled="" />
                    </div>
                </div>
                <div class="form-group row mb-2">
                    <label class="col-md-3 col-form-label form-control-label"><?php echo lang('status'); ?> <span class="required">*</span></label>
                    <div class="col-md-9">
                        <select name="status" id="modal_status" class="form-control">
                            <option value="active">AKTIIF</option>
                            <option value="notactive">TIDAK AKTIF</option>
                            <option value="done">SELESAI</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-warning" data-dismiss="modal"><?php echo lang('back'); ?></button>
                <button type="submit" class="btn btn-primary" id="btn-submit-status-stages"><?php echo lang('edit'); ?></button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>