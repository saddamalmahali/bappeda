<div class="header bg-sidebar pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item"><a href="<?php echo base_url('dashboard') ?>"><i class="fas fa-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="#"><?php echo lang('menu_setting') ?></a></li>
                            <li class="breadcrumb-item active" aria-current="page"><?php echo lang('menu_setting_general'); ?></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid mt--6">
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body wrapper-setting-general">
                    <div class="accordion" id="accordionGeneralSetting">
                        <div class="card mb-2">
                            <div class="card-header py-3 bg-sidebar" id="headCompanyInfo" data-toggle="collapse" data-target="#companyInfo" aria-expanded="false" aria-controls="companyInfo">
                                <h5 class="text-white mb-0">Informasi Perusahaan</h5>
                            </div>
                            <div id="companyInfo" class="collapse show" aria-labelledby="headCompanyInfo" data-parent="#accordionGeneralSetting">
                                <?php $this->load->view(VIEW_BACK . 'setting/formgeneral/company'); ?>
                            </div>
                        </div>
                        <div class="card mb-2">
                            <div class="card-header py-3 bg-sidebar" id="headWaNotif" data-toggle="collapse" data-target="#waNotif" aria-expanded="false" aria-controls="waNotif">
                                <h5 class="text-white mb-0">WA-Notif</h5>
                            </div>
                            <div id="waNotif" class="collapse" aria-labelledby="headWaNotif" data-parent="#accordionGeneralSetting">
                                <?php $this->load->view(VIEW_BACK . 'setting/formgeneral/wanotif'); ?>
                            </div>
                        </div>
                        <div class="card mb-2">
                            <div class="card-header py-3 bg-sidebar" id="headImportCode" data-toggle="collapse" data-target="#importCode" aria-expanded="false" aria-controls="importCode">
                                <h5 class="text-white mb-0">Kode Import</h5>
                            </div>
                            <div id="importCode" class="collapse" aria-labelledby="headImportCode" data-parent="#accordionGeneralSetting">
                                <?php $this->load->view(VIEW_BACK . 'setting/formgeneral/importcode'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>