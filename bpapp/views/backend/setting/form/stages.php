<div class="header bg-sidebar pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item"><a href="<?php echo base_url('dashboard') ?>"><i class="fas fa-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="#"><?php echo lang('menu_setting') ?></a></li>
                            <li class="breadcrumb-item"><a href="<?php echo base_url('setting/stages') ?>"><?php echo $title_page ?></a></li>
                            <li class="breadcrumb-item active" aria-current="page">
                                <?php 
                                    $box_title = lang(strtolower($form)) . ' ' . $title_page;
                                    echo $box_title;
                                ?>
                            </li>
                        </ol>
                    </nav>
                </div>
                <div class="col-lg-6 col-5 text-right">
                    <a href="<?php echo base_url('setting/stages'); ?>" class="btn btn-sm btn-neutral">
                        <i class="fa fa-step-backward mr-1"></i> <?php echo lang('back'); ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid mt--6">
    <div class="row justify-content-center">
        <div class="col-lg-8">
            <div class="card">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0"><?php echo $box_title; ?></h3>
                        </div>
                    </div>
                </div>
                <div class="card-body wrapper-form-setting-stages pt-0">
                    <?php 
                        $cfg_stages     = config_item('stages');
                        $cfg_sub_stages = config_item('sub_stages');
                        $action         = base_url('setting/savestages');
                        if ( $dataform ) {
                            if ( isset($dataform->id) && $dataform->id ) {
                                $action .= '/'. bp_encrypt($dataform->id);
                            }
                        }
                    ?>
                    <form action="<?php echo $action; ?>" method="post" class="form-horizontal" id="form-setting-stages" >
                        <div class="form-group row mb-2">
                            <label class="col-md-3 col-form-label form-control-label"><?php echo lang('year'); ?> <span class="required">*</span></label>
                            <div class="col-md-9">
                                <input type="text" name="year" id="year" class="form-control numbermask phonenumber" value="<?php echo ($dataform ? $dataform->year : date('Y') ) ?>" autocomplete="off" maxlength="4" />
                            </div>
                        </div>
                        <div class="form-group row mb-2">
                            <label class="col-md-3 col-form-label form-control-label"><?php echo lang('step'); ?> <span class="required">*</span></label>
                            <div class="col-md-9">
                                <select name="stages" id="stages" class="form-control select_stage" data-url="api/setting/substages" >
                                    <option value=""><?php echo lang('select') .' '. lang('step'); ?></option>
                                    <?php
                                        if ( $cfg_stages ) {
                                            foreach ($cfg_stages as $key => $value) {
                                                $selected   = '';
                                                if ( isset($dataform->stage)  ) {
                                                    $selected   = ($dataform->stage == $key) ? 'selected=""' : '';
                                                }
                                                echo '<option value="' . $key . '" '. $selected .'>' . lang($value) . '</option>';
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row mb-2">
                            <label class="col-md-3 col-form-label form-control-label"><?php echo lang('sub_step'); ?> <span class="required">*</span></label>
                            <div class="col-md-9">
                                <select name="sub_stages" id="sub_stages" class="form-control select_substage">
                                    <option value=""><?php echo lang('select') .' '. lang('sub_step'); ?></option>
                                    <?php
                                        if ( $cfg_sub_stages ) {
                                            foreach ($cfg_sub_stages as $key => $value) {
                                                if ( isset($dataform->stage)  ) {
                                                    if ( $dataform->stage == $key ) {
                                                        $sub_stages = $value;
                                                        if ( !$sub_stages || !is_array($sub_stages) ) {
                                                            continue;
                                                        } 
                                                        foreach ($sub_stages as $skey => $subval) {
                                                            $selected       = '';
                                                            if ( isset($dataform->sub_stage)  ) {
                                                                $selected   = ($dataform->sub_stage == $skey) ? 'selected=""' : '';
                                                            }
                                                            $name_subval    = isset($subval['menu']) ? $subval['menu'] : '';
                                                            echo '<option value="' . $skey . '" '. $selected .'>' . lang($name_subval) . '</option>';

                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row mb-3" id="period_stages">
                            <label class="col-md-3 col-form-label form-control-label"><?php echo lang('period'); ?> <span class="required">*</span></label>
                            <div class="col-md-7">
                                <?php 
                                    $start_date     = date('Y-m-d');
                                    $end_date       = date('Y-m-t');
                                    if ( $dataform ) {
                                        $start_date = ($dataform->start_date != '0000-00-00 00:00:00') ? date('Y-m-d', strtotime($dataform->start_date)) : $start_date;
                                        $end_date   = ($dataform->end_date != '0000-00-00 00:00:00') ? date('Y-m-d', strtotime($dataform->end_date)) : $end_date;
                                    }
                                ?>
                                <div class="row input-daterange datepicker align-items-center">
                                    <div class="col">
                                        <div class="input-group">
                                            <input type="text" class="form-control text-center" readonly name="start_date" data-date-format="yyyy-mm-dd" placeholder="From" value="<?php echo $start_date; ?>" />
                                            <span class="input-group-btn">
                                                <button class="btn btn-neutral" type="button"><i class="ni ni-calendar-grid-58 text-primary"></i></button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-1 text-center">
                                        <label>s/d</label>
                                    </div>
                                    <div class="col">
                                        <div class="input-group">
                                            <input type="text" class="form-control text-center" readonly name="end_date" data-date-format="yyyy-mm-dd" placeholder="To" value="<?php echo $end_date; ?>" />
                                            <span class="input-group-btn">
                                                <button class="btn btn-neutral" type="button"><i class="ni ni-calendar-grid-58 text-primary"></i></button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr class="my-3">
                        <div class="row justify-content-center">
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-primary bg-sidebar"><?php echo lang('save') .' '. $title_page; ?></button> 
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>