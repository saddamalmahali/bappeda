<?php
$import_code     = get_option('import_code');
$import_code     = $import_code ? $import_code : 0;
?>


<form role="form" class="form-horizontal">
    <div class="card-body wrapper-setting-importcode">
        <div class="form-group row justify-content-center mb-2">
            <label class="col-md-3 col-form-label form-control-label">Kode Import <span class="required"></span></label>
            <div class="col-md-9">
                <div class="input-group">
                    <input type="text" class="form-control import_code" name="field[import_code]" readonly id="field_import_code" placeholder="Token / API Key" value="<?php echo get_option('import_code'); ?>" />
                    <span class="input-group-addon input-group-append">
                        <button class="btn btn-outline-primary" data-toggle="tooltip" data-placement="top" title="Salin Kode Import ke Clipboard" type="button" id="btn-copy-import-code"> <span class="fa fa-copy"></span></button>
                    </span>
                </div>

            </div>
        </div>
    </div>
    <div class="card-footer my-0">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <button type="button" class="btn btn-primary general-setting-each" data-type="import_code" data-id="be_dashboard_member" data-wraper="text_dashboard_wraper" data-url="<?php echo base_url('setting/updateallsetting'); ?>">
                    <?php echo lang('save') . ' ' . lang('menu_setting'); ?>
                </button>
            </div>
        </div>
    </div>
</form>