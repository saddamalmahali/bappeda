<div class="main-content kd-content" id="panel" data-name="<?php echo $this->security->get_csrf_token_name(); ?>" data-token="<?php echo $this->security->get_csrf_hash(); ?>">
    <!-- BEGIN TOPBAR -->
    <?php $this->load->view(VIEW_BACK . 'template_topbar'); ?>
    <!-- END TOPBAR -->

    <!-- BEGIN CONTENT -->
    <?php if (bp_is_assuming()) : ?>
        <div class="px-3 py-2 bg-gradient-danger text-white">
            <?php
            $_curr_member   = bp_get_current_member();
            $_assume_member = $_curr_member->name . ' (' . $_curr_member->username . ')';
            $assumed_text   = lang('assumed_text');
            $assumed_text   = str_replace("%cur_member_name%", $_assume_member, $assumed_text);
            echo $assumed_text;
            ?>
        </div>
    <?php endif ?>

    <?php $this->load->view(VIEW_BACK . $main_content); ?>

    <!-- Modal Action Program -->
    <div class="modal fade" id="modal-choice-year-stages" tabindex="-1" role="dialog" aria-labelledby="modal-choiche-year-stages" aria-hidden="true">
        <div class="modal-dialog modal-md modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header pt-3 pb-1">
                    <h5 class="modal-title text-default"><i class="ni ni-book-bookmark mr-1"></i> Tahun dan Tahap Perencanaan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <?php echo form_open('auth/validateyearstages', array('id' => 'form-choice-year-stages', 'role' => 'form', 'class' => 'form-horizontal')); ?>
                    <div class="modal-body wrapper-modal px-4 py-3" style="background-color: #f8f9fe">
                        <div class="form-group row mb-2">
                            <label class="col-md-3 col-form-label form-control-label">Tahun</label>
                            <div class="col-md-9">
                                <select name="year" id="year" class="form-control select_top_year" data-url="api/getstages" >
                                    <?php 
                                        $cfg_stage_year = bp_stages(0, array('year'), 'year', 'DESC');
                                        if ( $cfg_stage_year ) {
                                            foreach ($cfg_stage_year as $key => $row) {
                                                $selected   = ($member->select_year == $row->year) ? 'selected=""' : '';
                                                echo '<option value="'. $row->year .'" '. $selected .'>'. $row->year .'</option>';
                                            }
                                        } else {
                                            $next_year      = date('Y', strtotime('+1 year'));
                                            $before_year    = date('Y', strtotime('-1 year'));
                                            for ($i=$next_year; $i>=$before_year; $i--) { 
                                                $selected   = ($member->select_year == $i) ? 'selected=""' : '';
                                                echo '<option value="'. $i .'" '. $selected .'>'. $i .'</option>';
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row mb-2">
                            <label class="col-md-3 col-form-label form-control-label">Tahap</label>
                            <div class="col-md-9">
                                <select name="stages" id="stages" class="form-control select_top_stage">
                                    <?php
                                        $cfg_stages_opt = config_item('stages');
                                        $cfg_stages_by  = bp_stage_by('year', $member->select_year, '', 0, 'stage');
                                        if ( $cfg_stages_by ) {
                                            foreach ($cfg_stages_by as $key => $row) {
                                                $selected   = ($member->select_stages == $row->stage) ? 'selected=""' : '';
                                                $stage_name = isset($cfg_stages_opt[$row->stage]) ? lang($cfg_stages_opt[$row->stage]) : $row->stage;
                                                echo '<option value="'. $row->stage .'" '. $selected .'>'. $stage_name .'</option>';
                                            }
                                        } else {
                                            if ( $cfg_stages_opt ) {
                                                foreach ($cfg_stages_opt as $key => $value) {
                                                    $selected   = ($member->select_stages == $key) ? 'selected=""' : '';
                                                    echo '<option value="' . $key . '" '. $selected .'>' . lang($value) . '</option>';
                                                }
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer py-2">
                        <button type="button" class="btn btn-sm btn-outline-warning" data-dismiss="modal"><?php echo lang('back'); ?></button>
                        <button type="submit" class="btn btn-sm bg-sidebar text-white" id="btn-modal-action">Submit</button>
                    </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
    <!-- END CONTENT -->

    <!-- BEGIN FOOTER -->
    <div class="container-fluid mt-4">
        <footer class="footer pt-0">
            <div class="row align-items-center justify-content-lg-between">
                <div class="col-lg-12">
                    <div class="copyright text-center  text-lg-left  text-muted">
                        &copy; <a href="#!" class="font-weight-bold ml-1"><?php echo COMPANY_NAME; ?></a> 2021
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <!-- END FOOTER -->
</div>