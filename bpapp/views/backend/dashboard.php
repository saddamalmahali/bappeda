<?php
$currency           = config_item('currency');
$time               = date('H');
$hi                 = '';
$name               = $member->name;
$dashboard_admin    = true;

if ($staff = bp_get_current_staff()) {
    $name   = $staff->name;
    if ( $staff->access == 'partial' ) {
        $dashboard_admin = false;
    }
}

if ($time >= '00' && $time <= '09') {
    $hi = lang('morning');
} elseif ($time > '09' && $time <= '14') {
    $hi = lang('daylight');
} elseif ($time > '14' && $time <= '18') {
    $hi = lang('afternoon');
} elseif ($time > '18' && $time <= '24') {
    $hi = lang('evening');
}

// Chart
$sales_label    = array(lang('menu_initial_plan'), lang('menu_plan'), lang('menu_final_plan'));
$sales_trx      = array(1500, 1200, 1000);
$sales_data     = array(6500, 5250, 5000);
$sales_label    = '"' . implode('","', $sales_label) . '"';

?>

<div class="header bg-sidebar pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-12">
                    <?php
                    $welcome_text   = lang('welcome_text');
                    $welcome_text   = str_replace("%hi%", $hi, $welcome_text);
                    $welcome_text   = str_replace("%name%", ucwords(strtolower($name)), $welcome_text);
                    echo $welcome_text;
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-3 col-md-6">
                    <div class="card card-stats">
                        <!-- Card body -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col pr-0">
                                    <h5 class="card-title text-uppercase text-muted mb-0">Total Program</h5>
                                    <span class="h4 font-weight-bold mb-0"><?= bp_accounting(350); ?></span>
                                </div>
                                <div class="col-auto">
                                    <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                                        <i class="ni ni-book-bookmark"></i>
                                    </div>
                                </div>
                            </div>
                            <p class="mt-3 mb-0 text-sm">
                                <a href="<?= base_url('member/lists'); ?>" class="text-nowrap text-default"><?= lang('see_more'); ?></a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card card-stats">
                        <!-- Card body -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col pr-0">
                                    <h5 class="card-title text-uppercase text-muted mb-0">Total Kegiatan</h5>
                                    <span class="h4 font-weight-bold mb-0"><?= bp_accounting(1500) ?></span>
                                </div>
                                <div class="col-auto">
                                    <div class="icon icon-shape bg-gradient-orange text-white rounded-circle shadow">
                                        <i class="ni ni-book-bookmark"></i>
                                    </div>
                                </div>
                            </div>
                            <p class="mt-3 mb-0 text-sm">
                                <a href="<?= base_url('report/omzet'); ?>" class="text-nowrap text-default"><?= lang('see_more'); ?></a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card card-stats">
                        <!-- Card body -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col pr-0">
                                    <h5 class="card-title text-uppercase text-muted mb-0">Total Sub-Kegiatan</h5>
                                    <span class="h4 font-weight-bold mb-0"><?= bp_accounting(2000); ?></span>
                                    <!-- <span class="h6 font-weight-bold text-info mb-0"><?= bp_number($percentage); ?> %</span> -->
                                </div>
                                <div class="col-auto">
                                    <div class="icon icon-shape bg-gradient-info text-white rounded-circle shadow">
                                        <i class="ni ni-book-bookmark"></i>
                                    </div>
                                </div>
                            </div>
                            <p class="mt-3 mb-0 text-sm">
                                <a href="<?= base_url('commission/bonus'); ?>" class="text-nowrap text-default"><?= lang('see_more'); ?></a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card card-stats">
                        <!-- Card body -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col pr-0">
                                    <h5 class="card-title text-uppercase text-muted mb-0">Total Anggaran</h5>
                                    <span class="h5 font-weight-bold mb-0">
                                        <?= bp_accounting(5000000000000, $currency); ?> 
                                    </span>
                                </div>
                                <div class="col-auto">
                                    <div class="icon icon-shape bg-gradient-green text-white rounded-circle shadow">
                                        <i class="ni ni-money-coins"></i>
                                    </div>
                                </div>
                            </div>
                            <p class="mt-3 mb-0 text-sm">
                                <a href="<?= base_url('report/sales'); ?>" class="text-nowrap text-default"><?= lang('see_more'); ?></a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid mt--6">
    <div class="row">
        <div class="col-xl-8">
            <div class="card">
                <div class="card-header bg-transparent">
                    <div class="row align-items-center">
                        <div class="col">
                            <h6 class="text-muted text-uppercase ls-1 mb-1">Overview</h6>
                            <h5 class="h3 mb-0">Total Perencanaan Anggaran</h5>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <!-- Chart -->
                    <div class="chart">
                        <span class="d-none sales-light" data-toggle="chart" data-target="#chart-sales-light" data-prefix="" data-suffix=" M" data-update='{
                            "data":{
                                "labels": [<?= $sales_label; ?>],
                                "datasets":[{
                                    "label": "Total Anggaran",
                                    "data":[<?= implode(',', $sales_data); ?>]
                                }]
                            }
                        }'></span>
                        <!-- Chart wrapper -->
                        <canvas id="chart-sales-light" class="chart-canvas"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-4">
            <div class="card">
                <div class="card-header bg-transparent">
                    <div class="row align-items-center">
                        <div class="col">
                            <h6 class="text-uppercase text-muted ls-1 mb-1">Overview</h6>
                            <h5 class="h3 mb-0">Total Sub-Kegiatan</h5>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <!-- Chart -->
                    <div class="chart">
                        <span class="d-none sales-canvas" data-toggle="chart" data-target="#chart-bars" data-prefix=" " data-suffix="" data-update='{
                            "data":{
                                "labels": [<?= $sales_label; ?>],
                                "datasets":[{
                                    "label": "Total Sub-Kegiatan",
                                    "data":[<?= implode(',', $sales_trx); ?>]
                                }]
                            }
                        }'></span>
                        <canvas id="chart-bars" class="chart-canvas"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>