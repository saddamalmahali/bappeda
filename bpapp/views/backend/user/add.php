<div class="header bg-sidebar pb-6">
	<div class="container-fluid">
		<div class="header-body">
			<div class="row align-items-center py-4">
				<div class="col-lg-6 col-7">
					<nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
						<ol class="breadcrumb breadcrumb-links breadcrumb-dark">
							<li class="breadcrumb-item"><a href="<?= base_url('dashboard') ?>"><i class="fas fa-home"></i></a></li>
							<li class="breadcrumb-item"><a href="#"><?= lang('menu_users') ?></a></li>
							<li class="breadcrumb-item active" aria-current="page"><?= $title_page ?></li>
						</ol>
					</nav>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid mt--6">
	<div class="row">
		<div class="col-xl-12">
			<div class="row justify-content-center">
				<div class="col-lg-12 card-wrapper">
					<div class="card">
						<div class="card-header">
							<div class="row align-items-center">
								<div class="col">
									<h3 class="mb-0">Form <?= $title_page ?></h3>
								</div>
								<div class="col text-right">
									<a href="<?= base_url('user') ?>" class="btn btn-sm btn-danger"><span class="fa fa-history"></span> Kembali</a>
								</div>
							</div>
						</div>
						<div class="card-body wrapper-form-staff">
							<?php
							$cfg_access 	= config_item('user_access');
							$form_action 	= base_url('user/saveuser');
							$form_input  	= 'create';
							$user_access 	= bp_isset($userdata->type, '');
							$fitur_roles 	= bp_isset($userdata->roles, '', '', false, false);
							if (isset($userdata->id) && !empty($userdata->id)) {
								$form_input  = 'edit';
								$form_action .= '/' . bp_encrypt($userdata->id);
							}
							?>
							<form role="form" method="post" action="<?= $form_action; ?>" id="form-staff" class="form-horizontal" data-type="<?= $form_input ?>">
								<div class="row justify-content-center">
									<div class="col-md-10 col-sm-12">
										<?php if ($is_admin) : ?>
											<hr>
											<div class="form-group row mb-2">
												<label class="col-md-3 col-form-label form-control-label"><?= lang('access'); ?> <span class="required">*</span></label>
												<div class="col-md-9">
													<select name="user_access" id="user_access" class="form-control">
														<option value=""><?= lang('select') ?></option>
														<?php
														if ($cfg_access) {
															foreach ($cfg_access as $key => $value) {
																$selected = ($user_access == $key) ? 'selected=""' : '';
																echo '<option value="' . $key . '" data-access="' . strtolower($value) . '" ' . $selected . '>' . $value . '</option>';
															}
														}
														?>
													</select>
												</div>
											</div>

											<div class="access-box access-box-admin" <?= ($user_access == ADMIN) ? '' : 'style="display: none"' ?>>
												<div class="form-group row">
													<label class="col-md-3 col-form-label form-control-label">Fitur <span class="required">*</span></label>
													<div class="col-md-9">
														<div class="btn-group" data-toggle="buttons">
															<label id="fitur_not_all" class="btn btn-flat btn-primary fitur-toggle <?= bp_isset($userdata->access, 'partial') == 'partial' ? 'active' : ''; ?>">
																<input name="fitur_access" class="toggle" type="radio" value="partial" <?= bp_isset($userdata->access, 'partial') == 'partial' ? 'checked="checked"' : ''; ?>>
																<i class="fa fa-lock mr-1"></i> Fitur Tertentu
															</label>
															<label id="fitur_all" class="btn btn-flat btn-primary fitur-toggle <?= bp_isset($userdata->access, 'partial') == 'all' ? 'active' : ''; ?>">
																<input name="fitur_access" class="toggle" type="radio" value="all" <?= bp_isset($userdata->access, 'partial') == 'all' ? 'checked="checked"' : ''; ?>>
																<i class="fa fa-unlock-alt mr-1"></i> Semua Fitur
															</label>
														</div>
													</div>
												</div>

												<!-- particular features access -->
												<div class="form-group row mb-2 fitur-box fitur-box-partial">
													<div class="col-md-3"></div>
													<div class="col-md-9">
														<?php for ($i = 1; $i <= 9; $i++) { ?>
															<?php
															$access_id = constant('STAFF_ACCESS' . $i);
															if (in_array($access_id, array(STAFF_ACCESS14, STAFF_ACCESS15))) continue;

															$checked_partial 	= '';
															if (is_array($fitur_roles) && in_array($access_id, $fitur_roles)) {
																$checked_partial = 'checked="checked"';
															}
															?>
															<div class="custom-control custom-checkbox mb-3">
																<input type="checkbox" class="custom-control-input" id="fitur_partial_<?= $access_id; ?>" name="fitur_partial[]" value="<?= $access_id; ?>" <?= $checked_partial ?> />
																<label class="custom-control-label" for="fitur_partial_<?= $access_id; ?>" style="vertical-align: unset;">
																	<?= $config[$access_id]; ?>
																</label>
															</div>
														<?php } ?>
													</div>
												</div>

												<!-- all features access -->
												<div class="form-group row mb-2 fitur-box fitur-box-all">
													<div class="col-md-3"></div>
													<div class="col-md-9">
														<?php for ($i = 14; $i <= 15; $i++) { ?>
															<?php
															$access_id = constant('STAFF_ACCESS' . $i);
															if (!in_array($access_id, array(STAFF_ACCESS14, STAFF_ACCESS15))) continue;

															$checked_all 	= '';
															if (is_array($fitur_roles) && in_array($access_id, $fitur_roles)) {
																$checked_all = 'checked="checked"';
															}
															?>
															<div class="custom-control custom-checkbox mb-3">
																<input type="checkbox" class="custom-control-input" id="fitur_all_<?= $access_id; ?>" name="fitur_all[]" value="<?= $access_id; ?>" <?= $checked_all  ?> />
																<label class="custom-control-label" for="fitur_all_<?= $access_id; ?>" style="vertical-align: unset;">
																	<?= 'Termasuk ' . $config[$access_id]; ?>
																</label>
															</div>
														<?php } ?>
													</div>
												</div>
											</div>

											<div class="access-box access-box-bidang" <?= ($user_access == BIDANG) ? '' : 'style="display: none"' ?>>
												<div class="form-group row">
													<label class="col-md-3 col-form-label form-control-label">Bidang <span class="required">*</span></label>
													<div class="col-md-9">
														<select name="user_bidang" id="user_bidang" class="form-control select-user-access">
															<option value=""><?= lang('select') ?></option>
															<?php
															if ($cfg_bidang = bp_bidang()) {
																$user_bidang = bp_isset($userdata->id_bidang, '');
																foreach ($cfg_bidang as $row) {
																	$selected = ($user_bidang == $row->id) ? 'selected=""' : '';
																	echo '<option value="' . $row->id . '" ' . $selected . '>' . strtoupper($row->nama) . '</option>';
																}
															}
															?>
														</select>
													</div>
												</div>
											</div>

											<div class="access-box access-box-skpd" <?= ($user_access == SKPD) ? '' : 'style="display: none"' ?>>
												<div class="form-group row">
													<label class="col-md-3 col-form-label form-control-label">SKPD <span class="required">*</span></label>
													<div class="col-md-9">
														<select name="user_skpd" id="user_skpd" class="form-control select-user-access" data-toggle="select2">
															<option value=""><?= lang('select') ?></option>
															<?php
															if ($cfg_skpd = bp_skpd()) {
																$user_skpd = bp_isset($userdata->id_skpd, '');
																foreach ($cfg_skpd as $row) {
																	$selected = ($user_skpd == $row->id) ? 'selected=""' : '';
																	echo '<option value="' . $row->id . '" ' . $selected . '>' . strtoupper($row->nama) . '</option>';
																}
															}
															?>
														</select>
													</div>
												</div>
											</div>
											<hr>
										<?php endif; ?>
										<div class="form-group row mb-2">
											<label class="col-md-3 col-form-label form-control-label"><?= lang('username'); ?> <span class="required">*</span></label>
											<div class="col-md-9">
												<div class="input-group input-group-merge">
													<div class="input-group-prepend">
														<span class="input-group-text"><i class="ni ni-circle-08"></i></span>
													</div>
													<input type="text" class="form-control text-lowercase" name="user_username" id="user_username" data-url="<?= base_url('user/checkusername'); ?>" placeholder="<?= lang('username'); ?>" value="<?= bp_isset($userdata->username, '', '') ?>" <?= ($form_input == 'edit') ? 'disabled="disabled"' : ''; ?> />
												</div>
											</div>
										</div>

										<?php if ($form_input == 'create') { ?>
											<div class="form-group row mb-2">
												<label class="col-md-3 col-form-label form-control-label"><?= lang('reg_password'); ?> <span class="required">*</span></label>
												<div class="col-md-9">
													<div class="input-group input-group-merge">
														<div class="input-group-prepend">
															<span class="input-group-text"><i class="fa fa-lock"></i></span>
														</div>
														<input type="password" name="user_password" id="user_password" class="form-control" placeholder="<?= lang('reg_valid_password'); ?>" autocomplete="off" value="" />
														<div class="input-group-append">
															<button class="btn btn-white pass-show-hide" type="button"><i class="fa fa-eye-slash"></i></button>
														</div>
													</div>
												</div>
											</div>

											<div class="form-group row mb-2">
												<label class="col-md-3 col-form-label form-control-label">Confirm password <span class="required">*</span></label>
												<div class="col-md-9">
													<div class="input-group input-group-merge">
														<div class="input-group-prepend">
															<span class="input-group-text"><i class="fa fa-lock"></i></span>
														</div>
														<input type="password" name="user_password_confirm" id="user_password_confirm" class="form-control" placeholder="Konfirmasi Password" autocomplete="off" value="" />
														<div class="input-group-append">
															<button class="btn btn-white pass-show-hide" type="button"><i class="fa fa-eye-slash"></i></button>
														</div>
													</div>
												</div>
											</div>
										<?php } ?>

										<!-- Name -->
										<div class="form-group row mb-2">
											<label class="col-md-3 col-form-label form-control-label"><?= lang('name'); ?> <span class="required">*</span></label>
											<div class="col-md-9">
												<div class="input-group input-group-merge">
													<div class="input-group-prepend">
														<span class="input-group-text"><i class="fa fa-user"></i></span>
													</div>
													<input type="text" class="form-control text-uppercase" name="user_name" id="user_name" placeholder="<?= lang('name'); ?>" value="<?= bp_isset($userdata->name, '', '') ?>" />
												</div>
											</div>
										</div>

										<!-- Email -->
										<div class="form-group row mb-2">
											<label class="col-md-3 col-form-label form-control-label"><?= lang('reg_email'); ?> <span class="required">*</span></label>
											<div class="col-md-9">
												<div class="input-group input-group-merge">
													<div class="input-group-prepend">
														<span class="input-group-text"><i class="fa fa-envelope"></i></span>
													</div>
													<input type="text" name="user_email" id="user_email" class="form-control text-lowercase" placeholder="<?= lang('reg_email'); ?>" data-url="<?= base_url('user/checkemail'); ?>" value="<?= bp_isset($userdata->email, '', '') ?>" />
												</div>
											</div>
										</div>

										<!-- No. Telp/HP -->
										<div class="form-group row mb-2">
											<label class="col-md-3 col-form-label form-control-label"><?= lang('reg_no_telp'); ?> </label>
											<div class="col-md-9">
												<div class="input-group input-group-merge">
													<div class="input-group-prepend">
														<span class="input-group-text">+62</span>
													</div>
													<input type="text" name="user_phone" id="user_phone" class="form-control numbermask phonenumber" placeholder="8xxxxxxxxx" value="<?= bp_isset($userdata->phone, '', '') ?>" />
													<div class="input-group-append">
														<span class="input-group-text"><i class="fa fa-phone"></i></span>
													</div>
												</div>
											</div>
										</div>

									</div>
								</div>
								<hr class="my-4" />
								<div class="text-center">
									<button type="submit" class="btn btn-primary my-2">
										<?= lang('save') . ' ' . lang('menu_users'); ?>
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>