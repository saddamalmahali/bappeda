<div class="header bg-sidebar pb-6">
	<div class="container-fluid">
		<div class="header-body">
			<div class="row align-items-center py-4">
				<div class="col-lg-6 col-7">
					<nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
						<ol class="breadcrumb breadcrumb-links breadcrumb-dark">
							<li class="breadcrumb-item"><a href="<?php echo base_url('dashboard') ?>"><i class="fas fa-home"></i></a></li>
							<li class="breadcrumb-item"><a href="#"><?php echo lang('menu_users') ?></a></li>
							<li class="breadcrumb-item active" aria-current="page"><?= $title_page ?></li>
						</ol>
					</nav>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid mt--6">
	<div class="row">
		<div class="col-xl-12">
			<div class="row justify-content-center">
				<div class="col-lg-12 card-wrapper">
					<div class="card">
						<div class="card-header">
							<div class="row align-items-center">
								<div class="col">
									<h3 class="mb-0">Form <?= $title_page ?></h3>
								</div>
								<div class="col text-right">
									<a href="<?php echo base_url('user') ?>" class="btn btn-sm btn-danger"><span class="fa fa-history"></span> Kembali</a>
								</div>
							</div>
						</div>
						<div class="card-body wrapper-form-staff">
							<?php
							$cfg_access 	= config_item('user_access');
							$form_action 	= base_url('user/savestaff');
							$form_input  	= 'create';
							if (isset($userdata->id) && !empty($userdata->id)) {
								$form_input  = 'edit';
								$form_action .= '/' . bp_encrypt($userdata->id);
							}
							?>
							<form role="form" method="post" action="<?php echo $form_action; ?>" id="form-staff" class="form-horizontal">
								<div class="row justify-content-center">
									<div class="col-md-10 col-sm-12">
										<div class="form-group row mb-2">
											<label class="col-md-3 col-form-label form-control-label"><?php echo lang('username'); ?> <span class="required">*</span></label>
											<div class="col-md-9">
												<div class="input-group input-group-merge">
													<div class="input-group-prepend">
														<span class="input-group-text"><i class="ni ni-circle-08"></i></span>
													</div>
													<input type="text" class="form-control text-lowercase" name="user_username" id="user_username" data-url="<?php echo base_url('member/checkusername'); ?>" placeholder="<?php echo lang('username'); ?>" value="<?php echo bp_isset($userdata->username, '', '') ?>" <?php echo ($form_input == 'edit') ? 'disabled="disabled"' : ''; ?> />
												</div>
											</div>
										</div>

										<?php if ($form_input == 'create') { ?>
											<div class="form-group row mb-2">
												<label class="col-md-3 col-form-label form-control-label"><?php echo lang('reg_password'); ?> <span class="required">*</span></label>
												<div class="col-md-9">
													<div class="input-group input-group-merge">
														<div class="input-group-prepend">
															<span class="input-group-text"><i class="fa fa-lock"></i></span>
														</div>
														<input type="password" name="user_password" id="user_password" class="form-control" placeholder="<?php echo lang('reg_valid_password'); ?>" autocomplete="off" value="" />
														<div class="input-group-append">
															<button class="btn btn-default pass-show-hide" type="button"><i class="fa fa-eye-slash"></i></button>
														</div>
													</div>
												</div>
											</div>

											<div class="form-group row mb-2">
												<label class="col-md-3 col-form-label form-control-label">Confirm password <span class="required">*</span></label>
												<div class="col-md-9">
													<div class="input-group input-group-merge">
														<div class="input-group-prepend">
															<span class="input-group-text"><i class="fa fa-lock"></i></span>
														</div>
														<input type="password" name="user_password_confirm" id="user_password_confirm" class="form-control" placeholder="Konfirmasi Password" autocomplete="off" value="" />
														<div class="input-group-append">
															<button class="btn btn-default pass-show-hide" type="button"><i class="fa fa-eye-slash"></i></button>
														</div>
													</div>
												</div>
											</div>
										<?php } ?>

										<!-- Name -->
										<div class="form-group row mb-2">
											<label class="col-md-3 col-form-label form-control-label"><?php echo lang('name'); ?> <span class="required">*</span></label>
											<div class="col-md-9">
												<div class="input-group input-group-merge">
													<div class="input-group-prepend">
														<span class="input-group-text"><i class="fa fa-user"></i></span>
													</div>
													<input type="text" class="form-control text-uppercase" name="user_name" id="user_name" placeholder="<?php echo lang('name'); ?>" value="<?php echo bp_isset($userdata->name, '', '') ?>" />
												</div>
											</div>
										</div>

										<!-- No. Telp/HP -->
										<div class="form-group row mb-2">
											<label class="col-md-3 col-form-label form-control-label"><?php echo lang('reg_no_telp'); ?> <span class="required">*</span></label>
											<div class="col-md-9">
												<div class="input-group input-group-merge">
													<div class="input-group-prepend">
														<span class="input-group-text">+62</span>
													</div>
													<input type="text" name="user_phone" id="user_phone" class="form-control numbermask phonenumber" placeholder="8xxxxxxxxx" value="<?php echo bp_isset($userdata->phone, '', '') ?>" />
													<div class="input-group-append">
														<span class="input-group-text"><i class="fa fa-phone"></i></span>
													</div>
												</div>
											</div>
										</div>

										<!-- Email -->
										<div class="form-group row mb-2">
											<label class="col-md-3 col-form-label form-control-label"><?php echo lang('reg_email'); ?> <span class="required">*</span></label>
											<div class="col-md-9">
												<div class="input-group input-group-merge">
													<div class="input-group-prepend">
														<span class="input-group-text"><i class="fa fa-envelope"></i></span>
													</div>
													<input type="text" name="user_email" id="user_email" class="form-control text-lowercase" placeholder="<?php echo lang('reg_email'); ?>" data-url="<?php echo base_url('member/checkemail'); ?>" value="<?php echo bp_isset($userdata->email, '', '') ?>" />
												</div>
											</div>
										</div>

										<hr>

										<div class="form-group row mb-2">
											<label class="col-md-3 col-form-label form-control-label"><?php echo lang('access'); ?> <span class="required">*</span></label>
											<div class="col-md-9">
												<select name="user_access" id="user_access" class="form-control">
													<option value=""><?= lang('select') ?></option>
													<?php 
														if ( $cfg_access ) {
															foreach ($cfg_access as $key => $value) {
																echo '<option value="'. $key .'" data-access="'. strtolower($value). '">'. $value .'</option>';
															}
														}
													?>
												</select>
											</div>
										</div>

										<div class="access-box access-box-admin" style="display: none">
											<div class="form-group row">
												<label class="col-md-3 col-form-label form-control-label">Fitur <span class="required">*</span></label>
												<div class="col-md-9">
													<div class="btn-group" data-toggle="buttons">
														<label id="fitur_not_all" class="btn btn-flat btn-primary fitur-toggle <?php echo bp_isset($userdata->access, 'partial') == 'partial' ? 'active' : ''; ?>">
															<input name="fitur_access" class="toggle" type="radio" value="partial" <?php echo bp_isset($userdata->access, 'partial') == 'partial' ? 'checked="checked"' : ''; ?>>
															<i class="fa fa-lock mr-1"></i> Fitur Tertentu
														</label>
														<label id="fitur_all" class="btn btn-flat btn-primary fitur-toggle <?php echo bp_isset($userdata->access, 'partial') == 'all' ? 'active' : ''; ?>">
															<input name="fitur_access" class="toggle" type="radio" value="all" <?php echo bp_isset($userdata->access, 'partial') == 'all' ? 'checked="checked"' : ''; ?>>
															<i class="fa fa-unlock-alt mr-1"></i> Semua Fitur
														</label>
													</div>
												</div>
											</div>

											<!-- particular features access -->
											<div class="form-group row mb-2 fitur-box fitur-box-partial">
												<div class="col-md-3"></div>
												<div class="col-md-9">
													<?php for ($i = 1; $i <= 9; $i++) { ?>
														<?php $access_id = constant('STAFF_ACCESS' . $i); ?>
														<?php if (in_array($access_id, array(STAFF_ACCESS14, STAFF_ACCESS15))) continue; ?>
														<div class="custom-control custom-checkbox mb-3">
															<input type="checkbox" class="custom-control-input" id="fitur_partial_<?php echo $access_id; ?>" name="fitur_partial[]" value="<?php echo $access_id; ?>" <?php echo bp_isset($userdata->role, '', '', false, false) && is_array($userdata->role) && in_array($access_id, $userdata->role) ? 'checked="checked"' : ''; ?> />
															<label class="custom-control-label" for="fitur_partial_<?php echo $access_id; ?>" style="vertical-align: unset;">
																<?php echo $config[$access_id]; ?>
															</label>
														</div>
													<?php } ?>
												</div>
											</div>

											<!-- all features access -->
											<div class="form-group row mb-2 fitur-box fitur-box-all">
												<div class="col-md-3"></div>
												<div class="col-md-9">
													<?php for ($i = 14; $i <= 15; $i++) { ?>
														<?php $access_id = constant('STAFF_ACCESS' . $i); ?>
														<?php if (!in_array($access_id, array(STAFF_ACCESS14, STAFF_ACCESS15))) continue; ?>
														<div class="custom-control custom-checkbox mb-3">
															<input type="checkbox" class="custom-control-input" id="fitur_all_<?php echo $access_id; ?>" name="fitur_all[]" value="<?php echo $access_id; ?>" <?php echo bp_isset($userdata->role, '', '', false, false) && is_array($userdata->role) && in_array($access_id, $userdata->role) ? 'checked="checked"' : ''; ?> />
															<label class="custom-control-label" for="fitur_all_<?php echo $access_id; ?>" style="vertical-align: unset;">
																<?php echo 'Termasuk ' . $config[$access_id]; ?>
															</label>
														</div>
													<?php } ?>
												</div>
											</div>
										</div>

										<div class="access-box access-box-bidang" style="display: none">
											<div class="form-group row">
												<label class="col-md-3 col-form-label form-control-label">Bidang <span class="required">*</span></label>
												<div class="col-md-9">
													<select name="user_bidang" id="user_bidang" class="form-control select-user-access">
														<option value=""><?= lang('select') ?></option>
														<?php 
															if ( $cfg_bidang = bp_bidang() ) {
																foreach ($cfg_bidang as $key => $row) {
																	echo '<option value="'. $row->id .'">'. strtoupper($row->nama) .'</option>';
																}
															}
														?>
													</select>
												</div>
											</div>
										</div>

										<div class="access-box access-box-skpd" style="display: none">
											<div class="form-group row">
												<label class="col-md-3 col-form-label form-control-label">SKPD <span class="required">*</span></label>
												<div class="col-md-9">
													<select name="user_skpd" id="user_skpd" class="form-control select-user-access" data-toggle="select2">
														<option value=""><?= lang('select') ?></option>
														<?php 
															if ( $cfg_skpd = bp_skpd() ) {
																foreach ($cfg_skpd as $key => $row) {
																	echo '<option value="'. $row->id .'">'. strtoupper($row->nama) .'</option>';
																}
															}
														?>
													</select>
												</div>
											</div>
										</div>

									</div>
								</div>
								<hr class="my-4" />
								<div class="text-center">
									<button type="submit" class="btn btn-default bg-gradient-default my-2">
										<?php echo lang('save') . ' ' . lang('menu_setting_staff'); ?>
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>