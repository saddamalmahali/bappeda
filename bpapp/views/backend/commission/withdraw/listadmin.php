<div class="nav-wrapper px-3">
    <ul class="nav nav-pills nav-fill flex-column flex-sm-row" id="tabs-icons-text" role="tablist">
        <li class="nav-item">
            <a class="nav-link btn_withdraw_status active" id="wd-withdraw-tabs" data-toggle="tab" href="#tabs-wd-withdraw" role="tab" aria-controls="tabs-wd-withdraw" aria-selected="true" data-status="withdraw">List <?php echo lang('menu_financial_withdraw'); ?></a>
        </li>
        <li class="nav-item">
            <a class="nav-link btn_withdraw_status" id="wd-withdraw-total-tabs" data-toggle="tab" href="#tabs-wd-withdraw-total" role="tab" aria-controls="tabs-wd-withdraw-total" aria-selected="false" data-status="total_withdraw">List <?php echo lang('transaction_total'); ?></a>
        </li>
    </ul>
</div>

<div class="card shadow">
    <div class="card-body px-0">
        <div class="tab-content" id="withdrawListContent">
            <div class="tab-pane fade show active" id="tabs-wd-withdraw" role="tabpanel" aria-labelledby="tabs-wd-withdraw">
                <h3 class="px-3">List <?php echo lang('menu_financial_withdraw'); ?></h3>
                <div class="table-container">
                    <table class="table align-items-center table-flush" id="list_table_withdraw" data-url="<?php echo base_url('commission/withdrawlistdata'); ?>">
                        <thead class="thead-light">
                            <tr role="row" class="heading">
                                <th scope="col" style="width: 10px">#</th>
                                <th scope="col" class="text-center"><?php echo lang('username'); ?></th>
                                <th scope="col"><?php echo lang('name'); ?></th>
                                <th scope="col" class="text-center"><?php echo lang('bank'); ?></th>
                                <th scope="col" class="text-center"><?php echo lang('bank_account'); ?></th>
                                <th scope="col" class="text-center"><?php echo lang('nominal'); ?></th>
                                <th scope="col" class="text-center"><?php echo lang('status'); ?></th>
                                <th scope="col" class="text-center"><?php echo lang('information'); ?></th>
                                <th scope="col" class="text-center">Flip ID</th>
                                <th scope="col" class="text-center"><?php echo lang('date'); ?></th>
                                <th scope="col" class="text-center"><?php echo lang('confirm_date'); ?></th>
                                <th scope="col" class="text-center"><?php echo lang('confirm_by'); ?></th>
                                <th scope="col" class="text-center"><?php echo lang('actions'); ?></th>
                            </tr>
                            <tr role="row" class="filter" style="background-color: #f6f9fc">
                                <td></td>
                                <td class="px-1"><input type="text" class="form-control form-control-sm form-filter" name="search_username" /></td>
                                <td class="px-1"><input type="text" class="form-control form-control-sm form-filter" name="search_name" /></td>
                                <td class="px-1">
                                    <select class="form-control form-control-sm form-filter" name="search_bank">
                                        <option value=""><?php echo lang('select'); ?>...</option>
                                        <?php
                                        if ($banks = bp_banks()) {
                                            foreach ($banks as $b) {
                                                echo '<option value="' . $b->id . '">' . $b->kode . ' - ' . $b->nama . '</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                </td>
                                <td class="px-1">
                                    <div class="mb-1">
                                        <input type="text" class="form-control form-control-sm form-filter numbermask" name="search_bill" placeholder="<?php echo lang('no_rekening'); ?>" />
                                    </div>
                                    <input type="text" class="form-control form-control-sm form-filter" name="search_bill_name" placeholder="<?php echo lang('pemilik_rek'); ?>" />
                                </td>
                                <td class="px-1">
                                    <div class="mb-1">
                                        <input type="text" class="form-control form-control-sm form-filter text-center numbermask" name="search_nominal_min" placeholder="Min" />
                                    </div>
                                    <input type="text" class="form-control form-control-sm form-filter text-center numbermask" name="search_nominal_max" placeholder="Max" />
                                </td>
                                <td class="px-1">
                                    <select name="search_status" class="form-control form-control-sm form-filter">
                                        <option value=""><?php echo lang('select'); ?>...</option>
                                        <option value="pending">PENDING</option>
                                        <option value="transfered">TRANSFERED</option>
                                    </select>
                                </td>
                                <td class="px-1"></td>
                                <td class="px-1"><input type="text" class="form-control form-control-sm form-filter" name="search_flip_id" /></td>
                                <td class="px-1">
                                    <div class="input-group input-group-sm date date-picker mb-1" data-date-format="yyyy-mm-dd">
                                        <input type="text" class="form-control form-control-sm form-filter" readonly name="search_datecreated_min" placeholder="From" />
                                        <span class="input-group-btn">
                                            <button class="btn btn-sm btn-white btn-flat" type="button"><i class="ni ni-calendar-grid-58 text-primary"></i></button>
                                        </span>
                                    </div>
                                    <div class="input-group input-group-sm date date-picker" data-date-format="yyyy-mm-dd">
                                        <input type="text" class="form-control form-control-sm form-filter" readonly name="search_datecreated_max" placeholder="To" />
                                        <span class="input-group-btn">
                                            <button class="btn btn-sm btn-white btn-flat" type="button"><i class="ni ni-calendar-grid-58 text-primary"></i></button>
                                        </span>
                                    </div>
                                </td>
                                <td class="px-1">
                                    <div class="input-group input-group-sm date date-picker mb-1" data-date-format="yyyy-mm-dd">
                                        <input type="text" class="form-control form-control-sm form-filter" readonly name="search_datemodified_min" placeholder="From" />
                                        <span class="input-group-btn">
                                            <button class="btn btn-sm btn-white btn-flat" type="button"><i class="ni ni-calendar-grid-58 text-primary"></i></button>
                                        </span>
                                    </div>
                                    <div class="input-group input-group-sm date date-picker" data-date-format="yyyy-mm-dd">
                                        <input type="text" class="form-control form-control-sm form-filter" readonly name="search_datemodified_max" placeholder="To" />
                                        <span class="input-group-btn">
                                            <button class="btn btn-sm btn-white btn-flat" type="button"><i class="ni ni-calendar-grid-58 text-primary"></i></button>
                                        </span>
                                    </div>
                                </td>
                                <td class="px-1"><input type="text" class="form-control form-control-sm form-filter" name="search_confirm_by" /></td>
                                <td style="text-align: center;">
                                    <button class="btn btn-sm btn-outline-default btn-tooltip filter-submit" id="btn_list_table_withdraw" title="Search"><i class="fa fa-search"></i></button>
                                    <button class="btn btn-sm btn-outline-warning btn-tooltip filter-cancel" title="Reset"><i class="fa fa-times"></i></button>
                                </td>
                            </tr>
                        </thead>
                        <tbody class="list">
                            <!-- Data Will Be Placed Here -->
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="tab-pane fade" id="tabs-wd-withdraw-total" role="tabpanel" aria-labelledby="tabs-wd-withdraw-total">
                <h3 class="px-3">List <?php echo lang('transaction_total'); ?></h3>
                <input type="hidden" class="d-none" id="load_withdraw_total" value="0">
                <div class="table-container mb-3">
                    <table class="table align-items-center table-flush" id="list_table_withdraw_total" data-url="<?php echo base_url('commission/withdrawtotallistdata'); ?>">
                        <thead class="thead-light">
                            <tr role="row" class="heading">
                                <th scope="col" style="width: 10px">#</th>
                                <th scope="col" class="text-center"><?php echo lang('date'); ?></th>
                                <th scope="col" class="text-center"><?php echo lang('transaction'); ?></th>
                                <th scope="col" class="text-center"><?php echo lang('nominal'); ?></th>
                                <th scope="col" class="text-center">Fee Transfer</th>
                                <th scope="col" class="text-center"><?php echo lang('nominal'); ?> + Fee</th>
                                <th scope="col" class="text-center">Topup</th>
                                <th scope="col" class="text-center">Saldo</th>
                                <th scope="col" class="text-center"><?php echo lang('actions'); ?></th>
                            </tr>
                            <tr role="row" class="filter" style="background-color: #f6f9fc">
                                <td></td>
                                <td class="px-1">
                                    <div class="input-group input-group-sm date date-picker mb-1" data-date-format="yyyy-mm-dd">
                                        <input type="text" class="form-control form-control-sm form-filter" readonly name="search_datecreated_min" placeholder="From" />
                                        <span class="input-group-btn">
                                            <button class="btn btn-sm btn-white btn-flat" type="button"><i class="ni ni-calendar-grid-58 text-primary"></i></button>
                                        </span>
                                    </div>
                                    <div class="input-group input-group-sm date date-picker" data-date-format="yyyy-mm-dd">
                                        <input type="text" class="form-control form-control-sm form-filter" readonly name="search_datecreated_max" placeholder="To" />
                                        <span class="input-group-btn">
                                            <button class="btn btn-sm btn-white btn-flat" type="button"><i class="ni ni-calendar-grid-58 text-primary"></i></button>
                                        </span>
                                    </div>
                                </td>
                                <td class="px-1">
                                    <div class="mb-1">
                                        <input type="text" class="form-control form-control-sm form-filter text-center numbermask" name="search_trx_min" placeholder="Min" />
                                    </div>
                                    <input type="text" class="form-control form-control-sm form-filter text-center numbermask" name="search_trx_max" placeholder="Max" />
                                </td>
                                <td class="px-1">
                                    <div class="mb-1">
                                        <input type="text" class="form-control form-control-sm form-filter text-center numbermask" name="search_nominal_min" placeholder="Min" />
                                    </div>
                                    <input type="text" class="form-control form-control-sm form-filter text-center numbermask" name="search_nominal_max" placeholder="Max" />
                                </td>
                                <td class="px-1">
                                    <div class="mb-1">
                                        <input type="text" class="form-control form-control-sm form-filter text-center numbermask" name="search_fee_min" placeholder="Min" />
                                    </div>
                                    <input type="text" class="form-control form-control-sm form-filter text-center numbermask" name="search_fee_max" placeholder="Max" />
                                </td>
                                <td class="px-1">
                                    <div class="mb-1">
                                        <input type="text" class="form-control form-control-sm form-filter text-center numbermask" name="search_nominal_fee_min" placeholder="Min" />
                                    </div>
                                    <input type="text" class="form-control form-control-sm form-filter text-center numbermask" name="search_nominal_fee_max" placeholder="Max" />
                                </td>
                                <td class="px-1">
                                    <div class="mb-1">
                                        <input type="text" class="form-control form-control-sm form-filter text-center numbermask" name="search_topup_min" placeholder="Min" />
                                    </div>
                                    <input type="text" class="form-control form-control-sm form-filter text-center numbermask" name="search_topup_max" placeholder="Max" />
                                </td>
                                <td class="px-1">
                                    <div class="mb-1">
                                        <input type="text" class="form-control form-control-sm form-filter text-center numbermask" name="search_saldo_min" placeholder="Min" />
                                    </div>
                                    <input type="text" class="form-control form-control-sm form-filter text-center numbermask" name="search_saldo_max" placeholder="Max" />
                                </td>
                                <td style="text-align: center;">
                                    <button class="btn btn-sm btn-outline-default btn-tooltip filter-submit" id="btn_list_table_withdraw_total" title="Search"><i class="fa fa-search"></i></button>
                                    <button class="btn btn-sm btn-outline-warning btn-tooltip filter-cancel" title="Reset"><i class="fa fa-times"></i></button>
                                </td>
                            </tr>
                        </thead>
                        <tbody class="list">
                            <!-- Data Will Be Placed Here -->
                        </tbody>
                    </table>
                </div>
                <div class="mt-3 mx-3">
                    <h5 class="m-0 text-muted">Note : Tanggal Withdraw + 1 Hari</h5>
                </div>
            </div>
        </div>
    </div>
</div>