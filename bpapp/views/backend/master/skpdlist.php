<?php
$cfg_member_type    = config_item('member_type');
$formid             = 'form_add_skpd';
$url                = '';
?>


<div class="header bg-sidebar pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item"><a href="<?php echo base_url('dashboard') ?>"><i class="fas fa-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="#"><?php echo lang('menu_master') ?></a></li>
                            <li class="breadcrumb-item active" aria-current="page"><?php echo $title_page; ?></li>
                        </ol>
                    </nav>
                </div>
                <?php if ($crud_access) { ?>
                    <div class="col-lg-6 col-5 text-right">
                        <?php if ($is_admin) : ?>
                            <a href="javascript:;" data-url="<?= base_url('master/importskpd') ?>" class="btn btn-sm btn-info btn-import-skpd"><i class="fa fa-download mr-1"></i> <?= lang('import_data') ?></a>
                        <?php endif; ?>

                        <a href="javascript:;" data-url="<?php echo base_url('master/saveskpd') ?>" class="btn btn-sm btn-neutral btn-add-skpd"><i class="fa fa-plus mr-1"></i> <?php echo lang('add') . ' ' . $title_page; ?></a>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid mt--6">
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0">Data <?php echo $title_page; ?> </h3>
                        </div>
                    </div>
                </div>
                <div class="table-container">
                    <table class="table align-items-center table-flush" id="list_table_skpd" data-url="<?php echo base_url('master/skpdlistsdata'); ?>">
                        <thead class="thead-light">
                            <tr role="row" class="heading">
                                <th scope="col" style="width: 10px" rowspan="2">#</th>
                                <th scope="col" class="text-center" rowspan="2">Kode</th>
                                <th scope="col" class="text-center" rowspan="2">SKPD</th>
                                <th scope="col" class="text-center" rowspan="2">Type</th>
                                <th scope="col" class="text-center" rowspan="2">Bidang</th>
                                <th scope="col" class="text-center" colspan="3">Urusan </th>
                                <th scope="col" class="text-center" rowspan="2">Urut </th>
                                <th scope="col" class="text-center" rowspan="2" style="width: 30px"><?php echo lang('actions'); ?></th>
                            </tr>
                            <tr>
                                <th class="text-center">U1</th>
                                <th class="text-center">U2</th>
                                <th class="text-center">U3</th>
                            </tr>
                            <tr role="row" class="filter" style="background-color: #f6f9fc">
                                <td></td>
                                <td>
                                    <input type="text" class="form-control form-control-sm form-filter" name="search_code" />
                                </td>
                                <td class="px-1"><input type="text" class="form-control form-control-sm form-filter" name="search_name" /></td>
                                <td class="px-1">
                                    <select name="search_type" class="form-control form-control-sm form-filter">
                                        <option value=""><?php echo lang('select'); ?>...</option>
                                        <?php if (!empty($data_type = config_item('type_skpd'))) : ?>
                                            <?php foreach ($data_type as $key => $value) : ?>
                                                <option value="<?= $key ?>"><?= $value ?></option>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </select>
                                </td>
                                <td class="px-1">
                                    <select name="search_bidang" class="form-control form-control-sm form-filter">
                                        <option value=""><?php echo lang('select'); ?>...</option>
                                        <?php if (!empty($data_bidang)) : ?>
                                            <?php foreach ($data_bidang as $bidang) : ?>
                                                <option value="<?= $bidang->id ?>"><?= $bidang->nama ?></option>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </select>
                                </td>
                                <td class="px-1">
                                    <select name="search_u1" class="form-control form-control-sm form-filter">
                                        <option value=""><?php echo lang('select'); ?>...</option>
                                        <?php if (!empty($data_suburusan)) : ?>
                                            <?php foreach ($data_suburusan as $urusan) : ?>
                                                <option value="<?= $urusan->kode ?>"><?= $urusan->nama ?></option>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </select>
                                </td>
                                <td class="px-1">
                                    <select name="search_u2" class="form-control form-control-sm form-filter">
                                        <option value=""><?php echo lang('select'); ?>...</option>
                                        <?php if (!empty($data_suburusan)) : ?>
                                            <?php foreach ($data_suburusan as $urusan) : ?>
                                                <option value="<?= $urusan->kode ?>"><?= $urusan->nama ?></option>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </select>
                                </td>
                                <td class="px-1">
                                    <select name="search_u3" class="form-control form-control-sm form-filter">
                                        <option value=""><?php echo lang('select'); ?>...</option>
                                        <?php if (!empty($data_suburusan)) : ?>
                                            <?php foreach ($data_suburusan as $urusan) : ?>
                                                <option value="<?= $urusan->kode ?>"><?= $urusan->nama ?></option>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </select>
                                </td>
                                <td class="px-1">
                                    <div class="my-1">
                                        <input type="text" class="form-control form-control-sm form-filter" name="search_urut_min" placeholder="Min" />
                                    </div>
                                    <input type="text" class="form-control form-control-sm form-filter" name="search_urut_max" placeholder="Max" />
                                </td>
                                <td class="text-center">
                                    <button class="btn btn-sm btn-outline-default btn-tooltip filter-submit" title="<?= lang('search') ?>" id="btn_list_table_skpd"><i class="fa fa-search"></i></button>
                                    <button class="btn btn-sm btn-outline-warning btn-tooltip filter-cancel" title="<?= lang('reset') ?>"><i class="fa fa-times"></i></button>
                                </td>
                            </tr>
                        </thead>
                        <tbody class="list">
                            <!-- Data Will Be Placed Here -->
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Action SKPD -->
<div class="modal fade" id="modal-action-skpd" tabindex="-1" role="dialog" aria-labelledby="modal-action-skpd" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header pt-3 pb-1">
                <h5 class="modal-title text-default"><i class="ni ni-book-bookmark mr-1"></i> <span class="modal-title-text font-weight-bold">{{modal_title}}</span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php echo form_open($url, array(
                'id'                => $formid,
                'role'              => 'form',
                'class'             => 'form-horizontal',
                'data-urlurusan'    => base_url('api/master/suburusan'),
                'data-bidang'       => base_url('api/master/bidang'),
                'data-urlskpd'      => base_url('api/master/skpd')
            )); ?>
            <div class="modal-body wrapper-modal px-4 py-1" style="background-color: #f8f9fe">
                <div class="row mt-4">
                    <div class="col-md-6">
                        <div class="form-group row mb-2">
                            <label class="col-md-3 col-form-label form-control-label"><?php echo lang('reg_bidang'); ?> <span class="required">*</span></label>
                            <div class="col-md-9">
                                <select name="reg_select_bidang" v-model="bidang" id="reg_bidang" data-toggle="select2" class="form-control">
                                    <option value="">-- <?= lang('select') . ' ' . lang('reg_bidang') ?> --</option>
                                </select>
                                <input type="hidden" name="reg_bidang" value="">
                            </div>
                        </div>
                        <div class="form-group row mb-2">
                            <label class="col-md-3 col-form-label form-control-label"><?php echo lang('reg_code'); ?> <span class="required">*</span></label>
                            <div class="col-md-9">
                                <input type="text" name="reg_code" id="reg_code" readonly class="form-control text-uppercase" placeholder="<?php echo lang('reg_code'); ?>" autocomplete="off" />
                            </div>
                        </div>
                        <div class="form-group row mb-2">
                            <label class="col-md-3 col-form-label form-control-label"><?php echo lang('reg_name'); ?> <span class="required">*</span></label>
                            <div class="col-md-9">
                                <input type="text" name="reg_name" id="reg_name" class="form-control text-uppercase" placeholder="<?php echo lang('reg_name'); ?>" autocomplete="off" />
                            </div>
                        </div>
                        <div class="form-group row mb-2">
                            <label class="col-md-3 col-form-label form-control-label"><?php echo lang('reg_u1'); ?> <span class="required">*</span></label>
                            <div class="col-md-9">
                                <select id="reg_u1" name="reg_select_u1" data-toggle="select2" class="form-control">
                                    <option value="">-- Pilih Urusan --</option>
                                </select>
                                <input type="hidden" name="reg_u1" value="">
                            </div>
                        </div>
                        <div class="form-group row mb-2">
                            <label class="col-md-3 col-form-label form-control-label"><?php echo lang('reg_u2'); ?></label>
                            <div class="col-md-9">
                                <select id="reg_u2" name="reg_select_u2" data-toggle="select2" class="form-control">
                                    <option value="">-- Pilih Urusan --</option>
                                    <option v-bind:value="item" v-for="item in data_u2">{{item.nama}}</option>
                                </select>
                                <input type="hidden" name="reg_u2" value="">
                            </div>
                        </div>
                        <div class="form-group row mb-2">
                            <label class="col-md-3 col-form-label form-control-label"><?php echo lang('reg_u3'); ?></label>
                            <div class="col-md-9">
                                <select id="reg_u3" name="reg_select_u3" data-toggle="select2" class="form-control">
                                    <option value="">-- Pilih Urusan --</option>
                                </select>
                                <input type="hidden" name="reg_u3" value="">
                            </div>
                        </div>
                        <div class="form-group row mb-2">
                            <label class="col-md-3 col-form-label form-control-label"><?php echo lang('reg_urut'); ?> <span class="required">*</span></label>
                            <div class="col-md-9">
                                <input type="text" name="reg_urut" id="urut" class="form-control numbermask" placeholder="<?php echo lang('reg_urut'); ?>" autocomplete="off" />
                            </div>
                        </div>

                    </div>
                    <div class="col-md-6">
                        <div class="form-group row mb-2 align-items-center">
                            <label class="col-md-3 col-form-label form-control-label"><?php echo lang('reg_make_unit'); ?></label>
                            <div class="col-md-9">
                                <label class="custom-toggle">
                                    <input type="checkbox" class="custom-checked" name="reg_is_unit" id="reg_is_unit" value="0">
                                    <span class="custom-toggle-slider rounded-circle" data-label-off="No" data-label-on="Yes"></span>
                                </label>
                            </div>
                        </div>
                        <div id="unit_section" style="display: none;">
                            <div class="form-group row mb-2">
                                <label class="col-md-3 col-form-label form-control-label"><?php echo lang('reg_unit'); ?> <span class="required">*</span></label>
                                <div class="col-md-9">
                                    <input type="text" name="reg_unit" id="unit" class="form-control numbermask" placeholder="<?php echo lang('reg_unit'); ?>" autocomplete="off" />
                                </div>
                            </div>
                            <div class="form-group row mb-2">
                                <label class="col-md-3 col-form-label form-control-label"><?php echo lang('reg_skpd'); ?> <span class="required">*</span></label>
                                <div class="col-md-9">
                                    <select name="reg_skpd" id="reg_skpd" data-toggle="select2" class="form-control">
                                        <option value="">-- <?= lang('select') . ' ' . lang('reg_skpd') ?> --</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <h5 class="mt-4">Informasi Kepala </h5>
                        <hr class="py-0 mt-2 mb-4">
                        <div class="form-group row mb-2">
                            <label class="col-md-3 col-form-label form-control-label"><?php echo lang('reg_nip'); ?> </label>
                            <div class="col-md-9">
                                <input type="text" id="reg_nip" name="reg_nip" class="form-control numbermask" placeholder="<?php echo lang('head'); ?>" autocomplete="off" />
                            </div>
                        </div>
                        <div class="form-group row mb-2">
                            <label class="col-md-3 col-form-label form-control-label"><?php echo lang('reg_name') . ' ' . lang('head'); ?> </label>
                            <div class="col-md-9">
                                <input type="text" id="reg_head" name="reg_head" class="form-control text-uppercase" placeholder="<?php echo lang('reg_name') . ' ' . lang('head'); ?>" autocomplete="off" />
                            </div>
                        </div>
                        <div class="form-group row mb-2">
                            <label class="col-md-3 col-form-label form-control-label"><?php echo lang('status') . ' ' . lang('head'); ?> </label>
                            <div class="col-md-9">
                                <input type="text" id="reg_status_head" name="reg_status_head" class="form-control numbermask" placeholder="<?php echo lang('status') . ' ' . lang('head'); ?>" autocomplete="off" />
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-outline-warning" data-dismiss="modal"><?php echo lang('back'); ?></button>
                <button type="submit" class="btn btn-sm btn-primary" @click.prevent="save"><span class="fa fa-"></span> Simpan</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<!-- Modal Import SKPD -->
<div class="modal fade" id="modal-import-skpd" tabindex="-1" role="dialog" aria-labelledby="modal-import-skpd" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header pt-3 pb-1">
                <h5 class="modal-title text-default"><i class="ni ni-book-bookmark mr-1"></i> <span class="modal-title-text font-weight-bold"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php echo form_open($url, array('id' => 'form_import_skpd', 'role' => 'form', 'class' => 'form-horizontal')); ?>
            <div class="modal-body wrapper-modal px-4 py-3" style="background-color: #f8f9fe">
                <div class="row mb-2">
                    <div class="col">
                        <div class="alert alert-info" role="alert">
                            <strong>Info!</strong> Untuk format file import dapat didownload <a href="<?= ASSET_PATH . 'upload/import/skpd.xlsx' ?>" target="__blank">disini</a>
                        </div>
                    </div>
                </div>
                <div class="form-group row mb-2">
                    <label class="col-md-3 col-form-label form-control-label"><?php echo lang('reg_file'); ?> <span class="required">*</span></label>
                    <div class="col-md-9">
                        <input type="file" accept=".xlsx" name="reg_file" id="reg_file" class="form-control text-lowercase" placeholder="<?php echo lang('reg_file'); ?>" autocomplete="off" data-url="<?php echo base_url('member/checkusername'); ?>" />
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-outline-warning" data-dismiss="modal"><?php echo lang('back'); ?></button>
                <button type="submit" class="btn btn-sm btn-primary" id="btn-modal-import">Simpan</button>
            </div>

            <?php echo form_close(); ?>
        </div>
    </div>
</div>