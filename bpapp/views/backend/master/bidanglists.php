<?php
$cfg_member_type    = config_item('member_type');
$formid             = 'form_add_bidang';
$url                = '';
?>

<div class="header bg-sidebar pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item"><a href="<?php echo base_url('dashboard') ?>"><i class="fas fa-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="#"><?php echo lang('menu_master') ?></a></li>
                            <li class="breadcrumb-item active" aria-current="page"><?php echo $title_page; ?></li>
                        </ol>
                    </nav>
                </div>
                <?php if ($crud_access) { ?>
                    <div class="col-lg-6 col-5 text-right">
                        <?php if ($is_admin) : ?>
                            <a href="javascript:;" data-url="<?= base_url('master/importbidang') ?>" class="btn btn-sm btn-info btn-import-bidang"><i class="fa fa-download mr-1"></i> <?= lang('import_data') ?></a>
                        <?php endif; ?>

                        <a href="javascript:;" data-url="<?php echo base_url('master/savebidang') ?>" class="btn btn-sm btn-neutral btn-add-bidang"><i class="fa fa-plus mr-1"></i> <?php echo lang('add') . ' ' . $title_page; ?></a>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid mt--6">
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0">Data <?php echo $title_page; ?> </h3>
                        </div>
                    </div>
                </div>
                <div class="table-container">
                    <table class="table align-items-center table-flush" id="list_table_bidang" data-url="<?php echo base_url('master/bidanglistsdata'); ?>">
                        <thead class="thead-light">
                            <tr role="row" class="heading">
                                <th scope="col" style="width: 10px">#</th>
                                <th scope="col" class="text-center">Bidang</th>
                                <th scope="col" class="text-center">Sumua SKPD </th>
                                <th scope="col" class="text-center" style="width: 30px"><?php echo lang('actions'); ?></th>
                            </tr>
                            <tr role="row" class="filter" style="background-color: #f6f9fc">
                                <td></td>
                                <td class="px-1"><input type="text" class="form-control form-control-sm form-filter" name="search_name" /></td>
                                <td class="px-1">
                                    <select name="search_skpd" class="form-control form-control-sm form-filter">
                                        <option value=""><?php echo lang('select'); ?>...</option>
                                        <option value="ya">YA</option>
                                        <option value="no">TIDAK</option>
                                    </select>
                                </td>
                                <td class="text-center">
                                    <button class="btn btn-sm btn-outline-default btn-tooltip filter-submit" title="<?= lang('search') ?>" id="btn_list_table_bidang"><i class="fa fa-search"></i></button>
                                    <button class="btn btn-sm btn-outline-warning btn-tooltip filter-cancel" title="<?= lang('reset') ?>"><i class="fa fa-times"></i></button>
                                </td>
                            </tr>
                        </thead>
                        <tbody class="list">
                            <!-- Data Will Be Placed Here -->
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal Action bidang -->
<div class="modal fade" id="modal-action-bidang" tabindex="-1" role="dialog" aria-labelledby="modal-action-bidang" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header pt-3 pb-1">
                <h5 class="modal-title text-default"><i class="ni ni-book-bookmark mr-1"></i> <span class="modal-title-text font-weight-bold"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php echo form_open($url, array('id' => $formid, 'role' => 'form', 'class' => 'form-horizontal')); ?>
            <div class="modal-body wrapper-modal px-4 py-3" style="background-color: #f8f9fe">
                <div class="form-group row mb-2">
                    <label class="col-md-3 col-form-label form-control-label"><?php echo lang('reg_name'); ?> <span class="required">*</span></label>
                    <div class="col-md-9">
                        <input type="text" name="reg_name" id="reg_name" class="form-control text-uppercase" placeholder="<?php echo lang('reg_name'); ?>" autocomplete="off" />
                    </div>
                </div>
            </div>
            <div class="form-group row mb-2">
                <div class="col-md-3"></div>
                <div class="col-md-9 pl-4">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="reg_all_skpd" name="reg_all_skpd" value="1" />
                        <label class="custom-control-label" for="reg_all_skpd" style="vertical-align: unset; padding-top: 2px">
                            Semua SKPD
                        </label>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-outline-warning" data-dismiss="modal"><?php echo lang('back'); ?></button>
                <button type="submit" class="btn btn-sm btn-primary" id="btn-modal-action">Simpan</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<!-- Modal Import bidang -->
<div class="modal fade" id="modal-import-bidang" tabindex="-1" role="dialog" aria-labelledby="modal-import-bidang" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header pt-3 pb-1">
                <h5 class="modal-title text-default"><i class="ni ni-book-bookmark mr-1"></i> <span class="modal-title-text font-weight-bold"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php echo form_open($url, array('id' => 'form_import_bidang', 'role' => 'form', 'class' => 'form-horizontal')); ?>
            <div class="modal-body wrapper-modal px-4 py-3" style="background-color: #f8f9fe">
                <div class="row mb-2">
                    <div class="col">
                        <div class="alert alert-info" role="alert">
                            <strong>Info!</strong> Untuk format file import dapat didownload <a href="<?= ASSET_PATH . 'upload/import/bidang.xlsx' ?>" target="__blank">disini</a>
                        </div>
                    </div>
                </div>
                <div class="form-group row mb-2">
                    <label class="col-md-3 col-form-label form-control-label"><?php echo lang('reg_file'); ?> <span class="required">*</span></label>
                    <div class="col-md-9">
                        <input type="file" accept=".xlsx" name="reg_file" id="reg_file" class="form-control text-lowercase" placeholder="<?php echo lang('reg_file'); ?>" autocomplete="off" data-url="<?php echo base_url('member/checkusername'); ?>" />
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-outline-warning" data-dismiss="modal"><?php echo lang('back'); ?></button>
                <button type="submit" class="btn btn-sm btn-primary" id="btn-modal-import">Simpan</button>
            </div>

            <?php echo form_close(); ?>
        </div>
    </div>
</div>