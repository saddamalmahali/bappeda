<!DOCTYPE HTML>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title><?= COMPANY_NAME . ' | ' . $title ?></title>

    <link rel="shortcut icon" href="<?= LOGO_IMG ?>" type="image/png">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link rel="stylesheet" href="<?= FE_CSS_PATH ?>bootstrap.min.css">
    <link rel="stylesheet" href="<?= FE_CSS_PATH ?>slick.css">
    <link rel="stylesheet" href="<?= FE_CSS_PATH ?>LineIcons.css">
    <link rel="stylesheet" href="<?= FE_CSS_PATH ?>magnific-popup.css">
    <link rel="stylesheet" href="<?= FE_CSS_PATH ?>default.css">
    <link rel="stylesheet" href="<?= FE_CSS_PATH ?>style.css?ver=<?= CSS_VER_FRONT ?>">
    <link rel="stylesheet" href="<?= FE_CSS_PATH ?>custom.css?ver=<?= CSS_VER_FRONT ?>">
</head>

<body>

    <?php $this->load->view(VIEW_FRONT . $main_content); ?>

    <section id="call-to-action" class="call-to-action">
        <div class="row">
            <div class="col-md-6">
                <img src="<?= FE_IMG_PATH ?>cs.jpg" alt="call-to-action">
            </div>
            <div class="col-md-6 text-center text-md-right">
                <div class="container-fluid">
                    <div class="call-action-content">
                        <h4 class="call-title">PUNYA PERTANYAAN MENGENAI KAIDAH NETWORK?</h4>
                        <p class="text text-primary-color">
                            Kami siap membantu Anda!
                        </p>
                        <div class="row">
                            <div class="col-lg-4">
                                <a class="main-btn rounded-four text-uppercase text-primary-color mt-4" href="#" style="font-size: 15px;display: flex;justify-content: center;align-items: center;">
                                    <i class="fab fa-whatsapp" aria-hidden="true" style="font-size:20px;margin-right:20px"></i> WHATSAPP
                                </a>
                            </div>
                            <div class="col-lg-4">
                                <a class="main-btn rounded-four text-uppercase text-primary-color mt-4" href="#" style="font-size: 15px;display: flex;justify-content: center;align-items: center;">
                                    <i class="fa fa-phone" aria-hidden="true" style="font-size:20px;margin-right:20px"></i> TELEPON
                                </a>
                            </div>
                            <div class="col-lg-4">
                                <a class="main-btn rounded-four text-uppercase text-primary-color mt-4" href="#" style="font-size: 15px;display: flex;justify-content: center;align-items: center;">
                                    <i class="fab fa-instagram" aria-hidden="true" style="font-size:20px;margin-right:20px"></i> INSTAGRAM
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <footer id="footer" class="footer-area">
        <div class="footer-widget">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-6">
                        <div class="row justify-content-center align-items-center footer-link">
                            <div class="col-md-4">
                                <a href="<? base_url() ?>"><img src="<?= LOGO_IMG ?>" alt="Logo" class="logo"></a>
                            </div>
                            <div class="col-md-8">
                                <h6 class="footer-title">Office</h6>
                                <br>
                                <p style="font-size: 15px;">
                                    Komplek Perkantoran Garden Boulevard E-17 <br>
                                    Jl. Raya Kapuk, Kel.Cengkareng Timur<br>
                                    Jakarta Barat
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6">
                        <div class="footer-link">
                            <h6 class="footer-title">Product & Services</h6>
                            <ul>
                                <li><a href="#">Products</a></li>
                                <li><a href="#">Business</a></li>
                                <li><a href="#">Event</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-4 col-sm-5">
                        <div class="footer-link">
                            <h6 class="footer-title">Help & Suuport</h6>
                            <ul>
                                <li><a href="#">Support Center</a></li>
                                <li><a href="#">FAQ</a></li>
                                <li><a href="#">Terms & Conditions</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-7">
                        <div class="footer-newsletter">
                            <h6 class="footer-title">Subscribe Newsletter</h6>
                            <div class="newsletter">
                                <form action="#">
                                    <input type="text" placeholder="Your Email">
                                    <button type="submit"><i class="lni-angle-double-right"></i></button>
                                </form>
                            </div>
                            <p class="text">Subscribe weekly newsletter to stay upto date. We don’t send spam.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="copyright text-center">
                            <p class="text">
                                Copyright ©2021 All rights reserved | <?= COMPANY_NAME ?> - DKI Jakarta Indonesia
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>


    <a class="back-to-top" href="#"><i class="lni-chevron-up"></i></a>

    <script src="<?= FE_JS_PATH ?>vendor/modernizr-3.6.0.min.js"></script>
    <script src="<?= FE_JS_PATH ?>vendor/jquery-1.12.4.min.js"></script>
    <script src="<?= FE_JS_PATH ?>bootstrap.min.js"></script>
    <script src="<?= FE_JS_PATH ?>popper.min.js"></script>
    <script src="<?= FE_JS_PATH ?>ajax-contact.js"></script>
    <script src="<?= FE_JS_PATH ?>jquery.easing.min.js"></script>
    <script src="<?= FE_JS_PATH ?>scrolling-nav.js"></script>
    <script src="<?= FE_JS_PATH ?>validator.min.js"></script>
    <script src="<?= FE_JS_PATH ?>jquery.magnific-popup.min.js"></script>
    <script src="<?= FE_JS_PATH ?>slick.min.js"></script>
    <script src="<?= FE_JS_PATH ?>main.js?ver=<?= JS_VER_FRONT ?>"></script>
</body>

</html>