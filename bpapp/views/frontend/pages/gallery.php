<div class="breadcumb-wrapper breadcumb-layout1 background-image link-inherit space" data-vs-img="<?= FE_IMG_PATH ?>breadcumb-bg.jpg" data-overlay="black" data-opacity="3">
    <div class="container z-index-common py-10">
        <div class="breadcumb-content">
            <h1 class="breadcumb-title sec-title-style1 text-white mt-0 mb-2">Gallery</h1>
            <ul class="breadcumb-menu-style1 text-white">
                <li><a href="<?= base_url() ?>"><i class="fal fa-home text-theme"></i>Home</a></li>
                <li class="active">Gallery</li>
            </ul>
        </div>
    </div>
</div>

<div class="vs-gallery-wrapper vs-gallery-layout1 space">
    <div class="container">
        <div class="row">
            <div class="col-xl-8">
                <div class="row">
                    <div class="col-md-6">
                        <div class="vs-gallery mb-30">
                            <div class="vs-gallery-img image-scale-hover"><a href="javascript:;"><img src="<?= FE_IMG_PATH ?>gallery/gallery-3-1.jpg" alt="gallery Image" class="w-100"></a></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="vs-gallery mb-30">
                            <div class="vs-gallery-img image-scale-hover"><a href="javascript:;"><img src="<?= FE_IMG_PATH ?>gallery/gallery-3-2.jpg" alt="gallery Image" class="w-100"></a></div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="vs-gallery mb-30">
                            <div class="vs-gallery-img image-scale-hover"><a href="javascript:;"><img src="<?= FE_IMG_PATH ?>gallery/gallery-3-4.jpg" alt="gallery Image" class="w-100"></a></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xl-4">
                <div class="vs-gallery mb-30">
                    <div class="vs-gallery-img image-scale-hover"><a href="javascript:;"><img src="<?= FE_IMG_PATH ?>gallery/gallery-3-3.jpg" alt="gallery Image" class="w-100"></a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xl-4">
                <div class="vs-gallery mb-30">
                    <div class="vs-gallery-img image-scale-hover"><a href="javascript:;"><img src="<?= FE_IMG_PATH ?>gallery/gallery-3-5.jpg" alt="gallery Image" class="w-100"></a>
                    </div>
                </div>
            </div>
            <div class="col-xl-8">
                <div class="vs-gallery mb-30">
                    <div class="vs-gallery-img image-scale-hover"><a href="javascript:;"><img src="<?= FE_IMG_PATH ?>gallery/gallery-3-6.jpg" alt="gallery Image" class="w-100"></a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="vs-gallery mb-30">
                            <div class="vs-gallery-img image-scale-hover"><a href="javascript:;"><img src="<?= FE_IMG_PATH ?>gallery/gallery-3-7.jpg" alt="gallery Image" class="w-100"></a></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="vs-gallery mb-30">
                            <div class="vs-gallery-img image-scale-hover"><a href="javascript:;"><img src="<?= FE_IMG_PATH ?>gallery/gallery-3-8.jpg" alt="gallery Image" class="w-100"></a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>