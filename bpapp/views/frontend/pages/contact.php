<header class="header-area contact">

    <?php $this->load->view(VIEW_FRONT . 'pages/components/nav.php'); ?>

    <div id="home" class="header-hero bg_cover" style="background-image: url(<?= FE_IMG_PATH ?>header/contact.jpg)">
        <div class="header-shape">
            <img src="<?= FE_IMG_PATH ?>header-shape.svg" alt="shape">
        </div>
    </div>
</header>


<section class="services-area">
    <div class="container">
        <div class="section-title text-center mb-5">
            <h4 class="title">GO AHEAD TALK TO US</h4>
        </div>
        <div class="row">
            <div class="col-lg-5 mb-5" style="justify-content: center;align-items: center;display: flex;">
                <img src="<?= FE_IMG_PATH ?>bg-contact.png">
            </div>
            <div class="col-lg-7 mb-5">
                <form id="contact">
                    <div class="form-group mb-4">
                        <input type="text" class="form-control" placeholder="Enter email" style="border-radius: 20px !important;background: #bcead6;">
                    </div>
                    <div class="form-group mb-4">
                        <input type="text" class="form-control" placeholder="Password" style="border-radius: 20px !important;background: #bcead6;">
                    </div>
                    <div class="form-group mb-4">
                        <textarea class="form-control" rows="3" placeholder="Password" style="border-radius: 20px !important;background: #bcead6;"></textarea>
                    </div>
                    <a class="main-btn rounded-three text-uppercase" href="#" style="background: #0b7346;color: white;">SUBMIT</a>
                </form>
            </div>
        </div>
    </div>
</section>