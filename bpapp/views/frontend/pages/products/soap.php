<header class="header-area">
    <?php $this->load->view(VIEW_FRONT . 'pages/components/nav.php'); ?>
    <div id="home" class="header-hero bg_cover" style="background-image: url(<?= FE_IMG_PATH ?>header/soap.jpg)"></div>
</header>

<div class="section-title text-center py-4" style="background: #7DC604;">
    <h4 class="title text-uppercase text-white">k-dha soap transparent</h4>
</div>

<img src="<?= FE_IMG_PATH ?>product/soap/bg.jpg" width="100%">

<section id="service" class="services-area pb-0">

    <div class="container">
        <div class="row">
            <div class="col-lg-12 mb-5" style="justify-content: center;align-items: center;display: flex;">
                <div class="section-title">
                    <p class="text mt-0">
                        Sabun yang diracik untuk masalah kulit berminyak dan berjerawat. Bahan baku yang digunakan dalam pembuatan produk ini adalah minyak kelapa. Manfaat yang terdapat dari minyak kelapa dapat melembabkan serta menghaluskan kulit, selain itu vitam E yang terdapat dari minyak kelapa dapat melindungi kulit dari kerusakan akibat radikal bebas.
                        <br><br>
                        Kandungan Bahan aktif tea tree oil sangat bermanfaat bagi pemilik wajah berminyak dan berjerawat karena tea tree oil mampu mengurangi produksi minyak yang berlebih, sehingga jumlah jerawat dan tingkat radangnya akan berkurang. Meski mengurangi produksi minyak pada wajah, sabun ini tidak menyebabkan kulit wajah menjadi kering. Selain untuk wajah, sabun ini juga dapat digunakan untuk mengurangi jerawat pada tubuh dan dapat mengurangi bau badan yang tidak sedap.
                    </p>
                </div>
            </div>
            <div class="col-lg-12 mb-5">
                <div class="section-title text-center my-4">
                    <h5 class="title text-gray" style="font-size: 30px">Manfaat TREE OIL</h5>
                </div>
                <div class="row">
                    <div class="col-md-3 col-6 mb-3 text-center">
                        <img src="<?= FE_IMG_PATH ?>product/soap/tree1.png" style="padding: 25px;">
                        <p class="text-uppercase">Membantu mengurangi jumlah jerawat dan tingkat keparahannya</p>
                    </div>
                    <div class="col-md-3 col-6 mb-3 text-center">
                        <img src="<?= FE_IMG_PATH ?>product/soap/tree2.png" style="padding: 25px;">
                        <p class="text-uppercase">Membantu mengatasi kulit yang merah dan meradang</p>
                    </div>
                    <div class="col-md-3 col-6 mb-3 text-center">
                        <img src="<?= FE_IMG_PATH ?>product/soap/tree3.png" style="padding: 25px;">
                        <p class="text-uppercase">Membantu mengurangi produksi minyak berlebih</p>
                    </div>
                    <div class="col-md-3 col-6 mb-3 text-center">
                        <img src="<?= FE_IMG_PATH ?>product/soap/tree4.png" style="padding: 25px;">
                        <p class="text-uppercase">menyamarkan flek kulit</p>
                    </div>
                    <div class="col-md-3 col-6 mb-3 text-center">
                        <img src="<?= FE_IMG_PATH ?>product/soap/tree5.png" style="padding: 25px;">
                        <p class="text-uppercase">Membantu mengatasi gatal pada kulit</p>
                    </div>
                    <div class="col-md-3 col-6 mb-3 text-center">
                        <img src="<?= FE_IMG_PATH ?>product/soap/tree6.png" style="padding: 25px;">
                        <p class="text-uppercase">Membantu mengatasi kulit kering dan meringankan eksim ( ruam kulit)</p>
                    </div>
                    <div class="col-md-3 col-6 mb-3 text-center">
                        <img src="<?= FE_IMG_PATH ?>product/soap/tree7.png" style="padding: 25px;">
                        <p class="text-uppercase">Membantu mengatasi kutu air</p>
                    </div>
                    <div class="col-md-3 col-6 mb-3 text-center">
                        <img src="<?= FE_IMG_PATH ?>product/soap/tree8.png" style="padding: 25px;">
                        <p class="text-uppercase">Membantu mengurangi bau badan tidak sedap</p>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<img src="<?= FE_IMG_PATH ?>product/soap/bg2.jpg" width="100%">

<section class="services-area py-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title">
                    <p class="text mt-0">
                        Temulawak sudah dikenal dalam khasiatnya mencerahkan kulit. Brightening Soap ini diproduksi dengan menggunakan bahan baku minyak kelapa. Kandungan dari minyak kelapa dapat melembabkan serta menghaluskan kulit. Selain itu Vitamin E yang terkandung dalam minyak kelapa dapat menjaga kulit dari kerusakan akibat radikal bebas. bahan aktif yang digunakan sabun ini adalah ekstrak temulawak. Temulawak berfungsi untuk meremajakan kulit, mencerahkan wajah , mengencangkan kulit dan dapat mengecilkan pori-pori yang besar. Temulawak juga dapat menjadi anti oksidan alami kulit. Penggunaan rutin produk ini dapat membantu menghilangkan bekas luka.
                    </p>
                </div>
            </div>
            <div class="col-lg-12 mb-5">
                <div class="section-title text-center my-4">
                    <h5 class="title text-gray" style="font-size: 30px">Manfaat JAVA TUMERIC EXTRACT</h5>
                </div>
                <div class="row">
                    <div class="col-md-3 col-6 mb-3 text-center">
                        <img src="<?= FE_IMG_PATH ?>product/soap/java1.png" style="padding: 25px;">
                        <p class="text-uppercase">Membantu meremajakan & menghaluskan kulit</p>
                    </div>
                    <div class="col-md-3 col-6 mb-3 text-center">
                        <img src="<?= FE_IMG_PATH ?>product/soap/java2.png" style="padding: 25px;">
                        <p class="text-uppercase">membantu menyamarkan noda hitam</p>
                    </div>
                    <div class="col-md-3 col-6 mb-3 text-center">
                        <img src="<?= FE_IMG_PATH ?>product/soap/java3.png" style="padding: 25px;">
                        <p class="text-uppercase">membantu melembabkan kulit</p>
                    </div>
                    <div class="col-md-3 col-6 mb-3 text-center">
                        <img src="<?= FE_IMG_PATH ?>product/soap/java4.png" style="padding: 25px;">
                        <p class="text-uppercase">membantu mencerahkan kulit</p>
                    </div>
                    <div class="col-md-3 col-6 mb-3 text-center">
                        <img src="<?= FE_IMG_PATH ?>product/soap/java5.png" style="padding: 25px;">
                        <p class="text-uppercase">Membantu mengecilkan pori-pori</p>
                    </div>
                    <div class="col-md-3 col-6 mb-3 text-center">
                        <img src="<?= FE_IMG_PATH ?>product/soap/java6.png" style="padding: 25px;">
                        <p class="text-uppercase">Membantu menghilangkan bekas luka</p>
                    </div>
                    <div class="col-md-3 col-6 mb-3 text-center">
                        <img src="<?= FE_IMG_PATH ?>product/soap/java7.png" style="padding: 25px;">
                        <p class="text-uppercase">Membantu mengencangkan kulit</p>
                    </div>
                    <div class="col-md-3 col-6 mb-3 text-center">
                        <img src="<?= FE_IMG_PATH ?>product/soap/java8.png" style="padding: 25px;">
                        <p class="text-uppercase">Menjadi antioksidan yang alami</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<img src="<?= FE_IMG_PATH ?>product/soap/bg3.jpg" width="100%">

<section class="services-area py-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 mb-5">
                <div class="section-title text-center my-4">
                    <h5 class="title text-gray text-uppercase" style="font-size: 30px">minyak kelapa (COCONUT NUCIFERA OIL)</h5>
                    <h5 class="title text-gray text-uppercase" style="font-size: 20px">dengan berbagai manfaat diantaranya:</h5>
                </div>
                <div class="row">
                    <div class="col-6 mb-3 text-center">
                        <img src="<?= FE_IMG_PATH ?>product/soap/lactic.png" style="padding: 25px;">
                        <h5 class="title text-gray text-uppercase mb-3" style="font-size: 20px">Lactic acid</h5>
                        <p class="text-left">Asam laurat berkhasiat sebagai anti mikroba alami, Ketika asam laurat dalam minyak kelapa bereaksi dengan alkali akan menghasilkan gliserin yang berfungsi untuk menghaluskan dan melembabkan kulit.</p>
                    </div>
                    <div class="col-6 mb-3 text-center">
                        <img src="<?= FE_IMG_PATH ?>product/soap/vite.png" style="padding: 25px;">
                        <h5 class="title text-gray text-uppercase mb-3" style="font-size: 20px">Lactic acid</h5>
                        <p class="text-left">vitamin E dapat membantu melembabkan kulit kering Memiliki kandungan antioksidan yang tinggi sehingga dipercaya dapat melindungi kulit dari kerusakan akibat radikal bebas merangsang tubuh untuk memproduksi kolagen yang dapat mencegah munculnya keriput</p>
                    </div>
                </div>
            </div>

            <center>
                <img src="<?= FE_IMG_PATH ?>safety.png" width="100%">
                <p class="text-center mt-5">Konsultasikan terlebih dahulu masalah kulit anda agar kami dapat memberikan kombinasi poduk terbaik untuk anda.</p>
            </center>

        </div>
    </div>
</section>