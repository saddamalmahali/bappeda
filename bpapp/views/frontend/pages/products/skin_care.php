<header class="header-area">

    <?php $this->load->view(VIEW_FRONT . 'pages/components/nav.php'); ?>

    <div id="home" class="header-hero bg_cover" style="background-image: url(<?= FE_IMG_PATH ?>header/product.jpg)">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="header-content text-center text-md-left" style="padding-top: 200px;">
                        <p class="text text-primary-color mt-0 mb-2">
                            Bersama kami jadikan
                        </p>
                        <h1 class="mt-0 mb-2">KELUARGA</h1>
                        <p class="text text-primary-color mt-0 mb-2">
                            yang penuh bahagia
                        </p>
                        <ul class="header-btn rounded-buttons">
                            <li><a class="main-btn rounded-three text-uppercase" href="#">menjadi member</a></li>
                            <li><a class="main-btn rounded-four text-uppercase text-primary-color" href="<?php echo base_url('store'); ?>">produk kami</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<div class="section-title text-center py-4" style="background: #7DC604;">
    <h4 class="title text-uppercase text-white">k-dha skin care series</h4>
</div>

<section id="service" class="services-area pb-0">

    <div class="container">
        <div class="row">
            <div class="col-lg-4 mb-5" style="justify-content: center;align-items: center;display: flex;">
                <img src="<?= FE_IMG_PATH ?>product/skin_care/skin-care.png">
            </div>
            <div class="col-lg-8 mb-5" style="justify-content: center;align-items: center;display: flex;">
                <div class="section-title">
                    <p class="text mt-0">
                        K-DHA Skin Care Series terdiri dari beberapa jenis, yaitu Glow Serum, Lightener Serum, krim pagi dan malam serta Body Glow Spray.
                        <br><br>
                        Kandungan utama dari skin care series ini adalah saffron. Saffron dikenal merupakan rempah yang kaya akan manfaat, baik bagi kesehatan ataupun untuk kulit. Manfaat saffron akan sangat terasa bagi usia 25 tahun keatas karena dapan mempertahankan kulit agar tetap muda dan merona. Flek hitam juga dapat memudar karena kandungan ini.
                        <br><br>
                        Selain saffron, K-DHA Skin Care series juga memiliki kandungan lain seperti Vitamin C, Beta Glucan, Aloe vera, Tuberose, Birabilis Jalapa dan Pricky pear. Miliki kulit wajah yang sehat dan tampak muda dengan K-DHA Skincare Series. Gunakan produk K-DHA Skin Care series secara rutin untuk memberikan hasil yang terbaik
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="service" class="services-area bg_cover" style="background-image: url(<?= FE_IMG_PATH ?>product/skin_care/bg-skin-care.png)">
    <div class="container">
        <div class="section-title text-center mb-5">
            <h4 class="title text-white">MANFAAT PRODUK</h4>
        </div>
        <div class="row">
            <div class="col-md-4 mb-5 text-center">
                <img src="<?= FE_IMG_PATH ?>product/skin_care/manfaat1.png">
                <h5 class="text-white mt-4">MEMPERBAIKI KULIT IRITASI, KULIT RUSAK, MENINGKATKAN DAYA TAHAN KULIT</h5>
            </div>
            <div class="col-md-4 mb-5 text-center">
                <img src="<?= FE_IMG_PATH ?>product/skin_care/manfaat2.png">
                <h5 class="text-white mt-4">MENCERAHKAN KULIT, MEMUDARKAN PIGMEN GELAP</h5>
            </div>
            <div class="col-md-4 mb-5 text-center">
                <img src="<?= FE_IMG_PATH ?>product/skin_care/manfaat3.png">
                <h5 class="text-white mt-4">MEMPERTAHAN KULIT TETAP MUDA, TAMPAK MERAH MERONA</h5>
            </div>
        </div>
    </div>
</section>

<section id="manfaat" class="services-area">
    <div class="container">
        <div class="section-title text-center mb-5">
            <h4 class="title">Bahan AKTIF yang Digunakan</h4>
        </div>
        <div class="row">
            <div class="col-lg-12 mb-5">
                <img src="<?= FE_IMG_PATH ?>product/skin_care/saffron.jpg" width="100%">
                <p class="text mt-4">
                    Rempah termahal di dunia ini berbentuk untaian-untaian halus mirip benang, yang berasal dari putik bunga Crocus sativus. Cara memanennya yang terbilang sulit mengakibatkan harga saffron cukup mahal, 450 gram saffron berkualitas baik dihargai US$5000 atau sekitar Rp70 juta. Dibutuhkan lahan seluas 1 acre (4.046,86 m²) dengan 150.000 bunga untuk menghasilkan 1 KG saffron. Sebanyak 94% produksi saffron berasal dari Iran dan sisanya berada di India (kashmir), Spanyol dan Yunani. Saffron hanya berbunga seminggu sepanjang tahun.
                    <br><br>
                    Senyawa aktif dalam saffron bisa membantu mencegah kerusakan kulit akibat radiasi UV, UVA dan UVB. Crocin juga bersifat antioksidan, yang artinya dapat membantu mengurangi stres oksidatif dengan menetralkan radikal bebas. Sebuah penelitian menemukan, senyawa aktif saffron, termasuk crocin, dapat menurunkan kadar melanin
                    Safron mengandung lebih dari 150 senyawa volatil (mudah menguap) penghasil aroma ditambah berbagai senyawa aktif nonvolatil (tidak mudah menguap), dan banyak di antaranya merupakan karotenoid, termasuk zeaksantin, likopena, dan berbagai α- dan β-karoten
                </p>
                <div class="section-title text-center my-4">
                    <h5 class="title text-gray" style="font-size: 30px">Manfaat Saffron</h5>
                </div>
                <div class="row">
                    <div class="col-md-3 col-6 mb-5 text-center">
                        <img src="<?= FE_IMG_PATH ?>product/skin_care/saffron1.png" style="padding: 25px;">
                        <p class="text-uppercase">melindungi dari sinar uv-a</p>
                    </div>
                    <div class="col-md-3 col-6 mb-5 text-center">
                        <img src="<?= FE_IMG_PATH ?>product/skin_care/saffron2.png" style="padding: 25px;">
                        <p class="text-uppercase">anti oksidan stress</p>
                    </div>
                    <div class="col-md-3 col-6 mb-5 text-center">
                        <img src="<?= FE_IMG_PATH ?>product/skin_care/saffron3.png" style="padding: 25px;">
                        <p class="text-uppercase">menyembuhkan dan memudarkan luka</p>
                    </div>
                    <div class="col-md-3 col-6 mb-5 text-center">
                        <img src="<?= FE_IMG_PATH ?>product/skin_care/saffron4.png" style="padding: 25px;">
                        <p class="text-uppercase">menyamarkan flek kulit</p>
                    </div>
                </div>
            </div>

            <div class="col-lg-12 mb-5">
                <img src="<?= FE_IMG_PATH ?>product/skin_care/tuberose.jpg" width="100%">
                <p class="text mt-4">
                    Nama tuberosa menunjukkan bahwa tumbuhan ini memiliki umbi(tuber). Saat ini di ketahui memiliki sekitar 12 species dari genus polianthes. Tanaman ini mampu tumbuh baik di daerah tropis dan sub tropis. Bunga sedap malam berasal dari Meksiko. Dahulu bangsa Aztec menyebut tanaman ini dengan sebutan Omixochitl, artinya adalah bunga tulang.
                    <br><br>
                    Bunga sedap malam mengandung bahan aktif minyak atsiri, dengan kandungan bahan aktif euganol, nerol, farnesol, dan geraniol, yang dapat mengatasi bakteri dan penyumbatan pada pori-pori dan membantu proses sintesis kolagen.
                </p>
                <div class="section-title text-center my-4">
                    <h5 class="title text-gray" style="font-size: 30px">Manfaat TUBEROSE EXTRACT</h5>
                </div>
                <div class="row">
                    <div class="col-md-3 col-6 mb-5 text-center">
                        <img src="<?= FE_IMG_PATH ?>product/skin_care/tuberose1.png" style="padding: 25px;">
                        <p class="text-uppercase">anti kerut dan mengencangkan kulit</p>
                    </div>
                    <div class="col-md-3 col-6 mb-5 text-center">
                        <img src="<?= FE_IMG_PATH ?>product/skin_care/tuberose2.png" style="padding: 25px;">
                        <p class="text-uppercase">mengurangi kantung mata</p>
                    </div>
                    <div class="col-md-3 col-6 mb-5 text-center">
                        <img src="<?= FE_IMG_PATH ?>product/skin_care/tuberose3.png" style="padding: 25px;">
                        <p class="text-uppercase">menjadikan kulit kencang</p>
                    </div>
                    <div class="col-md-3 col-6 mb-5 text-center">
                        <img src="<?= FE_IMG_PATH ?>product/skin_care/tuberose4.png" style="padding: 25px;">
                        <p class="text-uppercase">Mencerahkan kulit</p>
                    </div>
                    <div class="col-md-3"></div>
                    <div class="col-md-3 col-6 mb-5 text-center">
                        <img src="<?= FE_IMG_PATH ?>product/skin_care/tuberose5.png" style="padding: 25px;">
                        <p class="text-uppercase">MELEMBABKAN KULIT</p>
                    </div>
                    <div class="col-md-3 col-6 mb-5 text-center">
                        <img src="<?= FE_IMG_PATH ?>product/skin_care/tuberose6.png" style="padding: 25px;">
                        <p class="text-uppercase">memperbaiki peredaran darah mikro untuk Nutrisi kulit</p>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>

            <div class="col-lg-12 mb-5">
                <img src="<?= FE_IMG_PATH ?>product/skin_care/aloe.jpg" width="100%">
                <p class="text mt-4">
                    Lidah buaya (Aloe vera) berasal dari Jazirah Arab. Dedaunannya berdaging tebal, berwarna hijau atau hijau keabuan. Bunga-bunganya tumbuh pada musim panas. Tanaman ini banyak dibudidayakan di kawasan tropis dan subtropis, serta kawasan-kawasan kering di Benua Amerika, Asia, dan Australia.
                    <br><br>
                    Aloe vera mengandung mineral yang cukup lengkap, seperti kalsium, sodium, magnesium, kromium, selenium, zinc , dan potassium mangan. Daging lidah buaya mengandung vitamin B, Aloe vera memiliki kandungan vitamin B12 yang cukup tinggi.
                    Enzim dalam aloe vera yang disebut Bradykinase berfungsi sebagai zat antiinflamasi (Peradangan).
                </p>
                <div class="section-title text-center my-4">
                    <h5 class="title text-gray" style="font-size: 30px">Manfaat ALOE VERA</h5>
                </div>
                <div class="row">
                    <div class="col-md-3 col-6 mb-5 text-center">
                        <img src="<?= FE_IMG_PATH ?>product/skin_care/aloe1.png" style="padding: 25px;">
                        <p class="text-uppercase">anti inflamasi</p>
                    </div>
                    <div class="col-md-3 col-6 mb-5 text-center">
                        <img src="<?= FE_IMG_PATH ?>product/skin_care/aloe2.png" style="padding: 25px;">
                        <p class="text-uppercase">anti oksidan</p>
                    </div>
                    <div class="col-md-3 col-6 mb-5 text-center">
                        <img src="<?= FE_IMG_PATH ?>product/skin_care/aloe3.png" style="padding: 25px;">
                        <p class="text-uppercase">mengurangi jerawat & Infeksi</p>
                    </div>
                    <div class="col-md-3 col-6 mb-5 text-center">
                        <img src="<?= FE_IMG_PATH ?>product/skin_care/aloe4.png" style="padding: 25px;">
                        <p class="text-uppercase">mempercepat penyembuhan luka</p>
                    </div>
                </div>
            </div>

            <div class="col-lg-12 mb-5">
                <img src="<?= FE_IMG_PATH ?>product/skin_care/mirabilis.jpg" width="100%">
                <p class="text mt-4">
                    Disebut sebagai bunga pukul empat karena biasanya bunga ini mekar saat pukul empat sore.
                    Bunga pukul empat berasal Amerika Selatan, yang mempunyai banyak warna, di antaranya merah, kuning, dan putih.
                    <br><br>
                    Bunga pukul empat memiliki beberapa kandungan kimia. Akar mengandung betaxanthins, trigonelline. Daun mengandung saponin, flavonoid, dan tannin berguna mengatasi gangguan kulit dan membuat wajah lebih cerah dan sehat.
                    Biji mengandung zat tepung-lemak, zat asam lemak, dan zat asam minyak. zat tepung yang terdapat dalam Mirabilis jalapa juga ampuh mengatasi jerawat.
                </p>
                <div class="section-title text-center my-4">
                    <h5 class="title text-gray" style="font-size: 30px">Manfaat MIRABILIS JALAPA</h5>
                </div>
                <div class="row">
                    <div class="col-md-3 col-6 mb-5 text-center">
                        <img src="<?= FE_IMG_PATH ?>product/skin_care/mirabilis1.png" style="padding: 25px;">
                        <p class="text-uppercase">mencegah pertumbuhan microbial</p>
                    </div>
                    <div class="col-md-3 col-6 mb-5 text-center">
                        <img src="<?= FE_IMG_PATH ?>product/skin_care/mirabilis2.png" style="padding: 25px;">
                        <p class="text-uppercase">memperkecil pori-pori kulit (astringent)</p>
                    </div>
                    <div class="col-md-3 col-6 mb-5 text-center">
                        <img src="<?= FE_IMG_PATH ?>product/skin_care/mirabilis3.png" style="padding: 25px;">
                        <p class="text-uppercase">melembutkan kulit</p>
                    </div>
                    <div class="col-md-3 col-6 mb-5 text-center">
                        <img src="<?= FE_IMG_PATH ?>product/skin_care/mirabilis4.png" style="padding: 25px;">
                        <p class="text-uppercase">melindungi kulit dari faktor eksternal</p>
                    </div>
                </div>
            </div>

            <div class="col-lg-12 mb-5">
                <img src="<?= FE_IMG_PATH ?>product/skin_care/prickly.jpg" width="100%">
                <p class="text mt-4">
                    Tanaman prickly pear atau buah pir berduri menyukai tanah yang berpasir dan kasar seperti di gurun atau lereng berbatu. Buah yang banyak ditemukan di Sonoran Desert, Arizona ini ternyata mengandung amino acids, vitamin B, kalsium, betakarotin, magnesium, dan zat besi yang bisa memberikan banyak kebaikan bagi kulit.
                    <br><br>
                    Minyak prickly pear membutuhkan waktu tiga hingga empat hari untuk mengekstrak buah dari bijinya. Proses pengerjaan manual yang memakan waktu cukup panjang menjadikannya salah satu minyak termahal di industri kecantikan. Minyak ini tercatat sebagai beauty oil dengan kandungan vitamin E paling tinggi yang melampaui minyak argan. Diperkaya pula dengan linoleic acid untuk anti-inflamasi dan vitamin K.
                </p>
                <div class="section-title text-center my-4">
                    <h5 class="title text-gray" style="font-size: 30px">Manfaat PRICKLY PEAR</h5>
                </div>
                <div class="row">
                    <div class="col-md-3 col-6 mb-5 text-center">
                        <img src="<?= FE_IMG_PATH ?>product/skin_care/prickly1.png" style="padding: 25px;">
                        <p class="text-uppercase">mengandung berbagai vitamin & mineral penting untuk kulit</p>
                    </div>
                    <div class="col-md-3 col-6 mb-5 text-center">
                        <img src="<?= FE_IMG_PATH ?>product/skin_care/prickly2.png" style="padding: 25px;">
                        <p class="text-uppercase">anti oksidan</p>
                    </div>
                    <div class="col-md-3 col-6 mb-5 text-center">
                        <img src="<?= FE_IMG_PATH ?>product/skin_care/prickly3.png" style="padding: 25px;">
                        <p class="text-uppercase">anti kanker</p>
                    </div>
                    <div class="col-md-3 col-6 mb-5 text-center">
                        <img src="<?= FE_IMG_PATH ?>product/skin_care/prickly4.png" style="padding: 25px;">
                        <p class="text-uppercase">mencegah iritasi dan infeksi karena virus</p>
                    </div>
                </div>
            </div>

            <div class="col-lg-12 mb-5">
                <img src="<?= FE_IMG_PATH ?>product/skin_care/beta.jpg" width="100%">
                <p class="text mt-4">
                    Beta glucan adalah polikasarida yang dapat ditemui pada dinding sel tumbuhan seperti ragi, bakteri, jamur, rumput laut serta biji-bijian pada gandum.
                    <br><br>
                    Beta glucan tidak dihasilkan alami oleh tubuh manusia, namun sangat dibutuhkan untuk kulit awet muda. Kerja beta-glucan adalah dengan meningkatkan sistem imun dan melawan patogen. Patogen sendiri merupakan penyebab penyakit, seperti virus, bakteri, jamur, dan parasit.
                </p>
                <div class="section-title text-center my-4">
                    <h5 class="title text-gray" style="font-size: 30px">Manfaat BETA GLUCAN</h5>
                </div>
                <div class="row">
                    <div class="col-md-3 col-6 mb-5 text-center">
                        <img src="<?= FE_IMG_PATH ?>product/skin_care/beta1.png" style="padding: 25px;">
                        <p class="text-uppercase">meningkatkan sistem imun melawan pantogen (Bakteri, jamur, virus, parasit)</p>
                    </div>
                    <div class="col-md-3 col-6 mb-5 text-center">
                        <img src="<?= FE_IMG_PATH ?>product/skin_care/beta2.png" style="padding: 25px;">
                        <p class="text-uppercase">meremajakan kulit sehingga terlihat merona</p>
                    </div>
                    <div class="col-md-3 col-6 mb-5 text-center">
                        <img src="<?= FE_IMG_PATH ?>product/skin_care/beta3.png" style="padding: 25px;">
                        <p class="text-uppercase">membantu kulit menyimpan kelembaban</p>
                    </div>
                    <div class="col-md-3 col-6 mb-5 text-center">
                        <img src="<?= FE_IMG_PATH ?>product/skin_care/beta4.png" style="padding: 25px;">
                        <p class="text-uppercase">merangsang pertumbuhan kolagen kulit</p>
                    </div>
                </div>
            </div>

            <div class="col-lg-12 mb-5">
                <img src="<?= FE_IMG_PATH ?>product/skin_care/vitc.jpg" width="100%">
                <p class="text mt-4">
                    Vitamin C atau dikenal juga dengan nama asam askorbat, adalah antioksidan terbaik yang dikenal memiliki manfaat untuk meningkatkan kekebalan tubuh. Termasuk dalam salah satu jenis vitamin yang larut dalam air dan memiliki peranan penting dalam menangkal berbagai penyakit. Vitamin C termasuk golongan vitamin antioksidan yang mampu menangkal berbagai radikal bebas ekstraselular
                </p>
                <div class="section-title text-center my-4">
                    <h5 class="title text-gray" style="font-size: 30px">Manfaat VITAMIN C</h5>
                </div>
                <div class="row">
                    <div class="col-md-3 col-6 mb-5 text-center">
                        <img src="<?= FE_IMG_PATH ?>product/skin_care/vitc1.png" style="padding: 25px;">
                        <p class="text-uppercase">mencerahkan kulit</p>
                    </div>
                    <div class="col-md-3 col-6 mb-5 text-center">
                        <img src="<?= FE_IMG_PATH ?>product/skin_care/vitc2.png" style="padding: 25px;">
                        <p class="text-uppercase">meningkatkan kolagen kulit</p>
                    </div>
                    <div class="col-md-3 col-6 mb-5 text-center">
                        <img src="<?= FE_IMG_PATH ?>product/skin_care/vitc3.png" style="padding: 25px;">
                        <p class="text-uppercase">menggunakan vitamin c ‘ascorbic acid’ yang mampu diserap kulit manusia</p>
                    </div>
                    <div class="col-md-3 col-6 mb-5 text-center">
                        <img src="<?= FE_IMG_PATH ?>product/skin_care/vitc4.png" style="padding: 25px;">
                        <p class="text-uppercase">Menurunkan risiko kanker kulit</p>
                    </div>
                </div>
            </div>

            <center>
                <img src="<?= FE_IMG_PATH ?>safety.png" width="100%">
                <p class="text-center mt-5">Konsultasikan terlebih dahulu masalah kulit anda agar kami dapat memberikan kombinasi poduk terbaik untuk anda.</p>
            </center>

        </div>
    </div>
</section>

<!-- <section id="certification" class="pricing-area">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="section-title text-center pb-10">
                    <h4 class="title">Sertifikasi Lengkap KAIDAH NETWORK</h4>
                    <p class="text">
                        KAIDAH NETWORK telah memiliki sertifikasi lengkap untuk menjalankan sistem penjualan langsung di Indonesia, selain telah bergabungnya KAIDAH NETWOK di Asosiasi Perusahaan Penjualan Langsung (AP2LI), produk yang kami pasarkan juga telah memiliki sertifikasi BPOM dan Halal MUI
                    </p>
                </div>
            </div>
        </div>
        <div class="row justify-content-center mt-5">
            <div class="col-md-12 text-center">
                <img src="<?= FE_IMG_PATH ?>bpom.png" style="padding:20px;width:130px;height:130px;" />
                <img src="<?= FE_IMG_PATH ?>mui.png" style="padding:20px;width:130px;height:130px;" />
                <img src="<?= FE_IMG_PATH ?>apli.jpg" style="padding:20px;border-radius: 50%;width:130px;height:130px;" />
            </div>
        </div>
    </div>
</section> -->