<header class="header-area">

    <?php $this->load->view(VIEW_FRONT . 'pages/components/nav.php'); ?>

    <div id="home" class="header-hero bg_cover" style="background-image: url(<?= FE_IMG_PATH ?>header/company.jpg)">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="header-content text-center text-md-left" style="padding-top: 200px;">
                        <p class="text text-primary-color mt-0 mb-2">
                            Bersama kami jadikan
                        </p>
                        <h1 class="mt-0 mb-2">KELUARGA</h1>
                        <p class="text text-primary-color mt-0 mb-2">
                            yang penuh bahagia
                        </p>
                        <ul class="header-btn rounded-buttons">
                            <li><a class="main-btn rounded-three text-uppercase" href="#">menjadi member</a></li>
                            <li><a class="main-btn rounded-four text-uppercase text-primary-color" href="<?php echo base_url('store'); ?>">produk kami</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-shape">
            <img src="<?= FE_IMG_PATH ?>header-shape.svg" alt="shape">
        </div>
    </div>
</header>

<section id="service" class="services-area pb-0">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 mb-5" style="justify-content: center;align-items: center;display: flex;">
                <img src="<?= FE_IMG_PATH ?>founder.png">
            </div>
            <div class="col-lg-9 mb-5" style="justify-content: center;align-items: center;display: flex;">
                <div class="section-title">
                    <p class="text mt-0">
                        <b>Founder</b><br><br>
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blan
                    </p>
                </div>
            </div>
            <div class="col-lg-3 mb-5" style="justify-content: center;align-items: center;display: flex;">
                <div class="section-title">
                    <h4 class="title">VISI</h4>
                </div>
            </div>
            <div class="col-lg-9 mb-5">
                <div class="section-title">
                    <p class="text mt-0">
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blan
                    </p>
                </div>
            </div>
            <div class="col-lg-3 mb-5" style="justify-content: center;align-items: center;display: flex;">
                <div class="section-title">
                    <h4 class="title">MISI</h4>
                </div>
            </div>
            <div class="col-lg-9 mb-5">
                <div class="section-title">
                    <p class="text mt-0">
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blan
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="pricing" class="pricing-area" style="background-color: #ffffff;">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="section-title text-center pb-10">
                    <h4 class="title">Legal</h4>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-4 col-md-7 col-sm-9">
                <div class="single-pricing mt-40">
                    <div class="pricing-header text-center">
                        <h5 class="sub-title">Title One</h5>
                        <p class="year mt-5">Subtitle One</p>
                    </div>
                    <div class="pricing-list">
                        <ul>
                            <li><i class="lni-check-mark-circle"></i> Lorem ipsum dolor sit amet</li>
                            <li><i class="lni-check-mark-circle"></i> Consectetur adipisicing elit</li>
                            <li><i class="lni-check-mark-circle"></i> Idquae error nulla corrupti</li>
                            <li><i class="lni-check-mark-circle"></i> Qui nobis cumque placeat optio</li>
                        </ul>
                    </div>
                    <div class="pricing-btn text-center">
                        <a class="main-btn" href="#">LEARN MORE ..</a>
                    </div>
                    <div class="buttom-shape">
                        <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 350 112.35">
                            <defs>
                                <style>
                                    .color-1 {
                                        fill: #2bdbdc;
                                        isolation: isolate;
                                    }

                                    .cls-1 {
                                        opacity: 0.1;
                                    }

                                    .cls-2 {
                                        opacity: 0.2;
                                    }

                                    .cls-3 {
                                        opacity: 0.4;
                                    }

                                    .cls-4 {
                                        opacity: 0.6;
                                    }
                                </style>
                            </defs>
                            <title>bottom-part1</title>
                            <g id="bottom-part">
                                <g id="Group_747" data-name="Group 747">
                                    <path id="Path_294" data-name="Path 294" class="cls-1 color-1" d="M0,24.21c120-55.74,214.32,2.57,267,0S349.18,7.4,349.18,7.4V82.35H0Z" transform="translate(0 0)" />
                                    <path id="Path_297" data-name="Path 297" class="cls-2 color-1" d="M350,34.21c-120-55.74-214.32,2.57-267,0S.82,17.4.82,17.4V92.35H350Z" transform="translate(0 0)" />
                                    <path id="Path_296" data-name="Path 296" class="cls-3 color-1" d="M0,44.21c120-55.74,214.32,2.57,267,0S349.18,27.4,349.18,27.4v74.95H0Z" transform="translate(0 0)" />
                                    <path id="Path_295" data-name="Path 295" class="cls-4 color-1" d="M349.17,54.21c-120-55.74-214.32,2.57-267,0S0,37.4,0,37.4v74.95H349.17Z" transform="translate(0 0)" />
                                </g>
                            </g>
                        </svg>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-7 col-sm-9">
                <div class="single-pricing pro mt-40">
                    <div class="pricing-baloon">
                        <img src="<?= FE_IMG_PATH ?>baloon.svg" alt="baloon">
                    </div>
                    <div class="pricing-header">
                        <h5 class="sub-title">Title One</h5>
                        <p class="year mt-5">Subtitle Two</p>
                    </div>
                    <div class="pricing-list">
                        <ul>
                            <li><i class="lni-check-mark-circle"></i> Lorem ipsum dolor sit amet</li>
                            <li><i class="lni-check-mark-circle"></i> Consectetur adipisicing elit</li>
                            <li><i class="lni-check-mark-circle"></i> Idquae error nulla corrupti</li>
                            <li><i class="lni-check-mark-circle"></i> Qui nobis cumque placeat optio</li>
                        </ul>
                    </div>
                    <div class="pricing-btn text-center">
                        <a class="main-btn" href="#">LEARN MORE ..</a>
                    </div>
                    <div class="buttom-shape">
                        <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 350 112.35">
                            <defs>
                                <style>
                                    .color-2 {
                                        fill: #0067f4;
                                        isolation: isolate;
                                    }

                                    .cls-1 {
                                        opacity: 0.1;
                                    }

                                    .cls-2 {
                                        opacity: 0.2;
                                    }

                                    .cls-3 {
                                        opacity: 0.4;
                                    }

                                    .cls-4 {
                                        opacity: 0.6;
                                    }
                                </style>
                            </defs>
                            <title>bottom-part1</title>
                            <g id="bottom-part">
                                <g id="Group_747" data-name="Group 747">
                                    <path id="Path_294" data-name="Path 294" class="cls-1 color-2" d="M0,24.21c120-55.74,214.32,2.57,267,0S349.18,7.4,349.18,7.4V82.35H0Z" transform="translate(0 0)" />
                                    <path id="Path_297" data-name="Path 297" class="cls-2 color-2" d="M350,34.21c-120-55.74-214.32,2.57-267,0S.82,17.4.82,17.4V92.35H350Z" transform="translate(0 0)" />
                                    <path id="Path_296" data-name="Path 296" class="cls-3 color-2" d="M0,44.21c120-55.74,214.32,2.57,267,0S349.18,27.4,349.18,27.4v74.95H0Z" transform="translate(0 0)" />
                                    <path id="Path_295" data-name="Path 295" class="cls-4 color-2" d="M349.17,54.21c-120-55.74-214.32,2.57-267,0S0,37.4,0,37.4v74.95H349.17Z" transform="translate(0 0)" />
                                </g>
                            </g>
                        </svg>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-7 col-sm-9">
                <div class="single-pricing enterprise mt-40">
                    <div class="pricing-flower">
                        <img src="<?= FE_IMG_PATH ?>flower.svg" alt="flower">
                    </div>
                    <div class="pricing-header text-right">
                        <h5 class="sub-title">Title Three</h5>
                        <p class="year mt-5">Subtitle Three</p>
                    </div>
                    <div class="pricing-list">
                        <ul>
                            <li><i class="lni-check-mark-circle"></i> Lorem ipsum dolor sit amet</li>
                            <li><i class="lni-check-mark-circle"></i> Consectetur adipisicing elit</li>
                            <li><i class="lni-check-mark-circle"></i> Idquae error nulla corrupti</li>
                            <li><i class="lni-check-mark-circle"></i> Qui nobis cumque placeat optio</li>
                        </ul>
                    </div>
                    <div class="pricing-btn text-center">
                        <a class="main-btn" href="#">LEARN MORE ..</a>
                    </div>
                    <div class="buttom-shape">
                        <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 350 112.35">
                            <defs>
                                <style>
                                    .color-3 {
                                        fill: #4da422;
                                        isolation: isolate;
                                    }

                                    .cls-1 {
                                        opacity: 0.1;
                                    }

                                    .cls-2 {
                                        opacity: 0.2;
                                    }

                                    .cls-3 {
                                        opacity: 0.4;
                                    }

                                    .cls-4 {
                                        opacity: 0.6;
                                    }
                                </style>
                            </defs>
                            <title>bottom-part1</title>
                            <g id="bottom-part">
                                <g id="Group_747" data-name="Group 747">
                                    <path id="Path_294" data-name="Path 294" class="cls-1 color-3" d="M0,24.21c120-55.74,214.32,2.57,267,0S349.18,7.4,349.18,7.4V82.35H0Z" transform="translate(0 0)" />
                                    <path id="Path_297" data-name="Path 297" class="cls-2 color-3" d="M350,34.21c-120-55.74-214.32,2.57-267,0S.82,17.4.82,17.4V92.35H350Z" transform="translate(0 0)" />
                                    <path id="Path_296" data-name="Path 296" class="cls-3 color-3" d="M0,44.21c120-55.74,214.32,2.57,267,0S349.18,27.4,349.18,27.4v74.95H0Z" transform="translate(0 0)" />
                                    <path id="Path_295" data-name="Path 295" class="cls-4 color-3" d="M349.17,54.21c-120-55.74-214.32,2.57-267,0S0,37.4,0,37.4v74.95H349.17Z" transform="translate(0 0)" />
                                </g>
                            </g>
                        </svg>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>