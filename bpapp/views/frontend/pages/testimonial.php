<header class="header-area">

    <?php $this->load->view(VIEW_FRONT . 'pages/components/nav.php'); ?>

    <div id="home" class="header-hero bg_cover" style="background-image: url(<?= FE_IMG_PATH ?>header/testi.jpg)">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="header-content text-center text-md-left" style="padding-top: 100px;">
                        <p class="text text-primary-color mt-0 mb-2">
                            Bersama kami jadikan
                        </p>
                        <h1 class="mt-0 mb-2">KELUARGA</h1>
                        <p class="text text-primary-color mt-0 mb-2">
                            yang penuh bahagia
                        </p>
                        <ul class="header-btn rounded-buttons">
                            <li><a class="main-btn rounded-three text-uppercase" href="#">menjadi member</a></li>
                            <li><a class="main-btn rounded-four text-uppercase text-primary-color" href="<?php echo base_url('store'); ?>">produk kami</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-shape">
            <img src="<?= FE_IMG_PATH ?>header-shape.svg" alt="shape">
        </div>
    </div>
</header>

<section id="service" class="services-area pb-0">
    <div class="container">
        <div class="section-title text-center mb-5">
            <h4 class="title">OUR CLIENT FEEDBACK</h4>
        </div>
        <div class="row">
            <div class="col-md-6 mb-5" style="justify-content: center;align-items: center;display: flex;">
                <div class="row">
                    <div class="col-md-4 mb-5" style="justify-content: center;align-items: center;display: flex;">
                        <img src="<?= FE_IMG_PATH ?>testi/1.png">
                    </div>
                    <div class="col-md-7 mb-5" style="justify-content: center;align-items: center;display: flex;">
                        <p class="text mt-0">
                            <img src="<?= FE_IMG_PATH ?>star.png" width="30px">
                            <img src="<?= FE_IMG_PATH ?>star.png" width="30px">
                            <img src="<?= FE_IMG_PATH ?>star.png" width="30px">
                            <img src="<?= FE_IMG_PATH ?>star.png" width="30px">
                            <img src="<?= FE_IMG_PATH ?>star.png" width="30px">
                            <br><br>
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreev
                            <br><br>
                            Name Surname
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-5" style="justify-content: center;align-items: center;display: flex;">
                <div class="row">
                    <div class="col-md-4 mb-5" style="justify-content: center;align-items: center;display: flex;">
                        <img src="<?= FE_IMG_PATH ?>testi/2.png">
                    </div>
                    <div class="col-md-7 mb-5" style="justify-content: center;align-items: center;display: flex;">
                        <p class="text mt-0">
                            <img src="<?= FE_IMG_PATH ?>star.png" width="30px">
                            <img src="<?= FE_IMG_PATH ?>star.png" width="30px">
                            <img src="<?= FE_IMG_PATH ?>star.png" width="30px">
                            <img src="<?= FE_IMG_PATH ?>star.png" width="30px">
                            <img src="<?= FE_IMG_PATH ?>star.png" width="30px">
                            <br><br>
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreev
                            <br><br>
                            Name Surname
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-5" style="justify-content: center;align-items: center;display: flex;">
                <div class="row">
                    <div class="col-md-4 mb-5" style="justify-content: center;align-items: center;display: flex;">
                        <img src="<?= FE_IMG_PATH ?>testi/3.png">
                    </div>
                    <div class="col-md-7 mb-5" style="justify-content: center;align-items: center;display: flex;">
                        <p class="text mt-0">
                            <img src="<?= FE_IMG_PATH ?>star.png" width="30px">
                            <img src="<?= FE_IMG_PATH ?>star.png" width="30px">
                            <img src="<?= FE_IMG_PATH ?>star.png" width="30px">
                            <img src="<?= FE_IMG_PATH ?>star.png" width="30px">
                            <img src="<?= FE_IMG_PATH ?>star.png" width="30px">
                            <br><br>
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreev
                            <br><br>
                            Name Surname
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-5" style="justify-content: center;align-items: center;display: flex;">
                <div class="row">
                    <div class="col-md-4 mb-5" style="justify-content: center;align-items: center;display: flex;">
                        <img src="<?= FE_IMG_PATH ?>testi/4.png">
                    </div>
                    <div class="col-md-7 mb-5" style="justify-content: center;align-items: center;display: flex;">
                        <p class="text mt-0">
                            <img src="<?= FE_IMG_PATH ?>star.png" width="30px">
                            <img src="<?= FE_IMG_PATH ?>star.png" width="30px">
                            <img src="<?= FE_IMG_PATH ?>star.png" width="30px">
                            <img src="<?= FE_IMG_PATH ?>star.png" width="30px">
                            <img src="<?= FE_IMG_PATH ?>star.png" width="30px">
                            <br><br>
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreev
                            <br><br>
                            Name Surname
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="services-area mb-0 pb-0">
    <img src="<?= FE_IMG_PATH ?>cs2.jpg">
</section>

<section class="pricing-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 mb-5" style="justify-content: center;align-items: center;display: flex;">
                <img src="<?= FE_IMG_PATH ?>header/testi.jpg">
            </div>
            <div class="col-lg-7 mb-5" style="justify-content: center;align-items: center;display: flex;">
                <div class="section-title">
                    <p class="text mt-0">
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blan
                        <br><br>
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- <section class="testimonial-area">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="section-title text-center pb-10">
                    <h4 class="title">Testimonial</h4>
                    <p class="text">Stop wasting time and money designing and managing a website that doesn’t get
                        results. Happiness guaranteed!</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="row testimonial-active">
                    <div class="col-lg-4">
                        <div class="single-testimonial mt-30 mb-30 text-center">
                            <div class="testimonial-image">
                                <img src="<?= FE_IMG_PATH ?>author-3.jpg" alt="Author">
                            </div>
                            <div class="testimonial-content">
                                <p class="text">Stop wasting time and money designing and managing a website that
                                    doesn’t get results. Happiness guaranteed! Stop wasting time and money designing
                                    and managing a website that doesn’t get results. Happiness guaranteed!</p>
                                <h6 class="author-name">Isabela Moreira</h6>
                                <span class="sub-title">CEO, GrayGrids</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="single-testimonial mt-30 mb-30 text-center">
                            <div class="testimonial-image">
                                <img src="<?= FE_IMG_PATH ?>author-1.jpg" alt="Author">
                            </div>
                            <div class="testimonial-content">
                                <p class="text">Stop wasting time and money designing and managing a website that
                                    doesn’t get results. Happiness guaranteed! Stop wasting time and money designing
                                    and managing a website that doesn’t get results. Happiness guaranteed!</p>
                                <h6 class="author-name">Fiona</h6>
                                <span class="sub-title">Lead Designer, UIdeck</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="single-testimonial mt-30 mb-30 text-center">
                            <div class="testimonial-image">
                                <img src="<?= FE_IMG_PATH ?>author-2.jpg" alt="Author">
                            </div>
                            <div class="testimonial-content">
                                <p class="text">Stop wasting time and money designing and managing a website that
                                    doesn’t get results. Happiness guaranteed! Stop wasting time and money designing
                                    and managing a website that doesn’t get results. Happiness guaranteed!</p>
                                <h6 class="author-name">Elon Musk</h6>
                                <span class="sub-title">CEO, SpaceX</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="single-testimonial mt-30 mb-30 text-center">
                            <div class="testimonial-image">
                                <img src="<?= FE_IMG_PATH ?>author-4.jpg" alt="Author">
                            </div>
                            <div class="testimonial-content">
                                <p class="text">Stop wasting time and money designing and managing a website that
                                    doesn’t get results. Happiness guaranteed! Stop wasting time and money designing
                                    and managing a website that doesn’t get results. Happiness guaranteed!</p>
                                <h6 class="author-name">Fajar Siddiq</h6>
                                <span class="sub-title">CEO, MakerFlix</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> -->
