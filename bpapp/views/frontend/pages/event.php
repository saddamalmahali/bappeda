<header class="header-area">

    <?php $this->load->view(VIEW_FRONT . 'pages/components/nav.php'); ?>

    <div id="home" class="header-hero bg_cover" style="background-image: url(<?= FE_IMG_PATH ?>header/event2.jpg)">
        <div class="header-shape">
            <img src="<?= FE_IMG_PATH ?>header-shape.svg" alt="shape">
        </div>
    </div>
</header>

<section id="service" class="services-area pb-0">
    <div class="container">
        <div class="section-title text-center mb-5">
            <h4 class="title">REWARDS AND RECOGNITION</h4>
        </div>
        <div class="row">
            <p class="text mt-0 mb-5">
                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blan nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
            </p>
            <div class="col-lg-4 mb-5" style="justify-content: center;align-items: center;display: flex;">
                <img src="<?= FE_IMG_PATH ?>reward/1.jpg">
            </div>
            <div class="col-lg-4 mb-5" style="justify-content: center;align-items: center;display: flex;">
                <img src="<?= FE_IMG_PATH ?>reward/2.jpg">
            </div>
            <div class="col-lg-4 mb-5" style="justify-content: center;align-items: center;display: flex;">
                <img src="<?= FE_IMG_PATH ?>reward/3.jpg">
            </div>
        </div>
    </div>
</section>

<section id="manfaat" class="services-area pb-0">
    <div class="container">
        <div class="section-title text-center mb-5">
            <h4 class="title">OUR LEADER</h4>
        </div>
        <div class="row">
            <div class="col-md-6 mb-5" style="justify-content: center;align-items: center;display: flex;">
                <img src="<?= FE_IMG_PATH ?>placeholder1.png">
            </div>
            <div class="col-md-6 mb-5" style="justify-content: center;align-items: center;display: flex;">
                <img src="<?= FE_IMG_PATH ?>placeholder1.png">
            </div>
            <div class="col-md-6 mb-5" style="justify-content: center;align-items: center;display: flex;">
                <img src="<?= FE_IMG_PATH ?>placeholder1.png">
            </div>
            <div class="col-md-6 mb-5" style="justify-content: center;align-items: center;display: flex;">
                <img src="<?= FE_IMG_PATH ?>placeholder1.png">
            </div>
        </div>
    </div>
</section>

<section class="services-area mb-0 pb-0">
    <img src="<?= FE_IMG_PATH ?>reward/4.jpg">
</section>

<section id="manfaat" class="pricing-area">
    <div class="container">
        <div class="section-title text-center mb-5">
            <h4 class="title">QUOTES</h4>
        </div>
        <div class="row">
            <div class="col-lg-12 mb-5" style="justify-content: center;align-items: center;display: flex;">
                <div class="section-title">
                    <p class="text text-center mt-0">
                        I’m convinced that about half of what separates successful entrepreneurs from the non-successful ones is pure perseverance.
                        <br><br>
                        <span style="color: #0f6a43;font-weight: 500;">- STEVE JOBS -</span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>