<div class="navgition navgition-transparent">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <nav class="navbar navbar-expand-lg">
                    <a href="<?= base_url() ?>"><img src="<?= LOGO_IMG ?>" alt="Logo" class="mr-3" style="width: 60px;"></a>
                    <ul class="d-flex">
                        <li class="px-2"><a href="javascript:;" class="text-primary-color" style="font-weight: 600;">REGISTER</a></li>
                        <li class="px-2"><a href="javascript:;" class="text-primary-color" style="font-weight: 600;">|</a></li>
                        <li class="px-2"><a href="<?= base_url('login') ?>" class="text-primary-color" style="font-weight: 600;">LOGIN</a></li>
                    </ul>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarOne" aria-controls="navbarOne" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="toggler-icon"></span>
                        <span class="toggler-icon"></span>
                        <span class="toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse sub-menu-bar" id="navbarOne" style="justify-content: flex-end;">
                        <ul class="navbar-nav">
                            <li class="nav-item <?= ($this->uri->uri_string() == '' ? 'active' : '') ?>"><a href="<?= base_url() ?>"><span class="pl-2 pr-2">HOME</span></a>
                            </li>
                            <li class="nav-item <?= ($this->uri->uri_string() == 'company' ? 'active' : '') ?>"><a href="<?= base_url('company') ?>"><span class="pl-2 pr-2">COMPANY</span></a>
                            </li>
                            <li class="nav-item dropdown" style="display:flex;align-items:center;cursor:pointer">
                                <div class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="pl-2 pr-2">PRODUCT</span>
                                </div>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                    <a class="dropdown-item" href="<?= base_url('product/skin-care') ?>">Skin Care</a>
                                    <a class="dropdown-item" href="<?= base_url('product/soap') ?>">Soap</a>
                                </div>
                            </li>
                            <li class="nav-item dropdown" style="display:flex;align-items:center;cursor:pointer">
                                <div class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="pl-2 pr-2">SHOP</span>
                                </div>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                    <a class="dropdown-item" href="<?= base_url('store') ?>">Belanja Produk</a>
                                    <a class="dropdown-item" href="<?= base_url('store/searchorder') ?>">Telusuri Pesanan</a>
                                </div>
                            </li>
                            <li class="nav-item <?= ($this->uri->uri_string() == 'testimonial' ? 'active' : '') ?>"><a href="<?= base_url('testimonial') ?>"><span class="pl-2 pr-2">TESTIMONY</span></a>
                            </li>
                            <li class="nav-item <?= ($this->uri->uri_string() == 'event' ? 'active' : '') ?>"><a href="<?= base_url('event') ?>"><span class="pl-2 pr-2">EVENT</span></a>
                            </li>
                            <li class="nav-item <?= ($this->uri->uri_string() == 'contact' ? 'active' : '') ?>"><a href="<?= base_url('contact') ?>"><span class="pl-2 pr-2">CONTACT US</span></a>
                            </li>

                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>