<header class="header-area">

    <?php $this->load->view(VIEW_FRONT . 'pages/components/nav.php'); ?>

    <div id="home" class="header-hero bg_cover" style="background-image: url(<?= FE_IMG_PATH ?>header/home.jpg)">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="header-content text-center text-md-left">
                        <p class="text text-primary-color mt-0 mb-2">
                            Bersama kami jadikan
                        </p>
                        <h1 class="mt-0 mb-2">KELUARGA</h1>
                        <p class="text text-primary-color mt-0 mb-2">
                            yang penuh bahagia
                        </p>
                        <ul class="header-btn rounded-buttons">
                            <li><a class="main-btn rounded-three text-uppercase" href="#">menjadi member</a></li>
                            <li><a class="main-btn rounded-four text-uppercase text-primary-color" href="<?php echo base_url('store'); ?>">produk kami</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-shape">
            <img src="<?= FE_IMG_PATH ?>header-shape.svg" alt="shape">
        </div>
    </div>
</header>

<div class="container py-5 my-5">
    <div class="row">
        <div class="col-md-12 text-center">
            <h1 class="text-uppercase">be precious and happy</h1>
        </div>
    </div>
</div>

<section id="service" class="services-area bg_cover" style="background-image: url(<?= FE_IMG_PATH ?>bg-product.png)">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="section-title pb-10">
                    <h4 class="title"><img src="<?= FE_IMG_PATH ?>brightening-soap.png"></h4>
                    <p class="text" style="padding-right: 30px;">
                        Temulawak sudah dikenal dalam khasiatnya mencerahkan kulit. Brightening Soap ini diproduksi dengan menggunakan bahan baku minyak kelapa. Kandungan dari minyak kelapa dapat melembabkan serta menghaluskan kulit. Selain itu Vitamin E yang terkandung dalam minyak kelapa dapat menjaga kulit dari kerusakan akibat radikal bebas. bahan aktif yang digunakan sabun ini adalah ekstrak temulawak. Temulawak berfungsi untuk meremajakan kulit, mencerahkan wajah , mengencangkan kulit dan dapat mengecilkan pori-pori yang besar. Temulawak juga dapat menjadi anti oksidan alami kulit. Penggunaan rutin produk ini dapat membantu menghilangkan bekas luka.
                    </p>
                    <a class="main-btn rounded-four text-uppercase text-primary-color mt-4" href="<?= base_url('product/soap') ?>" style="font-size: 13px;">LEARN MORE ...</a>
                </div>
            </div>
        </div>
    </div>
</section>

<!--
<section id="certification" class="pricing-area">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="section-title text-center pb-10">
                    <h4 class="title">Sertifikasi Lengkap KAIDAH NETWORK</h4>
                    <p class="text">
                        KAIDAH NETWORK telah memiliki sertifikasi lengkap untuk menjalankan sistem penjualan langsung di Indonesia, selain telah bergabungnya KAIDAH NETWOK di Asosiasi Perusahaan Penjualan Langsung (AP2LI), produk yang kami pasarkan juga telah memiliki sertifikasi BPOM dan Halal MUI
                    </p>
                </div>
            </div>
        </div>
        <div class="row justify-content-center mt-5">
            <div class="col-md-12 text-center">
                <img src="<?= FE_IMG_PATH ?>bpom.png" style="padding:20px;width:130px;height:130px;" />
                <img src="<?= FE_IMG_PATH ?>mui.png" style="padding:20px;width:130px;height:130px;" />
                <img src="<?= FE_IMG_PATH ?>apli.jpg" style="padding:20px;border-radius: 50%;width:130px;height:130px;" />
            </div>
        </div>
    </div>
</section>
-->