<div class="breadcumb-wrapper breadcumb-layout1 background-image link-inherit space" data-vs-img="<?= FE_IMG_PATH ?>breadcumb-bg.jpg" data-overlay="black" data-opacity="3">
    <div class="container z-index-common py-10">
        <div class="breadcumb-content">
            <h1 class="breadcumb-title sec-title-style1 text-white mt-0 mb-2">About Us</h1>
            <ul class="breadcumb-menu-style1 text-white">
                <li><a href="index.html"><i class="fal fa-home text-theme"></i>Home</a></li>
                <li class="active">About Us</li>
            </ul>
        </div>
    </div>
</div>
<section class="vs-about-wrapper vs-about-layout6 space">
    <div class="container">
        <div class="row flex-row-reverse">
            <div class="col-lg-6 align-self-end">
                <div class="about-image-box5 position-relative text-center z-index-common"><span class="ripple d-none d-md-inline-block"></span> <img src="<?= FE_IMG_PATH ?>about/about-img-5-1.png" alt="About Image"></div>
            </div>
            <div class="col-md-11 col-lg-6 align-self-center py-30 py-lg-0">
                <div class="about-content-box1 text-center">
                    <p class="mb-30">We Have <span class="inner-label">25 Years</span> Of Experience</p>
                    <h2 class="sec-title-style1">Learn More <span class="sec-subtitle-style1">About<br>Us</span>
                    </h2>
                    <p class="text-font2 text-20">Lorem ipsum dolor sit amet consectetur adipisicing elit. Delectus voluptates possimus fugiat iusto optio tempore</p>
                    <div class="row gutters-20 justify-content-center justify-content-xl-end pb-4">
                        <div class="col-sm-4 col-md-3 col-lg-4 text-center text-sm-right mb-10 mb-sm-0"><img src="<?= FE_IMG_PATH ?>about/signature.png" alt="Author Signature"></div>
                        <div class="col-sm-7 col-md-6 col-lg-8 col-xl-7">
                            <div class="author-box1 d-sm-flex">
                                <div class="author-img mb-15 mb-sm-0 mr-20"><img src="<?= FE_IMG_PATH ?>testimonial/testil-author-1-2.png" alt="Author Image"></div>
                                <div class="author-content text-center text-sm-left align-self-center">
                                    <h3 class="author-name h5 mb-0">Founder</h3><strong class="author-degi text-theme">Founder & Beautician</strong>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a href="#more" class="vs-btn wave-style1">Learn More<i class="fal fa-arrow-down"></i></a>
                </div>
            </div>
        </div>

        <div class="skill-area px-100 pt-95 pb-60 bg-light-theme">
            <div class="row gutters-40">
                <div class="col-md-6">
                    <div class="vs-skill-bar1 mb-35">
                        <div class="bar-head d-flex justify-content-between">
                            <h4 class="text-md mb-2">Facewash</h4><span class="text-md text-font2">90%</span>
                        </div>
                        <div class="vs-progress">
                            <div class="progress-value" data-valuenow="90"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="vs-skill-bar1 mb-35">
                        <div class="bar-head d-flex justify-content-between">
                            <h4 class="text-md mb-2">Hair Care</h4><span class="text-md text-font2">70%</span>
                        </div>
                        <div class="vs-progress">
                            <div class="progress-value" data-valuenow="70"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="vs-skill-bar1 mb-35">
                        <div class="bar-head d-flex justify-content-between">
                            <h4 class="text-md mb-2">Body Care</h4><span class="text-md text-font2">85%</span>
                        </div>
                        <div class="vs-progress">
                            <div class="progress-value" data-valuenow="85"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="vs-skill-bar1 mb-35">
                        <div class="bar-head d-flex justify-content-between">
                            <h4 class="text-md mb-2">Skin Care</h4><span class="text-md text-font2">95%</span>
                        </div>
                        <div class="vs-progress">
                            <div class="progress-value" data-valuenow="95"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="vs-about-wrapper mb-5" id="more">
    <div class="container-fluid px-0">
        <div class="row no-gutters">
            <div class="col-xl-6">
                <div class="vs-about-layout7 d-xl-flex flex-column no-gutters pt-70">
                    <div class="about-image-box6"><img src="<?= FE_IMG_PATH ?>about/about-img-5-1.jpg" alt="About Image" class="w-100"></div>
                    <div class="about-content-box5 h-100 w-100 d-flex align-items-center px-2 px-4 px-xl-5 space background-image bg-fluid" data-vs-img="<?= FE_IMG_PATH ?>shape/about-bg-patten-2.png">
                        <div>
                            <p class="mb-30 text-white">Your <span class="inner-label">Daily Product</span> is Right Here</p>
                            <h2 class="sec-title-style1 text-white">Daily Beauty<span class="sec-subtitle-style1">Explore Product</span></h2>
                            <p class="text-font2 text-20 text-white mb-25">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Fugit illo autem eius repellat ea, quae odio voluptas</p>
                            <div class="text-box1 py-0 d-block d-md-flex align-items-center">
                                <div class="media-img mb-20 mb-md-0 mr-md-4"><img src="<?= FE_IMG_PATH ?>about/about-img-2-3.jpg" alt="Thumb Image"></div>
                                <div class="media-body">
                                    <p class="mb-0 text-white">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Fugit illo autem eius repellat ea, quae odio voluptas amet consectetur</p>
                                </div>
                            </div>
                            <p class="mb-0 text-white">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt, delectus blanditiis. Magnam eveniet vel ipsum possimus impedit accusantium animi, eum alias, voluptatibus itaque libero rerum quis dolorum maxime</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6">
                <div class="vs-about-layout7 d-xl-flex flex-column no-gutters">
                    <div class="about-image-box6 order-2"><img src="<?= FE_IMG_PATH ?>about/about-img-5-2.jpg" alt="About Image" class="w-100"></div>
                    <div class="about-content-box5 h-100 w-100 d-flex align-items-center px-2 px-4 px-xl-5 space background-image bg-fluid" data-vs-img="<?= FE_IMG_PATH ?>shape/about-bg-patten-1.png">
                        <div>
                            <p class="mb-30 text-white">Your <span class="inner-label">Best Price</span> is Right
                                Here</p>
                            <h2 class="sec-title-style1 text-white">Check Some <span class="sec-subtitle-style1">Price Plans</span></h2>
                            <p class="text-font2 text-20 text-white mb-25">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Fugit illo autem eius repellat ea, quae odio voluptas</p>
                            <div class="text-box1 py-0 d-block d-md-flex align-items-center">
                                <div class="media-img mb-20 mb-md-0 mr-md-4"><img src="<?= FE_IMG_PATH ?>about/about-img-2-2.jpg" alt="Thumb Image"></div>
                                <div class="media-body">
                                    <p class="mb-0 text-white">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Fugit illo autem eius repellat ea, quae odio voluptas</p>
                                </div>
                            </div>
                            <p class="mb-0 text-white">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt, delectus blanditiis. Magnam eveniet vel ipsum possimus impedit accusantium animi, eum alias, voluptatibus itaque libero rerum quis dolorum maxime, praesentium repellat.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="vs-subscribe-wrapper vs-subscribe-layout1 position-relative bg-light-theme space-md mt-5">
    <div class="shape1 position-absolute ani-moving-y"><img src="<?= FE_IMG_PATH ?>shape/leaf-icon-1.png" alt="Shape BG">
    </div>
    <div class="container">
        <div class="row justify-content-center justify-content-lg-start align-items-center py-20 py-lg-0 text-center text-lg-left">
            <div class="col-md-10 col-lg-6">
                <div class="form-title mb-30 mb-lg-0">
                    <h2 class="sec-title-style1">Get Newsletter <span class="sec-subtitle-style1">Get Connect</span>
                    </h2>
                    <p class="sec-text-style1 mb-0">Lorem ipsum dolor sit amet consectetur, adipisicing elit consectetur.</p>
                </div>
            </div>
            <div class="col-lg-6">
                <form action="#" class="subscribe-form-style1 ml-xl-4 d-sm-flex bg-white p-2">
                    <input type="email" placeholder="Enter your email address" class="form-control"> <button type="submit" class="vs-btn vs-style1">Subscribe Now</button>
                </form>
            </div>
        </div>
    </div>
</section>