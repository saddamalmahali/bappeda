<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title><?= COMPANY_NAME . ' | ' . $title ?></title>

    <meta name="author" content="">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css2?family=Prata&amp;family=Roboto:wght@300;400;500;700&amp;display=swap" rel="stylesheet">
    <link rel="apple-touch-icon" href="<?= FE_IMG_PATH ?>favicon.png">
    <link rel="icon" type="image/png" href="<?= FE_IMG_PATH ?>favicon.png">

    <link rel="manifest" href="<?= FE_IMG_PATH ?>manifest.json">

    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?= FE_IMG_PATH ?>favicon.png">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" href="<?= FE_CSS_PATH ?>app.min.css">
    <link rel="stylesheet" href="<?= FE_CSS_PATH ?>flaticon.min.css">
    <link rel="stylesheet" href="<?= FE_CSS_PATH ?>fontawesome.min.css">
    <link rel="stylesheet" href="<?= FE_CSS_PATH ?>style.css">
    <link rel="stylesheet" href="<?= FE_CSS_PATH ?>theme-color1.css">
    <link rel="stylesheet" href="<?= FE_CSS_PATH ?>custom.css">

</head>

<body>

    <div class="preloader"><button class="vs-btn vs-style1 preloaderCls">Cancel Preloader</button>
        <div class="preloader-inner">
            <div class="loader-logo"><img src="<?= FE_IMG_PATH ?>logo-white.png" alt="" width="150px">
            </div>
            <div class="preloader-box">
                <div class="letter">L</div>
                <div class="letter">O</div>
                <div class="letter">A</div>
                <div class="letter">D</div>
                <div class="letter">I</div>
                <div class="letter">N</div>
                <div class="letter">G</div>
            </div>
        </div>
    </div>

    <div class="vs-menu-wrapper">
        <div class="vs-menu-area"><button class="vs-menu-toggle text-theme"><i class="fal fa-times"></i></button>
            <div class="mobile-logo"><a href="<?= base_url() ?>"><img src="<?= FE_IMG_PATH ?>logo.png"></a></div>
            <div class="vs-mobile-menu link-inherit"></div>
        </div>
    </div>

    <header class="header-wrapper header-layout4">
        <div class="bg-theme header-top-layout1 py-2">
            <div class="container">
                <div class="row">
                    <div class="col-md-7 d-none d-lg-block">
                        <ul class="info-links list-style-none text-white">
                            <li><a href="tel:01234568910"><i class="fas fa-phone-alt"></i><?= COMPANY_PHONE ?></a></li>
                            <li><i class="fas fa-map-marker-alt"></i><?= COMPANY_ADDRESS ?></li>
                        </ul>
                    </div>
                    <div class="col-md-5">
                        <ul class="social-links text-center text-lg-right text-white">
                            <li><a href="javascript:;"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="javascript:;"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="javascript:;"><i class="fab fa-instagram"></i></a></li>
                            <li><a href="javascript:;"><i class="fab fa-youtube"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="container position-relative py-3 py-lg-0">
            <div class="row align-items-center">
                <div class="col-6 col-lg-2">
                    <div class="header-logo"><a href="<?= base_url() ?>"><img src="<?= FE_IMG_PATH ?>logo.png" alt=""></a></div>
                </div>
                <div class="col-6 col-lg-8 position-static text-lg-center text-right">
                    <nav class="main-menu menu-style2 mobile-menu-active">
                        <ul>
                            <li class="<?= ($this->uri->uri_string() == '') ? 'active' : '' ?>"><a href="<?= base_url() ?>">Home</a></li>
                            <li class="<?= ($this->uri->uri_string() == 'about') ? 'active' : '' ?>"><a href="<?= base_url('about') ?>">About</a></li>
                            <li class="<?= ($this->uri->uri_string() == 'contact') ? 'active' : '' ?>"><a href="<?= base_url('contact') ?>">Contact</a></li>
                            <li class="<?= ($this->uri->uri_string() == 'gallery') ? 'active' : '' ?>"><a href="<?= base_url('gallery') ?>">Gallery</a></li>
                            <li class="d-sm-block d-md-none"><a href="<?= base_url('login') ?>">Member Area</a></li>
                        </ul>
                    </nav><button type="button" class="vs-menu-toggle d-inline-block d-lg-none"><i class="fas fa-bars"></i></button>
                </div>
                <div class="col-lg-2 text-right d-none d-lg-block">
                    <div class="header-btn">
                        <a href="<?= base_url('login') ?>" class="icon-btn bg-theme" style="width: 45px;height: 45px;">
                            <i class="fas fa-user-circle d-block" style="font-size: 23px;line-height: 44px;"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <?php $this->load->view(VIEW_FRONT . $main_content); ?>

    <footer class="footer-wrapper footer-layout2 position-relative bg-body">
        <div class="shape1 ani-moving-y position-absolute"><img src="<?= FE_IMG_PATH ?>shape/flower-1-2.png" alt="Flowers">
        </div>
        <div class="footer-widget footer-widget-layout2 pt-100 pb-70">
            <div class="container">
                <div class="row gutters-40">
                    <div class="col-lg-4">
                        <div class="widget">
                            <div class="vs-widget-about pr-lg-4">
                                <div class="widget-about-logo mb-20 mb-lg-25">
                                    <a href="<?= base_url() ?>"><img src="<?= FE_IMG_PATH ?>logo-white.png" width="200px"></a>
                                </div>
                                <p class="widget-about-text text-white mb-20">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Deleniti, rerum. Vel maiores est necessitatibus</p>
                                <p class="contact-info-style1 text-white"><i class="far fa-phone text-theme"></i><a href="javascript:;"><?= COMPANY_PHONE ?></a></p>
                                <p class="contact-info-style1 text-white"><i class="fal fa-envelope text-theme"></i><a href="mailto:mixlax@email.com"><?= COMPANY_EMAIL ?></a></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <div class="widget widget_nav_menu">
                            <h3 class="widget_title text-white">Link</h3>
                            <div class="menu-all-pages-container style-white">
                                <ul class="menu">
                                    <li><a href="#">How It Works</a></li>
                                    <li><a href="#">Our History</a></li>
                                    <li><a href="#">Services</a></li>
                                    <li><a href="#">Our Products</a></li>
                                    <li><a href="#">Policy</a></li>
                                    <li><a href="#">Home Service</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <div class="widget widget_nav_menu">
                            <h3 class="widget_title text-white">Sitemap</h3>
                            <div class="menu-all-pages-container style-white">
                                <ul class="menu">
                                    <li><a href="#">About Us</a></li>
                                    <li><a href="#">FAQ</a></li>
                                    <li><a href="#">Pricing</a></li>
                                    <li><a href="#">Settings</a></li>
                                    <li><a href="#">Contact</a></li>
                                    <li><a href="#">Support</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-copyright bg-black py-3 link-inherit">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6 text-center text-lg-left">
                        <p class="mb-0 text-bold text-white">Copyright <i class="fal fa-copyright"></i> 2021 <a href="<?= base_url() ?>"><?= DOMAIN_NAME ?></a></p>
                    </div>
                    <div class="col-lg-6 text-lg-right d-none d-lg-inline-block">
                        <ul class="social-links links-hover-border text-white">
                            <li><a href="javascript:;"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="javascript:;"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="javascript:;"><i class="fab fa-instagram"></i></a></li>
                            <li><a href="javascript:;"><i class="fab fa-youtube"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <a href="#" class="scrollToTop icon-btn bg-theme"><i class="far fa-arrow-up"></i></a>

    <script src="<?= FE_JS_PATH ?>vendor/jquery-1.12.4.min.js"></script>
    <script src="<?= FE_JS_PATH ?>app.min.js"></script>
    <script src="<?= FE_JS_PATH ?>vscustom-carousel.min.js"></script>
    <script src="<?= FE_JS_PATH ?>vsmenu.min.js"></script>
    <script src="<?= FE_JS_PATH ?>ajax-mail.js"></script>
    <script src="<?= FE_JS_PATH ?>vs-colorplate.min.js"></script>
    <script src="<?= FE_JS_PATH ?>main.js"></script>

</body>

</html>