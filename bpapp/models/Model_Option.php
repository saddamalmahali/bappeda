<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once('BP_Model.php');

class Model_Option extends BP_Model{
    /**
     * Initialize table and primary field variable
     */
    var $table              = TBL_OPTIONS;
    var $options            = TBL_OPTIONS;
    var $stages             = TBL_PREFIX . 'tahap';
    var $notification       = TBL_PREFIX . 'notification';

    /**
     * Initialize primary field
     */
    var $primary            = "id";
    
    /**
	* Constructor - Sets up the object properties.
	*/
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get Stages
     * 
     * @author  Yuda
     * @param   Int     $id     (Optional)  ID of data
     * @return  Mixed   False on invalid date parameter, otherwise data of data(s).
     */
    function get_stages($id = '', $group_by = '', $order_by = '', $sort = 'ASC')
    {
        if (!empty($id)) {
            $this->db->where('id', $id);
        }

        if ( $group_by ) {
            $this->db->group_by($group_by);
        }

        if ( $order_by ) {
            $sort = $sort ? $sort : 'ASC';
            $this->db->order_by($order_by, $sort);
        }

        $this->db->order_by('sort_stage', 'ASC');
        $this->db->order_by("id", "ASC");
        $query      = $this->db->get($this->stages);
        return (!empty($id) ? $query->row() : $query->result());
    }

    /**
     * Get Stages by Field
     *
     * @author  Yuda
     * @param   String  $field  (Required)  Database field name or special field name defined inside this function
     * @param   String  $value  (Optional)  Value of the field being searched
     * @return  Mixed   Boolean false on failed process, invalid data, or data is not found, otherwise StdClass of data
     */
    function get_stages_by($field='', $value='', $conditions='', $limit = 0, $group_by = ''){
        if ( !$field || !$value ) return false;

        $this->db->where($field, $value);
        if ($conditions) {
            $this->db->where($conditions);
        }

        if ( $group_by ) {
            $this->db->group_by($group_by);
        }

        $this->db->order_by('sort_stage', 'ASC');
        $this->db->order_by("id", "ASC");
        $query  = $this->db->get($this->stages);
        if (!$query->num_rows()) {
            return false;
        }

        $data   = $query->result();
        if ($field == 'id' || $limit == 1) {
            foreach ($data as $row) {
                $datarow = $row;
            }
            $data = $datarow;
        }

        return $data;
    }

    /**
     * Get Notification by Field
     *
     * @author  Yuda
     * @param   String  $field  (Required)  Database field name or special field name defined inside this function
     * @param   String  $value  (Optional)  Value of the field being searched
     * @return  Mixed   Boolean false on failed process, invalid data, or data is not found, otherwise StdClass of member
     */
    function get_notification_by($field='', $value='', $type='', $limit = 0){
        if ( !$field || !$value ) return false;
        switch ($field) {
            case 'id':
                $field  = 'id';
                $id     = $value;
                break;
            case 'name':
                $field  = 'name';
                $value  = $value;
                break;
            case 'slug':
                $field  = 'slug';
                $value  = $value;
                break;
            case 'type':
                $field  = 'type';
                $value  = $value;
                break;
            return false;
        }

        if( empty($field) ) return false;

        $condition = array($field => $value);

        if( !empty($type) ) { $condition['type'] = $type; }

        $query  = $this->db->get_where($this->notification, $condition);
        if ( !$query->num_rows() )
            return false;

        $data   = $query->result();

        $onerow = false;
        if ( $field == 'id' || $field == 'slug' || $limit == 1 ) {
            $onerow = true;
        }
        if ( $field && $type ) {
            $onerow = true;
        }

        if ( $onerow ) {
            foreach ( $data as $row ) {
                $datarow = $row;
            }
            $data = $datarow;
        }

        return $data;
    }

    // =============================================================================================
    // GET ALL DATA FUNCTION
    // =============================================================================================

    /**
     * Retrieve Stages data
     *
     * @author  Yuda
     * @param   Int     $limit              Limit of stages             default 0
     * @param   Int     $offset             Offset ot stages            default 0
     * @param   String  $conditions         Condition of query          default ''
     * @param   String  $order_by           Column that make to order   default ''
     * @return  Object  Result of data list
     */
    function get_all_stages_data($limit=0, $offset=0, $conditions='', $order_by='', $params=''){
        if( !empty($conditions) ){
            $conditions = str_replace("%id%",               "id", $conditions);
            $conditions = str_replace("%year%",             "year", $conditions);
            $conditions = str_replace("%stage%",            "stage", $conditions);
            $conditions = str_replace("%sub_stage%",        "sub_stage", $conditions);
            $conditions = str_replace("%start_date%",       "start_date", $conditions);
            $conditions = str_replace("%end_date%",         "end_date", $conditions);
            $conditions = str_replace("%status%",           "status", $conditions);
        }

        if( !empty($order_by) ){
            $order_by   = str_replace("%id%",               "id", $order_by);
            $order_by   = str_replace("%year%",             "year", $order_by);
            $order_by   = str_replace("%stage%",            "stage", $order_by);
            $order_by   = str_replace("%sub_stage%",        "sub_stage", $order_by);
            $order_by   = str_replace("%start_date%",       "start_date", $order_by);
            $order_by   = str_replace("%end_date%",         "end_date", $order_by);
            $order_by   = str_replace("%status%",           "status", $order_by);
        }

        $sql = 'SELECT SQL_CALC_FOUND_ROWS * FROM ' . $this->stages . ' WHERE id > 0';

        if( !empty($conditions) )   { $sql .= $conditions; }

        $sql   .= ' ORDER BY '. ( !empty($order_by) ? $order_by : ' year DESC, sort_stage ASC, sort_sub_stage ASC');

        if( $limit ) $sql .= ' LIMIT ' . $offset . ', ' . $limit;

        if ( $params && is_array($params) ) {
            $query = $this->db->query($sql, $params);
        } else {
            $query = $this->db->query($sql);
        }

        if(!$query || !$query->num_rows()) return false;

        return $query->result();
    }

    /**
     * Retrieve all Notification data
     *
     * @author  Yuda
     * @param   Int     $limit              Limit of Data               default 0
     * @param   Int     $offset             Offset ot Data              default 0
     * @param   String  $conditions         Condition of query          default ''
     * @param   String  $order_by           Column that make to order   default ''
     * @return  Object  Result of data list
     */
    function get_all_notification_data($limit=0, $offset=0, $conditions='', $order_by=''){
        if( !empty($conditions) ){
            $conditions = str_replace("%id%",               "id", $conditions);
            $conditions = str_replace("%name%",             "name", $conditions);
            $conditions = str_replace("%slug%",             "slug", $conditions);
            $conditions = str_replace("%type%",             "type", $conditions);
            $conditions = str_replace("%status%",           "status", $conditions);
        }

        if( !empty($order_by) ){
            $order_by   = str_replace("%id%",               "id",  $order_by);
            $order_by   = str_replace("%name%",             "name", $order_by);
            $order_by   = str_replace("%slug%",             "slug", $order_by);
            $order_by   = str_replace("%type%",             "type", $order_by);
            $order_by   = str_replace("%status%",           "status", $order_by);
        }

        $sql = 'SELECT SQL_CALC_FOUND_ROWS * FROM ' . $this->notification . ' WHERE id > 0';

        if( !empty($conditions) ){ $sql .= $conditions; }
        $sql   .= ' ORDER BY '. ( !empty($order_by) ? $order_by : 'type ASC, id ASC');

        if( $limit ) $sql .= ' LIMIT ' . $offset . ', ' . $limit;

        $query = $this->db->query($sql);
        if(!$query || !$query->num_rows()) return false;

        return $query->result();
    }
    
    /**
     * Add Options
     * 
     * @author  Yuda
     * @param   Array/Object    $data   (Required)  Data of option to add
     * @return  Mixed   Boolean false on failed process, invalid data, or data is not found, otherwise Int of Option ID
     */
    function add_option($data){
        if( empty($data) ) return false;
        if( $this->db->insert($this->table, $data) ) {
            $id = $this->db->insert_id();
            return $id;
        };
        return false;
    }
    
    /**
     * Insert Data stages
     * 
     * @author  Yuda
     * @param   Array/Object    $data   (Required)  Data of stages
     * @return  Mixed   Boolean false on failed process, invalid data, or data is not found, otherwise Int of stages ID
     */
    function save_data_stages($data){
        if( empty($data) ) return false;
        if( $this->db->insert($this->stages, $data) ) {
            $id = $this->db->insert_id();
            return $id;
        };
        return false;
    }
    
    /**
     * Update Options
     * 
     * @author  Yuda
     * @param   Array/Object    $data   (Required)  Data of option to update
     * @param   Int             $id     (Required)  ID of Option
     * @return  Mixed   Boolean false on failed process, invalid data, or data is not found, otherwise Int of Option ID
     */
    function update_option($data, $id){
        if( empty($id) ) return false;
        if( empty($data) ) return false;
        if( $this->db->update($this->table, $data, array('id_option' => $id)) ) return true;
        return false;
    }
    
    /**
     * Update stages
     * 
     * @author  Yuda
     * @param   Int             $id     (Required)  ID of stages
     * @param   Array/Object    $data   (Required)  Data of stages to update
     * @return  Mixed   Boolean false on failed process, invalid data, or data is not found, otherwise Int of stages ID
     */
    function update_data_stages($id, $data){
        if( !$id || empty($id) ) return false;
        if( !$data || empty($data) ) return false;

        $this->db->where($this->primary, $id);
        if( $this->db->update($this->stages, $data) ){
            return true;
        }
        return false;
    }

    /**
     * Update Notification
     * 
     * @author  Yuda
     * @param   Int             $id     (Required)  ID of Notification
     * @param   Array/Object    $data   (Required)  Data of Notification to update
     * @return  Mixed   Boolean false on failed process, invalid data, or data is not found, otherwise Int of Notification ID
     */
    function update_data_notification($id, $data){
        if( !$id || empty($id) ) return false;
        if( !$data || empty($data) ) return false;

        $this->db->where($this->primary, $id);
        if( $this->db->update($this->notification, $data) ){
            return true;
        }
        return false;
    }

    /**
     * Delete data of stages
     *
     * @author  Yuda
     * @param   Int     $id   (Required)  ID of data
     * @return  Boolean Boolean false on failed process or invalid data, otherwise true
     */
    function delete_data_stages($id){
        if( empty($id) ) return false;
        $this->db->where($this->primary, $id);
        if( $this->db->delete($this->stages) ) {
            return true;
        };
        return false;
    }
}
/* End of file Model_Option.php */
/* Location: ./application/models/Model_Option.php */
