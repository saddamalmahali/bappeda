<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once('BP_Model.php');

class Model_Master extends BP_Model
{
    public $_table          = 'skpd';

    var $skpd               = TBL_PREFIX . "skpd";
    var $urusan             = TBL_PREFIX . "urusan";
    var $sub_urusan         = TBL_PREFIX . "sub_urusan";
    var $program            = TBL_PREFIX . "program";
    var $kegiatan           = TBL_PREFIX . "kegiatan";
    var $sub_kegiatan       = TBL_PREFIX . "sub_kegiatan";
    var $bidang             = TBL_PREFIX . "bidang";

    /**
     * Save data of Urusan
     *
     * @author  Yuda
     * @param   Array   $data   (Required)  Array data
     * @return  Boolean Boolean false on failed process or invalid data, otherwise true
     */
    function save_urusan($data)
    {
        if (empty($data)) return false;
        if ($this->db->insert($this->urusan, $data)) {
            $id = $this->db->insert_id();
            return $id;
        };
        return false;
    }

    /**
     * Save data of Sub Urusan
     *
     * @author  Yuda
     * @param   Array   $data   (Required)  Array data
     * @return  Boolean Boolean false on failed process or invalid data, otherwise true
     */
    function save_sub_urusan($data)
    {
        if (empty($data)) return false;
        if ($this->db->insert($this->sub_urusan, $data)) {
            $id = $this->db->insert_id();
            return $id;
        };
        return false;
    }

    /**
     * Save data of Program
     *
     * @author  Yuda
     * @param   Array   $data   (Required)  Array data
     * @return  Boolean Boolean false on failed process or invalid data, otherwise true
     */
    function save_program($data)
    {
        if (empty($data)) return false;
        if ($this->db->insert($this->program, $data)) {
            $id = $this->db->insert_id();
            return $id;
        };
        return false;
    }

    /**
     * Save data of Kegiatan
     *
     * @author  Yuda
     * @param   Array   $data   (Required)  Array data
     * @return  Boolean Boolean false on failed process or invalid data, otherwise true
     */
    function save_kegiatan($data)
    {
        if (empty($data)) return false;
        if ($this->db->insert($this->kegiatan, $data)) {
            $id = $this->db->insert_id();
            return $id;
        };
        return false;
    }

    /**
     * Save data of Kegiatan
     *
     * @author  Yuda
     * @param   Array   $data   (Required)  Array data
     * @return  Boolean Boolean false on failed process or invalid data, otherwise true
     */
    function save_sub_kegiatan($data)
    {
        if (empty($data)) return false;
        if ($this->db->insert($this->sub_kegiatan, $data)) {
            $id = $this->db->insert_id();
            return $id;
        };
        return false;
    }

    /**
     * Save data of Bidang
     *
     * @author  Yuda
     * @param   Array   $data   (Required)  Array data
     * @return  Boolean Boolean false on failed process or invalid data, otherwise true
     */
    function save_bidang($data)
    {
        if (empty($data)) return false;
        if ($this->db->insert($this->bidang, $data)) {
            $id = $this->db->insert_id();
            return $id;
        };
        return false;
    }

    /**
     * Save data of SKPD
     *
     * @author  Yuda
     * @param   Array   $data   (Required)  Array data
     * @return  Boolean Boolean false on failed process or invalid data, otherwise true
     */
    function save_skpd($data)
    {
        if (empty($data)) return false;
        if ($this->db->insert($this->skpd, $data)) {
            $id = $this->db->insert_id();
            return $id;
        };
        return false;
    }

    /**
     * Get Urusan
     * 
     * @author  Yuda
     * @param   Int     $id     (Optional)  ID of data
     * @return  Mixed   False on invalid date parameter, otherwise data of data(s).
     */
    function get_urusan($id = '')
    {
        if (!empty($id)) {
            $this->db->where('id', $id);
        }

        $this->db->order_by("id", "ASC");
        $query      = $this->db->get($this->urusan);
        return (!empty($id) ? $query->row() : $query->result());
    }

    /**
     * Get Sub Urusan
     * 
     * @author  Yuda
     * @param   Int     $id     (Optional)  ID of data
     * @return  Mixed   False on invalid date parameter, otherwise data of data(s).
     */
    function get_sub_urusan($id = '')
    {
        if (!empty($id)) {
            $this->db->where('id', $id);
        }

        $this->db->order_by("id", "ASC");
        $query      = $this->db->get($this->sub_urusan);
        return (!empty($id) ? $query->row() : $query->result());
    }

    /**
     * Get Program
     * 
     * @author  Yuda
     * @param   Int     $id     (Optional)  ID of data
     * @return  Mixed   False on invalid date parameter, otherwise data of data(s).
     */
    function get_program($id = '')
    {
        if (!empty($id)) {
            $this->db->where('id', $id);
        }

        $this->db->order_by("id", "ASC");
        $query      = $this->db->get($this->program);
        return (!empty($id) ? $query->row() : $query->result());
    }

    /**
     * Get Kegiatan
     * 
     * @author  Yuda
     * @param   Int     $id     (Optional)  ID of data
     * @return  Mixed   False on invalid date parameter, otherwise data of data(s).
     */
    function get_kegiatan($id = '')
    {
        if (!empty($id)) {
            $this->db->where('id', $id);
        }

        $this->db->order_by("id", "ASC");
        $query      = $this->db->get($this->kegiatan);
        return (!empty($id) ? $query->row() : $query->result());
    }

    /**
     * Get Sub Kegiatan
     * 
     * @author  Yuda
     * @param   Int     $id     (Optional)  ID of data
     * @return  Mixed   False on invalid date parameter, otherwise data of data(s).
     */
    function get_sub_kegiatan($id = '')
    {
        if (!empty($id)) {
            $this->db->where('id', $id);
        }

        $this->db->order_by("id", "ASC");
        $query      = $this->db->get($this->sub_kegiatan);
        return (!empty($id) ? $query->row() : $query->result());
    }

    /**
     * Get Bidang
     * 
     * @author  Yuda
     * @param   Int     $id     (Optional)  ID of data
     * @return  Mixed   False on invalid date parameter, otherwise data of data(s).
     */
    function get_bidang($id = '')
    {
        if (!empty($id)) {
            $this->db->where('id', $id);
        }

        $this->db->order_by("id", "ASC");
        $query      = $this->db->get($this->bidang);
        return (!empty($id) ? $query->row() : $query->result());
    }

    /**
     * Update Urusan
     *
     * @author  Yuda
     * @param   Int     $id     (Required)  ID of data
     * @param   Array   $data   (Required)  Data Array
     * @return  Boolean Boolean false on failed process or invalid data, otherwise true
     */
    function update_data_urusan($id, $data)
    {
        if (!$id || empty($id)) return false;
        if (!$data || empty($data)) return false;

        if (is_array($id)) $this->db->where_in($this->urusan, $id);
        else $this->db->where('id', $id);

        if ($this->db->update($this->urusan, $data))
            return true;

        return false;
    }

    /**
     * Update Sub Urusan
     *
     * @author  Yuda
     * @param   Int     $id     (Required)  ID of data
     * @param   Array   $data   (Required)  Data Array
     * @return  Boolean Boolean false on failed process or invalid data, otherwise true
     */
    function update_data_sub_urusan($id, $data)
    {
        if (!$id || empty($id)) return false;
        if (!$data || empty($data)) return false;

        if (is_array($id)) $this->db->where_in($this->sub_urusan, $id);
        else $this->db->where('id', $id);

        if ($this->db->update($this->sub_urusan, $data))
            return true;

        return false;
    }

    /**
     * Update Program
     *
     * @author  Yuda
     * @param   Int     $id     (Required)  ID of data
     * @param   Array   $data   (Required)  Data Array
     * @return  Boolean Boolean false on failed process or invalid data, otherwise true
     */
    function update_data_program($id, $data)
    {
        if (!$id || empty($id)) return false;
        if (!$data || empty($data)) return false;

        if (is_array($id)) $this->db->where_in($this->program, $id);
        else $this->db->where('id', $id);

        if ($this->db->update($this->program, $data))
            return true;

        return false;
    }

    /**
     * Update Kegiatan
     *
     * @author  Yuda
     * @param   Int     $id     (Required)  ID of data
     * @param   Array   $data   (Required)  Data Array
     * @return  Boolean Boolean false on failed process or invalid data, otherwise true
     */
    function update_data_kegiatan($id, $data)
    {
        if (!$id || empty($id)) return false;
        if (!$data || empty($data)) return false;

        if (is_array($id)) $this->db->where_in($this->kegiatan, $id);
        else $this->db->where('id', $id);

        if ($this->db->update($this->kegiatan, $data))
            return true;

        return false;
    }

    /**
     * Update Sub Kegiatan
     *
     * @author  Yuda
     * @param   Int     $id     (Required)  ID of data
     * @param   Array   $data   (Required)  Data Array
     * @return  Boolean Boolean false on failed process or invalid data, otherwise true
     */
    function update_data_sub_kegiatan($id, $data)
    {
        if (!$id || empty($id)) return false;
        if (!$data || empty($data)) return false;

        if (is_array($id)) $this->db->where_in($this->sub_kegiatan, $id);
        else $this->db->where('id', $id);

        if ($this->db->update($this->sub_kegiatan, $data))
            return true;

        return false;
    }

    /**
     * Update Bidang
     *
     * @author  Yuda
     * @param   Int     $id     (Required)  ID of data
     * @param   Array   $data   (Required)  Data Array
     * @return  Boolean Boolean false on failed process or invalid data, otherwise true
     */
    function update_data_bidang($id, $data)
    {
        if (!$id || empty($id)) return false;
        if (!$data || empty($data)) return false;

        if (is_array($id)) $this->db->where_in($this->bidang, $id);
        else $this->db->where('id', $id);

        if ($this->db->update($this->bidang, $data))
            return true;

        return false;
    }

    /**
     * Update SKPD
     *
     * @author  Yuda
     * @param   Int     $id     (Required)  ID of data
     * @param   Array   $data   (Required)  Data Array
     * @return  Boolean Boolean false on failed process or invalid data, otherwise true
     */
    function update_data_skpd($id, $data)
    {
        if (!$id || empty($id)) return false;
        if (!$data || empty($data)) return false;

        if (is_array($id)) $this->db->where_in($this->skpd, $id);
        else $this->db->where('id', $id);

        if ($this->db->update($this->skpd, $data))
            return true;

        return false;
    }

    /**
     * Delete data of Urusan
     *
     * @author  Yuda
     * @param   Int     $id   (Required)  ID of data
     * @return  Boolean Boolean false on failed process or invalid data, otherwise true
     */
    function delete_data_urusan($id)
    {
        if (empty($id)) return false;
        $this->db->where('id', $id);
        if ($this->db->delete($this->urusan)) {
            return true;
        };
        return false;
    }

    /**
     * Delete data of Sub Urusan
     *
     * @author  Yuda
     * @param   Int     $id   (Required)  ID of data
     * @return  Boolean Boolean false on failed process or invalid data, otherwise true
     */
    function delete_data_sub_urusan($id)
    {
        if (empty($id)) return false;
        $this->db->where('id', $id);
        if ($this->db->delete($this->sub_urusan)) {
            return true;
        };
        return false;
    }

    /**
     * Delete data of Program
     *
     * @author  Yuda
     * @param   Int     $id   (Required)  ID of data
     * @return  Boolean Boolean false on failed process or invalid data, otherwise true
     */
    function delete_data_program($id)
    {
        if (empty($id)) return false;
        $this->db->where('id', $id);
        if ($this->db->delete($this->program)) {
            return true;
        };
        return false;
    }

    /**
     * Delete data of Kegiatan
     *
     * @author  Yuda
     * @param   Int     $id   (Required)  ID of data
     * @return  Boolean Boolean false on failed process or invalid data, otherwise true
     */
    function delete_data_kegiatan($id)
    {
        if (empty($id)) return false;
        $this->db->where('id', $id);
        if ($this->db->delete($this->kegiatan)) {
            return true;
        };
        return false;
    }

    /**
     * Delete data of Sub Kegiatan
     *
     * @author  Yuda
     * @param   Int     $id   (Required)  ID of data
     * @return  Boolean Boolean false on failed process or invalid data, otherwise true
     */
    function delete_data_sub_kegiatan($id)
    {
        if (empty($id)) return false;
        $this->db->where('id', $id);
        if ($this->db->delete($this->sub_kegiatan)) {
            return true;
        };
        return false;
    }

    /**
     * Delete data of Bidang
     *
     * @author  Yuda
     * @param   Int     $id   (Required)  ID of data
     * @return  Boolean Boolean false on failed process or invalid data, otherwise true
     */
    function delete_data_bidang($id)
    {
        if (empty($id)) return false;
        $this->db->where('id', $id);
        if ($this->db->delete($this->bidang)) {
            return true;
        };
        return false;
    }

    /**
     * Delete data of SKPD
     *
     * @author  Saddam
     * @param   Int     $id   (Required)  ID of data
     * @return  Boolean Boolean false on failed process or invalid data, otherwise true
     */
    function delete_data_skpd($id)
    {
        if (empty($id)) return false;
        $this->db->where('id', $id);
        if ($this->db->delete($this->skpd)) {
            return true;
        };
        return false;
    }

    /**
     * Get Urusan by Field
     *
     * @author  Saddam
     * @param   String  $field  (Required)  Database field name or special field name defined inside this function
     * @param   String  $value  (Optional)  Value of the field being searched
     * @return  Mixed   Boolean false on failed process, invalid data, or data is not found, otherwise StdClass of data
     */
    function get_skpd_by($field = '', $value = '', $conditions = '', $limit = 0)
    {
        if ($field) {
            if (!$value) return false;
            $this->db->where($field, $value);
        }

        $this->db->where('unit', '0000');

        if ($conditions) {
            $this->db->where($conditions);
        }

        $this->db->order_by("id", "ASC");

        $query  = $this->db->get($this->skpd);
        if (!$query->num_rows()) {
            return false;
        }

        $data   = $query->result();
        if ($field == 'id' || $limit == 1) {
            foreach ($data as $row) {
                $datarow = $row;
            }
            $data = $datarow;
        }

        return $data;
    }

    /**
     * Get Urusan by Field
     *
     * @author  Saddam
     * @param   String  $field  (Required)  Database field name or special field name defined inside this function
     * @param   String  $value  (Optional)  Value of the field being searched
     * @return  Mixed   Boolean false on failed process, invalid data, or data is not found, otherwise StdClass of data
     */
    function get_urusan_by($field = '', $value = '', $conditions = '', $limit = 0)
    {
        if (!$field || !$value) return false;

        $this->db->where($field, $value);
        if ($conditions) {
            $this->db->where($conditions);
        }

        $this->db->order_by("id", "ASC");
        $query  = $this->db->get($this->urusan);
        if (!$query->num_rows()) {
            return false;
        }

        $data   = $query->result();
        if ($field == 'id' || $limit == 1) {
            foreach ($data as $row) {
                $datarow = $row;
            }
            $data = $datarow;
        }

        return $data;
    }

    /**
     * Get Urusan by Field
     *
     * @author  Saddam
     * @param   String  $field  (Required)  Database field name or special field name defined inside this function
     * @param   String  $value  (Optional)  Value of the field being searched
     * @return  Mixed   Boolean false on failed process, invalid data, or data is not found, otherwise StdClass of data
     */
    function get_sub_urusan_by($field = '', $value = '', $conditions = '', $limit = 0)
    {
        if (!$field || !$value) return false;

        $this->db->where($field, $value);
        if ($conditions) {
            $this->db->where($conditions);
        }

        $this->db->order_by("id", "ASC");
        $query  = $this->db->get($this->sub_urusan);
        if (!$query->num_rows()) {
            return false;
        }

        $data   = $query->result();
        if ($field == 'id' || $limit == 1) {
            foreach ($data as $row) {
                $datarow = $row;
            }
            $data = $datarow;
        }

        return $data;
    }

    /**
     * Get Program by Field
     *
     * @author  Saddam
     * @param   String  $field  (Required)  Database field name or special field name defined inside this function
     * @param   String  $value  (Optional)  Value of the field being searched
     * @return  Mixed   Boolean false on failed process, invalid data, or data is not found, otherwise StdClass of data
     */
    function get_program_by($field = '', $value = '', $conditions = '', $limit = 0)
    {
        if (!$field || !$value) return false;

        $this->db->where($field, $value);
        if ($conditions) {
            $this->db->where($conditions);
        }

        $this->db->order_by("id", "ASC");
        $query  = $this->db->get($this->program);
        if (!$query->num_rows()) {
            return false;
        }

        $data   = $query->result();
        if ($field == 'id' || $limit == 1) {
            foreach ($data as $row) {
                $datarow = $row;
            }
            $data = $datarow;
        }

        return $data;
    }

    /**
     * Get Kegiatan by Field
     *
     * @author  Saddam
     * @param   String  $field  (Required)  Database field name or special field name defined inside this function
     * @param   String  $value  (Optional)  Value of the field being searched
     * @return  Mixed   Boolean false on failed process, invalid data, or data is not found, otherwise StdClass of data
     */
    function get_kegiatan_by($field = '', $value = '', $conditions = '', $limit = 0)
    {
        if (!$field || !$value) return false;

        $this->db->where($field, $value);
        if ($conditions) {
            $this->db->where($conditions);
        }

        $this->db->order_by("id", "ASC");
        $query  = $this->db->get($this->kegiatan);
        if (!$query->num_rows()) {
            return false;
        }

        $data   = $query->result();
        if ($field == 'id' || $limit == 1) {
            foreach ($data as $row) {
                $datarow = $row;
            }
            $data = $datarow;
        }

        return $data;
    }

    /**
     * Get Sub Kegiatan by Field
     *
     * @author  Saddam
     * @param   String  $field  (Required)  Database field name or special field name defined inside this function
     * @param   String  $value  (Optional)  Value of the field being searched
     * @return  Mixed   Boolean false on failed process, invalid data, or data is not found, otherwise StdClass of data
     */
    function get_sub_kegiatan_by($field = '', $value = '', $conditions = '', $limit = 0)
    {
        if (!$field || !$value) return false;

        $this->db->where($field, $value);
        if ($conditions) {
            $this->db->where($conditions);
        }

        $this->db->order_by("id", "ASC");
        $query  = $this->db->get($this->sub_kegiatan);
        if (!$query->num_rows()) {
            return false;
        }

        $data   = $query->result();
        if ($field == 'id' || $limit == 1) {
            foreach ($data as $row) {
                $datarow = $row;
            }
            $data = $datarow;
        }

        return $data;
    }

    /**
     * Get Bidang by Field
     *
     * @author  Saddam
     * @param   String  $field  (Required)  Database field name or special field name defined inside this function
     * @param   String  $value  (Optional)  Value of the field being searched
     * @return  Mixed   Boolean false on failed process, invalid data, or data is not found, otherwise StdClass of data
     */
    function get_bidang_by($field = '', $value = '', $conditions = '', $limit = 0)
    {
        if ($field) {
            if (!$value) return false;
            $this->db->where($field, $value);
        }

        if ($conditions) {
            $this->db->where($conditions);
        }

        $this->db->order_by("id", "ASC");
        $query  = $this->db->get($this->bidang);
        if (!$query->num_rows()) {
            return false;
        }

        $data   = $query->result();
        if ($field == 'id' || $limit == 1) {
            foreach ($data as $row) {
                $datarow = $row;
            }
            $data = $datarow;
        }

        return $data;
    }

    /**
     * Retrieve all urusan data
     *
     * @author  Yuda
     * @param   Int     $limit              Limit of member             default 0
     * @param   Int     $offset             Offset ot member            default 0
     * @param   String  $conditions         Condition of query          default ''
     * @param   String  $order_by           Column that make to order   default ''
     * @return  Object  Result of data list
     */
    function get_all_urusan_data($limit = 0, $offset = 0, $conditions = '', $order_by = '', $params = '', $num_rows = false)
    {
        if (!empty($conditions)) {
            $conditions = str_replace("%id%",       "id", $conditions);
            $conditions = str_replace("%code%",     "kode", $conditions);
            $conditions = str_replace("%name%",     "nama", $conditions);
        }

        if (!empty($order_by)) {
            $order_by   = str_replace("%id%",       "id",  $order_by);
            $order_by   = str_replace("%code%",     "kode", $order_by);
            $order_by   = str_replace("%name%",     "nama", $order_by);
        }

        $sql = 'SELECT SQL_CALC_FOUND_ROWS * FROM ' . $this->urusan . ' WHERE id > 0';

        if (!empty($conditions)) {
            $sql .= $conditions;
        }

        $sql   .= ' ORDER BY ' . (!empty($order_by) ? $order_by : 'kode ASC');

        if ($limit) $sql .= ' LIMIT ' . $offset . ', ' . $limit;

        if ($params && is_array($params)) {
            $query = $this->db->query($sql, $params);
        } else {
            $query = $this->db->query($sql);
        }

        if (!$query || !$query->num_rows()) return false;

        if ($num_rows) {
            return $query->num_rows();
        }

        return $query->result();
    }

    /**
     * Retrieve all suburusan data
     *
     * @author  Yuda
     * @param   Int     $limit              Limit of member             default 0
     * @param   Int     $offset             Offset ot member            default 0
     * @param   String  $conditions         Condition of query          default ''
     * @param   String  $order_by           Column that make to order   default ''
     * @return  Object  Result of data list
     */
    function get_all_suburusan_data($limit = 0, $offset = 0, $conditions = '', $order_by = '', $params = '', $num_rows = false)
    {
        if (!empty($conditions)) {
            $conditions = str_replace("%id%",           "A.id", $conditions);
            $conditions = str_replace("%id_urusan%",    "A.id_urusan", $conditions);
            $conditions = str_replace("%code%",         "A.kode", $conditions);
            $conditions = str_replace("%name%",         "A.nama", $conditions);
            $conditions = str_replace("%urusan_code%",  "B.kode", $conditions);
            $conditions = str_replace("%urusan_name%",  "B.nama", $conditions);
        }

        if (!empty($order_by)) {
            $order_by   = str_replace("%id%",           "A.id",  $order_by);
            $order_by   = str_replace("%id_urusan%",    "A.id_urusan",  $order_by);
            $order_by   = str_replace("%code%",         "A.kode", $order_by);
            $order_by   = str_replace("%name%",         "A.nama", $order_by);
            $order_by   = str_replace("%urusan_code%",  "B.kode", $order_by);
            $order_by   = str_replace("%urusan_name%",  "B.nama", $order_by);
        }

        $sql = 'SELECT SQL_CALC_FOUND_ROWS 
                    A.*,
                    B.kode AS urusan_code, 
                    B.nama AS urusan_name 
                FROM ' . $this->sub_urusan . ' AS A 
                INNER JOIN ' . $this->urusan . ' AS B ON (A.id_urusan = B.id)
                WHERE A.id > 0';

        if (!empty($conditions)) {
            $sql .= $conditions;
        }

        $sql   .= ' ORDER BY ' . (!empty($order_by) ? $order_by : 'A.kode ASC');

        if ($limit) $sql .= ' LIMIT ' . $offset . ', ' . $limit;

        if ($params && is_array($params)) {
            $query = $this->db->query($sql, $params);
        } else {
            $query = $this->db->query($sql);
        }

        if (!$query || !$query->num_rows()) return false;

        if ($num_rows) {
            return $query->num_rows();
        }

        return $query->result();
    }

    /**
     * Retrieve all program data
     *
     * @author  Yuda
     * @param   Int     $limit              Limit of member             default 0
     * @param   Int     $offset             Offset ot member            default 0
     * @param   String  $conditions         Condition of query          default ''
     * @param   String  $order_by           Column that make to order   default ''
     * @return  Object  Result of data list
     */
    function get_all_program_data($limit = 0, $offset = 0, $conditions = '', $order_by = '', $params = '', $num_rows = false)
    {
        if (!empty($conditions)) {
            $conditions = str_replace("%id%",               "A.id", $conditions);
            $conditions = str_replace("%id_urusan%",        "A.id_urusan", $conditions);
            $conditions = str_replace("%code%",             "A.kode", $conditions);
            $conditions = str_replace("%name%",             "A.nama", $conditions);
            $conditions = str_replace("%suburusan_code%",   "B.kode", $conditions);
            $conditions = str_replace("%suburusan_name%",   "B.nama", $conditions);
        }

        if (!empty($order_by)) {
            $order_by   = str_replace("%id%",               "A.id",  $order_by);
            $order_by   = str_replace("%id_urusan%",        "A.id_urusan",  $order_by);
            $order_by   = str_replace("%code%",             "A.kode", $order_by);
            $order_by   = str_replace("%name%",             "A.nama", $order_by);
            $order_by   = str_replace("%suburusan_code%",   "B.kode", $order_by);
            $order_by   = str_replace("%suburusan_name%",   "B.nama", $order_by);
        }

        $sql = 'SELECT SQL_CALC_FOUND_ROWS 
                    A.*,
                    B.kode AS suburusan_code, 
                    B.nama AS suburusan_name 
                FROM ' . $this->program . ' AS A 
                INNER JOIN ' . $this->sub_urusan . ' AS B ON (A.id_sub_urusan = B.id)
                WHERE A.id > 0';

        if (!empty($conditions)) {
            $sql .= $conditions;
        }

        $sql   .= ' ORDER BY ' . (!empty($order_by) ? $order_by : 'A.kode ASC');

        if ($limit) $sql .= ' LIMIT ' . $offset . ', ' . $limit;

        if ($params && is_array($params)) {
            $query = $this->db->query($sql, $params);
        } else {
            $query = $this->db->query($sql);
        }

        if (!$query || !$query->num_rows()) return false;

        if ($num_rows) {
            return $query->num_rows();
        }

        return $query->result();
    }

    /**
     * Retrieve all kegiatan data
     *
     * @author  Yuda
     * @param   Int     $limit              Limit of member             default 0
     * @param   Int     $offset             Offset ot member            default 0
     * @param   String  $conditions         Condition of query          default ''
     * @param   String  $order_by           Column that make to order   default ''
     * @return  Object  Result of data list
     */
    function get_all_kegiatan_data($limit = 0, $offset = 0, $conditions = '', $order_by = '', $params = '', $num_rows = false)
    {
        if (!empty($conditions)) {
            $conditions = str_replace("%id%",               "A.id", $conditions);
            $conditions = str_replace("%id_urusan%",        "A.id_urusan", $conditions);
            $conditions = str_replace("%code%",             "A.kode", $conditions);
            $conditions = str_replace("%name%",             "A.nama", $conditions);
            $conditions = str_replace("%program_code%",     "B.kode", $conditions);
            $conditions = str_replace("%program_name%",     "B.nama", $conditions);
        }

        if (!empty($order_by)) {
            $order_by   = str_replace("%id%",               "A.id",  $order_by);
            $order_by   = str_replace("%id_urusan%",        "A.id_urusan",  $order_by);
            $order_by   = str_replace("%code%",             "A.kode", $order_by);
            $order_by   = str_replace("%name%",             "A.nama", $order_by);
            $order_by   = str_replace("%program_code%",     "B.kode", $order_by);
            $order_by   = str_replace("%program_name%",     "B.nama", $order_by);
        }

        $sql = 'SELECT SQL_CALC_FOUND_ROWS 
                    A.*,
                    B.kode AS program_code, 
                    B.nama AS program_name 
                FROM ' . $this->kegiatan . ' AS A 
                INNER JOIN ' . $this->program . ' AS B ON (A.id_program = B.id)
                WHERE A.id > 0';

        if (!empty($conditions)) {
            $sql .= $conditions;
        }

        $sql   .= ' ORDER BY ' . (!empty($order_by) ? $order_by : 'A.kode ASC');

        if ($limit) $sql .= ' LIMIT ' . $offset . ', ' . $limit;

        if ($params && is_array($params)) {
            $query = $this->db->query($sql, $params);
        } else {
            $query = $this->db->query($sql);
        }

        if (!$query || !$query->num_rows()) return false;

        if ($num_rows) {
            return $query->num_rows();
        }

        return $query->result();
    }

    /**
     * Retrieve all subkegiatan data
     *
     * @author  Yuda
     * @param   Int     $limit              Limit of member             default 0
     * @param   Int     $offset             Offset ot member            default 0
     * @param   String  $conditions         Condition of query          default ''
     * @param   String  $order_by           Column that make to order   default ''
     * @return  Object  Result of data list
     */
    function get_all_subkegiatan_data($limit = 0, $offset = 0, $conditions = '', $order_by = '', $params = '', $num_rows = false)
    {
        if (!empty($conditions)) {
            $conditions = str_replace("%id%",               "A.id", $conditions);
            $conditions = str_replace("%id_urusan%",        "A.id_urusan", $conditions);
            $conditions = str_replace("%code%",             "A.kode", $conditions);
            $conditions = str_replace("%name%",             "A.nama", $conditions);
            $conditions = str_replace("%kegiatan_code%",    "B.kode", $conditions);
            $conditions = str_replace("%kegiatan_name%",    "B.nama", $conditions);
        }

        if (!empty($order_by)) {
            $order_by   = str_replace("%id%",               "A.id",  $order_by);
            $order_by   = str_replace("%id_urusan%",        "A.id_urusan",  $order_by);
            $order_by   = str_replace("%code%",             "A.kode", $order_by);
            $order_by   = str_replace("%name%",             "A.nama", $order_by);
            $order_by   = str_replace("%kegiatan_code%",    "B.kode", $order_by);
            $order_by   = str_replace("%kegiatan_name%",    "B.nama", $order_by);
        }

        $sql = 'SELECT SQL_CALC_FOUND_ROWS 
                    A.*,
                    B.kode AS kegiatan_code, 
                    B.nama AS kegiatan_name 
                FROM ' . $this->sub_kegiatan . ' AS A 
                INNER JOIN ' . $this->kegiatan . ' AS B ON (A.id_kegiatan = B.id)
                WHERE A.id > 0';

        if (!empty($conditions)) {
            $sql .= $conditions;
        }

        $sql   .= ' ORDER BY ' . (!empty($order_by) ? $order_by : 'A.kode ASC');

        if ($limit) $sql .= ' LIMIT ' . $offset . ', ' . $limit;

        if ($params && is_array($params)) {
            $query = $this->db->query($sql, $params);
        } else {
            $query = $this->db->query($sql);
        }

        if (!$query || !$query->num_rows()) return false;

        if ($num_rows) {
            return $query->num_rows();
        }

        return $query->result();
    }

    /**
     * Retrieve all bidang data
     *
     * @author  Yuda
     * @param   Int     $limit              Limit of member             default 0
     * @param   Int     $offset             Offset ot member            default 0
     * @param   String  $conditions         Condition of query          default ''
     * @param   String  $order_by           Column that make to order   default ''
     * @return  Object  Result of data list
     */
    function get_all_bidang_data($limit = 0, $offset = 0, $conditions = '', $order_by = '', $params = '', $num_rows = false)
    {
        if (!empty($conditions)) {
            $conditions = str_replace("%id%",       "id", $conditions);
            $conditions = str_replace("%name%",     "nama", $conditions);
            $conditions = str_replace("%all_skpd%", "all_skpd", $conditions);
        }

        if (!empty($order_by)) {
            $order_by   = str_replace("%id%",       "id",  $order_by);
            $order_by   = str_replace("%name%",     "nama", $order_by);
            $order_by   = str_replace("%all_skpd%", "all_skpd", $order_by);
        }

        $sql = 'SELECT SQL_CALC_FOUND_ROWS * FROM ' . $this->bidang . ' WHERE id > 0';

        if (!empty($conditions)) {
            $sql .= $conditions;
        }

        $sql   .= ' ORDER BY ' . (!empty($order_by) ? $order_by : 'id ASC');

        if ($limit) $sql .= ' LIMIT ' . $offset . ', ' . $limit;

        if ($params && is_array($params)) {
            $query = $this->db->query($sql, $params);
        } else {
            $query = $this->db->query($sql);
        }

        if (!$query || !$query->num_rows()) return false;

        if ($num_rows) {
            return $query->num_rows();
        }

        return $query->result();
    }

    /**
     * Retrieve all SKPD data
     *
     * @author  Yuda
     * @param   Int     $limit              Limit of member             default 0
     * @param   Int     $offset             Offset ot member            default 0
     * @param   String  $conditions         Condition of query          default ''
     * @param   String  $order_by           Column that make to order   default ''
     * @return  Object  Result of data list
     */
    function get_all_skpd_data($limit = 0, $offset = 0, $conditions = '', $order_by = '', $params = '', $num_rows = false)
    {
        if (!empty($conditions)) {
            $conditions = str_replace("%id%",       "A.id", $conditions);
            $conditions = str_replace("%code%",     "A.kode", $conditions);
            $conditions = str_replace("%name%",     "A.nama", $conditions);
            $conditions = str_replace("%u1%",       "A.u1", $conditions);
            $conditions = str_replace("%u2%",       "A.u2", $conditions);
            $conditions = str_replace("%u3%",       "A.u3", $conditions);
            $conditions = str_replace("%urut%",     "A.urut", $conditions);
            $conditions = str_replace("%bidang%",   "A.id_bidang", $conditions);
            $conditions = str_replace("%type%",     "A.type", $conditions);
        }

        if (!empty($order_by)) {
            $order_by   = str_replace("%id%",       "A.id",  $order_by);
            $order_by   = str_replace("%code%",     "A.kode", $order_by);
            $order_by   = str_replace("%name%",     "A.nama", $order_by);
            $order_by   = str_replace("%bidang%",   "B.nama", $order_by);
            $order_by   = str_replace("%type%",   "A.type", $order_by);
            $order_by   = str_replace("%u1%",       "A.u1", $order_by);
            $order_by   = str_replace("%u2%",       "A.u2", $order_by);
            $order_by   = str_replace("%u3%",       "A.u3", $order_by);
            $order_by   = str_replace("%urut%",     "A.urut", $order_by);
        }

        $sql = '
                SELECT SQL_CALC_FOUND_ROWS
                    A.*
                FROM
                    ' . $this->skpd . ' AS A
                    LEFT JOIN ' . $this->bidang . ' AS B ON B.id = A.id_bidang 
                WHERE
                    A.id > 0 ';

        if (!empty($conditions)) {
            $sql .= $conditions;
        }

        $sql   .= ' ORDER BY ' . (!empty($order_by) ? $order_by : ' A.id ASC');

        if ($limit) $sql .= ' LIMIT ' . $offset . ', ' . $limit;

        if ($params && is_array($params)) {
            $query = $this->db->query($sql, $params);
        } else {
            $query = $this->db->query($sql);
        }

        if (!$query || !$query->num_rows()) return false;

        if ($num_rows) {
            return $query->num_rows();
        }

        return $query->result();
    }
}
