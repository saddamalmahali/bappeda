<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Frontend extends BP_Controller
{

    /**
     * Constructor.
     */
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Home Page
     */
    public function index()
    {
        $data['title']          = 'Home';
        $data['main_content']   = 'pages/home';
        $this->load->view(VIEW_FRONT . 'template', $data);
    }

    /**
     * About Us Page
     */
    public function aboutUs()
    {
        $data['title']          = 'About Us';
        $data['main_content']   = 'pages/about_us';
        $this->load->view(VIEW_FRONT . 'template', $data);
    }

    /**
     * Contact Us Page
     */
    public function contact()
    {
        $data['title']          = 'Contact Us';
        $data['main_content']   = 'pages/contact';
        $this->load->view(VIEW_FRONT . 'template', $data);
    }

    /**
     * Company Page
     */
    public function company()
    {
        $data['title']          = 'Company';
        $data['main_content']   = 'pages/company';
        $this->load->view(VIEW_FRONT . 'template', $data);
    }

    /**
     * Product Page
     */
    public function productSkinCare()
    {
        $data['title']          = 'Skin Caree';
        $data['main_content']   = 'pages/products/skin_care';
        $this->load->view(VIEW_FRONT . 'template', $data);
    }

    /**
     * Product Page
     */
    public function productSoap()
    {
        $data['title']          = 'Soap';
        $data['main_content']   = 'pages/products/soap';
        $this->load->view(VIEW_FRONT . 'template', $data);
    }

    /**
     * Testimoni Page
     */
    public function testimonial()
    {
        $data['title']          = 'Testimoni';
        $data['main_content']   = 'pages/testimonial';
        $this->load->view(VIEW_FRONT . 'template', $data);
    }

    /**
     * Event Page
     */
    public function event()
    {
        $data['title']          = 'Event';
        $data['main_content']   = 'pages/event';
        $this->load->view(VIEW_FRONT . 'template', $data);
    }
}
