<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Debug Controller.
 *
 * @class     Debug
 * @version   1.0.0
 */
class Debug extends Public_Controller
{

    /**
     * Update Member HLR Code of Phone
     * @author	Iqbal
     */
    public function decode_pwd($password = '')
    {
        $this->benchmark->mark('started');
        $this->load->library('user_agent');

        $password   = $password ? $password : 'P@SS4kaidah';
        $password   = $password ? $password : '123@kaidah';
        $pass       = bp_password_hash($password);
        $encrypt    = bp_encrypt($password);

        echo "<pre>";

        echo "Password : " . $password . br();
        echo "----------------------------------------------" . br();
        echo "  encrypt  " . br();
        echo "----------------------------------------------" . br();
        echo "MD5      : " . md5($password) . br();
        echo "Hash     : " . $pass . br(2);
        echo "encrypt  : " . $encrypt . br(2);
        echo "----------------------------------------------" . br();

        if (password_verify($password, $pass)) {
            echo 'Password is valid!';
        } else {
            echo 'Invalid password.';
        }
        echo br(5);

        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }

        echo 'browser : ' .  $this->agent->browser() . ' ' . $this->agent->version() . br();
        echo 'mobile : ' .  $this->agent->mobile() . br();
        echo 'robot : ' .  $this->agent->robot() . br();
        echo 'referrer : ' .  $this->agent->referrer() . br(3);

        echo 'agent : ' .  $agent . br();
        echo $this->agent->platform() . br();
        echo $this->agent->agent_string() . br();

        $this->benchmark->mark('ended');
        $elapsed_time = $this->benchmark->elapsed_time('started', 'ended');
        echo br() . 'Elapsed Time : ' . $elapsed_time . ' seconds' . "\n";
        echo "</pre>";
    }

    function email_register($id_member = 0, $send = false)
    {
        set_time_limit(0);
        $this->benchmark->mark('started');

        if (!$id_member) die('Member not found!');

        if (!$member  = bp_get_memberdata_by_id($id_member)) die('Member not found!');
        if (!$sponsor = bp_get_memberdata_by_id($member->sponsor)) die('Sponsor not found!');
        if (!$upline  = bp_get_memberdata_by_id($member->parent)) die('Upline not found!');

        $member->email  = 'developer.dhaeka@gmail.com';
        $member->phone  = '085211838515';
        $sponsor->email = 'developer.dhaeka@gmail.com';
        $sponsor->phone = '085211838515';
        $upline->email  = 'developer.dhaeka@gmail.com';
        $rand           = random_string('alnum', 8);

        echo '<pre style="color:#111">';
        echo '----------------------------------------------------' . br();
        echo '              Send Notif New Member ' . br();
        echo '----------------------------------------------------' . br();
        echo ' ID Member    : ' . $member->id . br();
        echo ' Username     : ' . $member->username . br();
        echo ' Name         : ' . $member->name . br();
        echo ' Email        : ' . $member->email . br();
        echo ' Phone        : ' . $member->phone . br();
        echo '----------------------------------------------------' . br();
        echo ' Password     : ' . $rand . br();
        echo '----------------------------------------------------' . br();
        echo ' Sponsor      : ' . $sponsor->username . ' / ' . $sponsor->name . br();
        echo ' Email        : ' . $sponsor->email . br();
        echo ' Phone        : ' . $sponsor->phone . br();
        echo '----------------------------------------------------' . br();
        echo ' Upline       : ' . $upline->username . ' / ' . $upline->name . br();
        echo '----------------------------------------------------' . br(3);

        if ($send) {
            $this->bp_email->send_email_new_member($member, $sponsor, $rand);
            $this->bp_email->send_email_sponsor($member, $sponsor, $upline);
            $this->bp_wa->send_wa_sponsor($member, $sponsor, $upline);
        } else {
            $wa = $this->bp_wa->send_wa_new_member($member, $sponsor, $rand, TRUE);
            echo '----------------------------------------------------' . br();
            echo 'WhatsApp New Member : ' . br();
            echo '----------------------------------------------------' . br(2);
            echo $wa;
            echo br(3);

            $wa_sponsor = $this->bp_wa->send_wa_sponsor($member, $sponsor, $upline, TRUE);
            echo '----------------------------------------------------' . br();
            echo 'WhatsApp Sponsor : ' . br();
            echo '----------------------------------------------------' . br(2);
            echo $wa_sponsor;
            echo br(3);

            $mail = $this->bp_email->send_email_new_member($member, $sponsor, $rand, TRUE);
            if (isset($mail->html)) {
                echo 'Email New Member : ' . br();
                echo '----------------------------------------------------' . br(2);
                echo '</pre>';
                echo $mail->html;
                echo '<pre>';
                echo br(3);
            }

            $mail_sponsor = $this->bp_email->send_email_sponsor($member, $sponsor, $upline, TRUE);
            if (isset($mail_sponsor->html)) {
                echo 'Email Sponsor : ' . br();
                echo '----------------------------------------------------' . br(2);
                echo '</pre>';
                echo $mail_sponsor->html;
                echo '<pre>';
                echo br(3);
            }
        }

        echo br(2) . '-----------------------------------------' . br();
        $this->benchmark->mark('ended');
        $elapsed_time = $this->benchmark->elapsed_time('started', 'ended');
        echo 'Elapsed Time: ' . $elapsed_time . ' seconds';
        echo '</pre>';
    }

    public function reader()
    {
        /** Create a new Xls Reader  **/
        // $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
        $reader                         = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        //    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xml();
        //    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Ods();
        //    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Slk();
        //    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Gnumeric();
        //    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();

        return $reader;
    }

    function import_data_bidang($debug = TRUE)
    {
        $file_src                       = IMPORT_PATH . 'bidang.xlsx';
        $data                           = array();

        $reader                         = $this->reader();
        /** Load $inputFileName to a Spreadsheet Object  **/
        $spreadsheet                    = $reader->load($file_src);
        if ($spreadsheet) {
            $this->db->truncate(TBL_PREFIX . 'bidang');
            foreach ($spreadsheet->getWorksheetIterator() as $worksheet) {
                $highestRow             = $worksheet->getHighestRow();
                $highestColumn          = $worksheet->getHighestColumn();
                $data['higgestRow']     = $highestRow;
                $data['highestColumn']  = $highestColumn;

                for ($row = 2; $row <= $highestRow; $row++) {

                    $nama           = $worksheet->getCellByColumnAndRow(2, $row)->getValue();

                    $data['data'][] = array(
                        'nama'                  => $nama
                    );
                }
            }

            if (is_array($data['data']) && (count($data['data']))) {
                $this->db->insert_batch(TBL_PREFIX . 'bidang', $data['data']);
            }

            var_dump($data);
            die();
        }
    }

    function import_data_skpd($debug = TRUE)
    {
        $file_src                       = IMPORT_PATH . 'skpd.xlsx';
        $data                           = array();

        $reader                         = $this->reader();

        /** Load $inputFileName to a Spreadsheet Object  **/
        $spreadsheet                    = $reader->load($file_src);
        if ($spreadsheet) {
            $this->db->truncate(TBL_PREFIX . 'skpd');
            foreach ($spreadsheet->getWorksheetIterator() as $worksheet) {
                $highestRow             = $worksheet->getHighestRow();
                $highestColumn          = $worksheet->getHighestColumn();
                $data['higgestRow']     = $highestRow;
                $data['highestColumn']  = $highestColumn;

                for ($row = 2; $row <= $highestRow; $row++) {

                    $nama           = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                    $u1             = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                    $u2             = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                    $u3             = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
                    $urut           = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
                    $unit           = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
                    $kode           = $worksheet->getCellByColumnAndRow(8, $row)->getValue();
                    $id_bidang      = $worksheet->getCellByColumnAndRow(9, $row)->getValue();

                    $bidang         = $this->Model_Master->get_bidang_by('id', $id_bidang, '', 1);
                    $bidang         = isset($bidang) ? $bidang : '';

                    $data['data'][] = array(
                        'nama'              => $nama,
                        'u1'                => $u1,
                        'u2'                => $u2,
                        'u3'                => $u3,
                        'urut'              => $urut,
                        'unit'              => $unit,
                        'kode'              => $kode,
                        'id_bidang'         => $bidang ? $bidang->id : $id_bidang,

                    );
                }
            }
            if (is_array($data['data']) && (count($data['data']))) {
                $this->db->insert_batch(TBL_PREFIX . 'skpd', $data['data']);
            }

            var_dump($data);
            die();
        }
    }

    function import_data_skpd_unit($debug = TRUE)
    {
        $file_src                       = IMPORT_PATH . 'skpd_unit.xlsx';
        $data                           = array();

        $reader                         = $this->reader();

        /** Load $inputFileName to a Spreadsheet Object  **/
        $spreadsheet                    = $reader->load($file_src);
        if ($spreadsheet) {
            $this->db->truncate(TBL_PREFIX . 'skpd_unit');
            foreach ($spreadsheet->getWorksheetIterator() as $worksheet) {
                $highestRow             = $worksheet->getHighestRow();
                $highestColumn          = $worksheet->getHighestColumn();
                $data['higgestRow']     = $highestRow;
                $data['highestColumn']  = $highestColumn;

                for ($row = 2; $row <= $highestRow; $row++) {

                    $nama           = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                    $u1             = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                    $u2             = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                    $u3             = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
                    $urut           = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
                    $unit           = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
                    $kode           = $worksheet->getCellByColumnAndRow(8, $row)->getValue();
                    $id_skpd        = $worksheet->getCellByColumnAndRow(9, $row)->getValue();

                    $skpd           = $this->Model_Master->get_skpd_by('id', $id_skpd, '', 1);
                    $skpd           = isset($skpd) ? $skpd : '';

                    $data['data'][] = array(
                        'nama'              => $nama,
                        'u1'                => $u1,
                        'u2'                => $u2,
                        'u3'                => $u3,
                        'urut'              => $urut,
                        'unit'              => $unit,
                        'kode'              => $kode,
                        'id_skpd'           => $skpd ? $skpd->id : $id_skpd,

                    );
                }
            }
            if (is_array($data['data']) && (count($data['data']))) {
                $this->db->insert_batch(TBL_PREFIX . 'skpd_unit', $data['data']);
            }

            var_dump($data);
            die();
        }
    }

    function import_data_urusan($debug = TRUE)
    {
        $file_src                       = IMPORT_PATH . 'urusan.xlsx';
        $migrate                        = bp_import_urusan($file_src);

        if (!$migrate) {
            $data['status']             = 'error';
            $data['message']            = 'Gagal Import Data Urusan';
            $data['data']               = null;
            die(json_encode($data));
        }

        $data['status']                         = 'success';
        $data['message']                        = 'Import Data Urusan Berhasil';
        $data['data']                           = $migrate;

        echo '<pre>';
        die(json_encode($data));
    }

    function import_data_sub_urusan($debug = TRUE)
    {
        $file_src                       = IMPORT_PATH . 'sub_urusan.xlsx';
        $migrate                        = bp_import_sub_urusan($file_src);

        if (!$migrate) {
            $data['status']             = 'error';
            $data['message']            = 'Gagal Import Data Sub Urusan';
            $data['data']               = null;
            die(json_encode($data));
        }

        $data['status']                         = 'success';
        $data['message']                        = 'Import Data Sub Urusan Berhasil';
        $data['data']                           = $migrate;

        echo '<pre>';
        die(json_encode($data));
    }

    function import_data_program($debug = TRUE)
    {
        $file_src                       = IMPORT_PATH . 'program.xlsx';
        $migrate                        = bp_import_program($file_src);

        if (!$migrate) {
            $data['status']             = 'error';
            $data['message']            = 'Gagal Import Data Program';
            $data['data']               = null;
            die(json_encode($data));
        }

        $data['status']                         = 'success';
        $data['message']                        = 'Import Data Program Berhasil';
        $data['data']                           = $migrate;

        echo '<pre>';
        die(json_encode($data));
    }

    function import_data_kegiatan($debug = TRUE)
    {
        $file_src                       = IMPORT_PATH . 'kegiatan.xlsx';
        $migrate                        = bp_import_kegiatan($file_src);

        if (!$migrate) {
            $data['status']             = 'error';
            $data['message']            = 'Gagal Import Data Kegiatan';
            $data['data']               = null;
            die(json_encode($data));
        }

        $data['status']                         = 'success';
        $data['message']                        = 'Import Data Kegiatan Berhasil';
        $data['data']                           = $migrate;
        echo '<pre>';
        die(json_encode($data));
    }

    function import_data_sub_kegiatan($debug = TRUE)
    {
        $file_src                       = IMPORT_PATH . 'sub_kegiatan.xlsx';
        $migrate                        = bp_import_sub_kegiatan($file_src);

        if (!$migrate) {
            $data['status']             = 'error';
            $data['message']            = 'Gagal Import Data Sub Kegiatan';
            $data['data']               = null;
            die(json_encode($data));
        }

        $data['status']                         = 'success';
        $data['message']                        = 'Import Data Sub Kegiatan Berhasil';
        $data['data']                           = $migrate;
        echo '<pre>';
        die(json_encode($data));
    }

    function import_data_transaction($debug = TRUE)
    {
        $this->benchmark->mark('started');

        $file_src                       = IMPORT_PATH . 'rkpd.xlsx';
        $migrate                        = bp_import_transaction($file_src);

        if (!$migrate) {
            $data['status']             = 'error';
            $data['message']            = 'Gagal Import Data Urusan';
            $data['data']               = null;
            die(json_encode($data));
        }

        $data['status']                         = 'success';
        $data['message']                        = 'Import Data Urusan Berhasil';
        $data['data']                           = $migrate;

        echo '<pre>';
        json_encode($data);
        $this->benchmark->mark('ended');
        $elapsed_time   = $this->benchmark->elapsed_time('started', 'ended');
        $elapsed_time   = 'Elapsed Time : ' . $elapsed_time . ' seconds';
        echo  br(3) . '--------------------------------------' . br();
        echo  $elapsed_time . br();
        echo '</pre>';
        die();
    }

    function get_skpd($nama_skpd = '', $debug = TRUE)
    {
        if (!$nama_skpd) die();

        $nama_skpd = str_replace('%20', ' ', $nama_skpd);

        $skpd = $this->Model_Master->get_skpd_by('nama', $nama_skpd, '', 1);

        if ($skpd) {
            var_dump($skpd);
        }
    }
}

/* End of file debug.php */
/* Location: ./application/controllers/debug.php */
