<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * User Controller.
 * 
 * @class     User
 * @version   1.0.0
 */
class User extends Member_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        return $this->manage();
    }

    function edit($id_user)
    {
        if ($id_user) {
            $id_user     = bp_decrypt($id_user);
            return $this->formuser($id_user);
        } else {
            redirect(base_url('user'), 'refresh');
        }
    }

    function manage()
    {
        auth_redirect();

        $current_member         = bp_get_current_member();
        $is_admin               = as_administrator($current_member);

        $headstyles             = bp_headstyles(array(
            // Default CSS Plugin
            BE_PLUGIN_PATH . 'datatables/dataTables.bootstrap.css?ver=' . CSS_VER_MAIN
        ));
        $loadscripts            = bp_scripts(array(
            // Default JS Plugin
            BE_PLUGIN_PATH . 'datatables/jquery.dataTables.min.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'datatables/dataTables.bootstrap.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'datatables/datatable.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'jquery-inputmask/jquery.inputmask.bundle.min.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'jquery-validation/dist/jquery.validate.min.js?ver=' . JS_VER_MAIN,
            // Always placed at bottom
            BE_JS_PATH . 'table-ajax.js?ver=' . JS_VER_BACK,
            BE_JS_PATH . 'custom.js?ver=' . JS_VER_BACK
        ));
        $scripts_init           = bp_scripts_init(array(
            'InputMask.init();',
            'Staff.init();',
            'TableAjaxStaffList.init();'
        ));
        $scripts_add            = '';

        $title_page             = lang('menu_users');
        $data['title']          = TITLE . $title_page;
        $data['title_page']     = $title_page;
        $data['member']         = $current_member;
        $data['is_admin']       = $is_admin;
        $data['headstyles']     = $headstyles;
        $data['scripts']        = $loadscripts;
        $data['scripts_init']   = $scripts_init;
        $data['scripts_add']    = $scripts_add;
        $data['cfg_access']     = config_item('user_access');
        $data['main_content']   = 'user/manage';

        $this->load->view(VIEW_BACK . 'template_index', $data);
    }

    function formuser($id_user = 0)
    {
        auth_redirect();

        $userdata               = '';
        $current_member         = bp_get_current_member();
        $is_admin               = as_administrator($current_member);

        $headstyles             = bp_headstyles(array(
            // Default CSS Plugin
            BE_PLUGIN_PATH . 'select2/dist/css/select2.min.css?ver=' . CSS_VER_MAIN,
        ));
        $loadscripts            = bp_scripts(array(
            // Default JS Plugin
            BE_PLUGIN_PATH . 'jquery-inputmask/jquery.inputmask.bundle.min.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'jquery-validation/dist/jquery.validate.min.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'select2/dist/js/select2.min.js?ver=' . JS_VER_MAIN,
            // Always placed at bottom
            BE_JS_PATH . 'pages/user.js?ver=' . JS_VER_PAGE,
            BE_JS_PATH . 'custom.js?ver=' . JS_VER_BACK
        ));
        $scripts_init           = bp_scripts_init(array(
            'App.select2();',
            'InputMask.init();',
            'User.init();'
        ));
        $scripts_add            = '';
        $menu_title             = lang('add') . ' ' . lang('menu_users');

        if ($id_user) {
            $userdata           = $this->Model_Auth->get($id_user);
            if ( $userdata ) {
                $menu_title     = lang('edit') . ' ' . lang('menu_users');
                if ( $current_member->id == $userdata->id ) {
                    redirect(base_url('user'), 'refresh');
                }
            }
        }

        $data['title']          = TITLE . $menu_title;
        $data['title_page']     = $menu_title;
        $data['member']         = $current_member;
        $data['is_admin']       = $is_admin;
        $data['userdata']       = $userdata;
        $data['config']         = config_item('staff_access_text');
        $data['headstyles']     = $headstyles;
        $data['scripts']        = $loadscripts;
        $data['scripts_init']   = $scripts_init;
        $data['scripts_add']    = $scripts_add;
        $data['main_content']   = 'user/add';

        $this->load->view(VIEW_BACK . 'template_index', $data);
    }

    function managelistdata()
    {
        // This is for AJAX request
        if (!$this->input->is_ajax_request()) exit('No direct script access allowed');

        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            // Set JSON data
            $data = array('status' => 'access_denied', 'url' => base_url('login'));
            die(json_encode($data));
        }

        $current_member     = bp_get_current_member();
        $is_admin           = as_administrator($current_member);

        $params             = array();
        $condition          = 'WHERE %id% > 1';
        if ( !$is_admin ) {
            if ( $current_member->type == BIDANG ) {
                $params     = array($current_member->id_bidang, 'bidang');
                $condition .= ' AND %id_bidang% = ? AND %access% = ?';
            }

            if ( $current_member->type == SKPD ) {
                $params     = array($current_member->id_skpd, 'skpd');
                $condition .= ' AND %id_skpd% = ? AND %access% = ?';
            }
        }
        $order_by           = '';
        $iTotalRecords      = 0;

        $iDisplayLength     = intval($_REQUEST['iDisplayLength']);
        $iDisplayStart      = intval($_REQUEST['iDisplayStart']);
        $sEcho              = intval($_REQUEST['sEcho']);
        $sAction            = isset($_REQUEST['sAction']) ? $_REQUEST['sAction'] : '';

        $sort               = $_REQUEST['sSortDir_0'];
        $column             = intval($_REQUEST['iSortCol_0']);

        $limit              = ($iDisplayLength == '-1' ? 0 : $iDisplayLength);
        $offset             = $iDisplayStart;

        $s_name             = $this->input->post('search_name');
        $s_name             = bp_isset($s_name, '');
        $s_username         = $this->input->post('search_username');
        $s_username         = bp_isset($s_username, '');
        $s_email            = $this->input->post('search_email');
        $s_email            = bp_isset($s_email, '');
        $s_phone            = $this->input->post('search_phone');
        $s_phone            = bp_isset($s_phone, '');
        $s_access           = $this->input->post('search_access');
        $s_access           = bp_isset($s_access, '');
        $s_status           = $this->input->post('search_status');
        $s_status           = bp_isset($s_status, '');

        if ( !empty($s_username) )  { $condition .= ' AND %username% LIKE CONCAT("%", ?, "%")'; $params[] = $s_username; }
        if ( !empty($s_name) )      { $condition .= ' AND %name% LIKE CONCAT("%", ?, "%")'; $params[] = $s_name; }
        if ( !empty($s_email) )     { $condition .= ' AND %email% LIKE CONCAT("%", ?, "%")'; $params[] = $s_email; }
        if ( !empty($s_phone) )     { $condition .= ' AND %phone% LIKE CONCAT("%", ?, "%")'; $params[] = $s_phone; }
        if ( !empty($s_access) )    { $condition .= ' AND %type% = ?'; $params[] = $s_access; }
        if ( !empty($s_status) )    { 
            if ( strtolower($s_status) == 'active' )        { $condition .= ' AND %status% = 1'; }
            if ( strtolower($s_status) == 'banned' )        { $condition .= ' AND %status% = 2'; }
            if ( strtolower($s_status) == 'deleted' )       { $condition .= ' AND %status% >= 3'; }
            if ( strtolower($s_status) == 'not_active' )    { $condition .= ' AND %status% = 0'; }
        }

        if ($column == 1) { $order_by = '%username% ' . $sort; } 
        if ($column == 2) { $order_by = '%name% ' . $sort; } 
        if ($column == 3) { $order_by = '%email% ' . $sort; }
        if ($column == 4) { $order_by = '%phone% ' . $sort; }
        if ($column == 5) { $order_by = ($is_admin ? '%type% ' : '%status% ') . $sort; }
        if ($column == 6) { $order_by = ($is_admin ? '%status% ' : '%lastlogin% ') . $sort; }
        if ($column == 7) { $order_by = '%lastlogin% ' . $sort; }

        $data               = $this->Model_Auth->get_all_user_data($limit, $offset, $condition, $order_by, $params);
        $records            = array();
        $records["aaData"]  = array();

        if (!empty($data)) {
            $iTotalRecords  = bp_get_last_found_rows();
            $i = $offset + 1;
            foreach ($data as $row) {
                $id_user        = bp_encrypt($row->id);
                $user_access    = '';
                if ( $row->type == ADMIN ) {
                    $user_access    = '<div class="text-center mb-1"><span class="badge badge-sm badge-success">ADMIN</span></div>';
                    $access         = '';
                    if ( $row->access == 'all' ) {
                        $access     = '<b>** Semua Fitur **</b>';
                        $roles      = array();
                        if ($row->roles) {
                            $roles  = maybe_unserialize($row->roles);
                        }
                        $config     = config_item('staff_access_text');
                        foreach (array(STAFF_ACCESS14, STAFF_ACCESS15) as $val) {
                            if (empty($roles) || !in_array($val, $roles)){
                                $access .= '<br />Tidak bisa akses ' . $config[$val];
                            }
                        }
                    }
                    if ($row->access == 'partial') {
                        $access = array();
                        if ($row->roles) {
                            $access = maybe_unserialize($row->roles);
                        }
                        if (is_array($access)) {
                            array_walk($access, function (&$val) {
                                $config = config_item('staff_access_text');
                                $val    = '- '.$config[$val];
                            });
                            $access = implode('<br />', $access);
                        }
                    }
                    if ( $access ) {
                        $user_access .= $access;
                    }
                }

                if ( $row->type == BIDANG ) {
                    $user_access    = '<div class="text-center"><span class="badge badge-sm badge-info">BIDANG</span></div>';
                    if ( $row->id_bidang ) {
                        $getbidang = bp_bidang($row->id_bidang);
                        if ( $getbidang ) {
                            $user_access .= '<div class="text-center"><span class="badge badge-info">'. $getbidang->nama .'</span></div>';
                        }
                    }
                }

                if ( $row->type == SKPD ) {
                    $user_access    = '<div class="text-center"><span class="badge badge-sm badge-primary">SKPD</span></div>';
                    if ( $row->id_skpd ) {
                        $getskpd = bp_skpd($row->id_skpd);
                        if ( $getskpd ) {
                            $user_access .= '<div class="text-center"><span class="badge badge-primary">'. $getskpd->nama .'</span></div>';
                        }
                    }
                }

                $status    = '<span class="badge badge-sm badge-warning">NOT ACTIVE</span>';
                if ( $row->status == 1 ) { $status = '<span class="badge badge-sm badge-success">ACTIVE</span>'; }
                if ( $row->status == 2 ) { $status = '<span class="badge badge-sm badge-danger">BANNED</span>'; }

                $btn_edit   = '<a href="' . base_url('user/edit/' . $id_user) . '" class="btn btn-sm btn-default btn-tooltip" title="Edit User"><i class="fa fa-edit"></i></a>';
                $btn_reset  = '<a href="' . base_url('user/getuser/' . $id_user) . '" class="btn btn-sm btn-warning btn-tooltip grid-reset-password-staff" title="Reset Password"><i class="fa fa-key"></i></a>';
                $btn_reset  = '<a href="' . base_url('user/resetpassword/' . $id_user) . '" class="btn btn-sm btn-warning btn-tooltip btn-reset-password" title="Reset Password"><i class="fa fa-key"></i></a>';
                $btn_del    = '<a href="' . base_url('user/del/' . $id_user) . '" class="btn btn-sm btn-outline-danger btn-tooltip delstaff" title="Hapus User"><i class="fa fa-trash"></i></a>';

                if ( $current_member->id == $row->id ) {
                    $btn_edit   = $btn_reset = $btn_del = '';
                }

                $last_login     = '-';
                if ($row->last_login != '0000-00-00 00:00:00') {
                    $last_login     = date('d M y H:i', strtotime($row->last_login));
                }

                $datatables     = array(
                    '<center>' . $i++ . '</center>',
                    '<div style="min-width:150px"><center>' . $row->username . '</center></div>',
                    '<div style="min-width:150px"><center>' . $row->name . '</center></div>',
                    '<center>' . $row->email . '</center>',
                    '<center>' . ($row->phone ? $row->phone : '-') . '</center>',
                );

                if ( $is_admin ) {
                    $datatables[] = $user_access;
                }

                $datatables[] = '<center>' . $status . '</center>';
                $datatables[] = '<center>' . $last_login . '</center>';
                $datatables[] = '<center>' . $btn_edit . $btn_reset . $btn_del . '</center>';

                $records["aaData"][] = $datatables;
            }
        }

        $end                = $iDisplayStart + $iDisplayLength;
        $end                = $end > $iTotalRecords ? $iTotalRecords : $end;

        $records["sEcho"]                   = $sEcho;
        $records["iTotalRecords"]           = $iTotalRecords;
        $records["iTotalDisplayRecords"]    = $iTotalRecords;
        $records["token"]                   = $this->security->get_csrf_hash();

        echo json_encode($records);
    }

    function saveuser($id_user = 0)
    {
        if (!$this->input->is_ajax_request()) { redirect(base_url('staff'), 'refresh'); }

        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            $data = array('status' => 'access_denied', 'url' => base_url('login'));
            die(json_encode($data)); // JSON encode data
        }

        $bp_token           = $this->security->get_csrf_hash();
        $data               = array('status' => 'error', 'token' => $bp_token, 'message' => 'Simpan data pengguna tidak berhasil');

        // set variables
        $current_member     = bp_get_current_member();
        $is_admin           = as_administrator($current_member);
        $datetime           = date('Y-m-d H:i:s');
        $_access            = '';
        $_roles             = 0;

        // POST Input Form
        $username           = trim($this->input->post('user_username'));
        $username           = bp_isset($username, '', '', true);
        $password           = trim($this->input->post('user_password'));
        $password           = bp_isset($password, '', '', true);
        $password_confirm   = trim($this->input->post('user_password_confirm'));
        $password_confirm   = bp_isset($password_confirm, '', '', true);
        $name               = trim($this->input->post('user_name'));
        $name               = bp_isset($name, '', '', true);
        $phone              = trim($this->input->post('user_phone'));
        $phone              = bp_isset($phone, '', '', true);
        $email              = trim($this->input->post('user_email'));
        $email              = bp_isset($email, '', '', true);
        $access             = trim($this->input->post('user_access'));
        $access             = bp_isset($access, '', '', true);
        $bidang             = trim($this->input->post('user_bidang'));
        $bidang             = bp_isset($bidang, '', '', true);
        $skpd               = trim($this->input->post('user_skpd'));
        $skpd               = bp_isset($skpd, '', '', true);

        if (!$id_user) {
            $this->form_validation->set_rules('user_username',             'Username', 'trim|required|is_unique[users.username]');
            $this->form_validation->set_rules('user_password',             'Password', 'trim|required');
            $this->form_validation->set_rules('user_password_confirm',     'Password Confirm', 'trim|required|matches[user_password]');
        }
        $this->form_validation->set_rules('user_name',     'Nama', 'trim|required');
        $this->form_validation->set_rules('user_email',    'Email', 'trim|required');
        // $this->form_validation->set_rules('user_phone',    'Phone', 'trim|required');

        if ( $is_admin ) {
            if ( $access == BIDANG ) {
                $this->form_validation->set_rules('user_bidang',    'Bidang', 'trim|required');
                $_access = 'bidang';
            }

            if ( $access == SKPD ) {
                $this->form_validation->set_rules('user_skpd',      'SKPD', 'trim|required');
                $_access = 'skpd';
            }
        } else {
            if ( $current_member->type == BIDANG ) {
                $bidang     = $current_member->id_bidang;
                $access     = BIDANG;
                $_access    = 'bidang';
            }

            if ( $current_member->type == SKPD ) {
                $skpd       = $current_member->id_skpd;
                $access     = SKPD;
                $_access    = 'skpd';
            }
        }

        $this->form_validation->set_message('required', '%s harus di isi');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() == FALSE) {
            $data['message'] = 'Simpan data pengguna tidak berhasil disimpan. ' . validation_errors();
            die(json_encode($data));
        }

        if ( $is_admin ) {
            if ( $access == ADMIN ) {
                $_access    = $this->input->post('fitur_access');
                $_access    = bp_isset($_access, '', '', true);
                if ( $_access == 'all' ) {
                    $_roles  = $this->input->post('fitur_all');
                    $_roles  = bp_isset($_roles, '', '', false, false);
                    $_roles  = $_roles ? $_roles : 0;
                } else {
                    $_roles  = $this->input->post('fitur_partial');
                    $_roles  = bp_isset($_roles, '', '', false, false);
                    if ( !$_roles ) {
                        $msg = $id_user ? 'Edit Pengguna tidak berhasil.' : 'Pendaftaran Pengguna tidak berhasil.';
                        $data['message'] = $msg . ' Fitur Harus di pilih !';
                        die(json_encode($data));
                    }
                }
            }
        }

        if (substr($phone, 0, 1) != '0') { $phone = '0' . $phone; }

        // --------------------------------------
        // Set Data
        // --------------------------------------
        $data_user  = array(
            'username'      => strtolower($username),
            'name'          => strtoupper($name),
            'email'         => strtolower($email),
            'phone'         => $phone,
            'status'        => ACTIVE,
            'type'          => $access,
            'access'        => $_access,
            'roles'         => $_roles,
            'datecreated'   => $datetime,
            'datemodified'  => $datetime
        );

        if ( $access == BIDANG ) {
            $data_user['id_source'] = $bidang;
            $data_user['id_bidang'] = $bidang;
        }

        if ( $access == SKPD ) {
            $data_user['id_source'] = $skpd;
            $data_user['id_skpd']   = $skpd;
        }

        $save_user      = false;
        if ($id_user) {
            $id_user    = bp_decrypt($id_user);
            unset($data_user['username']);
            unset($data_user['status']);
            unset($data_user['datecreated']);
            if ( $this->Model_Auth->update($id_user, $data_user) ) {
                $save_user = true;
            }
        } else {
            $data_user['password']      = bp_password_hash($password);
            $data_user['password_enc']  = bp_encrypt($password);
            if ( $this->Model_Auth->insert($data_user) ) {
                $save_user = true;
            }
        }

        if ( $save_user ) {
            $data['url']        = base_url('user');
            $data['status']     = 'success';
            $data['message']    = 'Simpan data Pengguna berhasil';
        }

        die(json_encode($data));
    }

    function getuser($id_user = 0)
    {
        // This is for AJAX request
        if (!$this->input->is_ajax_request()) exit('No direct script access allowed');

        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            // Set JSON data
            $data = array('status' => 'login', 'message' => base_url('login'));
            die(json_encode($data));
        }

        if (!$id_user) {
            // Set JSON data
            $data       = array('status' => 'error', 'message' => 'ID Pengguna tidak boleh kosong. Silahkan Pilih Pengguna lainnya.');
            die(json_encode($data));
        }

        $id_user       = bp_decrypt($id_user);
        if (!$userdata = $this->Model_Auth->get_userdata($id_user)) {
            // Set JSON data
            $data       = array('status' => 'error', 'message' => 'Data Pengguna tidak ditemukan.');
            die(json_encode($data));
        }

        $user['id']        = $userdata->id;
        $user['username']  = $userdata->username;
        $user['name']      = $userdata->name;

        // Set JSON data
        $data = array('status' => 'success', 'data' => $user);
        die(json_encode($data));
    }

    function changepassword()
    {
        // This is for AJAX request
        if (!$this->input->is_ajax_request()) exit('No direct script access allowed');

        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            // Set JSON data
            $data = array('status' => 'login', 'message' => base_url('login'));
            die(json_encode($data));
        }

        $current_member         = bp_get_current_member();
        $is_admin               = as_administrator($current_member);
        $staff_id               = $this->input->post('staff_id');
        $staff_id               = bp_isset($staff_id, 0);
        $staff_password         = $this->input->post('staff_password');
        $staff_password         = bp_isset($staff_password, '');
        $staff_password_retype  = $this->input->post('staff_password_retype');
        $staff_password_retype  = bp_isset($staff_password_retype, '');

        $this->form_validation->set_rules('staff_password',         'Password', 'trim|required');
        $this->form_validation->set_rules('staff_password_confirm', 'Konfirmasi Password', 'trim|required|matches[staff_password]');

        if ($this->form_validation->run() == FALSE) {
            $response = array('status' => 'error', 'message' => validation_errors());
            die(json_encode($response));
        }

        if (!$staff_id) {
            // Set JSON data
            $data       = array('status' => 'error', 'message' => ('ID Pengguna tidak boleh kosong. Silahkan Pilih Pengguna lainnya.'));
            die(json_encode($data));
        }

        if (!$staffdata = $this->Model_Auth->get_userdata($staff_id)) {
            // Set JSON data
            $data       = array('status' => 'error', 'message' => ('Data Pengguna tidak ditemukan.'));
            die(json_encode($data));
        }

        // Reset Password
        $password       = bp_password_hash($staff_password);
        $staff          = array(
            'password'      => $password,
            'password_enc'  => bp_encrypt($staff_password),
            'datecreated'   => date('Y-m-d H:i:s')
        );

        if (!$reset_pass = $this->Model_Auth->update_data($staff_id, $staff)) {
            $response = array('status' => 'error', 'message' => ('Reset Password tidak berhasil.'));
            die(json_encode($response));
        }

        // Set JSON data
        $data = array('status' => 'success', 'message' => ('Reset Password berhasil.'));
        die(json_encode($data));
    }

    function resetpassword($id_user = 0)
    {
        // This is for AJAX request
        if (!$this->input->is_ajax_request()) exit('No direct script access allowed');

        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            $data = array('status' => 'login', 'message' => base_url('login'));
            die(json_encode($data));
        }

        $id_user            = $id_user ? bp_decrypt($id_user) : 0;
        $current_member     = bp_get_current_member();
        $is_admin           = as_administrator($current_member);
        $bp_token           = $this->security->get_csrf_hash();
        $data               = array('status' => 'error', 'token' => $bp_token, 'message' => 'ID Pengguna tidak dikenali. Silahkan pilih Pengguna lainnya!');

        if (!$id_user) {
            die(json_encode($data));
        }

        if (!$userdata = $this->Model_Auth->get_userdata($id_user)) {
            $data['message'] = 'Data Pengguna tidak ditemukan.';
            die(json_encode($data));
        }

        $password_global    = get_option('global_password');
        $password_global    = $password_global ? $password_global : config_item('pwd_global');
        $password           = bp_password_hash($password_global);

        // Reset Password
        $staff              = array(
            'password'      => $password,
            'password_enc'  => bp_encrypt($password_global),
            'datemodified'  => date('Y-m-d H:i:s')
        );

        if (!$reset_pass = $this->Model_Auth->update_data($id_user, $staff)) {
            $data['message'] = 'Reset Password tidak berhasil. Terjadi kesalahan pada proses update password.';
            die(json_encode($data));
        }

        $data_log       = array(
            'cookie'    => $_COOKIE,
            'user'      => $current_member, 
            'password'  => $password_global, 
            'ip'        => bp_get_current_ip()
        );
        bp_log('RESET_PASSWORD', $userdata->username, maybe_serialize($data_log));
        // Set JSON data
        $data['status']     = 'success';
        $data['message']    = 'Reset Password <b>'. $userdata->username .'</b> berhasil.';
        die(json_encode($data));
    }

    function del($id_user)
    {
        if (!$this->input->is_ajax_request()) {
            redirect(base_url('staff'), 'refresh');
        }
        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            $data = array('status' => 'access_denied', 'url' => base_url('login'));
            die(json_encode($data)); // JSON encode data
        }

        $id_user    = bp_decrypt($id_user);
        $userdata   = $this->Model_Staff->get_userdata($id_user);

        if (!$userdata) {
            $response = array('success' => false);
            die(json_encode($response));
        }

        $data = array('status' => 3);
        if ($this->Model_Auth->update_data($id_user, $data)) {
            $response = array('success' => true);
            die(json_encode($response));
        }

        $response = array('success' => false);
        die(json_encode($response));
    }

    /**
     * Check Username function.
     */
    function checkusername()
    {
        // Check for AJAX Request
        if (!$this->input->is_ajax_request()) {
            redirect(base_url('dashboard'), 'location');
        }

        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            $data = array('status' => 'login', 'message' => base_url('login'));
            die(json_encode($data));
        }

        $id         = $this->input->post('id');
        $id         = bp_isset($id, '', '', true);
        $username   = $this->input->post('username');
        $username   = bp_isset($username, '', '', true);
        $bp_token   = $this->security->get_csrf_hash();

        if (!empty($username)) {
            $memberdata     = $this->Model_Member->get_member_by('login', strtolower($username));

            if ($memberdata) {
                $user_exist = true;
                if ( $id ) {
                    $id     = bp_decrypt($id);
                    if ( $id == $memberdata->id ) {
                        $user_exist = false;
                    }
                }

                if ( $user_exist ) {
                    die(json_encode(array('status' => false, 'token' => $bp_token)));
                }
            }

            // if staff with the username exists
            if ($staff = $this->Model_Staff->get_by('username', $username)){
                $user_exist = true;
                if ( $id ) {
                    $id     = bp_decrypt($id);
                    if ( $id == $staff->id ) {
                        $user_exist = false;
                    }
                }

                if ( $user_exist ) {
                    die(json_encode(array('status' => false, 'token' => $bp_token)));
                }
            }
        }

        die(json_encode(array('status' => true, 'token' => $bp_token)));
    }

    /**
     * Check Email function.
     */
    function checkemail()
    {
        // Check for AJAX Request
        if (!$this->input->is_ajax_request()) {
            redirect(base_url('dashboard'), 'location');
        }

        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            $data = array('status' => 'login', 'message' => base_url('login'));
            die(json_encode($data));
        }

        $current_member     = bp_get_current_member();
        $is_admin           = as_administrator($current_member);

        $id                 = $this->input->post('id');
        $id                 = trim(bp_isset($id, '', '', true));
        $id                 = $id ? bp_decrypt($id) : $current_member->id;
        $email              = $this->input->post('email');
        $email              = trim(bp_isset($email, '', '', true));
        $bp_token           = $this->security->get_csrf_hash();

        die(json_encode(array('status' => true, 'token' => $bp_token)));

        if (!empty($email)) {
            if ( $email == $current_member->email ) {
                die(json_encode(array('status' => true, 'token' => $bp_token)));
            }
            
            $memberdata = $this->Model_Member->get_member_by('email', $email);
            if ($memberdata) {
                if ($id) {
                    if ($id != $memberdata->id) {
                        die(json_encode(array('status' => false, 'token' => $bp_token)));
                    }
                } else {
                    die(json_encode(array('status' => false, 'token' => $bp_token)));
                }
            }
        }
        die(json_encode(array('status' => true, 'token' => $bp_token)));
    }

    /**
     * Check Phone function.
     */
    function checkphone()
    {
        // Check for AJAX Request
        if (!$this->input->is_ajax_request()) {
            redirect(base_url('dashboard'), 'location');
        }

        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            $data = array('status' => 'login', 'message' => base_url('login'));
            die(json_encode($data));
        }

        $current_member     = bp_get_current_member();
        $is_admin           = as_administrator($current_member);

        $id                 = $this->input->post('id');
        $id                 = trim(bp_isset($id, '', '', true));
        $id                 = $id ? bp_decrypt($id) : $current_member->id;
        $phone              = $this->input->post('phone');
        $phone              = trim(bp_isset($phone, '', '', true));
        $bp_token           = $this->security->get_csrf_hash();

        die(json_encode(array('status' => true, 'token' => $bp_token)));

        if (!empty($phone)) {
            if ( $phone == $current_member->phone ) {
                die(json_encode(array('status' => true, 'token' => $bp_token)));
            }

            $memberdata     = $this->Model_Member->get_member_by('phone', $phone);
            if ($memberdata) {
                if ($id) {
                    if ($id != $memberdata->id) {
                        die(json_encode(array('status' => false, 'token' => $bp_token)));
                    }
                } else {
                    die(json_encode(array('status' => false, 'token' => $bp_token)));
                }
            }
        }

        die(json_encode(array('status' => true, 'token' => $bp_token)));
    }
}

/* End of file user.php */
/* Location: ./application/controllers/user.php */