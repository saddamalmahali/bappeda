<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Backend Controller.
 *
 * @class     Backend
 * @version   1.0.0
 */
class Backend extends Member_Controller
{
    /**
     * Constructor.
     */
    function __construct()
    {
        parent::__construct();
    }

    // =============================================================================================
    // DASHBOARD
    // =============================================================================================

    /**
     * Dashboard function.
     */
    public function index()
    {
        auth_redirect();

        $current_member         = bp_get_current_member();
        $is_admin               = as_administrator($current_member);

        $headstyles             = bp_headstyles(array(
            // Default CSS Plugin
            BE_PLUGIN_PATH . 'datatables/dataTables.bootstrap.css?ver=' . CSS_VER_MAIN
        ));

        $loadscripts            = bp_scripts(array(
            // Default JS Plugin
            BE_PLUGIN_PATH . 'bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'datatables/jquery.dataTables.min.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'datatables/dataTables.bootstrap.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'datatables/datatable.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'jquery-inputmask/jquery.inputmask.bundle.min.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'jquery-validation/dist/jquery.validate.min.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'chart.js/dist/Chart.min.js',
            BE_PLUGIN_PATH . 'chart.js/dist/Chart.extension.js',
            // Always placed at bottom
            BE_JS_PATH . 'form-validation.js?ver=' . JS_VER_BACK,
            BE_JS_PATH . 'pages/dashboard.js?ver=' . JS_VER_BACK,
            BE_JS_PATH . 'custom.js?ver=' . JS_VER_BACK
        ));

        $scripts_init           = bp_scripts_init(array(
            'InputMask.init();',
            'ButtonAction.init();',
            'ShopOrderManage.init();',
            'Profile.init();',
            'FV_Profile.init();'
        ));
        $scripts_add            = '';

        $data_omzet             = false;

        $data['title']          = TITLE . 'Dashboard';
        $data['member']         = $current_member;
        $data['is_admin']       = $is_admin;
        $data['data_omzet']     = $data_omzet;
        $data['headstyles']     = $headstyles;
        $data['scripts']        = $loadscripts;
        $data['scripts_init']   = $scripts_init;
        $data['scripts_add']    = $scripts_add;
        $data['main_content']   = 'dashboard';

        // log for dashboard
        if (!$this->session->userdata('log_dashboard')) {
            $this->session->set_userdata('log_dashboard', true);
            bp_log('DASHBOARD', bp_get_current_ip(), maybe_serialize(array('current_member' => $current_member, 'cookie' => $_COOKIE)));
        }

        $this->load->view(VIEW_BACK . 'template_index', $data);
    }

    // =============================================================================================
    // MASTER PAGE
    // =============================================================================================

    /**
     * Member New function.
     */
    public function membernew()
    {
        auth_redirect();

        $current_member         = bp_get_current_member();
        $is_admin               = as_administrator($current_member);

        $headstyles             = bp_headstyles(array(
            // Default CSS Plugin
            BE_PLUGIN_PATH . 'select2/dist/css/select2.min.css?ver=' . CSS_VER_MAIN,
        ));
        $loadscripts            = bp_scripts(array(
            // Default JS Plugin
            BE_PLUGIN_PATH . 'jquery-inputmask/jquery.inputmask.bundle.min.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'jquery-validation/dist/jquery.validate.min.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'select2/dist/js/select2.min.js?ver=' . JS_VER_MAIN,
            // Always placed at bottom
            BE_JS_PATH . 'pages/register.js?ver=' . JS_VER_PAGE,
            BE_JS_PATH . 'custom.js?ver=' . JS_VER_BACK
        ));
        $scripts_init           = bp_scripts_init(array(
            'App.select2();',
            'InputMask.init();',
            'GetProduct.init();',
            'SearchAction.init();',
            'SelectChange.init();',
            'RegisterMember.init();'
        ));
        $scripts_add            = '';

        $packagedata            = bp_packages();

        $data['title']          = TITLE . lang('menu_member_new');
        $data['title_page']     = '<i class="fa fa-user-plus mr-1"></i> ' . lang('menu_member_new');
        $data['member']         = $current_member;
        $data['is_admin']       = $is_admin;
        $data['packagedata']    = $packagedata;
        $data['headstyles']     = $headstyles;
        $data['scripts']        = $loadscripts;
        $data['scripts_init']   = $scripts_init;
        $data['scripts_add']    = $scripts_add;
        $data['main_content']   = 'member/form/register';

        $this->load->view(VIEW_BACK . 'template_index', $data);
    }

    /**
     * Urusan List function.
     */
    public function urusanlist()
    {
        auth_redirect();

        $current_member         = bp_get_current_member();
        $is_administrator       = as_administrator($current_member);
        $is_admin               = as_admin($current_member);
        $crud_access            = $is_administrator ? TRUE : FALSE;
        $access                 = '';
        $roles                  = '';

        if ($is_admin) {
            $access             = $current_member->access;
            $roles              = maybe_unserialize($current_member->roles);
        }

        if ($staff = bp_get_current_staff()) {
            $access             = $staff->access;
            $roles              = maybe_unserialize($staff->role);
        }

        if ($access == 'partial') {
            $crud_access        = FALSE;
            if ($roles && is_array($roles)) {
                if (in_array(STAFF_ACCESS2, $roles)) {
                    $crud_access = TRUE;
                }
            }
        }

        $headstyles             = bp_headstyles(array(
            // Default CSS Plugin
            BE_PLUGIN_PATH . 'datatables/dataTables.bootstrap.css?ver=' . CSS_VER_MAIN
        ));
        $loadscripts            = bp_scripts(array(
            // Default JS Plugin
            BE_PLUGIN_PATH . 'bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'jquery-validation/dist/jquery.validate.min.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'datatables/jquery.dataTables.min.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'datatables/dataTables.bootstrap.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'datatables/datatable.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'jquery-inputmask/jquery.inputmask.bundle.min.js?ver=' . JS_VER_MAIN,
            // Always placed at bottom
            BE_JS_PATH . 'table-ajax.js?ver=' . JS_VER_BACK,
            BE_JS_PATH . 'custom.js?ver=' . JS_VER_BACK
        ));
        $scripts_init           = bp_scripts_init(array(
            'InputMask.init();',
            'TableAjaxMasterList.init();',
            'MasterManage.initUrusan();'
        ));
        $scripts_add            = '';

        $title_page             = lang('menu_master_urusan');
        $data['title']          = TITLE . $title_page;
        $data['title_page']     = $title_page;
        $data['member']         = $current_member;
        $data['is_admin']       = $is_administrator;
        $data['crud_access']    = $crud_access;
        $data['headstyles']     = $headstyles;
        $data['scripts']        = $loadscripts;
        $data['scripts_init']   = $scripts_init;
        $data['scripts_add']    = $scripts_add;
        $data['main_content']   = 'master/urusanlists';

        $this->load->view(VIEW_BACK . 'template_index', $data);
    }

    /**
     * Sub Urusan List function.
     */
    public function suburusanlist()
    {
        auth_redirect();

        $current_member         = bp_get_current_member();
        $is_administrator       = as_administrator($current_member);
        $is_admin               = as_admin($current_member);
        $crud_access            = $is_administrator ? TRUE : FALSE;
        $access                 = '';
        $roles                  = '';

        if ($is_admin) {
            $access             = $current_member->access;
            $roles              = maybe_unserialize($current_member->roles);
        }

        if ($staff = bp_get_current_staff()) {
            $access             = $staff->access;
            $roles              = maybe_unserialize($staff->role);
        }

        if ($access == 'partial') {
            $crud_access        = FALSE;
            if ($roles && is_array($roles)) {
                if (in_array(STAFF_ACCESS2, $roles)) {
                    $crud_access = TRUE;
                }
            }
        }

        $headstyles             = bp_headstyles(array(
            // Default CSS Plugin
            BE_PLUGIN_PATH . 'datatables/dataTables.bootstrap.css?ver=' . CSS_VER_MAIN,
            BE_PLUGIN_PATH . 'select2/dist/css/select2.min.css?ver=' . CSS_VER_MAIN,
        ));
        $loadscripts            = bp_scripts(array(
            // Default JS Plugin
            BE_PLUGIN_PATH . 'bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'jquery-validation/dist/jquery.validate.min.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'datatables/jquery.dataTables.min.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'datatables/dataTables.bootstrap.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'datatables/datatable.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'jquery-inputmask/jquery.inputmask.bundle.min.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'select2/dist/js/select2.min.js?ver=' . JS_VER_MAIN,
            // Always placed at bottom
            BE_JS_PATH . 'table-ajax.js?ver=' . JS_VER_BACK,
            BE_JS_PATH . 'custom.js?ver=' . JS_VER_BACK
        ));
        $scripts_init           = bp_scripts_init(array(
            'App.select2();',
            'InputMask.init();',
            'TableAjaxMasterList.init();',
            'MasterManage.initSubUrusan();'
        ));
        $scripts_add            = '';

        $data_urusan            = bp_urusan();

        $title_page             = lang('menu_master_suburusan');
        $data['title']          = TITLE . $title_page;
        $data['title_page']     = $title_page;
        $data['member']         = $current_member;
        $data['is_admin']       = $is_administrator;
        $data['crud_access']    = $crud_access;
        $data['headstyles']     = $headstyles;
        $data['scripts']        = $loadscripts;
        $data['scripts_init']   = $scripts_init;
        $data['scripts_add']    = $scripts_add;
        $data['data_urusan']    = $data_urusan;
        $data['main_content']   = 'master/suburusanlists';

        $this->load->view(VIEW_BACK . 'template_index', $data);
    }

    /**
     * Program List function.
     */
    public function programlist()
    {
        auth_redirect();

        $current_member         = bp_get_current_member();
        $is_administrator       = as_administrator($current_member);
        $is_admin               = as_admin($current_member);
        $crud_access            = $is_administrator ? TRUE : FALSE;
        $access                 = '';
        $roles                  = '';

        if ($is_admin) {
            $access             = $current_member->access;
            $roles              = maybe_unserialize($current_member->roles);
        }

        if ($staff = bp_get_current_staff()) {
            $access             = $staff->access;
            $roles              = maybe_unserialize($staff->role);
        }

        if ($access == 'partial') {
            $crud_access        = FALSE;
            if ($roles && is_array($roles)) {
                if (in_array(STAFF_ACCESS2, $roles)) {
                    $crud_access = TRUE;
                }
            }
        }

        $headstyles             = bp_headstyles(array(
            // Default CSS Plugin
            BE_PLUGIN_PATH . 'datatables/dataTables.bootstrap.css?ver=' . CSS_VER_MAIN,
            BE_PLUGIN_PATH . 'select2/dist/css/select2.min.css?ver=' . CSS_VER_MAIN,
        ));
        $loadscripts            = bp_scripts(array(
            // Default JS Plugin
            BE_PLUGIN_PATH . 'bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'jquery-validation/dist/jquery.validate.min.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'datatables/jquery.dataTables.min.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'datatables/dataTables.bootstrap.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'datatables/datatable.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'jquery-inputmask/jquery.inputmask.bundle.min.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'select2/dist/js/select2.min.js?ver=' . JS_VER_MAIN,
            // Always placed at bottom
            BE_JS_PATH . 'table-ajax.js?ver=' . JS_VER_BACK,
            BE_JS_PATH . 'custom.js?ver=' . JS_VER_BACK
        ));
        $scripts_init           = bp_scripts_init(array(
            'App.select2();',
            'InputMask.init();',
            'TableAjaxMasterList.init();',
            'MasterManage.initProgram();'
        ));
        $scripts_add            = '';
        $data_suburusan         = bp_sub_urusan();
        $title_page             = lang('menu_master_program');
        $data['title']          = TITLE . $title_page;
        $data['title_page']     = $title_page;
        $data['member']         = $current_member;
        $data['is_admin']       = $is_administrator;
        $data['crud_access']    = $crud_access;
        $data['headstyles']     = $headstyles;
        $data['scripts']        = $loadscripts;
        $data['scripts_init']   = $scripts_init;
        $data['scripts_add']    = $scripts_add;
        $data['data_suburusan'] = $data_suburusan;
        $data['main_content']   = 'master/programlists';

        $this->load->view(VIEW_BACK . 'template_index', $data);
    }

    /**
     * Kegiatan List function.
     */
    public function kegiatanlist()
    {
        auth_redirect();

        $current_member         = bp_get_current_member();
        $is_administrator       = as_administrator($current_member);
        $is_admin               = as_admin($current_member);
        $crud_access            = $is_administrator ? TRUE : FALSE;
        $access                 = '';
        $roles                  = '';

        if ($is_admin) {
            $access             = $current_member->access;
            $roles              = maybe_unserialize($current_member->roles);
        }

        if ($staff = bp_get_current_staff()) {
            $access             = $staff->access;
            $roles              = maybe_unserialize($staff->role);
        }

        if ($access == 'partial') {
            $crud_access        = FALSE;
            if ($roles && is_array($roles)) {
                if (in_array(STAFF_ACCESS2, $roles)) {
                    $crud_access = TRUE;
                }
            }
        }

        $headstyles             = bp_headstyles(array(
            // Default CSS Plugin
            BE_PLUGIN_PATH . 'datatables/dataTables.bootstrap.css?ver=' . CSS_VER_MAIN,
            BE_PLUGIN_PATH . 'select2/dist/css/select2.min.css?ver=' . CSS_VER_MAIN,
        ));
        $loadscripts            = bp_scripts(array(
            // Default JS Plugin
            BE_PLUGIN_PATH . 'bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'jquery-validation/dist/jquery.validate.min.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'datatables/jquery.dataTables.min.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'datatables/dataTables.bootstrap.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'datatables/datatable.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'jquery-inputmask/jquery.inputmask.bundle.min.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'select2/dist/js/select2.min.js?ver=' . JS_VER_MAIN,
            // Always placed at bottom
            BE_JS_PATH . 'table-ajax.js?ver=' . JS_VER_BACK,
            BE_JS_PATH . 'custom.js?ver=' . JS_VER_BACK
        ));
        $scripts_init           = bp_scripts_init(array(
            'InputMask.init();',
            'TableAjaxMasterList.init();',
            'MasterManage.initKegiatan();'
        ));
        $scripts_add            = '';
        $data_program           = bp_program();
        $title_page             = lang('menu_master_kegiatan');
        $data['title']          = TITLE . $title_page;
        $data['title_page']     = $title_page;
        $data['member']         = $current_member;
        $data['is_admin']       = $is_administrator;
        $data['crud_access']    = $crud_access;
        $data['headstyles']     = $headstyles;
        $data['scripts']        = $loadscripts;
        $data['scripts_init']   = $scripts_init;
        $data['scripts_add']    = $scripts_add;
        $data['data_program']   = $data_program;
        $data['main_content']   = 'master/kegiatanlists';

        $this->load->view(VIEW_BACK . 'template_index', $data);
    }

    /**
     * Sub Kegiatan List function.
     */
    public function subkegiatanlist()
    {
        auth_redirect();

        $current_member         = bp_get_current_member();
        $is_administrator       = as_administrator($current_member);
        $is_admin               = as_admin($current_member);
        $crud_access            = $is_administrator ? TRUE : FALSE;
        $access                 = '';
        $roles                  = '';

        if ($is_admin) {
            $access             = $current_member->access;
            $roles              = maybe_unserialize($current_member->roles);
        }

        if ($staff = bp_get_current_staff()) {
            $access             = $staff->access;
            $roles              = maybe_unserialize($staff->role);
        }

        if ($access == 'partial') {
            $crud_access        = FALSE;
            if ($roles && is_array($roles)) {
                if (in_array(STAFF_ACCESS2, $roles)) {
                    $crud_access = TRUE;
                }
            }
        }

        $headstyles             = bp_headstyles(array(
            // Default CSS Plugin
            BE_PLUGIN_PATH . 'datatables/dataTables.bootstrap.css?ver=' . CSS_VER_MAIN,
            BE_PLUGIN_PATH . 'select2/dist/css/select2.min.css?ver=' . CSS_VER_MAIN,
        ));
        $loadscripts            = bp_scripts(array(
            // Default JS Plugin
            BE_PLUGIN_PATH . 'bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'jquery-validation/dist/jquery.validate.min.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'datatables/jquery.dataTables.min.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'datatables/dataTables.bootstrap.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'datatables/datatable.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'jquery-inputmask/jquery.inputmask.bundle.min.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'select2/dist/js/select2.min.js?ver=' . JS_VER_MAIN,
            // Always placed at bottom
            BE_JS_PATH . 'table-ajax.js?ver=' . JS_VER_BACK,
            BE_JS_PATH . 'custom.js?ver=' . JS_VER_BACK
        ));
        $scripts_init           = bp_scripts_init(array(
            'InputMask.init();',
            'TableAjaxMasterList.init();',
            'MasterManage.initSubKegiatan();'
        ));
        $scripts_add            = '';
        $data_kegiatan          = bp_kegiatan();
        $title_page             = lang('menu_master_subkegiatan');
        $data['title']          = TITLE . $title_page;
        $data['title_page']     = $title_page;
        $data['member']         = $current_member;
        $data['is_admin']       = $is_administrator;
        $data['crud_access']    = $crud_access;
        $data['headstyles']     = $headstyles;
        $data['scripts']        = $loadscripts;
        $data['scripts_init']   = $scripts_init;
        $data['scripts_add']    = $scripts_add;
        $data['data_kegiatan']    = $data_kegiatan;
        $data['main_content']   = 'master/subkegiatanlists';

        $this->load->view(VIEW_BACK . 'template_index', $data);
    }

    // ---------------------------------------------------------------------------------------------

    // =============================================================================================
    // SATUAN KERJA
    // =============================================================================================

    /**
     * Bidang List function.
     */
    public function bidanglist()
    {
        auth_redirect();

        $current_member         = bp_get_current_member();
        $is_administrator       = as_administrator($current_member);
        $is_admin               = as_admin($current_member);
        $crud_access            = $is_administrator ? TRUE : FALSE;
        $access                 = '';
        $roles                  = '';

        if ($is_admin) {
            $access             = $current_member->access;
            $roles              = maybe_unserialize($current_member->roles);
        }

        if ($staff = bp_get_current_staff()) {
            $access             = $staff->access;
            $roles              = maybe_unserialize($staff->role);
        }

        if ($access == 'partial') {
            $crud_access        = FALSE;
            if ($roles && is_array($roles)) {
                if (in_array(STAFF_ACCESS4, $roles)) {
                    $crud_access = TRUE;
                }
            }
        }

        $headstyles             = bp_headstyles(array(
            // Default CSS Plugin
            BE_PLUGIN_PATH . 'datatables/dataTables.bootstrap.css?ver=' . CSS_VER_MAIN,
            BE_PLUGIN_PATH . 'select2/dist/css/select2.min.css?ver=' . CSS_VER_MAIN,
        ));
        $loadscripts            = bp_scripts(array(
            // Default JS Plugin
            BE_PLUGIN_PATH . 'bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'jquery-validation/dist/jquery.validate.min.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'datatables/jquery.dataTables.min.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'datatables/dataTables.bootstrap.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'datatables/datatable.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'jquery-inputmask/jquery.inputmask.bundle.min.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'select2/dist/js/select2.min.js?ver=' . JS_VER_MAIN,
            // Always placed at bottom
            BE_JS_PATH . 'table-ajax.js?ver=' . JS_VER_BACK,
            BE_JS_PATH . 'custom.js?ver=' . JS_VER_BACK
        ));
        $scripts_init           = bp_scripts_init(array(
            'InputMask.init();',
            'TableAjaxSatuanKerjaList.init();',
            'SatuanKerjaManage.initBidang();'
        ));
        $scripts_add            = '';
        $data_kegiatan          = bp_kegiatan();
        $title_page             = lang('menu_bidang');
        $data['title']          = TITLE . $title_page;
        $data['title_page']     = $title_page;
        $data['member']         = $current_member;
        $data['is_admin']       = $is_administrator;
        $data['crud_access']    = $crud_access;
        $data['headstyles']     = $headstyles;
        $data['scripts']        = $loadscripts;
        $data['scripts_init']   = $scripts_init;
        $data['scripts_add']    = $scripts_add;
        $data['data_kegiatan']    = $data_kegiatan;
        $data['main_content']   = 'master/bidanglists';

        $this->load->view(VIEW_BACK . 'template_index', $data);
    }

    /**
     * SKPD List function.
     */
    public function skpdlist()
    {
        auth_redirect();

        $current_member         = bp_get_current_member();
        $is_administrator       = as_administrator($current_member);
        $is_admin               = as_admin($current_member);
        $crud_access            = $is_administrator ? TRUE : FALSE;
        $access                 = '';
        $roles                  = '';

        if ($is_admin) {
            $access             = $current_member->access;
            $roles              = maybe_unserialize($current_member->roles);
        }

        if ($staff = bp_get_current_staff()) {
            $access             = $staff->access;
            $roles              = maybe_unserialize($staff->role);
        }

        if ($access == 'partial') {
            $crud_access        = FALSE;
            if ($roles && is_array($roles)) {
                if (in_array(STAFF_ACCESS4, $roles)) {
                    $crud_access = TRUE;
                }
            }
        }

        $headstyles             = bp_headstyles(array(
            // Default CSS Plugin
            BE_PLUGIN_PATH . 'datatables/dataTables.bootstrap.css?ver=' . CSS_VER_MAIN,
            BE_PLUGIN_PATH . 'select2/dist/css/select2.min.css?ver=' . CSS_VER_MAIN,
        ));
        $loadscripts            = bp_scripts(array(
            // Default JS Plugin
            BE_PLUGIN_PATH . 'bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'jquery-validation/dist/jquery.validate.min.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'datatables/jquery.dataTables.min.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'datatables/dataTables.bootstrap.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'datatables/datatable.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'jquery-inputmask/jquery.inputmask.bundle.min.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'select2/dist/js/select2.min.js?ver=' . JS_VER_MAIN,
            // Always placed at bottom
            BE_JS_PATH . 'table-ajax.js?ver=' . JS_VER_BACK,
            BE_JS_PATH . 'custom.js?ver=' . JS_VER_BACK
        ));
        $scripts_init           = bp_scripts_init(array(
            'InputMask.init();',
            'SatuanKerjaManage.initSkpd();',
            'TableAjaxSatuanKerjaList.initSkpd();'

        ));
        $data_suburusan         = bp_sub_urusan();
        $data_bidang            = bp_bidang();
        $data_skpd              = bp_skpd();
        $scripts_add            = '';
        $title_page             = lang('menu_skpd');
        $data['title']          = TITLE . $title_page;
        $data['title_page']     = $title_page;
        $data['member']         = $current_member;
        $data['is_admin']       = $is_administrator;
        $data['crud_access']    = $crud_access;
        $data['headstyles']     = $headstyles;
        $data['scripts']        = $loadscripts;
        $data['scripts_init']   = $scripts_init;
        $data['scripts_add']    = $scripts_add;
        $data['data_suburusan'] = $data_suburusan;
        $data['data_bidang']    = $data_bidang;
        $data['data_skpd']      = $data_skpd;
        $data['main_content']   = 'master/skpdlist';

        $this->load->view(VIEW_BACK . 'template_index', $data);
    }


    // ---------------------------------------------------------------------------------------------

    // =============================================================================================
    // TRANSACTION
    // =============================================================================================

    /**
     * Initial Plan List function.
     */
    public function initialplanlist()
    {
        auth_redirect();

        $current_member         = bp_get_current_member();
        $is_administrator       = as_administrator($current_member);
        $is_admin               = as_admin($current_member);
        $crud_access            = $is_administrator ? TRUE : FALSE;
        $access                 = '';
        $roles                  = '';

        if ($is_admin) {
            $access             = $current_member->access;
            $roles              = maybe_unserialize($current_member->roles);
        }

        if ($staff = bp_get_current_staff()) {
            $access             = $staff->access;
            $roles              = maybe_unserialize($staff->role);
        }

        if ($access == 'partial') {
            $crud_access        = FALSE;
            if ($roles && is_array($roles)) {
                if (in_array(STAFF_ACCESS4, $roles)) {
                    $crud_access = TRUE;
                }
            }
        }

        $headstyles             = bp_headstyles(array(
            // Default CSS Plugin
            BE_PLUGIN_PATH . 'datatables/dataTables.bootstrap.css?ver=' . CSS_VER_MAIN,
            BE_PLUGIN_PATH . 'select2/dist/css/select2.min.css?ver=' . CSS_VER_MAIN,
        ));
        $loadscripts            = bp_scripts(array(
            // Default JS Plugin
            BE_PLUGIN_PATH . 'bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'jquery-validation/dist/jquery.validate.min.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'datatables/jquery.dataTables.min.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'datatables/dataTables.bootstrap.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'datatables/datatable.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'jquery-inputmask/jquery.inputmask.bundle.min.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'select2/dist/js/select2.min.js?ver=' . JS_VER_MAIN,
            // Always placed at bottom
            BE_JS_PATH . 'table-ajax.js?ver=' . JS_VER_BACK,
            BE_JS_PATH . 'custom.js?ver=' . JS_VER_BACK
        ));
        $scripts_init           = bp_scripts_init(array(
            'InputMask.init();',
            'TransactionManage.initInitialPlan();',
        ));
        
        $scripts_add            = '';
        $title_page             = lang('menu_initial_plan');
        $data['title']          = TITLE . $title_page;
        $data['title_page']     = $title_page;
        $data['member']         = $current_member;
        $data['is_admin']       = $is_administrator;
        $data['crud_access']    = $crud_access;
        $data['headstyles']     = $headstyles;
        $data['scripts']        = $loadscripts;
        $data['scripts_init']   = $scripts_init;
        $data['scripts_add']    = $scripts_add;
        $data['main_content']   = 'transaction/initialplanlists';

        $this->load->view(VIEW_BACK . 'template_index', $data);
    }

    // ---------------------------------------------------------------------------------------------

    // =============================================================================================
    // PROFILE, ERROR, COMINGSOON PAGE
    // =============================================================================================

    /**
     * Profile Page function.
     */
    public function profile($id = 0)
    {
        auth_redirect();

        $member_data            = '';
        $current_member         = bp_get_current_member();
        $is_admin               = as_administrator($current_member);

        if ($id) {
            if ($is_admin) {
                $id             = bp_decrypt($id);
                if (!$member_data = bp_get_memberdata_by_id($id)) {
                    redirect(base_url('profile'), 'location');
                }
            } else {
                redirect(base_url('profile'), 'location');
            }
        }

        $headstyles             = bp_headstyles(array(
            // Default CSS Plugin
            BE_PLUGIN_PATH . 'jquery-ui/jquery-ui-1.8.13.custom.css?ver=' . CSS_VER_MAIN,
            BE_PLUGIN_PATH . 'select2/dist/css/select2.min.css?ver=' . CSS_VER_MAIN,
        ));
        $loadscripts            = bp_scripts(array(
            // Default JS Plugin
            BE_PLUGIN_PATH . 'jquery-inputmask/jquery.inputmask.bundle.min.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'jquery-validation/dist/jquery.validate.min.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'select2/dist/js/select2.min.js?ver=' . JS_VER_MAIN,
            // Always placed at bottom
            BE_JS_PATH . 'form-validation.js?ver=' . JS_VER_BACK,
            BE_JS_PATH . 'custom.js?ver=' . JS_VER_BACK
        ));
        $scripts_init           = bp_scripts_init(array(
            'App.select2();',
            'InputMask.init();',
            'SelectChange.init();',
            'Profile.init();',
            'FV_Profile.init();'
        ));
        $scripts_add            = '';

        $data['title']          = TITLE . 'Profil Member';
        $data['member']         = $current_member;
        $data['member_other']   = $member_data;
        $data['is_admin']       = $is_admin;
        $data['headstyles']     = $headstyles;
        $data['scripts']        = $loadscripts;
        $data['scripts_init']   = $scripts_init;
        $data['scripts_add']    = $scripts_add;
        $data['main_content']   = 'member/profile';

        $this->load->view(VIEW_BACK . 'template_index', $data);
    }

    /**
     * Error 404 Page function.
     */
    public function error_404()
    {
        // This is for AJAX request
        if ($this->input->is_ajax_request()) {
            // Set JSON data
            $data = array('success' => false, 'status' => 'error', 'message' => 'Terjadi kesalahan sistem! Ulangi proses beberapa saat lagi.', 'data' => '');
            die(json_encode($data));
        }

        auth_redirect();

        $current_member         = bp_get_current_member();
        $is_admin               = as_administrator($current_member);

        $headstyles             = bp_headstyles(array(
            // Default CSS Plugin
        ));

        $loadscripts            = bp_scripts(array(
            // Default JS Plugin
            BE_JS_PATH . 'custom.js?ver=' . JS_VER_BACK,
        ));

        $scripts_init           = '';
        $scripts_add            = '';

        $data['title']          = TITLE . '404 Page Not Found';
        $data['member']         = $current_member;
        $data['is_admin']       = $is_admin;
        $data['headstyles']     = $headstyles;
        $data['scripts']        = $loadscripts;
        $data['scripts_init']   = $scripts_init;
        $data['scripts_add']    = $scripts_add;
        $data['main_content']   = 'error_404';

        $this->load->view(VIEW_BACK . 'template_index', $data);
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Coming Soon View function.
     */
    function comingsoon()
    {
        auth_redirect();

        $current_member         = bp_get_current_member();
        $is_admin               = as_administrator($current_member);

        $data['title']          = TITLE . 'Coming Soon';
        $data['member']         = $current_member;
        $data['is_admin']       = $is_admin;
        $data['main_content']   = 'pages/comingsoon';

        $this->load->view(VIEW_BACK . 'template', $data);
    }

    // ---------------------------------------------------------------------------------------------

    // =============================================================================================
    // ASSUME AND REVERT ACCOUNT
    // =============================================================================================

    /**
     * Assume to member account
     *
     * @since 1.0.0
     * @access public
     *
     * @param int $member_id. Member ID.
     * @author Iqbal
     */
    function assume($member_id)
    {
        $this->auth(true);
        $current_member = bp_get_current_member();
        $uid            = $current_member->username;
        $type           = 'admin';
        if ($staff = bp_get_current_staff()) {
            $uid        = $staff->username;
            $type       = 'staff';
        }
        $id         = bp_encrypt($member_id, 'decrypt');
        $log_desc   = array('cookie' => $_COOKIE, 'type' => $type);
        bp_log_action('ASSUME', $id, $uid, json_encode($log_desc));
        bp_assume($id);
    }

    /**
     * Revert account
     *
     * @since 1.0.0
     * @access public
     *
     * @author ahmad
     */
    function revert()
    {
        bp_revert();
    }

    /**
     * Switch Language function.
     */
    function switchlang($lang = '')
    {
        if ($this->input->is_ajax_request()) {
            die('true');
        } else {
            $url  = $this->uri->uri_string();
            if ($url == 'switchlang') {
                redirect(base_url('dashboard'), 'refresh');
            } else {
                redirect($url);
            }
        }
    }

    // Ubah foto profile
    function ubah_foto_profile()
    {
        auth_redirect();
        $current_member = bp_get_current_member();
        $file = upload_file('file', 'images', ASSET_FOLDER . '/upload/profile_picture/');
        if ($file != 'error_upload' && $file != 'error_extension' && $file != 'error' && $file != 'empty') {
            $data_member = array(
                'id' => $current_member->id,
                'photo' => $file,
                'datemodified' => date('Y-m-d H:i:s')
            );
            $this->Model_Member->update_member($data_member);
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Upload Foto Profile Berhasil!</div>');
            redirect(base_url('profile'));
        } else {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Upload Foto Profile Gagal!</div>');
            redirect(base_url('profile'));
        }
    }
    // ---------------------------------------------------------------------------------------------
}

/* End of file Backend.php */
/* Location: ./application/controllers/Backend.php */
