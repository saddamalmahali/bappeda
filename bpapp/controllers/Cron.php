<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Cron Controller.
 *
 * @class     Cron
 * @author    Yuda
 * @version   1.0.0
 */
class Cron extends BP_Controller
{

    /**
     * ADVcron.
     */
    protected $codecron = '$2y$05$ZJIOlcrh3NNTrEJFWDen2uBZoGtDDt4tO239yB6dNIY7sesPmGaQG'; // CRONkaidah

    /**
     * Constructor.
     */
    function __construct()
    {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
    }
    /**
     * Start Cron Process
     */
    private function _start_cron($cron_name = '', $debug = true, $cron_exist = false, $cli = true)
    {
        $this->benchmark->mark('started');
        if (!$debug) {
            if ( $cli ) {
                if ( ! $this->input->is_cli_request() ){
                    echo "Access Denied! No direct script access allowed!'";
                    echo "</pre>";
                    die();
                }
            }
            $cron_name = $cron_name ? $cron_name : 'cronjob';
            if ( $cron_exist ) {
                $date   = date('Y-m-d');
                $check_cron_exist = bp_check_log_cron($cron_name, $date);
                if ( $check_cron_exist ) {
                    die('Cron Already Execute.');
                }
            }
            bp_log_cron($cron_name, 'STARTED');
        }
        echo "<pre>";
    }

    /**
     * End Cron Process
     */
    private function _end_cron($cron_name = '', $debug = true, $log = '')
    {
        $this->benchmark->mark('ended');
        $elapsed_time   = $this->benchmark->elapsed_time('started', 'ended');
        $elapsed_time   = 'Elapsed Time : ' . $elapsed_time . ' seconds';
        if (!$debug) {
            $cron_name  = $cron_name ? $cron_name : 'cronjob';
            $log_desc   = $elapsed_time . ', Log : ' . $log;
            bp_log_cron($cron_name, 'ENDED', $log_desc);
        }
        echo  $elapsed_time . br();
        echo '</pre>';
    }

    /**
     * CRON JOB : Withdraw
     *
     * @param    String     $code       default ''
     * @param    boolean    $debug      default true
     * @param    Date       $date       default current date
     * @author   Yuda
     */
    function withdraw($keycode = '', $debug = true, $date = '')
    {
        set_time_limit(0);

        if (!$keycode) die();

        $keycode    = trim($keycode);
        $validate   = bp_hash_verify($keycode, $this->codecron);

        if (!$validate) die();

        $datetime               = $date ? date('Y-m-d H:i:s', strtotime($date)) : date('Y-m-d 23:59:5', strtotime('-1 day')) . rand(0, 9);
        $date                   = $date ? date('Y-m-d', strtotime($date)) : date('Y-m-d', strtotime('-1 day'));

        // Config Withdraw
        $withdraw_minimal       = get_option('setting_withdraw_minimal');
        $withdraw_minimal       = $withdraw_minimal ? $withdraw_minimal : 0;
        $admin_fee              = get_option('setting_withdraw_fee');
        $admin_fee              = isset($admin_fee) ? $admin_fee : 0;
        $currency               = config_item('currency');
        $cron_log               = '';

        $this->_start_cron('Withdraw', $debug, true);
        echo '-------------------------------------------------------' . br();
        echo "                    Withdrawal" . br();
        echo '-------------------------------------------------------' . br();
        echo ' Function     : ' . ($debug ? 'View' : 'Save') . br();
        echo ' Datetime     : ' . $datetime . br();
        echo '-------------------------------------------------------' . br();
        echo ' WD Minimal   : ' . bp_accounting($withdraw_minimal) . br();
        echo ' Admin Fee    : ' . bp_accounting($admin_fee) . br();
        echo '-------------------------------------------------------' . br();

        $condition  = ' AND %wd_status% = 0 AND %status% = ' . ACTIVE . ' ';
        $data       = $this->Model_Bonus->get_all_total_ewallet_member(0, 0, $condition, '', ' %total% >= ' . $withdraw_minimal, $date);

        if ($data && $withdraw_minimal) {
            echo ' Total Data   : ' . count($data) . br();
            echo '-------------------------------------------------------' . br(3);
            $no = 0;
            foreach ($data as $row) {
                $wd_nominal             = $row->total_deposite;
                if ($withdraw_minimal > $wd_nominal) {
                    continue;
                }
                
                if (!$row->bank || !$row->bill) {
                    continue;
                }

                $bank_code              = '';
                if ($bankdata = bp_banks($row->bank)) {
                    $bank_code          = $bankdata->flipcode;
                }

                $tax                    = bp_calc_tax($wd_nominal, $row->npwp);
                $bill_name              = $row->bill_name ? $row->bill_name : $row->name;
                $amount_receipt         = $wd_nominal - $tax - $admin_fee;

                echo "No. " . ($no += 1) . br();
                echo '-------------------------------------------------------' . br();
                echo 'ID Member             : ' . $row->id . br();
                echo 'Username              : ' . $row->username . ' - ' . $row->name . br();
                echo 'Bank                  : ' . $row->bank . br();
                echo 'Bill                  : ' . $row->bill . ' (' . $bill_name . ')' . br();
                echo '-------------------------------------------------------' . br();
                echo 'Nominal WD            = ' . bp_accounting($wd_nominal, 'Rp') . br();
                echo 'Biaya Transfer        = ' . bp_accounting($admin_fee, 'Rp') . br();
                echo '-------------------------------------------------------' . br();
                echo 'WD Diterima           = ' . bp_accounting($amount_receipt, 'Rp') . br();
                echo '-------------------------------------------------------' . br(3);

                if (!$debug && $row->bank && $row->bill) {
                    // -------------------------------------------------
                    // Begin Transaction
                    // -------------------------------------------------
                    $this->db->trans_begin();

                    $data_withdraw          = array(
                        'id_member'         => $row->id,
                        'bank'              => $row->bank,
                        'bank_code'         => $bank_code,
                        'bill'              => $row->bill,
                        'bill_name'         => $bill_name,
                        'nominal'           => $wd_nominal,
                        'nominal_receipt'   => $amount_receipt,
                        'tax'               => $tax,
                        'admin_fund'        => $admin_fee,
                        'datecreated'       => $datetime,
                        'datemodified'      => $datetime
                    );
                    if (!$withdraw_id  = $this->Model_Bonus->save_data_withdraw($data_withdraw)) {
                        $this->db->trans_rollback();
                        $separated          = $cron_log ? ', ' : '';
                        $cron_log          .= $separated . 'ID member : ' . $row->id . ' (Withdraw Failed Save)';
                        continue;
                    }

                    $data_ewallet = array(
                        'id_member'     => $row->id,
                        'id_source'     => $withdraw_id,
                        'amount'        => $wd_nominal,
                        'source'        => 'withdraw',
                        'type'          => 'OUT',
                        'status'        => 1,
                        'description'   => 'Withdraw tgl ' . date('Y-m-d', strtotime($datetime)) . ' ' . bp_accounting($wd_nominal, $currency),
                        'datecreated'   => $datetime
                    );
                    if (!$wallet_id  = $this->Model_Bonus->save_data_ewallet($data_ewallet)) {
                        $this->db->trans_rollback();
                        $separated          = $cron_log ? ', ' : '';
                        $cron_log          .= $separated . 'ID Withdraw : ' . $withdraw_id . ' (Wallet Failed Save)';
                        continue;
                    }

                    // -------------------------------------------------
                    // Commit or Rollback Transaction
                    // -------------------------------------------------
                    if ($this->db->trans_status() === FALSE) {
                        // Rollback Transaction
                        $this->db->trans_rollback();
                    } else {
                        // Commit Transaction
                        $this->db->trans_commit();
                        // Complete Transaction
                        $this->db->trans_complete();
                    }
                }
            }
        } else {
            $cron_log = 'Data Deposite tidak ditemukan';
            echo br(1) . " " . $cron_log . br();
        }

        echo br(2) . '----------------------------------------------------' . br();
        $this->_end_cron('Withdraw', $debug, $cron_log);
        if ( ! $debug ) {
            $this->daily_flip_bank_inquiry($keycode, $debug, $datetime);
        }
    }

    /**
     * CRON JOB : Daily Inquiry Cron
     *
     * @param    boolean    $code    default true
     * @param    Date       $date    default current date
     * @author   Yuda
     */
    function daily_flip_bank_inquiry($keycode = '', $debug = true, $date = '', $cli = true)
    {
        set_time_limit(0);
        if (!$keycode) die();

        $keycode        = trim($keycode);
        $validate       = password_verify($keycode, $this->codecron);

        if (!$validate) die();

        $start_date     = $date ? date('Y-m-d', strtotime($date)) : date('Y-m-d', strtotime('-1 day'));
        $datetime       = $start_date . ' ' . date('23:59:5') . rand(5, 9);
        $flip_active    = get_option('flip_active');
        $secret_key     = get_option('flip_secret');
        $url_inquiry    = config_item('flip_url_bank_inquiry');
        $total_data     = 0;
        $total_inquiry  = 0;

        if ( $flip_active != ACTIVE ) {
            die('Fitur Flip belum diaktifkan !');
        }

        $this->_start_cron('FLIP_INQUIRY', $debug, false, $cli);
        echo '----------------------------------------------------' . br();
        echo ' FLIP INQUIRY CRON' . br();
        echo '----------------------------------------------------' . br();
        echo ' Function     : ' . ($debug ? 'Debug ' : 'Save ') . br();
        echo ' Date         : ' . $start_date . br();
        echo ' Datetime     : ' . $datetime . br();
        echo '----------------------------------------------------' . br();

        $params     = array($start_date);
        $condition  = ' AND %status% = 0 AND DATE(%datecreated%) = ?';
        if ($data = $this->Model_Bonus->get_all_member_withdraw(0, 0, $condition, '%id% ASC', $params)) {
            $total_data = count($data);
            echo ' Total Member : ' . $total_data . br();
            echo '----------------------------------------------------' . br(3);
            $no = 1;
            foreach ($data as $k => $row) {
                $bank_code          = $row->bank_code ? $row->bank_code : $row->flipcode;
                $account_number     = trim($row->bill);
                $account_holder     = strtoupper($row->bill_name);
                $idempotency        = $bank_code .'-'. $account_number;

                echo " No. " . ($no) . br();
                echo '-------------------------------------------------------' . br();
                echo ' ID Member     : ' . $row->id_member . br();
                echo ' Username      : ' . $row->username . br();
                echo ' Name          : ' . $row->name . br();
                echo ' Bank          : ' . $bank_code . ' (' . $row->bank . ')' . br();
                echo ' No. Rek       : ' . $account_number . br();
                echo ' A.N. Rek      : ' . $account_holder . br();
                echo '-------------------------------------------------------' . br();

                if ( !$debug && $bank_code ) {
                    $total_inquiry         += 1;
                    $payloads               = array("account_number" => $account_number, "bank_code" => $bank_code);
                    $ch                     = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url_inquiry);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                    curl_setopt($ch, CURLOPT_TIMEOUT, 50);
                    curl_setopt($ch, CURLOPT_HEADER, FALSE);
                    curl_setopt($ch, CURLOPT_POST, TRUE);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($payloads));
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        "Content-Type: application/x-www-form-urlencoded",
                        'idempotency-key: ' . md5($idempotency)
                    ));
                    curl_setopt($ch, CURLOPT_USERPWD, $secret_key . ":");

                    $response               = curl_exec($ch);
                    $curl_error             = curl_error($ch);
                    curl_close($ch);

                    if ($curl_error || $response === false) {
                        // bp_log_flip('FLIP_INQUIRY_CRON', 'ERROR', json_encode($payloads));
                        continue;
                    }

                    $decoded_data   = json_decode($response);
                    $bank_code      = isset($decoded_data->bank_code) ? $decoded_data->bank_code : $bank_code;
                    $account_number = isset($decoded_data->account_number) ? $decoded_data->account_number : $account_number;
                    $account_holder = (isset($decoded_data->account_holder) && !empty($decoded_data->account_holder)) ? $decoded_data->account_holder : $account_holder;
                    $status         = isset($decoded_data->status) ? $decoded_data->status : 'INVALID_ACCOUNT_NUMBER';

                    $data_inquiry   = array(
                        "bank_code"         => $bank_code,
                        "account_number"    => $account_number,
                        "account_holder"    => $account_holder,
                        "status"            => $status,
                        "datecreated"       => $datetime
                    );

                    if ($inquiry = $this->Model_Flip->get_detail_flip_inquiry_by_account($account_number)) {
                        unset($data_inquiry['account_number']);
                        unset($data_inquiry['datecreated']);
                        $this->Model_Flip->update_flip_inquiry_data($account_number, '', $data_inquiry);
                    } else {
                        $this->Model_Flip->save_data_flip_inquiry($data_inquiry);
                    }

                    if ( $status ) {
                        if ( $withdraw = $this->Model_Bonus->get_withdraw_by_id($row->id) ) {
                            if ( $withdraw->bank_code == $bank_code && trim($withdraw->bill) == $account_number ) {
                                $datawithdraw   = array( 'bill_name' => $account_holder, 'inquiry_status' => strtoupper($status) );
                                $update_wd_data = $this->Model_Bonus->update_data_withdraw($row->id, $datawithdraw);
                            }
                        }
                    }

                    echo ' Inquiry       : ' . $status . br();
                    echo '-------------------------------------------------------' . br();
                }

                echo br(2);
                $no++;
            }
        }

        echo br() . '----------------------------------------------------' . br();
        $log_desc = 'Total Data WD : ' . bp_accounting($total_data) .' Total Inquiry : '. bp_accounting($total_inquiry);
        echo ' ' . $log_desc . br();
        echo '----------------------------------------------------' . br();
        $this->_end_cron('FLIP_INQUIRY', $debug, $log_desc);
    }

    /**
     * CRON JOB : Expired Order
     *
     * @param    boolean    $code    default true
     * @param    Date       $date    default current date
     * @author   Yuda
     */
    function expired_order($keycode = '', $debug = true, $date = '') {
        set_time_limit( 0 );
        if ( ! $keycode ) die();

        $keycode        = trim($keycode);
        $validate       = password_verify($keycode, $this->codecron);
        
        if ( ! $validate ) die();

        $datetime       = $date ? date('Y-m-d H:i:s', strtotime($date)) : date('Y-m-d 23:59:59',strtotime('-1 day'));
        $iResult        = 0;
        $total_cancel   = 0;

        $this->_start_cron('Expired_Order', $debug, false, false);
        echo '----------------------------------------------------'. br();
        echo ' Expired Product Order'. br();
        echo '----------------------------------------------------'. br();
        echo ' Function       : ' . ( $debug ? 'Debug' : 'Save' ) . br();
        echo '----------------------------------------------------'. br();
        echo ' Datetime       : ' . $datetime . br();
        echo '----------------------------------------------------'. br(3);

        $condition      = ' AND %status% = 0 AND %dateexpired% <= ?';
        $params         = array($datetime);
        if ( $data_list = $this->Model_Shop->get_all_shop_order_data(0, 0, $condition, '%dateexpired% ASC', $params) ) {
            $iResult    = count($data_list);
            $no         = 0;
            foreach ($data_list as $k => $row) {
                echo " No. " . ($no += 1) . br();
                echo '-------------------------------------------------------' . br();
                echo ' ID Order       : ' . $row->id . br();
                echo ' Invoice        : ' . $row->invoice . br();
                echo '-------------------------------------------------------' . br();
                echo ' Type           : ' . $row->type_order . br();
                echo '-------------------------------------------------------' . br();
                echo ' ID Member      : ' . $row->id_member . br();
                echo ' Username       : ' . $row->username . ' - ' . $row->membername . br();
                echo '-------------------------------------------------------' . br();

                if ( $row->id_stockist ) {
                    echo ' ID Stockist    : ' . $row->id_stockist . br();
                    echo ' Stockist       : ' . $row->stockist . ' - ' . $row->stockistname . br();
                    echo '-------------------------------------------------------' . br();
                }

                echo ' Total Payment  = ' . bp_accounting($row->total_payment, 'Rp') . br();
                echo '-------------------------------------------------------' . br();

                if ( !$debug ) {
                    // Update status shop order
                    $data_order     = array(
                        'status'        => 4,
                        'datemodified'  => $datetime,
                        'modified_by'   => 'expired',
                    );

                    if ( $update_shop_order = $this->Model_Shop->update_data_shop_order($row->id, $data_order)) {
                        $total_cancel += 1;
                        echo ' Success Update' .br();
                    } else {
                        echo ' Failed Update' .br();
                    }
                    echo '-------------------------------------------------------' . br();
                }


                echo br(2);
            }
        }

        echo br() . '----------------------------------------------------' . br();
        $log_desc = 'Total Data (' . bp_accounting($iResult) .') Total Cancelled ('. bp_accounting($total_cancel) .')';
        echo $log_desc . br();
        echo '----------------------------------------------------' . br();
        $this->_end_cron('Expired_Order', $debug, $log_desc);
    }

    /**
     * CRON JOB : Bonus Pairing
     *
     * @param    boolean    $code    default true
     * @param    Date       $date    default current date
     * @author   Yuda
     */
    function bonus_pairing($keycode = '', $debug = true, $date = '') {
        set_time_limit( 0 );
        if ( ! $keycode ) die();

        $keycode        = trim($keycode);
        $validate       = password_verify($keycode, $this->codecron);
        
        if ( ! $validate ) die();

        $start_date     = $date ? date('Y-m-d', strtotime($date)) : date('Y-m-d',strtotime('-1 day'));
        $start_calc     = config_item('start_calculation');
        if ( date('Ymd', strtotime($start_date)) <= date('Ymd', strtotime($start_calc)) ) {
            $start_date = date('Y-m-d');
        }

        $datetime       = $start_date . ' ' . date('23:59:3') . rand(0,9); 
        $reg_member     = array();
        $ro_member      = array();

        $this->_start_cron('Bonus_Pairing', $debug);
        echo '----------------------------------------------------'. br();
        echo ' Calculate Bonus Pairing'. br();
        echo '----------------------------------------------------'. br();
        echo ' Member Join at '. date('d F Y', strtotime($start_date)) . br();
        echo '----------------------------------------------------'. br();
        echo ' Function    : ' . ( $debug ? 'Debug ' : 'Save ' ) . br();
        echo ' Datecreated : ' . $start_date . br();
        echo ' Datetime    : ' . $datetime . br();
        echo '----------------------------------------------------'. br();

        $member_omzet   = '';
        $condition      = ' WHERE DATE(%datecreated%) <= "'.$start_date.'"';
        if ( $data_omzet = $this->Model_Member->get_all_member_data(0, 0, $condition, '%datecreated% ASC') ) {
            $iResult    = count($data_omzet);
            foreach ($data_omzet as $k => $val) {
                $member_omzet .= $val->tree;

                if ( ($k+1) < $iResult ) {
                    $member_omzet .= '-';
                }
            }

            if ( $member_omzet ) {
                if(substr($member_omzet, -1) == '-') $member_omzet = substr_replace($member_omzet, '', -1);
                $member_omzet = explode('-', $member_omzet);
                $member_omzet = array_unique($member_omzet);
            }
        }
        if ( $member_omzet ) {
            rsort($member_omzet);
            $count_member = count($member_omzet);
            echo ' Total Member : ' . ($count_member-1) . br();
            echo '----------------------------------------------------'. br(3);
            $no =1;
            foreach ($member_omzet as $key => $id) {
                if ( $id == 1 ) { continue; }
                echo ($no++) .".  Member ID : " . $id . br();
                // -------------------------------------------------
                // calculate bonus pairing
                // -------------------------------------------------
                bp_calculate_pairing_bonus($id, $datetime, $debug);
                echo br(2);
            }
        }

        echo br(2) . '----------------------------------------------------'. br();
        $this->_end_cron('Bonus_Pairing', $debug);
    }
}
