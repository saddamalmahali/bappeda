<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Setting Controller.
 *
 * @class     Setting
 * @version   1.0.0
 */
class Setting extends Admin_Controller
{
    /**
     * Constructor.
     */
    function __construct()
    {
        parent::__construct();
    }

    // =============================================================================================
    // SETTING PAGE
    // =============================================================================================

    /**
     * Setting General function.
     */
    function general()
    {
        auth_redirect();

        $current_member         = bp_get_current_member();
        $is_admin               = as_administrator($current_member);

        $headstyles             = bp_headstyles(array(
            // Default CSS Plugin
        ));

        $loadscripts            = bp_scripts(array(
            // Default JS Plugin
            BE_PLUGIN_PATH . 'bootstrap-notify/bootstrap-notify.min.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'bootbox/bootbox.min.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'jquery-inputmask/jquery.inputmask.bundle.min.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'jquery-validation/dist/jquery.validate.min.js?ver=' . JS_VER_MAIN,
            // Always placed at bottom
            BE_JS_PATH . 'form-validation.js?ver=' . JS_VER_BACK,
            BE_JS_PATH . 'custom.js?ver=' . JS_VER_BACK,
        ));

        $scripts_init           = bp_scripts_init(array(
            'InputMask.init();',
            'SelectChange.init();',
            'GeneralSetting.init();'
        ));
        $scripts_add            = '';

        $data['title']          = TITLE . lang('menu_setting') . ' ' . lang('menu_setting_general');
        $data['member']         = $current_member;
        $data['is_admin']       = $is_admin;
        $data['headstyles']     = $headstyles;
        $data['scripts']        = $loadscripts;
        $data['scripts_init']   = $scripts_init;
        $data['scripts_add']    = $scripts_add;
        $data['main_content']   = 'setting/general';

        $this->load->view(VIEW_BACK . 'template_index', $data);
    }

    /**
     * Setting Notification function.
     */
    public function notification()
    {
        auth_redirect();

        $current_member         = bp_get_current_member();
        $is_admin               = as_administrator($current_member);

        $headstyles             = bp_headstyles(array(
            // Default CSS Plugin
            BE_PLUGIN_PATH . 'datatables/dataTables.bootstrap.css?ver=' . CSS_VER_MAIN
        ));
        $loadscripts            = bp_scripts(array(
            // Default JS Plugin
            BE_PLUGIN_PATH . 'bootstrap-notify/bootstrap-notify.min.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'bootbox/bootbox.min.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'jquery-validation/dist/jquery.validate.min.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'datatables/jquery.dataTables.min.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'datatables/dataTables.bootstrap.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'datatables/datatable.js?ver=' . JS_VER_MAIN,
            BE_PLUGIN_PATH . 'ckeditor/ckeditor.js?ver=' . JS_VER_MAIN,
            // Always placed at bottom
            BE_JS_PATH . 'form-validation.js?ver=' . JS_VER_BACK,
            BE_JS_PATH . 'table-ajax.js?ver=' . JS_VER_BACK,
            BE_JS_PATH . 'custom.js?ver=' . JS_VER_BACK
        ));
        $scripts_init           = bp_scripts_init(array(
            'GeneralSetting.init();',
            'FV_Notification.init();',
            'TableAjaxNotifList.init();'
        ));
        $scripts_add            = '';

        $data['title']          = TITLE . lang('menu_setting') . ' ' . lang('menu_setting_notification');
        $data['member']         = $current_member;
        $data['is_admin']       = $is_admin;
        $data['headstyles']     = $headstyles;
        $data['scripts']        = $loadscripts;
        $data['scripts_init']   = $scripts_init;
        $data['scripts_add']    = $scripts_add;
        $data['main_content']   = 'setting/notifications';

        $this->load->view(VIEW_BACK . 'template_index', $data);
    }

    /**
     * Setting Stages function.
     */
    public function stages($form = '', $id = '')
    {
        auth_redirect();

        $dataform               = '';
        $current_member         = bp_get_current_member();
        $is_admin               = as_administrator($current_member);

        if ($form) {
            if ($form != 'create' && $form != 'edit') {
                redirect(base_url('setting/stages'), 'refresh');
            }
            if ($form == 'create' && $id) {
                redirect(base_url('setting/stages'), 'refresh');
            }
            if ($form == 'edit' && !$id) {
                redirect(base_url('setting/stages'), 'refresh');
            }

            $id = bp_decrypt($id);
            if ($form == 'edit' && $id) {
                if (!$dataform = $this->Model_Option->get_stages_by('id', $id)) {
                    redirect(base_url('setting/stages'), 'refresh');
                }
            }

            $main_content           = 'setting/form/stages';
            $headstyles             = bp_headstyles(array(
                // Default CSS Plugin
            ));
            $loadscripts            = bp_scripts(array(
                // Default JS Plugin
                BE_PLUGIN_PATH . 'bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js?ver=' . JS_VER_MAIN,
                BE_PLUGIN_PATH . 'jquery-inputmask/jquery.inputmask.bundle.min.js?ver=' . JS_VER_MAIN,
                BE_PLUGIN_PATH . 'jquery-validation/dist/jquery.validate.min.js?ver=' . JS_VER_MAIN,
                // Always placed at bottom
                BE_JS_PATH . 'table-ajax.js?ver=' . JS_VER_BACK,
                BE_JS_PATH . 'custom.js?ver=' . JS_VER_BACK
            ));
            $scripts_init           = bp_scripts_init(array(
                'InputMask.init();',
                'HandleDatepicker.init();',
                'GeneralSetting.initStages();'
            ));
        } else {
            $main_content           = 'setting/stages';
            $headstyles             = bp_headstyles(array(
                // Default CSS Plugin
                BE_PLUGIN_PATH . 'datatables/dataTables.bootstrap.css?ver=' . CSS_VER_MAIN
            ));
            $loadscripts            = bp_scripts(array(
                // Default JS Plugin
                BE_PLUGIN_PATH . 'jquery-inputmask/jquery.inputmask.bundle.min.js?ver=' . JS_VER_MAIN,
                BE_PLUGIN_PATH . 'bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js?ver=' . JS_VER_MAIN,
                BE_PLUGIN_PATH . 'datatables/jquery.dataTables.min.js?ver=' . JS_VER_MAIN,
                BE_PLUGIN_PATH . 'datatables/dataTables.bootstrap.js?ver=' . JS_VER_MAIN,
                BE_PLUGIN_PATH . 'datatables/datatable.js?ver=' . JS_VER_MAIN,
                // Always placed at bottom
                BE_JS_PATH . 'table-ajax.js?ver=' . JS_VER_BACK,
                BE_JS_PATH . 'custom.js?ver=' . JS_VER_BACK
            ));
            $scripts_init           = bp_scripts_init(array(
                'InputMask.init();',
                'TableAjaxSettingStagesList.init();'
            ));
        }

        $alert_msg              = '';
        if ($this->session->userdata('alert_msg')) {
            $alert_msg          = $this->session->userdata('alert_msg');
            $this->session->unset_userdata('alert_msg');
        }

        $title_page             = lang('menu_setting_stages');
        $data['title']          = TITLE . lang('menu_setting') . ' ' . $title_page;
        $data['title_page']     = $title_page;
        $data['member']         = $current_member;
        $data['is_admin']       = $is_admin;
        $data['form']           = $form;
        $data['dataform']       = $dataform;
        $data['alert_msg']      = $alert_msg;
        $data['headstyles']     = $headstyles;
        $data['scripts']        = $loadscripts;
        $data['scripts_init']   = $scripts_init;
        $data['scripts_add']    = '';
        $data['main_content']   = $main_content;

        $this->load->view(VIEW_BACK . 'template_index', $data);
    }

    // ---------------------------------------------------------------------------------------------

    // =============================================================================================
    // LIST DATA SETTING
    // =============================================================================================

    /**
     * Setting Stages List Data function.
     */
    function stageslistdata()
    {
        if (!$this->input->is_ajax_request()) {
            redirect(base_url('setting/reward'), 'refresh');
        }
        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            $data = array('status' => 'login', 'url' => base_url('login'));
            die(json_encode($data)); // JSON encode data
        }

        $current_member     = bp_get_current_member();
        $is_admin           = as_administrator($current_member);

        $params             = array();
        $condition          = '';
        $order_by           = '';
        $iTotalRecords      = 0;

        $iDisplayLength     = intval($_REQUEST['iDisplayLength']);
        $iDisplayStart      = intval($_REQUEST['iDisplayStart']);

        $sEcho              = intval($_REQUEST['sEcho']);
        $sort               = $_REQUEST['sSortDir_0'];
        $column             = intval($_REQUEST['iSortCol_0']);

        $limit              = ($iDisplayLength == '-1' ? 0 : $iDisplayLength);
        $offset             = $iDisplayStart;

        $s_year             = $this->input->post('search_year');
        $s_year             = bp_isset($s_year, '');
        $s_stage            = $this->input->post('search_stage');
        $s_stage            = bp_isset($s_stage, '');
        $s_sub_stage        = $this->input->post('search_sub_stage');
        $s_sub_stage        = bp_isset($s_sub_stage, '');
        $s_status           = $this->input->post('search_status');
        $s_status           = bp_isset($s_status, '');
        $s_startdate_min    = $this->input->post('search_startdate_min');
        $s_startdate_min    = bp_isset($s_startdate_min, '');
        $s_startdate_max    = $this->input->post('search_startdate_max');
        $s_startdate_max    = bp_isset($s_startdate_max, '');
        $s_enddate_min      = $this->input->post('search_enddate_min');
        $s_enddate_min      = bp_isset($s_enddate_min, '');
        $s_enddate_max      = $this->input->post('search_enddate_max');
        $s_enddate_max      = bp_isset($s_enddate_max, '');

        if (!empty($s_year))            { $condition .= ' AND %year% LIKE CONCAT("%", ?, "%")'; $params[] = $s_year; }
        if (!empty($s_stage))           { $condition .= ' AND %stage% LIKE CONCAT("%", ?, "%")'; $params[] = $s_stage; }
        if (!empty($s_sub_stage))       { $condition .= ' AND %sub_stage% LIKE CONCAT("%", ?, "%")'; $params[] = $s_sub_stage; }
        if (!empty($s_startdate_min))   { $condition .= ' AND DATE(%start_date%) >= ?'; $params[] = $s_startdate_min; }
        if (!empty($s_startdate_max))   { $condition .= ' AND DATE(%start_date%) <= ?'; $params[] = $s_startdate_max; }
        if (!empty($s_enddate_min))     { $condition .= ' AND DATE(%end_date%) >= ?'; $params[] = $s_enddate_min; }
        if (!empty($s_enddate_max))     { $condition .= ' AND DATE(%end_date%) <= ?'; $params[] = $s_enddate_max; }
        if ( $s_status == 'done' )      { $condition .= ' AND %status% = 2'; }
        if ( $s_status == 'active' )    { $condition .= ' AND %status% = 1'; }
        if ( $s_status == 'notactive' ) { $condition .= ' AND %status% = 0'; }

        if ($column == 1) { $order_by .= '%year% ' . $sort; } 
        if ($column == 2) { $order_by .= '%stage% ' . $sort; } 
        if ($column == 3) { $order_by .= '%sub_stage% ' . $sort; }
        if ($column == 4) { $order_by .= '%start_date% ' . $sort; }
        if ($column == 5) { $order_by .= '%end_date% ' . $sort; }
        if ($column == 6) { $order_by .= '%status% ' . $sort; }

        $data_list          = $this->Model_Option->get_all_stages_data($limit, $offset, $condition, $order_by);
        $records            = array();
        $records["aaData"]  = array();

        if (!empty($data_list)) {
            $iTotalRecords  = bp_get_last_found_rows();
            $cfg_stages     = config_item('stages');
            $cfg_sub_stages = config_item('sub_stages');
            $i = $offset + 1;
            foreach ($data_list as $row) {
                $id             = bp_encrypt($row->id);
                $stage_name     = isset($cfg_stages[$row->stage]) ? lang($cfg_stages[$row->stage]) : $row->stage;
                $sub_stage_name = isset($cfg_sub_stages[$row->stage][$row->sub_stage]['menu']) ? lang($cfg_sub_stages[$row->stage][$row->sub_stage]['menu']) : $row->sub_stage;

                $status = '<a href="javascript:;" class="btn btn-sm btn-outline-danger btn-status-setting-stages"
                            data-url="' . base_url('setting/stagestatus/' . $id) . '" 
                            data-year="' . $row->year . '" 
                            data-stage="' . $stage_name . '" 
                            data-substage="' . $sub_stage_name . '" 
                            data-status="notactive">Tidak Aktif</a>';

                if ($row->status == 1) {
                    $status = '<a href="javascript:;" class="btn btn-sm btn-outline-primary btn-status-setting-stages"
                                data-url="' . base_url('setting/stagestatus/' . $id) . '" 
                                data-year="' . $row->year . '" 
                                data-stage="' . $stage_name . '" 
                                data-substage="' . $sub_stage_name . '" 
                                data-status="active"><i class="fa fa-check"></i> Aktif</a>';
                }

                if ($row->status == 2) {
                    $status = '<a href="javascript:;" class="btn btn-sm btn-outline-success btn-status-setting-stages"
                                data-url="' . base_url('setting/stagestatus/' . $id) . '" 
                                data-year="' . $row->year . '" 
                                data-stage="' . $stage_name . '" 
                                data-substage="' . $sub_stage_name . '" 
                                data-status="done">Selesai</a>';
                }

                $btn_edit   = '<a class="btn btn-sm btn-tooltip btn-primary" title="Edit" href="' . base_url('setting/stages/edit/' . $id) . '"><i class="fa fa-edit"></i></a>';
                $btn_delete = '<a class="btn btn-sm btn-tooltip btn-warning btn-delete-stages" title="Hapus" href="javascript:;" 
                                data-url="' . base_url('setting/stagedelete/' . $id) . '"
                                data-year="' . $row->year . '" 
                                data-stage="' . $stage_name . '" 
                                data-substage="' . $sub_stage_name . '" ><i class="fa fa-times"></i></a>';

                $records["aaData"][]    = array(
                    bp_center($i),
                    bp_center($row->year),
                    bp_center($stage_name),
                    bp_center($sub_stage_name),
                    bp_center(date('Y-m-d', strtotime($row->start_date))),
                    bp_center(date('Y-m-d', strtotime($row->end_date))),
                    bp_center($status),
                    bp_center($btn_edit . $btn_delete),
                );
                $i++;
            }
        }

        $end                = $iDisplayStart + $iDisplayLength;
        $end                = $end > $iTotalRecords ? $iTotalRecords : $end;

        $records["sEcho"]                   = $sEcho;
        $records["iTotalRecords"]           = $iTotalRecords;
        $records["iTotalDisplayRecords"]    = $iTotalRecords;

        echo json_encode($records);
    }

    /**
     * Setting Notification List Data function.
     */
    function notificationlistdata()
    {
        $member_data        = '';
        $current_member     = bp_get_current_member();
        $is_admin           = as_administrator($current_member);

        $condition          = '';
        $order_by           = '';
        $iTotalRecords      = 0;

        $iDisplayLength     = intval($_REQUEST['iDisplayLength']);
        $iDisplayStart      = intval($_REQUEST['iDisplayStart']);

        $sEcho              = intval($_REQUEST['sEcho']);
        $sort               = $_REQUEST['sSortDir_0'];
        $column             = intval($_REQUEST['iSortCol_0']);

        $limit              = ($iDisplayLength == '-1' ? 0 : $iDisplayLength);
        $offset             = $iDisplayStart;

        $s_name             = $this->input->post('search_name');
        $s_name             = bp_isset($s_name, '');
        $s_type             = $this->input->post('search_type');
        $s_type             = bp_isset($s_type, '');
        $s_status           = $this->input->post('search_status');
        $s_status           = bp_isset($s_status, '');

        if (!empty($s_name)) {
            $condition .= str_replace('%s%', $s_name, ' AND %name% LIKE "%%s%%"');
        }
        if (!empty($s_type)) {
            $condition .= str_replace('%s%', $s_type, ' AND %type% = "%s%"');
        }
        if (!empty($s_status)) {
            $s_status   = ($s_status == 'active') ? 1 : 0;
            $condition .= str_replace('%s%', $s_status, ' AND %status% = %s%');
        }

        if ($column == 1) {
            $order_by .= '%name% ' . $sort;
        } elseif ($column == 2) {
            $order_by .= '%type% ' . $sort;
        } elseif ($column == 3) {
            $order_by .= '%status% ' . $sort;
        }

        $data_list          = $this->Model_Option->get_all_notification_data($limit, $offset, $condition, $order_by);
        $records            = array();
        $records["aaData"]  = array();

        if (!empty($data_list)) {
            $iTotalRecords  = bp_get_last_found_rows();
            $i = $offset + 1;
            foreach ($data_list as $row) {
                $lbl_class  = 'default';
                if ($row->type == 'email') {
                    $lbl_class = 'primary';
                }
                if ($row->type == 'whatsapp') {
                    $lbl_class = 'success';
                }
                $type       = '<span class="badge badge-sm badge-' . $lbl_class . '">' . strtoupper($row->type) . '</span>';

                $status     = '<span class="badge badge-sm badge-danger">TIDAK AKTIF</span>';
                if ($row->status > 0) {
                    $status = '<span class="badge badge-sm badge-success">AKTIF</span>';
                }

                $btn_edit   = '<a class="btn btn-sm btn-tooltip btn-primary notifdata" title="Edit" href="' . base_url('setting/notifdata/' . $row->id . '/edit') . '"><i class="fa fa-edit"></i></a>';
                $btn_view   = '<a class="btn btn-sm btn-tooltip btn-secondary notifdata" title="View" href="' . base_url('setting/notifdata/' . $row->id . '/view') . '"><i class="fa fa-eye"></i></a>';

                $records["aaData"][]    = array(
                    bp_center($i),
                    $row->name,
                    bp_center($type),
                    bp_center($status),
                    bp_center($btn_edit . $btn_view),
                );
                $i++;
            }
        }

        $end                = $iDisplayStart + $iDisplayLength;
        $end                = $end > $iTotalRecords ? $iTotalRecords : $end;

        $records["sEcho"]                   = $sEcho;
        $records["iTotalRecords"]           = $iTotalRecords;
        $records["iTotalDisplayRecords"]    = $iTotalRecords;

        echo json_encode($records);
    }

    // ---------------------------------------------------------------------------------------------

    // =============================================================================================
    // ACTIONS SETTING
    // =============================================================================================

    /**
     * Get Data Notification function.
     */
    function notifdata($id = '', $action = '')
    {
        // Check for AJAX Request
        if (!$this->input->is_ajax_request()) {
            redirect(base_url('setting/notification'), 'location');
        }
        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            $data = array('status' => 'login', 'url' => base_url('login'));
            die(json_encode($data)); // JSON encode data
        }

        // ID Data 
        if (!$id) {
            $data = array('status' => 'error', 'message' => 'ID Notification tidak dikenali !');
            die(json_encode($data));
        }

        // Get Data Notification 
        if (!$notification = $this->Model_Option->get_notification_by('id', $id)) {
            $data = array('status' => 'error', 'message' => 'Data Notification tidak ditemukan !');
            die(json_encode($data));
        }

        $action     = $action ? $action : 'view';

        if ($action == 'view') {
            if ($notification->type == 'email') {
                $notification->content = bp_notification_email_template($notification->content, $notification->title);
            } else {
                $notification->content = '<div style="padding: 0px 15px"><pre>' . $notification->content . '</pre></div>';
            }
        } else {
            if ($notification->type != 'email') {
                $notification->content = strip_tags($notification->content);
            }
        }

        $data = array('status' => 'success', 'process' => $action, 'notification' => $notification, 'message' => 'Data Notification ditemukan.');
        die(json_encode($data));
    }

    /**
     * Update Setting General function.
     */
    function updatesetting($field = '')
    {
        // Check for AJAX Request
        if (!$this->input->is_ajax_request()) {
            redirect(base_url('setting/general'), 'location');
        }
        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            $data = array('status' => 'login', 'url' => base_url('login'));
            die(json_encode($data)); // JSON encode data
        }

        // Get Data Field 
        if (!$field) {
            $data = array('status' => 'error', 'message' => 'Update Setting tidak berhasil. Data Setting tidak ditemukan !');
            die(json_encode($data));
        }

        // Get Data Form
        $value              = $this->input->post('value');
        $value              = bp_isset($value, '');

        if ($field == 'register_fee') {
            $value          = str_replace('.', '', $value);
        }

        // Update Setting
        $newvalue           = maybe_serialize($value);
        $data               = array('value' => $newvalue);
        $this->db->where('name', $field);

        // Get Data Field 
        if (!$result = $this->db->update(TBL_OPTIONS, $data)) {
            $data = array('status' => 'error', 'message' => 'Update Setting tidak berhasil. Terjadi kesalahan pada proses transaksi !');
            die(json_encode($data));
        }

        // Update Setting Success
        $data = array('status' => 'success', 'message' => 'Update Setting berhasil.');
        die(json_encode($data));
    }

    /**
     * Update All Setting General function.
     */
    function updateallsetting($field = '')
    {
        // Check for AJAX Request
        if (!$this->input->is_ajax_request()) {
            redirect(base_url('setting/general'), 'location');
        }
        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            $data = array('status' => 'login', 'url' => base_url('login'));
            die(json_encode($data)); // JSON encode data
        }

        // Get Data Field 
        if (!$field = $this->input->post('field')) {
            $data = array('status' => 'error', 'message' => 'Update Setting tidak berhasil. Data Setting tidak ditemukan !');
            die(json_encode($data));
        }

        foreach ($field as $key => $value) {
            // Update Data Setting
            $newvalue   = maybe_serialize($value);
            $data       = array('value' => $newvalue);
            if (!$update_data = $this->db->where('name', $key)->update(TBL_OPTIONS, $data)) {
                $data = array('status' => 'error', 'message' => 'Update Setting tidak berhasil. Terjadi kesalahan pada proses transaksi !');
                die(json_encode($data));
            }
        }

        // Update Setting Success
        $data = array('status' => 'success', 'message' => 'Update Setting berhasil.');
        die(json_encode($data));
    }


    /**
     * Update Data Company function.
     */
    function updatecompany()
    {
        // Check for AJAX Request
        if (!$this->input->is_ajax_request()) {
            redirect(base_url('setting/general'), 'location');
        }
        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            $data = array('status' => 'access_denied', 'url' => base_url('login'));
            die(json_encode($data)); // JSON encode data
        }

        // POST Input Form
        $company_name       = $this->input->post('company_name');
        $company_name       = bp_isset($company_name, '');
        $company_phone      = $this->input->post('company_phone');
        $company_phone      = bp_isset($company_phone, '');
        $company_email      = $this->input->post('company_email');
        $company_email      = bp_isset($company_email, '');
        $company_province   = $this->input->post('company_province');
        $company_province   = bp_isset($company_province, '');
        $company_city       = $this->input->post('company_city');
        $company_city       = bp_isset($company_city, '');
        $company_address   = $this->input->post('company_address');
        $company_address   = bp_isset($company_address, '');

        $this->form_validation->set_rules('company_name', 'Nama Perusahaan', 'required');
        $this->form_validation->set_rules('company_phone', 'No. Telp Perusahaan', 'required');
        $this->form_validation->set_rules('company_email', 'Email Perusahaan', 'required');
        $this->form_validation->set_rules('company_province', 'Provinsi', 'required');
        $this->form_validation->set_rules('company_city', 'Kota/Kabupaten', 'required');
        $this->form_validation->set_rules('company_address', 'Alamat Perusahaan', 'required');
        $this->form_validation->set_message('required', '%s harus di isi');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() == FALSE) {
            $data = array('status' => 'error', 'message' => validation_errors());
            die(json_encode($data));
        }

        // Update Data Company Name
        if (!$update_data = $this->db->where('name', 'company_name')->update(TBL_OPTIONS, array('value' => $company_name))) {
            $data = array('status' => 'error', 'message' => 'Nama Perusahaan tidak berhasil di ubah. ');
            die(json_encode($data));
        }

        if (substr($company_phone, 0, 1) != '0') {
            $company_phone  = '0' . $company_phone;
        }

        // Update Data Company Phone
        if (!$update_data = $this->db->where('name', 'company_phone')->update(TBL_OPTIONS, array('value' => $company_phone))) {
            $data = array('status' => 'error', 'message' => 'No. Telp Perusahaan tidak berhasil di ubah. ');
            die(json_encode($data));
        }

        // Update Data Company Phone
        if (!$update_data = $this->db->where('name', 'company_email')->update(TBL_OPTIONS, array('value' => $company_email))) {
            $data = array('status' => 'error', 'message' => 'Email Perusahaan tidak berhasil di ubah. ');
            die(json_encode($data));
        }

        // Update Data Province
        if (!$update_data = $this->db->where('name', 'company_province')->update(TBL_OPTIONS, array('value' => $company_province))) {
            $data = array('status' => 'error', 'message' => 'Provinsi tidak berhasil di ubah. ');
            die(json_encode($data));
        }

        // Update Data City
        if (!$update_data = $this->db->where('name', 'company_city')->update(TBL_OPTIONS, array('value' => $company_city))) {
            $data = array('status' => 'error', 'message' => 'Kota/Kabupaten tidak berhasil di ubah. ');
            die(json_encode($data));
        }

        // Update Data Address
        if (!$update_data = $this->db->where('name', 'company_address')->update(TBL_OPTIONS, array('value' => $company_address))) {
            $data = array('status' => 'error', 'message' => 'Alamat Perusahaan tidak berhasil di ubah. ');
            die(json_encode($data));
        }

        // Update Success
        $data = array('status' => 'success', 'message' => 'Informasi Perusahaan berhasil d ubah.');
        die(json_encode($data));
    }

    /**
     * Update Data Company Billing function.
     */
    function updatecompanybilling()
    {
        // Check for AJAX Request
        if (!$this->input->is_ajax_request()) {
            redirect(base_url('setting/general'), 'location');
        }
        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            $data = array('status' => 'access_denied', 'url' => base_url('login'));
            die(json_encode($data)); // JSON encode data
        }

        // POST Input Form
        $company_bank       = $this->input->post('company_bank');
        $company_bank       = bp_isset($company_bank, '');
        $company_bill       = $this->input->post('company_bill');
        $company_bill       = bp_isset($company_bill, '');
        $company_bill_name  = $this->input->post('company_bill_name');
        $company_bill_name  = bp_isset($company_bill_name, '');

        $this->form_validation->set_rules('company_bank', 'Bank Perusahaan', 'required');
        $this->form_validation->set_rules('company_bill', 'Nomor Rekening Perusahaan', 'required');
        $this->form_validation->set_rules('company_bill_name', 'Nama Pemilik Rekening', 'required');
        $this->form_validation->set_message('required', '%s harus di isi');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() == FALSE) {
            $data = array('status' => 'error', 'message' => validation_errors());
            die(json_encode($data));
        }

        // Update Data Company Bank
        if (!$update_data = $this->db->where('name', 'company_bank')->update(TBL_OPTIONS, array('value' => $company_bank))) {
            $data = array('status' => 'error', 'message' => 'Bank Perusahaan tidak berhasil di ubah. ');
            die(json_encode($data));
        }

        // Update Data Company Bill
        if (!$update_data = $this->db->where('name', 'company_bill')->update(TBL_OPTIONS, array('value' => $company_bill))) {
            $data = array('status' => 'error', 'message' => 'No. Rekening Perusahaan tidak berhasil di ubah. ');
            die(json_encode($data));
        }

        // Update Data Company Bill Name
        if (!$update_data = $this->db->where('name', 'company_bill_name')->update(TBL_OPTIONS, array('value' => $company_bill_name))) {
            $data = array('status' => 'error', 'message' => 'Nama Pemilik Rekening Perusahaan tidak berhasil di ubah. ');
            die(json_encode($data));
        }

        // Update Success
        $data = array('status' => 'success', 'message' => 'Informasi Bank Perusahaan berhasil d ubah.');
        die(json_encode($data));
    }

    /**
     * Update Data Notification function.
     */
    function updatenotification()
    {
        // Check for AJAX Request
        if (!$this->input->is_ajax_request()) {
            redirect(base_url('setting/notification'), 'location');
        }
        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            $data = array('status' => 'login', 'url' => base_url('login'));
            die(json_encode($data)); // JSON encode data
        }

        // POST Input Form
        $notif_id       = $this->input->post('notif_id');
        $notif_id       = bp_isset($notif_id, '');
        $notif_type     = $this->input->post('notif_type');
        $notif_type     = bp_isset($notif_type, '');
        $notif_title    = $this->input->post('notif_title');
        $notif_title    = bp_isset($notif_title, '');
        $notif_status   = $this->input->post('notif_status');
        $notif_status   = bp_isset($notif_status, '');
        $content_email  = $this->input->post('content_email');
        $content_email  = bp_isset($content_email, '', '', false, false);
        $content_plain  = $this->input->post('content_plain');
        $content_plain  = bp_isset($content_plain, '', '', false, false);

        // Get Data Notification 
        if (!$notification = $this->Model_Option->get_notification_by('id', $notif_id)) {
            $data = array('status' => 'error', 'message' => 'Update Notifikasi tidak berhasil. Data Notification tidak ditemukan !');
            die(json_encode($data));
        }

        $content        = (strtolower($notif_type) == 'email') ? $content_email : $content_plain;

        // Set and Update Data Notification
        $data_notif     = array('title' => $notif_title, 'content' => $content, 'status' => $notif_status);
        if (!$update_notif = $this->Model_Option->update_data_notification($notification->id, $data_notif)) {
            $data = array('status' => 'error', 'message' => 'Update Notifikasi tidak berhasil. Terjasi kesalahan pada proses transaksi.');
            die(json_encode($data));
        }

        // Update Success
        $data = array('status' => 'success', 'message' => 'Update Notifikasi berhasil.');
        die(json_encode($data));
    }

    /**
     * Save Promo Code function.
     */
    function savepromocode($id = '')
    {
        // Check for AJAX Request
        if (!$this->input->is_ajax_request()) {
            redirect(base_url('promocode/global'), 'location');
        }
        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            // Set JSON data
            $data = array('status' => 'access_denied', 'url' => base_url('login'));
            die(json_encode($data));
        }

        $current_member     = bp_get_current_member();
        $datetime           = date('Y-m-d H:i:s');
        $created_by         = $current_member->username;
        if ($staff = bp_get_current_staff()) {
            $created_by     = $staff->username;
        }

        $promo_id           = '';
        if ($id) {
            $id = bp_decrypt($id);
            if (!$data_promo = $this->Model_Option->get_promo_codes($id)) {
                $data = array('status' => 'error', 'message' => 'Data Kode Promo tidak berhasil disimpan. ID Kode Promo tidak ditemukan !');
                die(json_encode($data));
            }
            $promo_id       = $data_promo->id;
        }

        // POST Input Form
        $promo_code             = trim($this->input->post('promo_code'));
        $promo_code             = bp_isset($promo_code, '');
        $discount_agent_type    = trim($this->input->post('discount_agent_type'));
        $discount_agent_type    = bp_isset($discount_agent_type, '');
        $discount_agent         = trim($this->input->post('discount_agent'));
        $discount_agent         = bp_isset($discount_agent, '');
        $discount_customer_type = trim($this->input->post('discount_customer_type'));
        $discount_customer_type = bp_isset($discount_customer_type, '');
        $discount_customer      = trim($this->input->post('discount_customer'));
        $discount_customer      = bp_isset($discount_customer, '');
        $form_input             = trim($this->input->post('form_input'));
        $form_input             = bp_isset($form_input, 'global');
        $product_ids            = '';

        if (!$promo_code) {
            $data = array('status' => 'error', 'message' => 'Kode Promo harus di isi !');
            die(json_encode($data));
        }

        $discount_agent         = str_replace('.', '', $discount_agent);
        $discount_customer      = str_replace('.', '', $discount_customer);

        if (!$discount_agent && !$discount_customer) {
            $data = array('status' => 'error', 'message' => 'Salah atu diskon (Diskon Agen atau Diskon Konsumen) harus di isi !');
            die(json_encode($data));
        }

        if ($form_input == 'products') {
            if (!$products = $this->input->post('products')) {
                $data = array('status' => 'error', 'message' => 'Produk belum di pilih !');
                die(json_encode($data));
            }
            foreach ($products as $key => $value) {
                $product_ids[] = $value;
            }
        }

        $user_type              = 'all';
        if ($discount_agent && !$discount_customer) {
            $user_type          = 'agent';
        }
        if (!$discount_agent && $discount_customer) {
            $user_type          = 'customer';
        }

        $data = array(
            'promo_code'            => strtoupper($promo_code),
            'discount_agent_type'   => $discount_agent_type,
            'discount_agent'        => $discount_agent,
            'discount_customer_type' => $discount_customer_type,
            'discount_customer'     => $discount_customer,
            'usertype'              => $user_type,
            'datecreated'           => $datetime,
            'datemodified'          => $datetime,
        );

        if ($form_input == 'products' && $product_ids) {
            $data['products']       = json_encode($product_ids);
        }

        if ($id) {
            unset($data['datecreated']);
            $data['modified_by'] = $created_by;
            if (!$update_data = $this->Model_Option->update_data_promo_code($id, $data)) {
                $data = array('status' => 'error', 'message' => 'Data Kode Promo tidak berhasil disimpan. Silahkan cek form Kode Promo !');
                die(json_encode($data));
            }
        } else {
            $data['status']     = 1;
            $data['created_by'] = $created_by;
            if (!$saved_data = $this->Model_Option->save_data_promo_code($data)) {
                $data = array('status' => 'error', 'message' => 'Data Kode Promo tidak berhasil disimpan. Silahkan cek form Kode Promo !');
                die(json_encode($data));
            }
            $id = $saved_data;
        }

        $data = array('status' => 'success', 'message' => 'Data Kode Promo berhasil disimpan.');
        die(json_encode($data));
    }

    /**
     * Save Stages Function
     */
    function savestages($id = 0)
    {
        if (!$this->input->is_ajax_request()) {
            redirect(base_url('setting/reward'), 'refresh');
        }
        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            $data = array('status' => 'login', 'url' => base_url('login'));
            die(json_encode($data)); // JSON encode data
        }

        // set variables
        $current_member     = bp_get_current_member();
        $is_admin           = as_administrator($current_member);
        $datetime           = date('Y-m-d H:i:s');

        // POST Input Form
        $year               = trim($this->input->post('year'));
        $year               = bp_isset($year, '');
        $stage              = trim($this->input->post('stages'));
        $stage              = bp_isset($stage, '');
        $sub_stage          = trim($this->input->post('sub_stages'));
        $sub_stage          = bp_isset($sub_stage, '');
        $start_date         = trim($this->input->post('start_date'));
        $start_date         = bp_isset($start_date, '');
        $end_date           = trim($this->input->post('end_date'));
        $end_date           = bp_isset($end_date, '');

        $this->form_validation->set_rules('year', lang('year'), 'required');
        $this->form_validation->set_rules('stages', lang('step'), 'required');
        $this->form_validation->set_rules('sub_stages', lang('sub_step'), 'required');
        $this->form_validation->set_rules('start_date', lang('start_date'), 'required');
        $this->form_validation->set_rules('end_date', lang('start_date'), 'required');
        $this->form_validation->set_message('required', '%s harus di isi');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() == FALSE) {
            $data = array('status' => 'error', 'message' => 'Setting Tahap tidak berhasil disimpan. ' . validation_errors());
            die(json_encode($data));
        } else {
            $sort_stage         = 1;
            if ( $stage == 'rkpd_change' ) {
                $sort_stage     = 2;
            }
            if ( $stage == 'kua_ppas' ) {
                $sort_stage     = 3;
            }

            $cfg_stages         = config_item('stages');
            $stage_name         = isset($cfg_stages[$stage]) ? lang($cfg_stages[$stage]) : $stage;

            $cfg_sub_stages     = config_item('sub_stages');
            $sort_sub_stage     = isset($cfg_sub_stages[$stage][$sub_stage]['no']) ? $cfg_sub_stages[$stage][$sub_stage]['no'] : 0;
            $sub_stage_name     = isset($cfg_sub_stages[$stage][$sub_stage]['menu']) ? lang($cfg_sub_stages[$stage][$sub_stage]['menu']) : '';

            $data = array(
                'year'              => $year,
                'stage'             => $stage,
                'sub_stage'         => $sub_stage,
                'start_date'        => $start_date .' 00:00:00',
                'end_date'          => $end_date .' 23:59:59',
                'sort_stage'        => $sort_stage,
                'sort_sub_stage'    => $sort_sub_stage,
                'datecreated'       => $datetime,
                'datemodified'      => $datetime,
            );

            if ($id) {
                $id = bp_decrypt($id);
                unset($data['datecreated']);
                if (!$stagedata = $this->Model_Option->get_stages_by('id', $id)) {
                    $data = array('status' => 'error', 'message' => 'Setting Tahap tidak berhasil disimpan. Silahkan cek form Tahap !');
                    die(json_encode($data));
                }
                if (!$update_data = $this->Model_Option->update_data_stages($id, $data)) {
                    $data = array('status' => 'error', 'message' => 'Setting Tahap tidak berhasil disimpan. Silahkan cek form Tahap !');
                    die(json_encode($data));
                }
            } else {
                $condition = array('stage' => $stage, 'sub_stage' => $sub_stage);
                if ( $stagedata = $this->Model_Option->get_stages_by('year', $year, $condition) ) {
                    $data = array('status' => 'error', 'message' => 'Setting Tahap tidak berhasil disimpan. Data Tahap '. $stage_name .' '. $sub_stage_name .' Tahun '. $year .' sudah tersedia !');
                    die(json_encode($data));
                }

                if (!$saved_data = $this->Model_Option->save_data_stages($data)) {
                    $data = array('status' => 'error', 'message' => 'Setting Tahap tidak berhasil disimpan. Silahkan cek form Tahap !');
                    die(json_encode($data));
                }
            }

            // Save Success
            $this->session->set_userdata('alert_msg', 'Tahap berhasil disimpan.');
            $data = array('status' => 'success', 'message' => 'Setting Tahap berhasil disimpan.', 'url' => base_url('setting/stages'));
            die(json_encode($data));
        }
    }

    /**
     * Status Stage Function
     */
    function stagestatus($id = 0)
    {
        if (!$this->input->is_ajax_request()) {
            redirect(base_url('productmanage/categorylist'), 'refresh');
        }
        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            $data = array('status' => 'access_denied', 'url' => base_url('login'));
            die(json_encode($data)); // JSON encode data
        }

        $bp_token   = $this->security->get_csrf_hash();
        $return     = array('status' => 'error', 'token' => $bp_token, 'message' => 'Data Tahap tidak ditemukan !');
        if (!$id) {
            die(json_encode($return));
        }

        $id = bp_decrypt($id);
        if (!$stagesdata = $this->Model_Option->get_stages_by('id', $id)) {
            die(json_encode($return));
        }

        // set variables
        $current_member     = bp_get_current_member();
        $is_admin           = as_administrator($current_member);
        $datetime           = date('Y-m-d H:i:s');

        // POST Input Form
        $status             = trim($this->input->post('status'));
        $status             = bp_isset($status, '');

        $stage_status       = 0;
        if ( $status == 'active' )  { $stage_status = 1; }
        if ( $status == 'done' )    { $stage_status = 2; }

        $data = array(
            'status'        => $stage_status,
            'datemodified'  => $datetime,
        );

        if (!$update_data = $this->Model_Option->update_data_stages($id, $data)) {
            $return['message'] = 'Status Tahap tidak berhasil diedit !';
            die(json_encode($return));
        }

        // Save Success
        $return['status'] = 'success';
        $return['message'] = 'Status Tahap berhasil diedit.';
        die(json_encode($return));
    }

    /**
     * Delete Stage Function
     */
    function stagedelete($id = 0)
    {
        if (!$this->input->is_ajax_request()) {
            redirect(base_url('setting/stages'), 'refresh');
        }

        if (!$id) {
            $data = array('status' => 'error', 'message' => 'ID Tahap tidak ditemukan !');
            die(json_encode($data));
        }

        $id         = bp_decrypt($id);
        $stagedata  = $this->Model_Option->get_stages_by('id', $id);
        if (!$stagedata) {
            $data = array('status' => 'error', 'message' => 'Data Tahap tidak ditemukan !');
            die(json_encode($data));
        }

        if (!$delete_data = $this->Model_Option->delete_data_stages($id)) {
            $data = array('status' => 'error', 'message' => 'Tahap tidak berhasil dihapus !');
            die(json_encode($data));
        }

        // Save Success
        $data = array('status' => 'success', 'message' => 'Tahap berhasil dihapus.');
        die(json_encode($data));
    }

    /**
     * Check Promo Code function.
     */
    function checkpromocode()
    {
        // Check for AJAX Request
        if (!$this->input->is_ajax_request()) {
            redirect(base_url('promocode/global'), 'location');
        }

        $code       = $this->input->post('code');
        $code       = trim(bp_isset($code, ''));
        $promo_code = $this->input->post('promo_code');
        $promo_code = trim(bp_isset($promo_code, ''));
        $bp_token  = $this->security->get_csrf_hash();

        if (!empty($promo_code)) {
            $promodata = $this->Model_Option->get_promo_code_by('promo_code', $promo_code);
            if ($promodata) {
                if ($code) {
                    $code = bp_encrypt($code, 'decrypt');
                    if ($code != $promodata->id) {
                        die(json_encode(array('status' => false, 'token' => $bp_token)));
                    }
                } else {
                    die(json_encode(array('status' => false, 'token' => $bp_token)));
                }
            }
        }
        die(json_encode(array('status' => true, 'token' => $bp_token)));
    }
}

/* End of file Setting.php */
/* Location: ./application/controllers/Setting.php */
