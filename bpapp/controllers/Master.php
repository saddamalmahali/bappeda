<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Master Controller.
 *
 * @class     Master
 * @author    Yuda
 * @version   1.0.0
 */
class Master extends BP_Controller
{
    /**
     * Constructor.
     */
    function __construct()
    {
        parent::__construct();
    }

    // =============================================================================================
    // LIST DATA Master
    // =============================================================================================

    /**
     * Master urusan List Data function.
     */
    function urusanlistsdata()
    {
        // This is for AJAX request
        if (!$this->input->is_ajax_request()) exit('No direct script access allowed');

        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            // Set JSON data
            $data = array('status' => 'access_denied', 'data' => '');
            die(json_encode($data));
        }

        $current_member         = bp_get_current_member();
        $is_administrator       = as_administrator($current_member);
        $is_admin               = as_admin($current_member);
        $is_bidang              = as_bidang($current_member);
        $list_access            = ($is_administrator || $is_bidang) ? TRUE : FALSE;
        $crud_access            = $is_administrator ? TRUE : FALSE;
        $access                 = '';
        $roles                  = '';

        if ($is_admin) {
            $access             = $current_member->access;
            $roles              = maybe_unserialize($current_member->roles);
        }

        if ($staff = bp_get_current_staff()) {
            $access             = $staff->access;
            $roles              = maybe_unserialize($staff->role);
        }

        if ($access == 'partial') {
            $crud_access        = FALSE;
            if ($roles && is_array($roles)) {
                if (in_array(STAFF_ACCESS2, $roles)) {
                    $crud_access = TRUE;
                }
            }
        }

        $params             = array();
        $condition          = '';
        $order_by           = '';
        $iTotalRecords      = 0;

        $sExport            = $this->input->get('export');
        $sAction            = bp_isset($_REQUEST['sAction'], '');
        $sAction            = bp_isset($sExport, $sAction);

        $search_method      = 'post';
        if ($sAction == 'download_excel') {
            $search_method  = 'get';
        }

        $iDisplayLength     = intval($_REQUEST['iDisplayLength']);
        $iDisplayStart      = intval($_REQUEST['iDisplayStart']);
        $sEcho              = intval($_REQUEST['sEcho']);
        $sort               = $_REQUEST['sSortDir_0'];
        $column             = intval($_REQUEST['iSortCol_0']);

        $limit              = ($iDisplayLength == '-1' ? 0 : $iDisplayLength);
        $offset             = $iDisplayStart;

        $s_code             = $this->input->$search_method('search_code');
        $s_code             = bp_isset($s_code, '', '', true);
        $s_name             = $this->input->$search_method('search_name');
        $s_name             = bp_isset($s_name, '', '', true);

        if (!empty($s_code)) {
            $condition .= ' AND %code% LIKE CONCAT("%", ?, "%")';
            $params[] = $s_code;
        }
        if (!empty($s_name)) {
            $condition .= ' AND %name% LIKE CONCAT("%", ?, "%")';
            $params[] = $s_name;
        }

        if ($column == 1) {
            $order_by .= '%code% ' . $sort;
        } elseif ($column == 2) {
            $order_by .= '%name% ' . $sort;
        }

        $data_list          = ($list_access) ? $this->Model_Master->get_all_urusan_data($limit, $offset, $condition, $order_by, $params) : '';
        $records            = array();
        $records["aaData"]  = array();

        if (!empty($data_list)) {
            $iTotalRecords  = bp_get_last_found_rows();
            $i = $offset + 1;
            foreach ($data_list as $row) {
                $id             = bp_encrypt($row->id);
                $code           = bp_strong(strtoupper($row->kode));
                $code           = '<a href="javascript:;">' . $code . '</a>';
                $name           = bp_strong(strtoupper($row->nama));

                $btn_edit       = '<a href="javascript:;" data-url="' . base_url('master/saveurusan/' . $id) . '" class="btn btn-sm btn-outline-primary btn-tooltip" title="Edit Urusan" data-code="' . $row->kode . '" data-name="' . $row->nama . '" id="btn-update-urusan"><i class="fa fa-edit"></i></a>';
                $btn_delete     = '<a href="javascript:;" data-url="' . base_url('master/deleteurusan/' . $id) . '" data-code="' . $row->kode . '" data-name="' . $row->nama . '" class="btn btn-sm btn-outline-danger btn-tooltip" title="Hapus Urusan" id="btn-delete-urusan"><i class="fa fa-times"></i></a>';

                $records["aaData"][] = array(
                    bp_center($i),
                    bp_center($code),
                    $name,
                    bp_center((($is_administrator && $crud_access) ? $btn_edit . $btn_delete : ''))
                );
                $i++;
            }
        }

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $records["sEcho"]                   = $sEcho;
        $records["iTotalRecords"]           = $iTotalRecords;
        $records["iTotalDisplayRecords"]    = $iTotalRecords;

        echo json_encode($records);
    }

    /**
     * Master suburusan List Data function.
     */
    function suburusanlistsdata()
    {
        // This is for AJAX request
        if (!$this->input->is_ajax_request()) exit('No direct script access allowed');

        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            // Set JSON data
            $data = array('status' => 'access_denied', 'data' => '');
            die(json_encode($data));
        }

        $current_member         = bp_get_current_member();
        $is_administrator       = as_administrator($current_member);
        $is_admin               = as_admin($current_member);
        $is_bidang              = as_bidang($current_member);
        $list_access            = ($is_administrator || $is_bidang) ? TRUE : FALSE;
        $crud_access            = $is_administrator ? TRUE : FALSE;
        $access                 = '';
        $roles                  = '';

        if ($is_admin) {
            $access             = $current_member->access;
            $roles              = maybe_unserialize($current_member->roles);
        }

        if ($staff = bp_get_current_staff()) {
            $access             = $staff->access;
            $roles              = maybe_unserialize($staff->role);
        }

        if ($access == 'partial') {
            $crud_access        = FALSE;
            if ($roles && is_array($roles)) {
                if (in_array(STAFF_ACCESS2, $roles)) {
                    $crud_access = TRUE;
                }
            }
        }

        $params             = array();
        $condition          = '';
        $order_by           = '';
        $iTotalRecords      = 0;

        $sExport            = $this->input->get('export');
        $sAction            = bp_isset($_REQUEST['sAction'], '');
        $sAction            = bp_isset($sExport, $sAction);

        $search_method      = 'post';
        if ($sAction == 'download_excel') {
            $search_method  = 'get';
        }

        $iDisplayLength     = intval($_REQUEST['iDisplayLength']);
        $iDisplayStart      = intval($_REQUEST['iDisplayStart']);
        $sEcho              = intval($_REQUEST['sEcho']);
        $sort               = $_REQUEST['sSortDir_0'];
        $column             = intval($_REQUEST['iSortCol_0']);

        $limit              = ($iDisplayLength == '-1' ? 0 : $iDisplayLength);
        $offset             = $iDisplayStart;

        $s_code             = $this->input->$search_method('search_code');
        $s_code             = bp_isset($s_code, '', '', true);
        $s_name             = $this->input->$search_method('search_name');
        $s_name             = bp_isset($s_name, '', '', true);
        $s_urusan_name      = $this->input->$search_method('search_urusan_name');
        $s_urusan_name      = bp_isset($s_urusan_name, '', '', true);

        if (!empty($s_code)) {
            $condition .= ' AND %code% LIKE CONCAT("%", ?, "%")';
            $params[] = $s_code;
        }
        if (!empty($s_name)) {
            $condition .= ' AND %name% LIKE CONCAT("%", ?, "%")';
            $params[] = $s_name;
        }
        if (!empty($s_urusan_name)) {
            $condition .= ' AND %urusan_name% LIKE CONCAT("%", ?, "%")';
            $params[] = $s_urusan_name;
        }

        if ($column == 1) {
            $order_by .= '%code% ' . $sort;
        } elseif ($column == 2) {
            $order_by .= '%name% ' . $sort;
        } elseif ($column == 3) {
            $order_by .= '%urusan_name% ' . $sort;
        }

        $data_list          = ($list_access) ? $this->Model_Master->get_all_suburusan_data($limit, $offset, $condition, $order_by, $params) : '';
        $records            = array();
        $records["aaData"]  = array();

        if (!empty($data_list)) {
            $iTotalRecords  = bp_get_last_found_rows();
            $i = $offset + 1;
            foreach ($data_list as $row) {
                $id             = bp_encrypt($row->id);
                $code           = bp_strong(strtoupper($row->kode));
                $code           = '<a href="javascript:;">' . $code . '</a>';
                $name           = bp_strong(strtoupper($row->nama));
                $urusan         = bp_strong(strtoupper($row->urusan_name));

                $btn_edit       = '<a href="javascript:;" data-url="' . base_url('master/savesuburusan/' . $id) . '" class="btn btn-sm btn-outline-primary btn-tooltip" title="Edit Sub Urusan" data-code="' . $row->kode . '" data-name="' . $row->nama . '" data-urusan="' . bp_encrypt($row->id_urusan) . '" data-urusan-name="' . $row->urusan_name . '" id="btn-update-sub-urusan"><i class="fa fa-edit"></i></a>';
                $btn_delete     = '<a href="javascript:;" data-url="' . base_url('master/deletesuburusan/' . $id) . '" class="btn btn-sm btn-outline-danger btn-tooltip" title="Hapus Sub Urusan" data-code="' . $row->kode . '" data-name="' . $row->nama . '" data-urusan="' . bp_encrypt($row->id_urusan) . '" data-urusan-name="' . $row->urusan_name . '" id="btn-delete-sub-urusan"><i class="fa fa-times"></i></a>';

                $records["aaData"][] = array(
                    bp_center($i),
                    bp_center($code),
                    '<div style="white-space:normal; min-width:250px;">' . $name . '</div>',
                    '<div style="white-space:normal; min-width:250px;">' . $urusan . '</div>',
                    bp_center((($is_administrator && $crud_access) ? $btn_edit . $btn_delete : ''))
                );
                $i++;
            }
        }

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $records["sEcho"]                   = $sEcho;
        $records["iTotalRecords"]           = $iTotalRecords;
        $records["iTotalDisplayRecords"]    = $iTotalRecords;

        echo json_encode($records);
    }

    /**
     * Master program List Data function.
     */
    function programlistsdata()
    {
        // This is for AJAX request
        if (!$this->input->is_ajax_request()) exit('No direct script access allowed');

        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            // Set JSON data
            $data = array('status' => 'access_denied', 'data' => '');
            die(json_encode($data));
        }

        $current_member         = bp_get_current_member();
        $is_administrator       = as_administrator($current_member);
        $is_admin               = as_admin($current_member);
        $is_bidang              = as_bidang($current_member);
        $list_access            = ($is_administrator || $is_bidang) ? TRUE : FALSE;
        $crud_access            = $is_administrator ? TRUE : FALSE;
        $access                 = '';
        $roles                  = '';

        if ($is_admin) {
            $access             = $current_member->access;
            $roles              = maybe_unserialize($current_member->roles);
        }

        if ($staff = bp_get_current_staff()) {
            $access             = $staff->access;
            $roles              = maybe_unserialize($staff->role);
        }

        if ($access == 'partial') {
            $crud_access        = FALSE;
            if ($roles && is_array($roles)) {
                if (in_array(STAFF_ACCESS2, $roles)) {
                    $crud_access = TRUE;
                }
            }
        }

        $params             = array();
        $condition          = '';
        $order_by           = '';
        $iTotalRecords      = 0;

        $sExport            = $this->input->get('export');
        $sAction            = bp_isset($_REQUEST['sAction'], '');
        $sAction            = bp_isset($sExport, $sAction);

        $search_method      = 'post';
        if ($sAction == 'download_excel') {
            $search_method  = 'get';
        }

        $iDisplayLength     = intval($_REQUEST['iDisplayLength']);
        $iDisplayStart      = intval($_REQUEST['iDisplayStart']);
        $sEcho              = intval($_REQUEST['sEcho']);
        $sort               = $_REQUEST['sSortDir_0'];
        $column             = intval($_REQUEST['iSortCol_0']);

        $limit              = ($iDisplayLength == '-1' ? 0 : $iDisplayLength);
        $offset             = $iDisplayStart;

        $s_code             = $this->input->$search_method('search_code');
        $s_code             = bp_isset($s_code, '', '', true);
        $s_name             = $this->input->$search_method('search_name');
        $s_name             = bp_isset($s_name, '', '', true);

        if (!empty($s_code)) {
            $condition .= ' AND %code% LIKE CONCAT("%", ?, "%")';
            $params[] = $s_code;
        }
        if (!empty($s_name)) {
            $condition .= ' AND %name% LIKE CONCAT("%", ?, "%")';
            $params[] = $s_name;
        }

        if ($column == 1) {
            $order_by .= '%code% ' . $sort;
        } elseif ($column == 2) {
            $order_by .= '%name% ' . $sort;
        }

        $data_list          = ($list_access) ? $this->Model_Master->get_all_program_data($limit, $offset, $condition, $order_by, $params) : '';
        $records            = array();
        $records["aaData"]  = array();

        if (!empty($data_list)) {
            $iTotalRecords  = bp_get_last_found_rows();
            $i = $offset + 1;
            foreach ($data_list as $row) {
                $id             = bp_encrypt($row->id);
                $code           = bp_strong(strtoupper($row->kode));
                $code           = '<a href="javascript:;" class="btn-tooltip" title="SUB URUSAN : ' . br() . ucwords(strtolower($row->suburusan_name)) . '">' . $code . '</a>';
                $name           = bp_strong(strtoupper($row->nama));

                $btn_edit       = '<a 
                                        href="javascript:;"
                                        data-url="' . base_url('master/saveprogram/' . $id) . '" 
                                        class="btn btn-sm btn-outline-primary btn-tooltip" 
                                        data-name="' . $row->nama . '" 
                                        data-code="' . $row->kode . '" 
                                        data-sub-urusan="' . bp_encrypt($row->id_sub_urusan) . '"
                                        data-sub-urusan-name="' . $row->suburusan_name . '"
                                        data-sub-urusan-code="' . $row->suburusan_code . '"
                                        id="btn-update-program" 
                                        title="Edit Program"><i class="fa fa-edit"></i></a>';
                $btn_delete     = '<a 
                                        href="javascript:;"
                                        data-url="' . base_url('master/deleteprogram/' . $id) . '" 
                                        class="btn btn-sm btn-outline-danger btn-tooltip" 
                                        data-name="' . $row->nama . '" 
                                        data-code="' . $row->kode . '" 
                                        data-sub-urusan="' . bp_encrypt($row->id_sub_urusan) . '"
                                        data-sub-urusan-name="' . $row->suburusan_name . '"
                                        data-sub-urusan-code="' . $row->suburusan_code . '"
                                        id="btn-delete-program"                                         
                                        title="Hapus Program"><i class="fa fa-times"></i></a>';

                $records["aaData"][] = array(
                    bp_center($i),
                    bp_center($code),
                    '<div style="white-space:normal;">' . $name . '</div>',
                    bp_center((($is_administrator && $crud_access) ? $btn_edit . $btn_delete : ''))
                );
                $i++;
            }
        }

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $records["sEcho"]                   = $sEcho;
        $records["iTotalRecords"]           = $iTotalRecords;
        $records["iTotalDisplayRecords"]    = $iTotalRecords;

        echo json_encode($records);
    }

    /**
     * Master kegiatan List Data function.
     */
    function kegiatanlistsdata()
    {
        // This is for AJAX request
        if (!$this->input->is_ajax_request()) exit('No direct script access allowed');

        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            // Set JSON data
            $data = array('status' => 'access_denied', 'data' => '');
            die(json_encode($data));
        }

        $current_member     = bp_get_current_member();
        $is_administrator   = as_administrator($current_member);
        $is_admin           = as_admin($current_member);
        $is_bidang          = as_bidang($current_member);
        $list_access        = ($is_administrator || $is_bidang) ? TRUE : FALSE;
        $crud_access        = $is_administrator ? TRUE : FALSE;
        $access             = '';
        $roles              = '';

        if ($is_admin) {
            $access         = $current_member->access;
            $roles          = maybe_unserialize($current_member->roles);
        }

        if ($staff = bp_get_current_staff()) {
            $access         = $staff->access;
            $roles          = maybe_unserialize($staff->role);
        }

        if ($access == 'partial') {
            $crud_access        = FALSE;
            if ($roles && is_array($roles)) {
                if (in_array(STAFF_ACCESS2, $roles)) {
                    $crud_access = TRUE;
                }
            }
        }

        $params             = array();
        $condition          = '';
        $order_by           = '';
        $iTotalRecords      = 0;

        $sExport            = $this->input->get('export');
        $sAction            = bp_isset($_REQUEST['sAction'], '');
        $sAction            = bp_isset($sExport, $sAction);

        $search_method      = 'post';
        if ($sAction == 'download_excel') {
            $search_method  = 'get';
        }

        $iDisplayLength     = intval($_REQUEST['iDisplayLength']);
        $iDisplayStart      = intval($_REQUEST['iDisplayStart']);
        $sEcho              = intval($_REQUEST['sEcho']);
        $sort               = $_REQUEST['sSortDir_0'];
        $column             = intval($_REQUEST['iSortCol_0']);

        $limit              = ($iDisplayLength == '-1' ? 0 : $iDisplayLength);
        $offset             = $iDisplayStart;

        $s_code             = $this->input->$search_method('search_code');
        $s_code             = bp_isset($s_code, '', '', true);
        $s_name             = $this->input->$search_method('search_name');
        $s_name             = bp_isset($s_name, '', '', true);

        if (!empty($s_code)) {
            $condition .= ' AND %code% LIKE CONCAT("%", ?, "%")';
            $params[] = $s_code;
        }
        if (!empty($s_name)) {
            $condition .= ' AND %name% LIKE CONCAT("%", ?, "%")';
            $params[] = $s_name;
        }

        if ($column == 1) {
            $order_by .= '%code% ' . $sort;
        } elseif ($column == 2) {
            $order_by .= '%name% ' . $sort;
        }

        $data_list          = ($list_access) ? $this->Model_Master->get_all_kegiatan_data($limit, $offset, $condition, $order_by, $params) : '';
        $records            = array();
        $records["aaData"]  = array();

        if (!empty($data_list)) {
            $iTotalRecords  = bp_get_last_found_rows();
            $access         = TRUE;
            if ($staff = bp_get_current_staff()) {
                if ($staff->access == 'partial') {
                    $role   = array();
                    if ($staff->role) {
                        $role = $staff->role;
                    }

                    foreach (array(STAFF_ACCESS2) as $val) {
                        if (empty($role) || !in_array($val, $role))
                            $access = FALSE;
                    }
                }
            }
            $i = $offset + 1;
            foreach ($data_list as $row) {
                $id             = bp_encrypt($row->id);
                $code           = bp_strong(strtoupper($row->kode));
                $code           = '<a href="javascript:;" class="btn-tooltip" title="PROGRAM : ' . br() . ucwords(strtolower($row->program_name)) . '">' . $code . '</a>';
                $name           = bp_strong(strtoupper($row->nama));

                $btn_edit       = '<a 
                                        href="javascript:;"
                                        data-url="' . base_url('master/savekegiatan/' . $id) . '" 
                                        class="btn btn-sm btn-outline-primary btn-tooltip" 
                                        data-name="' . $row->nama . '" 
                                        data-code="' . $row->kode . '" 
                                        data-program="' . bp_encrypt($row->id_program) . '"
                                        data-program-name="' . $row->program_name . '"
                                        data-program-code="' . $row->program_code . '"
                                        id="btn-update-kegiatan" 
                                        title="Edit Kegiatan"
                                        ><i class="fa fa-edit"></i></a>';
                $btn_delete     = '<a 
                                        href="javascript:;"
                                        data-url="' . base_url('master/deletekegiatan/' . $id) . '" 
                                        class="btn btn-sm btn-outline-danger btn-tooltip" 
                                        data-name="' . $row->nama . '" 
                                        data-code="' . $row->kode . '" 
                                        data-program="' . bp_encrypt($row->id_program) . '"
                                        data-program-name="' . $row->program_name . '"
                                        data-program-code="' . $row->program_code . '"
                                        id="btn-delete-kegiatan"
                                        title="Hapus Kegiatan"
                                        ><i class="fa fa-times"></i></a>';

                $records["aaData"][] = array(
                    bp_center($i),
                    bp_center($code),
                    '<div style="white-space:normal;">' . $name . '</div>',
                    bp_center((($is_administrator && $crud_access) ? $btn_edit . $btn_delete : ''))
                );
                $i++;
            }
        }

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $records["sEcho"]                   = $sEcho;
        $records["iTotalRecords"]           = $iTotalRecords;
        $records["iTotalDisplayRecords"]    = $iTotalRecords;

        echo json_encode($records);
    }

    /**
     * Master sub-kegiatan List Data function.
     */
    function subkegiatanlistsdata()
    {
        // This is for AJAX request
        if (!$this->input->is_ajax_request()) exit('No direct script access allowed');

        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            // Set JSON data
            $data = array('status' => 'access_denied', 'data' => '');
            die(json_encode($data));
        }

        $current_member     = bp_get_current_member();
        $is_administrator   = as_administrator($current_member);
        $is_admin           = as_admin($current_member);
        $is_bidang          = as_bidang($current_member);
        $list_access        = ($is_administrator || $is_bidang) ? TRUE : FALSE;
        $crud_access        = $is_administrator ? TRUE : FALSE;
        $access             = '';
        $roles              = '';

        if ($is_admin) {
            $access         = $current_member->access;
            $roles          = maybe_unserialize($current_member->roles);
        }

        if ($staff = bp_get_current_staff()) {
            $access         = $staff->access;
            $roles          = maybe_unserialize($staff->role);
        }

        if ($access == 'partial') {
            $crud_access        = FALSE;
            if ($roles && is_array($roles)) {
                if (in_array(STAFF_ACCESS2, $roles)) {
                    $crud_access = TRUE;
                }
            }
        }

        $params             = array();
        $condition          = '';
        $order_by           = '';
        $iTotalRecords      = 0;

        $sExport            = $this->input->get('export');
        $sAction            = bp_isset($_REQUEST['sAction'], '');
        $sAction            = bp_isset($sExport, $sAction);

        $search_method      = 'post';
        if ($sAction == 'download_excel') {
            $search_method  = 'get';
        }

        $iDisplayLength     = intval($_REQUEST['iDisplayLength']);
        $iDisplayStart      = intval($_REQUEST['iDisplayStart']);
        $sEcho              = intval($_REQUEST['sEcho']);
        $sort               = $_REQUEST['sSortDir_0'];
        $column             = intval($_REQUEST['iSortCol_0']);

        $limit              = ($iDisplayLength == '-1' ? 0 : $iDisplayLength);
        $offset             = $iDisplayStart;

        $s_code             = $this->input->$search_method('search_code');
        $s_code             = bp_isset($s_code, '', '', true);
        $s_name             = $this->input->$search_method('search_name');
        $s_name             = bp_isset($s_name, '', '', true);

        if (!empty($s_code)) {
            $condition .= ' AND %code% LIKE CONCAT("%", ?, "%")';
            $params[] = $s_code;
        }
        if (!empty($s_name)) {
            $condition .= ' AND %name% LIKE CONCAT("%", ?, "%")';
            $params[] = $s_name;
        }

        if ($column == 1) {
            $order_by .= '%code% ' . $sort;
        } elseif ($column == 2) {
            $order_by .= '%name% ' . $sort;
        }

        $data_list          = ($list_access) ? $this->Model_Master->get_all_subkegiatan_data($limit, $offset, $condition, $order_by, $params) : '';
        $records            = array();
        $records["aaData"]  = array();

        if (!empty($data_list)) {
            $iTotalRecords  = bp_get_last_found_rows();
            $i = $offset + 1;
            foreach ($data_list as $row) {
                $id             = bp_encrypt($row->id);
                $code           = bp_strong(strtoupper($row->kode));
                $code           = '<a href="javascript:;" class="btn-tooltip" title="KEGIATAN : ' . br() . ucwords(strtolower($row->kegiatan_name)) . '">' . $code . '</a>';
                $name           = bp_strong(strtoupper($row->nama));

                $btn_edit       = '<a 
                                        href="javascript:;"
                                        data-url="' . base_url('master/savesubkegiatan/' . $id) . '" 
                                        class="btn btn-sm btn-outline-primary btn-tooltip"
                                        data-name="' . $row->nama . '" 
                                        data-code="' . $row->kode . '" 
                                        data-kegiatan="' . bp_encrypt($row->id_kegiatan) . '"
                                        data-kegiatan-name="' . $row->kegiatan_name . '"
                                        data-kegiatan-code="' . $row->kegiatan_code . '"
                                        id="btn-update-sub-kegiatan" 
                                        title="Edit Sub Kegiatan"
                                        ><i class="fa fa-edit"></i></a>';
                $btn_delete     = '<a   
                                        href="javascript:;"
                                        data-url="' . base_url('master/deletesubkegiatan/' . $id) . '" 
                                        class="btn btn-sm btn-outline-danger btn-tooltip" 
                                        data-name="' . $row->nama . '" 
                                        data-code="' . $row->kode . '" 
                                        data-kegiatan="' . bp_encrypt($row->id_kegiatan) . '"
                                        data-kegiatan-name="' . $row->kegiatan_name . '"
                                        data-kegiatan-code="' . $row->kegiatan_code . '"
                                        id="btn-delete-sub-kegiatan" 
                                        title="Hapus Sub Kegiatan"
                                        ><i class="fa fa-times"></i></a>';

                $records["aaData"][] = array(
                    bp_center($i),
                    bp_center($code),
                    '<div style="white-space:normal;">' . $name . '</div>',
                    bp_center((($is_administrator && $crud_access) ? $btn_edit . $btn_delete : ''))
                );
                $i++;
            }
        }

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $records["sEcho"]                   = $sEcho;
        $records["iTotalRecords"]           = $iTotalRecords;
        $records["iTotalDisplayRecords"]    = $iTotalRecords;

        echo json_encode($records);
    }

    // =============================================================================================
    // LIST DATA Satuan Kerja
    // =============================================================================================

    /**
     * Bidang List Data function.
     */
    function bidanglistsdata()
    {
        // This is for AJAX request
        if (!$this->input->is_ajax_request()) exit('No direct script access allowed');

        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            // Set JSON data
            $data = array('status' => 'access_denied', 'data' => '');
            die(json_encode($data));
        }

        $current_member         = bp_get_current_member();
        $is_administrator       = as_administrator($current_member);
        $is_admin               = as_admin($current_member);
        $is_bidang              = as_bidang($current_member);
        $list_access            = ($is_administrator || $is_bidang) ? TRUE : FALSE;
        $crud_access            = $is_administrator ? TRUE : FALSE;
        $access                 = '';
        $roles                  = '';

        if ($is_admin) {
            $access             = $current_member->access;
            $roles              = maybe_unserialize($current_member->roles);
        }

        if ($staff = bp_get_current_staff()) {
            $access             = $staff->access;
            $roles              = maybe_unserialize($staff->role);
        }

        if ($access == 'partial') {
            $crud_access        = FALSE;
            if ($roles && is_array($roles)) {
                if (in_array(STAFF_ACCESS4, $roles)) {
                    $crud_access = TRUE;
                }
            }
        }

        $params             = array();
        $condition          = '';
        $order_by           = '';
        $iTotalRecords      = 0;

        $sExport            = $this->input->get('export');
        $sAction            = bp_isset($_REQUEST['sAction'], '');
        $sAction            = bp_isset($sExport, $sAction);

        $search_method      = 'post';
        if ($sAction == 'download_excel') {
            $search_method  = 'get';
        }

        $iDisplayLength     = intval($_REQUEST['iDisplayLength']);
        $iDisplayStart      = intval($_REQUEST['iDisplayStart']);
        $sEcho              = intval($_REQUEST['sEcho']);
        $sort               = $_REQUEST['sSortDir_0'];
        $column             = intval($_REQUEST['iSortCol_0']);

        $limit              = ($iDisplayLength == '-1' ? 0 : $iDisplayLength);
        $offset             = $iDisplayStart;

        $s_name             = $this->input->$search_method('search_name');
        $s_name             = bp_isset($s_name, '', '', true);
        $s_skpd             = $this->input->$search_method('search_skpd');
        $s_skpd             = bp_isset($s_skpd, '', '', true);

        if (!empty($s_name)) {
            $condition .= ' AND %name% LIKE CONCAT("%", ?, "%")';
            $params[] = $s_name;
        }
        if (strtolower($s_skpd) == 'ya') {
            $condition .= ' AND %all_skpd% > 0';
        }
        if (strtolower($s_skpd) == 'no') {
            $condition .= ' AND %all_skpd% = 0';
        }

        if ($column == 1) {
            $order_by .= '%name% ' . $sort;
        } elseif ($column == 2) {
            $order_by .= '%all_skpd% ' . $sort;
        }

        $data_list          = ($list_access) ? $this->Model_Master->get_all_bidang_data($limit, $offset, $condition, $order_by, $params) : '';
        $records            = array();
        $records["aaData"]  = array();

        if (!empty($data_list)) {
            $iTotalRecords  = bp_get_last_found_rows();
            $i = $offset + 1;
            foreach ($data_list as $row) {
                $id             = bp_encrypt($row->id);
                $name           = bp_strong(strtoupper($row->nama));
                $all_skpd       = '';
                if ($row->all_skpd > 0) {
                    $all_skpd   = '<strong class="text-primary"><i class="fa fa-check"></i></strong>';
                }

                $btn_edit       = '<a href="javascript:;" data-url="' . base_url('master/savebidang/' . $id) . '" class="btn btn-sm btn-outline-primary btn-tooltip" title="Edit Bidang" data-name="' . $row->nama . '" data-skpd="' . $row->all_skpd . '" id="btn-update-bidang"><i class="fa fa-edit"></i></a>';
                $btn_delete     = '<a href="javascript:;" data-url="' . base_url('master/deletebidang/' . $id) . '" data-name="' . $row->nama . '" data-skpd="' . $row->all_skpd . '" class="btn btn-sm btn-outline-danger btn-tooltip" title="Hapus Bidang" id="btn-delete-bidang"><i class="fa fa-times"></i></a>';

                $records["aaData"][] = array(
                    bp_center($i),
                    $name,
                    bp_center($all_skpd),
                    bp_center((($is_administrator && $crud_access) ? $btn_edit . $btn_delete : ''))
                );
                $i++;
            }
        }

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $records["sEcho"]                   = $sEcho;
        $records["iTotalRecords"]           = $iTotalRecords;
        $records["iTotalDisplayRecords"]    = $iTotalRecords;

        echo json_encode($records);
    }

    /**
     * SKPD List Data function.
     */
    function skpdlistsdata()
    {
        // This is for AJAX request
        if (!$this->input->is_ajax_request()) exit('No direct script access allowed');

        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            // Set JSON data
            $data = array('status' => 'access_denied', 'data' => '');
            die(json_encode($data));
        }

        $current_member         = bp_get_current_member();
        $is_administrator       = as_administrator($current_member);
        $is_admin               = as_admin($current_member);
        $is_bidang              = as_bidang($current_member);
        $list_access            = ($is_administrator || $is_bidang) ? TRUE : FALSE;
        $crud_access            = $is_administrator ? TRUE : FALSE;
        $access                 = '';
        $roles                  = '';

        if ($is_admin) {
            $access             = $current_member->access;
            $roles              = maybe_unserialize($current_member->roles);
        }

        if ($staff = bp_get_current_staff()) {
            $access             = $staff->access;
            $roles              = maybe_unserialize($staff->role);
        }

        if ($access == 'partial') {
            $crud_access        = FALSE;
            if ($roles && is_array($roles)) {
                if (in_array(STAFF_ACCESS4, $roles)) {
                    $crud_access = TRUE;
                }
            }
        }

        $params             = array();
        $condition          = '';
        $order_by           = '';
        $iTotalRecords      = 0;

        $sExport            = $this->input->get('export');
        $sAction            = bp_isset($_REQUEST['sAction'], '');
        $sAction            = bp_isset($sExport, $sAction);

        $search_method      = 'post';
        if ($sAction == 'download_excel') {
            $search_method  = 'get';
        }

        $iDisplayLength     = intval($_REQUEST['iDisplayLength']);
        $iDisplayStart      = intval($_REQUEST['iDisplayStart']);
        $sEcho              = intval($_REQUEST['sEcho']);
        $sort               = $_REQUEST['sSortDir_0'];
        $column             = intval($_REQUEST['iSortCol_0']);

        $limit              = ($iDisplayLength == '-1' ? 0 : $iDisplayLength);
        $offset             = $iDisplayStart;

        $s_code             = $this->input->$search_method('search_code');
        $s_code             = bp_isset($s_code, '', '', true);
        $s_name             = $this->input->$search_method('search_name');
        $s_name             = bp_isset($s_name, '', '', true);
        $s_u1               = $this->input->$search_method('search_u1');
        $s_u1               = bp_isset($s_u1, '', '', true);
        $s_u2               = $this->input->$search_method('search_u2');
        $s_u2               = bp_isset($s_u2, '', '', true);
        $s_u3               = $this->input->$search_method('search_u3');
        $s_u3               = bp_isset($s_u3, '', '', true);
        $s_urut_min         = $this->input->$search_method('search_urut_min');
        $s_urut_min         = bp_isset($s_urut_min);
        $s_urut_max         = $this->input->$search_method('search_urut_max');
        $s_urut_max         = bp_isset($s_urut_max);
        $s_bidang           = $this->input->$search_method('search_bidang');
        $s_bidang           = bp_isset($s_bidang, '', '', true);
        $s_type             = $this->input->$search_method('search_type');
        $s_type             = bp_isset($s_type, '', '', true);

        if (!empty($s_code)) {
            $condition      .= ' AND %code% LIKE ?';
            $params[]       = $s_code;
        }
        if (!empty($s_name)) {
            $condition .= ' AND %name% LIKE CONCAT("%", ?, "%")';
            $params[] = $s_name;
        }
        if (!empty($s_u1)) {
            $condition      .= ' AND %u1% = ? ';
            $params[]       = $s_u1;
        }
        if (!empty($s_u2)) {
            $condition      .= ' AND %u2% = ? ';
            $params[]       = $s_u2;
        }
        if (!empty($s_u3)) {
            $condition      .= ' AND %u3% = ? ';
            $params[]       = $s_u3;
        }
        if (!empty($s_urut_min)) {
            $condition      .= ' AND CONVERT(%urut%,UNSIGNED INTEGER) >= ? ';
            $params[]       = absint($s_urut_min);
        }
        if (!empty($s_urut_max)) {
            $condition      .= ' AND CONVERT(%urut%,UNSIGNED INTEGER) <= ? ';
            $params[]       = absint($s_urut_max);
        }

        if (!empty($s_bidang)) {
            $condition      .= ' AND %bidang% = ?';
            $params[]       = $s_bidang;
        }

        if (!empty($s_type)) {
            $condition      .= ' AND %type% = ?';
            $params[]       = $s_type;
        }

        if ($column == 1) {
            $order_by .= '%code% ' . $sort;
        } else if ($column == 2) {
            $order_by .= '%name% ' . $sort;
        } else if ($column == 3) {
            $order_by .= '%type% ' . $sort;
        } else if ($column == 4) {
            $order_by .= '%bidang% ' . $sort;
        } else if ($column == 5) {
            $order_by .= '%u1% ' . $sort;
        } else if ($column == 6) {
            $order_by .= '%u2% ' . $sort;
        } else if ($column == 7) {
            $order_by .= '%u3% ' . $sort;
        } else if ($column == 8) {
            $order_by .= '%urut% ' . $sort;
        }


        $data_list          = ($list_access) ? $this->Model_Master->get_all_skpd_data($limit, $offset, $condition, $order_by, $params) : '';
        $records            = array();
        $records["aaData"]  = array();

        if (!empty($data_list)) {
            $iTotalRecords  = bp_get_last_found_rows();
            $i = $offset + 1;
            foreach ($data_list as $row) {
                $id             = bp_encrypt($row->id);
                $name           = bp_strong(strtoupper($row->nama));
                $code           = bp_strong(strtoupper($row->kode));



                // Urusan 1, 2 & 3, dan bidang
                // ----------------------------------------
                $suburusan1         = '';
                $dataurusan1        = '';
                $suburusan2         = '';
                $dataurusan2        = '';
                $suburusan3         = '';
                $dataurusan3        = '';
                $databidang         = '';

                if ($row->u1 && ($row->u1 != '0.00')) {
                    $dataurusan1    = $this->Model_Master->get_sub_urusan_by('kode', $row->u1, 0, 1);
                }

                if ($row->u2 && ($row->u2 != '0.00')) {
                    $dataurusan2    = $this->Model_Master->get_sub_urusan_by('kode', $row->u2, 0, 1);
                }

                if ($row->u3 && ($row->u3 != '0.00')) {
                    $dataurusan3    = $this->Model_Master->get_sub_urusan_by('kode', $row->u3, 0, 1);
                }

                if ($dataurusan1) {
                    $suburusan1     = '<a href="javascript:;" class="btn-tooltip font-bold" title="SUB URUSAN : ' . br() . ucwords(strtolower($dataurusan1->nama)) . '">' . $row->u1 . '</a>';
                }
                if ($dataurusan2) {
                    $suburusan2     = '<a href="javascript:;" class="btn-tooltip font-bold" title="SUB URUSAN : ' . br() . ucwords(strtolower($dataurusan2->nama)) . '">' . $row->u2 . '</a>';
                }
                if ($dataurusan3) {
                    $suburusan3     = '<a href="javascript:;" class="btn-tooltip font-bold" title="SUB URUSAN : ' . br() . ucwords(strtolower($dataurusan3->nama)) . '">' . $row->u3 . '</a>';
                }

                $u1             = $suburusan1 ? $suburusan1 : $row->u1;
                $u2             = $suburusan2 ? $suburusan2 : $row->u2;
                $u3             = $suburusan3 ? $suburusan3 : $row->u3;

                if ($row->id_bidang) {
                    $databidang = bp_bidang($row->id_bidang);
                }

                $type           = '<span class="badge badge-sm badge-info">SKPD</span>';

                if ($row->type) {
                    $type           = '<span class="badge badge-sm badge-danger">UNIT</span>';
                }

                $btn_edit       = '<a 
                                        href="javascript:;" 
                                        data-url="' . base_url('master/saveskpd/' . $id) . '" 
                                        class="btn btn-sm btn-outline-primary btn-tooltip" 
                                        title="Edit SKPD" 
                                        data-name="' . $row->nama . '" 
                                        data-code="' . $row->kode . '"
                                        data-u1="' . $row->u1 . '"
                                        data-u2="' . $row->u2 . '"
                                        data-u3="' . $row->u3 . '"
                                        data-urut="' . $row->urut . '"
                                        data-unit="' . $row->unit . '"
                                        data-bidang="' . bp_encrypt($row->id_bidang) . '"
                                        data-head="' . $row->nama_kepala . '"
                                        data-nip="' . $row->nip . '"
                                        data-status="' . $row->status_kepala . '"
                                        id="btn-update-skpd"><i class="fa fa-edit"></i>
                                    </a>';
                $btn_delete     = '<a href="javascript:;" data-url="' . base_url('master/deleteskpd/' . $id) . '" data-name="' . $row->nama . '" class="btn btn-sm btn-outline-danger btn-tooltip" title="Hapus SKPD" id="btn-delete-skpd"><i class="fa fa-times"></i></a>';


                $skpd           = '<span class="text-primary text-bold">' . $name . '</span><hr class="px-2 m-0" />';
                $skpd           .= 'Kepala      : ' . ($row->nama_kepala ? bp_strong(strtoupper($row->nama_kepala)) : '-') . '<br />';
                $skpd           .= 'NIP         : ' . ($row->nip ? $row->nip : '-') . '<br />';
                $skpd           .= 'Status      : ' . ($row->status_kepala ? $row->status_kepala : '-') . '<br />';

                $records["aaData"][] = array(
                    bp_center($i),
                    $code,
                    "<div style='width: 150px;' class='text-wrap'>" . $skpd . "</div>",
                    bp_center($type),
                    "<div style='width: 150px;' class='text-wrap'>" . ($databidang ? $databidang->nama : '-') . "</div>",
                    bp_center($u1),
                    bp_center($u2),
                    bp_center($u3),
                    bp_center($row->urut),
                    bp_center((($is_administrator && $crud_access) ? $btn_edit . $btn_delete : ''))
                );
                $i++;
            }
        }

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $records["sEcho"]                   = $sEcho;
        $records["iTotalRecords"]           = $iTotalRecords;
        $records["iTotalDisplayRecords"]    = $iTotalRecords;

        echo json_encode($records);
    }



    // =============================================================================================
    // Action Data Master
    // =============================================================================================

    function saveurusan($id = '')
    {
        // This is for AJAX request
        if (!$this->input->is_ajax_request()) exit('No direct script access allowed');

        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            // Set JSON data
            $data = array(
                'status'        => 'access_denied',
                'login'         => 'login',
                'url'           => base_url('login'),
            );
            die(json_encode($data));
        }

        // Set data Return
        $bp_token       = $this->security->get_csrf_hash();
        $data           = array(
            'status'    => 'error',
            'token'     => $bp_token,
            'message'   => 'Simpan Urusan tidak berhasil.'
        );

        // -------------------------------------------------
        // Set Variable
        // -------------------------------------------------
        $current_member         = bp_get_current_member();
        $is_admin               = as_administrator($current_member);

        $reg_code               = $this->input->post('reg_code');
        $reg_code               = trim(bp_isset($reg_code, '', '', true));
        $reg_name               = $this->input->post('reg_name');
        $reg_name               = trim(bp_isset($reg_name, '', '', true));

        // Check Available Urusan if ID not null
        // -------------------------------------------------
        $dataurusan         = '';
        if ($id) {
            $id = bp_decrypt($id);

            // Check Data Urusan
            // -------------------------------------------------
            if (!$dataurusan = bp_urusan($id)) {
                $data['message'] = 'Simpan Urusan tidak berhasil. Data Urusan Tidak Ditemukan';
                die(json_encode($data)); // Set JSON data
            }
        }

        // -------------------------------------------------
        // Check Form Validation
        // -------------------------------------------------
        $this->form_validation->set_rules('reg_code', 'Kode', 'required');

        if (!$dataurusan) {
            $this->form_validation->set_rules('reg_code', 'Kode', 'is_unique[urusan.kode]');
        }

        $this->form_validation->set_rules('reg_name', 'Nama', 'required');
        $this->form_validation->set_message('required', '%s harus di isi');
        $this->form_validation->set_message('is_unique', '%s Harus Unique');
        $this->form_validation->set_error_delimiters('* ', br());

        if ($this->form_validation->run() == FALSE) {

            $data['message'] = 'Simpan Urusan tidak berhasil. ' . br() . validation_errors();
            die(json_encode($data)); // Set JSON data
        }

        // -------------------------------------------------
        // Begin Transaction
        // -------------------------------------------------
        $this->db->trans_begin();

        // Set Data Urusan
        // -------------------------------------------------

        $data_save = array(
            'kode'      => $reg_code,
            'nama'      => $reg_name
        );

        if ($dataurusan) {
            if (!$id_update_urusan = $this->Model_Master->update_data_urusan($dataurusan->id, $data_save)) {
                $data['message'] = 'Simpan Urusan tidak berhasil. Terjadi kesalahan sistem';
                die(json_encode($data)); // Set JSON data
            }
        } else {
            if (!$id_save_urusan = $this->Model_Master->save_urusan($data_save)) {
                $data['message'] = 'Simpan Urusan tidak berhasil. Terjadi kesalahan sistem';
                die(json_encode($data)); // Set JSON data
            }
        }


        // -------------------------------------------------
        // Commit or Rollback Transaction
        // -------------------------------------------------
        if ($this->db->trans_status() === FALSE) {
            // Rollback Transaction
            $this->db->trans_rollback();
            $data['message'] = 'Simpan Data Urusan tidak berhasil. Terjadi kesalahan data transaksi.';
            die(json_encode($data)); // Set JSON data
        } else {
            // Commit Transaction
            $this->db->trans_commit();
            // Complete Transaction
            $this->db->trans_complete();
            bp_log_action('ACTION_SAVE_URUSAN', $reg_code, $reg_code, json_encode(array('cookie' => $_COOKIE, 'status' => 'SUCCESS', 'code' => $reg_code, 'name' => $reg_name)));

            $data           = array(
                'status'    => 'success',
                'token'     => $bp_token,
                'message'   => 'Simpan Data Urusan Berhasil'
            );
            die(json_encode($data));
        }
    }

    function savesuburusan($id = '')
    {
        // This is for AJAX request
        if (!$this->input->is_ajax_request()) exit('No direct script access allowed');

        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            // Set JSON data
            $data = array(
                'status'        => 'access_denied',
                'login'         => 'login',
                'url'           => base_url('login'),
            );
            die(json_encode($data));
        }

        // Set data Return
        $bp_token       = $this->security->get_csrf_hash();
        $data           = array(
            'status'    => 'error',
            'token'     => $bp_token,
            'message'   => 'Simpan Sub Urusan tidak berhasil.'
        );

        // -------------------------------------------------
        // Set Variable
        // -------------------------------------------------
        $current_member         = bp_get_current_member();
        $is_admin               = as_administrator($current_member);

        $reg_urusan             = $this->input->post('reg_urusan');
        $reg_urusan             = trim(bp_isset($reg_urusan, '', '', true));
        $reg_code               = $this->input->post('reg_code');
        $reg_code               = trim(bp_isset($reg_code, '', '', true));
        $reg_name               = $this->input->post('reg_name');
        $reg_name               = trim(bp_isset($reg_name, '', '', true));

        // Check Available Urusan if ID not null
        // -------------------------------------------------
        $datasuburusan         = '';
        if ($id) {
            $id = bp_decrypt($id);

            // Check Data Urusan
            // -------------------------------------------------
            if (!$datasuburusan = bp_sub_urusan($id)) {
                $data['message'] = 'Simpan Sub Urusan tidak berhasil. Data Urusan Tidak Ditemukan';
                die(json_encode($data)); // Set JSON data
            }
        }

        // -------------------------------------------------
        // Check Form Validation
        // -------------------------------------------------
        $this->form_validation->set_rules('reg_code', 'Kode', 'required');

        if (!$datasuburusan) {
            $this->form_validation->set_rules('reg_code', 'Kode', 'is_unique[sub_urusan.kode]');
        }

        $this->form_validation->set_rules('reg_urusan', 'Urusan', 'required');
        $this->form_validation->set_rules('reg_name', 'Nama', 'required');
        $this->form_validation->set_message('required', '%s harus di isi');
        $this->form_validation->set_message('is_unique', '%s Harus Unique');
        $this->form_validation->set_error_delimiters('* ', br());

        if ($this->form_validation->run() == FALSE) {
            $data['message'] = 'Simpan Sub Urusan tidak berhasil. ' . br() . validation_errors();
            die(json_encode($data)); // Set JSON data
        }

        // Check Data Urusan
        // -------------------------------------------------
        $reg_urusan                 = bp_decrypt($reg_urusan);

        if (!$reg_urusan) {
            $data['message']        = 'Simpan Sub Urusan tidak berhasil. Data Urusan Tidak Ditemukan';
            die(json_encode($data)); // Set JSON data
        }

        $dataurusan = bp_urusan($reg_urusan);

        if (!$dataurusan) {
            $data['message'] = 'Simpan Sub Urusan tidak berhasil. Data Urusan Tidak Ditemukan';
            die(json_encode($data)); // Set JSON data
        }

        // -------------------------------------------------
        // Begin Transaction
        // -------------------------------------------------
        $this->db->trans_begin();

        // Set Data Urusan
        // -------------------------------------------------

        $data_save = array(
            'id_urusan' => $dataurusan->id,
            'kode'      => $reg_code,
            'nama'      => $reg_name
        );

        if ($datasuburusan) {
            if (!$id_update_sub_urusan = $this->Model_Master->update_data_sub_urusan($datasuburusan->id, $data_save)) {
                $data['message'] = 'Simpan Sub Urusan tidak berhasil. Terjadi kesalahan sistem';
                die(json_encode($data)); // Set JSON data
            }
        } else {
            if (!$id_save_sub_urusan = $this->Model_Master->save_sub_urusan($data_save)) {
                $data['message'] = 'Simpan Sub Urusan tidak berhasil. Terjadi kesalahan sistem';
                die(json_encode($data)); // Set JSON data
            }
        }


        // -------------------------------------------------
        // Commit or Rollback Transaction
        // -------------------------------------------------
        if ($this->db->trans_status() === FALSE) {
            // Rollback Transaction
            $this->db->trans_rollback();
            $data['message'] = 'Simpan Data Sub Urusan tidak berhasil. Terjadi kesalahan data transaksi.';
            die(json_encode($data)); // Set JSON data
        } else {
            // Commit Transaction
            $this->db->trans_commit();
            // Complete Transaction
            $this->db->trans_complete();
            bp_log_action('ACTION_SAVE_SUB_URUSAN', $reg_code, $reg_code, json_encode(array('cookie' => $_COOKIE, 'status' => 'SUCCESS', 'code' => $reg_code, 'name' => $reg_name)));

            $data           = array(
                'status'    => 'success',
                'token'     => $bp_token,
                'message'   => 'Simpan Data Sub Urusan Berhasil'
            );
            die(json_encode($data));
        }
    }

    function saveprogram($id = '')
    {
        // This is for AJAX request
        if (!$this->input->is_ajax_request()) exit('No direct script access allowed');

        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            // Set JSON data
            $data = array(
                'status'        => 'access_denied',
                'login'         => 'login',
                'url'           => base_url('login'),
            );
            die(json_encode($data));
        }

        // Set data Return
        $bp_token       = $this->security->get_csrf_hash();
        $data           = array(
            'status'    => 'error',
            'token'     => $bp_token,
            'message'   => 'Simpan Program tidak berhasil.'
        );

        // -------------------------------------------------
        // Set Variable
        // -------------------------------------------------
        $current_member         = bp_get_current_member();
        $is_admin               = as_administrator($current_member);

        $reg_sub_urusan         = $this->input->post('reg_sub_urusan');
        $reg_sub_urusan         = trim(bp_isset($reg_sub_urusan, '', '', true));
        $reg_code               = $this->input->post('reg_code');
        $reg_code               = trim(bp_isset($reg_code, '', '', true));
        $reg_name               = $this->input->post('reg_name');
        $reg_name               = trim(bp_isset($reg_name, '', '', true));

        // Check Available Program if ID not null
        // -------------------------------------------------
        $dataprogram         = '';
        if ($id) {
            $id = bp_decrypt($id);

            // Check Data Program
            // -------------------------------------------------
            if (!$dataprogram = bp_program($id)) {
                $data['message'] = 'Simpan Program tidak berhasil. Data Urusan Tidak Ditemukan';
                die(json_encode($data)); // Set JSON data
            }
        }

        // -------------------------------------------------
        // Check Form Validation
        // -------------------------------------------------
        $this->form_validation->set_rules('reg_code', 'Kode', 'required');

        if (!$dataprogram) {
            $this->form_validation->set_rules('reg_code', 'Kode', 'is_unique[program.kode]');
        }

        $this->form_validation->set_rules('reg_sub_urusan', 'Sub Urusan', 'required');
        $this->form_validation->set_rules('reg_name', 'Nama', 'required');
        $this->form_validation->set_message('required', '%s harus di isi');
        $this->form_validation->set_message('is_unique', '%s Harus Unique');
        $this->form_validation->set_error_delimiters('* ', br());

        if ($this->form_validation->run() == FALSE) {
            $data['message'] = 'Simpan Program tidak berhasil. ' . br() . validation_errors();
            die(json_encode($data)); // Set JSON data
        }

        // Check Data Urusan
        // -------------------------------------------------
        $reg_sub_urusan                 = bp_decrypt($reg_sub_urusan);

        if (!$reg_sub_urusan) {
            $data['message']        = 'Simpan Program tidak berhasil. Data Urusan Tidak Ditemukan';
            die(json_encode($data)); // Set JSON data
        }

        $datasuburusan = bp_sub_urusan($reg_sub_urusan);

        if (!$datasuburusan) {
            $data['message'] = 'Simpan Program tidak berhasil. Data Urusan Tidak Ditemukan';
            die(json_encode($data)); // Set JSON data
        }

        // -------------------------------------------------
        // Begin Transaction
        // -------------------------------------------------
        $this->db->trans_begin();

        // Set Data Urusan
        // -------------------------------------------------

        $data_save = array(
            'id_sub_urusan'         => $datasuburusan->id,
            'kode'                  => $reg_code,
            'nama'                  => $reg_name
        );

        if ($dataprogram) {
            if (!$id_update_program = $this->Model_Master->update_data_program($dataprogram->id, $data_save)) {
                $data['message'] = 'Simpan Program tidak berhasil. Terjadi kesalahan sistem';
                die(json_encode($data)); // Set JSON data
            }
        } else {
            if (!$id_save_program = $this->Model_Master->save_program($data_save)) {
                $data['message'] = 'Simpan Program tidak berhasil. Terjadi kesalahan sistem';
                die(json_encode($data)); // Set JSON data
            }
        }


        // -------------------------------------------------
        // Commit or Rollback Transaction
        // -------------------------------------------------
        if ($this->db->trans_status() === FALSE) {
            // Rollback Transaction
            $this->db->trans_rollback();
            $data['message'] = 'Simpan Data Program tidak berhasil. Terjadi kesalahan data transaksi.';
            die(json_encode($data)); // Set JSON data
        } else {
            // Commit Transaction
            $this->db->trans_commit();
            // Complete Transaction
            $this->db->trans_complete();
            bp_log_action('ACTION_SAVE_PROGRAM', $reg_code, $reg_code, json_encode(array('cookie' => $_COOKIE, 'status' => 'SUCCESS', 'code' => $reg_code, 'name' => $reg_name)));

            $data           = array(
                'status'    => 'success',
                'token'     => $bp_token,
                'message'   => 'Simpan Data Program Berhasil'
            );
            die(json_encode($data));
        }
    }

    function savekegiatan($id = '')
    {
        // This is for AJAX request
        if (!$this->input->is_ajax_request()) exit('No direct script access allowed');

        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            // Set JSON data
            $data = array(
                'status'        => 'access_denied',
                'login'         => 'login',
                'url'           => base_url('login'),
            );
            die(json_encode($data));
        }

        // Set data Return
        $bp_token       = $this->security->get_csrf_hash();
        $data           = array(
            'status'    => 'error',
            'token'     => $bp_token,
            'message'   => 'Simpan Kegiatan tidak berhasil.'
        );

        // -------------------------------------------------
        // Set Variable
        // -------------------------------------------------
        $current_member         = bp_get_current_member();
        $is_admin               = as_administrator($current_member);

        $reg_program            = $this->input->post('reg_program');
        $reg_program            = trim(bp_isset($reg_program, '', '', true));
        $reg_code               = $this->input->post('reg_code');
        $reg_code               = trim(bp_isset($reg_code, '', '', true));
        $reg_name               = $this->input->post('reg_name');
        $reg_name               = trim(bp_isset($reg_name, '', '', true));

        // Check Available Kegiatan if ID not null
        // -------------------------------------------------
        $datakegiatan         = '';
        if ($id) {
            $id = bp_decrypt($id);

            // Check Data Program
            // -------------------------------------------------
            if (!$datakegiatan = bp_kegiatan($id)) {
                $data['message'] = 'Simpan Program tidak berhasil. Data Urusan Tidak Ditemukan';
                die(json_encode($data)); // Set JSON data
            }
        }

        // -------------------------------------------------
        // Check Form Validation
        // -------------------------------------------------
        $this->form_validation->set_rules('reg_code', 'Kode', 'required');

        if (!$datakegiatan) {
            $this->form_validation->set_rules('reg_code', 'Kode', 'is_unique[kegiatan.kode]');
        }

        $this->form_validation->set_rules('reg_program', 'Program', 'required');
        $this->form_validation->set_rules('reg_name', 'Nama', 'required');
        $this->form_validation->set_message('required', '%s harus di isi');
        $this->form_validation->set_message('is_unique', '%s Harus Unique');
        $this->form_validation->set_error_delimiters('* ', br());

        if ($this->form_validation->run() == FALSE) {
            $data['message'] = 'Simpan Kegiatan tidak berhasil. ' . br() . validation_errors();
            die(json_encode($data)); // Set JSON data
        }

        // Check Data Program
        // -------------------------------------------------
        $reg_program                 = bp_decrypt($reg_program);

        if (!$reg_program) {
            $data['message']        = 'Simpan Kegiatan tidak berhasil. Data Urusan Tidak Ditemukan';
            die(json_encode($data)); // Set JSON data
        }

        $dataprogram = bp_program($reg_program);

        if (!$dataprogram) {
            $data['message'] = 'Simpan Kegiatan tidak berhasil. Data Urusan Tidak Ditemukan';
            die(json_encode($data)); // Set JSON data
        }

        // -------------------------------------------------
        // Begin Transaction
        // -------------------------------------------------
        $this->db->trans_begin();

        // Set Data Urusan
        // -------------------------------------------------

        $data_save = array(
            'id_program'            => $dataprogram->id,
            'kode'                  => $reg_code,
            'nama'                  => $reg_name
        );

        if ($datakegiatan) {
            if (!$id_update = $this->Model_Master->update_data_kegiatan($datakegiatan->id, $data_save)) {
                $data['message'] = 'Simpan Kegiatan tidak berhasil. Terjadi kesalahan sistem';
                die(json_encode($data)); // Set JSON data
            }
        } else {
            if (!$id_save = $this->Model_Master->save_kegiatan($data_save)) {
                $data['message'] = 'Simpan Kegiatan tidak berhasil. Terjadi kesalahan sistem';
                die(json_encode($data)); // Set JSON data
            }
        }


        // -------------------------------------------------
        // Commit or Rollback Transaction
        // -------------------------------------------------
        if ($this->db->trans_status() === FALSE) {
            // Rollback Transaction
            $this->db->trans_rollback();
            $data['message'] = 'Simpan Data Kegiatan tidak berhasil. Terjadi kesalahan data transaksi.';
            die(json_encode($data)); // Set JSON data
        } else {
            // Commit Transaction
            $this->db->trans_commit();
            // Complete Transaction
            $this->db->trans_complete();
            bp_log_action('ACTION_SAVE_KEGIATAN', $reg_code, $reg_code, json_encode(array('cookie' => $_COOKIE, 'status' => 'SUCCESS', 'code' => $reg_code, 'name' => $reg_name)));

            $data           = array(
                'status'    => 'success',
                'token'     => $bp_token,
                'message'   => 'Simpan Data Kegiatan Berhasil'
            );
            die(json_encode($data));
        }
    }

    function savesubkegiatan($id = '')
    {
        // This is for AJAX request
        if (!$this->input->is_ajax_request()) exit('No direct script access allowed');

        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            // Set JSON data
            $data = array(
                'status'        => 'access_denied',
                'login'         => 'login',
                'url'           => base_url('login'),
            );
            die(json_encode($data));
        }

        // Set data Return
        $bp_token       = $this->security->get_csrf_hash();
        $data           = array(
            'status'    => 'error',
            'token'     => $bp_token,
            'message'   => 'Simpan Sub Kegiatan tidak berhasil.'
        );

        // -------------------------------------------------
        // Set Variable
        // -------------------------------------------------
        $current_member         = bp_get_current_member();
        $is_admin               = as_administrator($current_member);

        $reg_kegiatan           = $this->input->post('reg_kegiatan');
        $reg_kegiatan           = trim(bp_isset($reg_kegiatan, '', '', true));
        $reg_code               = $this->input->post('reg_code');
        $reg_code               = trim(bp_isset($reg_code, '', '', true));
        $reg_name               = $this->input->post('reg_name');
        $reg_name               = trim(bp_isset($reg_name, '', '', true));

        // Check Available Kegiatan if ID not null
        // -------------------------------------------------
        $datasubkegiatan         = '';
        if ($id) {
            $id = bp_decrypt($id);

            // Check Data Program
            // -------------------------------------------------
            if (!$datasubkegiatan = bp_sub_kegiatan($id)) {
                $data['message'] = 'Simpan Sub Kegiatan tidak berhasil. Data Urusan Tidak Ditemukan';
                die(json_encode($data)); // Set JSON data
            }
        }

        // -------------------------------------------------
        // Check Form Validation
        // -------------------------------------------------
        $this->form_validation->set_rules('reg_code', 'Kode', 'required');

        if (!$datasubkegiatan) {
            $this->form_validation->set_rules('reg_code', 'Kode', 'is_unique[sub_kegiatan.kode]');
        }

        $this->form_validation->set_rules('reg_kegiatan', 'Kegiatan', 'required');
        $this->form_validation->set_rules('reg_name', 'Nama', 'required');
        $this->form_validation->set_message('required', '%s harus di isi');
        $this->form_validation->set_message('is_unique', '%s Harus Unique');
        $this->form_validation->set_error_delimiters('* ', br());

        if ($this->form_validation->run() == FALSE) {
            $data['message'] = 'Simpan Sub Kegiatan tidak berhasil. ' . br() . validation_errors();
            die(json_encode($data)); // Set JSON data
        }

        // Check Data Kegiatan
        // -------------------------------------------------
        $reg_kegiatan                 = bp_decrypt($reg_kegiatan);

        if (!$reg_kegiatan) {
            $data['message']        = 'Simpan Sub Kegiatan tidak berhasil. Data Urusan Tidak Ditemukan';
            die(json_encode($data)); // Set JSON data
        }

        $datakegiatan = bp_kegiatan($reg_kegiatan);

        if (!$datakegiatan) {
            $data['message'] = 'Simpan Sub Kegiatan tidak berhasil. Data Urusan Tidak Ditemukan';
            die(json_encode($data)); // Set JSON data
        }

        // -------------------------------------------------
        // Begin Transaction
        // -------------------------------------------------
        $this->db->trans_begin();

        // Set Data Urusan
        // -------------------------------------------------

        $data_save = array(
            'id_kegiatan'            => $datakegiatan->id,
            'kode'                  => $reg_code,
            'nama'                  => $reg_name
        );

        if ($datasubkegiatan) {
            if (!$id_update = $this->Model_Master->update_data_sub_kegiatan($datasubkegiatan->id, $data_save)) {
                $data['message'] = 'Simpan Sub Kegiatan tidak berhasil. Terjadi kesalahan sistem';
                die(json_encode($data)); // Set JSON data
            }
        } else {
            if (!$id_save = $this->Model_Master->save_sub_kegiatan($data_save)) {
                $data['message'] = 'Simpan Sub Kegiatan tidak berhasil. Terjadi kesalahan sistem';
                die(json_encode($data)); // Set JSON data
            }
        }


        // -------------------------------------------------
        // Commit or Rollback Transaction
        // -------------------------------------------------
        if ($this->db->trans_status() === FALSE) {
            // Rollback Transaction
            $this->db->trans_rollback();
            $data['message'] = 'Simpan Data Sub Kegiatan tidak berhasil. Terjadi kesalahan data transaksi.';
            die(json_encode($data)); // Set JSON data
        } else {
            // Commit Transaction
            $this->db->trans_commit();
            // Complete Transaction
            $this->db->trans_complete();
            bp_log_action('ACTION_SAVE_SUB_KEGIATAN', $reg_code, $reg_code, json_encode(array('cookie' => $_COOKIE, 'status' => 'SUCCESS', 'code' => $reg_code, 'name' => $reg_name)));

            $data           = array(
                'status'    => 'success',
                'token'     => $bp_token,
                'message'   => 'Simpan Data Sub Kegiatan Berhasil'
            );
            die(json_encode($data));
        }
    }



    function deleteurusan($id = '')
    {
        if (!$this->input->is_ajax_request()) {
            redirect(base_url('productmanage/productlist'), 'refresh');
        }
        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            $data = array('status' => 'access_denied', 'url' => base_url('login'));
            die(json_encode($data)); // JSON encode data
        }

        if (!$id) {
            $data = array('status' => 'error', 'message' => 'ID Urusan tidak ditemukan !');
            die(json_encode($data));
        }

        $id = bp_decrypt($id);
        if (!$data_urusan = bp_urusan($id)) {
            $data = array('status' => 'error', 'message' => 'Data Urusan tidak ditemukan !');
            die(json_encode($data));
        }

        if (!$delete_data = $this->Model_Master->delete_data_urusan($id)) {
            $data = array('status' => 'error', 'message' => 'Urusan tidak berhasil dihapus !');
            die(json_encode($data));
        }

        // Save Success
        $data = array('status' => 'success', 'message' => 'Urusan berhasil dihapus.');
        die(json_encode($data));
    }

    function deletesuburusan($id = '')
    {
        if (!$this->input->is_ajax_request()) {
            redirect(base_url('master/suburusan'), 'refresh');
        }
        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            $data = array('status' => 'access_denied', 'url' => base_url('login'));
            die(json_encode($data)); // JSON encode data
        }

        if (!$id) {
            $data = array('status' => 'error', 'message' => 'ID Sub Urusan tidak ditemukan !');
            die(json_encode($data));
        }

        $id = bp_decrypt($id);
        if (!$data_sub_urusan = bp_sub_urusan($id)) {
            $data = array('status' => 'error', 'message' => 'Data Sub Urusan tidak ditemukan !');
            die(json_encode($data));
        }

        if (!$delete_data = $this->Model_Master->delete_data_sub_urusan($id)) {
            $data = array('status' => 'error', 'message' => 'Sub Urusan tidak berhasil dihapus !');
            die(json_encode($data));
        }

        // Save Success
        $data = array('status' => 'success', 'message' => 'Sub Urusan berhasil dihapus.');
        die(json_encode($data));
    }

    function deleteprogram($id = '')
    {
        if (!$this->input->is_ajax_request()) {
            redirect(base_url('master/program'), 'refresh');
        }
        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            $data = array('status' => 'access_denied', 'url' => base_url('login'));
            die(json_encode($data)); // JSON encode data
        }

        if (!$id) {
            $data = array('status' => 'error', 'message' => 'ID Program tidak ditemukan !');
            die(json_encode($data));
        }

        $id = bp_decrypt($id);
        if (!$data_program = bp_program($id)) {
            $data = array('status' => 'error', 'message' => 'Data Program tidak ditemukan !');
            die(json_encode($data));
        }

        if (!$delete_data = $this->Model_Master->delete_data_program($id)) {
            $data = array('status' => 'error', 'message' => 'Data Program tidak berhasil dihapus !');
            die(json_encode($data));
        }

        // Save Success
        $data = array('status' => 'success', 'message' => 'Data Program berhasil dihapus.');
        die(json_encode($data));
    }

    function deletekegiatan($id = '')
    {
        if (!$this->input->is_ajax_request()) {
            redirect(base_url('master/kegiatan'), 'refresh');
        }
        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            $data = array('status' => 'access_denied', 'url' => base_url('login'));
            die(json_encode($data)); // JSON encode data
        }

        if (!$id) {
            $data = array('status' => 'error', 'message' => 'ID Kegiatan tidak ditemukan !');
            die(json_encode($data));
        }

        $id = bp_decrypt($id);
        if (!$data_kegiatan = bp_kegiatan($id)) {
            $data = array('status' => 'error', 'message' => 'Data Kegiatan tidak ditemukan !');
            die(json_encode($data));
        }

        if (!$delete_data = $this->Model_Master->delete_data_kegiatan($id)) {
            $data = array('status' => 'error', 'message' => 'Data Kegiatan tidak berhasil dihapus !');
            die(json_encode($data));
        }

        // Save Success
        $data = array('status' => 'success', 'message' => 'Data Kegiatan berhasil dihapus.');
        die(json_encode($data));
    }

    function deletesubkegiatan($id = '')
    {
        if (!$this->input->is_ajax_request()) {
            redirect(base_url('master/subkegiatan'), 'refresh');
        }
        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            $data = array('status' => 'access_denied', 'url' => base_url('login'));
            die(json_encode($data)); // JSON encode data
        }

        if (!$id) {
            $data = array('status' => 'error', 'message' => 'ID Sub Kegiatan tidak ditemukan !');
            die(json_encode($data));
        }

        $id = bp_decrypt($id);
        if (!$data_sub_kegiatan = bp_sub_kegiatan($id)) {
            $data = array('status' => 'error', 'message' => 'Data Sub Kegiatan tidak ditemukan !');
            die(json_encode($data));
        }

        if (!$delete_data = $this->Model_Master->delete_data_sub_kegiatan($id)) {
            $data = array('status' => 'error', 'message' => 'Data Sub Kegiatan tidak berhasil dihapus !');
            die(json_encode($data));
        }

        // Save Success
        $data = array('status' => 'success', 'message' => 'Data Sub Kegiatan berhasil dihapus.');
        die(json_encode($data));
    }

    function importurusan()
    {

        if (!$this->input->is_ajax_request()) {
            redirect(base_url('master/urusan'), 'refresh');
        }

        $auth = auth_redirect($this->input->is_ajax_request());

        if (!$auth) {
            $data = array('status' => 'access_denied', 'url' => base_url('login'));
            die(json_encode($data)); // JSON encode data
        }

        // Set data Return
        $bp_token       = $this->security->get_csrf_hash();

        $data           = array(
            'status'    => 'error',
            'token'     => $bp_token,
            'message'   => 'Import Data Urusan tidak berhasil.'
        );

        // -------------------------------------------------
        // Set Variable
        // -------------------------------------------------
        $current_member         = bp_get_current_member();
        $is_admin               = as_administrator($current_member);

        if (!$is_admin) {
            $data['message'] = 'Import Data Urusan tidak berhasil. Hanya admin yang dapat melakukan aksi ini.';
        }

        $img_name                   = random_string() . '-' . time();

        $config['upload_path']      = DATA_IMPORT_PATH;
        $config['allowed_types']    = 'xlsx';
        $config['max_size']         = '1048';
        $config['overwrite']        = true;
        $config['file_name']        = $img_name;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        $img_upload                 = true;
        $img_msg                    = '';
        if (!$this->upload->do_upload("reg_file")) {
            $img_upload             = false;
            $img_msg                = $this->upload->display_errors();
        }

        if (!$img_upload && $img_msg) {
            $data['message'] = 'Import Data Gagal. ' . $img_msg;
            die(json_encode($data)); // Set JSON data
        }

        $get_data_import       = $this->upload->data();

        if (!$get_data_import) {
            $data['message'] = 'Import Data Gagal. Terjadi kesalahan Sistem';
            die(json_encode($data)); // Set JSON data
        }

        $filedata                       = IMPORT_PATH . $img_name . '.xlsx';

        if (!file_exists($filedata)) {
            $data['status']             = 'error';
            $data['message']            = 'Gagal Import Data Urusan, File gagal diupload';
            $data['data']               = null;
            die(json_encode($data));
        }

        $migrate                        = bp_import_urusan($filedata);

        unlink($filedata);

        if ($migrate['status'] == 'error') {
            $data['status']             = 'error';
            $data['message']            = 'Gagal Import Data Urusan';
            $data['data']               = null;
            die(json_encode($data));
        }

        $message = 'Import Data Urusan Berhasil.';
        $message .= ' ' . $migrate['data_insert'] . ' data berhasil ditambahkan';
        $message .= ' dan ' . $migrate['data_update'] . ' data berhasil dirubah';

        $data['status']                         = 'success';
        $data['message']                        = $message;
        $data['data']                           = $migrate;

        die(json_encode($data));
    }

    function importsuburusan()
    {

        if (!$this->input->is_ajax_request()) {
            redirect(base_url('master/suburusan'), 'refresh');
        }

        $auth = auth_redirect($this->input->is_ajax_request());

        if (!$auth) {
            $data = array('status' => 'access_denied', 'url' => base_url('login'));
            die(json_encode($data)); // JSON encode data
        }

        // Set data Return
        $bp_token       = $this->security->get_csrf_hash();

        $data           = array(
            'status'    => 'error',
            'token'     => $bp_token,
            'message'   => 'Import Data Sub Urusan tidak berhasil.'
        );

        // -------------------------------------------------
        // Set Variable
        // -------------------------------------------------
        $current_member         = bp_get_current_member();
        $is_admin               = as_administrator($current_member);

        if (!$is_admin) {
            $data['message'] = 'Import Data Sub Urusan tidak berhasil. Hanya admin yang dapat melakukan aksi ini.';
        }

        $img_name                   = random_string() . '-' . time();

        $config['upload_path']      = DATA_IMPORT_PATH;
        $config['allowed_types']    = 'xlsx';
        $config['max_size']         = '1048';
        $config['overwrite']        = true;
        $config['file_name']        = $img_name;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        $img_upload                 = true;
        $img_msg                    = '';
        if (!$this->upload->do_upload("reg_file")) {
            $img_upload             = false;
            $img_msg                = $this->upload->display_errors();
        }

        if (!$img_upload && $img_msg) {
            $data['message'] = 'Import Data Gagal. ' . $img_msg;
            die(json_encode($data)); // Set JSON data
        }

        $get_data_import       = $this->upload->data();

        if (!$get_data_import) {
            $data['message'] = 'Import Data Gagal. Terjadi kesalahan Sistem';
            die(json_encode($data)); // Set JSON data
        }

        $filedata                       = IMPORT_PATH . $img_name . '.xlsx';

        if (!file_exists($filedata)) {
            $data['status']             = 'error';
            $data['message']            = 'Gagal Import Data Sub Urusan, File gagal diupload';
            $data['data']               = null;
            die(json_encode($data));
        }

        $migrate                        = bp_import_sub_urusan($filedata);
        unlink($filedata);

        if ($migrate['status'] == 'error') {
            $data['status']             = 'error';
            $data['message']            = 'Gagal Import Data Sub Urusan';
            $data['data']               = null;
            die(json_encode($data));
        }

        $message = 'Import Data Sub Urusan Berhasil.';
        $message .= ' ' . $migrate['data_insert'] . ' data berhasil ditambahkan';
        $message .= ' dan ' . $migrate['data_update'] . ' data berhasil dirubah';

        $data['status']                         = 'success';
        $data['message']                        = $message;
        $data['data']                           = $migrate;

        die(json_encode($data));
    }

    function importprogram()
    {

        if (!$this->input->is_ajax_request()) {
            redirect(base_url('master/program'), 'refresh');
        }

        $auth = auth_redirect($this->input->is_ajax_request());

        if (!$auth) {
            $data = array('status' => 'access_denied', 'url' => base_url('login'));
            die(json_encode($data)); // JSON encode data
        }

        // Set data Return
        $bp_token       = $this->security->get_csrf_hash();

        $data           = array(
            'status'    => 'error',
            'token'     => $bp_token,
            'message'   => 'Import Data Program tidak berhasil.'
        );

        // -------------------------------------------------
        // Set Variable
        // -------------------------------------------------
        $current_member         = bp_get_current_member();
        $is_admin               = as_administrator($current_member);

        if (!$is_admin) {
            $data['message'] = 'Import Data Program tidak berhasil. Hanya admin yang dapat melakukan aksi ini.';
            die(json_encode($data));
        }

        $img_name                   = random_string() . '-' . time();

        $config['upload_path']      = DATA_IMPORT_PATH;
        $config['allowed_types']    = 'xlsx';
        $config['max_size']         = '1048';
        $config['overwrite']        = true;
        $config['file_name']        = $img_name;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        $img_upload                 = true;
        $img_msg                    = '';
        if (!$this->upload->do_upload("reg_file")) {
            $img_upload             = false;
            $img_msg                = $this->upload->display_errors();
        }

        if (!$img_upload && $img_msg) {
            $data['message'] = 'Import Data Gagal. ' . $img_msg;
            die(json_encode($data)); // Set JSON data
        }

        $get_data_import       = $this->upload->data();

        if (!$get_data_import) {
            $data['message'] = 'Import Data Gagal. Terjadi kesalahan Sistem';
            die(json_encode($data)); // Set JSON data
        }

        $filedata                       = IMPORT_PATH . $img_name . '.xlsx';

        if (!file_exists($filedata)) {
            $data['status']             = 'error';
            $data['message']            = 'Gagal Import Data Program, File gagal diupload';
            $data['data']               = null;
            die(json_encode($data));
        }

        $migrate                        = bp_import_program($filedata);
        unlink($filedata);

        if ($migrate['status'] == 'error') {
            $data['status']             = 'error';
            $data['message']            = 'Gagal Import Data Program';
            $data['data']               = null;
            die(json_encode($data));
        }

        $message = 'Import Data Program Berhasil.';
        $message .= ' ' . $migrate['data_insert'] . ' data berhasil ditambahkan';
        $message .= ' dan ' . $migrate['data_update'] . ' data berhasil dirubah';

        $data['status']                         = 'success';
        $data['message']                        = $message;
        $data['data']                           = $migrate;

        die(json_encode($data));
    }

    function importkegiatan()
    {

        if (!$this->input->is_ajax_request()) {
            redirect(base_url('master/kegiatan'), 'refresh');
        }

        $auth = auth_redirect($this->input->is_ajax_request());

        if (!$auth) {
            $data = array('status' => 'access_denied', 'url' => base_url('login'));
            die(json_encode($data)); // JSON encode data
        }

        // Set data Return
        $bp_token       = $this->security->get_csrf_hash();

        $data           = array(
            'status'    => 'error',
            'token'     => $bp_token,
            'message'   => 'Import Data Kegiatan tidak berhasil.'
        );

        // -------------------------------------------------
        // Set Variable
        // -------------------------------------------------
        $current_member         = bp_get_current_member();
        $is_admin               = as_administrator($current_member);

        if (!$is_admin) {
            $data['message'] = 'Import Data Kegiatan tidak berhasil. Hanya admin yang dapat melakukan aksi ini.';
            die(json_encode($data));
        }

        $img_name                   = random_string() . '-' . time();

        $config['upload_path']      = DATA_IMPORT_PATH;
        $config['allowed_types']    = 'xlsx';
        $config['max_size']         = '1048';
        $config['overwrite']        = true;
        $config['file_name']        = $img_name;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        $img_upload                 = true;
        $img_msg                    = '';
        if (!$this->upload->do_upload("reg_file")) {
            $img_upload             = false;
            $img_msg                = $this->upload->display_errors();
        }

        if (!$img_upload && $img_msg) {
            $data['message'] = 'Import Data Gagal. ' . $img_msg;
            die(json_encode($data)); // Set JSON data
        }

        $get_data_import       = $this->upload->data();

        if (!$get_data_import) {
            $data['message'] = 'Import Data Gagal. Terjadi kesalahan Sistem';
            die(json_encode($data)); // Set JSON data
        }

        $filedata                       = IMPORT_PATH . $img_name . '.xlsx';

        if (!file_exists($filedata)) {
            $data['status']             = 'error';
            $data['message']            = 'Gagal Import Data Kegiatan, File gagal diupload';
            $data['data']               = null;
            die(json_encode($data));
        }

        $migrate                        = bp_import_kegiatan($filedata);
        unlink($filedata);

        if ($migrate['status'] == 'error') {
            $data['status']             = 'error';
            $data['message']            = 'Gagal Import Data Kegiatan';
            $data['data']               = null;
            die(json_encode($data));
        }

        $message = 'Import Data Kegiatan Berhasil.';
        $message .= ' ' . $migrate['data_insert'] . ' data berhasil ditambahkan';
        $message .= ' dan ' . $migrate['data_update'] . ' data berhasil dirubah';

        $data['status']                         = 'success';
        $data['message']                        = $message;
        $data['data']                           = $migrate;

        die(json_encode($data));
    }

    function importsubkegiatan()
    {

        if (!$this->input->is_ajax_request()) {
            redirect(base_url('master/subkegiatan'), 'refresh');
        }

        $auth = auth_redirect($this->input->is_ajax_request());

        if (!$auth) {
            $data = array('status' => 'access_denied', 'url' => base_url('login'));
            die(json_encode($data)); // JSON encode data
        }

        // Set data Return
        $bp_token       = $this->security->get_csrf_hash();

        $data           = array(
            'status'    => 'error',
            'token'     => $bp_token,
            'message'   => 'Import Data Sub Kegiatan tidak berhasil.'
        );

        // -------------------------------------------------
        // Set Variable
        // -------------------------------------------------
        $current_member         = bp_get_current_member();
        $is_admin               = as_administrator($current_member);

        if (!$is_admin) {
            $data['message'] = 'Import Data Sub Kegiatan tidak berhasil. Hanya admin yang dapat melakukan aksi ini.';
            die(json_encode($data));
        }

        $img_name                   = random_string() . '-' . time();

        $config['upload_path']      = DATA_IMPORT_PATH;
        $config['allowed_types']    = 'xlsx';
        $config['max_size']         = '1048';
        $config['overwrite']        = true;
        $config['file_name']        = $img_name;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        $img_upload                 = true;
        $img_msg                    = '';
        if (!$this->upload->do_upload("reg_file")) {
            $img_upload             = false;
            $img_msg                = $this->upload->display_errors();
        }

        if (!$img_upload && $img_msg) {
            $data['message'] = 'Import Data Gagal. ' . $img_msg;
            die(json_encode($data)); // Set JSON data
        }

        $get_data_import       = $this->upload->data();

        if (!$get_data_import) {
            $data['message'] = 'Import Data Gagal. Terjadi kesalahan Sistem';
            die(json_encode($data)); // Set JSON data
        }

        $filedata                       = IMPORT_PATH . $img_name . '.xlsx';

        if (!file_exists($filedata)) {
            $data['status']             = 'error';
            $data['message']            = 'Gagal Import Data Sub Kegiatan, File gagal diupload';
            $data['data']               = null;
            die(json_encode($data));
        }

        $migrate                        = bp_import_sub_kegiatan($filedata);
        unlink($filedata);

        if ($migrate['status'] == 'error') {
            $data['status']             = 'error';
            $data['message']            = 'Gagal Import Data Sub Kegiatan';
            $data['data']               = null;
            die(json_encode($data));
        }

        $message = 'Import Data Sub Kegiatan Berhasil.';
        $message .= ' ' . $migrate['data_insert'] . ' data berhasil ditambahkan';
        $message .= ' dan ' . $migrate['data_update'] . ' data berhasil dirubah';

        $data['status']                         = 'success';
        $data['message']                        = $message;
        $data['data']                           = $migrate;

        die(json_encode($data));
    }

    // =============================================================================================
    // Action Data Satuan Kerja
    // =============================================================================================

    function savebidang($id = '')
    {
        // This is for AJAX request
        if (!$this->input->is_ajax_request()) exit('No direct script access allowed');

        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            // Set JSON data
            $data = array(
                'status'        => 'access_denied',
                'login'         => 'login',
                'url'           => base_url('login'),
            );
            die(json_encode($data));
        }

        // Set data Return
        $bp_token       = $this->security->get_csrf_hash();
        $data           = array(
            'status'    => 'error',
            'token'     => $bp_token,
            'message'   => 'Simpan Bidang tidak berhasil.'
        );

        // -------------------------------------------------
        // Set Variable
        // -------------------------------------------------
        $current_member         = bp_get_current_member();
        $is_admin               = as_administrator($current_member);

        $reg_name               = $this->input->post('reg_name');
        $reg_name               = trim(bp_isset($reg_name, '', '', false, false));
        $reg_all_skpd           = $this->input->post('reg_all_skpd');
        $reg_all_skpd           = trim(bp_isset($reg_all_skpd, '', '', true));

        // Check Available Urusan if ID not null
        // -------------------------------------------------
        $databidang             = '';
        if ($id) {
            $id = bp_decrypt($id);

            // Check Data Urusan
            // -------------------------------------------------
            if (!$databidang = bp_bidang($id)) {
                $data['message'] = 'Simpan Bidang tidak berhasil. Data Bidang Tidak Ditemukan';
                die(json_encode($data)); // Set JSON data
            }
        }

        // -------------------------------------------------
        // Check Form Validation
        // -------------------------------------------------

        $this->form_validation->set_rules('reg_name', 'Nama', 'required');
        if (!$databidang) {
            $this->form_validation->set_rules('reg_name', 'Nama', 'is_unique[bidang.nama]');
        }
        $this->form_validation->set_message('required', '%s harus di isi');
        $this->form_validation->set_message('is_unique', '%s Harus Unique');
        $this->form_validation->set_error_delimiters('* ', br());

        if ($this->form_validation->run() == FALSE) {

            $data['message'] = 'Simpan Bidang tidak berhasil. ' . br() . validation_errors();
            die(json_encode($data)); // Set JSON data
        }

        // -------------------------------------------------
        // Begin Transaction
        // -------------------------------------------------
        $this->db->trans_begin();

        // Set Data Urusan
        // -------------------------------------------------

        $data_save = array(
            'nama'      => $reg_name,
            'all_skpd'  => $reg_all_skpd
        );

        if ($databidang) {
            if (!$id_update_bidang = $this->Model_Master->update_data_bidang($databidang->id, $data_save)) {
                $data['message'] = 'Simpan Bidang tidak berhasil. Terjadi kesalahan sistem';
                die(json_encode($data)); // Set JSON data
            }
        } else {
            if (!$id_save_bidang = $this->Model_Master->save_bidang($data_save)) {
                $data['message'] = 'Simpan Bidang tidak berhasil. Terjadi kesalahan sistem';
                die(json_encode($data)); // Set JSON data
            }
        }


        // -------------------------------------------------
        // Commit or Rollback Transaction
        // -------------------------------------------------
        if ($this->db->trans_status() === FALSE) {
            // Rollback Transaction
            $this->db->trans_rollback();
            $data['message'] = 'Simpan Data Bidang tidak berhasil. Terjadi kesalahan data transaksi.';
            die(json_encode($data)); // Set JSON data
        } else {
            // Commit Transaction
            $this->db->trans_commit();
            // Complete Transaction
            $this->db->trans_complete();
            bp_log_action('ACTION_SAVE_BIDANG', $reg_name, $current_member->username, json_encode(array('cookie' => $_COOKIE, 'status' => 'SUCCESS', 'name' => $reg_name, 'all_skpd' => $reg_all_skpd)));

            $data           = array(
                'status'    => 'success',
                'token'     => $bp_token,
                'data_save'     => $data_save,
                'message'   => 'Simpan Data Bidang Berhasil'
            );
            die(json_encode($data));
        }
    }

    function saveskpd($id = '')
    {
        // This is for AJAX request
        if (!$this->input->is_ajax_request()) exit('No direct script access allowed');

        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            // Set JSON data
            $data = array(
                'status'        => 'access_denied',
                'login'         => 'login',
                'url'           => base_url('login'),
            );
            die(json_encode($data));
        }

        // Set data Return
        $bp_token       = $this->security->get_csrf_hash();
        $data           = array(
            'status'    => 'error',
            'token'     => $bp_token,
            'message'   => 'Simpan SKPD tidak berhasil.'
        );

        // -------------------------------------------------
        // Set Variable
        // -------------------------------------------------
        $current_member         = bp_get_current_member();
        $is_admin               = as_administrator($current_member);

        $reg_code               = $this->input->post('reg_code');
        $reg_code               = trim(bp_isset($reg_code, '', '', false, false));
        $reg_name               = $this->input->post('reg_name');
        $reg_name               = trim(bp_isset($reg_name, '', '', false, false));
        $reg_u1                 = $this->input->post('reg_u1');
        $reg_u1                 = trim(bp_isset($reg_u1, '', '', false, false));
        $reg_u2                 = $this->input->post('reg_u2');
        $reg_u2                 = trim(bp_isset($reg_u2, '', '', false, false));
        $reg_u3                 = $this->input->post('reg_u3');
        $reg_u3                 = trim(bp_isset($reg_u3, '', '', false, false));
        $reg_urut               = $this->input->post('reg_urut');
        $reg_urut               = trim(bp_isset($reg_urut, '', '', false, false));
        $reg_unit               = $this->input->post('reg_unit');
        $reg_unit               = trim(bp_isset($reg_unit, '', '', false, false));
        $reg_bidang             = $this->input->post('reg_bidang');
        $reg_bidang             = trim(bp_isset($reg_bidang, '', '', false, false));
        $reg_bidang             = $reg_bidang ? bp_decrypt($reg_bidang) : '';

        $reg_nip                = $this->input->post('reg_nip');
        $reg_nip                = trim(bp_isset($reg_nip, '', '', false, false));
        $reg_head               = $this->input->post('reg_head');
        $reg_head               = trim(bp_isset($reg_head, '', '', false, false));
        $reg_status_head        = $this->input->post('reg_status_head');
        $reg_status_head        = trim(bp_isset($reg_status_head, '', '', false, false));
        $reg_is_unit            = $this->input->post('reg_is_unit');
        $reg_is_unit            = trim(bp_isset($reg_is_unit, '', '', false, false));
        $reg_skpd               = $this->input->post('reg_skpd');
        $reg_skpd               = trim(bp_isset($reg_skpd, '', '', false, false));


        if (!$reg_u2) {
            $reg_u2             = '0.00';
        }

        if (!$reg_u3) {
            $reg_u3             = '0.00';
        }

        $reg_unit               = $reg_unit ? str_pad($reg_unit, 4, '0', STR_PAD_LEFT) : '0000';

        // Check Available Urusan if ID not null
        // -------------------------------------------------
        $dataskpd               = '';
        if ($id) {
            $id = bp_decrypt($id);

            // Check Data Urusan
            // -------------------------------------------------
            if (!$dataskpd = bp_skpd($id)) {
                $data['message'] = 'Simpan SKPD tidak berhasil. Data SKPD Tidak Ditemukan';
                die(json_encode($data)); // Set JSON data
            }
        }

        // -------------------------------------------------
        // Check Form Validation
        // -------------------------------------------------

        $this->form_validation->set_rules('reg_code', 'Kode', 'required');
        if (!$dataskpd) {
            $this->form_validation->set_rules('reg_code', 'Kode', 'is_unique[skpd.kode]');
        }

        $this->form_validation->set_rules('reg_name', 'Nama', 'required');
        if (!$dataskpd) {
            $this->form_validation->set_rules('reg_name', 'Nama', 'is_unique[skpd.nama]');
        }

        $this->form_validation->set_message('required', '%s harus di isi');
        $this->form_validation->set_message('is_unique', '%s Harus Unique');
        $this->form_validation->set_error_delimiters('* ', br());

        if ($this->form_validation->run() == FALSE) {

            $data['message'] = 'Simpan SKPD tidak berhasil. ' . br() . validation_errors();
            die(json_encode($data)); // Set JSON data
        }

        // Check Data Bidang
        // -------------------------------------------------
        if (!$databidang = bp_bidang($reg_bidang)) {
            $data['message'] = 'Simpan SKPD tidak berhasil. Data Bidang Tidak ditemukan!';
            die(json_encode($data)); // Set JSON data
        }

        $dataskpdunit           = '';
        if ($reg_skpd) {
            $reg_skpd       = bp_decrypt($reg_skpd);
            if (!$dataskpdunit = bp_skpd($reg_skpd)) {
                $data['message'] = 'Simpan SKPD tidak berhasil. Data SKPD Tidak ditemukan!';
                die(json_encode($data)); // Set JSON data
            }
        }


        // -------------------------------------------------
        // Begin Transaction
        // -------------------------------------------------
        $this->db->trans_begin();

        // Set Data Urusan
        // -------------------------------------------------

        $data_save = array(
            'kode'              => $reg_code,
            'nama'              => $reg_name,
            'u1'                => $reg_u1,
            'u2'                => $reg_u2,
            'u3'                => $reg_u3,
            'urut'              => $reg_urut,
            'unit'              => $reg_unit,
            'id_bidang'         => $databidang->id,
            'nama_kepala'       => $reg_head,
            'nip'               => $reg_nip,
            'status_kepala'     => $reg_status_head,
            'type'              => $reg_is_unit ? 1 : 0,
            'id_skpd'           => $dataskpdunit ? $dataskpdunit->id : ''
        );

        if ($dataskpd) {
            if (!$id_update_skpd = $this->Model_Master->update_data_skpd($dataskpd->id, $data_save)) {
                $data['message'] = 'Simpan SKPD tidak berhasil. Terjadi kesalahan sistem';
                die(json_encode($data)); // Set JSON data
            }
        } else {
            if (!$id_save_skpd = $this->Model_Master->save_skpd($data_save)) {
                $data['message'] = 'Simpan SKPD tidak berhasil. Terjadi kesalahan sistem';
                die(json_encode($data)); // Set JSON data
            }
        }


        // -------------------------------------------------
        // Commit or Rollback Transaction
        // -------------------------------------------------
        if ($this->db->trans_status() === FALSE) {
            // Rollback Transaction
            $this->db->trans_rollback();
            $data['message'] = 'Simpan Data SKPD tidak berhasil. Terjadi kesalahan data transaksi.';
            die(json_encode($data)); // Set JSON data
        } else {
            // Commit Transaction
            $this->db->trans_commit();
            // Complete Transaction
            $this->db->trans_complete();
            bp_log_action('ACTION_SAVE_SKPD', $reg_name, $current_member->username, json_encode(array('cookie' => $_COOKIE, 'status' => 'SUCCESS', 'name' => $reg_name)));

            $data           = array(
                'status'    => 'success',
                'token'     => $bp_token,
                'data_save' => $data_save,
                'message'   => 'Simpan Data SKPD Berhasil'
            );
            die(json_encode($data));
        }
    }

    function deletebidang($id = '')
    {
        if (!$this->input->is_ajax_request()) {
            redirect(base_url('satuankerja/bidang'), 'refresh');
        }
        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            $data = array('status' => 'access_denied', 'url' => base_url('login'));
            die(json_encode($data)); // JSON encode data
        }

        if (!$id) {
            $data = array('status' => 'error', 'message' => 'ID Bidang tidak ditemukan !');
            die(json_encode($data));
        }

        $id = bp_decrypt($id);
        if (!$data_bidang = bp_bidang($id)) {
            $data = array('status' => 'error', 'message' => 'Data Bidang tidak ditemukan !');
            die(json_encode($data));
        }

        if (!$delete_data = $this->Model_Master->delete_data_bidang($id)) {
            $data = array('status' => 'error', 'message' => 'Bidang tidak berhasil dihapus !');
            die(json_encode($data));
        }

        // Save Success
        $data = array('status' => 'success', 'message' => 'Bidang berhasil dihapus.');
        die(json_encode($data));
    }

    function deleteskpd($id = '')
    {
        if (!$this->input->is_ajax_request()) {
            redirect(base_url('satuankerja/skpd'), 'refresh');
        }
        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            $data = array('status' => 'access_denied', 'url' => base_url('login'));
            die(json_encode($data)); // JSON encode data
        }

        if (!$id) {
            $data = array('status' => 'error', 'message' => 'ID SKPD tidak ditemukan !');
            die(json_encode($data));
        }

        $id = bp_decrypt($id);
        if (!$data_skpd = bp_skpd($id)) {
            $data = array('status' => 'error', 'message' => 'Data SKPD tidak ditemukan !');
            die(json_encode($data));
        }

        if (!$delete_data = $this->Model_Master->delete_data_skpd($id)) {
            $data = array('status' => 'error', 'message' => 'SKPD tidak berhasil dihapus !');
            die(json_encode($data));
        }

        // Save Success
        $data = array('status' => 'success', 'message' => 'Bidang berhasil dihapus.');
        die(json_encode($data));
    }

    function importskpd()
    {
        if (!$this->input->is_ajax_request()) {
            redirect(base_url('satuankerja/skpd'), 'refresh');
        }

        $auth = auth_redirect($this->input->is_ajax_request());

        if (!$auth) {
            $data = array('status' => 'access_denied', 'url' => base_url('login'));
            die(json_encode($data)); // JSON encode data
        }

        // Set data Return
        $bp_token       = $this->security->get_csrf_hash();

        $data           = array(
            'status'    => 'error',
            'token'     => $bp_token,
            'message'   => 'Import Data SKPD tidak berhasil.'
        );

        // -------------------------------------------------
        // Set Variable
        // -------------------------------------------------
        $current_member         = bp_get_current_member();
        $is_admin               = as_administrator($current_member);

        if (!$is_admin) {
            $data['message'] = 'Import Data SKPD tidak berhasil. Hanya admin yang dapat melakukan aksi ini.';
        }

        $img_name                   = random_string() . '-' . time();

        $config['upload_path']      = DATA_IMPORT_PATH;
        $config['allowed_types']    = 'xlsx';
        $config['max_size']         = '1048';
        $config['overwrite']        = true;
        $config['file_name']        = $img_name;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        $img_upload                 = true;
        $img_msg                    = '';
        if (!$this->upload->do_upload("reg_file")) {
            $img_upload             = false;
            $img_msg                = $this->upload->display_errors();
        }

        if (!$img_upload && $img_msg) {
            $data['message'] = 'Import Data Gagal. ' . $img_msg;
            die(json_encode($data)); // Set JSON data
        }

        $get_data_import       = $this->upload->data();

        if (!$get_data_import) {
            $data['message'] = 'Import Data Gagal. Terjadi kesalahan Sistem';
            die(json_encode($data)); // Set JSON data
        }

        $filedata                       = IMPORT_PATH . $img_name . '.xlsx';

        if (!file_exists($filedata)) {
            $data['status']             = 'error';
            $data['message']            = 'Gagal Import Data SKPD, File gagal diupload';
            $data['data']               = null;
            die(json_encode($data));
        }

        $migrate                        = bp_import_skpd($filedata, SKPD, TRUE);

        unlink($filedata);

        if ($migrate['status'] == 'error') {
            $data['status']             = 'error';
            $data['message']            = 'Gagal Import Data SKPD, ' . $migrate['message'];
            $data['data']               = null;
            die(json_encode($data));
        }

        $message = 'Import Data SKPD Berhasil.';
        $message .= ' ' . $migrate['data_insert'] . ' data berhasil ditambahkan';
        $message .= ' dan ' . $migrate['data_update'] . ' data berhasil dirubah';

        $data['status']                         = 'success';
        $data['message']                        = $message;
        $data['data']                           = $migrate;

        die(json_encode($data));
    }

    // ------------------------------------------------------------------------------------------------
}

/* End of file Member.php */
/* Location: ./application/controllers/Member.php */
