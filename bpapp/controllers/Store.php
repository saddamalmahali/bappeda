<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Store extends BP_Controller
{

    /**
     * Constructor.
     */
    function __construct()
    {
        parent::__construct();
        $this->load->helper('shop_helper');
        $auth = auth_redirect( true );
        if( $auth ){
            $current_member     = bp_get_current_member();
            $is_admin           = $current_member ? as_administrator($current_member) : false;
            $redirect           = $is_admin ? base_url('report/sales') : base_url('shopping/shoplist');
            redirect( $redirect, 'location' );
        }
    }

    /**
     * Indes Store Page
     */
    public function index()
    {
        $loadscripts            = bp_scripts(array(
        ));
        $scripts_init           = bp_scripts_init(array(
            'StoreProduct.init();'
        ));

        // Get Search
        $s_product              = $this->input->get('search');
        $s_product              = bp_isset($s_product, '', '', true);
        $s_category             = $this->input->get('category');
        $s_category             = bp_isset($s_category, '', '', true);
        $s_sortby               = $this->input->get('sortby');
        $s_sortby               = bp_isset($s_sortby, '', '', true);

        $data['title']          = TITLE . 'Produk';
        $data['scripts']        = $loadscripts;
        $data['scripts_init']   = $scripts_init;
        $data['s_product']      = $s_product;
        $data['s_category']     = $s_category;
        $data['s_sortby']       = $s_sortby;
        $data['cart_content']   = bp_cart_customer_contents();
        $data['main_content']   = 'shop';
        $this->load->view(VIEW_STORE . 'template', $data);
    }

    /**
     * Cart Store Page
     */
    public function cart()
    {
        $loadscripts            = bp_scripts(array(
        ));
        $scripts_init           = bp_scripts_init(array(
            'StoreCart.init();'
        ));

        $data['title']          = TITLE . 'Keranjang Belanja';
        $data['scripts']        = $loadscripts;
        $data['scripts_init']   = $scripts_init;
        $data['s_product']      = '';
        $data['s_category']     = '';
        $data['s_sortby']       = '';
        $data['currency']       = config_item('currency');
        $data['cart_content']   = bp_cart_customer_contents();
        $data['main_content']   = 'cart';
        $this->load->view(VIEW_STORE . 'template', $data);
    }

    /**
     * Checkout Store Page
     */
    public function checkout()
    {
        $loadscripts            = bp_scripts(array(
            BE_PLUGIN_PATH . 'jquery-validation/dist/jquery.validate.min.js?ver=' . JS_VER_MAIN,
        ));
        $scripts_init           = bp_scripts_init(array(
            'StoreCheckout.init();'
        ));

        $cart_content           = bp_cart_customer_contents();
        if ( !$cart_content ) {
            redirect(base_url('store/cart'), 'refresh');
        }

        if ( $cart_content['has_error'] ) {
            redirect(base_url('store/cart'), 'refresh');
        }

        $data_cart_content      = isset($cart_content['data']) ? $cart_content['data'] : false;
        $total_payment          = $this->cart->total();

        $data['title']          = TITLE . 'Checkout';
        $data['scripts']        = $loadscripts;
        $data['scripts_init']   = $scripts_init;
        $data['s_product']      = '';
        $data['s_category']     = '';
        $data['s_sortby']       = '';
        $data['currency']       = config_item('currency');
        $data['cart_content']   = $cart_content;
        $data['cart_product']   = $data_cart_content;
        $data['total_payment']  = $total_payment;
        $data['main_content']   = 'checkout';
        $this->load->view(VIEW_STORE . 'template', $data);
    }

    /**
     * Search Order Store Page
     */
    public function searchorder()
    {
        $shop_order             = false;
        $loadscripts            = bp_scripts(array());
        $scripts_init           = bp_scripts_init(array());

        // Get Search Get
        $invoice                = $this->input->get('invoice');
        $invoice                = bp_isset($invoice, '', '', true);
        if ( $invoice ) {
            $shop_order         = $this->Model_Shop->get_shop_customer_order_by('invoice', $invoice);
        }

        $data['title']          = TITLE . 'Keranjang Belanja';
        $data['scripts']        = $loadscripts;
        $data['scripts_init']   = $scripts_init;
        $data['s_product']      = '';
        $data['s_category']     = '';
        $data['s_sortby']       = '';
        $data['invoice']        = $invoice;
        $data['shop_order']     = $shop_order;
        $data['currency']       = config_item('currency');
        $data['cart_content']   = bp_cart_customer_contents();
        $data['main_content']   = 'shoplist';
        $this->load->view(VIEW_STORE . 'template', $data);
    }

    /**
     * Detail Order Store Page
     */
    public function detailorder($id = 0)
    {
        $id                     = $id ? bp_decrypt($id) : 0;
        if ( !$id ) {
            redirect( base_url('store/searchorder'), 'location' );
        }

        $shop_order             = false;
        $loadscripts            = bp_scripts(array());
        $scripts_init           = bp_scripts_init(array());

        // Get Shop Order
        $shop_order             = $this->Model_Shop->get_shop_customer_order_by('id', $id);

        $data['title']          = TITLE . 'Keranjang Belanja';
        $data['scripts']        = $loadscripts;
        $data['scripts_init']   = $scripts_init;
        $data['s_product']      = '';
        $data['s_category']     = '';
        $data['s_sortby']       = '';
        $data['shop_order']     = $shop_order;
        $data['currency']       = config_item('currency');
        $data['cart_content']   = bp_cart_customer_contents();
        $data['main_content']   = 'shopdetail';
        $this->load->view(VIEW_STORE . 'template', $data);
    }

    /**
     * Invoice Page function.
     */
    public function invoice($id = '')
    {
        $id                     = $id ? bp_decrypt($id) : 0;
        if ( !$id ) {
            redirect( base_url('store/searchorder'), 'location' );
        }

        $shop_order             = false;
        $invoice                = '';
        $loadscripts            = bp_scripts(array());
        $scripts_init           = bp_scripts_init(array());

        // Get Shop Order
        $shop_order             = $this->Model_Shop->get_shop_customer_order_by('id', $id);
        if ( !$shop_order ) {
            redirect( base_url('store/searchorder'), 'location' );
        }

        $invoice                = $shop_order->invoice;
        $data['title']          = TITLE . 'Invoice ' . $invoice;
        $data['scripts']        = $loadscripts;
        $data['scripts_init']   = $scripts_init;
        $data['s_product']      = '';
        $data['s_category']     = '';
        $data['s_sortby']       = '';
        $data['shop_order']     = $shop_order;
        $data['main_content']   = bp_notification_shop_customer_template($shop_order, '', false);
        $this->load->view(VIEW_STORE . 'invoice', $data);
    }

    // =============================================================================================
    // LIST PRODUCT
    // =============================================================================================

    /**
     * Product Shop List Data function.
     */
    function productlistdata($type = '')
    {
        // This is for AJAX request
        if ( ! $this->input->is_ajax_request() ) exit('No direct script access allowed');

        $type               = $type ? bp_decrypt($type) : '';

        // Get Search POST
        $s_product          = $this->input->post('search');
        $s_product          = bp_isset($s_product, '', '', true);
        $s_category         = $this->input->post('category');
        $s_category         = bp_isset($s_category, '', '', true);
        $s_category         = ( strtolower($s_category) == 'all' ) ? '' : $s_category;
        $s_limit            = $this->input->post('limit');
        $s_limit            = bp_isset($s_limit, 12, 12, true);
        $s_offset           = $this->input->post('offset');
        $s_offset           = bp_isset($s_offset, 0, 0, true);
        $s_sortby           = $this->input->post('sortby');
        $s_sortby           = bp_isset($s_sortby, '', '', true);

        $condition          = ' AND %status% = 1';
        $order_by           = '';
        $params             = array();

        $iTotalRecords      = 0;
        $iDisplayLength     = $s_limit;
        $iDisplayStart      = $s_offset;

        if ( $s_sortby ) {
            if ( strtolower($s_sortby) ==  'dateupdate')    { $order_by = '%dateupdated% DESC'; }
            if ( strtolower($s_sortby) ==  'name_asc')      { $order_by = '%product% ASC'; }
            if ( strtolower($s_sortby) ==  'name_desc')     { $order_by = '%product% DESC'; }
            if ( strtolower($s_sortby) ==  'price_asc')     { $order_by = 'price_customer ASC'; }
            if ( strtolower($s_sortby) ==  'price_desc')    { $order_by = 'price_customer DESC'; }
        }

        if( !empty($s_product) )    { $condition .= ' AND %product% LIKE CONCAT("%", ?, "%") '; $params[] = $s_product; }
        if( !empty($s_category) )   { $condition .= ' AND %slug_category% = ?'; $params[] = $s_category; }

        $get_products   = ($type == 'get') ? $this->Model_Product->get_all_product($s_limit, $s_offset, $condition, $order_by, $params) : false;

        if ( $get_products ) { $iTotalRecords  = bp_get_last_found_rows(); }

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        if ( $iDisplayLength > $end ) {
            $iTotalRecords = $end;
        }

        $data["data"]                   = $get_products;
        $data["type"]                   = $type;
        $data["displayLimit"]           = $iDisplayLength;
        $data["displayStart"]           = $end;
        $data["totalRecords"]           = $iTotalRecords;
        $data["totalDisplayRecords"]    = (int) $end;
        $data["token"]                  = $this->security->get_csrf_hash();
        $data["displayHTML"]            = $this->load->view(VIEW_STORE . 'productlist', $data, true);
        unset($data["data"]);

        echo json_encode($data);
        die();
    }

    /**
     * Get Product Detail Function
     */
    function getproductdetail( $id = 0 ){
        if ( ! $this->input->is_ajax_request() ) { redirect(base_url('store'), 'refresh'); }

        if( !$id ){
            $data = array('status' => 'error', 'message' => 'Produk tidak ditemukan !');
            die(json_encode($data));
        }

        $id         = bp_decrypt($id);
        if ( ! $productdata = bp_products($id) ) {
            $data = array('status' => 'error', 'message' => 'Produk tidak ditemukan !');
            die(json_encode($data));
        }

        $set_html       = $this->sethtmlproductdetailstore($productdata);
        $data = array('status'=>'success', 'message'=>'Produk Detail', 'data'=>$set_html );
        die(json_encode($data));
    }

    /**
     * Set HTML Product Detail function.
     */
    private function sethtmlproductdetailstore($productdata = 0){
        $product_detail     = '';
        if ( !$productdata ) { return $product_detail; }

        $currency           = config_item('currency');
        $shopping_lock      = config_item('shopping_lock');
        $product_id         = bp_encrypt($productdata->id);

        // --------------------------------------
        // Get Cart Content
        // --------------------------------------
        $cart_content       = bp_cart_customer_contents();
        $product_cart       = isset($cart_content['data']) ? $cart_content['data'] : array();
        $in_cart            = false;
        if ( $product_cart ) {
            foreach ($product_cart as $item) {
                $cart_product_id = isset($item['id']) ? $item['id'] : 'none';
                if ( $cart_product_id == $productdata->id ) {
                    $in_cart = true;
                }
            }
        }   

        $btn_addtocart      = '';
        if ( !$shopping_lock ) {
            $btn_addtocart  = '<a class="btn btn-default add-to-cart text-uppercase" 
                                href="'. base_url('store/addToCart') .'" 
                                data-type="addcart" 
                                data-qty="qty" 
                                data-cart="'. $product_id .'">
                                <span class="shopping-cart-loading mr-2" style="display:none"><img src="'. BE_IMG_PATH .'loading-spinner-blue.gif"></span>
                                ADD TO CART</a>';
            if ( $in_cart ) {
                $btn_addtocart = '<a class="btn btn-default add-to-cart text-uppercase" href="'. base_url('store/cart') .'" data-type="cart">GO TO CART</a>';
            }
        }

        $img_src            = bp_product_image($productdata->image, false); 
        $category           = '';
        if ( $productdata->id_category ) {
            $categorydata   = bp_product_category($productdata->id_category);
            $category       = isset($categorydata->name) ? $categorydata->name : '';
            if ( $category ) {
                $category   = '<span><i class="fa fa-tag"></i> '. $category .'</span>';
            }
        }

        $product_price      = $productdata->price_customer;
        $view_price         = bp_accounting($product_price, $currency);

        $product_detail     = '
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                <!-- Product Slider -->
                    <div class="product-gallery">
                        <div class="quickview-slider-active">
                            <div class="single-slider">
                                <img src="'. $img_src .'" alt="#">
                            </div>
                            <div class="single-slider">
                                <img src="'. $img_src .'" alt="#">
                            </div>
                        </div>
                    </div>
                <!-- End Product slider -->
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                <div class="quickview-content">
                    <h2>'. ucwords(strtolower($productdata->name)) .'</h2>
                    <div class="quickview-ratting-review">
                        <!-- 
                        <div class="quickview-ratting-wrap">
                            <div class="quickview-ratting">
                                <i class="yellow fa fa-star"></i>
                                <i class="yellow fa fa-star"></i>
                                <i class="yellow fa fa-star"></i>
                                <i class="yellow fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </div>
                            <a href="#"> (1 customer review)</a>
                        </div>
                        -->
                        '. $category .'
                        <div class="quickview-stock">
                            <span><i class="fa fa-check-circle-o"></i> in stock</span>
                        </div>
                        <small class="ml-3 text-success">'. bp_accounting($productdata->bv) .' Bv</small>
                    </div>
                    <h3 class="text-success">'. $view_price .'</h3>
                    <div class="quickview-peragraph mb-4">
                        '. $productdata->description .'
                    </div>
                    '. ( ( !$in_cart )  ? '
                    <div class="quantity modal-detail-qty">
                        <!-- Input Order -->
                        <div class="input-group">
                            <div class="button minus">
                                <button type="button" class="btn btn-primary btn-number" disabled="disabled" data-type="minus" data-field="qty">
                                    <i class="ti-minus"></i>
                                </button>
                            </div>
                            <input type="text" name="qty" class="input-number"  data-min="1" data-max="1000" value="1">
                            <div class="button plus">
                                <button type="button" class="btn btn-primary btn-number" data-type="plus" data-field="qty">
                                    <i class="ti-plus"></i>
                                </button>
                            </div>
                        </div>
                        <!--/ End Input Order -->
                    </div>
                    ' : '' ) .'
                    <div class="add-to-cart">
                        '. $btn_addtocart .'
                    </div>
                    <div class="default-social">
                        <h4 class="share-now">Share:</h4>
                        <ul>
                            <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a class="instagram" href="#"><i class="fa fa-instagram"></i></a></li>
                            <li><a class="google-plus" href="#"><i class="fa fa-google-plus"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        ';

        // $product_detail     = '
        //     <div class="row">
        //         <div class="col-xl-5 order-xl-1 bg-secondary py-2">
        //             <a href="'.$img_src.'" target="_blank">
        //                 <div class="thumbnail mb-1">
        //                     <img class="img-thumbnail" width="100%" src="'.$img_src.'" style="cursor: pointer;">
        //                 </div>
        //             </a>
        //         </div>
        //         <div class="col-xl-7 order-xl-2 pt-3">
        //             <div class="card">
        //                 <div class="card-header border-0">
        //                     <div class="row align-items-center">
        //                         <div class="col">
        //                             <h4 class="mb-0 text-capitalize text-capitalize">'.$productdata->name.'</h4>
        //                             '. $category .'
        //                         </div>
        //                     </div>
        //                 </div>
        //                 <div class="card-body pt-0 pb-3">
        //                     <h2 class="h2 text-warning font-weight-bold">'. $view_price .'</h2>
        //                     <hr class="my-3">
        //                     '. $btn_addtocart .'
        //                 </div>
        //             </div>      
        //         </div>
        //     </div>
        //     <div class="row">
        //         <div class="col">
        //             <div class="card">
        //                 <div class="card-header border-0">
        //                     <div class="row align-items-center">
        //                         <div class="col">
        //                             <h3 class="mb-0 text-capitalize text-capitalize">'. lang('information') .'</h3>
        //                         </div>
        //                     </div>
        //                 </div>
        //                 <div class="card-body py-2 pb-3">
        //                     '. $productdata->description .'
        //                 </div>
        //             </div>  
        //         </div>
        //     </div>
        // ';
        return $product_detail;
    }

    // =============================================================================================
    // CART
    // =============================================================================================

    /**
     * Add To Cart
     */
    public function addToCart()
    {
        // This is for AJAX request
        if ( ! $this->input->is_ajax_request() ) exit('No direct script access allowed');

        $token          = $this->security->get_csrf_hash();
        $data           = array(
            'status'    => 'error',
            'token'     => $token,
            'message'   => 'Added to cart Failed!'
        );

        // Get Form POST
        $id             = $this->input->post('id');
        $id             = bp_isset($id, '', '', true);
        $id             = bp_decrypt($id);
        $qty            = $this->input->post('qty');
        $qty            = bp_isset($qty, '', '', true);
        $qty            = is_numeric($qty) ? $qty : 1;

        if ( !$qty || $qty == 0 ) {
            $data['message'] = 'Added to cart Failed! (Qty cannot zero)';
            die(json_encode($data));
        }

        $productdata    = bp_products($id, true);
        if ( !$productdata ) {
            $data['message'] = 'Added to cart Failed! (product not found)';
            die(json_encode($data));
        }

        $product_title  = $productdata->name;
        $product_slug   = url_title($product_title, 'dash', TRUE);

        // Check Price
        $price          = $productdata->price_customer;

        // Check Weight
        $weight         = $productdata->weight;
        $total_weight   = $weight * $qty;
        $product_weight = $total_weight;
        $product_type   = 'shop';
        $img_src        = bp_product_image($productdata->image); 

        $html_product   = '
            <li>
                <a class="cart-img" href="javascript:;"><img src="'. $img_src .'" alt="#"></a>
                <h4><a href="javascript:;">'. $product_title .'</a></h4>
                <p><span class="amount">'. bp_accounting($price) .'</span></p>
                <p class="quantity">Qty : '. $qty .'</p>
            </li>';

        // Check Type Product On Cart
        $cart_contents  = $this->cart->contents();
        $html_menu      = '';
        if ( $cart_contents ) {
            foreach ($cart_contents as $item) {
                $_id    = isset($item['id']) ? $item['id'] : 'none';
                $_type  = isset($item['type']) ? $item['type'] : 'none';
            }
            $html_menu  = $html_product;
        } else {
            $html_menu  = '
                <div class="shopping-item">
                    <div class="dropdown-cart-header">
                        <span>Produk</span>
                        <a href="#">1 Items</a>
                    </div>
                    <ul class="shopping-list">
                        '.$html_product.'
                    </ul>
                    <div class="bottom">
                        <div class="total">
                            <span>Total</span>
                            <span class="total-amount-menu">'. bp_accounting(($price * $qty)) .'</span>
                        </div>
                        <a href="'. base_url('store/cart') .'" class="btn animate">Keranjang Belanja</a>
                    </div>
                </div>';
        }

        // Set Data Product Add To Cart
        $data_cart      = array(
            'id'        => $productdata->id,
            'type'      => $product_type,
            'name'      => $product_slug,
            'title'     => $product_title,
            'qty'       => $qty,
            'price'     => $price,
            'subtotal'  => ($price * $qty),
            'options'   => array(
                'weight' => $total_weight,
                'product_weight' => $product_weight,
            )
        );
        $insert_cart    = $this->cart->insert($data_cart);

        if ( $insert_cart ) {

            $total_payment          = $this->cart->total();
            $view_price             = bp_accounting($price);
            $view_subtotal          = bp_accounting(($price * $qty));
            $view_total             = bp_accounting($total_payment);

            $data['status']         = 'success';
            $data['message']        = 'Success Added to Cart!';
            $data['total_item']     = count($this->cart->contents());
            $data['total_qty']      = $this->cart->total_items();
            $data['price_cart']     = $view_price;
            $data['subtotal_cart']  = $view_subtotal;
            $data['total_cart']     = $view_total;
            $data['html_menu']      = $html_menu;
            $data['url_cart']       = base_url('store/cart');
        } else {
            $data['status']     = 'error';
            $data['message']    = 'Failed Added to Cart!';
        }
        // $data['data_cart']  = $data_cart;
        die(json_encode($data));
    }

    /**
     * Update Qty and checking stock availability
     */
    function updateQtyCart()
    {
        // This is for AJAX request
        if ( ! $this->input->is_ajax_request() ) exit('No direct script access allowed');

        $token          = $this->security->get_csrf_hash();
        $data           = array(
            'status'    => 'error',
            'token'     => $token,
            'message'   => 'Failed to Update Qty!'
        );

        $rowid          = $this->input->post('rowid');
        $rowid          = bp_isset($rowid, '', '', true);
        $product_id     = $this->input->post('productid');
        $product_id     = bp_isset($product_id, '', '', true);
        $product_id     = bp_decrypt($product_id);
        $qty            = $this->input->post('qty');
        $qty            = bp_isset($qty, '', '', true);

        $productdata    = bp_products($product_id);
        if ( !$productdata ) {
            $data['message'] = 'Product not found';
            die(json_encode($data));
        }

        // Check Price
        $price          = $productdata->price_customer;
        $subtotal       = $price * $qty;

        // Check Weight
        $weight         = $productdata->weight;
        $total_weight   = $weight * $qty;
        $product_weight = $total_weight;

        $data_cart      = array(
            'rowid'     => $rowid,
            'qty'       => $qty,
            'price'     => $price,
            'options'   => array(
                'weight'         => $product_weight,
                'product_weight' => $product_weight,
            )
        );

        // update cart
        $update_cart    = $this->cart->update($data_cart); 

        if ( $update_cart ) {
            $data['status']     = 'success';
            $data['message']    = 'Success Update Qty Cart!';
        } else {
            $data['status']     = 'error';
            $data['message']    = 'Failed Added to Cart!';
        }

        $total_payment          = $this->cart->total();
        $view_price             = bp_accounting($price);
        $view_subtotal          = bp_accounting($subtotal);
        $view_total             = bp_accounting($total_payment);

        $data['price_cart']     = $view_price;
        $data['subtotal_cart']  = $view_subtotal;
        $data['total_cart']     = $view_total;
        $data['total_qty']      = $this->cart->total_items();
        $data['total_item']     = count($this->cart->contents());

        die(json_encode($data));
    }

    /**
     * Delete Product Cart
     */
    public function deleteCart($rowid = '')
    {
        // This is for AJAX request
        if ( ! $this->input->is_ajax_request() ) exit('No direct script access allowed');

        $token          = $this->security->get_csrf_hash();
        $data           = array(
            'status'    => 'error',
            'token'     => $token,
            'message'   => 'Failed Delete Product Cart!'
        );

        // --------------------------------------
        // Get Cart Content
        // --------------------------------------
        $cart_content   = bp_cart_customer_contents();
        $product_cart   = isset($cart_content['data']) ? $cart_content['data'] : array();
        $product_type   = isset($cart_content['product_type']) ? $cart_content['product_type'] : '';

        // Get Input POST
        $id             = $this->input->post('rowid');
        $id             = bp_isset($id, '', '', true);
        $rowid          = $rowid ? $rowid : $id;

        $product_cart   = array(
            'rowid'     => $rowid,
            'qty'       => 0,
        );
        $delete_cart = $this->cart->update($product_cart);

        if ( $delete_cart ) {
            $data['status']     = 'success';
            $data['message']    = 'Success Delete Product Cart!';
        }

        $total_payment          = $this->cart->total();
        $cart_payment           = bp_accounting($total_payment);
        
        $data['total_cart']     = $cart_payment;
        $data['total_qty']      = $this->cart->total_items();
        $data['total_item']     = count($this->cart->contents());

        die(json_encode($data, true));
    }

    /**
     * Destroy Cart
     */
    public function emptyCart()
    {
        // This is for AJAX request
        if ( ! $this->input->is_ajax_request() ) exit('No direct script access allowed');

        // Empty Cart
        $this->cart->destroy();

        $token          = $this->security->get_csrf_hash();
        $data           = array(
            'status'    => 'success',
            'token'     => $token,
            'message'   => 'Cart Empty'
        ); die(json_encode($data));
    }

    // =============================================================================================
    // CHECKOUT
    // =============================================================================================

    /**
     * Process Checkout Cart
     */
    public function processcheckout()
    {
        // This is for AJAX request
        if ( ! $this->input->is_ajax_request() ) exit('No direct script access allowed');

        $token          = $this->security->get_csrf_hash();
        $data           = array(
            'status'    => 'error',
            'token'     => $token,
            'message'   => 'Checkout tidak berhasil. Silahkan periksa kembali keranjang belanjaan anda!'
        );

        $package        = MEMBER_BASIC;
        $datetime       = date('Y-m-d H:i:s');
        $dateexpired    = date('Y-m-d H:i:s', strtotime('+2 day'));
        $customer_id    = 0;
        $sponsor_id     = 0;

        // --------------------------------------
        // Get Cart Content
        // --------------------------------------
        $cart_content   = bp_cart_customer_contents();
        if ( ! $cart_content ) {
            die(json_encode($data));
        }

        if ( isset($cart_content['has_error']) && $cart_content['has_error'] ) {
            die(json_encode($data));
        }

        $product_cart   = isset($cart_content['data']) ? $cart_content['data'] : array();
        $product_type   = isset($cart_content['product_type']) ? $cart_content['product_type'] : '';

        if ( !$product_cart ) {
            die(json_encode($data));
        }

        // POST Input Form
        $sponsor                = trim( $this->input->post('sponsor') );
        $sponsor                = bp_isset($sponsor, '', '', true);
        $sponsor                = strtolower($sponsor);
        $username               = trim( $this->input->post('username') );
        $username               = bp_isset($username, '', '', true);
        $username               = strtolower($username);
        $password               = trim( $this->input->post('password') );
        $password               = bp_isset($password, '', '', true);
        $idcard                 = trim( $this->input->post('idcard') );
        $idcard                 = bp_isset($idcard, '', '', true);

        $options_reg            = trim( $this->input->post('options_reg') );
        $options_reg            = bp_isset($options_reg, '', '', true);
        $options_reg            = strtolower($options_reg);

        $name                   = trim( $this->input->post('name') );
        $name                   = bp_isset($name, '', '', true);
        $phone                  = trim( $this->input->post('phone') );
        $phone                  = bp_isset($phone, '', '', true);
        $email                  = trim( $this->input->post('email') );
        $email                  = bp_isset($email, '', '', true);
        $province               = trim( $this->input->post('province') );
        $province               = bp_isset($province, '', '', true);
        $district               = trim( $this->input->post('district') );
        $district               = bp_isset($district, '', '', true);
        $subdistrict            = trim( $this->input->post('subdistrict') );
        $subdistrict            = bp_isset($subdistrict, '', '', true);
        $village                = trim( $this->input->post('village') );
        $village                = bp_isset($village, '', '', true);
        $address                = trim( $this->input->post('address') );
        $address                = bp_isset($address, '', '', true);
        $courier                = trim( $this->input->post('courier') );
        $courier                = bp_isset($courier, '', '', true);
        $service                = trim( $this->input->post('service') );
        $service                = bp_isset($service, '', '', true);
        $courier_cost           = trim( $this->input->post('courier_cost') );
        $courier_cost           = bp_isset($courier_cost, 0, 0, true);
        $courier_cost           = $courier_cost ? $courier_cost : 0;

        $payment_method         = 'transfer'; 
        $shipping_method        = 'ekspedisi'; 
        $created_by             = $username;

        $this->form_validation->set_rules('name','Nama','required');
        $this->form_validation->set_rules('phone','No. Hp/WA','required');
        $this->form_validation->set_rules('email','Email','required');
        $this->form_validation->set_rules('province','Provinsi','required');
        $this->form_validation->set_rules('district','Kota/Kabupaten','required');
        $this->form_validation->set_rules('subdistrict','Kecamatan','required');
        $this->form_validation->set_rules('village','Kelurahan/Desa','required');
        $this->form_validation->set_rules('address','Alamat','required');
        $this->form_validation->set_rules('courier','Kurir','required');
        $this->form_validation->set_rules('service','Layanan Kurir','required');

        if ( $options_reg == 'member' ) {
            $this->form_validation->set_rules('sponsor','Referral','required');
            $this->form_validation->set_rules('username','Username','required');
            $this->form_validation->set_rules('password','Password','required');
            $this->form_validation->set_rules('idcard','No. KTP','required');
        }

        $this->form_validation->set_message('required', '%s harus di isi');
        $this->form_validation->set_error_delimiters('* ', br());

        if ( $this->form_validation->run() == FALSE ){
            $data['message'] = 'Checkout tidak berhasil. '. br() . validation_errors();
            die(json_encode($data));
        }

        if ( $phone ) { if ( substr($phone, 0, 1) != '0' ) { $phone = '0'. $phone; } }

        // -------------------------------------------------
        // IF Registration Member
        // -------------------------------------------------
        if ( $options_reg == 'member' ) {
            // -------------------------------------------------
            // Check Username Exsts
            // -------------------------------------------------
            $username_exist     = bp_check_username($username);
            if( $username_exist || !empty($username_exist) ){
                $data['message'] = 'Checkout tidak berhasil. Username tidak dapat digunakan. Silahkan gunakan Username lainnya!';
                die(json_encode($data)); // Set JSON data
            }

            // -------------------------------------------------
            // Check Sponsor
            // -------------------------------------------------
            $sponsordata        = $this->Model_Member->get_member_by('login', strtolower($sponsor));
            if (!$sponsordata) {
                $data['message'] = 'Checkout tidak berhasil. Referral tidak ditemukan. Silahkan masukkan username Referral lainnya!';
                die(json_encode($data)); // Set JSON data
            }
            if ($sponsordata->status != ACTIVE) {
                $data['message'] = 'Checkout tidak berhasil. Referral tidak ditemukan. Silahkan masukkan username Referral lainnya!';
                die(json_encode($data)); // Set JSON data
            }

            $sponsor_id         = $sponsordata->id;
            $sponsor_username   = $sponsordata->username;
        }

        // -------------------------------------------------
        // Check Province, District, SubDistrict
        // -------------------------------------------------
        $province_name  = '';
        if ($province && $get_province = bp_provinces($province)) {
            $province_name = $get_province->province_name;
        }

        // -------------------------------------------------
        // Check District Code
        // -------------------------------------------------
        $district_name  = '';
        if ($district && $get_district = bp_districts($district)) {
            $district_name = $get_district->district_type . ' ' . $get_district->district_name;
        }

        // -------------------------------------------------
        // Check SubDistrict
        // -------------------------------------------------
        $subdistrict_name  = '';
        if ($subdistrict && $get_subdistrict = bp_subdistricts($subdistrict)) {
            $subdistrict_name = $get_subdistrict->subdistrict_name;
        }
        
        if ( !$province_name  ) {
            $data['message'] = 'Provinsi tidak ditemukan atau belum terdaftar!';
            die(json_encode($data)); // Set JSON data
        }
        if ( !$district_name ) {
            $data['message'] = 'Kode Kota/Kabupaten tidak ditemukan atau belum terdaftar!';
            die(json_encode($data)); // Set JSON data
        }
        if ( !$subdistrict_name ) {
            $data['message'] = 'Kecamatan tidak ditemukan atau belum terdaftar!';
            die(json_encode($data)); // Set JSON data
        }

        // --------------------------------------
        // Product Cart
        // --------------------------------------
        $cart_total_qty     = $this->cart->total_items();
        $cart_total_payment = $this->cart->total();

        // Set Product
        $product_detail     = array();
        $total_bv           = 0; 
        $total_qty          = 0; 
        $total_price        = 0;
        $total_weight       = 0;
        $total_payment      = 0;
        foreach ($product_cart as $key => $row) {
            $_id            = isset($row['id']) ? $row['id'] : 0;
            $_qty           = isset($row['qty']) ? $row['qty'] : 0;
            $_price         = isset($row['cart_price']) ? $row['cart_price'] : 0;
            if ( !$_id || !$_qty || !$_price ) { continue; }
            if ( !$productdata = bp_products($_id) ) { continue; }

            $subtotal           = $_qty * $_price;
            $subtotal_bv        = $_qty * $productdata->bv;
            $product_weight     = $_qty * $productdata->weight;
            $product_price      = $productdata->price_customer;
            
            // Set Product Detail
            $product_detail[]   = array(
                'id'                => $_id,
                'name'              => $productdata->name,
                'bv'                => $productdata->bv,
                'qty'               => $_qty,
                'price'             => $product_price,      // original price
                'price_cart'        => $_price,             // cart price
                'discount'          => 0,
                'subtotal'          => $subtotal,
                'weight'            => $product_weight
            );

            $total_bv           += $subtotal_bv; 
            $total_qty          += $_qty; 
            $total_price        += $subtotal;
            $total_weight       += $product_weight;
        }

        if ( !$product_detail ) {
            die(json_encode($data));
        }

        if ( $cart_total_qty != $total_qty ) {
            die(json_encode($data));
        }

        if ( $cart_total_payment != $total_price ) {
            die(json_encode($data));
        }

        if ( $options_reg == 'member' ) {
            // --------------------------------------
            // Check Package Member
            // --------------------------------------
            if ( $packagedata = bp_packages() ) {
                $check_package  = false;
                $minimal_omzet  = 0;
                foreach ($packagedata as $key => $row) {
                    $minimal_omzet  = $row->omzet;
                    if ( $total_bv >= $minimal_omzet ) {
                        $package        = $row->package;
                        $check_package  = true;
                    }
                }

                if ( ! $check_package ) {
                    $data['message'] = 'Checkout tidak berhasil. Total Belanja minimal '. bp_accounting($minimal_omzet) .' Bv produk untuk pendaftaran member.';
                    die(json_encode($data));
                }
            }
        }

        // -------------------------------------------------
        // Transaction Begin
        // -------------------------------------------------
        $this->db->trans_begin();

        $invoice_prefix         = config_item('invoice_prefix');
        $code_unique            = bp_generate_shop_customer_order();
        $invoice_number         = str_pad($code_unique, 8, '0', STR_PAD_LEFT);;
        $invoice                = $invoice_prefix .'SHOP/'. $invoice_number;

        $total_payment          = $total_price + $code_unique + $courier_cost;
        $type_order             = 'customer_order';

        // Set Data Member
        // --------------------------------------
        if ( $options_reg == 'member' ) {
            $type_order             = 'member_order';
            $name                   = strtoupper($name);
            $bill_name              = $name;
            $password_bcript        = bp_password_hash($password);
            $position               = 'LR-'. bp_generate_rand_string(4) . $code_unique;

            $data_member            = array(
                'username'              => $username,
                'password'              => $password_bcript,
                'password_pin'          => bp_encrypt($password),
                'type'                  => MEMBER,
                'package'               => $package,
                'sponsor'               => $sponsor_id,
                'parent'                => 0,
                'position'              => $position,
                'name'                  => $name,
                'idcard_type'           => 'KTP',
                'idcard'                => $idcard,
                'npwp'                  => '',
                'country'               => 'IDN',
                'province'              => $province,
                'district'              => $district,
                'subdistrict'           => $subdistrict,
                'village'               => $village,
                'address'               => $address,
                'email'                 => strtolower(trim($email)),
                'phone'                 => $phone,
                'status'                => 0,
                'total_omzet'           => $total_bv,
                'uniquecode'            => $code_unique,
                'datecreated'           => $datetime,
            );

            $member_save_id         = $this->Model_Member->save_data($data_member);
            if ( !$member_save_id ) {
                $this->db->trans_rollback();
                $data['message'] = 'Checkout tidak berhasil. Terjadi kesalahan data pada transaksi pendaftaran member baru.';
                die(json_encode($data)); // Set JSON data
            }

            $downline               = bp_get_memberdata_by_id($member_save_id);
            if ( !$downline ) {
                $this->db->trans_rollback();
                $data['message'] = 'Checkout tidak berhasil. Terjadi kesalahan data pada transaksi pendaftaran member baru.';
                die(json_encode($data)); // Set JSON data
            }

            $customer_id            = $downline->id;
            $data_member_confirm    = array(
                'id_member'         => $downline->id,
                'member'            => $downline->username,
                'id_sponsor'        => $sponsordata->id,
                'sponsor'           => $sponsordata->username,
                'id_downline'       => $downline->id,
                'downline'          => $downline->username,
                'status'            => 0,
                'access'            => 'referral',
                'package'           => $package,
                'omzet'             => $total_price,
                'uniquecode'        => $code_unique,
                'nominal'           => $total_payment,
                'datecreated'       => $datetime,
                'datemodified'      => $datetime,
            );

            $member_confirm_save_id  = $this->Model_Member->save_data_confirm($data_member_confirm);
            if ( !$member_confirm_save_id ) {
                $this->db->trans_rollback();
                $data['message'] = 'Checkout tidak berhasil. Terjadi kesalahan data pada transaksi pendaftaran member baru.';
                die(json_encode($data)); // Set JSON data
            }
        }

        // Set Data Shop Order
        // --------------------------------------
        $data_shop_order        = array(
            'invoice'           => $invoice,
            'id_customer'       => $customer_id,
            'id_sponsor'        => $sponsor_id,
            'type_order'        => $type_order,
            'products'          => maybe_serialize($product_detail),
            'total_bv'          => $total_bv,
            'total_qty'         => $total_qty,
            'subtotal'          => $total_price,
            'unique'            => $code_unique,
            'shipping'          => $courier_cost,
            'discount'          => 0,
            'total_payment'     => $total_payment,
            'status'            => 0,
            'weight'            => $total_weight,
            'payment_method'    => $payment_method,
            'shipping_method'   => $shipping_method,
            'voucher'           => '',
            'name'              => $name,
            'phone'             => $phone,
            'email'             => $email,
            'id_province'       => $province,
            'id_district'       => $district,
            'id_subdistrict'    => $subdistrict,
            'province'          => $province_name,
            'district'          => $district_name,
            'subdistrict'       => $subdistrict_name,
            'village'           => $village,
            'address'           => $address,
            'courier'           => $courier,
            'service'           => $service,
            'datecreated'       => $datetime,
            'datemodified'      => $datetime,
            'dateexpired'       => $dateexpired,
            'created_by'        => $created_by,
        );

        // -------------------------------------------------
        // Save Shop Order
        // -------------------------------------------------
        $saved_shop_id = $this->Model_Shop->save_data_shop_customer_order($data_shop_order);
        if( ! $saved_shop_id ){
            $this->db->trans_rollback(); // Rollback Transaction
            $data['message'] = 'Checkout tidak berhasil. Terjadi kesalahan pada proses data transaksi.';
            die(json_encode($data));
        }

        // Set shop order detail
        $data_shop_detail       = array();
        foreach ($product_detail as $prodkey => $val) {
            $data_shop_detail[]     = array(
                'id_shop_order'     => $saved_shop_id,
                'id_customer'       => $customer_id,
                'product'           => $val['id'],
                'bv'                => $val['bv'],
                'qty'               => $val['qty'],
                'price'             => $val['price'],
                'price_cart'        => $val['price_cart'],
                'discount'          => $val['discount'],
                'subtotal'          => $val['subtotal'],
                'weight'            => $val['weight'],
                'datecreated'       => $datetime,
                'datemodified'      => $datetime,
            );
        }

        if ( !$data_shop_detail ) {
            $this->db->trans_rollback(); // Rollback Transaction
            $data['message'] = 'Checkout tidak berhasil. Terjadi kesalahan pada proses data transaksi produk detail.';
            die(json_encode($data));
        }

        foreach ($data_shop_detail as $row) {
            // -------------------------------------------------
            // Save Shop Order Detail
            // -------------------------------------------------
            $saved_shop_detail  = $this->Model_Shop->save_data_shop_customer_detail($row);

            if ( !$saved_shop_detail ) {
                $this->db->trans_rollback(); // Rollback Transaction
                $data['message'] = 'Checkout tidak berhasil. Terjadi kesalahan pada proses data transaksi produk detail.';
                die(json_encode($data));
            }
        }

        ## Order Success -------------------------------------------------------
        $this->db->trans_commit();
        $this->db->trans_complete();    // complete database transactions  
        $this->cart->destroy();         // Empty Product Cart

        remove_code_discount();
        remove_code_seller();

        $data_log   = array(
            'cookie'        => $_COOKIE, 
            'status'        => 'SUCCESS', 
            'customer_id'   => $customer_id,
            'sponsor_id'    => $sponsor_id,
            'shop_order_id' => $saved_shop_id,
            'product_detail'=> $product_detail,
            'total_payment' => $total_payment,
        );

        bp_log_action( 'SHOP_CUSTOMER', $invoice, $saved_shop_id, json_encode($data_log) );

        $shop_order_id      = bp_encrypt($saved_shop_id);
        $data['status']     = 'success';
        $data['message']    = 'Checkout berhasil';
        $data['url']        = base_url('store/detailorder/'.$shop_order_id);
        die(json_encode($data));
    }

    // =============================================================================================
    // CHECK SPOSNOR REFERRAL & CHECK USERNAME
    // =============================================================================================

    /**
     * Check Sponsor function.
     */
    function checksponsor()
    {
        // Check for AJAX Request
        if (!$this->input->is_ajax_request()) exit('No direct script access allowed');

        $sponsor    = $this->input->post('sponsor');
        $sponsor    = bp_isset($sponsor, '', '', true);
        $bp_token   = $this->security->get_csrf_hash();
        $data       = array('status' => false, 'token' => $bp_token, 'info' => '');

        if (!empty($sponsor)) {
            $memberdata     = $this->Model_Member->get_member_by('login', strtolower($sponsor));
            if (!$memberdata) { die(json_encode($data)); }
            if ($memberdata->status != ACTIVE) { die(json_encode($data)); }

            $info   = '<div class="form-group mb-2"><input type="text" class="mb-0" disabled="disabled" value="'. strtoupper($memberdata->name) .'"></div>';
            $info   = '<div for="sponsor" class="valid-feedback" style="display: block;">Nama Referral : '. strtoupper($memberdata->name) .'</div>';
            $data['status'] = true;
            $data['info']   = $info;
        }

        die(json_encode($data));
    }

    /**
     * Check Username function.
     */
    function checkusername()
    {
        // Check for AJAX Request
        if (!$this->input->is_ajax_request()) exit('No direct script access allowed');

        $username   = $this->input->post('username');
        $username   = bp_isset($username, '', '', true);
        $bp_token   = $this->security->get_csrf_hash();

        if (!empty($username)) {
            $memberdata     = $this->Model_Member->get_member_by('login', strtolower($username));

            if ($memberdata) {
                die(json_encode(array('status' => false, 'token' => $bp_token)));
            }

            // if staff with the username exists
            if ($staff = $this->Model_Staff->get_by('username', $username)){
                die(json_encode(array('status' => false, 'token' => $bp_token)));
            }
        }

        die(json_encode(array('status' => true, 'token' => $bp_token)));
    }
}
