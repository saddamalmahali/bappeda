<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Transaction Controller.
 *
 * @class     Transaction
 * @author    Yuda
 * @version   1.0.0
 */
class Transaction extends BP_Controller
{
    /**
     * Constructor.
     */
    function __construct()
    {
        parent::__construct();
    }

    // =============================================================================================
    // LIST DATA Transaction
    // =============================================================================================

    /**
     * Transaction List Data function.
     */
    function transactionlistsdata()
    {
        // This is for AJAX request
        if (!$this->input->is_ajax_request()) exit('No direct script access allowed');

        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            // Set JSON data
            $data = array('status' => 'access_denied', 'data' => '');
            die(json_encode($data));
        }

        $current_member         = bp_get_current_member();
        $is_administrator       = as_administrator($current_member);
        $is_admin               = as_admin($current_member);
        $is_bidang              = as_bidang($current_member);
        $list_access            = ($is_administrator || $is_bidang) ? TRUE : FALSE;
        $crud_access            = $is_administrator ? TRUE : FALSE;
        $access                 = '';
        $roles                  = '';

        if ($is_admin) {
            $access             = $current_member->access;
            $roles              = maybe_unserialize($current_member->roles);
        }

        if ($staff = bp_get_current_staff()) {
            $access             = $staff->access;
            $roles              = maybe_unserialize($staff->role);
        }

        if ($access == 'partial') {
            $crud_access        = FALSE;
            if ($roles && is_array($roles)) {
                if (in_array(STAFF_ACCESS2, $roles)) {
                    $crud_access = TRUE;
                }
            }
        }

        $params             = array();
        $condition          = '';
        $order_by           = '';
        $iTotalRecords      = 0;

        $sExport            = $this->input->get('export');
        $sAction            = bp_isset($_REQUEST['sAction'], '');
        $sAction            = bp_isset($sExport, $sAction);

        $search_method      = 'post';
        if ($sAction == 'download_excel') {
            $search_method  = 'get';
        }

        $iDisplayLength     = intval($_REQUEST['iDisplayLength']);
        $iDisplayStart      = intval($_REQUEST['iDisplayStart']);
        $sEcho              = intval($_REQUEST['sEcho']);
        $sort               = $_REQUEST['sSortDir_0'];
        $column             = intval($_REQUEST['iSortCol_0']);

        $limit              = ($iDisplayLength == '-1' ? 0 : $iDisplayLength);
        $offset             = $iDisplayStart;

        $s_code             = $this->input->$search_method('search_code');
        $s_code             = bp_isset($s_code, '', '', true);
        $s_name             = $this->input->$search_method('search_name');
        $s_name             = bp_isset($s_name, '', '', true);

        if (!empty($s_code)) {
            $condition .= ' AND %code% LIKE CONCAT("%", ?, "%")';
            $params[] = $s_code;
        }
        if (!empty($s_name)) {
            $condition .= ' AND %name% LIKE CONCAT("%", ?, "%")';
            $params[] = $s_name;
        }

        if ($column == 1) {
            $order_by .= '%code% ' . $sort;
        } elseif ($column == 2) {
            $order_by .= '%name% ' . $sort;
        }

        $data_list          = ($list_access) ? $this->Model_Master->get_all_urusan_data($limit, $offset, $condition, $order_by, $params) : '';
        $records            = array();
        $records["aaData"]  = array();

        if (!empty($data_list)) {
            $iTotalRecords  = bp_get_last_found_rows();
            $i = $offset + 1;
            foreach ($data_list as $row) {
                $id             = bp_encrypt($row->id);
                $code           = bp_strong(strtoupper($row->kode));
                $code           = '<a href="javascript:;">' . $code . '</a>';
                $name           = bp_strong(strtoupper($row->nama));

                $btn_edit       = '<a href="javascript:;" data-url="' . base_url('master/saveurusan/' . $id) . '" class="btn btn-sm btn-outline-primary btn-tooltip" title="Edit Urusan" data-code="' . $row->kode . '" data-name="' . $row->nama . '" id="btn-update-urusan"><i class="fa fa-edit"></i></a>';
                $btn_delete     = '<a href="javascript:;" data-url="' . base_url('master/deleteurusan/' . $id) . '" data-code="' . $row->kode . '" data-name="' . $row->nama . '" class="btn btn-sm btn-outline-danger btn-tooltip" title="Hapus Urusan" id="btn-delete-urusan"><i class="fa fa-times"></i></a>';

                $records["aaData"][] = array(
                    bp_center($i),
                    bp_center($code),
                    $name,
                    bp_center((($is_administrator && $crud_access) ? $btn_edit . $btn_delete : ''))
                );
                $i++;
            }
        }

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $records["sEcho"]                   = $sEcho;
        $records["iTotalRecords"]           = $iTotalRecords;
        $records["iTotalDisplayRecords"]    = $iTotalRecords;

        echo json_encode($records);
    }

    // =============================================================================================
    // Action Data Transaction
    // =============================================================================================

    function savetransaction($id = '')
    {
        // This is for AJAX request
        if (!$this->input->is_ajax_request()) exit('No direct script access allowed');

        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            // Set JSON data
            $data = array(
                'status'        => 'access_denied',
                'login'         => 'login',
                'url'           => base_url('login'),
            );
            die(json_encode($data));
        }

        // Set data Return
        $bp_token       = $this->security->get_csrf_hash();
        $data           = array(
            'status'    => 'error',
            'token'     => $bp_token,
            'message'   => 'Simpan Urusan tidak berhasil.'
        );

        // -------------------------------------------------
        // Set Variable
        // -------------------------------------------------
        $current_member         = bp_get_current_member();
        $is_admin               = as_administrator($current_member);

        $reg_code               = $this->input->post('reg_code');
        $reg_code               = trim(bp_isset($reg_code, '', '', true));
        $reg_name               = $this->input->post('reg_name');
        $reg_name               = trim(bp_isset($reg_name, '', '', true));

        // Check Available Urusan if ID not null
        // -------------------------------------------------
        $dataurusan         = '';
        if ($id) {
            $id = bp_decrypt($id);

            // Check Data Urusan
            // -------------------------------------------------
            if (!$dataurusan = bp_urusan($id)) {
                $data['message'] = 'Simpan Urusan tidak berhasil. Data Urusan Tidak Ditemukan';
                die(json_encode($data)); // Set JSON data
            }
        }

        // -------------------------------------------------
        // Check Form Validation
        // -------------------------------------------------
        $this->form_validation->set_rules('reg_code', 'Kode', 'required');

        if (!$dataurusan) {
            $this->form_validation->set_rules('reg_code', 'Kode', 'is_unique[urusan.kode]');
        }

        $this->form_validation->set_rules('reg_name', 'Nama', 'required');
        $this->form_validation->set_message('required', '%s harus di isi');
        $this->form_validation->set_message('is_unique', '%s Harus Unique');
        $this->form_validation->set_error_delimiters('* ', br());

        if ($this->form_validation->run() == FALSE) {

            $data['message'] = 'Simpan Urusan tidak berhasil. ' . br() . validation_errors();
            die(json_encode($data)); // Set JSON data
        }

        // -------------------------------------------------
        // Begin Transaction
        // -------------------------------------------------
        $this->db->trans_begin();

        // Set Data Urusan
        // -------------------------------------------------

        $data_save = array(
            'kode'      => $reg_code,
            'nama'      => $reg_name
        );

        if ($dataurusan) {
            if (!$id_update_urusan = $this->Model_Master->update_data_urusan($dataurusan->id, $data_save)) {
                $data['message'] = 'Simpan Urusan tidak berhasil. Terjadi kesalahan sistem';
                die(json_encode($data)); // Set JSON data
            }
        } else {
            if (!$id_save_urusan = $this->Model_Master->save_urusan($data_save)) {
                $data['message'] = 'Simpan Urusan tidak berhasil. Terjadi kesalahan sistem';
                die(json_encode($data)); // Set JSON data
            }
        }


        // -------------------------------------------------
        // Commit or Rollback Transaction
        // -------------------------------------------------
        if ($this->db->trans_status() === FALSE) {
            // Rollback Transaction
            $this->db->trans_rollback();
            $data['message'] = 'Simpan Data Urusan tidak berhasil. Terjadi kesalahan data transaksi.';
            die(json_encode($data)); // Set JSON data
        } else {
            // Commit Transaction
            $this->db->trans_commit();
            // Complete Transaction
            $this->db->trans_complete();
            bp_log_action('ACTION_SAVE_URUSAN', $reg_code, $reg_code, json_encode(array('cookie' => $_COOKIE, 'status' => 'SUCCESS', 'code' => $reg_code, 'name' => $reg_name)));

            $data           = array(
                'status'    => 'success',
                'token'     => $bp_token,
                'message'   => 'Simpan Data Urusan Berhasil'
            );
            die(json_encode($data));
        }
    }

    function deletetransaction($id = '')
    {
        if (!$this->input->is_ajax_request()) {
            redirect(base_url('productmanage/productlist'), 'refresh');
        }
        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            $data = array('status' => 'access_denied', 'url' => base_url('login'));
            die(json_encode($data)); // JSON encode data
        }

        if (!$id) {
            $data = array('status' => 'error', 'message' => 'ID Urusan tidak ditemukan !');
            die(json_encode($data));
        }

        $id = bp_decrypt($id);
        if (!$data_urusan = bp_urusan($id)) {
            $data = array('status' => 'error', 'message' => 'Data Urusan tidak ditemukan !');
            die(json_encode($data));
        }

        if (!$delete_data = $this->Model_Master->delete_data_urusan($id)) {
            $data = array('status' => 'error', 'message' => 'Urusan tidak berhasil dihapus !');
            die(json_encode($data));
        }

        // Save Success
        $data = array('status' => 'success', 'message' => 'Urusan berhasil dihapus.');
        die(json_encode($data));
    }

    function importtransaction()
    {

        if (!$this->input->is_ajax_request()) {
            redirect(base_url('master/urusan'), 'refresh');
        }

        $auth = auth_redirect($this->input->is_ajax_request());

        if (!$auth) {
            $data = array('status' => 'access_denied', 'url' => base_url('login'));
            die(json_encode($data)); // JSON encode data
        }

        // Set data Return
        $bp_token       = $this->security->get_csrf_hash();

        $data           = array(
            'status'    => 'error',
            'token'     => $bp_token,
            'message'   => 'Import Data Urusan tidak berhasil.'
        );

        // -------------------------------------------------
        // Set Variable
        // -------------------------------------------------
        $current_member         = bp_get_current_member();
        $is_admin               = as_administrator($current_member);

        if (!$is_admin) {
            $data['message'] = 'Import Data Rancangan tidak berhasil. Hanya admin yang dapat melakukan aksi ini.';
        }

        $img_name                   = random_string() . '-' . time();

        $config['upload_path']      = DATA_IMPORT_PATH;
        $config['allowed_types']    = 'xlsx';
        $config['max_size']         = '1048';
        $config['overwrite']        = true;
        $config['file_name']        = $img_name;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        $img_upload                 = true;
        $img_msg                    = '';
        if (!$this->upload->do_upload("reg_file")) {
            $img_upload             = false;
            $img_msg                = $this->upload->display_errors();
        }

        if (!$img_upload && $img_msg) {
            $data['message'] = 'Import Data Gagal. ' . $img_msg;
            die(json_encode($data)); // Set JSON data
        }

        $get_data_import       = $this->upload->data();

        if (!$get_data_import) {
            $data['message'] = 'Import Data Gagal. Terjadi kesalahan Sistem';
            die(json_encode($data)); // Set JSON data
        }

        $filedata                       = IMPORT_PATH . $img_name . '.xlsx';

        if (!file_exists($filedata)) {
            $data['status']             = 'error';
            $data['message']            = 'Gagal Import Data Rancangan, File gagal diupload';
            $data['data']               = null;
            die(json_encode($data));
        }

        $migrate                        = bp_import_urusan($filedata);

        unlink($filedata);

        if ($migrate['status'] == 'error') {
            $data['status']             = 'error';
            $data['message']            = 'Gagal Import Data Rancangan';
            $data['data']               = null;
            die(json_encode($data));
        }

        $message = 'Import Data Urusan Berhasil.';
        $message .= ' ' . $migrate['data_insert'] . ' data berhasil ditambahkan';
        $message .= ' dan ' . $migrate['data_update'] . ' data berhasil dirubah';

        $data['status']                         = 'success';
        $data['message']                        = $message;
        $data['data']                           = $migrate;

        die(json_encode($data));
    }

    // ------------------------------------------------------------------------------------------------
}

/* End of file Transaction.php */
/* Location: ./application/controllers/Transaction.php */
