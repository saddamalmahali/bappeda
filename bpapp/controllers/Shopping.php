<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Shopping Controller.
 *
 * @class     Shopping
 * @version   1.0.0
 */
class Shopping extends BP_Controller
{
    /**
     * Constructor.
     */
    function __construct()
    {
        parent::__construct();
    }

    // =============================================================================================
    // LIST DATA
    // =============================================================================================

    /**
     * Shop Order List Data function.
     */
    function shoporderlistsdata($type_order = '', $status_order = '', $product_type = '')
    {
        // This is for AJAX request
        if (!$this->input->is_ajax_request()) exit('No direct script access allowed');

        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            // Set JSON data
            $data = array('status' => 'access_denied', 'data' => '');
            die(json_encode($data));
        }

        $this->load->helper('shop_helper');

        $current_member     = bp_get_current_member();
        $is_admin           = as_administrator($current_member);
        $type_order         = $type_order ? bp_decrypt($type_order) : '';
        $load_data          = false;

        $params             = array();
        $condition          = ' AND %type_member% = ' . MEMBER;
        $order_by           = '';

        if ($is_admin && $type_order == 'member_to_admin') {
            $load_data      = true;
            $condition     .= ' AND %id_stockist% = 0';
        }

        if ($is_admin && $type_order == 'member_to_stockist') {
            $load_data      = true;
            $condition     .= ' AND %id_stockist% > 0';
        }

        if (!$is_admin && $type_order == 'member_to_stockist') {
            $load_data      = true;
            $condition     .= ' AND %id_stockist% = ' . $current_member->id;
        }

        if (!$is_admin && $type_order == 'me_to_admin') {
            $load_data      = true;
            $condition     .= ' AND %id_member% = ' . $current_member->id . ' AND %id_stockist% = 0';
        }

        if (!$is_admin && $type_order == 'me_to_stockist') {
            $load_data      = true;
            $condition     .= ' AND %id_member% = ' . $current_member->id . ' AND %id_stockist% > 0';
        }

        if ($status_order) {
            $status_order   = bp_decrypt($status_order);
            if ($status_order == 'pending') {
                $condition .= ' AND %status% = 0';
            }
            if ($status_order == 'confirmed') {
                $condition .= ' AND %status% = 1';
            }
            if ($status_order == 'done') {
                $condition .= ' AND %status% = 2';
            }
            if ($status_order == 'cancelled') {
                $condition .= ' AND %status% = 4';
            }
        }

        $sExport            = $this->input->get('export');
        $sExport            = bp_isset($sExport, '', '', true);
        $sAction            = bp_isset($_REQUEST['sAction'], '', true);
        $sAction            = bp_isset($sExport, $sAction, $sAction, true);

        $search_method      = 'post';
        if ($sAction == 'download_excel') {
            $search_method  = 'get';
        }

        $iTotalRecords      = 0;
        $iDisplayLength     = intval($_REQUEST['iDisplayLength']);
        $iDisplayStart      = intval($_REQUEST['iDisplayStart']);
        $sEcho              = intval($_REQUEST['sEcho']);
        $sort               = $_REQUEST['sSortDir_0'];
        $column             = intval($_REQUEST['iSortCol_0']);

        $limit              = ($iDisplayLength == '-1' ? 0 : $iDisplayLength);
        $offset             = $iDisplayStart;

        $s_invoice          = $this->input->$search_method('search_invoice');
        $s_invoice          = bp_isset($s_invoice, '', '', true);
        $s_username         = $this->input->$search_method('search_username');
        $s_username         = bp_isset($s_username, '', '', true);
        $s_name             = $this->input->$search_method('search_name');
        $s_name             = bp_isset($s_name, '', '', true);
        $s_stockist         = $this->input->$search_method('search_stockist');
        $s_stockist         = bp_isset($s_stockist, '', '', true);
        $s_stockist_name    = $this->input->$search_method('search_stockist_name');
        $s_stockist_name    = bp_isset($s_stockist_name, '', '', true);
        $s_type             = $this->input->$search_method('search_type');
        $s_type             = bp_isset($s_type, '', '', true);
        $s_qty_min          = $this->input->$search_method('search_qty_min');
        $s_qty_min          = bp_isset($s_qty_min, '', '', true);
        $s_qty_max          = $this->input->$search_method('search_qty_max');
        $s_qty_max          = bp_isset($s_qty_max, '', '', true);
        $s_bv_min           = $this->input->$search_method('search_bv_min');
        $s_bv_min           = bp_isset($s_bv_min, '', '', true);
        $s_bv_max           = $this->input->$search_method('search_bv_max');
        $s_bv_max           = bp_isset($s_bv_max, '', '', true);
        $s_payment_min      = $this->input->$search_method('search_nominal_min');
        $s_payment_min      = bp_isset($s_payment_min, '', '', true);
        $s_payment_max      = $this->input->$search_method('search_nominal_max');
        $s_payment_max      = bp_isset($s_payment_max, '', '', true);
        $s_status           = $this->input->$search_method('search_status');
        $s_status           = bp_isset($s_status, '', '', true);
        $s_shipping         = $this->input->$search_method('search_shipping');
        $s_shipping         = bp_isset($s_shipping, '', '', true);
        $s_confirm_by       = $this->input->$search_method('search_confirm_by');
        $s_confirm_by       = bp_isset($s_confirm_by, '', '', true);
        $s_date_min         = $this->input->$search_method('search_datecreated_min');
        $s_date_min         = bp_isset($s_date_min, '', '', true);
        $s_date_max         = $this->input->$search_method('search_datecreated_max');
        $s_date_max         = bp_isset($s_date_max, '', '', true);
        $s_dateconfirm_min  = $this->input->$search_method('search_dateconfirm_min');
        $s_dateconfirm_min  = bp_isset($s_dateconfirm_min, '', '', true);
        $s_dateconfirm_max  = $this->input->$search_method('search_dateconfirm_max');
        $s_dateconfirm_max  = bp_isset($s_dateconfirm_max, '', '', true);

        if (!empty($s_invoice)) {
            $condition .= ' AND %invoice% LIKE CONCAT("%", ?, "%") ';
            $params[] = $s_invoice;
        }
        if (!empty($s_username)) {
            $condition .= ' AND %username% LIKE CONCAT("%", ?, "%") ';
            $params[] = $s_username;
        }
        if (!empty($s_name)) {
            $condition .= ' AND %name% LIKE CONCAT("%", ?, "%") ';
            $params[] = $s_name;
        }
        if (!empty($s_stockist)) {
            $condition .= ' AND %stockist% LIKE CONCAT("%", ?, "%") ';
            $params[] = $s_stockist;
        }
        if (!empty($s_stockist_name)) {
            $condition .= ' AND %stockist_name% LIKE CONCAT("%", ?, "%") ';
            $params[] = $s_stockist_name;
        }
        if (!empty($s_confirm_by)) {
            $condition .= ' AND %modified_by% LIKE CONCAT("%", ?, "%") ';
            $params[] = $s_confirm_by;
        }
        if (!empty($s_type)) {
            $condition .= ' AND %type_product% = ?';
            $params[] = $s_type;
        }
        if (!empty($s_shipping)) {
            $condition .= ' AND shipping_method = ?';
            $params[] = $s_shipping;
        }
        if (!empty($s_qty_min)) {
            $condition .= ' AND total_qty >= ?';
            $params[] = $s_qty_min;
        }
        if (!empty($s_qty_max)) {
            $condition .= ' AND total_qty <= ?';
            $params[] = $s_qty_max;
        }
        if (!empty($s_bv_min)) {
            $condition .= ' AND total_bv >= ?';
            $params[] = $s_bv_min;
        }
        if (!empty($s_bv_max)) {
            $condition .= ' AND total_bv <= ?';
            $params[] = $s_bv_max;
        }
        if (!empty($s_payment_min)) {
            $condition .= ' AND total_payment >= ?';
            $params[] = $s_payment_min;
        }
        if (!empty($s_payment_max)) {
            $condition .= ' AND total_payment <= ?';
            $params[] = $s_payment_max;
        }
        if (!empty($s_date_min)) {
            $condition .= ' AND DATE(%datecreated%) >= ?';
            $params[] = $s_date_min;
        }
        if (!empty($s_date_max)) {
            $condition .= ' AND DATE(%datecreated%) <= ?';
            $params[] = $s_date_max;
        }
        if (!empty($s_dateconfirm_min)) {
            $condition .= ' AND DATE(%dateconfirmed%) >= ?';
            $params[] = $s_dateconfirm_min;
        }
        if (!empty($s_dateconfirm_max)) {
            $condition .= ' AND DATE(%dateconfirmed%) <= ?';
            $params[] = $s_dateconfirm_max;
        }
        if (!empty($s_status)) {
            if ($s_status == 'pending') {
                $condition .= ' AND %status% = 0';
            }
            if ($s_status == 'confirmed') {
                $condition .= ' AND %status% = 1';
            }
            if ($s_status == 'done') {
                $condition .= ' AND %status% = 2';
            }
            if ($s_status == 'cancelled') {
                $condition .= ' AND %status% = 4';
            }
        }

        if (($is_admin && $type_order == 'member_to_admin') || (!$is_admin && $type_order == 'member_to_stockist')) {
            if ($column == 1) {
                $order_by = '%invoice% ' . $sort;
            } elseif ($column == 2) {
                $order_by = '%username% ' . $sort;
            } elseif ($column == 3) {
                $order_by = '%name% ' . $sort;
            } elseif ($column == 4) {
                $order_by = 'total_qty ' . $sort;
            } elseif ($column == 5) {
                $order_by = 'total_qty ' . $sort;
            } elseif ($column == 6) {
                $order_by = 'total_bv ' . $sort;
            } elseif ($column == 7) {
                $order_by = 'total_payment ' . $sort;
            } elseif ($column == 8) {
                $order_by = 'shipping_method ' . $sort;
            } elseif ($column == 9) {
                $order_by = '%datecreated% ' . $sort;
            } elseif ($column == 10) {
                if ($status_order == 'pending') {
                    $order_by = '%dateexpired% ' . $sort;
                }
                if ($status_order == 'confirmed') {
                    $order_by = '%dateconfirmed% ' . $sort;
                }
                if ($status_order == 'done') {
                    $order_by = '%datemodified% ' . $sort;
                }
                if ($status_order == 'cancelled') {
                    $order_by = '%datemodified% ' . $sort;
                }
            } elseif ($column == 11) {
                if ($status_order == 'cancelled') {
                    $order_by = '%modified_by% ' . $sort;
                } else {
                    $order_by = '%confirmed_by% ' . $sort;
                }
            }
        }

        if ($is_admin && $type_order == 'member_to_stockist') {
            if ($column == 1) {
                $order_by = '%invoice% ' . $sort;
            } elseif ($column == 2) {
                $order_by = '%username% ' . $sort;
            } elseif ($column == 3) {
                $order_by = '%name% ' . $sort;
            } elseif ($column == 4) {
                $order_by = '%stockist% ' . $sort . ', %stockist_name% ' . $sort;
            } elseif ($column == 5) {
                $order_by = 'total_qty ' . $sort;
            } elseif ($column == 6) {
                $order_by = 'total_qty ' . $sort;
            } elseif ($column == 7) {
                $order_by = 'total_bv ' . $sort;
            } elseif ($column == 8) {
                $order_by = 'total_payment ' . $sort;
            } elseif ($column == 9) {
                $order_by = '%status% ' . $sort;
            } elseif ($column == 10) {
                $order_by = 'shipping_method ' . $sort;
            } elseif ($column == 11) {
                $order_by = '%datecreated% ' . $sort;
            } elseif ($column == 12) {
                $order_by = '%datemodified% ' . $sort;
            } elseif ($column == 13) {
                $order_by = '%modified_by% ' . $sort;
            }
        }

        if (!$is_admin && $type_order == 'me_to_stockist') {
            if ($column == 1) {
                $order_by = '%invoice% ' . $sort;
            } elseif ($column == 2) {
                $order_by = '%stockist% ' . $sort;
            } elseif ($column == 3) {
                $order_by = '%stockist_name% ' . $sort;
            } elseif ($column == 4) {
                $order_by = 'total_qty ' . $sort;
            } elseif ($column == 5) {
                $order_by = 'total_qty ' . $sort;
            } elseif ($column == 6) {
                $order_by = 'total_bv ' . $sort;
            } elseif ($column == 7) {
                $order_by = 'total_payment ' . $sort;
            } elseif ($column == 8) {
                $order_by = '%status% ' . $sort;
            } elseif ($column == 9) {
                $order_by = 'shipping_method ' . $sort;
            } elseif ($column == 10) {
                $order_by = '%datecreated% ' . $sort;
            } elseif ($column == 11) {
                $order_by = '%datemodified% ' . $sort;
            } elseif ($column == 12) {
                $order_by = '%modified_by% ' . $sort;
            }
        }

        if (!$is_admin && $type_order == 'me_to_admin') {
            if ($column == 1) {
                $order_by = '%invoice% ' . $sort;
            } elseif ($column == 2) {
                $order_by = 'total_qty ' . $sort;
            } elseif ($column == 3) {
                $order_by = 'total_qty ' . $sort;
            } elseif ($column == 4) {
                $order_by = 'total_bv ' . $sort;
            } elseif ($column == 5) {
                $order_by = 'total_payment ' . $sort;
            } elseif ($column == 6) {
                $order_by = '%status% ' . $sort;
            } elseif ($column == 7) {
                $order_by = 'shipping_method ' . $sort;
            } elseif ($column == 8) {
                $order_by = '%datecreated% ' . $sort;
            } elseif ($column == 9) {
                $order_by = '%datemodified% ' . $sort;
            } elseif ($column == 10) {
                $order_by = 'modified_by ' . $sort;
            }
        }

        if ($load_data) {
            $data_list      = $this->Model_Shop->get_all_shop_order_data($limit, $offset, $condition, $order_by, $params);
        } else {
            $data_list      = array();
        }
        $records            = array();
        $records["aaData"]  = array();

        if (!empty($data_list)) {
            $iTotalRecords  = bp_get_last_found_rows();
            $currency       = config_item('currency');
            $datenow        = date('Y-m-d H:i:s');
            $access         = FALSE;
            if ($is_admin) {
                if ($staff = bp_get_current_staff()) {
                    if ($staff->access == 'partial') {
                        $role   = array();
                        if ($staff->role) {
                            $role = $staff->role;
                        }

                        if (!empty($role)) {
                            if (in_array(STAFF_ACCESS10, $role)) {
                                $access = TRUE;
                            }
                        }
                    } else {
                        $access = TRUE;
                    }
                } else {
                    $access = TRUE;
                }
            }
            $i = $offset + 1;
            foreach ($data_list as $row) {
                $id             = bp_encrypt($row->id);
                $id_member      = bp_encrypt($row->id_member);
                $id_stockist    = bp_encrypt($row->id_stockist);
                $username       = bp_strong(strtolower($row->username));
                $stockist       = bp_strong(strtolower($row->stockist));
                $username       = ($is_admin ? '<a href="' . base_url('profile/' . $id_member) . '">' . $username . '</a>' : $username);
                $stockist       = ($is_admin ? '<a href="' . base_url('profile/' . $id_stockist) . '">' . $stockist . '</a>' : $stockist);
                $name           = (strtoupper($row->membername));
                $stockist_name  = (strtoupper($row->stockistname));

                $datemodified   = date('d M y H:i', strtotime($row->datemodified));
                $dateconfirm    = '-';
                $confirmed_by   = '-';
                $pending_expire = false;
                if ($row->status > 0) {
                    $confirmed_by   = $row->modified_by;
                }
                if ($row->dateconfirmed != '0000-00-00 00:00:00' && $row->status == 1) {
                    $dateconfirm    = date('d M y H:i', strtotime($row->dateconfirmed));
                    $confirmed_by   = $row->confirmed_by;
                }

                $dateexpired    = '-';
                if ($row->dateexpired && $row->dateexpired != '0000-00-00 00:00:00') {
                    $dateexpired    = date('d M y H:i', strtotime($row->dateexpired));
                    if ($row->status == 0 && strtotime($datenow) > strtotime($row->dateexpired)) {
                        // $row->status    = 4;
                        $datemodified   = $dateexpired;
                        $confirmed_by   = 'expired';
                        $pending_expire = true;
                    }
                }

                $total_payment  = bp_accounting($row->total_payment);
                $uniquecode     = $row->unique ? str_pad($row->unique, 3, '0', STR_PAD_LEFT) : '';
                $status         = '';
                if ($row->status == 0) {
                    $status = '<span class="badge badge-default">PENDING</span>';
                }
                if ($row->status == 1) {
                    $status = '<span class="badge badge-info">CONFIRMED</span>';
                }
                if ($row->status == 2) {
                    $status = '<span class="badge badge-success">DONE</span>';
                }
                if ($row->status == 4) {
                    $status = '<span class="badge badge-danger">CANCELLED</span>';
                }

                $btn_invoice    = '<a href="' . base_url('invoice/' . $id) . '"  target="+_blank"
                                    class="btn btn-sm btn_block btn-outline-default" ><i class="fa fa-file"></i> ' . $row->invoice . '</a>';

                $btn_product    = 'SubTotal : <b>' . bp_accounting($row->subtotal) . '</b>' . br();
                $btn_product    = '<a href="javascript:;" 
                                    data-url="' . base_url('shopping/getshoporderdetail/' . $id) . '" 
                                    data-invoice="' . $row->invoice . '"
                                    class="btn btn-sm btn-block btn-outline-primary btn-shop-order-detail">
                                    <i class="ni ni-bag-17 mr-1"></i> Detail Order</a>';


                if (strtolower($row->shipping_method) == 'pickup') {
                    $courier    = '<center><b>PICKUP</b></center>';
                    if ($row->status == 2) {
                        $courier .= '<br><b>Nama Pengambil</b> : <br><span class="text-warning font-weight-bold">' . ($row->resi ? strtoupper($row->resi) : '-') . '</span>';
                    }
                } else {
                    $courier    = '<center><b>EKSPEDISI</b></center>';
                    if ($row->courier) {
                        $courier .= br() . '<b>' . lang('courier') . '</b> : ' . ((strtolower($row->courier) == 'ekspedisi') ? '-' : strtoupper($row->courier));
                        $courier .= br() . '<b>Layanan</b> : ' . ((strtolower($row->courier) == 'ekspedisi') ? '-' : strtoupper($row->service));
                    }
                    if ($row->status == 2) {
                        $courier .= br() . '<b>Resi</b> : <span class="text-warning font-weight-bold">' . ($row->resi ? strtoupper($row->resi) : '-') . '</span>';
                    }
                }


                $btn_confirm    = $btn_cancel = $btn_payment = '';
                if ($row->status == 0) {
                    $detail     = bp_extract_products_order($row);
                    if ($is_admin && $access && $row->id_stockist == 0) {
                        $btn_cancel = '<a href="javascript:;" 
                                            data-url="' . base_url('shopping/cancelorder/' . $id) . '" 
                                            data-status="payment"
                                            data-shippingmethod="' . strtolower($row->shipping_method) . '"
                                            data-invoice="' . $row->invoice . '"
                                            data-name="' . $row->username . '"
                                            data-total="' . $total_payment . '"
                                            data-message="Apakah anda yakin akan membatalkan pesanan ini ?"
                                            class="btn btn-sm btn-block btn-outline-warning btn-tooltip btn-shop-order-action" 
                                            title="Batalkan Pesanan"><i class="fa fa-times"></i> Cancel</a>';

                        $btn_confirm = '<a href="javascript:;" 
                                            data-url="' . base_url('shopping/confirmorder/' . $id) . '" 
                                            data-status="payment"
                                            data-shippingmethod="' . strtolower($row->shipping_method) . '"
                                            data-invoice="' . $row->invoice . '"
                                            data-name="' . $row->username . '"
                                            data-detail=\'' . json_encode($detail) . '\'
                                            data-total="' . $total_payment . '"
                                            data-uniquecode="' . $uniquecode . '"
                                            data-subtotal="' . $row->subtotal . '"
                                            data-shipping="' . $row->shipping . '"
                                            data-discount="' . $row->discount . '"
                                            data-voucher="' . $row->voucher . '"
                                            data-message="Apakah anda yakin akan Konfirmasi pembayaran atas pesanan ini ?"
                                            class="btn btn-sm btn-block btn-default btn-tooltip btn-shop-order-action" 
                                            title="Konfirmasi Pembayaran"><i class="fa fa-check"></i> Konfirmasi</a>';
                    }

                    if (!$is_admin && $row->id_stockist == $current_member->id) {
                        $btn_cancel = '<a href="javascript:;" 
                                            data-url="' . base_url('shopping/cancelorder/' . $id) . '" 
                                            data-status="payment"
                                            data-shippingmethod="' . strtolower($row->shipping_method) . '"
                                            data-invoice="' . $row->invoice . '"
                                            data-name="' . $row->username . '"
                                            data-total="' . $total_payment . '"
                                            data-message="Apakah anda yakin akan membatalkan pesanan ini ?"
                                            class="btn btn-sm btn-block btn-outline-warning btn-tooltip btn-shop-order-action" 
                                            title="Batalkan Pesanan"><i class="fa fa-times"></i> Cancel</a>';

                        $btn_confirm = '<a href="javascript:;" 
                                            data-url="' . base_url('shopping/confirmorder/' . $id) . '" 
                                            data-status="payment"
                                            data-shippingmethod="' . strtolower($row->shipping_method) . '"
                                            data-invoice="' . $row->invoice . '"
                                            data-name="' . $row->username . '"
                                            data-detail=\'' . json_encode($detail) . '\'
                                            data-total="' . $total_payment . '"
                                            data-uniquecode="' . $uniquecode . '"
                                            data-subtotal="' . $row->subtotal . '"
                                            data-shipping="' . $row->shipping . '"
                                            data-discount="' . $row->discount . '"
                                            data-voucher="' . $row->voucher . '"
                                            data-message="Apakah anda yakin akan Konfirmasi pembayaran atas pesanan ini ?"
                                            class="btn btn-sm btn-block btn-default btn-tooltip btn-shop-order-action" 
                                            title="Konfirmasi Pembayaran"><i class="fa fa-check"></i> Konfirmasi</a>';
                    }

                    if ($row->id_member == $current_member->id) {
                        $btn_cancel = '<a href="javascript:;" 
                                            data-url="' . base_url('shopping/cancelorder/' . $id) . '" 
                                            data-status="payment"
                                            data-shippingmethod="' . strtolower($row->shipping_method) . '"
                                            data-invoice="' . $row->invoice . '"
                                            data-name="' . $row->username . '"
                                            data-total="' . $total_payment . '"
                                            data-message="Apakah anda yakin akan membatalkan pesanan ini ?"
                                            class="btn btn-sm btn-block btn-outline-warning btn-tooltip btn-shop-order-action" 
                                            title="Batalkan Pesanan"><i class="fa fa-times"></i> Cancel</a>';
                    }
                }

                if ($row->status == 1) {
                    $detail     = bp_extract_products_order($row);
                    if ($is_admin && $access && $row->id_stockist == 0) {
                        $btn_confirm = '<a href="javascript:;" 
                                            data-url="' . base_url('shopping/confirmshipping/' . $id) . '" 
                                            data-status="shipping"
                                            data-shippingmethod="' . strtolower($row->shipping_method) . '"
                                            data-invoice="' . $row->invoice . '"
                                            data-name="' . $row->username . '"
                                            data-detail=\'' . json_encode($detail) . '\'
                                            data-total="' . $total_payment . '"
                                            data-uniquecode="' . $uniquecode . '"
                                            data-subtotal="' . $row->subtotal . '"
                                            data-shipping="' . $row->shipping . '"
                                            data-discount="' . $row->discount . '"
                                            data-voucher="' . $row->voucher . '"
                                            data-courier="' . strtoupper($row->courier) . '"
                                            data-service="' . $row->service . '"
                                            data-message="Apakah anda yakin akan Konfirmasi Pengiriman atas pesanan ini ?"
                                            class="btn btn-sm btn-block btn-default btn-tooltip btn-shop-order-action" 
                                            title="Konfirmasi Pengiriman"><i class="fa fa-truck"></i> Kirim Produk</a>';
                    }

                    if (!$is_admin && $row->id_stockist == $current_member->id) {
                        $btn_confirm = '<a href="javascript:;" 
                                            data-url="' . base_url('shopping/confirmshipping/' . $id) . '" 
                                            data-status="shipping"
                                            data-shippingmethod="' . strtolower($row->shipping_method) . '"
                                            data-invoice="' . $row->invoice . '"
                                            data-name="' . $row->username . '"
                                            data-detail=\'' . json_encode($detail) . '\'
                                            data-total="' . $total_payment . '"
                                            data-uniquecode="' . $uniquecode . '"
                                            data-subtotal="' . $row->subtotal . '"
                                            data-shipping="' . $row->shipping . '"
                                            data-discount="' . $row->discount . '"
                                            data-voucher="' . $row->voucher . '"
                                            data-courier="' . strtoupper($row->courier) . '"
                                            data-service="' . $row->service . '"
                                            data-message="Apakah anda yakin akan Konfirmasi Pengiriman atas pesanan ini ?"
                                            class="btn btn-sm btn-block btn-default btn-tooltip btn-shop-order-action" 
                                            title="Konfirmasi Pengiriman"><i class="fa fa-truck"></i> Kirim Produk</a>';
                    }
                }

                if ($row->status == 2) {
                    $btn_confirm    = '<a href="javascript:;" class="btn btn-sm btn-block btn-outline-success"><i class="fa fa-check"></i> Done</a>';
                }

                if ($pending_expire) {
                    $btn_confirm    = '';
                    $btn_cancel     = '<a href="javascript:;" class="btn btn-sm btn-block btn-outline-danger"><i class="ni ni-time-alarm"></i> expired</a>';
                }

                $datatables     = array(
                    bp_center($i),
                    bp_center($btn_invoice)
                );

                if (($is_admin && $type_order == 'member_to_admin') || (!$is_admin && $type_order == 'member_to_stockist')) {
                    $datatables[]   = bp_center($username);
                    $datatables[]   = $name;
                }

                if (!$is_admin && $type_order == 'me_to_stockist') {
                    $datatables[]   = bp_center($stockist);
                    $datatables[]   = $stockist_name;
                }

                if ($is_admin && $type_order == 'member_to_stockist') {
                    $datatables[]   = bp_center($username);
                    $datatables[]   = $name;
                    $datatables[]   = $stockist . br() . $stockist_name;
                }

                $datatables[]       = bp_center($btn_product);
                $datatables[]       = '<div style="min-width:50px">' . bp_center(bp_accounting($row->total_qty)) . '</div>';
                $datatables[]       = '<div style="min-width:80px">' . bp_accounting($row->total_bv, '', true) . '</div>';
                $datatables[]       = bp_right($total_payment);

                if (($is_admin && $type_order == 'member_to_stockist') || $type_order == 'me_to_admin' || $type_order == 'me_to_stockist') {
                    $datatables[]   = bp_center($status);
                }

                $datatables[]       = $courier;
                $datatables[]       = bp_center(date('j M y @H:i', strtotime($row->datecreated)));

                if ($status_order == 'pending') {
                    $datatables[]       = bp_center($dateexpired);
                } else {
                    $datatables[]   = bp_center($datemodified);
                    $datatables[]   = bp_center($confirmed_by);
                }

                $datatables[]       = bp_center($btn_confirm . $btn_cancel);

                $records["aaData"][] = $datatables;
                $i++;
            }
        }

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $records["sEcho"]                   = $sEcho;
        $records["iTotalRecords"]           = $iTotalRecords;
        $records["iTotalDisplayRecords"]    = $iTotalRecords;
        $records["load_data"]               = $load_data;
        $records["type_order"]              = $type_order;
        $records["token"]                   = $this->security->get_csrf_hash();;

        echo json_encode($records);
    }

    /**
     * Shop Order List Data function.
     */
    function shopordercustomerlistsdata($type_order = '', $status_order = '', $product_type = '')
    {
        // This is for AJAX request
        if (!$this->input->is_ajax_request()) exit('No direct script access allowed');

        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            // Set JSON data
            $data = array('status' => 'access_denied', 'data' => '');
            die(json_encode($data));
        }

        $this->load->helper('shop_helper');

        $current_member     = bp_get_current_member();
        $is_admin           = as_administrator($current_member);
        $type_order         = $type_order ? bp_decrypt($type_order) : '';
        $load_data          = false;

        $params             = array();
        $condition          = '';
        $order_by           = '';

        if ($is_admin && $type_order == 'member_to_admin') {
            $load_data      = true;
        }

        if (!$is_admin && $type_order == 'me_to_admin') {
            $load_data      = true;
            $condition     .= ' AND %id_customer% = ' . $current_member->id;
        }

        if ($status_order) {
            $status_order   = bp_decrypt($status_order);
            if ($status_order == 'pending') {
                $condition .= ' AND %status% = 0';
            }
            if ($status_order == 'confirmed') {
                $condition .= ' AND %status% = 1';
            }
            if ($status_order == 'done') {
                $condition .= ' AND %status% = 2';
            }
            if ($status_order == 'cancelled') {
                $condition .= ' AND %status% = 4';
            }
        }

        $sExport            = $this->input->get('export');
        $sExport            = bp_isset($sExport, '', '', true);
        $sAction            = bp_isset($_REQUEST['sAction'], '', true);
        $sAction            = bp_isset($sExport, $sAction, $sAction, true);

        $search_method      = 'post';
        if ($sAction == 'download_excel') {
            $search_method  = 'get';
        }

        $iTotalRecords      = 0;
        $iDisplayLength     = intval($_REQUEST['iDisplayLength']);
        $iDisplayStart      = intval($_REQUEST['iDisplayStart']);
        $sEcho              = intval($_REQUEST['sEcho']);
        $sort               = $_REQUEST['sSortDir_0'];
        $column             = intval($_REQUEST['iSortCol_0']);

        $limit              = ($iDisplayLength == '-1' ? 0 : $iDisplayLength);
        $offset             = $iDisplayStart;

        $s_invoice          = $this->input->$search_method('search_invoice');
        $s_invoice          = bp_isset($s_invoice, '', '', true);
        $s_username         = $this->input->$search_method('search_username');
        $s_username         = bp_isset($s_username, '', '', true);
        $s_name             = $this->input->$search_method('search_name');
        $s_name             = bp_isset($s_name, '', '', true);
        $s_type             = $this->input->$search_method('search_type');
        $s_type             = bp_isset($s_type, '', '', true);
        $s_qty_min          = $this->input->$search_method('search_qty_min');
        $s_qty_min          = bp_isset($s_qty_min, '', '', true);
        $s_qty_max          = $this->input->$search_method('search_qty_max');
        $s_qty_max          = bp_isset($s_qty_max, '', '', true);
        $s_bv_min           = $this->input->$search_method('search_bv_min');
        $s_bv_min           = bp_isset($s_bv_min, '', '', true);
        $s_bv_max           = $this->input->$search_method('search_bv_max');
        $s_bv_max           = bp_isset($s_bv_max, '', '', true);
        $s_payment_min      = $this->input->$search_method('search_nominal_min');
        $s_payment_min      = bp_isset($s_payment_min, '', '', true);
        $s_payment_max      = $this->input->$search_method('search_nominal_max');
        $s_payment_max      = bp_isset($s_payment_max, '', '', true);
        $s_status           = $this->input->$search_method('search_status');
        $s_status           = bp_isset($s_status, '', '', true);
        $s_shipping         = $this->input->$search_method('search_shipping');
        $s_shipping         = bp_isset($s_shipping, '', '', true);
        $s_courier          = $this->input->$search_method('search_courier');
        $s_courier          = bp_isset($s_courier, '', '', true);
        $s_resi             = $this->input->$search_method('search_resi');
        $s_resi             = bp_isset($s_resi, '', '', true);
        $s_confirm_by       = $this->input->$search_method('search_confirm_by');
        $s_confirm_by       = bp_isset($s_confirm_by, '', '', true);
        $s_date_min         = $this->input->$search_method('search_datecreated_min');
        $s_date_min         = bp_isset($s_date_min, '', '', true);
        $s_date_max         = $this->input->$search_method('search_datecreated_max');
        $s_date_max         = bp_isset($s_date_max, '', '', true);
        $s_dateconfirm_min  = $this->input->$search_method('search_dateconfirm_min');
        $s_dateconfirm_min  = bp_isset($s_dateconfirm_min, '', '', true);
        $s_dateconfirm_max  = $this->input->$search_method('search_dateconfirm_max');
        $s_dateconfirm_max  = bp_isset($s_dateconfirm_max, '', '', true);

        if (!empty($s_invoice)) {
            $condition .= ' AND %invoice% LIKE CONCAT("%", ?, "%") ';
            $params[] = $s_invoice;
        }
        if (!empty($s_username)) {
            $condition .= ' AND %username% LIKE CONCAT("%", ?, "%") ';
            $params[] = $s_username;
        }
        if (!empty($s_name)) {
            $condition .= ' AND %name% LIKE CONCAT("%", ?, "%") ';
            $params[] = $s_name;
        }
        if (!empty($s_stockist)) {
            $condition .= ' AND %stockist% LIKE CONCAT("%", ?, "%") ';
            $params[] = $s_stockist;
        }
        if (!empty($s_stockist_name)) {
            $condition .= ' AND %stockist_name% LIKE CONCAT("%", ?, "%") ';
            $params[] = $s_stockist_name;
        }
        if (!empty($s_confirm_by)) {
            $condition .= ' AND %modified_by% LIKE CONCAT("%", ?, "%") ';
            $params[] = $s_confirm_by;
        }
        if (!empty($s_type)) {
            $condition .= ' AND %type% = ?';
            $params[] = $s_type;
        }
        if (!empty($s_courier)) {
            $condition .= ' AND courier = LIKE CONCAT("%", ?, "%") ';
            $params[] = $s_courier;
        }
        if (!empty($s_resi)) {
            $condition .= ' AND resi = LIKE CONCAT("%", ?, "%") ';
            $params[] = $s_resi;
        }
        if (!empty($s_shipping)) {
            $condition .= ' AND shipping_method = ?';
            $params[] = $s_shipping;
        }
        if (!empty($s_qty_min)) {
            $condition .= ' AND total_qty >= ?';
            $params[] = $s_qty_min;
        }
        if (!empty($s_qty_max)) {
            $condition .= ' AND total_qty <= ?';
            $params[] = $s_qty_max;
        }
        if (!empty($s_bv_min)) {
            $condition .= ' AND total_bv >= ?';
            $params[] = $s_bv_min;
        }
        if (!empty($s_bv_max)) {
            $condition .= ' AND total_bv <= ?';
            $params[] = $s_bv_max;
        }
        if (!empty($s_payment_min)) {
            $condition .= ' AND total_payment >= ?';
            $params[] = $s_payment_min;
        }
        if (!empty($s_payment_max)) {
            $condition .= ' AND total_payment <= ?';
            $params[] = $s_payment_max;
        }
        if (!empty($s_date_min)) {
            $condition .= ' AND DATE(%datecreated%) >= ?';
            $params[] = $s_date_min;
        }
        if (!empty($s_date_max)) {
            $condition .= ' AND DATE(%datecreated%) <= ?';
            $params[] = $s_date_max;
        }
        if (!empty($s_dateconfirm_min)) {
            $condition .= ' AND DATE(%dateconfirmed%) >= ?';
            $params[] = $s_dateconfirm_min;
        }
        if (!empty($s_dateconfirm_max)) {
            $condition .= ' AND DATE(%dateconfirmed%) <= ?';
            $params[] = $s_dateconfirm_max;
        }
        if (!empty($s_status)) {
            if ($s_status == 'pending') {
                $condition .= ' AND %status% = 0';
            }
            if ($s_status == 'confirmed') {
                $condition .= ' AND %status% = 1';
            }
            if ($s_status == 'done') {
                $condition .= ' AND %status% = 2';
            }
            if ($s_status == 'cancelled') {
                $condition .= ' AND %status% = 4';
            }
        }

        if ($is_admin && $type_order == 'member_to_admin') {
            if ($column == 1) {
                $order_by = '%invoice% ' . $sort;
            } elseif ($column == 2) {
                $order_by = '%name% ' . $sort;
            } elseif ($column == 3) {
                $order_by = '%type% ' . $sort;
            } elseif ($column == 4) {
                $order_by = 'total_qty ' . $sort;
            } elseif ($column == 5) {
                $order_by = 'total_qty ' . $sort;
            } elseif ($column == 6) {
                $order_by = 'total_bv ' . $sort;
            } elseif ($column == 7) {
                $order_by = 'total_payment ' . $sort;
            } elseif ($column == 8) {
                $order_by = 'courier ' . $sort . ', resi ' . $sort;
            } elseif ($column == 9) {
                $order_by = '%datecreated% ' . $sort;
            } elseif ($column == 10) {
                if ($status_order == 'pending') {
                    $order_by = '%dateexpired% ' . $sort;
                }
                if ($status_order == 'confirmed') {
                    $order_by = '%dateconfirmed% ' . $sort;
                }
                if ($status_order == 'done') {
                    $order_by = '%datemodified% ' . $sort;
                }
                if ($status_order == 'cancelled') {
                    $order_by = '%datemodified% ' . $sort;
                }
            } elseif ($column == 11) {
                if ($status_order == 'cancelled') {
                    $order_by = '%modified_by% ' . $sort;
                } else {
                    $order_by = '%confirmed_by% ' . $sort;
                }
            }
        }

        if (!$is_admin && $type_order == 'me_to_admin') {
            if ($column == 1) {
                $order_by = '%invoice% ' . $sort;
            } elseif ($column == 2) {
                $order_by = 'total_qty ' . $sort;
            } elseif ($column == 3) {
                $order_by = 'total_qty ' . $sort;
            } elseif ($column == 4) {
                $order_by = 'total_bv ' . $sort;
            } elseif ($column == 5) {
                $order_by = 'total_payment ' . $sort;
            } elseif ($column == 6) {
                $order_by = '%status% ' . $sort;
            } elseif ($column == 7) {
                $order_by = 'shipping_method ' . $sort;
            } elseif ($column == 8) {
                $order_by = '%datecreated% ' . $sort;
            } elseif ($column == 9) {
                $order_by = '%datemodified% ' . $sort;
            } elseif ($column == 10) {
                $order_by = 'modified_by ' . $sort;
            }
        }

        if ($load_data) {
            $data_list      = $this->Model_Shop->get_all_shop_customer_order_data($limit, $offset, $condition, $order_by, $params);
        } else {
            $data_list      = array();
        }
        $records            = array();
        $records["aaData"]  = array();

        if (!empty($data_list)) {
            $iTotalRecords  = bp_get_last_found_rows();
            $currency       = config_item('currency');
            $datenow        = date('Y-m-d H:i:s');
            $access         = FALSE;
            if ($is_admin) {
                if ($staff = bp_get_current_staff()) {
                    if ($staff->access == 'partial') {
                        $role   = array();
                        if ($staff->role) {
                            $role = $staff->role;
                        }

                        if (!empty($role)) {
                            if (in_array(STAFF_ACCESS10, $role)) {
                                $access = TRUE;
                            }
                        }
                    } else {
                        $access = TRUE;
                    }
                } else {
                    $access = TRUE;
                }
            }
            $i = $offset + 1;
            foreach ($data_list as $row) {
                $id             = bp_encrypt($row->id);
                $id_member      = bp_encrypt($row->id_customer);
                $id_customer    = $id_member;
                $id_sponsor     = bp_encrypt($row->id_sponsor);
                $username       = bp_strong(strtolower($row->username));
                $sponsor        = bp_strong(strtolower($row->sponsor));
                $username       = ($is_admin ? '<a href="' . base_url('profile/' . $id_member) . '" class="text-primary">' . $username . '</a>' : $username);
                $sponsor        = ($is_admin ? '<a href="' . base_url('profile/' . $id_sponsor) . '">' . $sponsor . '</a>' : $sponsor);
                $member_name    = (strtoupper($row->membername));
                $sponsor_name   = (strtoupper($row->sponsorname));

                $customer       = '<b>' . $row->name . '</b>';
                $order_type     = '<span class="text-warning font-weight-bold text-uppercase">' . lang('customer') . '</span>';
                if ($row->id_customer) {
                    $customer   = '<pre class="mb-0">';
                    $customer  .= 'Username : ' . $username . '</b>' . br();
                    $customer  .= '<b>' . $member_name . '</b>' . br();
                    $customer  .= '</pre>';
                    $order_type = '<span class="d-block text-primary font-weight-bold">MEMBER</span><span>( REFERRAL )</span>';
                }

                $datemodified   = date('d M y H:i', strtotime($row->datemodified));
                $dateconfirm    = '-';
                $confirmed_by   = '-';
                $pending_expire = false;
                if ($row->status > 0) {
                    $confirmed_by   = $row->modified_by;
                }
                if ($row->dateconfirmed != '0000-00-00 00:00:00' && $row->status == 1) {
                    $dateconfirm    = date('d M y H:i', strtotime($row->dateconfirmed));
                    $confirmed_by   = $row->confirmed_by;
                }

                $dateexpired    = '-';
                if ($row->dateexpired && $row->dateexpired != '0000-00-00 00:00:00') {
                    $dateexpired    = date('d M y H:i', strtotime($row->dateexpired));
                    if ($row->status == 0 && strtotime($datenow) > strtotime($row->dateexpired)) {
                        // $row->status    = 4;
                        $datemodified   = $dateexpired;
                        $confirmed_by   = 'expired';
                        $pending_expire = true;
                    }
                }

                $total_payment  = bp_accounting($row->total_payment);
                $uniquecode     = $row->unique ? str_pad($row->unique, 3, '0', STR_PAD_LEFT) : '';
                $status         = '';
                if ($row->status == 0) {
                    $status = '<span class="badge badge-default">PENDING</span>';
                }
                if ($row->status == 1) {
                    $status = '<span class="badge badge-info">CONFIRMED</span>';
                }
                if ($row->status == 2) {
                    $status = '<span class="badge badge-success">DONE</span>';
                }
                if ($row->status == 4) {
                    $status = '<span class="badge badge-danger">CANCELLED</span>';
                }

                $btn_invoice    = '<a href="' . base_url('invoicecustomer/' . $id) . '"  target="+_blank"
                                    class="btn btn-sm btn_block btn-outline-default" ><i class="fa fa-file"></i> ' . $row->invoice . '</a>';

                $btn_product    = 'SubTotal : <b>' . bp_accounting($row->subtotal) . '</b>' . br();
                $btn_product    = '<a href="javascript:;" 
                                    data-url="' . base_url('shopping/getshopcustomerdetail/' . $id) . '" 
                                    data-invoice="' . $row->invoice . '"
                                    class="btn btn-sm btn-block btn-outline-primary btn-shop-order-detail">
                                    <i class="ni ni-bag-17 mr-1"></i> Detail Order</a>';


                if (strtolower($row->shipping_method) == 'pickup') {
                    $courier    = '<center><b>PICKUP</b></center>';
                    if ($row->status == 2) {
                        $courier .= '<br><b>Nama Pengambil</b> : <br><span class="text-warning font-weight-bold">' . ($row->resi ? strtoupper($row->resi) : '-') . '</span>';
                    }
                } else {
                    $courier    = '<pre class="mb-0">';
                    if ($row->courier) {
                        $courier .= '<b>' . lang('courier') . '</b> : ' . ((strtolower($row->courier) == 'ekspedisi') ? '-' : strtoupper($row->courier));
                        $courier .= br() . '<b>Layanan</b> : ' . ((strtolower($row->courier) == 'ekspedisi') ? '-' : strtoupper($row->service));
                    }
                    if ($row->status == 2) {
                        $courier .= br() . '<b>Resi</b> : <span class="text-warning font-weight-bold">' . ($row->resi ? strtoupper($row->resi) : '-') . '</span>';
                    }
                    $courier    .= '</pre>';
                }


                $btn_confirm    = $btn_cancel = $btn_payment = '';
                if ($row->status == 0) {
                    $detail     = bp_extract_products_order($row);
                    if ($is_admin && $access) {
                        $btn_cancel = '<a href="javascript:;" 
                                            data-url="' . base_url('shopping/cancelcustomerorder/' . $id) . '" 
                                            data-status="payment"
                                            data-shippingmethod="' . strtolower($row->shipping_method) . '"
                                            data-invoice="' . $row->invoice . '"
                                            data-name="' . $row->name . '"
                                            data-total="' . $total_payment . '"
                                            data-message="Apakah anda yakin akan membatalkan pesanan ini ?"
                                            class="btn btn-sm btn-block btn-outline-warning btn-tooltip btn-shop-order-action" 
                                            title="Batalkan Pesanan"><i class="fa fa-times"></i> Cancel</a>';

                        $btn_confirm = '<a href="javascript:;" 
                                            data-url="' . base_url('shopping/confirmcustomerorder/' . $id) . '" 
                                            data-status="payment"
                                            data-shippingmethod="' . strtolower($row->shipping_method) . '"
                                            data-invoice="' . $row->invoice . '"
                                            data-name="' . $row->name . '"
                                            data-detail=\'' . json_encode($detail) . '\'
                                            data-total="' . $total_payment . '"
                                            data-uniquecode="' . $uniquecode . '"
                                            data-subtotal="' . $row->subtotal . '"
                                            data-shipping="' . $row->shipping . '"
                                            data-discount="' . $row->discount . '"
                                            data-voucher="' . $row->voucher . '"
                                            data-message="Apakah anda yakin akan Konfirmasi pembayaran atas pesanan ini ?"
                                            class="btn btn-sm btn-block btn-default btn-tooltip btn-shop-order-action" 
                                            title="Konfirmasi Pembayaran"><i class="fa fa-check"></i> Konfirmasi</a>';
                    }
                }

                if ($row->status == 1) {
                    $detail     = bp_extract_products_order($row);
                    if ($is_admin && $access) {
                        $btn_confirm = '<a href="javascript:;" 
                                            data-url="' . base_url('shopping/confirmcustomershipping/' . $id) . '" 
                                            data-status="shipping"
                                            data-shippingmethod="' . strtolower($row->shipping_method) . '"
                                            data-invoice="' . $row->invoice . '"
                                            data-name="' . $row->username . '"
                                            data-detail=\'' . json_encode($detail) . '\'
                                            data-total="' . $total_payment . '"
                                            data-uniquecode="' . $uniquecode . '"
                                            data-subtotal="' . $row->subtotal . '"
                                            data-shipping="' . $row->shipping . '"
                                            data-discount="' . $row->discount . '"
                                            data-voucher="' . $row->voucher . '"
                                            data-courier="' . strtoupper($row->courier) . '"
                                            data-service="' . $row->service . '"
                                            data-message="Apakah anda yakin akan Konfirmasi Pengiriman atas pesanan ini ?"
                                            class="btn btn-sm btn-block btn-default btn-tooltip btn-shop-order-action" 
                                            title="Konfirmasi Pengiriman"><i class="fa fa-truck"></i> Kirim Produk</a>';
                    }
                }

                if ($row->status == 2) {
                    $btn_confirm    = '<a href="javascript:;" class="btn btn-sm btn-block btn-outline-success"><i class="fa fa-check"></i> Done</a>';
                }

                if ($pending_expire) {
                    $btn_confirm    = '';
                    $btn_cancel     = '<a href="javascript:;" class="btn btn-sm btn-block btn-outline-danger"><i class="ni ni-time-alarm"></i> expired</a>';
                }

                $datatables     = array(
                    bp_center($i),
                    bp_center($btn_invoice)
                );

                if ($is_admin && $type_order == 'member_to_admin') {
                    $datatables[]   = $customer;
                    $datatables[]   = bp_center($order_type);
                }

                if (!$is_admin && $type_order == 'me_to_admin') {
                    $datatables[]   = bp_center($stockist);
                    $datatables[]   = $stockist_name;
                }

                $datatables[]       = bp_center($btn_product);
                $datatables[]       = '<div style="min-width:50px">' . bp_center(bp_accounting($row->total_qty)) . '</div>';
                $datatables[]       = '<div style="min-width:80px">' . bp_accounting($row->total_bv, '', true) . '</div>';
                $datatables[]       = bp_right($total_payment);

                if (($is_admin && $type_order == 'member_to_stockist') || $type_order == 'me_to_admin' || $type_order == 'me_to_stockist') {
                    $datatables[]   = bp_center($status);
                }

                $datatables[]       = $courier;
                $datatables[]       = bp_center(date('j M y @H:i', strtotime($row->datecreated)));

                if ($status_order == 'pending') {
                    $datatables[]       = bp_center($dateexpired);
                } else {
                    $datatables[]   = bp_center($datemodified);
                    $datatables[]   = bp_center($confirmed_by);
                }

                $datatables[]       = bp_center($btn_confirm . $btn_cancel);

                $records["aaData"][] = $datatables;
                $i++;
            }
        }

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $records["sEcho"]                   = $sEcho;
        $records["iTotalRecords"]           = $iTotalRecords;
        $records["iTotalDisplayRecords"]    = $iTotalRecords;
        $records["load_data"]               = $load_data;
        $records["type_order"]              = $type_order;
        $records["token"]                   = $this->security->get_csrf_hash();;

        echo json_encode($records);
    }

    /**
     * Product Shop List Data function.
     */
    function productshoppinglistdata($type = 'all')
    {
        // This is for AJAX request
        if (!$this->input->is_ajax_request()) exit('No direct script access allowed');

        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            // Set JSON data
            $data = array('status' => 'access_denied', 'url' => base_url('login'));
            die(json_encode($data));
        }

        $current_member     = bp_get_current_member();
        $is_admin           = as_administrator($current_member);
        $type               = $type ? strtolower($type) : 'all';

        // Get Search POST
        $s_product          = $this->input->post('product');
        $s_product          = bp_isset($s_product, '', '', true);
        $s_category         = $this->input->post('category');
        $s_category         = bp_isset($s_category, '', '', true);
        $s_limit            = $this->input->post('limit');
        $s_limit            = bp_isset($s_limit, 12, 12, true);
        $s_offset           = $this->input->post('offset');
        $s_offset           = bp_isset($s_offset, 0, 0, true);
        $s_sortby           = $this->input->post('sortby');
        $s_sortby           = bp_isset($s_sortby, '', '', true);
        $s_orderby          = $this->input->post('orderby');
        $s_orderby          = bp_isset($s_orderby, '', '', true);

        $condition          = ' AND %status% = 1';
        $order_by           = '';
        $params             = array();

        $iTotalRecords      = 0;
        $iDisplayLength     = $s_limit;
        $iDisplayStart      = $s_offset;

        if ($s_sortby && $s_orderby) {
            if (strtolower($s_sortby) ==  'datecreated') {
                $order_by = '%dateupdated% ' . $s_orderby;
            }
            if (strtolower($s_sortby) ==  'price') {
                $order_by = ($current_member->as_stockist >= 1) ? 'price ' . $s_orderby : 'price_member ' . $s_orderby;
            }
        }

        if (!empty($s_product)) {
            $condition .= ' AND %product% LIKE CONCAT("%", ?, "%") ';
            $params[] = $s_product;
        }
        if (!empty($s_category)) {
            $condition .= ' AND %slug_category% = ?';
            $params[] = $s_category;
        }

        $get_products   = $this->Model_Product->get_all_product($s_limit, $s_offset, $condition, $order_by, $params);

        if ($get_products) {
            $iTotalRecords  = bp_get_last_found_rows();
        }

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        if ($iDisplayLength > $end) {
            $iTotalRecords = $end;
        }

        $data["data"]                   = $get_products;
        $data["type"]                   = $type;
        $data["type_member"]            = $current_member->as_stockist;
        $data["displayLimit"]           = $iDisplayLength;
        $data["displayStart"]           = $end;
        $data["totalRecords"]           = $iTotalRecords;
        $data["totalDisplayRecords"]    = $end;
        $data["token"]                  = $this->security->get_csrf_hash();
        $data["displayHTML"]            = $this->load->view(VIEW_BACK . 'shopping/productlists', $data, true);
        unset($data["data"]);

        echo json_encode($data);
        die();
    }

    /**
     * Omzet Order Daily List Data function.
     */
    function omzetorderdailylistdata()
    {
        // This is for AJAX request
        if (!$this->input->is_ajax_request()) exit('No direct script access allowed');

        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            // Set JSON data
            $data = array('status' => 'access_denied', 'data' => '');
            die(json_encode($data));
        }

        $current_member     = bp_get_current_member();
        $is_admin           = as_administrator($current_member);

        $params             = array();
        $condition          = '';
        $total_condition    = '';
        $order_by           = '';
        $iTotalRecords      = 0;

        $iDisplayLength     = intval($_REQUEST['iDisplayLength']);
        $iDisplayStart      = intval($_REQUEST['iDisplayStart']);

        $sAction            = bp_isset($_REQUEST['sAction'], '');
        $sEcho              = intval($_REQUEST['sEcho']);
        $sort               = $_REQUEST['sSortDir_0'];
        $column             = intval($_REQUEST['iSortCol_0']);

        $limit              = ($iDisplayLength == '-1' ? 0 : $iDisplayLength);
        $offset             = $iDisplayStart;

        $s_omzet_generate_min   = $this->input->post('search_omzet_generate_min');
        $s_omzet_generate_min   = bp_isset($s_omzet_generate_min, '', '', true);
        $s_omzet_generate_max   = $this->input->post('search_omzet_generate_max');
        $s_omzet_generate_max   = bp_isset($s_omzet_generate_max, '', '', true);
        $s_omzet_order_min      = $this->input->post('search_omzet_order_min');
        $s_omzet_order_min      = bp_isset($s_omzet_order_min, '', '', true);
        $s_omzet_order_max      = $this->input->post('search_omzet_order_max');
        $s_omzet_order_max      = bp_isset($s_omzet_order_max, '', '', true);
        $s_omzet_min            = $this->input->post('search_omzet_min');
        $s_omzet_min            = bp_isset($s_omzet_min, '', '', true);
        $s_omzet_max            = $this->input->post('search_omzet_max');
        $s_omzet_max            = bp_isset($s_omzet_max, '', '', true);
        $s_percent_min          = $this->input->post('search_percent_min');
        $s_percent_min          = bp_isset($s_percent_min, '', '', true);
        $s_percent_max          = $this->input->post('search_percent_max');
        $s_percent_max          = bp_isset($s_percent_max, '', '', true);
        $s_date_min             = $this->input->post('search_datecreated_min');
        $s_date_min             = bp_isset($s_date_min, '', '', true);
        $s_date_max             = $this->input->post('search_datecreated_max');
        $s_date_max             = bp_isset($s_date_max, '', '', true);

        if (!empty($s_date_min)) {
            $condition .= ' AND %date_omzet% >= ?';
            $params[] = $s_date_min;
        }
        if (!empty($s_date_max)) {
            $condition .= ' AND %date_omzet% <= ?';
            $params[] = $s_date_max;
        }
        if (!empty($s_omzet_generate_min)) {
            $total_condition .= ' AND %subtotal_generate% >= ?';
            $params[] = $s_omzet_generate_min;
        }
        if (!empty($s_omzet_generate_max)) {
            $total_condition .= ' AND %subtotal_generate% <= ?';
            $params[] = $s_omzet_generate_max;
        }
        if (!empty($s_omzet_order_min)) {
            $total_condition .= ' AND %subtotal_order% >= ?';
            $params[] = $s_omzet_order_min;
        }
        if (!empty($s_omzet_order_max)) {
            $total_condition .= ' AND %subtotal_order% <= ?';
            $params[] = $s_omzet_order_max;
        }
        if (!empty($s_omzet_min)) {
            $total_condition .= ' AND %subtotal_omzet% >= ?';
            $params[] = $s_omzet_min;
        }
        if (!empty($s_omzet_max)) {
            $total_condition .= ' AND %subtotal_omzet% <= ?';
            $params[] = $s_omzet_max;
        }
        if (!empty($s_percent_min)) {
            $total_condition .= ' AND %percent% >= ?';
            $params[] = $s_percent_min;
        }
        if (!empty($s_percent_max)) {
            $total_condition .= ' AND %percent% <= ?';
            $params[] = $s_percent_max;
        }

        if (!empty($condition)) {
            $condition = substr($condition, 4);
            $condition = ' WHERE' . $condition;
        }

        if ($column == 1) {
            $order_by .= '%date_omzet% ' . $sort;
        } elseif ($column == 2) {
            $order_by .= '%omzet_generate% ' . $sort;
        } elseif ($column == 3) {
            $order_by .= '%omzet_order% ' . $sort;
        } elseif ($column == 4) {
            $order_by .= '%total_omzet% ' . $sort;
        }

        $data_list          = $this->Model_Shop->get_all_omzet_order_daily($limit, $offset, $condition, $order_by, $total_condition, $params);
        $records            = array();
        $records["aaData"]  = array();

        if (!empty($data_list)) {
            $iTotalRecords  = bp_get_last_found_rows();
            $currency       = config_item('currency');
            $i = $offset + 1;
            foreach ($data_list as $row) {
                $id         = bp_encrypt($row->date_omzet);
                $btn_detail = '<a href="' . base_url('report/omzetorderdailydetail/' . $id) . '" data-id="' . $id . '" class="btn btn-sm btn-primary omzetorderdailydetail"><i class="fa fa-plus"></i> Detail</a>';

                $records["aaData"][] = array(
                    bp_center($i),
                    bp_center(date("Y-m-d", strtotime($row->date_omzet))),
                    bp_right(bp_accounting($row->omzet_generate)),
                    bp_right(bp_accounting($row->omzet_order)),
                    bp_right(bp_accounting($row->total_omzet)),
                    ''
                );
                $i++;
            }
        }

        $end                = $iDisplayStart + $iDisplayLength;
        $end                = $end > $iTotalRecords ? $iTotalRecords : $end;

        $records["sEcho"]                   = $sEcho;
        $records["iTotalRecords"]           = $iTotalRecords;
        $records["iTotalDisplayRecords"]    = $iTotalRecords;

        echo json_encode($records);
    }

    /**
     * Omzet Order Monthly List Data function.
     */
    function omzetordermonthlylistdata()
    {
        // This is for AJAX request
        if (!$this->input->is_ajax_request()) exit('No direct script access allowed');

        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            // Set JSON data
            $data = array('status' => 'access_denied', 'data' => '');
            die(json_encode($data));
        }

        $current_member     = bp_get_current_member();
        $is_admin           = as_administrator($current_member);

        $params             = array();
        $condition          = '';
        $total_condition    = '';
        $order_by           = '';
        $iTotalRecords      = 0;

        $iDisplayLength     = intval($_REQUEST['iDisplayLength']);
        $iDisplayStart      = intval($_REQUEST['iDisplayStart']);

        $sAction            = bp_isset($_REQUEST['sAction'], '');
        $sEcho              = intval($_REQUEST['sEcho']);
        $sort               = $_REQUEST['sSortDir_0'];
        $column             = intval($_REQUEST['iSortCol_0']);

        $limit              = ($iDisplayLength == '-1' ? 0 : $iDisplayLength);
        $offset             = $iDisplayStart;

        $s_omzet_generate_min   = $this->input->post('search_omzet_generate_min');
        $s_omzet_generate_min   = bp_isset($s_omzet_generate_min, '', '', true);
        $s_omzet_generate_max   = $this->input->post('search_omzet_generate_max');
        $s_omzet_generate_max   = bp_isset($s_omzet_generate_max, '', '', true);
        $s_omzet_order_min      = $this->input->post('search_omzet_order_min');
        $s_omzet_order_min      = bp_isset($s_omzet_order_min, '', '', true);
        $s_omzet_order_max      = $this->input->post('search_omzet_order_max');
        $s_omzet_order_max      = bp_isset($s_omzet_order_max, '', '', true);
        $s_omzet_min            = $this->input->post('search_omzet_min');
        $s_omzet_min            = bp_isset($s_omzet_min, '', '', true);
        $s_omzet_max            = $this->input->post('search_omzet_max');
        $s_omzet_max            = bp_isset($s_omzet_max, '', '', true);
        $s_percent_min          = $this->input->post('search_percent_min');
        $s_percent_min          = bp_isset($s_percent_min, '', '', true);
        $s_percent_max          = $this->input->post('search_percent_max');
        $s_percent_max          = bp_isset($s_percent_max, '', '', true);
        $s_date_min             = $this->input->post('search_datecreated_min');
        $s_date_min             = bp_isset($s_date_min, '', '', true);
        $s_date_max             = $this->input->post('search_datecreated_max');
        $s_date_max             = bp_isset($s_date_max, '', '', true);

        if (!empty($s_date_min)) {
            $condition .= ' AND %month_omzet% >= ?';
            $params[] = $s_date_min;
        }
        if (!empty($s_date_max)) {
            $condition .= ' AND %month_omzet% <= ?';
            $params[] = $s_date_max;
        }
        if (!empty($s_omzet_generate_min)) {
            $total_condition .= ' AND %subtotal_generate% >= ?';
            $params[] = $s_omzet_generate_min;
        }
        if (!empty($s_omzet_generate_max)) {
            $total_condition .= ' AND %subtotal_generate% <= ?';
            $params[] = $s_omzet_generate_max;
        }
        if (!empty($s_omzet_order_min)) {
            $total_condition .= ' AND %subtotal_order% >= ?';
            $params[] = $s_omzet_order_min;
        }
        if (!empty($s_omzet_order_max)) {
            $total_condition .= ' AND %subtotal_order% <= ?';
            $params[] = $s_omzet_order_max;
        }
        if (!empty($s_omzet_min)) {
            $total_condition .= ' AND %subtotal_omzet% >= ?';
            $params[] = $s_omzet_min;
        }
        if (!empty($s_omzet_max)) {
            $total_condition .= ' AND %subtotal_omzet% <= ?';
            $params[] = $s_omzet_max;
        }
        if (!empty($s_percent_min)) {
            $total_condition .= ' AND %percent% >= ?';
            $params[] = $s_percent_min;
        }
        if (!empty($s_percent_max)) {
            $total_condition .= ' AND %percent% <= ?';
            $params[] = $s_percent_max;
        }

        if (!empty($condition)) {
            $condition = substr($condition, 4);
            $condition = ' WHERE' . $condition;
        }

        if ($column == 1) {
            $order_by .= '%month_omzet% ' . $sort;
        } elseif ($column == 2) {
            $order_by .= '%omzet_generate% ' . $sort;
        } elseif ($column == 3) {
            $order_by .= '%omzet_order% ' . $sort;
        } elseif ($column == 4) {
            $order_by .= '%total_omzet% ' . $sort;
        }

        $data_list          = $this->Model_Shop->get_all_omzet_order_monthly($limit, $offset, $condition, $order_by, $total_condition, $params);
        $records            = array();
        $records["aaData"]  = array();
        if (!empty($data_list)) {
            $iTotalRecords  = bp_get_last_found_rows();
            $currency       = config_item('currency');
            $i = $offset + 1;
            foreach ($data_list as $row) {
                $id         = bp_encrypt($row->month_omzet);
                $btn_detail = '<a href="' . base_url('report/omzetordermonthlydetail/' . $id) . '" data-id="' . $id . '" class="btn btn-sm btn-primary omzetordermonthlydetail"><i class="fa fa-plus"></i> Detail</a>';

                $records["aaData"][] = array(
                    bp_center($i),
                    bp_center(date("M, Y", strtotime($row->month_omzet))),
                    bp_right(bp_accounting($row->omzet_generate)),
                    bp_right(bp_accounting($row->omzet_order)),
                    bp_right(bp_accounting($row->total_omzet)),
                    ''
                );
                $i++;
            }
        }

        $end                = $iDisplayStart + $iDisplayLength;
        $end                = $end > $iTotalRecords ? $iTotalRecords : $end;

        $records["sEcho"]                   = $sEcho;
        $records["iTotalRecords"]           = $iTotalRecords;
        $records["iTotalDisplayRecords"]    = $iTotalRecords;

        echo json_encode($records);
    }

    // =============================================================================================
    // CART
    // =============================================================================================

    /**
     * Add To Cart
     */
    public function addToCart()
    {
        // This is for AJAX request
        if (!$this->input->is_ajax_request()) exit('No direct script access allowed');

        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            // Set JSON data
            $data = array('status' => 'access_denied', 'url' => base_url('login'));
            die(json_encode($data));
        }

        $current_member = bp_get_current_member();
        $is_admin       = as_administrator($current_member);

        $token          = $this->security->get_csrf_hash();
        $data           = array(
            'status'    => 'error',
            'token'     => $token,
            'message'   => 'Added to cart Failed!'
        );

        // Get Form POST
        $id             = $this->input->post('id');
        $id             = bp_isset($id, '', '', true);
        $id             = bp_decrypt($id);
        $qty            = $this->input->post('qty');
        $qty            = bp_isset($qty, '', '', true);

        if (!$qty || $qty == 0) {
            $data['message'] = 'Added to cart Failed! (Qty cannot zero)';
            die(json_encode($data));
        }

        $productdata    = bp_products($id, true);
        if (!$productdata) {
            $data['message'] = 'Added to cart Failed! (product not found)';
            die(json_encode($data));
        }

        $product_title  = $productdata->name;
        $product_slug   = url_title($product_title, 'dash', TRUE);

        // Check Price
        $price          = ($current_member->as_stockist >= 2) ? $productdata->price : $productdata->price_member;

        // Check Weight
        $weight         = $productdata->weight;
        $total_weight   = $weight * $qty;
        $product_weight = $total_weight;
        $product_type   = 'shop';

        // Check Type Product On Cart
        $cart_contents  = $this->cart->contents();
        if ($cart_contents) {
            foreach ($cart_contents as $item) {
                $_id    = isset($item['id']) ? $item['id'] : 'none';
                $_type  = isset($item['type']) ? $item['type'] : 'none';
            }
        }

        // Set Data Product Add To Cart
        $data_cart      = array(
            'id'        => $productdata->id,
            'type'      => $product_type,
            'name'      => $product_slug,
            'title'     => $product_title,
            'qty'       => $qty,
            'price'     => $price,
            'subtotal'  => ($price * $qty),
            'options'   => array(
                'weight' => $total_weight,
                'product_weight' => $product_weight,
            )
        );

        $insert_cart    = $this->cart->insert($data_cart);
        if ($insert_cart) {
            $data['status']     = 'success';
            $data['message']    = 'Success Added to Cart!';
            $data['total_item'] = count($this->cart->contents());
            $data['total_qty']  = $this->cart->total_items();
            $data['url_cart']   = base_url('shopping/cart');
        } else {
            $data['status']     = 'error';
            $data['message']    = 'Failed Added to Cart!';
        }
        // $data['data_cart']  = $data_cart;
        die(json_encode($data));
    }

    /**
     * Update Qty and checking stock availability
     */
    function updateQtyCart()
    {
        // This is for AJAX request
        if (!$this->input->is_ajax_request()) exit('No direct script access allowed');

        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            // Set JSON data
            $data = array('status' => 'access_denied', 'url' => base_url('login'));
            die(json_encode($data));
        }

        $current_member = bp_get_current_member();
        $is_admin       = as_administrator($current_member);

        $token          = $this->security->get_csrf_hash();
        $data           = array(
            'status'    => 'error',
            'token'     => $token,
            'message'   => 'Failed to Update Qty!'
        );

        $rowid          = $this->input->post('rowid');
        $rowid          = bp_isset($rowid, '', '', true);
        $product_id     = $this->input->post('productid');
        $product_id     = bp_isset($product_id, '', '', true);
        $product_id     = bp_decrypt($product_id);
        $qty            = $this->input->post('qty');
        $qty            = bp_isset($qty, '', '', true);

        $productdata    = bp_products($product_id);
        if (!$productdata) {
            $data['message'] = 'Product not found';
            die(json_encode($data));
        }

        // Check Price
        $price          = ($current_member->as_stockist >= 2) ? $productdata->price : $productdata->price_member;
        $subtotal       = $price * $qty;

        // Check Weight
        $weight         = $productdata->weight;
        $total_weight   = $weight * $qty;
        $product_weight = $total_weight;

        $data_cart      = array(
            'rowid'     => $rowid,
            'qty'       => $qty,
            'price'     => $price,
            'options'   => array(
                'weight'         => $product_weight,
                'product_weight' => $product_weight,
            )
        );

        // update cart
        $update_cart    = $this->cart->update($data_cart);

        if ($update_cart) {
            $data['status']     = 'success';
            $data['message']    = 'Success Update Qty Cart!';
        } else {
            $data['status']     = 'error';
            $data['message']    = 'Failed Added to Cart!';
        }

        $total_payment          = $this->cart->total();
        $view_price             = bp_accounting($price, '', true);
        $view_subtotal          = bp_accounting($subtotal, '', true);
        $view_total             = bp_accounting($total_payment, '', true);

        $data['price_cart']     = $view_price;
        $data['subtotal_cart']  = $view_subtotal;
        $data['total_cart']     = $view_total;
        $data['total_qty']      = $this->cart->total_items();
        $data['total_item']     = count($this->cart->contents());

        die(json_encode($data));
    }

    /**
     * Delete Product Cart
     */
    public function deleteCart($rowid = '')
    {
        // This is for AJAX request
        if (!$this->input->is_ajax_request()) exit('No direct script access allowed');

        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            // Set JSON data
            $data = array('status' => 'access_denied', 'url' => base_url('login'));
            die(json_encode($data));
        }
        $this->load->helper('shop_helper');


        $current_member = bp_get_current_member();
        $is_admin       = as_administrator($current_member);

        $token          = $this->security->get_csrf_hash();
        $data           = array(
            'status'    => 'error',
            'token'     => $token,
            'message'   => 'Failed Delete Product Cart!'
        );

        // --------------------------------------
        // Get Cart Content
        // --------------------------------------
        $cart_content   = bp_cart_contents();
        $product_cart   = isset($cart_content['data']) ? $cart_content['data'] : array();
        $product_type   = isset($cart_content['product_type']) ? $cart_content['product_type'] : '';

        // Get Input POST
        $id             = $this->input->post('rowid');
        $id             = bp_isset($id, '', '', true);
        $rowid          = $rowid ? $rowid : $id;

        $product_cart   = array(
            'rowid'     => $rowid,
            'qty'       => 0,
        );
        $delete_cart = $this->cart->update($product_cart);

        if ($delete_cart) {
            $data['status']     = 'success';
            $data['message']    = 'Success Delete Product Cart!';
        }

        $total_payment          = $this->cart->total();
        $cart_payment           = bp_accounting($total_payment, '', true);

        $data['total_cart']     = $cart_payment;
        $data['total_qty']      = $this->cart->total_items();
        $data['total_item']     = count($this->cart->contents());

        die(json_encode($data, true));
    }

    /**
     * Destroy Cart
     */
    public function emptyCart()
    {
        // This is for AJAX request
        if (!$this->input->is_ajax_request()) exit('No direct script access allowed');

        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            // Set JSON data
            $data = array('status' => 'access_denied', 'url' => base_url('login'));
            die(json_encode($data));
        }

        $current_member = bp_get_current_member();
        $is_admin       = as_administrator($current_member);

        // Empty Cart
        $this->cart->destroy();

        $token          = $this->security->get_csrf_hash();
        $data           = array(
            'status'    => 'success',
            'token'     => $token,
            'message'   => 'Cart Empty'
        );
        die(json_encode($data));
    }

    /**
     * Select Agent
     */
    public function selectagent($id = 0)
    {
        auth_redirect();

        $this->load->helper('shop_helper');
        $this->load->library('user_agent');

        $current_member = bp_get_current_member();
        $is_admin       = as_administrator($current_member);
        $refer          = '';

        if ($this->agent->is_referral()) {
            $refer      =  $this->agent->referrer();
        }

        if ($is_admin) {
            $refer      = $refer ? $refer : base_url('dashboard');
            redirect($refer, 'refresh');
        }

        $refer          = $refer ? $refer : base_url('cart');
        if ($current_member->as_stockist >= 2) {
            redirect($refer, 'refresh');
        }

        if (!$id) {
            redirect($refer, 'refresh');
        }

        $id_member      = bp_decrypt($id);
        $apply_agent    = apply_code_seller($id_member, $current_member->as_stockist);
        if ($apply_agent) {
            $refer      =  base_url('checkout');
        }
        redirect($refer, 'refresh');
    }

    /**
     * Clear/Remove Select Agent
     */
    public function shoppingclearagent()
    {
        auth_redirect();

        $this->load->helper('shop_helper');
        $this->load->library('user_agent');

        $current_member = bp_get_current_member();
        $is_admin       = as_administrator($current_member);
        $refer          = '';

        if ($this->agent->is_referral()) {
            $refer      =  $this->agent->referrer();
        }

        if ($is_admin) {
            $refer      = $refer ? $refer : base_url('dashboard');
            redirect($refer, 'refresh');
        }

        $refer          = $refer ? $refer : base_url('checkout');
        if ($current_member->as_stockist > 1) {
            redirect($refer, 'refresh');
        }

        remove_code_seller();
        redirect($refer, 'refresh');
    }

    // =============================================================================================
    // CHECKOUT
    // =============================================================================================

    /**
     * Checkout Cart
     */
    public function checkout()
    {
        // This is for AJAX request
        if (!$this->input->is_ajax_request()) exit('No direct script access allowed');

        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            // Set JSON data
            $data = array('status' => 'access_denied', 'url' => base_url('login'));
            die(json_encode($data));
        }

        $token          = $this->security->get_csrf_hash();
        $data           = array(
            'status'    => 'error',
            'token'     => $token,
            'message'   => 'Checkout tidak berhasil. Silahkan periksa kembali keranjang belanjaan anda!'
        );

        $this->load->helper('shop_helper');

        $current_member         = bp_get_current_member();
        $is_admin               = as_administrator($current_member);
        $as_stockist            = $current_member->as_stockist;
        $created_by             = strtolower($current_member->username);
        $datetime               = date('Y-m-d H:i:s');
        $dateexpired            = date('Y-m-d H:i:s', strtotime('+2 day'));

        $cfg_min_order_qty      = 0;
        $cfg_min_order_nominal  = 0;
        if ($current_member->as_stockist >= 2) {
            $cfg_min_order_qty      = get_option('cfg_stockist_minimal_order_qty');
            $cfg_min_order_qty      = is_numeric($cfg_min_order_qty) ? $cfg_min_order_qty : 0;
            $cfg_min_order_nominal  = get_option('cfg_stockist_minimal_order_nominal');
            $cfg_min_order_nominal  = is_numeric($cfg_min_order_nominal) ? $cfg_min_order_nominal : 0;
        }

        if ($is_admin) {
            $data['message'] = 'Maaf, Admin tidak dapat pesan produk!';
            die(json_encode($data));
        }

        // --------------------------------------
        // Get Cart Content
        // --------------------------------------
        $cart_content   = bp_cart_contents();
        if (!$cart_content) {
            die(json_encode($data));
        }

        if (isset($cart_content['has_error']) && $cart_content['has_error']) {
            die(json_encode($data));
        }

        $product_cart   = isset($cart_content['data']) ? $cart_content['data'] : array();
        $product_type   = isset($cart_content['product_type']) ? $cart_content['product_type'] : '';

        if (!$product_cart) {
            die(json_encode($data));
        }

        if ($current_member->as_stockist >= 2) {
            if ($cfg_min_order_qty || $cfg_min_order_nominal) {
                if ($cfg_min_order_qty > $this->cart->total_items()) {
                    $data['message'] = 'Minimal qty pembelanjaan <b>' . bp_accounting($cfg_min_order_qty) . ' Produk</b>';
                    die(json_encode($data));
                }
                if ($cfg_min_order_nominal > $this->cart->total()) {
                    $data['message'] = 'Minimal belanja sebesar <b>' . bp_accounting($cfg_min_order_nominal, config_item('currency')) . '</b>';
                    die(json_encode($data));
                }
            }
        }

        // POST Input Form
        $payment_method         = trim($this->input->post('payment_method'));
        $payment_method         = bp_isset($payment_method, '', '', true);
        $payment_method         = strtolower($payment_method);
        $shipping_method        = trim($this->input->post('shipping_method'));
        $shipping_method        = bp_isset($shipping_method, '', '', true);
        $shipping_method        = strtolower($shipping_method);

        $name                   = trim($this->input->post('name'));
        $name                   = bp_isset($name, '', '', true);
        $phone                  = trim($this->input->post('phone'));
        $phone                  = bp_isset($phone, '', '', true);
        $email                  = trim($this->input->post('email'));
        $email                  = bp_isset($email, '', '', true);
        $province               = trim($this->input->post('province'));
        $province               = bp_isset($province, '', '', true);
        $district               = trim($this->input->post('district'));
        $district               = bp_isset($district, '', '', true);
        $subdistrict            = trim($this->input->post('subdistrict'));
        $subdistrict            = bp_isset($subdistrict, '', '', true);
        $village                = trim($this->input->post('village'));
        $village                = bp_isset($village, '', '', true);
        $address                = trim($this->input->post('address'));
        $address                = bp_isset($address, '', '', true);
        $courier                = trim($this->input->post('select_courier'));
        $courier                = bp_isset($courier, '', '', true);
        $service                = trim($this->input->post('select_service'));
        $service                = bp_isset($service, '', '', true);
        $courier_cost           = trim($this->input->post('courier_cost'));
        $courier_cost           = bp_isset($courier_cost, 0, 0, true);
        $courier_cost           = $courier_cost ? $courier_cost : 0;

        $this->form_validation->set_rules('payment_method', 'Metode Pembayaran', 'required');
        $this->form_validation->set_rules('shipping_method', 'Metode Pengiriman', 'required');
        if ($shipping_method == 'ekspedisi') {
            $this->form_validation->set_rules('name', 'Nama', 'required');
            $this->form_validation->set_rules('phone', 'No. Hp/WA', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required');
            $this->form_validation->set_rules('province', 'Provinsi', 'required');
            $this->form_validation->set_rules('district', 'Kota/Kabupaten', 'required');
            $this->form_validation->set_rules('subdistrict', 'Kecamatan', 'required');
            $this->form_validation->set_rules('village', 'Kelurahan/Desa', 'required');
            $this->form_validation->set_rules('address', 'Alamat', 'required');

            if ($current_member->as_stockist < 2) {
                $this->form_validation->set_rules('select_courier', 'Kurir', 'required');
                $this->form_validation->set_rules('select_service', 'Layanan Kurir', 'required');
            }
        }

        $this->form_validation->set_message('required', '%s harus di isi');
        $this->form_validation->set_error_delimiters('* ', br());

        if ($this->form_validation->run() == FALSE) {
            $data['message'] = 'Checkout tidak berhasil. ' . br() . validation_errors();
            die(json_encode($data));
        }

        if ($phone) {
            if (substr($phone, 0, 1) != '0') {
                $phone      = '0' . $phone;
            }
        }

        if ($shipping_method != 'ekspedisi') {
            $province       = ($province) ? $province : $memberdata->province;
            $district       = ($district) ? $district : $memberdata->district;
            $subdistrict    = ($subdistrict) ? $subdistrict : $memberdata->subdistrict;
            $village        = ($village) ? $village : $memberdata->village;
            $address        = ($address) ? $address : $memberdata->address;
        }

        // -------------------------------------------------
        // Check Province, District, SubDistrict
        // -------------------------------------------------
        $province_name  = '';
        if ($province && $get_province = bp_provinces($province)) {
            $province_name = $get_province->province_name;
        }

        // -------------------------------------------------
        // Check District Code
        // -------------------------------------------------
        $district_name  = '';
        if ($district && $get_district = bp_districts($district)) {
            $district_name = $get_district->district_type . ' ' . $get_district->district_name;
        }

        // -------------------------------------------------
        // Check SubDistrict
        // -------------------------------------------------
        $subdistrict_name  = '';
        if ($subdistrict && $get_subdistrict = bp_subdistricts($subdistrict)) {
            $subdistrict_name = $get_subdistrict->subdistrict_name;
        }

        if ($shipping_method == 'ekspedisi') {
            if (!$province_name) {
                $data['message'] = 'Provinsi tidak ditemukan atau belum terdaftar!';
                die(json_encode($data)); // Set JSON data
            }
            if (!$district_name) {
                $data['message'] = 'Kode Kota/Kabupaten tidak ditemukan atau belum terdaftar!';
                die(json_encode($data)); // Set JSON data
            }
            if (!$subdistrict_name) {
                $data['message'] = 'Kecamatan tidak ditemukan atau belum terdaftar!';
                die(json_encode($data)); // Set JSON data
            }
        }

        // --------------------------------------
        // Product Cart
        // --------------------------------------
        $cart_total_qty     = $this->cart->total_items();
        $cart_total_payment = $this->cart->total();

        // Set Product
        $product_detail     = array();
        $total_bv           = 0;
        $total_qty          = 0;
        $total_price        = 0;
        $total_weight       = 0;
        $total_payment      = 0;

        foreach ($product_cart as $key => $row) {
            $_id            = isset($row['id']) ? $row['id'] : 0;
            $_qty           = isset($row['qty']) ? $row['qty'] : 0;
            $_price         = isset($row['cart_price']) ? $row['cart_price'] : 0;
            if (!$_id || !$_qty || !$_price) {
                continue;
            }
            if (!$productdata = bp_products($_id)) {
                continue;
            }

            $subtotal           = $_qty * $_price;
            $subtotal_bv        = $_qty * $productdata->bv;
            $product_weight     = $_qty * $productdata->weight;
            $product_price      = ($as_stockist >= 2) ? $productdata->price : $productdata->price_member;

            // Set Product Detail
            $product_detail[]   = array(
                'id'                => $_id,
                'name'              => $productdata->name,
                'bv'                => $productdata->bv,
                'qty'               => $_qty,
                'price'             => $product_price,      // original price
                'price_cart'        => $_price,                 // cart price
                'discount'          => 0,
                'subtotal'          => $subtotal,
                'weight'            => $product_weight
            );

            $total_bv           += $subtotal_bv;
            $total_qty          += $_qty;
            $total_price        += $subtotal;
            $total_weight       += $product_weight;
        }

        if (!$product_detail) {
            die(json_encode($data));
        }

        if ($cart_total_qty != $total_qty) {
            die(json_encode($data));
        }

        if ($cart_total_payment != $total_price) {
            die(json_encode($data));
        }

        // --------------------------------------
        // Check Code Checkout
        // --------------------------------------
        $set_checkout_code  = $product_type . $current_member->id;
        $get_checkout_code  = $this->session->userdata('checkout_code');
        if ($set_checkout_code != $get_checkout_code) {
            $data['message'] = 'Checkout tidak berhasil. Failed Code.';
            die(json_encode($data));
        }

        // --------------------------------------
        // Check Apply Sub/Agency
        // --------------------------------------
        $id_stockist        = 0;
        $username_stockist  = '';
        $checkStockist      = false;
        if ($as_stockist < 2) {
            $checkStockist  = bp_check_agent(true);
            if ($checkStockist) {
                $id_stockist        = isset($checkStockist->id) ? $checkStockist->id : 0;
                $username_stockist  = isset($checkStockist->username) ? $checkStockist->username : '';
            }

            if ($as_stockist < 2) {
                if (!$checkStockist || !$id_stockist || !$username_stockist) {
                    $data['message'] = 'Anda belum pilih <b>Stockist</b>. Silahkan pilih <b>Stockist</b> terlebih dahulu untuk memesan produk !';
                    die(json_encode($data));
                }
            }
        }

        // --------------------------------------
        // Check Saldo Type
        // --------------------------------------
        $saldo                  = 0;
        $status                 = 0;
        if ($payment_method == 'deposite') {
            if ($total_price > $saldo) {
                $data['message'] = 'Checkout tidak berhasil. Saldo Anda tidak mencukupi untuk belanja produk ini !';
                die(json_encode($data));
            }
        }

        // -------------------------------------------------
        // Transaction Begin
        // -------------------------------------------------
        $this->db->trans_begin();

        $type_order             = ($as_stockist) ? 'stockist_order' : 'member_order';
        $type_order             = ($as_stockist == 1) ? 'mobile_order' : $type_order;
        $code_unique            = 0;
        if ($id_stockist > 0) {
            $invoice            = bp_generate_member_invoice($id_stockist);
            $code_unique        = bp_generate_member_uniquecode($id_stockist);
        } else {
            $invoice            = bp_generate_shop_invoice();
            $code_unique        = bp_generate_shop_order();
        }

        $total_payment          = $total_price + $code_unique + $courier_cost;

        $data_shop_order        = array(
            'invoice'           => $invoice,
            'id_member'         => $current_member->id,
            'id_stockist'       => $id_stockist,
            'type_order'        => $type_order,
            'products'          => maybe_serialize($product_detail),
            'total_bv'          => $total_bv,
            'total_qty'         => $total_qty,
            'subtotal'          => $total_price,
            'unique'            => $code_unique,
            'shipping'          => $courier_cost,
            'discount'          => 0,
            'total_payment'     => $total_payment,
            'status'            => $status,
            'weight'            => $total_weight,
            'payment_method'    => $payment_method,
            'shipping_method'   => $shipping_method,
            'voucher'           => '',
            'name'              => $name,
            'phone'             => $phone,
            'email'             => $email,
            'id_province'       => $province,
            'id_district'       => $district,
            'id_subdistrict'    => $subdistrict,
            'province'          => $province_name,
            'district'          => $district_name,
            'subdistrict'       => $subdistrict_name,
            'village'           => $village,
            'address'           => $address,
            'courier'           => $courier,
            'service'           => $service,
            'datecreated'       => $datetime,
            'datemodified'      => $datetime,
            'dateexpired'       => $dateexpired,
            'created_by'        => $created_by,
        );

        // -------------------------------------------------
        // Save Shop Order
        // -------------------------------------------------
        $saved_shop_id = $this->Model_Shop->save_data_shop_order($data_shop_order);
        if (!$saved_shop_id) {
            $this->db->trans_rollback(); // Rollback Transaction
            $data['message'] = 'Checkout tidak berhasil. Terjadi kesalahan pada proses data transaksi.';
            die(json_encode($data));
        }

        // Set shop order detail
        $data_shop_detail       = array();
        foreach ($product_detail as $prodkey => $val) {
            $data_shop_detail[]     = array(
                'id_shop_order'     => $saved_shop_id,
                'id_member'         => $current_member->id,
                'product'           => $val['id'],
                'bv'                => $val['bv'],
                'qty'               => $val['qty'],
                'price'             => $val['price'],
                'price_cart'        => $val['price_cart'],
                'discount'          => $val['discount'],
                'subtotal'          => $val['subtotal'],
                'weight'            => $val['weight'],
                'datecreated'       => $datetime,
                'datemodified'      => $datetime,
            );
        }

        if (!$data_shop_detail) {
            $this->db->trans_rollback(); // Rollback Transaction
            $data['message'] = 'Checkout tidak berhasil. Terjadi kesalahan pada proses data transaksi produk detail.';
            die(json_encode($data));
        }

        foreach ($data_shop_detail as $row) {
            // -------------------------------------------------
            // Save Shop Order Detail
            // -------------------------------------------------
            $saved_shop_detail  = $this->Model_Shop->save_data_shop_order_detail($row);

            if (!$saved_shop_detail) {
                $this->db->trans_rollback(); // Rollback Transaction
                $data['message'] = 'Checkout tidak berhasil. Terjadi kesalahan pada proses data transaksi produk detail.';
                die(json_encode($data));
            }
        }

        ## Order Success -------------------------------------------------------
        $this->db->trans_commit();
        $this->db->trans_complete();    // complete database transactions  
        $this->cart->destroy();         // Empty Product Cart

        remove_code_discount();
        remove_code_seller();

        $data_log   = array(
            'cookie'        => $_COOKIE,
            'status'        => 'SUCCESS',
            'shop_order_id' => $saved_shop_id,
            'id_stockist'   => $id_stockist,
            'product_detail' => $product_detail,
            'total_payment' => $total_payment,
        );

        bp_log_action('SHOP', $invoice, $current_member->username, json_encode($data_log));

        // Send Notif
        if ($shop_order = $this->Model_Shop->get_shop_orders($saved_shop_id)) {
            $this->bp_email->send_email_shop_order($current_member, $shop_order);
            $this->bp_wa->send_wa_shop_order($current_member, $shop_order);
            if ($id_stockist) {
                if ($stockistdata = bp_get_memberdata_by_id($id_stockist)) {
                    $this->bp_email->send_email_shop_order_stockist($stockistdata, $shop_order);
                    $this->bp_wa->send_wa_shop_order_stockist($stockistdata, $shop_order);
                }
            }
        }

        $data['status']     = 'success';
        $data['message']    = 'Checkout berhasil';
        $data['url']        = base_url('shopping/shophistorylist');
        die(json_encode($data));
    }

    // =============================================================================================
    // ACTION SHOPPING
    // =============================================================================================

    /**
     * Confirm Shop Order Function
     */
    function confirmorder($id = 0)
    {
        if (!$this->input->is_ajax_request()) {
            redirect(base_url('shop/sales'), 'refresh');
        }
        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            $data = array('status' => 'access_denied', 'url' => base_url('login'));
            die(json_encode($data)); // JSON encode data
        }

        $bp_token   = $this->security->get_csrf_hash();
        $data       = array('status' => 'error', 'token' => $bp_token, 'message' => 'ID Pesanan tidak dikenali. Silahkan pilih Pesanan lainnya untuk dikonfirmasi');

        if (!$id) {
            die(json_encode($data));
        }

        $current_member     = bp_get_current_member();
        $is_admin           = as_administrator($current_member);
        $id                 = bp_decrypt($id);
        $confirmed_by       = $current_member->username;
        $datetime           = date('Y-m-d H:i:s');

        // POST Input Form
        $password           = trim($this->input->post('password'));
        $password           = bp_isset($password, '', '', true);

        if (!$password) {
            $data['message'] = 'Password harus diisi !';
            die(json_encode($data));
        }

        if (!$shop_order = $this->Model_Shop->get_shop_orders($id)) {
            die(json_encode($data));
        }

        $shop_detail        = $this->Model_Shop->get_shop_detail_by('id_shop_order', $id);

        // Set Data Shop Order
        $invoice            = $shop_order->invoice;
        $id_stockist        = $shop_order->id_stockist;
        $total_nominal      = $shop_order->total_payment;
        $product_detail     = maybe_unserialize($shop_order->products);

        if ($id_stockist > 0) {
            if ($is_admin) {
                $data['message'] = 'Maaf, Admin tidak dapat Konfirmasi Pesanan Member ke Stockist.';
                die(json_encode($data));
            }

            if ($id_stockist != $current_member->id) {
                $data['message'] = 'Maaf, Anda tidak dapat Konfirmasi Pesanan ini.';
                die(json_encode($data));
            }
        } else {
            if (!$is_admin) {
                $data['message'] = 'Maaf, hanya Administrator yang dapat Konfirmasi Pesanan Produk ini !';
                die(json_encode($data));
            }
        }

        if ($my_account = bp_get_memberdata_by_id($current_member->id)) {
            $my_password    = $my_account->password;
        }

        if ($is_admin && $id_stockist == 0) {
            if ($staff = bp_get_current_staff()) {
                $confirmed_by   = $staff->username;
                $my_password    = $staff->password;
            }
        }

        $password           = trim($password);
        $password_md5       = md5($password);
        $pwd_valid          = false;

        if ($password_md5 == $my_password) {
            $pwd_valid  = true;
        }

        if (bp_hash_verify($password, $my_password)) {
            $pwd_valid  = true;
        }

        // if ( $password_global = config_item('password_global') ) {
        //     if ( bp_hash_verify($password, $password_global) ) {
        //         $pwd_valid  = true;
        //     }
        // }

        // Set Log Data
        $status_msg             = '';
        $log_data               = array('cookie' => $_COOKIE);
        $log_data['id_shop']    = $id;
        $log_data['invoice']    = $shop_order->invoice;
        $log_data['status']     = 'Konfirmasi Pesanan Produk';

        if (!$pwd_valid) {
            $log_data['message']    = 'invalid password';
            $data['message']        = 'Maaf, Password anda tidak valid !';
            if ($shop_order->status == 0) {
                bp_log_action('SHOP_CONFIRM_ORDER', 'ERROR', $confirmed_by, json_encode($log_data));
            }
            die(json_encode($data));
        }

        if ($shop_order->status == 1) {
            $data['message'] = 'Status Pesanan sudah dikonfirmasi.';
            die(json_encode($data));
        }

        if ($shop_order->status != 0) {
            $data['message'] = 'Pesanan tidak dapat dikonfirmasi.';
            die(json_encode($data));
        }

        if (!$memberdata = bp_get_memberdata_by_id($shop_order->id_member)) {
            $data['message'] = 'Konfirmasi Pesanan tidak berhasil. Mitra tidak dikenali.';
            die(json_encode($data));
        }

        // Check Stock Produk
        if ($is_admin && $id_stockist == 0) {
            if (!$shop_detail) {
                $data['message'] = 'Konfirmasi Pesanan tidak berhasil. Data detail pesanan produk tidak ditemukan.';
                die(json_encode($data));
            }
            // foreach ($shop_detail as $key => $row) {
            //     $product_id     = $row->product;
            //     $product_qty    = $row->qty;
            //     $product_stock  = bp_stock_product($product_id);
            //     if ( $product_qty > $product_stock ) {
            //         $data['message'] = 'Konfirmasi Pesanan tidak berhasil. Qty pesanan produk melebihi total stok produk yang ada.';
            //         $get_product = bp_products($product_id);
            //         if ( $get_product ) {
            //             if ( $product_stock > 0) {
            //                 $data['message'] = 'Jumlah pesanan Produk '.$get_product->name.' ('.$product_qty.' qty) melebihi total stok Produk yang pusat miliki ('.$product_stock.' qty) !';
            //             } else {
            //                 $data['message'] = 'Pusat tidak memiliki Stok Produk '.$get_product->name;
            //             }
            //         }
            //         die(json_encode($data));
            //     }
            // }
        }

        // Check Stock Produk stockist
        if (!$is_admin && $id_stockist > 0) {
            if (!$shop_detail) {
                $data['message'] = 'Konfirmasi Pesanan tidak berhasil. Data detail pesanan produk tidak ditemukan.';
                die(json_encode($data));
            }

            foreach ($shop_detail as $key => $row) {
                $product_id     = $row->product;
                $product_qty    = $row->qty;
                $product_name   = '';

                if (!$product_id || !$product_qty) {
                    continue;
                }
                if ($productdata = bp_products($product_id)) {
                    $product_name = $productdata->name;
                }

                $total_my_pin   = bp_member_pin($id_stockist, 'active', true, $product_id);
                $total_my_pin   = $total_my_pin ? $total_my_pin : 0;

                if (!$total_my_pin) {
                    $data['message'] = 'Konfirmasi pesanan tidak berhasil. Anda tidak memiliki stok Produk <b>' . $product_name . '</b>';
                    die(json_encode($data));
                }

                if ($product_qty > $total_my_pin) {
                    $data['message'] = 'Konfirmasi pesanan tidak berhasil. Jumlah pesanan Produk <b>' . $product_name . '</b> (' . $qty . ') melebihi total stok Produk yang Anda miliki (' . $total_my_pin . ') !';
                    die(json_encode($data));
                }
            }
        }

        // Begin Transaction
        $this->db->trans_begin();

        // Update status shop order
        $data_order     = array(
            'status'        => 1,
            'datemodified'  => $datetime,
            'dateconfirmed' => $datetime,
            'confirmed_by'  => $confirmed_by,
            'modified_by'   => $confirmed_by,
        );

        if (!$update_shop_order = $this->Model_Shop->update_data_shop_order($id, $data_order)) {
            $this->db->trans_rollback();
            $data['message'] = 'Terjadi kesalahan sistem! Ulangi proses beberapa saat lagi.';
            die(json_encode($data)); // JSON encode data
        }

        // PIN Generate (Admin to Stockist)
        if ($is_admin && $id_stockist == 0) {
            $repl_invoice           = str_replace('INV/', '', $invoice);
            $repl_invoice           = absint($repl_invoice);
            $len_invoice            = strlen($repl_invoice);
            $len_id                 = strlen($memberdata->id);
            $len_string             = $len_invoice + $len_id;
            $len_rand               = ($len_string >= 12) ? 3 : (15 - $len_string);

            $data_pin               = array();
            foreach ($shop_detail as $key => $row) {
                $product_id = $row->product;
                $qty        = $row->qty;

                if (!$product_id || !$qty) {
                    continue;
                }

                // Set data pin
                for ($i = 1; $i <= $qty; $i++) {
                    $code_string        = bp_generate_rand_string($len_rand);
                    $uniquecode         = 'PO' . $repl_invoice . $memberdata->id . $product_id . $code_string;
                    $data_pin[]         = array(
                        'id_pin'            => strtoupper($uniquecode),
                        'id_order_pin'      => $id,
                        'id_member'         => $memberdata->id,
                        'id_member_owner'   => $memberdata->id,
                        'product'           => $product_id,
                        'bv'                => $row->bv,
                        'amount'            => $row->price,
                        'status'            => 1,
                        'datecreated'       => $datetime,
                        'datemodified'      => $datetime,
                    );
                }
            }

            if (!$data_pin) {
                // Rollback Transaction
                $this->db->trans_rollback();
                $data['message'] = 'Konfirmasi Pesanan tidak berhasil. Terjadi kesalahan pada data transaksi buat pin !';
                die(json_encode($data));
            }

            // save data pin
            foreach ($data_pin as $row) {
                if (!$pin_saved = $this->Model_Shop->save_data_pin($row)) {
                    // Rollback Transaction
                    $this->db->trans_rollback();
                    $data['message'] = 'Konfirmasi Pesanan tidak berhasil. Terjadi kesalahan pada data transaksi buat pin !';
                    die(json_encode($data));
                }
            }
        }

        // PIN Transfer (Stockist to Member)
        if (!$is_admin && $id_stockist > 0) {
            foreach ($shop_detail as $key => $row) {
                $product_id     = $row->product;
                $product_qty    = $row->qty;

                if (!$product_id || !$product_qty) {
                    continue;
                }

                $my_pin_active  = bp_member_pin($id_stockist, 'active', false, $product_id);
                $total_my_pin   = $my_pin_active ? count($my_pin_active) : 0;

                if (!$my_pin_active || !$total_my_pin) {
                    $this->db->trans_rollback();
                    $data['message'] = 'Konfirmasi pesanan tidak berhasil. Terjadi kesalahan pada data transaksi transfer pin !';
                    die(json_encode($data));
                }

                if ($product_qty > $total_my_pin) {
                    $this->db->trans_rollback();
                    $data['message'] = 'Konfirmasi pesanan tidak berhasil. Terjadi kesalahan pada data transaksi transfer pin !';
                    die(json_encode($data));
                }

                // Select some pins
                $transferred_pins       = array_slice($my_pin_active, 0, $product_qty);
                $transferred_pin_ids    = array();
                foreach ($transferred_pins as $pin) {
                    $data_pin_transfer  = array(
                        'id_member_sender'      => $current_member->id,
                        'username_sender'       => $current_member->username,
                        'id_member'             => $memberdata->id,
                        'username'              => $memberdata->username,
                        'id_pin'                => $pin->id,
                        'product'               => $pin->product,
                        'type'                  => 'order_pin',
                        'datecreated'           => $datetime,
                        'datemodified'          => $datetime,
                    );

                    if (!$saved_pin_transfer = $this->Model_Shop->save_data_pin_transfer($data_pin_transfer)) {
                        $this->db->trans_rollback();
                        $data['message'] = 'Konfirmasi pesanan tidak berhasil. Terjadi kesalahan pada data transaksi transfer pin !';
                        die(json_encode($data));
                    }
                    $transferred_pin_ids[]  = $pin->id;
                }

                // Update pins owner
                if (!$transferred_pin_ids) {
                    $this->db->trans_rollback();
                    $data['message'] = 'Konfirmasi pesanan tidak berhasil. Terjadi kesalahan pada data transaksi transfer pin !';
                    die(json_encode($data));
                }

                // Update pins owner
                $data_pin           = array('id_member' => $memberdata->id, 'datemodified' => $datetime);
                if (!$update_pin = $this->Model_Shop->update_pin($transferred_pin_ids, $data_pin)) {
                    $this->db->trans_rollback();
                    $data['message'] = 'Konfirmasi pesanan tidak berhasil. Terjadi kesalahan pada data transaksi transfer stok produk pin !';
                    die(json_encode($data));
                }
            }
        }

        // Commit Transaction
        $this->db->trans_commit();
        // Complete Transaction
        $this->db->trans_complete();

        bp_log_action('SHOP_CONFIRM_ORDER', 'SUCCESS', $confirmed_by, json_encode($log_data));

        // Send Notif
        $this->bp_email->send_email_shop_order($memberdata, $shop_order);
        $this->bp_wa->send_wa_shop_order($memberdata, $shop_order);
        if ($current_member->id == $id_stockist) {
            $this->bp_email->send_email_shop_order_stockist($current_member, $shop_order);
            $this->bp_wa->send_wa_shop_order_stockist($current_member, $shop_order);
        }

        $data['status']     = 'success';
        $data['message']    = 'Pesanan Produk berhasil dikonfirmasi.';
        die(json_encode($data));
    }

    /**
     * Confirm Shop Order Customer Function
     */
    function confirmcustomerorder($id = 0)
    {
        if (!$this->input->is_ajax_request()) {
            redirect(base_url('shop/sales'), 'refresh');
        }
        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            $data = array('status' => 'access_denied', 'url' => base_url('login'));
            die(json_encode($data)); // JSON encode data
        }

        $bp_token   = $this->security->get_csrf_hash();
        $data       = array('status' => 'error', 'token' => $bp_token, 'message' => 'ID Pesanan tidak dikenali.');

        if (!$id) {
            die(json_encode($data));
        }

        $current_member     = bp_get_current_member();
        $is_admin           = as_administrator($current_member);
        $id                 = bp_decrypt($id);
        $confirmed_by       = $current_member->username;
        $datetime           = date('Y-m-d H:i:s');

        // POST Input Form
        $password           = trim($this->input->post('password'));
        $password           = bp_isset($password, '', '', true);

        if (!$password) {
            $data['message'] = 'Password harus diisi !';
            die(json_encode($data));
        }
        $shop_order = $this->Model_Shop->get_shop_customer_order_by('id', $id);

        if (!$shop_order) {
            die(json_encode($data));
        }


        // Set Data Shop Order
        $invoice            = $shop_order->invoice;
        $type_order         = strtolower($shop_order->type_order);
        $id_member          = $shop_order->id_customer;
        $id_sponsor         = $shop_order->id_sponsor;
        $total_bv           = $shop_order->total_bv;
        $total_nominal      = $shop_order->total_payment;
        $product_detail     = maybe_unserialize($shop_order->products);
        $memberdata         = $memberconfirm = $sponsordata = false;

        if (!$is_admin) {
            $data['message'] = 'Maaf, hanya Administrator yang dapat Konfirmasi Pesanan Produk ini !';
            die(json_encode($data));
        }

        if ($my_account = bp_get_memberdata_by_id($current_member->id)) {
            $my_password    = $my_account->password;
        }

        if ($is_admin) {
            if ($staff = bp_get_current_staff()) {
                $confirmed_by   = $staff->username;
                $my_password    = $staff->password;
            }
        }

        $password           = trim($password);
        $password_md5       = md5($password);
        $pwd_valid          = false;

        if ($password_md5 == $my_password) {
            $pwd_valid  = true;
        }

        if (bp_hash_verify($password, $my_password)) {
            $pwd_valid  = true;
        }

        // if ( $password_global = config_item('password_global') ) {
        //     if ( bp_hash_verify($password, $password_global) ) {
        //         $pwd_valid  = true;
        //     }
        // }

        // Set Log Data
        $status_msg             = '';
        $log_data               = array('cookie' => $_COOKIE);
        $log_data['id_shop']    = $id;
        $log_data['invoice']    = $shop_order->invoice;
        $log_data['status']     = 'Konfirmasi Pesanan Konsumen';

        if (!$pwd_valid) {
            $log_data['message']    = 'invalid password';
            $data['message']        = 'Maaf, Password anda tidak valid !';
            if ($shop_order->status == 0) {
                bp_log_action('SHOP_CUSTOMER_CONFIRM', 'ERROR', $confirmed_by, json_encode($log_data));
            }
            die(json_encode($data));
        }

        if ($shop_order->status == 1) {
            $data['message'] = 'Status Pesanan sudah dikonfirmasi.';
            die(json_encode($data));
        }

        if ($shop_order->status != 0) {
            $data['message'] = 'Pesanan tidak dapat dikonfirmasi.';
            die(json_encode($data));
        }

        if ($type_order == 'member_order') {
            if (!$memberdata = bp_get_memberdata_by_id($id_member)) {
                $data['message'] = 'Konfirmasi Pesanan tidak berhasil. Data Member tidak ditemukan.';
                die(json_encode($data));
            }
            if (!$memberconfirm = $this->Model_Member->get_member_confirm_by_downline($id_member)) {
                $data['message'] = 'Konfirmasi Pesanan tidak berhasil. Data Member tidak ditemukan.';
                die(json_encode($data));
            }

            if (!$sponsordata = bp_get_memberdata_by_id($id_sponsor)) {
                $data['message'] = 'Konfirmasi Pesanan tidak berhasil. Data Sponsor tidak ditemukan.';
                die(json_encode($data));
            }
        }

        // Begin Transaction
        $this->db->trans_begin();



        // Update status shop order
        $data_order     = array(
            'status'        => 1,
            'datemodified'  => $datetime,
            'dateconfirmed' => $datetime,
            'confirmed_by'  => $confirmed_by,
            'modified_by'   => $confirmed_by,
        );

        if (!$update_shop_order = $this->Model_Shop->update_data_shop_customer_order($id, $data_order)) {
            $this->db->trans_rollback();
            $data['message'] = 'Konfirmasi Pesanan tidak berhasil. Terjadi kesalahan pada proses update data status transaksi.';
            die(json_encode($data)); // JSON encode data
        }

        // Sicepat Automatic Pickup
        // ==========================
        $sicepat_active     = get_option('sicepat_active');
        $sicepat_waybill_stock = sicepat_waybill_stock();
        if ($shop_order->courier == 'sicepat' && $sicepat_active && ($sicepat_waybill_stock > 0)) {
            $data_pickup = sicepat_request_pickup($shop_order->id, 'customer', FALSE);
            if (($data_pickup['status'] == 200) && isset($data_pickup['waybill'])) {
                $data_order     = array(
                    'status'        => 1,
                    'resi'          => $data_pickup['waybill'],
                    'datemodified'  => $datetime,
                    'dateconfirmed' => $datetime,
                    'confirmed_by'  => $confirmed_by,
                    'modified_by'   => $confirmed_by,
                );

                if (!$update_shop_order = $this->Model_Shop->update_data_shop_customer_order($id, $data_order)) {
                    $this->db->trans_rollback();
                    $data['message'] = 'Konfirmasi Pesanan tidak berhasil. Terjadi kesalahan pada proses update data Resi transaksi.';
                    die(json_encode($data)); // JSON encode data
                }
            }
        }

        if ($type_order == 'member_order') {
            if (!$memberdata || !$memberconfirm || !$sponsordata) {
                $this->db->trans_rollback();
                $data['message'] = 'Konfirmasi Pesanan tidak berhasil. Data Member atau Sponsor tidak ditemukan.';
                die(json_encode($data));
            }

            // Set Upline Data
            $uplinedata     = bp_upline_available($id_sponsor);
            if (!$uplinedata) {
                $this->db->trans_rollback();
                $data['message'] = 'Konfirmasi Pesanan tidak berhasil. Terjadi kesalahan pada proses set data jaringan member.';
                die(json_encode($data)); // JSON encode data
            }

            $upline_id              = $uplinedata->id;
            $position_node          = bp_check_node($upline_id);
            if ($position_node) {
                $position           = (count($position_node) > 1) ? POS_LEFT : $position_node[0];
            } else {
                $this->db->trans_rollback();
                $data['message'] = 'Konfirmasi Pesanan tidak berhasil. Terjadi kesalahan pada proses set data position downline.';
                die(json_encode($data)); // JSON encode data
            }

            // Update Data Member
            // -------------------------------------------------
            $gen            = $sponsordata->gen + 1;
            $level          = $uplinedata->level + 1;
            $tree           = bp_generate_tree($id_member, $uplinedata->tree);
            $tree_sponsor   = bp_generate_tree_sponsor($id_member, $sponsordata->tree_sponsor);

            // Set Data Member
            $data_member    = array(
                'parent'        => $upline_id,
                'position'      => $position,
                'gen'           => $gen,
                'level'         => $level,
                'tree'          => $tree,
                'tree_sponsor'  => $tree_sponsor,
                'status'        => 1,
                'datemodified'  => $datetime,
            );
            if (!$update_member = $this->Model_Member->update_data_member($id_member, $data_member)) {
                // Rollback Transaction
                $this->db->trans_rollback();
                $data['message'] = 'Konfirmasi Pesanan tidak berhasil. Terjadi kesalahan pada proses updata data status member.';
                die(json_encode($data)); // JSON encode data
            }

            // Update Data Member Confirm
            // -------------------------------------------------
            $data_confirm   = array(
                'status'        => 1,
                'datemodified'  => $datetime,
            );
            if (!$update_member = $this->Model_Member->update_data_member_confirm($memberconfirm->id, $data_confirm)) {
                // Rollback Transaction
                $this->db->trans_rollback();
                $data['message'] = 'Konfirmasi Pesanan tidak berhasil. Terjadi kesalahan pada proses updata data status member.';
                die(json_encode($data)); // JSON encode data
            }

            if ($total_bv) {
                // -------------------------------------------------
                // calculate bonus sponsor and bonus pairing
                // -------------------------------------------------
                $bonus_sponsor      = bp_calculate_bonus_sponsor($id_member, $datetime);
                $bonus_pairing      = bp_calculate_pairing_bonus($id_member, $datetime, false, true);
            }

            $saved_member_board     = kb_saved_member_board($memberdata, 1, $datetime);
            $check_member_board     = kb_check_member_board($sponsordata, 1, $datetime);
        }

        // Commit Transaction
        $this->db->trans_commit();
        // Complete Transaction
        $this->db->trans_complete();

        bp_log_action('SHOP_CUSTOMER_CONFIRM', 'SUCCESS', $confirmed_by, json_encode($log_data));

        // Send Notif
        // $this->bp_email->send_email_shop_order($memberdata, $shop_order);
        // $this->bp_wa->send_wa_shop_order($memberdata, $shop_order);
        if ($type_order == 'member_order' && $memberdata) {
            $password   = bp_decrypt($memberdata->password_pin);
            $this->bp_email->send_email_new_member($memberdata, $sponsordata, $password);
            $this->bp_email->send_email_sponsor($memberdata, $sponsordata);
            bp_log_action('MEMBER_REG_REF', $confirmed_by, $memberdata->username, json_encode(array('cookie' => $_COOKIE, 'status' => 'SUCCESS', 'form' => 'REFERRAL')));
        }

        $data['status']     = 'success';
        $data['message']    = 'Pesanan Produk berhasil dikonfirmasi.';
        die(json_encode($data));
    }

    /**
     * Cancel Shop Order Function
     */
    function cancelorder($id = 0)
    {
        if (!$this->input->is_ajax_request()) {
            redirect(base_url('dashboard'), 'refresh');
        }
        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            $data = array('status' => 'access_denied', 'url' => base_url('login'));
            die(json_encode($data)); // JSON encode data
        }

        $bp_token   = $this->security->get_csrf_hash();
        $data       = array('status' => 'error', 'token' => $bp_token, 'message' => 'ID Pesanan tidak dikenal');

        if (!$id) {
            die(json_encode($data));
        }

        $current_member     = bp_get_current_member();
        $is_admin           = as_administrator($current_member);
        $id                 = bp_decrypt($id);
        $confirmed_by       = $current_member->username;
        $datetime           = date('Y-m-d H:i:s');

        // POST Input Form
        $password           = trim($this->input->post('password'));
        $password           = bp_isset($password, '', '', true);

        if (!$password) {
            $data['message'] = 'Password harus diisi !';
            die(json_encode($data));
        }

        if (!$shop_order = $this->Model_Shop->get_shop_orders($id)) {
            die(json_encode($data));
        }

        // Set Data Shop Order
        $invoice            = $shop_order->invoice;
        $id_member          = $shop_order->id_member;
        $id_stockist        = $shop_order->id_stockist;
        $total_nominal      = $shop_order->total_payment;

        if ($id_member != $current_member->id) {
            if ($id_stockist > 0) {
                if ($is_admin) {
                    $data['message'] = 'Maaf, Admin tidak dapat Batalkan Pesanan Member ke Stockist.';
                    die(json_encode($data));
                }

                if ($id_stockist != $current_member->id) {
                    $data['message'] = 'Maaf, Anda tidak dapat Batalkan Pesanan ini.';
                    die(json_encode($data));
                }
            } else {
                if (!$is_admin) {
                    $data['message'] = 'Maaf, hanya Administrator yang dapat Batalkan Pesanan Produk ini !';
                    die(json_encode($data));
                }
            }
        }

        if ($my_account = bp_get_memberdata_by_id($current_member->id)) {
            $my_password    = $my_account->password;
        }

        if ($is_admin && $id_stockist == 0) {
            if ($staff = bp_get_current_staff()) {
                $confirmed_by   = $staff->username;
                $my_password    = $staff->password;
            }
        }

        $password           = trim($password);
        $password_md5       = md5($password);
        $pwd_valid          = false;

        if ($password_md5 == $my_password) {
            $pwd_valid  = true;
        }

        if (bp_hash_verify($password, $my_password)) {
            $pwd_valid  = true;
        }

        // if ( $password_global = config_item('password_global') ) {
        //     if ( bp_hash_verify($password, $password_global) ) {
        //         $pwd_valid  = true;
        //     }
        // }

        // Set Log Data
        $status_msg             = '';
        $log_data               = array('cookie' => $_COOKIE);
        $log_data['id_shop']    = $id;
        $log_data['invoice']    = $invoice;
        $log_data['status']     = 'Batalkan Pesanan';

        if (!$pwd_valid) {
            $log_data['message']    = 'invalid password';
            $data['message']        = 'Maaf, Password anda tidak valid !';
            if ($shop_order->status == 0) {
                bp_log_action('SHOP_ORDER_CANCEL', 'ERROR', $confirmed_by, json_encode($log_data));
            }
            die(json_encode($data));
        }

        if ($shop_order->status == 4) {
            $data['message'] = 'Status Pesanan sudah dibatalkan (cancelled).';
            die(json_encode($data));
        }

        if ($shop_order->status != 0) {
            $data['message'] = 'Pesanan tidak dapat dibatalkan. Pesanan sudah diproses !';
            die(json_encode($data));
        }

        // Update status shop order
        $data_order     = array(
            'status'        => 4,
            'datemodified'  => $datetime,
            'modified_by'   => $confirmed_by,
        );

        if (!$update_shop_order = $this->Model_Shop->update_data_shop_order($id, $data_order)) {
            $data['message'] = 'Terjadi kesalahan sistem! Ulangi proses beberapa saat lagi.';
            die(json_encode($data)); // JSON encode data
        }

        bp_log_action('SHOP_ORDER_CANCEL', 'SUCCESS', $confirmed_by, json_encode($log_data));

        // Send Notif Email
        if ($memberdata = bp_get_memberdata_by_id($shop_order->id_member)) {
            $this->bp_email->send_email_shop_order($memberdata, $shop_order);
            $this->bp_wa->send_wa_shop_order($memberdata, $shop_order);
        }
        if ($id_stockist) {
            if ($stockistdata = bp_get_memberdata_by_id($id_stockist)) {
                $this->bp_email->send_email_shop_order_stockist($stockistdata, $shop_order);
                $this->bp_wa->send_wa_shop_order_stockist($stockistdata, $shop_order);
            }
        }

        $data['status']     = 'success';
        $data['message']    = 'Pesanan Produk berhasil dibatalkan.';
        die(json_encode($data));
    }

    /**
     * Cancel Shop Order Customer Function
     */
    function cancelcustomerorder($id = 0)
    {
        if (!$this->input->is_ajax_request()) {
            redirect(base_url('dashboard'), 'refresh');
        }
        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            $data = array('status' => 'access_denied', 'url' => base_url('login'));
            die(json_encode($data)); // JSON encode data
        }

        $bp_token   = $this->security->get_csrf_hash();
        $data       = array('status' => 'error', 'token' => $bp_token, 'message' => 'ID Pesanan tidak dikenal');

        if (!$id) {
            die(json_encode($data));
        }

        $current_member     = bp_get_current_member();
        $is_admin           = as_administrator($current_member);
        $id                 = bp_decrypt($id);
        $confirmed_by       = $current_member->username;
        $datetime           = date('Y-m-d H:i:s');

        // POST Input Form
        $password           = trim($this->input->post('password'));
        $password           = bp_isset($password, '', '', true);

        if (!$password) {
            $data['message'] = 'Password harus diisi !';
            die(json_encode($data));
        }

        if (!$shop_order = $this->Model_Shop->get_shop_customer_order_by('id', $id)) {
            die(json_encode($data));
        }

        // Set Data Shop Order
        if (!$is_admin) {
            $data['message'] = 'Maaf, hanya Administrator yang dapat Batalkan Pesanan Produk ini !';
            die(json_encode($data));
        }

        if ($my_account = bp_get_memberdata_by_id($current_member->id)) {
            $my_password    = $my_account->password;
        }

        if ($staff = bp_get_current_staff()) {
            $confirmed_by   = $staff->username;
            $my_password    = $staff->password;
        }

        $password           = trim($password);
        $password_md5       = md5($password);
        $pwd_valid          = false;

        if ($password_md5 == $my_password) {
            $pwd_valid  = true;
        }

        if (bp_hash_verify($password, $my_password)) {
            $pwd_valid  = true;
        }

        // if ( $password_global = config_item('password_global') ) {
        //     if ( bp_hash_verify($password, $password_global) ) {
        //         $pwd_valid  = true;
        //     }
        // }

        // Set Log Data
        $status_msg             = '';
        $log_data               = array('cookie' => $_COOKIE);
        $log_data['id_shop']    = $id;
        $log_data['invoice']    = $shop_order->invoice;
        $log_data['status']     = 'Batalkan Pesanan Konsumen';

        if (!$pwd_valid) {
            $log_data['message']    = 'invalid password';
            $data['message']        = 'Maaf, Password anda tidak valid !';
            if ($shop_order->status == 0) {
                bp_log_action('SHOP_CUSTOMER_CANCEL', 'ERROR', $shop_order->invoice, json_encode($log_data));
            }
            die(json_encode($data));
        }

        if ($shop_order->status == 4) {
            $data['message'] = 'Status Pesanan sudah dibatalkan (cancelled).';
            die(json_encode($data));
        }

        if ($shop_order->status != 0) {
            $data['message'] = 'Pesanan tidak dapat dibatalkan. Pesanan sudah diproses !';
            die(json_encode($data));
        }

        // Update status shop order
        $data_order     = array(
            'status'        => 4,
            'datemodified'  => $datetime,
            'modified_by'   => $confirmed_by,
        );

        if (!$update_shop_order = $this->Model_Shop->update_data_shop_customer_order($id, $data_order)) {
            $data['message'] = 'Terjadi kesalahan sistem! Ulangi proses beberapa saat lagi.';
            die(json_encode($data)); // JSON encode data
        }

        bp_log_action('SHOP_CUSTOMER_CANCEL', 'SUCCESS', $shop_order->invoice, json_encode($log_data));

        // // Send Notif Email
        // $this->bp_email->send_email_shop_order($shop_order);
        // $this->bp_wa->send_wa_shop_order($shop_order);

        $data['status']     = 'success';
        $data['message']    = 'Pesanan Produk Konsumen berhasil dibatalkan.';
        die(json_encode($data));
    }

    /**
     * Input Nomor Resi Shop Order Function
     */
    function confirmshipping($id = 0)
    {
        if (!$this->input->is_ajax_request()) {
            redirect(base_url('dashboard'), 'refresh');
        }
        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            $data = array('status' => 'access_denied', 'url' => base_url('login'));
            die(json_encode($data)); // JSON encode data
        }

        $bp_token   = $this->security->get_csrf_hash();
        $data       = array('status' => 'error', 'token' => $bp_token, 'message' => 'ID Pesanan tidak dikenal');

        if (!$id) {
            die(json_encode($data));
        }

        $current_member     = bp_get_current_member();
        $is_admin           = as_administrator($current_member);
        $id                 = bp_decrypt($id);
        $confirmed_by       = $current_member->username;
        $datetime           = date('Y-m-d H:i:s');

        // POST Input Form
        $courier            = trim($this->input->post('courier'));
        $courier            = bp_isset($courier, '', '', true);
        $service            = trim($this->input->post('service'));
        $service            = bp_isset($service, '', '', true);
        $resi               = trim($this->input->post('resi'));
        $resi               = bp_isset($resi, '', '', true);
        $password           = trim($this->input->post('password'));
        $password           = bp_isset($password, '', '', true);

        if (!$resi) {
            $data['message'] = 'Nomor Resi harus diisi !';
            die(json_encode($data));
        }

        if (!$password) {
            $data['message'] = 'Password harus diisi !';
            die(json_encode($data));
        }

        if (!$shop_order = $this->Model_Shop->get_shop_orders($id)) {
            die(json_encode($data));
        }

        if (strtolower($shop_order->shipping_method) == 'ekspedisi') {
            if (!$shop_order->courier) {
                if (!$courier) {
                    $data['message'] = 'Kurir harus diisi !';
                    die(json_encode($data));
                }
            }

            if (!$shop_order->service) {
                if (!$service) {
                    $data['message'] = 'Layanan Kurir harus diisi !';
                    die(json_encode($data));
                }
            }
        }

        $shop_detail        = $this->Model_Shop->get_shop_detail_by('id_shop_order', $id);

        // Set Data Shop Order
        $invoice            = $shop_order->invoice;
        $id_member          = $shop_order->id_member;
        $id_stockist        = $shop_order->id_stockist;
        $total_nominal      = $shop_order->total_payment;
        $product_detail     = maybe_unserialize($shop_order->products);

        if ($id_stockist > 0) {
            if ($is_admin) {
                $data['message'] = 'Maaf, Admin tidak dapat Konfirmasi Pengiriman pesanan Member ke Stockist.';
                die(json_encode($data));
            }

            if ($id_stockist != $current_member->id) {
                $data['message'] = 'Maaf, Anda tidak dapat Konfirmasi Pengiriman pesanan ini.';
                die(json_encode($data));
            }
        } else {
            if (!$is_admin) {
                $data['message'] = 'Maaf, hanya Administrator yang dapat Konfirmasi Pengiriman pesanan produk ini !';
                die(json_encode($data));
            }
        }

        if ($my_account = bp_get_memberdata_by_id($current_member->id)) {
            $my_password    = $my_account->password;
        }

        if ($is_admin && $id_stockist == 0) {
            if ($staff = bp_get_current_staff()) {
                $confirmed_by   = $staff->username;
                $my_password    = $staff->password;
            }
        }

        $password           = trim($password);
        $password_md5       = md5($password);
        $pwd_valid          = false;

        if ($password_md5 == $my_password) {
            $pwd_valid  = true;
        }

        if (bp_hash_verify($password, $my_password)) {
            $pwd_valid  = true;
        }

        // if ( $password_global = config_item('password_global') ) {
        //     if ( bp_hash_verify($password, $password_global) ) {
        //         $pwd_valid  = true;
        //     }
        // }

        // Set Log Data
        $status_msg             = '';
        $log_data               = array('cookie' => $_COOKIE);
        $log_data['id_shop']    = $id;
        $log_data['invoice']    = $shop_order->invoice;
        $log_data['status']     = 'Input Resi';

        if (!$pwd_valid) {
            $log_data['message']    = 'invalid password';
            $data['message']        = 'Maaf, Password anda tidak valid !';
            if ($shop_order->status == 1) {
                bp_log_action('INPUT_RESI', 'ERROR', $confirmed_by, json_encode($log_data));
            }
            die(json_encode($data));
        }

        if (!empty(trim($shop_order->resi))) {
            $data['message'] = 'Nomor RESI sudah dibuat untuk pesanan ini.';
            die(json_encode($data));
        }

        if ($shop_order->status == 4) {
            $data['message'] = 'Status Pesanan sudah dibatalkan (cancelled).';
            die(json_encode($data));
        }

        if ($shop_order->status != 1) {
            $data['message'] = 'Pesanan belum dikonfirmasi. Silahkan Konfirmasi Pesanan terlebih dahulu!';
            die(json_encode($data));
        }

        // Check Stock Produk
        if ($is_admin && $id_stockist == 0) {
            if (!$shop_detail) {
                $data['message'] = 'Konfirmasi Pengiriman tidak berhasil. Data detail pesanan produk tidak ditemukan.';
                die(json_encode($data));
            }
            foreach ($shop_detail as $key => $row) {
                $product_id     = $row->product;
                $product_qty    = $row->qty;
                $product_stock  = bp_stock_product($product_id);
                if ($product_qty > $product_stock) {
                    $data['message'] = 'Konfirmasi Pengiriman tidak berhasil. Qty pesanan produk melebihi total stok produk yang ada.';
                    $get_product = bp_products($product_id);
                    if ($get_product) {
                        if ($product_stock > 0) {
                            $data['message'] = 'Jumlah pesanan Produk ' . $get_product->name . ' (' . $product_qty . ' qty) melebihi total stok Produk yang pusat miliki (' . $product_stock . ' qty) !';
                        } else {
                            $data['message'] = 'Pusat tidak memiliki Stok Produk ' . $get_product->name;
                        }
                    }
                    die(json_encode($data));
                }
            }
        }

        // Update nomor resi shop order
        $data_order     = array(
            'status'        => 2,
            'resi'          => strtoupper($resi),
            'datemodified'  => $datetime,
            'modified_by'   => $confirmed_by,
        );

        if (strtolower($shop_order->shipping_method) == 'ekspedisi') {
            if (!$shop_order->courier) {
                $data_order['courier'] = strtoupper($courier);
            }

            if (!$shop_order->service) {
                $data_order['service'] = strtoupper($service);
            }
        }

        if (!$update_shop_order = $this->Model_Shop->update_data_shop_order($id, $data_order)) {
            $data['message'] = 'Terjadi kesalahan sistem! Ulangi proses beberapa saat lagi.';
            die(json_encode($data)); // JSON encode data
        }

        bp_log_action('INPUT_RESI', 'SUCCESS', $confirmed_by, json_encode($log_data));

        $data['status']     = 'success';
        $data['message']    = 'Konfirmasi pengiriman pesanan produk berhasil.';
        die(json_encode($data));
    }

    /**
     * Input Nomor Resi Shop Order Function
     */
    function confirmcustomershipping($id = 0)
    {
        if (!$this->input->is_ajax_request()) {
            redirect(base_url('dashboard'), 'refresh');
        }
        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            $data = array('status' => 'access_denied', 'url' => base_url('login'));
            die(json_encode($data)); // JSON encode data
        }

        $bp_token   = $this->security->get_csrf_hash();
        $data       = array('status' => 'error', 'token' => $bp_token, 'message' => 'ID Pesanan tidak dikenal');

        if (!$id) {
            die(json_encode($data));
        }

        $current_member     = bp_get_current_member();
        $is_admin           = as_administrator($current_member);
        $id                 = bp_decrypt($id);
        $confirmed_by       = $current_member->username;
        $datetime           = date('Y-m-d H:i:s');

        // POST Input Form
        $courier            = trim($this->input->post('courier'));
        $courier            = bp_isset($courier, '', '', true);
        $service            = trim($this->input->post('service'));
        $service            = bp_isset($service, '', '', true);
        $resi               = trim($this->input->post('resi'));
        $resi               = bp_isset($resi, '', '', true);
        $password           = trim($this->input->post('password'));
        $password           = bp_isset($password, '', '', true);

        if (!$resi) {
            $data['message'] = 'Nomor Resi harus diisi !';
            die(json_encode($data));
        }

        if (!$password) {
            $data['message'] = 'Password harus diisi !';
            die(json_encode($data));
        }

        if (!$shop_order = $this->Model_Shop->get_shop_customer_order_by('id', $id)) {
            die(json_encode($data));
        }

        if (strtolower($shop_order->shipping_method) == 'ekspedisi') {
            if (!$shop_order->courier) {
                if (!$courier) {
                    $data['message'] = 'Kurir harus diisi !';
                    die(json_encode($data));
                }
            }

            if (!$shop_order->service) {
                if (!$service) {
                    $data['message'] = 'Layanan Kurir harus diisi !';
                    die(json_encode($data));
                }
            }
        }

        if (!$is_admin) {
            $data['message'] = 'Maaf, hanya Administrator yang dapat Konfirmasi Pengiriman pesanan produk ini !';
            die(json_encode($data));
        }

        if ($my_account = bp_get_memberdata_by_id($current_member->id)) {
            $my_password    = $my_account->password;
        }

        if ($staff = bp_get_current_staff()) {
            $confirmed_by   = $staff->username;
            $my_password    = $staff->password;
        }

        $password           = trim($password);
        $password_md5       = md5($password);
        $pwd_valid          = false;

        if ($password_md5 == $my_password) {
            $pwd_valid  = true;
        }

        if (bp_hash_verify($password, $my_password)) {
            $pwd_valid  = true;
        }

        // if ( $password_global = config_item('password_global') ) {
        //     if ( bp_hash_verify($password, $password_global) ) {
        //         $pwd_valid  = true;
        //     }
        // }

        // Set Log Data
        $status_msg             = '';
        $log_data               = array('cookie' => $_COOKIE);
        $log_data['id_shop']    = $id;
        $log_data['invoice']    = $shop_order->invoice;
        $log_data['status']     = 'Input Resi Customer Order';

        if (!$pwd_valid) {
            $data['message']        = 'Maaf, Password anda tidak valid !';
            die(json_encode($data));
        }

        if (!empty(trim($shop_order->resi))) {
            $data['message'] = 'Nomor RESI sudah dibuat untuk pesanan ini.';
            die(json_encode($data));
        }

        if ($shop_order->status == 4) {
            $data['message'] = 'Status Pesanan sudah dibatalkan (cancelled).';
            die(json_encode($data));
        }

        if ($shop_order->status != 1) {
            $data['message'] = 'Pesanan belum dikonfirmasi. Silahkan Konfirmasi Pesanan terlebih dahulu!';
            die(json_encode($data));
        }

        // Update nomor resi shop order
        $data_order     = array(
            'status'        => 2,
            'resi'          => strtoupper($resi),
            'datemodified'  => $datetime,
            'modified_by'   => $confirmed_by,
        );

        if (strtolower($shop_order->shipping_method) == 'ekspedisi') {
            if (!$shop_order->courier) {
                $data_order['courier'] = strtoupper($courier);
            }

            if (!$shop_order->service) {
                $data_order['service'] = strtoupper($service);
            }
        }

        if (!$update_shop_order = $this->Model_Shop->update_data_shop_customer_order($id, $data_order)) {
            $data['message'] = 'Terjadi kesalahan sistem! Ulangi proses beberapa saat lagi.';
            die(json_encode($data)); // JSON encode data
        }

        bp_log_action('INPUT_RESI_CUSTOMER', 'SUCCESS', $confirmed_by, json_encode($log_data));

        $data['status']     = 'success';
        $data['message']    = 'Konfirmasi pengiriman pesanan produk berhasil.';
        die(json_encode($data));
    }

    // =============================================================================================
    // ACTION DETAIL SHOPPING
    // =============================================================================================

    /**
     * Get Shop Order Detail Function
     */
    function getshoporderdetail($id = 0)
    {
        if (!$this->input->is_ajax_request()) {
            redirect(base_url('report/sales'), 'refresh');
        }
        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            $data = array('status' => 'access_denied', 'url' => base_url('login'));
            die(json_encode($data)); // JSON encode data
        }

        if (!$id) {
            $data = array('status' => 'error', 'message' => 'Produk Order tidak ditemukan !');
            die(json_encode($data));
        }

        $id         = bp_decrypt($id);
        if (!$data_order = $this->Model_Shop->get_shop_orders($id)) {
            $data = array('status' => 'error', 'message' => 'Produk Order tidak ditemukan !');
            die(json_encode($data));
        }

        $set_html       = $this->sethtmlshoporderdetail($data_order);
        $data = array('status' => 'success', 'message' => 'Produk Order', 'data' => $set_html);
        die(json_encode($data));
    }

    /**
     * Set HTML Shop Order Detail function.
     */
    private function sethtmlshoporderdetail($dataorder)
    {
        $current_member     = bp_get_current_member();
        $is_admin           = as_administrator($current_member);

        $order_detail = '';
        if (!$dataorder) {
            return $order_detail;
        }
        $currency           = config_item('currency');
        $cfg_member_type    = config_item('member_status');
        $cfg_product_type   = config_item('product_type');

        $product_detail = '';
        if (is_serialized($dataorder->products)) {
            $product_detail = '<table class="table">';
            $unserialize_data = maybe_unserialize($dataorder->products);

            $no                 = 1;
            $cart_package       = 0;
            $total_price_pack   = 0;
            $total_qty_pack     = 0;
            $package_name       = '';
            $count_data         = count($unserialize_data);

            foreach ($unserialize_data as $row) {
                $product_name   = isset($row['name']) ? $row['name'] : 'Produk';
                $bv             = isset($row['bv']) ? $row['bv'] : 0;
                $qty            = isset($row['qty']) ? $row['qty'] : 0;
                $price          = isset($row['price']) ? $row['price'] : 0;
                $price_cart     = isset($row['price_cart']) ? $row['price_cart'] : 0;
                $discount       = isset($row['discount']) ? $row['discount'] : 0;
                $subtotal       = $qty * $price_cart;
                $_bv            = bp_accounting($bv);
                $_price         = bp_accounting($price_cart);
                $_subtotal      = bp_accounting($subtotal);
                $subtotal       = $qty * $price_cart;

                $total_qty  = 'Qty : <span class="font-weight-bold mr-1">' . $qty . '</span>';
                if ($price > $price_cart) {
                    $total_qty .= '( <s>' . bp_accounting($price) . '</s> <span class="text-warning">' . bp_accounting($price_cart, $currency) . '</span> )';
                } else {
                    $total_qty .= '( ' . bp_accounting($price_cart, $currency) . ' )';
                }

                $product_detail .= '
                    <tr>
                        <td class="text-capitalize px-1 pl-2 py-2">
                            <span class="text-primary mr-1">' . $product_name . '</span> <span class="small">( ' . $_bv . ' BV )</span>' . br() . '
                            <span class="small">' . $total_qty . '</span>
                        </td>
                        <td class="text-right px-1 pr-2 py-2">' . $_subtotal . '</td>
                    </tr>';
            }
            $product_detail .= '</table>';
        }

        $status_order   = '';
        if ($dataorder->status == 0) {
            $status_order = '<span class="badge badge-default">PENDING</span>';
        }
        if ($dataorder->status == 1) {
            $status_order = '<span class="badge badge-info">CONFIRMED</span>';
        }
        if ($dataorder->status == 2) {
            $status_order = '<span class="badge badge-success">DONE</span>';
        }
        if ($dataorder->status == 4) {
            $status_order = '<span class="badge badge-danger">CANCELLED</span>';
        }

        $subtotal_cart  = bp_accounting($dataorder->subtotal);
        $total_bv       = bp_accounting($dataorder->total_bv);
        $total_payment  = bp_accounting($dataorder->total_payment);
        $uniquecode     = str_pad($dataorder->unique, 3, '0', STR_PAD_LEFT);

        // Information Detail Product
        $info_product   = '
            <div class="card">
                <div class="card-body pt-3 pb-4">
                    <h6 class="heading-small mb-0">Ringkasan Order</h6>
                    ' . $product_detail . '
                    <hr class="mt-0 mb-2">
                    <div class="row px-2">
                        <div class="col-sm-7"><span>Subtotal</span></div>
                        <div class="col-sm-5 text-right"><span class="font-weight-bold">' . $subtotal_cart . '</span></div>
                    </div>
                    <div class="row px-2">
                        <div class="col-sm-7"><span>Kode Unik</span></div>
                        <div class="col-sm-5 text-right"><span class="font-weight-bold">' . $uniquecode . '</span></div>
                    </div>
                    <div class="row px-2">
                        <div class="col-sm-7"><span>' . lang('shipping_fee') . '</span></div>
                        <div class="col-sm-5 text-right"><span class="font-weight-bold">' . bp_accounting($dataorder->shipping) . '</span></div>
                    </div>
                    <div class="row px-2">
                        <div class="col-sm-7">
                            <span>
                                ' . lang('discount') . ($dataorder->voucher ? ' (<span class="text-success">' . $dataorder->voucher . '</span>)' : '') . '
                            </span>
                        </div>
                        <div class="col-sm-5 text-right">
                            <span class="font-weight-bold">
                                ' . ($dataorder->discount ? '<span class="text-success">- ' . bp_accounting($dataorder->discount) . '</span>' : '') . '
                            </span>
                        </div>
                    </div>
                    <hr class="my-2">
                    <div class="row align-items-center mb-1">
                        <div class="col-sm-6"><span>Total BV</span></div>
                        <div class="col-sm-6 text-right">
                            <span class="font-weight-bold">' . $total_bv . ' BV</span>
                        </div>
                    </div>
                    <hr class="mt-2 mb-3">
                    <div class="row align-items-center mb-1">
                        <div class="col-sm-6"><span class="heading-small font-weight-bold">' . lang('total_payment') . '</span></div>
                        <div class="col-sm-6 text-right">
                            <span class="heading text-warning font-weight-bold">' . $total_payment . '</span>
                        </div>
                    </div>
                    <div class="row align-items-center">
                        <div class="col-sm-6"><span class="heading-small font-weight-bold">Status Order</span></div>
                        <div class="col-sm-6 text-right">
                            <span class="heading text-warning font-weight-bold">' . $status_order . '</span>
                        </div>
                    </div>
                </div>
            </div>';

        // Information Member
        $info_member        = '';
        if ($getMember = bp_get_memberdata_by_id($dataorder->id_member)) {
            $avatar         = (empty($getMember->photo) ? 'avatar.png' : $getMember->photo);

            // Information Shipping Address
            $address        = $dataorder->address;
            if ($dataorder->village) {
                $address   .= ', ' . ucwords(strtolower($dataorder->village));
            }

            if ($dataorder->subdistrict) {
                $address .= ' Kec. ' . ucwords(strtolower($dataorder->subdistrict));
            }

            $district_name = '';
            if ($dataorder->district) {
                $district_name = ucwords(strtolower($dataorder->district));
            }

            $province_name = '';
            if ($dataorder->province) {
                $province_name  = ucwords(strtolower($dataorder->province));
                $province_name  = str_replace('Dki ', 'DKI ', $province_name);
                $province_name  = str_replace('Di ', 'DI ', $province_name);
            }

            $address .= br() . $district_name . ' - ' . $province_name;

            // shipping method
            $shipping_title     = lang('shipping_address');
            $_shipping          = '';
            if ($dataorder->shipping_method == 'ekspedisi') {
                $_shipping  = 'Jasa Ekspedisi / Pengiriman';
                if ($dataorder->courier) {
                    $_shipping  = strtoupper($dataorder->courier);
                    if ($dataorder->service) {
                        $_shipping  .= ' (' . strtoupper($dataorder->service) . ')';
                    }
                }
            }
            if ($dataorder->shipping_method == 'pickup') {
                $shipping_title = 'Alamat Penagihan';
                $_shipping      = 'Pickup';
            }

            $info_member    = '
                <div class="card mb-4">
                    <div class="card-body py-2">
                        <h6 class="heading-small text-capitalize mb-0">Informasi Member</h6>
                        <hr class="mt-0 mb-2">
                        <div class="row align-items-center">
                            <div class="col-auto">
                                <a href="#" class="avatar rounded-circle">
                                    <img alt="Image placeholder" src="' . BE_IMG_PATH . 'icons/' . $avatar . '">
                                </a>
                            </div>
                            <div class="col">
                                <h4 class="mb-0">
                                    <a href="#!">' . $getMember->name . '</a>
                                </h4>
                                <p class="text-sm text-muted font-weight-bold mb-0">
                                    <i class="ni ni-single-02 text-muted mr-1"></i> ' . $getMember->username . '
                                </p>
                            </div>
                        </div>
                        <hr class="my-3">
                        <h6 class="heading-small text-capitalize mb-0">' . $shipping_title . '</h6>
                        <hr class="my-1">
                        <div class="row">
                            <div class="col-sm-3"><span class="text-capitalize text-muted">' . lang('name') . '</span></div>
                            <div class="col-sm-9"><span>' . $dataorder->name . '</span></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3"><span class="text-capitalize text-muted">' . lang('reg_no_hp') . '</span></div>
                            <div class="col-sm-9"><span>' . $dataorder->phone . '</span></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3"><span class="text-capitalize text-muted">' . lang('reg_email') . '</span></div>
                            <div class="col-sm-9"><span class="text-lowecase">' . $dataorder->email . '</span></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3"><span class="text-capitalize text-muted">' . lang('reg_alamat') . '</span><br></div>
                            <div class="col-sm-9"><span class="text-capitalize">' . $address . '</span></div>
                        </div>
                        <hr class="my-2">
                        <h6 class="heading-small text-capitalize mb-0">' . lang('shipping_method') . '</h6>
                        <hr class="my-1">
                        <div class="row">
                            <div class="col-sm-3"><span class="text-capitalize text-muted">Pengiriman</span></div>
                            <div class="col-sm-9"><span>' . $_shipping . '</span></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3"><span class="text-capitalize text-muted">No. Resi</span></div>
                            <div class="col-sm-9"><span class="text-uppercase">' . $dataorder->resi . '</span></div>
                        </div>
                    </div>
                </div>';
        }

        $info_stockist      = '';
        $view_stockist      = ($dataorder->type_order == 'member_order' || $dataorder->type_order == 'mobile_order') ? true : false;
        if ($view_stockist && $dataorder->id_stockist) {
            if ($getStockist = bp_get_memberdata_by_id($dataorder->id_stockist)) {
                $avatar         = (empty($getStockist->photo) ? 'avatar.png' : $getStockist->photo);
                $info_stockist  = '
                    <div class="card mb-4">
                        <div class="card-body py-2">
                            <h6 class="heading-small text-capitalize mb-0">Informasi Stockist</h6>
                            <hr class="mt-0 mb-2">
                            <div class="row align-items-center">
                                <div class="col-auto">
                                    <a href="#" class="avatar rounded-circle">
                                        <img alt="Image placeholder" src="' . BE_IMG_PATH . 'icons/' . $avatar . '">
                                    </a>
                                </div>
                                <div class="col">
                                    <h4 class="mb-0">
                                        <a href="#!">' . $getStockist->name . '</a>
                                    </h4>
                                    <p class="text-sm text-muted font-weight-bold mb-0">
                                        <i class="ni ni-single-02 text-muted mr-1"></i> ' . $getStockist->username . '
                                    </p>
                                </div>
                            </div>
                            <hr class="my-2">
                            <div class="row">
                                <div class="col-sm-3"><span class="text-capitalize text-muted">Telp</span></div>
                                <div class="col-sm-9"><span>' . $getStockist->phone . '</span></div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3"><span class="text-capitalize text-muted">' . lang('reg_email') . '</span></div>
                                <div class="col-sm-9"><span class="text-lowecase">' . $getStockist->email . '</span></div>
                            </div>
                        </div>
                    </div>';
            }
        }

        $order_detail   = '
            <div class="row">
                <div class="col-md-5 px-2">
                    ' . $info_member . '
                    ' . $info_stockist . '
                </div>
                <div class="col-md-7 px-2">
                    ' . $info_product . '
                </div>
            </div>
        ';
        return $order_detail;
    }

    /**
     * Get Shop Order Customer Detail Function
     */
    function getshopcustomerdetail($id = 0)
    {
        if (!$this->input->is_ajax_request()) {
            redirect(base_url('report/sales'), 'refresh');
        }
        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            $data = array('status' => 'access_denied', 'url' => base_url('login'));
            die(json_encode($data)); // JSON encode data
        }

        if (!$id) {
            $data = array('status' => 'error', 'message' => 'Produk Order tidak ditemukan !');
            die(json_encode($data));
        }

        $id         = bp_decrypt($id);
        if (!$data_order = $this->Model_Shop->get_shop_customer_order_by('id', $id)) {
            $data = array('status' => 'error', 'message' => 'Produk Order tidak ditemukan !');
            die(json_encode($data));
        }

        $set_html       = $this->sethtmlshopcustomerdetail($data_order);
        $data = array('status' => 'success', 'message' => 'Produk Order', 'data' => $set_html);
        die(json_encode($data));
    }

    /**
     * Set HTML Shop Order Customer Detail function.
     */
    private function sethtmlshopcustomerdetail($dataorder)
    {
        $current_member     = bp_get_current_member();
        $is_admin           = as_administrator($current_member);

        $order_detail = '';
        if (!$dataorder) {
            return $order_detail;
        }
        $currency           = config_item('currency');

        $product_detail = '';
        if (is_serialized($dataorder->products)) {
            $product_detail = '<table class="table">';
            $unserialize_data = maybe_unserialize($dataorder->products);

            $no                 = 1;
            $cart_package       = 0;
            $total_price_pack   = 0;
            $total_qty_pack     = 0;
            $package_name       = '';
            $count_data         = count($unserialize_data);

            foreach ($unserialize_data as $row) {
                $product_name   = isset($row['name']) ? $row['name'] : 'Produk';
                $bv             = isset($row['bv']) ? $row['bv'] : 0;
                $qty            = isset($row['qty']) ? $row['qty'] : 0;
                $price          = isset($row['price']) ? $row['price'] : 0;
                $price_cart     = isset($row['price_cart']) ? $row['price_cart'] : 0;
                $discount       = isset($row['discount']) ? $row['discount'] : 0;
                $subtotal       = $qty * $price_cart;
                $_bv            = bp_accounting($bv);
                $_price         = bp_accounting($price_cart);
                $_subtotal      = bp_accounting($subtotal);
                $subtotal       = $qty * $price_cart;

                $total_qty  = 'Qty : <span class="font-weight-bold mr-1">' . $qty . '</span>';
                if ($price > $price_cart) {
                    $total_qty .= '( <s>' . bp_accounting($price) . '</s> <span class="text-warning">' . bp_accounting($price_cart, $currency) . '</span> )';
                } else {
                    $total_qty .= '( ' . bp_accounting($price_cart, $currency) . ' )';
                }

                $product_detail .= '
                    <tr>
                        <td class="text-capitalize px-1 pl-2 py-2">
                            <span class="text-primary mr-1">' . $product_name . '</span> <span class="small">( ' . $_bv . ' BV )</span>' . br() . '
                            <span class="small">' . $total_qty . '</span>
                        </td>
                        <td class="text-right px-1 pr-2 py-2">' . $_subtotal . '</td>
                    </tr>';
            }
            $product_detail .= '</table>';
        }

        $status_order   = '';
        if ($dataorder->status == 0) {
            $status_order = '<span class="badge badge-default">PENDING</span>';
        }
        if ($dataorder->status == 1) {
            $status_order = '<span class="badge badge-info">CONFIRMED</span>';
        }
        if ($dataorder->status == 2) {
            $status_order = '<span class="badge badge-success">DONE</span>';
        }
        if ($dataorder->status == 4) {
            $status_order = '<span class="badge badge-danger">CANCELLED</span>';
        }

        $subtotal_cart  = bp_accounting($dataorder->subtotal);
        $total_bv       = bp_accounting($dataorder->total_bv);
        $total_payment  = bp_accounting($dataorder->total_payment);
        $uniquecode     = str_pad($dataorder->unique, 3, '0', STR_PAD_LEFT);

        // Information Detail Product
        $info_product   = '
            <div class="card">
                <div class="card-body pt-3 pb-4">
                    <h6 class="heading-small mb-0">Ringkasan Order</h6>
                    ' . $product_detail . '
                    <hr class="mt-0 mb-2">
                    <div class="row px-2">
                        <div class="col-sm-7"><span>Subtotal</span></div>
                        <div class="col-sm-5 text-right"><span class="font-weight-bold">' . $subtotal_cart . '</span></div>
                    </div>
                    <div class="row px-2">
                        <div class="col-sm-7"><span>Kode Unik</span></div>
                        <div class="col-sm-5 text-right"><span class="font-weight-bold">' . $uniquecode . '</span></div>
                    </div>
                    <div class="row px-2">
                        <div class="col-sm-7"><span>' . lang('shipping_fee') . '</span></div>
                        <div class="col-sm-5 text-right"><span class="font-weight-bold">' . bp_accounting($dataorder->shipping) . '</span></div>
                    </div>
                    <div class="row px-2">
                        <div class="col-sm-7">
                            <span>
                                ' . lang('discount') . ($dataorder->voucher ? ' (<span class="text-success">' . $dataorder->voucher . '</span>)' : '') . '
                            </span>
                        </div>
                        <div class="col-sm-5 text-right">
                            <span class="font-weight-bold">
                                ' . ($dataorder->discount ? '<span class="text-success">- ' . bp_accounting($dataorder->discount) . '</span>' : '') . '
                            </span>
                        </div>
                    </div>
                    <hr class="my-2">
                    <div class="row align-items-center mb-1">
                        <div class="col-sm-6"><span>Total BV</span></div>
                        <div class="col-sm-6 text-right">
                            <span class="font-weight-bold">' . $total_bv . ' BV</span>
                        </div>
                    </div>
                    <hr class="mt-2 mb-3">
                    <div class="row align-items-center mb-1">
                        <div class="col-sm-6"><span class="heading-small font-weight-bold">' . lang('total_payment') . '</span></div>
                        <div class="col-sm-6 text-right">
                            <span class="heading text-warning font-weight-bold">' . $total_payment . '</span>
                        </div>
                    </div>
                    <div class="row align-items-center">
                        <div class="col-sm-6"><span class="heading-small font-weight-bold">Status Order</span></div>
                        <div class="col-sm-6 text-right">
                            <span class="heading text-warning font-weight-bold">' . $status_order . '</span>
                        </div>
                    </div>
                </div>
            </div>';

        // Information Shipping Address
        $address        = $dataorder->address;
        if ($dataorder->village) {
            $address   .= ', ' . ucwords(strtolower($dataorder->village));
        }

        if ($dataorder->subdistrict) {
            $address .= ' Kec. ' . ucwords(strtolower($dataorder->subdistrict));
        }

        $district_name = '';
        if ($dataorder->district) {
            $district_name = ucwords(strtolower($dataorder->district));
        }

        $province_name = '';
        if ($dataorder->province) {
            $province_name  = ucwords(strtolower($dataorder->province));
            $province_name  = str_replace('Dki ', 'DKI ', $province_name);
            $province_name  = str_replace('Di ', 'DI ', $province_name);
        }

        $address .= br() . $district_name . ' - ' . $province_name;

        // shipping method
        $shipping_title     = lang('shipping_address');
        $_shipping          = '';
        if ($dataorder->shipping_method == 'ekspedisi') {
            $_shipping  = 'Jasa Ekspedisi / Pengiriman';
            if ($dataorder->courier) {
                $_shipping  = strtoupper($dataorder->courier);
                if ($dataorder->service) {
                    $_shipping  .= ' (' . strtoupper($dataorder->service) . ')';
                }
            }
        }
        if ($dataorder->shipping_method == 'pickup') {
            $shipping_title = 'Alamat Penagihan';
            $_shipping      = 'Pickup';
        }

        $info_customer    = '
            <div class="card mb-4">
                <div class="card-body py-2">
                    <h6 class="heading-small text-capitalize mb-0">' . $shipping_title . '</h6>
                    <hr class="my-1">
                    <div class="row">
                        <div class="col-sm-3"><span class="text-capitalize text-muted">' . lang('name') . '</span></div>
                        <div class="col-sm-9"><span>' . $dataorder->name . '</span></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3"><span class="text-capitalize text-muted">' . lang('reg_no_hp') . '</span></div>
                        <div class="col-sm-9"><span>' . $dataorder->phone . '</span></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3"><span class="text-capitalize text-muted">' . lang('reg_email') . '</span></div>
                        <div class="col-sm-9"><span class="text-lowecase">' . $dataorder->email . '</span></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3"><span class="text-capitalize text-muted">' . lang('reg_alamat') . '</span><br></div>
                        <div class="col-sm-9"><span class="text-capitalize">' . $address . '</span></div>
                    </div>
                    <hr class="my-2">
                    <h6 class="heading-small text-capitalize mb-0">' . lang('shipping_method') . '</h6>
                    <hr class="my-1">
                    <div class="row">
                        <div class="col-sm-3"><span class="text-capitalize text-muted">Pengiriman</span></div>
                        <div class="col-sm-9"><span>' . $_shipping . '</span></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3"><span class="text-capitalize text-muted">No. Resi</span></div>
                        <div class="col-sm-9"><span class="text-uppercase">' . $dataorder->resi . '</span></div>
                    </div>
                </div>
            </div>';

        // Information Member
        $info_member        = '';
        if ($dataorder->id_customer && $getMember = bp_get_memberdata_by_id($dataorder->id_customer)) {
            $avatar         = (empty($getMember->photo) ? 'avatar.png' : $getMember->photo);
            $info_member  = '
                <div class="card mb-4">
                    <div class="card-body py-2">
                        <h6 class="heading-small text-capitalize mb-0">Informasi Member</h6>
                        <hr class="mt-0 mb-2">
                        <div class="row align-items-center">
                            <div class="col-auto">
                                <a href="#" class="avatar rounded-circle">
                                    <img alt="Image placeholder" src="' . BE_IMG_PATH . 'icons/' . $avatar . '">
                                </a>
                            </div>
                            <div class="col">
                                <h4 class="mb-0">
                                    <a href="#!">' . $getMember->name . '</a>
                                </h4>
                                <p class="text-sm text-muted font-weight-bold mb-0">
                                    <i class="ni ni-single-02 text-muted mr-1"></i> ' . $getMember->username . '
                                </p>
                            </div>
                        </div>
                        <hr class="my-2">
                        <div class="row">
                            <div class="col-sm-3"><span class="text-capitalize text-muted">Telp</span></div>
                            <div class="col-sm-9"><span>' . $getMember->phone . '</span></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3"><span class="text-capitalize text-muted">' . lang('reg_email') . '</span></div>
                            <div class="col-sm-9"><span class="text-lowecase">' . $getMember->email . '</span></div>
                        </div>
                    </div>
                </div>';
        }

        $order_detail   = '
            <div class="row">
                <div class="col-md-5 px-2">
                    ' . $info_member . '
                    ' . $info_customer . '
                </div>
                <div class="col-md-7 px-2">
                    ' . $info_product . '
                </div>
            </div>
        ';
        return $order_detail;
    }

    // =============================================================================================
    // ACTION DETAIL PRODUCT
    // =============================================================================================

    /**
     * Get Product Detail Function
     */
    function getproductdetailshopping($id = 0)
    {
        if (!$this->input->is_ajax_request()) {
            redirect(base_url('report/shoplist'), 'refresh');
        }
        $auth = auth_redirect($this->input->is_ajax_request());
        if (!$auth) {
            $data = array('status' => 'access_denied', 'url' => base_url('login'));
            die(json_encode($data)); // JSON encode data
        }

        if (!$id) {
            $data = array('status' => 'error', 'message' => 'Produk tidak ditemukan !');
            die(json_encode($data));
        }

        $id         = bp_decrypt($id);
        if (!$productdata = bp_products($id)) {
            $data = array('status' => 'error', 'message' => 'Produk tidak ditemukan !');
            die(json_encode($data));
        }

        $set_html       = $this->sethtmlproductdetailshopping($productdata);
        $data = array('status' => 'success', 'message' => 'Produk Detail', 'data' => $set_html);
        die(json_encode($data));
    }

    /**
     * Set HTML Product Detail function.
     */
    private function sethtmlproductdetailshopping($productdata = 0)
    {
        $product_detail     = '';
        if (!$productdata) {
            return $product_detail;
        }

        $this->load->helper('shop_helper');

        $current_member     = bp_get_current_member();
        $is_admin           = as_administrator($current_member);
        $currency           = config_item('currency');
        $shopping_lock      = config_item('shopping_lock');
        $product_id         = bp_encrypt($productdata->id);

        // --------------------------------------
        // Get Cart Content
        // --------------------------------------
        $cart_content       = bp_cart_contents();
        $product_cart       = isset($cart_content['data']) ? $cart_content['data'] : array();
        $in_cart            = false;
        if ($product_cart) {
            foreach ($product_cart as $item) {
                $cart_product_id = isset($item['id']) ? $item['id'] : 'none';
                if ($cart_product_id == $productdata->id) {
                    $in_cart = true;
                }
            }
        }

        $btn_addtocart      = '';
        if (!$shopping_lock) {
            $btn_addtocart  = '<a class="btn btn-default add-to-cart text-uppercase" 
                                href="' . base_url('shopping/addToCart') . '" 
                                data-type="addcart" 
                                data-cart="' . $product_id . '">
                                <span class="shopping-cart-loading mr-2" style="display:none"><img src="' . BE_IMG_PATH . 'loading-spinner-blue.gif"></span>
                                ADD TO CART</a>';
            if ($in_cart) {
                $btn_addtocart = '<a class="btn btn-default add-to-cart text-uppercase" href="' . base_url('shopping/cart') . '" data-type="cart">GO TO CART</a>';
            }
        }

        $img_src            = bp_product_image($productdata->image, false);
        $category           = '';
        if ($productdata->id_category) {
            $categorydata   = bp_product_category($productdata->id_category);
            $category       = isset($categorydata->name) ? $categorydata->name : '';
            if ($category) {
                $category   = '<h6 class="text-muted text-capitalize ls-1 mb-0" style="font-size: 0.75rem;"><i class="ni ni-tag mr-1"></i>' . $category . '</h6>';
            }
        }

        $product_price      = ($current_member->as_stockist >= 2) ? $productdata->price : $productdata->price_member;
        $view_price         = bp_accounting($product_price, $currency);

        $product_detail     = '
            <div class="row">
                <div class="col-xl-5 order-xl-1 bg-secondary py-2">
                    <a href="' . $img_src . '" target="_blank">
                        <div class="thumbnail mb-1">
                            <img class="img-thumbnail" width="100%" src="' . $img_src . '" style="cursor: pointer;">
                        </div>
                    </a>
                </div>
                <div class="col-xl-7 order-xl-2 pt-3">
                    <div class="card">
                        <div class="card-header border-0">
                            <div class="row align-items-center">
                                <div class="col">
                                    <h4 class="mb-0 text-capitalize text-capitalize">' . $productdata->name . '</h4>
                                    ' . $category . '
                                </div>
                            </div>
                        </div>
                        <div class="card-body pt-0 pb-3">
                            <h2 class="h2 text-warning font-weight-bold">' . $view_price . '</h2>
                            <hr class="my-3">
                            ' . $btn_addtocart . '
                        </div>
                    </div>      
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header border-0">
                            <div class="row align-items-center">
                                <div class="col">
                                    <h3 class="mb-0 text-capitalize text-capitalize">' . lang('information') . '</h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-body py-2 pb-3">
                            ' . $productdata->description . '
                        </div>
                    </div>  
                </div>
            </div>
        ';
        return $product_detail;
    }
}

/* End of file Shopping.php */
/* Location: ./app/controllers/Shopping.php */
