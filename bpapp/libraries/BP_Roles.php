<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Roles Class
 *
 * @subpackage	Libraries
 */
class BP_Roles extends CI_Session
{

	/**
	 * 
	 */
	public $CI;

	/**
	 * 
	 */
	private $data;

	/**
	 * 
	 */
	private $role;

	/**
	 * 
	 */
	private $allowed_access;

	/**
	 * 
	 */
	private $restricted_access;

	/**
	 * 
	 */
	private $access_text;

	/**
	 * 
	 */
	protected static $DASHBOARD 		= array('backend/index');
	protected static $PROFILE 			= array('backend/profile', 'member/personalinfo', 'member/changepassword');
	protected static $AUTHACCESS 		= array('auth/logout', 'auth/validateyearstages');
	protected static $LOGOUT 			= array('auth/logout');
	protected static $SEARCHGENERAL 	= array(
		'member/searchtree',
		'member/searchboardtree',
		'member/searchuplinetree',
		'member/searchmemberdata',
		'member/searchmember'
	);

	// MASTER ACCESS
	protected static $MASTER_PAGE 		= array(
		'backend/urusanlist',
		'backend/suburusanlist',
		'backend/programlist',
		'backend/kegiatanlist',
		'backend/subkegiatanlist',
		'master/urusanlistsdata',
		'master/suburusanlistsdata',
		'master/programlistsdata',
		'master/kegiatanlistsdata',
		'master/subkegiatanlistsdata',
	);

	// MASTER CRUD ACCESS
	protected static $MASTER_CRUD 		= array(
		'master/saveurusan',
		'master/savesuburusan',
		'master/saveprogram',
		'master/savekegiatan',
		'master/savesubkegiatan',
		'master/deleteurusan',
		'master/deletesuburusan',
		'master/deleteprogram',
		'master/deletekegiatan',
		'master/deletesubkegiatan',
	);

	// STAFF	
	protected static $USER 			= array(
		'user/index',
		'user/manage',
		'user/managelist',
		'user/formuser',
		'user/edit',
		'user/getuser',
		'user/saveuser',
		'user/resetpassword',
		'user/del'
	);

	// SETTING	
	protected static $SETTING			= array(
		'setting/general',
		'setting/updatesetting',
		'setting/updatecompany',
		'setting/updatecompanybilling',
		'setting/notification',
		'setting/notificationlistdata',
		'setting/notifdata',
		'setting/updatenotification',
		'setting/reward',
		'setting/rewardlistdata',
		'setting/savereward',
		'setting/withdraw',
		'setting/updatewithdraw'
	);

	/**
	 * Session Constructor
	 *
	 * The constructor runs the session routines automatically
	 * whenever the class is instantiated.
	 */
	public function __construct($params = array())
	{
		$this->CI = &get_instance();
		$this->allowed_access = array();
		$this->restricted_access = array();
	}

	// --------------------------------------------------------------------

	public function user($id_user)
	{
		if (empty($id_user))
			return false;

		if (!$userdata = $this->CI->Model_Auth->get($id_user))
			return false;

		$this->data = $userdata;

		// set role
		$this->_set_role();

		// return staff object
		return $this->data;
	}

	// --------------------------------------------------------------------

	public function has_access()
	{
		$path = $this->_get_current_path();

		if ($this->data->type == ADMIN) {
			if ($this->data->access == 'all') {
				if (in_array($path, $this->restricted_access)) {
					return false;
				}
			} else {
				// partial access
				if (in_array($path, $this->allowed_access)) {
					return true;
				}
				return false;
			}
		}

		if ($this->data->type == SKPD || $this->data->type == DEWAN || $this->data->type == UNIT) {
			if (in_array($path, $this->restricted_access)) {
				return false;
			}
		}

		return true;
	}

	// --------------------------------------------------------------------

	public function get_access_text()
	{
		return $this->access_text;
	}

	// --------------------------------------------------------------------

	protected function _set_role()
	{
		$this->role = array();
		$this->access_text = array();

		if (is_array($this->data->roles)) {
			$this->role = $this->data->roles;
		}

		$config_access_text = config_item('staff_access_text');

		$this->_add_allowed_access(self::$DASHBOARD);
		$this->_add_allowed_access(self::$PROFILE);
		$this->_add_allowed_access(self::$LOGOUT);
		$this->_add_allowed_access(self::$AUTHACCESS);

		if ($this->data->type == ADMIN) {
			foreach ($this->role as $role) {
				$this->access_text[] = $config_access_text[$role];
				switch ($role) {
					case STAFF_ACCESS1:
						$this->_add_allowed_access(self::$MASTER_PAGE);
						break;
					case STAFF_ACCESS2:
						$this->_add_allowed_access(self::$MASTER_PAGE);
						$this->_add_allowed_access(self::$MASTER_CRUD);
						break;
				}
			}

			if ($this->data->access == 'all') {
				$this->access_text = array('Semua Fitur');
				foreach (array(STAFF_ACCESS14, STAFF_ACCESS15) as $role) {
					if (empty($this->role) || !in_array($role, $this->role)) {
						$this->access_text[] = 'Tidak bisa akses ' . $config_access_text[$role];
						switch ($role) {
							case STAFF_ACCESS14:
								$this->_add_restricted_access(self::$USER);
								break;
							case STAFF_ACCESS15:
								$this->_add_restricted_access(self::$SETTING);
								break;
						}
					}
				}
			}
		}


		if ($this->data->type == SKPD || $this->data->access == 'skpd') {
			$this->_add_restricted_access(self::$MASTER_PAGE);
		}

		if ($this->data->type == DEWAN || $this->data->access == 'dewan') {
			$this->_add_restricted_access(self::$MASTER_PAGE);
		}

		if ($this->data->type == UNIT || $this->data->access == 'unit') {
			$this->_add_restricted_access(self::$MASTER_PAGE);
		}
	}

	// --------------------------------------------------------------------

	protected function _get_current_path()
	{
		$controller = $this->CI->router->fetch_class();
		$method = $this->CI->router->fetch_method();
		return $controller . '/' . $method;
	}

	// --------------------------------------------------------------------

	protected function _add_allowed_access($access)
	{
		$this->_add_access($access, $this->allowed_access);
	}

	// --------------------------------------------------------------------

	protected function _add_restricted_access($access)
	{
		$this->_add_access($access, $this->restricted_access);
	}

	// --------------------------------------------------------------------

	protected function _add_access($access, &$to)
	{
		if (is_string($access)) {
			$to[] = $access;
			$to = array_unique($to);
			return;
		}

		$to = array_merge($to, $access);
		$to = array_unique($to);
	}

	// --------------------------------------------------------------------
}
// END Session Class